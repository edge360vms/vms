﻿//using LibVLCSharp.Shared;

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using CefSharp;
using CefSharp.Wpf;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.Extensions.CommandLine;
using Edge360.Platform.VMS.Client.Extensions.Commands;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Layouts;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Services;
using Edge360.Platform.VMS.Client.View.Control;
using Edge360.Platform.VMS.Client.View.Window;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Common.Taskbar;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Monitoring.Input;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.TelemetryControl;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Vms.Client.AuditLog;
using Edge360.VMS.Client.ImageEditor.Enum;
using Edge360.VMS.Client.ImageEditor.ImageEditor;
using Edge360.Vms.Client.VideoExport.Modules;
using log4net;
using log4net.Core;
using Microsoft.Win32;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Unity;
using Telerik.WinControls;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Animation;
using Unity;
using Unity.log4net;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {
        public static LoginWindow LoginWindow;
        private static IntPtr LoginWindowHandle;
        public static readonly ILog TestLog = LogManager.GetLogger("TestLogger");
        private Mutex _instanceMutex;
        private ILog _log;

        public static UnityContainer GlobalUnityContainer { get; private set; }
        public static bool IsExiting { get; set; }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override Window CreateShell()
        {
            if (CommandLineManager.StartupArgs.CacheCreate)
            {
                return null;
            }



            // Sets Process ID to a value that Log4net can see
            GlobalContext.Properties["pid"] = Process.GetCurrentProcess().Id;


           


            // Adds Trace Listener for Log4Net
            //Trace.Listeners.Add(
            //    new Log4netTraceListener(_log));
            //Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
            try
            {
                //    Core.Initialize(@"C:\Program Files\VideoLAN\VLC\");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }

            try
            {
                AppCommunication.InitIpcCommunication();
                AppCommunication.SendOpenHeartbeatWatchdog().ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }

            LayoutManager.LoadLayouts();
            if (SettingsManager.Settings.Miscellaneous.DisableHardwareAcceleration)
            {
                RenderOptions.ProcessRenderMode = RenderMode.SoftwareOnly;
            }

            //LPRListenServer.StartServer();
            StartCommunicationManager();
            GreenTheme();
            RegisterFonts();
            //ShowWindow<LoginWindow>();\
            var baseDirectory = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            if (baseDirectory?.Parent != null)
            {
                TaskbarUtil.TaskbarInfo = new TaskbarInfo
                {
                    TargetFile = Path.Combine(baseDirectory.Parent.FullName, "Edge360.Platform.VMS.Launcher.exe"),
                    AppGuid = new Guid("{71C1A27F-47F4-4182-BB45-793BC88FCBF9}"),
                    AppDisplayName = "Edge360 VMS"
                };
            }
            
            LoginWindow = Container.Resolve<LoginWindow>();
            LoginWindow?.Show();
            LoginWindow?.GetParentWindow<Window>()?.GlobalActivate();

            Task.Run(async () =>
            {
                try
                {
                    DirectInputManager.StartJoystick();
                }
                catch (Exception e)
                {
                    _log?.Error(e);
                }
                var offlineMode = await AppCommunication.HandleOfflineMode(CommandLineManager.StartupArgs);
                var handleLayout = await AppCommunication.HandleRecoveryLayout(CommandLineManager.StartupArgs);
            });

            //var testWindow = Container.Resolve<TestWindow>();
            //testWindow.Show();

            return null;
            //return Container.Resolve<TestWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            var unityContainer = containerRegistry.GetContainer();
            GlobalUnityContainer = unityContainer as UnityContainer;
            containerRegistry.RegisterSingleton<LoginViewModel>();
            Microsoft.Win32.SystemEvents.SessionSwitch += SystemEventsOnSessionSwitch;
            //unityContainer.RegisterFactory<ILog>(factory => new LoggerForInjection());
            // XmlConfigurator.ConfigureAndWatch(new FileInfo("log4net.config"));
            unityContainer
                .AddNewExtension<Log4NetExtension>();
            containerRegistry.RegisterSingleton<IApplicationCommands, EdgeApplicationCommands>();

            RegisterItems(unityContainer);
            unityContainer.RegisterType<CameraItemModel>();
        }

        private void SystemEventsOnSessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            try
            {
                if (LoginViewModel.IsLoggedIn)
                {
                    var tabbedWindow = ObjectResolver.Resolve<TabbedWindow>();
                    if (tabbedWindow != null)
                    {
                        tabbedWindow.SkipLogOutPrompt = true;
                        tabbedWindow?.Model?.LogOut(true);
                    }


                }
            }
            catch (Exception exception)
            {
                
            }
        }

        private void OnSessionEnding(object sender, SessionEndingCancelEventArgs e)
        {
            try
            {
                var tabbedWindow = ObjectResolver.Resolve<TabbedWindow>();
                tabbedWindow.SkipLogOutPrompt = true;
                tabbedWindow?.Model?.LogOut(true);
            }
            catch (Exception exception)
            {
                
            }
        }

        public T2 RegisterUnitySingleton<T1, T2>() where T2 : T1
        {
            GlobalUnityContainer.RegisterSingleton<T2>();

            var instance = GlobalUnityContainer.Resolve<T2>();

            var instanceType = typeof(T2).Name;
            GlobalUnityContainer.RegisterInstance<T1>(instanceType,
                instance, InstanceLifetime.Singleton);
            return instance;
        }

        private void RegisterItems(IUnityContainer unityContainer)
        {
            try
            {
                Current.Dispatcher.Invoke(async () =>
                {
                    unityContainer.RegisterSingleton<LoginWindow>();
                    unityContainer.RegisterSingleton<LoginWindowViewModel>();
                    //modeManager
                    unityContainer.RegisterSingleton<DirectModeManager>();
                    unityContainer.Resolve<DirectModeManager>();
                  //  unityContainer.RegisterSingleton<ActiveWindowService>();
                    //itemManagers

                    RegisterUnitySingleton<IBaseManager, PermissionItemManager>();
                    RegisterUnitySingleton<IBaseManager, NodeObjectItemManager>();
                    RegisterUnitySingleton<IBaseManager, LayoutItemManager>();
                    RegisterUnitySingleton<IBaseManager, PresetItemManager>();
                    RegisterUnitySingleton<IBaseManager, StreamProfileItemManager>();
                    RegisterUnitySingleton<IBaseManager, RecordingProfileItemManager>();
                    RegisterUnitySingleton<IBaseManager, OnvifProfileItemManager>();
                    RegisterUnitySingleton<IBaseManager, ZoneItemManager>();
                    RegisterUnitySingleton<IBaseManager, CameraItemModelManager>();
                    RegisterUnitySingleton<IBaseManager, RoleItemManager>();
                    RegisterUnitySingleton<IBaseManager, ServerItemManager>();
                    RegisterUnitySingleton<IBaseManager, UserItemManager>();
                    RegisterUnitySingleton<IBaseManager, GroupItemManager>();
                    RegisterUnitySingleton<IBaseManager, VolumeItemManager>();
                    RegisterUnitySingleton<IBaseManager, ThumbnailItemManager>();

                    //nodeObjectItemManager.ItemCollection.Add(Guid.NewGuid(), new NodeItemModel(new NodeDescriptor()){Label = "1"});
                    //nodeObjectItemManager.ItemCollection.Add(Guid.NewGuid(), new NodeItemModel(new NodeDescriptor()){Label = "2"});

                    //var nodemgr = (NodeObjectItemManager) GlobalUnityContainer.Resolve<IBaseManager>(nameof(NodeObjectItemManager));
                    //nodemgr.ItemCollection.Add(Guid.NewGuid(), new NodeItemModel(new NodeDescriptor()){Label = "3"});
                    //var resp = unityContainer.Resolve<IEnumerable<IBaseManager>>();
                    //var services = unityContainer.ResolveAll<IBaseManager>();

                    //intervalServices

                    unityContainer.RegisterSingleton<CacheUpdateService>();
                    //var windowService = unityContainer.Resolve<ActiveWindowService>();
                    //windowService.Initialize();
                    var cacheUpdateService = unityContainer.Resolve<CacheUpdateService>();
                    cacheUpdateService.Initialize();

                    var cacheCreation = false;
                    try
                    {
                        var offlineMode = await AppCommunication.HandleOfflineMode(CommandLineManager.StartupArgs);
                        cacheCreation = await AppCommunication.HandleCreateCache(CommandLineManager.StartupArgs);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }

                    try
                    {
                        if (cacheCreation)
                        {
                            var updateCount = 0;
                            cacheUpdateService.UpdateFinished += async () =>
                            {
                                updateCount++;
                                if (updateCount > 1)
                                {
                                    try
                                    {
                                        await Task.Delay(2000);
                                        Current?.Dispatcher?.InvokeAsync(() =>
                                        {
                                            Current.Shutdown(0); //Exit(0);
                                        });
                                    }
                                    catch (Exception e)
                                    {
                                        Environment.Exit(1);
                                    }
                                }
                            };
                        }
                    }
                    catch (Exception exception)
                    {
                    }
                });
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        protected override void ConfigureViewModelLocator()
        {
            base.ConfigureViewModelLocator();
            ViewModelLocationProvider.SetDefaultViewModelFactory((view, viewModelType) =>
            {
                //registers singletons for types
                //if (!Container.GetContainer().IsRegistered(view.GetType()))
                //{
                //    Container.GetContainer().RegisterSingleton(view.GetType());
                //    Container.GetContainer().RegisterSingleton(viewModelType);
                //}


                var viewModel = Container.Resolve(viewModelType);
                ObjectResolver.Register(view.GetType(), view);
                ObjectResolver.Register(viewModelType, viewModel);
                return viewModel;
            });
        }

        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            try
            {
                moduleCatalog.AddModule<ExportJobsModule>();
                //moduleCatalog.AddModule(new ModuleInfo
                //{
                //    ModuleName = (typeof(ExportedJobsModule)).Name,
                //    ModuleType = (typeof(ExportedJobsModule)).AssemblyQualifiedName,
                //});
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            try
            {
                if (SettingsManager.Settings.Miscellaneous.UseAuditTool)
                {
                    moduleCatalog.AddModule<AuditLogModule>();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            //var sw = new Stopwatch();
            //sw.Start();

            try
            {
                if (SettingsManager.Settings.Miscellaneous.UseHealthMonitoring)
                {
                    moduleCatalog.AddModule<TelemetryModule>();
                    Task.Run(async () =>
                    {
                        try
                        {
                            TelemetryControl.View.TelemetryControl.InitializeTelemetry();
                            TelemetryControl.View.TelemetryControl.MeterFontSize = 20;
                            TelemetryControl.View.TelemetryControl.LowColor = "Gray";
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            throw;
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            //sw.Stop();
            //Console.WriteLine($"Telemetry Control took {sw.ElapsedMilliseconds}ms to initialize!");
            //base.ConfigureModuleCatalog(moduleCatalog);
        }

        private void InitCefSharp()
        {
            try
            {
                var settings = new CefSettings
                {
                    BrowserSubprocessPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                        Environment.Is64BitProcess ? "x64" : "x86",
                        "CefSharp.BrowserSubprocess.exe")
                };

                // Set BrowserSubProcessPath based on app bitness at runtime

                //string dir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                //    Environment.Is64BitProcess ? "x64" : "x86");
                //var missingDeps = CefSharp.DependencyChecker.CheckDependencies(true, false, dir, string.Empty,
                //    Path.Combine(dir, "CefSharp.BrowserSubprocess.exe"));
                //if (missingDeps?.Count > 0)
                //    throw new InvalidOperationException("Missing components:\r\n  " + string.Join("\r\n  ", missingDeps));
                //// ReSharper disable once UnusedVariable
                //var browser = new CefSharp.Wpf.ChromiumWebBrowser(); //test, if browser can be instantiated


               // settings.CefCommandLineArgs.Add("enable-media-stream");
                //settings.RegisterScheme(new CefCustomScheme
                //{
                //    SchemeName = CustomProtocolSchemeHandlerFactory.SchemeName,
                //    SchemeHandlerFactory = new CustomProtocolSchemeHandlerFactory(),
                //    IsCSPBypassing = true
                //});
                //settings.LogSeverity = LogSeverity.Error;
                // Make sure you set performDependencyCheck false
                Cef.Initialize(settings); //, true, browserProcessHandler: null);
            }
            catch (Exception e)
            {
               _log?.Error($"Could not Initialize {e.GetType()} {e.Message}");
               // Trace.WriteLine($"Could not Initialize {e.GetType()} {e.Message}");
            }
        }

        protected override async void OnStartup(StartupEventArgs e)
        {
            AppDomain.CurrentDomain.AssemblyResolve += CefResolver;
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomainOnAssemblyResolve;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            _log = LogManager.GetLogger(typeof(App));
            _log?.Info("Application has started.");
            _log?.Info($"Application OnStartup.");
            _log?.Error($"Test");
            CommandLineManager.Start(e?.Args);
            GlobalUnityContainer = Container?.GetContainer() as UnityContainer;
            _instanceMutex = new Mutex(true, "Edge360VMS-Client", out var isNewInstance);
            SettingsManager.LoadSettings();
            if (!isNewInstance && !CommandLineManager.StartupArgs.AllowMultiInstance &&
                !SettingsManager.Settings.Miscellaneous.IsMultiInstanceEnabled)
            {
                try
                {
                    AppCommunication.SendIpcCommunication(Environment.GetCommandLineArgs());
                }
                catch (Exception)
                {
                }

                if (!CommandLineManager.StartupArgs.CacheCreate)
                {
                    Environment.Exit(0);
                }
            }

            try
            {
                ThreadPool.GetMaxThreads(out int workerThreadsCount, out int ioThreadsCount);
                Trace.WriteLine($"Maximum Thread Pool Workers:{workerThreadsCount} IO:{ioThreadsCount}");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
            try
            {
                InitCefSharp();
            }
            catch (Exception exception)
            {
                _log?.Error($"{exception.GetType()} {exception.Message}");
            }
            base.OnStartup(e);
        }

        private static void RegisterFonts()
        {
            var font = FontUtil.GetFontAwesomeFont();
            RadGlyph.RegisterFont(font);
        }

        private static void GreenTheme()
        {
            try
            {
                CustomRadImageEditor.HIDE_OPEN_BUTTON = false;
                ImageEditorManager.AddModule(ECommandType.Watermark, EToolType.Custom);
                ImageEditorManager.AddModule(ECommandType.AdjustLevels, EToolType.Custom);
                ThemeResolutionService.LoadPackageFile(
                    @".\Resources\Themes\VisualStudio2012Dark.tssp");
                ThemeResolutionService.ApplicationThemeName = "VisualStudio2012Dark";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            var animationEnabled = !SettingsManager.Settings.Miscellaneous.DisableAnimations;
            if (SettingsManager.Settings.Miscellaneous.DisableHardwareAcceleration ||
                RenderOptions.ProcessRenderMode == RenderMode.SoftwareOnly)
            {
                animationEnabled = false;
            }

            AnimationManager.IsGlobalAnimationEnabled = animationEnabled;
            StyleManager.ApplicationTheme = new GreenTheme();
            GreenPalette.Palette.AccentHighColor = GetColorFromString("#FF3B3B4C");
            GreenPalette.Palette.AccentLowColor = GetColorFromString("#FF7D86B1");
            GreenPalette.Palette.AlternativeColor = GetColorFromString("#FF1D1E21");
            GreenPalette.Palette.BasicColor = GetColorFromString("#FF474747");
            GreenPalette.Palette.ComplementaryColor = GetColorFromString("#FF444446");
            GreenPalette.Palette.HighColor = GetColorFromString("#FF131313");
            GreenPalette.Palette.LowColor = GetColorFromString("#FF343434");
            GreenPalette.Palette.MainColor = GetColorFromString("#FF1B1B1F");
            GreenPalette.Palette.MarkerColor = GetColorFromString("#FFF1F1F1");
            GreenPalette.Palette.MouseOverColor = GetColorFromString("#FF4B4B5F");
            GreenPalette.Palette.PrimaryColor = GetColorFromString("#272a31"); //GetColorFromString("#FF2B2C2E");
            GreenPalette.Palette.SelectedColor = GetColorFromString("#FFFFFFFF");
            GreenPalette.Palette.SemiAccentLowColor = GetColorFromString("#596560b3");
            GreenPalette.Palette.StrongColor = GetColorFromString("#FF646464");
            GreenPalette.Palette.ValidationColor = GetColorFromString("#FFE60000");
            GreenPalette.Palette.DisabledOpacity = 0.2;
            GreenPalette.Palette.ReadOnlyOpacity = 1;
        }

        public static Color GetColorFromString(string color)
        {
            try
            {
                var convertedColor = ColorConverter.ConvertFromString(color);
                if (convertedColor != null)
                {
                    return (Color) convertedColor;
                }
            }
            catch (Exception)
            {
            }

            return default;
        }


        private static void VsDarkTheme()
        {
            AnimationManager.IsGlobalAnimationEnabled = false;
            StyleManager.ApplicationTheme = new VisualStudio2019Theme();
            VisualStudio2019Palette.Palette.AccentColor = GetColorFromString("#FF484975");
            VisualStudio2019Palette.Palette.AccentMainColor = GetColorFromString("#FFFFFFFF");
            VisualStudio2019Palette.Palette.AccentDarkColor = GetColorFromString("#FFC6D3FF");
            VisualStudio2019Palette.Palette.AccentSecondaryDarkColor =
                GetColorFromString("#FFFFFFFF");
            VisualStudio2019Palette.Palette.AccentMouseOverColor =
                GetColorFromString("#FF525E7D");
            VisualStudio2019Palette.Palette.AccentFocusedColor = GetColorFromString("#FFD6CDBD");
            VisualStudio2019Palette.Palette.ValidationColor = GetColorFromString("#FFC16D6D");
            VisualStudio2019Palette.Palette.BasicColor = GetColorFromString("#FF16191D");
            VisualStudio2019Palette.Palette.SemiBasicColor = GetColorFromString("#FF7386C2");
            VisualStudio2019Palette.Palette.PrimaryColor = GetColorFromString("#FF252833");
            VisualStudio2019Palette.Palette.SecondaryColor = GetColorFromString("#FF45475F");
            VisualStudio2019Palette.Palette.MarkerColor = GetColorFromString("#FFE9E9E9");
            VisualStudio2019Palette.Palette.MarkerInvertedColor = GetColorFromString("#FFFEFCFC");
            VisualStudio2019Palette.Palette.IconColor = GetColorFromString("#FFECECEC");
            VisualStudio2019Palette.Palette.AlternativeColor = GetColorFromString("#FFF7F9FE");
            VisualStudio2019Palette.Palette.MouseOverColor = GetColorFromString("#FF616184");
            VisualStudio2019Palette.Palette.ComplementaryColor = GetColorFromString("#FFD9E0F8");
            VisualStudio2019Palette.Palette.MainColor = GetColorFromString("#FFFCFCFC");
            VisualStudio2019Palette.Palette.HeaderColor = GetColorFromString("#FF40568D");
            VisualStudio2019Palette.Palette.ReadOnlyBackgroundColor =
                GetColorFromString("#FFFCFCFC");
            VisualStudio2019Palette.Palette.ReadOnlyBorderColor = GetColorFromString("#FF94A6CA");
            VisualStudio2019Palette.Palette.DisabledOpacity = 0.3;
            VisualStudio2019Palette.Palette.ReadOnlyOpacity = 0.6;
        }

        protected override async void OnExit(ExitEventArgs e)
        {
            IsExiting = true;
            await ExitApp();
            base.OnExit(e);
        }

        public static async Task ExitApp()
        {
            try
            {
                TaskbarUtil.Cleanup(LoginWindowHandle);
            }
            catch (Exception e)
            {
            }

            DirectInputManager.StopJoystick();

            await AppCommunication.SendCloseHeartbeatWatchdog();
            SettingsManager.ExitingApplication();
            LayoutManager.SaveLayouts();
        }

        private static Assembly LookForAssemblyInLocation(string libraryLocation, AssemblyName assemblyName)
        {
            Assembly assembly = null;
            var assemblyPath = Path.Combine(libraryLocation, assemblyName.Name + ".dll");
            try
            {
                if (!string.IsNullOrEmpty(assemblyPath))
                {
                    if (File.Exists(assemblyPath))
                    {
                        assembly = Assembly.LoadFrom(assemblyPath);
                    }
                }
            }
            catch (Exception)
            {
            }

            return assembly;
        }

// Will attempt to load missing assembly from either x86 or x64 subdir
        private static Assembly CefResolver(object sender, ResolveEventArgs args)
        {
            try
            {
                if (args.Name.StartsWith("CefSharp"))
                {
                    var assemblyName = args.Name.Split(new[] {','}, 2)[0] + ".dll";
                    var archSpecificPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                        Environment.Is64BitProcess ? "x64" : "x86",
                        assemblyName);

                    return File.Exists(archSpecificPath)
                        ? Assembly.LoadFile(archSpecificPath)
                        : null;
                }

                return null;
            }
            catch (Exception e)
            {
                Trace.WriteLine($"CefResolver Error {e.GetType()} {e.Message}");
                return null;
            }
        }


        private void CurrentDomainOnUnhandledException(object sender,
            UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            Trace.WriteLine($"Unhandled Exception {unhandledExceptionEventArgs.ExceptionObject}");
            _log?.Error($"Unhandled Exception {unhandledExceptionEventArgs.ExceptionObject}");
            
        }

        private Assembly CurrentDomainOnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            var libraryLocation = $"{AppDomain.CurrentDomain.BaseDirectory}lib";
            Assembly assembly = null;
            var assemblyName = new AssemblyName(args.Name);

            assembly = LookForAssemblyInLocation(libraryLocation, assemblyName);
            if (assembly == null)
            {
                assembly = LookForAssemblyInLocation($"{AppDomain.CurrentDomain.BaseDirectory}x64", assemblyName);
                if (assembly == null)
                {
                    assembly = LookForAssemblyInLocation($"{AppDomain.CurrentDomain.BaseDirectory}x86", assemblyName);
                }
            }

            return assembly;
        }
    }
}