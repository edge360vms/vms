﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Common.Enums.Video;

namespace Edge360.Platform.VMS.Client.ItemModel.Video.Settings
{
    public class OverlayFlagItemModel : INotifyPropertyChanged
    {
        private bool _isChecked;
        private EControlOverlaySettings _setting;
        public bool ChangeSilently { get; set; }

        public bool IsChecked
        {
            get => _isChecked;
            set
            {
                if (_isChecked != value)
                {
                    _isChecked = value;
                    if (!ChangeSilently)
                    {
                        FlagChanged?.Invoke(Setting, value);
                    }

                    OnPropertyChanged();
                }
            }
        }

        public EControlOverlaySettings Setting
        {
            get => _setting;
            set
            {
                if (_setting != value)
                {
                    _setting = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event Action<EControlOverlaySettings, bool> FlagChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}