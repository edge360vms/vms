﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.StreamProfiles;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Interfaces;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.StreamingProfile;
using Edge360.Raven.Vms.CameraService.ServiceModel.Enums;
using Newtonsoft.Json;
using Prism.Commands;
using ServiceStack;

namespace Edge360.Platform.VMS.Client.ItemModel.Stream
{
    public class StreamProfileItemModel : IStreamingProfile, INotifyPropertyChanged
    {
        private Guid _cameraId;

        private DelegateCommand _deleteCommand;
        private StreamingProfileDescriptor _descriptor;
        private string _driverProfileId;
        private DelegateCommand _editCommand;
        private string _format;
        private Guid _id;
        private bool _isEditable;
        private bool _isStatic;
        private string _label;
        private int _maxConcurrentViewers;
        private int _maxViews;
        private string _password;
        private int _propertyFlags;
        private DelegateCommand _refreshCommand;
        private int _resolutionHeight;
        private int _resolutionWidth;
        private string _rtspUrl;
        private StreamProfileItemManager _streamItemMgr;
        private string _type;
        private string _username;
        private Guid _zoneId;

        [JsonIgnore]
        [Editable(false)]
        [Browsable(false)]
        public DelegateCommand DeleteCommand => _deleteCommand ??= new DelegateCommand(Delete);

        [JsonIgnore]
        [Editable(false)]
        [Browsable(false)]
        public StreamingProfileDescriptor Descriptor
        {
            get => _descriptor;
            set
            {
                if (Equals(value, _descriptor))
                {
                    return;
                }

                _descriptor = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [Editable(false)]
        [Browsable(false)]
        public DelegateCommand EditCommand => _editCommand ??= new DelegateCommand(Edit);

        [JsonIgnore]
        [Editable(false)]
        [Browsable(false)]
        public bool IsEditable

        {
            get => _isEditable;
            set
            {
                if (value == _isEditable)
                {
                    return;
                }

                _isEditable = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public ObservableCollection<StreamProfilePropertyItemModel> ProfileProperties { get; set; } =
            new ObservableCollection<StreamProfilePropertyItemModel>();

        [JsonIgnore]
        [Editable(false)]
        [Browsable(false)]
        public DelegateCommand RefreshCommand => _refreshCommand ??= new DelegateCommand(Refresh);


        [JsonIgnore]
        [Editable(false)]
        [Browsable(false)]
        public StreamProfileItemManager StreamItemMgr
        {
            get => _streamItemMgr;
            set
            {
                if (Equals(value, _streamItemMgr))
                {
                    return;
                }

                _streamItemMgr = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [Editable(false)]
        [Browsable(false)]
        public Guid ZoneId
        {
            get => _zoneId;
            set
            {
                if (value.Equals(_zoneId))
                {
                    return;
                }

                _zoneId = value;
                OnPropertyChanged();
            }
        }

        public StreamProfileItemModel()
        {
            try
            {
                StreamItemMgr = StreamProfileItemManager.Manager;

                if (!StreamItemMgr.ItemCollection.Any(o => o.Key.Item1 == Id && !string.IsNullOrEmpty(o.Value.RtspUrl)))
                {
                    StreamItemMgr.Update(
                        new KeyValuePair<Tuple<Guid, Guid>, StreamProfileItemModel>(new Tuple<Guid, Guid>(Id, CameraId),
                            this));
                }
              
            }
            catch (Exception e)
            {
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [JsonIgnore]
        [Editable(false)]
        [ReadOnly(true)]
        public Guid Id
        {
            get => _id;
            set
            {
                if (value.Equals(_id))
                {
                    return;
                }

                _id = value;
                OnPropertyChanged();
            }
        }

        [ReadOnly(true)]
        public Guid CameraId
        {
            get => _cameraId;
            set
            {
                if (value.Equals(_cameraId))
                {
                    return;
                }

                _cameraId = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [Editable(false)]
        [ReadOnly(true)]
        public string Format
        {
            get => _format;
            set
            {
                if (value == _format)
                {
                    return;
                }

                _format = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public EStreamProfileProperty StreamProfileProperty => (EStreamProfileProperty) PropertyFlags;

        [Editable(false)]
        [ReadOnly(true)]
        [Browsable(false)]
        public int PropertyFlags
        {
            get => _propertyFlags;
            set
            {
                if (value == _propertyFlags)
                {
                    return;
                }

                _propertyFlags = value;
                OnPropertyChanged(nameof(StreamProfileProperty));
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [Editable(false)]
        [Browsable(false)]
        public int ResolutionHeight
        {
            get => _resolutionHeight;
            set
            {
                if (value == _resolutionHeight)
                {
                    return;
                }

                _resolutionHeight = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [Editable(false)]
        [Browsable(false)]
        public int ResolutionWidth
        {
            get => _resolutionWidth;
            set
            {
                if (value == _resolutionWidth)
                {
                    return;
                }

                _resolutionWidth = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [Editable(false)]
        [Browsable(false)]
        public string Type
        {
            get => _type;
            set
            {
                if (value == _type)
                {
                    return;
                }

                _type = value;
                OnPropertyChanged();
            }
        }

        public string Label
        {
            get => _label;
            set
            {
                if (value == _label)
                {
                    return;
                }

                _label = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [Editable(false)]
        [ReadOnly(true)]
        public string DriverProfileId
        {
            get => _driverProfileId;
            set
            {
                if (value == _driverProfileId)
                {
                    return;
                }

                _driverProfileId = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [Editable(false)]
        [ReadOnly(true)]
        public string RtspUrl
        {
            get => _rtspUrl;
            set
            {
                if (value == _rtspUrl)
                {
                    return;
                }

                _rtspUrl = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [Editable(false)]
        [Browsable(false)]
        public string Username
        {
            get => _username;
            set
            {
                if (value == _username)
                {
                    return;
                }

                _username = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [Editable(false)]
        [Browsable(false)]
        public string Password
        {
            get => _password;
            set
            {
                if (value == _password)
                {
                    return;
                }

                _password = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        [Editable(false)]
        [Browsable(false)]
        public bool IsStatic
        {
            get => _isStatic;
            set
            {
                if (value == _isStatic)
                {
                    return;
                }

                _isStatic = value;
                OnPropertyChanged();
            }
        }

        public int MaxConcurrentViewers
        {
            get => _maxConcurrentViewers;
            set
            {
                if (value == _maxConcurrentViewers)
                {
                    return;
                }

                _maxConcurrentViewers = value;
                OnPropertyChanged();
            }
        }

        public int MaxViews
        {
            get => _maxViews;
            set
            {
                if (value == _maxViews)
                {
                    return;
                }

                _maxViews = value;
                OnPropertyChanged();
            }
        }

        public override string ToString()
        {
            return
                $"{nameof(ZoneId)}: {ZoneId}, {nameof(Id)}: {Id}, {nameof(CameraId)}: {CameraId}, {nameof(Format)}: {Format}, {nameof(ResolutionHeight)}: {ResolutionHeight}, {nameof(ResolutionWidth)}: {ResolutionWidth}, {nameof(Type)}: {Type}, {nameof(Label)}: {Label}, {nameof(DriverProfileId)}: {DriverProfileId}, {nameof(RtspUrl)}: {RtspUrl}, {nameof(Username)}: {Username}, {nameof(Password)}: {Password}, {nameof(IsStatic)}: {IsStatic}, {nameof(MaxConcurrentViewers)}: {MaxConcurrentViewers}, {nameof(MaxViews)}: {MaxViews}";
        }

        public static implicit operator StreamProfileItemModel(StreamProfile desc)
        {
            var converted = desc.ConvertTo<StreamProfileItemModel>();
            converted.Id = desc.ProfileId;
            converted.ResolutionWidth = desc.Width;
            converted.ResolutionHeight = desc.Height;
            converted.Descriptor = new StreamingProfileDescriptor();
            converted.Label = desc.ProfileName;
            converted.InitPropertyFlags(desc, converted);
            return converted;
        }


        public static implicit operator StreamProfileItemModel(StreamingProfileDescriptor desc)
        {
            var converted = desc.ConvertTo<StreamProfileItemModel>();
            converted.InitPropertyFlags(desc, converted);
            return converted;
        }

        private void InitPropertyFlags(StreamProfile config, StreamProfileItemModel cameraItem)
        {
            try
            {
                foreach (var e in Enum.GetValues(typeof(EStreamProfileProperty)).OfType<EStreamProfileProperty>())
                {
                    try
                    {
                        if (e == EStreamProfileProperty.None)
                        {
                            continue;
                        }

                        var streamProfilePropertyItemModel = new StreamProfilePropertyItemModel
                            {Flag = e, IsChecked = ((EStreamProfileProperty) config.PropertyFlags).HasFlag(e)};
                        streamProfilePropertyItemModel.FlagChanged += StreamProfilePropertyItemModelOnFlagChanged;
                        cameraItem.ProfileProperties.Add(streamProfilePropertyItemModel);
                    }
                    catch (Exception exception)
                    {
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private void InitPropertyFlags(StreamingProfileDescriptor config, StreamProfileItemModel cameraItem)
        {
            try
            {
                foreach (var e in Enum.GetValues(typeof(EStreamProfileProperty)).OfType<EStreamProfileProperty>())
                {
                    try
                    {
                        if (e == EStreamProfileProperty.None)
                        {
                            continue;
                        }

                        var streamProfilePropertyItemModel = new StreamProfilePropertyItemModel
                            {Flag = e, IsChecked = ((EStreamProfileProperty) config.PropertyFlags).HasFlag(e)};
                        streamProfilePropertyItemModel.FlagChanged += StreamProfilePropertyItemModelOnFlagChanged;
                        cameraItem.ProfileProperties.Add(streamProfilePropertyItemModel);
                    }
                    catch (Exception exception)
                    {
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private void StreamProfilePropertyItemModelOnFlagChanged(EStreamProfileProperty flag, bool enabled)
        {
            var existingFlag = (EStreamProfileProperty) PropertyFlags;
            if (enabled)
            {
                existingFlag |= flag;
            }
            else
            {
                existingFlag ^= flag;
            }
            PropertyFlags = (int) existingFlag;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public async void Delete()
        {
            try
            {
                await StreamItemMgr.RemoveProfile(this);
            }
            catch (Exception e)
            {
            }
        }

        private async void Edit()
        {
            try
            {
                await StreamItemMgr.EditProfile(this);
                IsEditable = false;
            }
            catch (Exception e)
            {
            }
        }

        private async void Refresh()
        {
            try
            {
                await StreamItemMgr.RefreshProfile(ZoneId, Id);
            }
            catch (Exception e)
            {
            }
        }
    }

    public class StreamProfilePropertyItemModel : INotifyPropertyChanged
    {
        private EStreamProfileProperty _flag;
        private bool _isChecked;

        public event Action<EStreamProfileProperty, bool> FlagChanged; 

        public EStreamProfileProperty Flag
        {
            get => _flag;
            set
            {
                if (value == _flag)
                {
                    return;
                }

                _flag = value;
                OnPropertyChanged();
            }
        }

        public bool IsChecked
        {
            get => _isChecked;
            set
            {
                if (value == _isChecked)
                {
                    return;
                }

                _isChecked = value;
                FlagChanged?.Invoke(Flag,value);
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}