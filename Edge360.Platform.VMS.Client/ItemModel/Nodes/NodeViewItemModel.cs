﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.Cluster;

namespace Edge360.Platform.VMS.Client.ItemModel.Nodes
{
    public enum ENodeViewAction
    {
        RemoveNode,
        RemoveServer,
        RefreshServers,
        GetZoneClusterStatus,
        UpdateReplication,
    }

    public class CassandraNodeItemModel : INotifyPropertyChanged
    {
        private string _address;
        private EClusterType _clusterType;
        private Guid? _hostId;
        private long? _load;
        private decimal? _ownershipPercent;
        private Guid? _serverId;
        private ENodeState _state;
        private ENodeStatus _status;
        private int? _tokens;
        private Guid? _zoneId;

        public string Address
        {
            get => _address;
            set
            {
                if (value == _address)
                {
                    return;
                }

                _address = value;
                OnPropertyChanged();
            }
        }


        public EClusterType ClusterType
        {
            get => _clusterType;
            set
            {
                if (value == _clusterType)
                {
                    return;
                }

                _clusterType = value;
                OnPropertyChanged();
            }
        }

        public Guid? HostId
        {
            get => _hostId;
            set
            {
                if (Nullable.Equals(value, _hostId))
                {
                    return;
                }

                _hostId = value;
                OnPropertyChanged();
            }
        }

        public long? Load
        {
            get => _load;
            set
            {
                if (value == _load)
                {
                    return;
                }

                _load = value;
                OnPropertyChanged();
            }
        }

        [DisplayName("Owns")]
        public decimal? OwnershipPercent
        {
            get => _ownershipPercent;
            set
            {
                if (value == _ownershipPercent)
                {
                    return;
                }

                _ownershipPercent = value;
                OnPropertyChanged();
            }
        }

        public Guid? ServerId
        {
            get => _serverId;
            set
            {
                if (Nullable.Equals(value, _serverId))
                {
                    return;
                }

                _serverId = value;
                OnPropertyChanged();
            }
        }

        public ENodeState State
        {
            get => _state;
            set
            {
                if (value == _state)
                {
                    return;
                }

                _state = value;
                OnPropertyChanged();
            }
        }

        public ENodeStatus Status
        {
            get => _status;
            set
            {
                if (value == _status)
                {
                    return;
                }

                _status = value;
                OnPropertyChanged();
            }
        }

        public int? Tokens
        {
            get => _tokens;
            set
            {
                if (value == _tokens)
                {
                    return;
                }

                _tokens = value;
                OnPropertyChanged();
            }
        }

        public Guid? ZoneId
        {
            get => _zoneId;
            set
            {
                if (Nullable.Equals(value, _zoneId))
                {
                    return;
                }

                _zoneId = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return
                $"{nameof(Address)}: {Address}, {nameof(ClusterType)}: {ClusterType}, {nameof(HostId)}: {HostId}, {nameof(Load)}: {Load}, {nameof(OwnershipPercent)}: {OwnershipPercent}, {nameof(ServerId)}: {ServerId}, {nameof(State)}: {State}, {nameof(Status)}: {Status}, {nameof(Tokens)}: {Tokens}, {nameof(ZoneId)}: {ZoneId}";
        }

        public static implicit operator CassandraNodeItemModel(NodeStatusResponse response)
        {
            return new CassandraNodeItemModel
            {
                Tokens = response.Tokens,
                Status = response.Status,
                State = response.State,
                ServerId = response.ServerId,
                OwnershipPercent = response.OwnershipPercent,
                Load = response.Load,
                HostId = response.HostId,
                Address = response.Address,
                ZoneId = response.ZoneId,
            };
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    internal class NodeViewItemModel
    {
    }
}