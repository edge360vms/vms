﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using Edge360.Platform.VMS.Monitoring.Properties;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.CameraAdapter;
using Mictlanix.DotNet.Onvif.Common;
using ServiceStack;
using Capabilities = CameraService.Adapters.Shared.Types.Capabilities;

namespace Edge360.Platform.VMS.Client.ItemModel.Camera
{
    public class BaseAdapterCameraProfileItemModel : Profile, INotifyPropertyChanged
    {
        private Size _resolution;
        private string _streamUri;

        public bool IsH264 => VideoEncoderConfiguration.H264 != null;

        public DateTimeOffset LastQueried { get; set; }

        public Size Resolution

        {
            get => _resolution;
            set
            {
                if (value.Equals(_resolution))
                {
                    return;
                }

                _resolution = value;
                OnPropertyChanged();
            }
        }

        public string StreamUri
        {
            get => _streamUri;
            set
            {
                if (value == _streamUri)
                {
                    return;
                }

                _streamUri = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class AdapterCameraProfileItemModel : CameraProfile, INotifyPropertyChanged
    {
        private Size _resolution;

        public Size Resolution

        {
            get => _resolution;
            set
            {
                if (value.Equals(_resolution))
                {
                    return;
                }

                _resolution = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class CameraInformationProfileItemModel : CameraService.Adapters.Shared.Types.Profile, INotifyPropertyChanged
    {
        private System.Drawing.Size _resolution;

        public System.Drawing.Size Resolution

        {
            get => _resolution;
            set
            {
                if (value.Equals(_resolution))
                {
                    return;
                }

                _resolution = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class CameraInformationItemModel : CameraInformation, INotifyPropertyChanged
    {
        private List<CameraInformationProfileItemModel> _profile;

        public Capabilities Capabilities
        {
            get => base.Capabilities;
            set
            {
                if (Equals(value, base.Capabilities))
                {
                    return;
                }

                base.Capabilities = value;
                OnPropertyChanged();
            }
        }

        public List<CameraInformationProfileItemModel> Profile
        {
            get => _profile ??= base.Profile?.Select(o => o?.ConvertTo<CameraInformationProfileItemModel>())?.ToList();
            set
            {
                if (Equals(value, _profile))
                {
                    return;
                }

                _profile = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}