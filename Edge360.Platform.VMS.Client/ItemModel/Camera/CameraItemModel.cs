﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ServiceModel.Security;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using CameraService.Adapters.ONVIF.Adapters;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.DataObject;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.ItemModel.Stream;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Management;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Nodes;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.CameraAdapter;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Cameras;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.CameraStatus;
using Edge360.Raven.Vms.CameraService.ServiceModel.Enums;
using Edge360.Raven.Vms.CameraService.ServiceModel.ItemModels;
using Edge360.Vms.AuthorityService.SharedModel;
using Edge360.Vms.AuthorityService.SharedModel.Enums;
using log4net;
using Mictlanix.DotNet.Onvif;
using Mictlanix.DotNet.Onvif.Device;
using Mictlanix.DotNet.Onvif.Media;
using Newtonsoft.Json;
using RecordingService.ServiceModel.Api.RecordingProfile;
using RecordingService.ServiceModel.Enums;
using ServiceStack;
using Telerik.Windows.Controls;
using WispFramework.Extensions.Common;
using static Edge360.Platform.VMS.Communication.CommunicationManager;
using IActiveAware = Prism.IActiveAware;
using TaskExtensions = WispFramework.Extensions.Tasks.TaskExtensions;

namespace Edge360.Platform.VMS.Client.ItemModel.Camera
{
    public class CameraItemModel : CameraObject, IActiveAware
    {
        public string DirectStreamAddress => CameraUrl?.Replace("rtsp://", "")?.Split('/')[0] ?? "";
   
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        //static CameraItemModel()
        //{
        //    PresetItemManager = presetItemManager;
        //    if (presetItemManager != null)
        //    {

        //        PresetItemManager.CollectionViewSource.Filter += CollectionViewSourceOnFilter;
        //    }
        //}
        private ICommand _addRecordingProfileCommand;
        private CameraStatus _cameraStatus;
        private DateTimeOffset _cameraTime;
        private string _cameraUrl;

        private RelayCommand _configureCommand;
        private RelayCommand _configureHardwareCommand;
        private BaseAdapterCameraProfileItemModel _currentOnvifProfile;
        private ICommand _deleteCommand;
        private DeviceClient _deviceClient;
        private CameraDetails _deviceInformation;
        private RelayCommand _getCameraInformationCommand;
        private bool _isActive;

        private bool _isCameraUpdating;
        private bool _isDirty;
        private bool _isEditable;
        private bool _isExpanded;
        private bool _isGettingCameraInformation;
        private bool _isLoadingCameraDetails;
        private bool _isRefreshingProfiles;
        private bool _isSelected;
        private CollectionViewSource _liveStreamProfileCollectionViewSource;
        private string _liveStreamUrl1;
        private MediaClient _mediaController;
        private OnvifProfileItemManager _onvifProfileMgr;

        private ObservableCollection<BaseAdapterCameraProfileItemModel> _onvifProfiles =
            new ObservableCollection<BaseAdapterCameraProfileItemModel>();

        private RelayCommand _openVideoArchive;

        private NodeItemModel _parent;
        private PermissionItemModel _permissions = new PermissionItemModel();
        private PresetItemManager _presetItemManager;
        private ObservableCollection<PtzPreset> _presets = new ObservableCollection<PtzPreset>();
        private RecordingProfileItemManager _recordingProfileItemMgr;

        private ObservableCollection<RecordingProfileItemModel> _recordingProfiles =
            new ObservableCollection<RecordingProfileItemModel>();

        private RelayCommand _refreshRecordingProfilesCommand;
        private object _selectedEditRecordingProfile;
        private PtzPreset _selectedPreset;
        private StreamProfileItemManager _streamProfileItemMgr;
        private RelayCommand _updateCameraCommand;


        private ICommand _viewRecordingProfileCommand;

        public override string Address
        {
            get => base.Address;
            set
            {
                base.Address = value;
                OnPropertyChanged();
                CheckDirty();
            }
        }

        public override string Label
        {
            get => base.Label;
            set
            {
                base.Label = value;
                OnPropertyChanged();
                CheckDirty();
            }
        }

        public override string LiveStreamUrl
        {
            get => _liveStreamUrl1;
            set
            {
                if (value == _liveStreamUrl1)
                {
                    return;
                }

                _liveStreamUrl1 = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(CameraUrl));
                CheckDirty();
            }
        }


        public override string Password
        {
            get => base.Password;
            set
            {
                base.Password = value;
                OnPropertyChanged();
                CheckDirty();
            }
        }


        public override string Username
        {
            get => base.Username;
            set
            {
                base.Username = value;
                OnPropertyChanged();
                CheckDirty();
            }
        }

        [JsonIgnore]
        public ICommand AddRecordingProfileCommand
        {
            get
            {
                if (_addRecordingProfileCommand == null)
                {
                    _addRecordingProfileCommand =
                        new RelayCommand(p => AddRecordingProfile(), p => CanAddRecordingProfile());
                }

                return _addRecordingProfileCommand;
            }
        }

        [JsonIgnore] public CameraItemModelManager CameraItemMgr { get; set; }

        [JsonIgnore]
        public CameraStatus CameraStatus
        {
            get => _cameraStatus;
            set
            {
                if (Equals(value, _cameraStatus))
                {
                    return;
                }

                _cameraStatus = value;
                OnPropertyChanged();
            }
        }

        public DateTimeOffset CameraTime
        {
            get => _cameraTime;
            set
            {
                if (value.Equals(_cameraTime))
                {
                    return;
                }

                _cameraTime = value;
                OnPropertyChanged();
            }
        }

        public string CameraUrl
        {
            get => _cameraUrl ?? LiveStreamUrl; // ?? (_cameraUrl = this.GetCameraUrl());
            set
            {
                if (value == _cameraUrl)
                {
                    return;
                }

                _cameraUrl = value;
                OnPropertyChanged();
            }
        }


        public string CameraUrlWithCredentials
        {
            get
            {
                var url = CameraUrl;
                try
                {
                    url = AddAuthUri(Username, Password, url);
                }
                catch (Exception e)
                {
                }

                return url;
            }
        }

        [JsonIgnore]
        public RelayCommand ConfigureHardwareCommand => _configureHardwareCommand ??= new RelayCommand(
            o => ConfigureHardware(o));

        [JsonIgnore]
        public RelayCommand ConfigureNodeCommand
        {
            get { return _configureCommand ??= new RelayCommand(Configure, p => CanConfigure()); }
        }

        [JsonIgnore]
        public BaseAdapterCameraProfileItemModel CurrentOnvifProfile
        {
            get => _currentOnvifProfile;
            set
            {
                if (value == _currentOnvifProfile)
                {
                    return;
                }

                _currentOnvifProfile = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public ICommand DeleteCameraCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        param => DeleteCamera(),
                        param => CanDelete()
                    );
                }

                return _deleteCommand;
            }
        }

        public CameraDescriptor Descriptor { get; set; }

        [JsonIgnore]
        public DeviceClient DeviceClient
        {
            get => _deviceClient;
            set
            {
                if (Equals(value, _deviceClient))
                {
                    return;
                }

                _deviceClient = value;
                Task.Run(async () =>
                {
                    try
                    {
                        //var getDeviceInfo = await GetManagerByZoneId(ZoneId).CameraAdapterService
                        //    .GetCameraDetails(new GetCameraDetails {CameraId = Id});
                        var getCameraDetailsResponse = await CameraItemMgr.GetCameraDetails(ZoneId, Id);
                        if (getCameraDetailsResponse != null)
                        {
                            DeviceInformation = getCameraDetailsResponse.CameraDetails; // getDeviceInfo.CameraDetails;
                        }

                        //var getDeviceInformationResponse =
                        //    await _deviceClient.GetDeviceInformationAsync(new GetDeviceInformationRequest());
                        //DeviceInformation =
                        //    new CameraDetails
                        //    {
                        //        Address = getDeviceInformationResponse.FirmwareVersion,
                        //        CameraId = default,
                        //        CameraName = getDeviceInformationResponse.SerialNumber,
                        //        Manufacturer = getDeviceInformationResponse.Manufacturer,
                        //        Model = getDeviceInformationResponse.Model,
                        //        Password = getDeviceInformationResponse.HardwareId,
                        //        Status = getDeviceInformationResponse != null
                        //            ? ECameraStatus.Online
                        //            : ECameraStatus.Offline,
                        //        SupportsPtz = false,
                        //        SupportsPtzPresetTour = false,
                        //    };
                    }
                    catch (Exception e)
                    {
                    }
                });

                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public CameraDetails DeviceInformation
        {
            get => _deviceInformation;
            set
            {
                if (Equals(value, _deviceInformation))
                {
                    return;
                }

                _deviceInformation = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public RelayCommand GetCameraInformationCommand =>
            _getCameraInformationCommand ??= new RelayCommand(GetCameraInformation,
                o => !IsGettingCameraInformation);

        [JsonIgnore]
        public virtual ZoneCameraManagementViewModel
            GetZoneCameraViewModel
        {
            get
            {
                var zoneCameraManagementViewModels = ObjectResolver.ResolveAll<ZoneCameraManagementViewModel>();
                var zoneCameraManagementViewModel = zoneCameraManagementViewModels
                    ?.FirstOrDefault(o => ((dynamic)o?.SelectedZone?.SelectedZoneItem)?.Value == this);
                return zoneCameraManagementViewModel;
            }
        }

        [JsonIgnore]
        public bool IsCameraUpdating
        {
            get => _isCameraUpdating;
            set
            {
                if (_isCameraUpdating != value)
                {
                    _isCameraUpdating = value;
                    OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public bool IsDirty
        {
            get => _isDirty;
            set
            {
                if (_isDirty != value)
                {
                    _isDirty = value;
                    OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public bool IsEditable
        {
            get => _isEditable;
            set
            {
                if (value == _isEditable)
                {
                    return;
                }

                _isEditable = value;
                OnPropertyChanged();
                if (IsDirty && !_isEditable) // cancelEdit
                {
                    if (Descriptor != null)
                    {
                        Label = Descriptor.Label;
                        Address = Descriptor.Address;
                        if (OriginalParent != null)
                        {
                            Parent = OriginalParent;
                            ParentId = OriginalParent.ObjectId;
                        }

                        Username = Descriptor.Username;
                        Password = Descriptor.Password;
                    }
                }
                Trace.WriteLine($"CameraItemModel 440 InvalidateRequerySuggested");
                OnPropertyChanged(nameof(UpdateCameraCommand));
                //CommandManager.InvalidateRequerySuggested();
                CheckDirty();
            }
        }

        [JsonIgnore]
        public bool IsExpanded
        {
            get => _isExpanded;
            set
            {
                if (value == _isExpanded)
                {
                    return;
                }

                _isExpanded = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public bool IsGettingCameraInformation
        {
            get => _isGettingCameraInformation;
            set
            {
                if (value == _isGettingCameraInformation)
                {
                    return;
                }

                _isGettingCameraInformation = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(GetCameraInformationCommand));
            }
        }

        [JsonIgnore] public bool IsGettingOnvifProfiles { get; set; }

        [JsonIgnore]
        public bool IsLoadingCameraDetails
        {
            get => _isLoadingCameraDetails;
            set
            {
                if (value == _isLoadingCameraDetails)
                {
                    return;
                }

                _isLoadingCameraDetails = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public bool IsRefreshingProfiles
        {
            get => _isRefreshingProfiles;
            set
            {
                if (_isRefreshingProfiles != value)
                {
                    _isRefreshingProfiles = value;
                    OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                if (value == _isSelected)
                {
                    return;
                }

                _isSelected = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore] public CollectionViewSource RemoteLiveStreamProfileCollectionViewSource { get; set; }

            [JsonIgnore]
        public CollectionViewSource LiveStreamProfileCollectionViewSource
        {
            get => _liveStreamProfileCollectionViewSource;
            set
            {
                if (Equals(value, _liveStreamProfileCollectionViewSource))
                {
                    return;
                }

                _liveStreamProfileCollectionViewSource = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore] public CollectionViewSource OnvifProfileCollectionViewSource { get; set; }

        [JsonIgnore]
        public OnvifProfileItemManager OnvifProfileMgr
        {
            get => _onvifProfileMgr;
            set
            {
                if (Equals(value, _onvifProfileMgr))
                {
                    return;
                }

                _onvifProfileMgr = value;
                OnPropertyChanged();
            }
        }

        //[JsonIgnore]
        //public MediaClient MediaController
        //{
        //    get => _mediaController;
        //    set
        //    {
        //        if (Equals(value, _mediaController))
        //        {
        //            return;
        //        }

        //        _mediaController = value;
        //        OnPropertyChanged();
        //    }
        //}

        //[JsonIgnore]
        //public ObservableCollection<BaseAdapterCameraProfileItemModel> OnvifProfiles
        //{
        //    get => _onvifProfiles;
        //    set
        //    {
        //        if (Equals(value, _onvifProfiles))
        //        {
        //            return;
        //        }

        //        _onvifProfiles = value;
        //        OnPropertyChanged();
        //    }
        //}

        [JsonIgnore]
        public RelayCommand OpenVideoArchiveCommand =>
            _openVideoArchive ??= new RelayCommand(o => OpenVideoArchive(o));

        [JsonIgnore]
        public virtual NodeItemModel Parent
        {
            get => _parent ??= GetParent(ZoneId).Result;
            set
            {
                if (_parent != value)
                {
                    _parent = value;
                    OnPropertyChanged();
                    CheckDirty();
                }
            }
        }

        [JsonIgnore]
        public PermissionItemModel Permissions
        {
            get => _permissions;
            set
            {
                if (Equals(value, _permissions))
                {
                    return;
                }

                _permissions = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public PresetItemManager PresetItemManager
        {
            get => _presetItemManager;
            private set
            {
                if (Equals(value, _presetItemManager))
                {
                    return;
                }

                _presetItemManager = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<CameraProfilePropertyItemModel> ProfileProperties { get; set; } =
            new ObservableCollection<CameraProfilePropertyItemModel>();

        
        [JsonIgnore] public CollectionViewSource RemoteRecordingCollectionViewSource { get; set; }


        [JsonIgnore] public CollectionViewSource RecordingCollectionViewSource { get; set; }

        public int RecordingCollectionViewSourceCount => 0;

        [JsonIgnore]
        public RecordingProfileItemManager RecordingProfileItemMgr
        {
            get => _recordingProfileItemMgr;
            set
            {
                if (Equals(value, _recordingProfileItemMgr))
                {
                    return;
                }

                _recordingProfileItemMgr = value;
                OnPropertyChanged();
            }
        }

        //[JsonIgnore]
        //public ObservableCollection<RecordingProfileItemModel> RecordingProfiles
        //{
        //    get => _recordingProfiles;
        //    set
        //    {
        //        if (_recordingProfiles != value)
        //        {
        //            _recordingProfiles = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        [JsonIgnore]
        public RelayCommand RefreshRecordingProfilesCommand =>
            _refreshRecordingProfilesCommand ??=
                new RelayCommand(RefreshRecordingProfiles, CanRefreshRecordingProfiles);

        [JsonIgnore]
        public object SelectedEditRecordingProfile
        {
            get => _selectedEditRecordingProfile;
            set
            {
                //if (Equals(value, _selectedEditRecordingProfile)) return;

                _selectedEditRecordingProfile = value;

                OnPropertyChanged();
                Task.Run(async () =>
                {
                    if (_selectedEditRecordingProfile is KeyValuePair<Guid, RecordingProfileItemModel> model)
                    {
                        try
                        {
                            model.Value.IsSelected = true;
                            model.Value.FindMatchingOnvifProfile();
                            Dispatcher.CurrentDispatcher.InvokeAsync(async () =>
                            {
                                model.Value?.OnvifProfileCollectionViewSource?.View?.Refresh();
                            },DispatcherPriority.Background);
                        }
                        catch (Exception e)
                        {
                        }

                        //  model.Value?.OnvifProfileCollectionViewSource?.View?.Refresh();
                    }
                });
            }
        }

        //public ObservableCollection<PtzPreset> Presets
        //{
        //    get => _presets;
        //    set
        //    {
        //        if (Equals(value, _presets)) return;
        //        _presets = value;
        //        OnPropertyChanged();
        //    }
        //}

        public PtzPreset SelectedPreset
        {
            get => _selectedPreset;
            set
            {
                if (Equals(value, _selectedPreset))
                {
                    return;
                }

                _selectedPreset = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore] public ServerItemManager ServerItemMgr => ServerItemManager.Manager;

        [JsonIgnore]
        public StreamProfileItemManager StreamProfileItemMgr
        {
            get => _streamProfileItemMgr;
            set
            {
                if (Equals(value, _streamProfileItemMgr))
                {
                    return;
                }

                _streamProfileItemMgr = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public RelayCommand UpdateCameraCommand => _updateCameraCommand ??= new RelayCommand(p => UpdateCamera(p),
            o => IsEditable && IsDirty && !IsCameraUpdating);

        [JsonIgnore]
        public ICommand ViewRecordingProfileCommand
        {
            get
            {
                if (_viewRecordingProfileCommand == null)
                {
                    _viewRecordingProfileCommand =
                        new RelayCommand(p => ViewRecordingProfile(), p => CanViewRecordingProfile());
                }

                return _viewRecordingProfileCommand;
            }
        }

        [JsonIgnore] private NodeItemModel OriginalParent { get; set; }

        public CameraItemModel()
        {
            PresetItemManager = PresetItemManager.Manager;
            RecordingProfileItemMgr = RecordingProfileItemManager.Manager;
            OnvifProfileMgr = OnvifProfileItemManager.Manager;
            CameraItemMgr = CameraItemModelManager.Manager;
            StreamProfileItemMgr = StreamProfileItemManager.Manager;

            try
            {
                if (Id != Guid.Empty && !CameraItemMgr.ItemCollection.Any(o => o.Key == Id && !string.IsNullOrEmpty(o.Value.Address)))
                {
                    CameraItemMgr.Update(new KeyValuePair<Guid, CameraItemModel>(Id, this));
                }
            }
            catch (Exception e)
            {
                
            }

            //_ = Task.Run(async () =>
            //{
            //    await Application.Current.Dispatcher.InvokeAsync(() =>
            //    {
            //        if (StreamProfileItemMgr != null)
            //        {
            //            LiveStreamProfileCollectionViewSource = new CollectionViewSource
            //            {
            //                Source = StreamProfileItemMgr.ItemCollection
            //            };
            //            LiveStreamProfileCollectionViewSource.Filter += LiveStreamProfileCollectionViewSourceOnFilter;
            //            RemoteLiveStreamProfileCollectionViewSource = new CollectionViewSource
            //            {
            //                Source = StreamProfileItemMgr.ItemCollection
            //            };
            //            RemoteLiveStreamProfileCollectionViewSource.Filter += RemoteLiveStreamProfileCollectionViewSourceOnFilter;
            //        }

            //        if (OnvifProfileMgr != null)
            //        {
            //            OnvifProfileCollectionViewSource = new CollectionViewSource
            //            {
            //                Source = OnvifProfileMgr.ItemCollection
            //            };
            //            OnvifProfileCollectionViewSource.Filter += OnvifProfileCollectionViewSourceOnFilter;
            //        }

            //        if (RecordingProfileItemMgr != null)
            //        {
            //            RecordingCollectionViewSource = new CollectionViewSource
            //            {
            //                Source = RecordingProfileItemMgr.ItemCollection
            //            };
            //            RecordingCollectionViewSource.Filter += RecordingCollectionViewSourceOnFilter;

            //            RemoteRecordingCollectionViewSource = new CollectionViewSource
            //            {
            //                Source = RecordingProfileItemMgr.ItemCollection
            //            };
            //            RemoteRecordingCollectionViewSource.Filter += RemoteRecordingCollectionViewSourceOnFilter;
            //        }
            //    }, DispatcherPriority.Background);
            //}).ConfigureAwait(false);
        }

        private void RemoteLiveStreamProfileCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is KeyValuePair<Tuple<Guid, Guid>, StreamProfileItemModel> model)
            {
                var notFiltered = model.Key.Item2 == Id;
                if (notFiltered)
                {
                    notFiltered = StreamProfileItemManager.FilterRemoteStream(ZoneId,model);
                }
                
                e.Accepted = notFiltered;
            }
        }

        private void RemoteRecordingCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is KeyValuePair<Guid, RecordingProfileItemModel> model)
            {
                var notFiltered = model.Value.CameraId == Id;
                if (notFiltered)
                {
                    notFiltered = RecordingProfileItemManager.FilterRemoteStream(ZoneId, model);
                }
                e.Accepted = notFiltered;
            }
        }

        [JsonIgnore]
        public bool IsActive
        {
            get => _isActive;
            set
            {
                if (value == _isActive)
                {
                    return;
                }

                _isActive = value;
                OnPropertyChanged();
                RecordingCollectionViewSource.View.Refresh();
                IsActiveChanged?.Invoke(null, null);
            }
        }

        public event EventHandler IsActiveChanged;

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(Address)}: {Address}, {nameof(Label)}: {Label}";
        }

        private async void ConfigureHardware(object o)
        {
            try
            {
                await Task.Run(async () =>
                {
                    if (o is ECameraHardwareConfigure command)
                    {
                        var deviceClient = await TryGetDeviceClient();
                        var findFocusedTab = TabUtil.FindFocusedTab<ManagementTab>();
                        var modalContentPresenter = findFocusedTab.ManagementView.DialogOverlayGrid
                            .ModalContentPresenter;
                        switch (command)
                        {
                            case ECameraHardwareConfigure.RebootCamera:
                                try
                                {
                                    _ = this.TryUiAsync(async() =>
                                    {
                                        var dialog = await DialogUtil.EmbeddedModalConfirmDialog(
                                            modalContentPresenter, new EmbeddedDialogParameters
                                            {
                                                DialogButtons = EDialogButtons.YesNo,
                                                Header = "Reboot Camera",
                                                Description = $"Are you sure you wish to 'Reboot' the Camera?\n{Label}",
                                                //GlyphFont = Application.Current.TryFindResource("FontAwe")
                                            });
                                        if (dialog)
                                        {
                                            var resp = await GetManagerByZoneId(ZoneId).CameraAdapterService
                                                .GetCameraDeviceAction(
                                                    new GetCameraDeviceAction
                                                    {
                                                        CameraId = Id,
                                                        HardwareAction = ECameraHardwareConfigure.RebootCamera,
                                                    });

                                            //  await deviceClient.SystemRebootAsync();
                                        }
                                    }).ConfigureAwait(true);
                                }
                                catch (Exception e)
                                {
                                }

                                break;
                            case ECameraHardwareConfigure.SystemRestore:
                                try
                                {
                                    var dialog = await DialogUtil.EmbeddedModalConfirmDialog(
                                        modalContentPresenter, new EmbeddedDialogParameters
                                        {
                                            DialogButtons = EDialogButtons.YesNo,
                                            Header = "Camera System Restore",
                                            Description = $"Are you sure you wish to 'System Restore' the Camera?\n{Label}",
                                            //GlyphFont = Application.Current.TryFindResource("FontAwe")
                                        });
                                    if (dialog)
                                    {
                                        var resp = await GetManagerByZoneId(ZoneId).CameraAdapterService
                                            .GetCameraDeviceAction(
                                                new GetCameraDeviceAction
                                                {
                                                    CameraId = Id,
                                                    HardwareAction = ECameraHardwareConfigure.SystemRestore,
                                                });
                                        //await deviceClient.SystemRebootAsync();
                                    }

                                    await deviceClient.StartSystemRestoreAsync(new StartSystemRestoreRequest());
                                }
                                catch (Exception e)
                                {
                                }

                                break;
                            case ECameraHardwareConfigure.UpdateFirmware:
                                try
                                {
                                    var fileDialog = new RadOpenFileDialog();
                                    if (fileDialog.ShowDialog() == true)
                                    {
                                        var fileSelected = fileDialog.FileName.IsNotEmpty();
                                        if (fileSelected)
                                        {
                                            var resp = await GetManagerByZoneId(ZoneId).CameraAdapterService
                                                .GetCameraDeviceAction(
                                                    new GetCameraDeviceAction
                                                    {
                                                        CameraId = Id,
                                                        HardwareAction = ECameraHardwareConfigure.UpdateFirmware,
                                                        FileBytes = File.ReadAllBytes(fileDialog.FileName)
                                                    });

                                            // await deviceClient.UpgradeSystemFirmwareAsync(new AttachmentData(){})

                                            //await deviceClient.StartFirmwareUpgradeAsync(new StartFirmwareUpgradeRequest()
                                            //{

                                            //});
                                        }
                                    }

                                    //
                                }
                                catch (Exception e)
                                {
                                }

                                break;
                            case ECameraHardwareConfigure.UpdateInformation:

                                if (!IsLoadingCameraDetails)
                                {
                                    try
                                    {
                                        IsLoadingCameraDetails = true;
                                        var cameraDetails = await CameraItemMgr.GetCameraDetails(ZoneId, Id,
                                            new ItemManagerRequestParameters {AcceptedCacheDuration = TimeSpan.Zero});
                                        if (cameraDetails != null)
                                        {
                                            DeviceInformation = cameraDetails.CameraDetails;
                                        }

                                        await GetCameraTime();
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e);
                                    }
                                    finally
                                    {
                                        IsLoadingCameraDetails = false;
                                    }
                                }

                                break;
                            case ECameraHardwareConfigure.SetCameraTime:
                                var setTime = await GetManagerByZoneId(ZoneId).CameraAdapterService.GetCameraDeviceAction(
                                    new GetCameraDeviceAction
                                    {
                                        CameraId = Id,
                                        HardwareAction = ECameraHardwareConfigure.SetCameraTime,
                                    });
                                break;
                            case ECameraHardwareConfigure.GetCameraTime:
                                await GetCameraTime();

                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
            }
        }

        private async Task GetCameraTime()
        {
            var time = await GetManagerByZoneId(ZoneId).CameraAdapterService.GetCameraDeviceAction(
                new GetCameraDeviceAction
                {
                    CameraId = Id,
                    HardwareAction = ECameraHardwareConfigure.GetCameraTime,
                });
            if (time != null)
            {
                //var dateTimeOffset =  ChangeUtcToLocal(ChangeLocalToUtc(DateTimeOffset.Parse(time.Response)).ToUniversalTime()); 
                //var dateTimeOffset1 = ChangeUtcToLocal(ChangeLocalToUtc(DateTimeOffset.Parse(time.Response).ToLocalTime())); 

                //var dateTimeOffset2 =  ChangeUtcToLocal(ChangeLocalToUtc(DateTimeOffset.Parse(time.Response).ToLocalTime()).ToUniversalTime()); 

                //var dateTimeOffset3 =  ChangeUtcToLocal(ChangeLocalToUtc(DateTimeOffset.Parse(time.Response).ToUniversalTime()).ToLocalTime()); 
                //var dateTimeOffset4 =  ChangeUtcToLocal(ChangeLocalToUtc(DateTimeOffset.Parse(time.Response)).ToLocalTime()); 
                //var dateTimeOffset5 =  ChangeLocalToUtc(DateTimeOffset.Parse(time.Response)); 
                //var dateTimeOffset6 =  ChangeLocalToUtc(DateTimeOffset.Parse(time.Response).ToLocalTime()); 
                //var dateTimeOffset7 =  ChangeLocalToUtc(DateTimeOffset.Parse(time.Response).ToUniversalTime()); 
                //var dateTimeOffset8 =  ChangeLocalToUtc(DateTimeOffset.Parse(time.Response).ToLocalTime()).ToUniversalTime(); 
                //var dateTimeOffset9 =  ChangeLocalToUtc(DateTimeOffset.Parse(time.Response).ToUniversalTime()).ToUniversalTime(); 
                //var convert = TimeZoneInfo.ConvertTime( DateTimeOffset.Parse(time.Response), TimeZoneInfo.Utc);
                var convertTime = TimeZoneInfo.ConvertTime(DateTimeOffset.Parse(time.Response), TimeZoneInfo.Local);
                var convert2 = convertTime.Offset;
                var dateTime = convertTime.Add(convert2);
                CameraTime = dateTime;
            }
        }

        private static DateTimeOffset ChangeUtcToLocal(DateTimeOffset original)
        {
            var cetInfo = TimeZoneInfo.Utc; //TimeZoneInfo.FindSystemTimeZoneById("UTC");
            var cetTime = TimeZoneInfo.ConvertTime(original, cetInfo);
            return original
                //  .Subtract(cetTime.Offset)
                .ToOffset(cetTime.Offset);
        }

        private static DateTimeOffset ChangeLocalToUtc(DateTimeOffset original)
        {
            var cetInfo = TimeZoneInfo.Local; //TimeZoneInfo.FindSystemTimeZoneById("UTC");
            var cetTime = TimeZoneInfo.ConvertTime(original, cetInfo);
            return original
                //.Subtract(cetTime.Offset)
                .ToOffset(cetTime.Offset);
        }

        private async Task<DeviceClient> TryGetDeviceClient()
        {
            try
            {
                var deviceClient = DeviceClient ??=
                    await OnvifClientFactory.CreateDeviceClientAsync(Address, Username, Password);
                return deviceClient;
            }
            catch (Exception e)
            {
            }

            return default;
        }

        //public IEnumerable<RecordingProfileItemModel> RecordingProfiles =>
        //    RecordingProfileItemMgr.ItemCollection.Values.Where(o => o.CameraId == Id);

        private async void OpenVideoArchive(object o)
        {
            try
            {
                if (o is UIElement element)
                {
                    var recordingProfileItemModel =
                        RecordingProfileItemMgr.ItemCollection.Values?.FirstOrDefault(o => o.CameraId == Id);
                    if (recordingProfileItemModel != null)
                    {
                       await element.ExportVideo(Id, recordingProfileItemModel.Id, DateTime.Now,
                            TimeSpan.FromSeconds(90), TimeSpan.FromMinutes(15));
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        public static string AddAuthUri(string username, string password, string url, string matchString = "rtsp://")
        {
            if (url.IsNotEmpty() && url.Contains(
                matchString) && !url.Contains(
                $"{matchString}{username}:{password}@"))
            {
                url = url.Replace($"{matchString}",
                    $"{matchString}{username}:{password}@");
            }

            return url;
        }

        private bool CanConfigure()
        {
            return EffectivePermissions.HasRoleAny(
                ERoles.Administrator | ERoles.CameraAdministrator);
        }

        private async void Configure(object owner = null)
        {
            await Dispatcher.CurrentDispatcher.InvokeAsync(async () =>
            {
                if (owner == null)
                {
                    owner = ObjectResolver.ResolveAll<MonitoringTab>().FirstOrDefault();
                }

                if (owner is FrameworkElement element)
                {
                    var managementView =
                        (await element.GetParentRadWindow<RadTabbedWindow>().FindTab<ManagementTab>(true))?.ManagementView ?? (await 
                            element.GetParentRadWindow<RadTabbedWindow>().OpenTab<ManagementTab>())?.ManagementView;

                    if (managementView != null)
                    {
                        try
                        {
                            managementView.HasLoadedItem = true;
                            var radPanelBarItem = managementView.CamerasPanelItem.GetVisualParent<RadPanelBarItem>();
                            radPanelBarItem.IsSelected = true;
                            managementView.Model.SelectedPanelItem = managementView.CamerasPanelItem;
                            managementView.RadPanelBar.SelectedItem = managementView.CamerasPanelItem;
                        }
                        catch (Exception e)
                        {
                            _log?.Info(e);
                        }

                        managementView.ZoneCameraManagementView.BringIntoView();
                        await Task.Delay(45);
                        try
                        {
                            // var zones = managementView?.ZoneCameraManagementView?.Model?.ZoneItemMgr.ItemCollection.Values;
                            var server =
                                ServerItemMgr.GetServersByZoneId(ZoneId)
                                    .FirstOrDefault();
                            if (server == null)
                            {
                                await ServerItemMgr.RefreshServers(ZoneId);
                                //await ServerItemManager.GetManagerByZoneIdConnectIfFail(ZoneId);
                                server =
                                    ServerItemMgr.GetServersByZoneId(ZoneId)
                                        .FirstOrDefault();
                            }

                            // zones?.FirstOrDefault(o => o.ZoneId == ZoneId)?.Servers.FirstOrDefault();
                            if (server != null && managementView.ZoneCameraManagementView.Model != null)
                            {
                                try
                                {
                                    using (var serverCts = new CancellationTokenSource(5000)) // serverCts.CancelAfter(5000);
                                    {
                                        var _ = Task.Run(async () =>
                                        {
                                            while (server?.Archiver == null || server?.Archiver?.ActiveCameras?.Count == 0)
                                            {
                                                server =
                                                    ServerItemMgr.GetServersByZoneId(ZoneId)
                                                        .FirstOrDefault();
                                                await Task.Delay(100);
                                            }
                                        }, serverCts.Token).ConfigureAwait(false);
                                    }
                                }
                                catch (Exception e)
                                {
                                }

                                managementView.ZoneCameraManagementView.Model.SelectedServer = server;

                                await Task.Delay(100);

                                var archiverActiveCameras = server?.Archiver?.ActiveCameras;
                                if (managementView.ZoneCameraManagementView.Model.SelectedServer != null)
                                {
                                    await Task.Delay(100);
                                    managementView.ZoneCameraManagementView.Model.SelectedZone.SelectedZoneItem =
                                        CameraItemMgr.ItemCollection.FirstOrDefault(o => o.Key == Id);
                                    //archiverActiveCameras?
                                    //    .FirstOrDefault(o => o.ObjectId == Id);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                }
            });     
        }

        public override void Initialize()
        {
            base.Initialize();
            try
            {
                //GetRecordingProfiles();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private Task<NodeItemModel> GetParent(Guid? zoneId)
        {
            // _log?.Info($"Get Parent Requested... {Label}");
            return Task.Run(async () =>
            {
                try
                {
                    OriginalParent = await ResolvedParent(zoneId);
                    return OriginalParent;
                }
                catch (Exception)
                {
                    return default;
                }
            });
        }

        private void CheckDirty()
        {
            IsDirty = true;
            return;
            if (!IsEditable)
            {
                IsDirty = false;
            }
            else
            {
                IsDirty = Parent != null && Parent.ObjectId != ParentId ||
                          Descriptor != null && (Label != Descriptor?.Label || Address != Descriptor.Address ||
                                                 LiveStreamUrl != Descriptor.LiveStreamUrl ||
                                                 Username != Descriptor.Username || Password != Descriptor.Password);
            }
        }

        private async Task<NodeItemModel> ResolvedParent(Guid? zoneId)
        {
            try
            {
                if (ObjectId != null)
                {
                    if (zoneId != null && zoneId != Guid.Empty)
                    {
                        var zone = zoneId.Value;

                        var resp = await NodeObjectItemManager.NodeManager.RefreshNode(ObjectId.Value,
                                new ItemManagerRequestParameters {AcceptedCacheDuration = TimeSpan.FromSeconds(3)});

                        if (resp?.ParentId != null)
                        {
                            var parentResp = await NodeObjectItemManager.NodeManager
                                .RefreshNode(resp.ParentId.Value,
                                    new ItemManagerRequestParameters {AcceptedCacheDuration = TimeSpan.FromSeconds(3)});
                            if (parentResp != null && parentResp.ParentId != null)
                            {
                                ParentId = parentResp.Descriptor.ParentId;
                                return new NodeItemModel(parentResp.Descriptor);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            return default;
        }


        private async void GetCameraInformation(object obj = null)
        {
            if (!IsGettingCameraInformation)
            {
                Console.WriteLine("IsGettingCameraInformation...");
                IsGettingCameraInformation = true;
                try
                {
                    if (IsDirty)
                    {
                        UpdateCamera(obj);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                try
                {
                    using (var cts = new CancellationTokenSource(12000))
                    {
                        var _ = await Task.Run(async () =>
                        {
                            while (Id == Guid.Empty)
                            {
                                await Task.Delay(500);
                            }

                            if (ObjectId != null)
                            {
                                var resp = await OnvifProfileMgr.GetProfile(this);
                                if (resp != null)
                                {
                                    var cameraInfo =
                                        JsonConvert.DeserializeObject<CameraInformation>(resp.SerializedCameraInfo,
                                            JsonUtil.IgnoreExceptionSettings());
                                    if (cameraInfo != null)
                                    {
                                        var cameraProfile = cameraInfo.Profile.FirstOrDefault();
                                        var cameraProfileStreamUri = cameraProfile?.StreamUri;
                                        if (EffectivePermissions.HasRoleAny(
                                            ERoles.Administrator | ERoles.CameraAdministrator))
                                        {
                                            if (LiveStreamUrl != cameraProfileStreamUri)
                                            {
                                                LiveStreamUrl = cameraProfileStreamUri;
                                                //UpdateCamera(force: true);
                                            }
                                        }

                                        if (cameraProfileStreamUri.IsNotEmpty())
                                        {
                                            CameraUrl = cameraProfileStreamUri;
                                            return true;
                                        }
                                    }
                                }
                            }

                            return false;
                        },cts.Token).ConfigureAwait(false);
                    }
                }
                catch (Exception)
                {
                }

                IsGettingCameraInformation = false;
            }
        }

        public async void UpdateCamera(object obj = null, bool force = false)
        {
            IsCameraUpdating = true;
            if (IsDirty || force)
            {
                try
                {
                    if (force)
                    {
                        var desc = this.ConvertTo<UpdateCamera>();
                        desc.Type = Type;
                        desc.ParentId = Parent.ObjectId;

                        var resp = await GetManagerByZoneId(ZoneId).CameraService.UpdateCamera(desc);
                        if (resp != null)
                        {
                            Descriptor = desc.ConvertTo<CameraDescriptor>();
                            // ParentId = Parent.ObjectId;
                        }
                    }
                    else
                    {
                        if (Parent == null)
                        {
                            var parent = await ResolvedParent(ZoneId);
                        }

                        if (Parent != null && Parent.ObjectId != null && Parent.ObjectId != Guid.Empty)
                        {
                            var node = await NodeObjectItemManager.NodeManager.RefreshNode(Parent.ObjectId.Value,
                                new ItemManagerRequestParameters());
                            if (node != null)
                            {
                                //UpdateCameraLabel();

                                var desc = this.ConvertTo<UpdateCamera>();
                                desc.Type = Type;
                                desc.ParentId = Parent.ObjectId;

                                var resp = await GetManagerByZoneId(ZoneId).CameraService.UpdateCamera(desc);
                                if (resp != null)
                                {
                                    OriginalParent = Parent;
                                    Descriptor = desc.ConvertTo<CameraDescriptor>();
                                    ParentId = Parent.ObjectId;
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                }
            }


            CheckDirty();
            IsEditable = false;
            IsCameraUpdating = false;
        }

        public static implicit operator CameraItemModel(
            Raven.Vms.AuthorityService.ServiceModel.Api.Cameras.Camera config)
        {
            try
            {
                if (config != null)
                {
                    var cameraItem = config.ConvertTo<CameraItemModel>();

                    cameraItem.Id = config.CameraId;
                    cameraItem.ObjectId = config.CameraId;
                    cameraItem.Label = config.CameraName;

                    // cameraItem.Type = config.Type;
                    cameraItem.Descriptor = config.ConvertTo<CameraDescriptor>();
                    // cameraItem.ParentId = config.;

                    return cameraItem;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            // Task.Run(async () => { await cameraItem.GetRecordingProfiles(); });
            return default;
        }


        public static implicit operator CameraItemModel(NodeDescriptor config)
        {
            try
            {
                var cameraItem = config.ConvertTo<CameraItemModel>();
                // cameraItem.Type = config.Type;
                cameraItem.Descriptor = config.ConvertTo<CameraDescriptor>();
                cameraItem.ParentId = config.ParentId;

                return cameraItem;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            // Task.Run(async () => { await cameraItem.GetRecordingProfiles(); });
            return default;
        }


        public static implicit operator CameraItemModel(CameraDescriptor config)
        {
            if (config == null)
            {
                return null;
            }

            var cameraItem = config.ConvertTo<CameraItemModel>();
            cameraItem.Descriptor = config;
            cameraItem.InitPropertyFlags(config, cameraItem);
            //cameraItem.ParentId = config.ParentId;
            // Task.Run(async () => { await cameraItem.GetRecordingProfiles(); });
            return cameraItem;
        }

        private void InitPropertyFlags(CameraDescriptor config, CameraItemModel cameraItem)
        {
            try
            {
                foreach (var e in Enum.GetValues(typeof(ECameraProperty)).OfType<ECameraProperty>())
                {
                    try
                    {
                        if (e == ECameraProperty.None)
                        {
                            continue;
                        }

                        var cameraProfilePropertyItemModel = new CameraProfilePropertyItemModel
                            {Flag = e, IsChecked = ((ECameraProperty) config.PropertyFlags).HasFlag(e)};
                        cameraProfilePropertyItemModel.FlagChanged += CameraProfilePropertyItemModelOnFlagChanged;
                        cameraItem.ProfileProperties.Add(cameraProfilePropertyItemModel);
                    }
                    catch (Exception exception)
                    {
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private async void RefreshRecordingProfiles(object obj)
        {
            await GetRecordingProfiles(true);
        }

        private bool CanRefreshRecordingProfiles(object obj)
        {
            return !IsRefreshingProfiles;
        }

        private void ViewRecordingProfile()
        {
            try
            {
                if (Id != null)
                {
                    var foundProfile =
                        RecordingProfileItemMgr.ItemCollection.Values.FirstOrDefault(o => o.CameraId == Id);
                    if (foundProfile != null)
                    {
                        //var window = WindowUtil.ShowWindow<CameraProfileManagementWindow>();
                        //window.CameraProfileManagementView.GetModel().RecordingProfile =
                        //    foundProfile;
                    }

                    //var resp = await GetRecordingProfiles(Id.Value);
                    //if (resp != null)
                    //{
                    //    var foundProfile = resp?.Recordings?.FirstOrDefault(r => r.CameraId == ObjectId);
                    //    if (foundProfile != null)
                    //    {
                    //        var window = WindowUtil.ShowWindow<CameraProfileManagementWindow>();
                    //        window.CameraProfileManagementView.GetModel().RecordingProfile =
                    //            foundProfile.ConvertTo<RecordingProfileItemModel>();
                    //    }
                    //}
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        private bool CanAddRecordingProfile()
        {
            return true;
        }

        private bool CanViewRecordingProfile()
        {
            return true;
        }

        public async Task GetRecordingProfiles(bool uncached = false)
        {
            if (!IsRefreshingProfiles)
            {
                IsRefreshingProfiles = true;
                try
                {
                    using (var cts = new CancellationTokenSource(12000))
                    {
                        await Task.Run(async () =>
                        {
                            var sw = new Stopwatch();
                            sw.Start();
                            while (ZoneId == null || ZoneId == Guid.Empty || Label.IsEmpty())
                            {
                                await Task.Delay(200);
                            }

                            sw.Stop();
                            // _log?.Info($"Resolving CameraItemModel Zone and Label took {sw.ElapsedMilliseconds}ms!");
                            if (ZoneId != null && ZoneId != Guid.Empty)
                            {
                                var resp = await RecordingProfileItemManager.Manager.RefreshProfileByCameraId(ZoneId, Id, uncached ? new ItemManagerRequestParameters(){AcceptedCacheDuration = TimeSpan.Zero} : null);
                                //var profiles = await GetManagerByZoneId(ZoneId).RecordingProfileService
                                //    .GetRecordingProfileByCameraId
                                // (Id);
                                if (resp != null )
                                {
                                    Dispatcher.CurrentDispatcher.InvokeAsync(async () =>
                                    {
                                        RecordingCollectionViewSource?.View?.Refresh();
                                    });
                                    //TODO: FIX ME
                                    //await Application.Current.Dispatcher.InvokeAsync(() =>
                                    //{
                                    //    RecordingProfiles.Clear();
                                    //    var recordings = profiles.RecordingProfiles
                                    //        .Select(o => (RecordingProfileItemModel)o)
                                    //        .OrderByDescending(o => o.Width + o.Height).ToList();
                                    //    foreach (var r in recordings)
                                    //    {
                                    //        r.Owner = this;
                                    //    }

                                    //    RecordingProfiles.AddRange(recordings);
                                    //});
                                    // OnPropertyChanged(nameof(RecordingProfiles));
                                    //RecordingProfiles =
                                    //    new ObservableCollection<RecordingProfileItemModel>(recordings
                                    //    );
                                }
                            }

                            //_log?.Info($"RecordingProfiles... {Label} - {RecordingProfiles.Count}");
                        },cts.Token);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"GetRecordingProfiles {e}");
                }


                IsRefreshingProfiles = false;
            }
        }

        private async void AddRecordingProfile()
        {
            var recordingProfileItemModels =
                RecordingProfileItemMgr?.ItemCollection?.Values?.Where(o => o.CameraId == Id)?.Count() ?? 0;
            var profileLabel = $"Profile{recordingProfileItemModels + 1}";
            var dialog =
                await DialogUtil.EmbeddedModalConfirmDialog(
                    TabUtil.FindFocusedTab<ManagementTab>().ManagementView.DialogOverlayGrid.ModalContentPresenter,
                    new EmbeddedDialogParameters
                    {
                        DialogButtons = EDialogButtons.OkCancel,
                        Description = $"Do you wish to add a recording profile?\nCamera: {Label}",
                    }
                );
            if (dialog)
            {
                try
                {
                    if (Id != null)
                    {
                        if (ZoneId != null)
                        {
                            var resp = await GetManagerByZoneId(ZoneId).RecordingProfileService
                                .CreateRecordingProfile(
                                    new CreateRecordingProfile
                                    {
                                        Label = $"{profileLabel}",
                                        CameraId = Id,
                                        Width = 1280,
                                        Height = 720,
                                        RtspAddress = CameraUrl,
                                        PropertyFlags = 0,
                                        //Format = "flv",
                                        //Type = "Camera"
                                    });
                            if (resp != null)
                            {
                                RefreshRecordingProfilesCommand?.Execute(null);
                            }
                        }
                    }

                    CameraDeleted?.Invoke();
                }
                catch (Exception e)
                {
                }
            }
        }

        public event Action CameraDeleted;


        private bool CanDelete()
        {
            return true;
        }

        public async void DeleteCamera()
        {
            try
            {
                if (DialogUtil.ConfirmDialog($"Do you wish to delete the camera?\n{Label}", true))
                {
                    if (ZoneId != null && ZoneId != Guid.Empty)
                    {
                        if (ServiceManagers.ContainsKey(ZoneId))
                        {
                            var resp = await GetManagerByZoneId(ZoneId).CameraService.DeleteCamera(this);
                            if (resp != null)
                            {
                                _log?.Info($"{resp}");
                                CameraItemMgr.ItemCollection.Remove(Id);
                            }
                        }
                    }
                    else
                    {
                        if (Label.Contains("(Offline)"))
                        {
                            await NodeObjectItemManager.NodeManager
                                .RemoveNode(false, Id);
                        }
                    }

                    CameraDeleted?.Invoke();
                }
            }
            catch (Exception e)
            {
            }
        }

        public async void UpdateCameraLabel()
        {
            if (ZoneId != null)
            {
                var resp = await GetManagerByZoneId(ZoneId).CameraService
                    .UpdateCamera(this.ConvertTo<UpdateCamera>());
                if (resp != null)
                {
                    _log?.Info($"{resp.Message} {resp.Id}");
                }
            }
        }

        private void CameraProfilePropertyItemModelOnFlagChanged(ECameraProperty flag, bool changed)
        {
            var existingFlag = (ECameraProperty) PropertyFlags;
            if (changed)
            {
                existingFlag |= flag;
            }
            else
            {
                existingFlag ^= flag;
            }

            PropertyFlags = (int) existingFlag;
        }

        private void LiveStreamProfileCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is KeyValuePair<Tuple<Guid, Guid>, StreamProfileItemModel> model)
            {
                e.Accepted = model.Key.Item2 == Id;
            }
        }

        private void OnvifProfileCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is KeyValuePair<Tuple<Guid, string>, BaseAdapterCameraProfileItemModel> model)
            {
                e.Accepted = model.Key.Item1 == Id;
            }
        }

        private void RecordingCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is KeyValuePair<Guid, RecordingProfileItemModel> model)
            {
                e.Accepted = model.Value.CameraId == Id;
                //  OnPropertyChanged(nameof(RecordingCollectionViewSourceCount));
            }
        }
    }

    public class CameraProfilePropertyItemModel : INotifyPropertyChanged
    {
        private ECameraProperty _flag;
        private bool _isChecked;

        public ECameraProperty Flag
        {
            get => _flag;
            set
            {
                if (value == _flag)
                {
                    return;
                }

                _flag = value;
                OnPropertyChanged();
            }
        }

        public bool IsChecked
        {
            get => _isChecked;
            set
            {
                if (value == _isChecked)
                {
                    return;
                }

                _isChecked = value;
                FlagChanged?.Invoke(Flag, value);
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public event Action<ECameraProperty, bool> FlagChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    //internal enum ECameraHardwareConfigure
    //{
    //    RebootCamera,
    //    SystemRestore,
    //    UpdateFirmware,

    //    UpdateInformation
    //}
}