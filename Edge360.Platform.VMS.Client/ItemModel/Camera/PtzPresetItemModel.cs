﻿using System;
using Edge360.Raven.Vms.CameraService.ServiceModel.ItemModels;

namespace Edge360.Platform.VMS.Client.ItemModel.Camera
{
    public class PtzPresetItemModel : PtzPreset
    {
        public bool IsBlank { get; set; }
        public DateTimeOffset CachedTime { get; set; }
    }
}