﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Prism.Commands;

namespace Edge360.Platform.VMS.Client.ItemModel.Camera.Discovery
{
    public class CameraDiscoveryItemModel : INotifyPropertyChanged
    {

        private string _address;

        private sealed class AddressNameModelEqualityComparer : IEqualityComparer<CameraDiscoveryItemModel>
        {
            public bool Equals(CameraDiscoveryItemModel x, CameraDiscoveryItemModel y)
            {
                if (ReferenceEquals(x, y))
                {
                    return true;
                }

                if (ReferenceEquals(x, null))
                {
                    return false;
                }

                if (ReferenceEquals(y, null))
                {
                    return false;
                }

                if (x.GetType() != y.GetType())
                {
                    return false;
                }

                return x._address == y._address && x._name == y._name && x._model == y._model;
            }

            public int GetHashCode(CameraDiscoveryItemModel obj)
            {
                unchecked
                {
                    var hashCode = (obj._address != null ? obj._address.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (obj._name != null ? obj._name.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (obj._model != null ? obj._model.GetHashCode() : 0);
                    return hashCode;
                }
            }
        }

        public static IEqualityComparer<CameraDiscoveryItemModel> AddressNameModelComparer { get; } = new AddressNameModelEqualityComparer();

        private string _name;
        private string _model;
        private DelegateCommand _openInWeb;
        private string _location;

        public string Model
        {
            get => _model;
            set
            {
                if (value == _model) return;
                _model = value;
                OnPropertyChanged();
            }
        }

        public override string ToString()
        {
            return $"{nameof(Model)}: {Model}, {nameof(Name)}: {Name}, {nameof(Address)}: {Address}";
        }

        public string Name
        {
            get => _name;
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }

        public string Address
        {
            get => _address;
            set
            {
                if (value == _address) return;
                _address = value;
                
                OnPropertyChanged();
            }
        }

        public string Location
        {
            get => _location;
            set
            {
                if (value == _location) return;
                _location = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}