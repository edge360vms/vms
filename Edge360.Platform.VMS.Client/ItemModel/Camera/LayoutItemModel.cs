﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.DataObject;
using Edge360.Platform.VMS.Client.ItemModel.Layouts;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Cameras;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Interfaces;
using Edge360.Raven.Vms.CameraService.ServiceModel.Enums;
using Edge360.Vms.AuthorityService.SharedModel.Enums;
using Newtonsoft.Json;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.ItemModel.Camera
{
    public class LayoutItemModel : NodeObject, INotifyPropertyChanged, IGridLayout
    {
        private ObservableCollection<Tuple<Size, CameraDescriptor>> _cameraCollection;

        [JsonIgnore] private RelayCommand _deleteLayoutCommand;

        private Size _gridSize;
        private bool _isRemote;
        private PermissionItemModel _permissions = new PermissionItemModel();

        public ObservableCollection<Tuple<Size, CameraDescriptor>> CameraCollection
        {
            get => _cameraCollection;
            set
            {
                if (_cameraCollection != value)
                {
                    _cameraCollection = value;
                    OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public RelayCommand DeleteLayoutCommand => _deleteLayoutCommand ??= new RelayCommand(p => { DeleteLayout(); },
            p => CanDeleteLayout());

        public Size GridSize
        {
            get => _gridSize;
            set
            {
                if (_gridSize != value)
                {
                    _gridSize = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool IsPersonal => ShareType == (int) EGridLayoutShareType.Personal;

        public bool IsShared => ShareType == (int) EGridLayoutShareType.Shared;

        public Guid Id { get; set; }
        public Guid OwnerId { get; set; }
        public string Description { get; set; }
        public string LayoutData { get; set; }
        public int LayoutType { get; set; }
        public int ShareType { get; set; }
        public DateTimeOffset CreatedUtc { get; set; }
        public DateTimeOffset UpdatedUtc { get; set; }

        public PermissionItemModel Permissions
        {
            get => _permissions;
            set
            {
                if (Equals(value, _permissions)) return;
                _permissions = value;
                OnPropertyChanged();
            }
        }

        public async Task<bool> DeleteLayout()
        {
            var dialog = DialogUtil.ConfirmDialog($"Are you sure you wish to delete {Label}?", true);
            if (dialog)
            {
                var existingLayout = LayoutManager.Layouts.Layouts.FirstOrDefault(o => o == this);
                if (existingLayout != null)
                {
                    LayoutManager.Layouts.Layouts.Remove(existingLayout);
                    LayoutManager.SaveLayouts();
                }

                if (Id != null)
                {
                    await GetManagerByZoneId().GridLayoutService.DeleteGridLayout(Id);
                }

                return true;
            }

            return false;
        }

        private bool CanDeleteLayout()
        {
            return EffectivePermissions.HasFlag(ERoles.Administrator | ERoles.LayoutEditor);
        }

        public void InitGridLayout(IGridLayout gridLayout)
        {
            Id = gridLayout.Id;
            OwnerId = gridLayout.OwnerId;
            Description = gridLayout.Description;
            LayoutType = gridLayout.LayoutType;
            LayoutData = gridLayout.LayoutData;
            ShareType = gridLayout.ShareType;
            CreatedUtc = gridLayout.CreatedUtc;
            UpdatedUtc = gridLayout.UpdatedUtc;
        }

        public async void UpdateLabel()
        {
            if (Id != null)
            {
                await GetManagerByZoneId().GridLayoutService.UpdateGridLayout(Label, Id,
                    Description,
                    JsonConvert.SerializeObject(this), LayoutType, ShareType);
            }
        }
    }
}