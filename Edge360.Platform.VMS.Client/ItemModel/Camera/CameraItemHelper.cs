using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using log4net;
using RecordingService.ServiceModel.Api.RecordingProfile;
using ServiceStack;
using Unity;
using WispFramework.Extensions.Tasks;

namespace Edge360.Platform.VMS.Client.ItemModel.Camera
{
    internal static class CameraItemHelper
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static DirectModeManager DirectModeMgr { get; set; }

        static CameraItemHelper()
        {
            DirectModeMgr = App.GlobalUnityContainer.Resolve<DirectModeManager>();
        }

        public static async Task InitCameraProfiles(List<CameraItemModel> cameras)
        {
            try
            {
                _log?.Info($"InitCameraProfiles {cameras?.Count}");

                if (cameras == null || cameras.Count == 0)
                {
                    return;
                }
                //TODO: This should come from the Database... OR LivePlaybackService
                ////GetCameraInformation();
                ///
                //using var cts = new CancellationTokenSource(30000);
                //await Task.Run(async () =>
                //{
                //    await LoadCameraStatus(cameras);
                //    await LoadOnvifProfiles(cameras);
                //    await LoadRecordingProfilesBatch(cameras);
                //    //await GetONVIFProfiles(cameras);
                //    await LoadPresetsBatch(cameras);
                //}, cts.Token);
            }
            catch (Exception e)
            {
                _log?.Info($"CameraItemHelper InitCameraProfiles {e.GetType()} {e.Message}");
            }
        }

        public static async Task LoadCameraStatus(List<CameraItemModel> cameras)
        {
            try
            {
                await Task.Run(async () =>
                {
                    var resp = await CameraItemModelManager.Manager.RequestCameraStatuses(cameras.ToArray());
                    foreach (var c in resp)
                    {
                        if (c.CameraStatuses != null)
                        {
                            foreach (var s in c.CameraStatuses)
                            {
                                try
                                {
                                    var matchingCamera = cameras.FirstOrDefault(o => o.Id == s.CameraId);
                                    if (matchingCamera != null)
                                    {
                                        matchingCamera.CameraStatus = s;
                                    }
                                }
                                catch (Exception e)
                                {
                                }
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static async Task LoadOnvifProfiles(List<CameraItemModel> cameras)
        {
            
            try
            {
                
                //await Task.WhenAll(cameras.Select(async c =>
                //{
                //    try
                //    {
                //        if (c != null)
                //        {
                            
                //        }
                //    }
                //    catch (Exception e)
                //    {
                //    }
                //})); 
                await Task.Run(async () =>
                {
                    await OnvifProfileItemManager.Manager.RefreshProfiles(
                        new ItemManagerRequestParameters
                        {
                            AcceptedCacheDuration = TimeSpan.FromMinutes(5),
                            CacheDuration = TimeSpan.FromMinutes(5)
                        },cameras.Where(o => o != null).ToArray());
                });
            }
            catch (Exception e)
            {
            }
        }

        public static async Task LoadPresetsBatch(List<CameraItemModel> cameras)
        {
            try
            {
                using (var cts = new CancellationTokenSource(12000))
                {
                    await Task.Run(async () =>
                    {
                        try
                        {
                            foreach (var c in cameras.ToList())
                            {
                                if (c != null)
                                {
                                    c.PresetItemManager.RefreshPresets(c.ZoneId,
                                        new ItemManagerRequestParameters
                                        {
                                            AcceptedCacheDuration = TimeSpan.FromMinutes(5),
                                            CacheDuration = TimeSpan.FromMinutes(5)
                                        }, cameras.Where(c => c != null).Select(o => o.Id).ToArray());
                                }
                            }
                            //var results = new List<KeyValuePair<Tuple<Guid, string>, PtzPreset>>();
                            //cameras = cameras.Where(o => o != null && o.ZoneId != Guid.Empty).ToList();
                            //var camerasByZoneId = cameras.GroupBy(b => b.ZoneId).ToDictionary(
                            //    o => o.Key,
                            //    model => cameras.Where(o => o.ZoneId == model.Key));
                            //foreach (var c in camerasByZoneId)
                            //{
                            //    if (c.Key != null && c.Key != Guid.Empty)
                            //    {
                            //        if (DirectModeMgr.IsDirectMode)
                            //        { 
                            //            //cameras.FirstOrDefault().PresetItemManager.
                            //        }
                            //        else if (CommunicationManager.ServiceManagers.ContainsKey(c.Key))
                            //        {
                            //            foreach (var cam in c.Value)
                            //            {

                            //            }
                            //        }
                            //    }
                            //}

                            //if (results.Count > 0)
                            //{
                            //    cameras?.FirstOrDefault()?.PresetItemManager.Update(results.ToArray());
                            //}
                        }
                        catch (Exception e)
                        {
                            _log?.Info(e);
                        }
                    }, cts.Token);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"GetRecordingProfiles {e}");
            }
        }

        public static async Task GetONVIFProfiles(List<CameraItemModel> cameras, Action<Exception> exceptionOut = null)
        {
            //Required for some ONVIF requests to our Hanwha
            ServicePointManager.Expect100Continue = false;
            try
            {
                foreach (var c in cameras.ToList())
                {
                    if (c != null)
                    {
                        //await Task.Run(async () =>
                        //{
                        //    try
                        //    {
                        //        if (!c.IsGettingOnvifProfiles)
                        //        {
                        //            c.IsGettingOnvifProfiles = true;
                        //            var sw = new Stopwatch();
                        //            sw.Start();
                        //            while (c.Address == null)
                        //            {
                        //                await Task.Delay(25);
                        //            }

                        //            sw.Stop();
                        //            Console.WriteLine($"Address took {sw.ElapsedMilliseconds}ms to resolve!");
                        //            if (c.MediaController == null)
                        //            {
                        //                //We now reuse the existing connection...
                        //                var onvifManager = new ONVIFManager(c.Address,
                        //                    c.Username ?? "root", c.Password ?? "pass");
                        //                while (c.MediaController == null)
                        //                {

                        //                    c.MediaController ??= onvifManager
                        //                        ?.OnvifCameraConnection.MediaClient;
                        //                    await Task.Delay(25);
                        //                }
                        //            }

                        //            if (c.MediaController != null)
                        //            {

                        //                while (c.MediaController.State != CommunicationState.Opened)
                        //                {
                        //                    await Task.Delay(35);
                        //                }

                        //                try
                        //                {
                        //                    var convertedProfiles = await GetOnvifProfiles(c.MediaController, c);
                        //                }
                        //                catch (Exception e)
                        //                {
                        //                    Console.WriteLine(
                        //                        $"GetProfilesAsync Threw an Error {e.GetType()} {e.Message} {c.Address} {c.Username} {c.Password}");
                        //                    exceptionOut?.Invoke(e);
                        //                }
                        //            }
                        //        }
                        //    }
                        //    catch (Exception e)
                        //    {
                        //        Console.WriteLine(
                        //            $"Getting Onvif Threw an Error {e.GetType()} {c.Address} {c.Username} {c.Password}");
                        //        exceptionOut?.Invoke(e);
                        //    }
                        //    finally
                        //    {
                        //        c.IsGettingOnvifProfiles = false;
                        //    }
                        //}).TimeoutAfter(TimeSpan.FromSeconds(4));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                exceptionOut?.Invoke(e);
            }
        }

        //private static async Task<List<BaseAdapterCameraProfileItemModel>> GetOnvifProfiles(MediaClient mediaController, CameraItemModel c, Action<Exception> exceptionOut = null)
        //{
        //    try
        //    {
        //        //  var capabilities = await MediaController.GetServiceCapabilitiesAsync();
        //        var profiles = await mediaController.GetProfilesAsync();
        //        var convertedProfiles = profiles.Profiles
        //            .Select(p => p.ConvertTo<BaseAdapterCameraProfileItemModel>()).ToList();

        //        if (convertedProfiles != null)
        //        {
        //            var firstProfile = true;
        //            foreach (var p in convertedProfiles)
        //            {
        //                if (p.token.IsNotEmpty())
        //                {
        //                    if (p.VideoEncoderConfiguration.Encoding != VideoEncoding.H264)
        //                    {
        //                        continue;
        //                    }

        //                    try
        //                    {
        //                        var uri =
        //                            await mediaController.GetStreamUriAsync(
        //                                new StreamSetup
        //                                {
        //                                    Stream = StreamType.RTPUnicast,
        //                                    Transport = new Transport
        //                                        {Protocol = TransportProtocol.RTSP}
        //                                }, p.token);
        //                        if (uri.Uri.IsNotEmpty())
        //                        {
        //                            p.StreamUri = uri.Uri;
        //                            try
        //                            {
        //                                p.Resolution =
        //                                    new Size(
        //                                        p.VideoEncoderConfiguration.Resolution
        //                                            .Width,
        //                                        p.VideoEncoderConfiguration.Resolution
        //                                            .Height);
        //                            }
        //                            catch (Exception e)
        //                            {
        //                                exceptionOut?.Invoke(e);
        //                            }

        //                            if (firstProfile)
        //                            {
        //                                c.CurrentOnvifProfile = p;
        //                                firstProfile = false;
        //                            }
        //                        }
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        Console.WriteLine($"GetStreamUriAsync {e}");
        //                        exceptionOut?.Invoke(e);
        //                    }
        //                }
        //            }

        //            //var baseAdapterCameraProfileItemModels = new ObservableCollection<BaseAdapterCameraProfileItemModel>(
        //            //    convertedProfiles.Where(o => o.StreamUri.IsNotEmpty()));
        //            //c.OnvifProfiles =
        //            //    baseAdapterCameraProfileItemModels;
        //        }

        //        return convertedProfiles;
        //    }
        //    catch (Exception e)
        //    {
        //        exceptionOut?.Invoke(e);
        //        return default;

        //    }
        //}

        /// <summary>
        ///     This method loads RecordingProfiles of Cameras in a Batch request
        /// </summary>
        /// <param name="cameras"></param>
        /// <returns></returns>
        public static async Task LoadRecordingProfilesBatch(List<CameraItemModel> cameras)
        {
            try
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        await RecordingProfileItemManager.Manager.RequestRecordingProfileBatch(cameras.ToArray());
                        //cameras = cameras.Where(o => o != null && o.ZoneId != Guid.Empty).ToList();
                        //var camerasByZoneId = cameras.GroupBy(b => b.ZoneId).ToDictionary(
                        //    o => o.Key,
                        //    model => cameras.Where(o => o.ZoneId == model.Key));
                        //foreach (var c in camerasByZoneId)
                        //{
                        //    if (c.Key != null && c.Key != Guid.Empty)
                        //    {
                        //        if (CommunicationManager.ServiceManagers.ContainsKey(c.Key))
                        //        {
                        //            var cameraGuids = c.Value
                        //                .Select(o => new GetRecordingProfilesByCameraId {Id = o.Id})
                        //                .ToList().Distinct().ToArray();
                        //            var profiles = await CommunicationManager.ServiceManagers[c.Key]
                        //                .RecordingProfileService.Client
                        //                .Request(cameraGuids);

                        //            if (profiles != null)
                        //            {
                        //                var keyValuePairs = profiles.SelectMany(o => o.RecordingProfiles)
                        //                    .ToDictionary(o => o.Id, o => (RecordingProfileItemModel) o).ToArray();
                        //                cameras.FirstOrDefault()?.RecordingProfileItemMgr.Update(keyValuePairs);
                        //            }
                        //        }
                        //    }
                        //}
                    }
                    catch (Exception e)
                    {
                        _log?.Info(e);
                    }
                }).TimeoutAfter(TimeSpan.FromSeconds(15));
            }
            catch (Exception e)
            {
                Console.WriteLine($"GetRecordingProfiles {e}");
            }
        }
    }
}