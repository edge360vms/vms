﻿using System;
using System.Collections.Generic;
using Edge360.Platform.VMS.Client.ItemModel.Camera;

namespace Edge360.Platform.VMS.Client.ItemModel.Layouts
{
    [Serializable]
    public class LayoutSettings
    {
        public List<LayoutItemModel> Layouts { get; set; } = new List<LayoutItemModel>();
    }
}