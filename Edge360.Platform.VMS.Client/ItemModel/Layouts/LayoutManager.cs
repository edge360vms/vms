﻿using System;
using System.Diagnostics;
using System.IO;
using log4net;
using Newtonsoft.Json;

namespace Edge360.Platform.VMS.Client.ItemModel.Layouts
{
    public static class LayoutManager
    {
        static ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly string AppDataPath =
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

        private static readonly string VmsAppDataPath =
            Path.Combine(AppDataPath, @"E360\VMS\");

        public static LayoutSettings Layouts { get; set; } = new LayoutSettings();

        public static void LoadLayouts()
        {
            // Layouts = Load<LayoutSettings>("layouts.json") ?? new LayoutSettings();
        }

        private static T Load<T>(string fileName)
        {
            try
            {
                if (CheckDirectoryExists())
                {
                    var file = File.ReadAllText(Path.Combine(VmsAppDataPath, fileName));
                    if (string.IsNullOrEmpty(file))
                    {
                        return default;
                    }

                    var bookmarks = JsonConvert.DeserializeObject<T>(file);
                    return bookmarks;
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }

            return default;
        }

        private static bool CheckDirectoryExists()
        {
            try
            {
                var appDirectory = Directory.Exists(VmsAppDataPath);
                if (!appDirectory)
                {
                    Directory.CreateDirectory(VmsAppDataPath);
                }

                return true;
            }
            catch (Exception e)
            {
                _log?.Info(e);
                return false;
            }
        }

        public static void SaveLayouts()
        {
            //try
            //{
            //    Save("layouts.json", JsonConvert.SerializeObject(Layouts, Formatting.Indented));
            //}
            //catch (Exception e)
            //{
            //    _log?.Info(e);
            //}
        }

        private static void Save(string fileName, string output)
        {
            try
            {
                CheckDirectoryExists();

                File.WriteAllText(Path.Combine(VmsAppDataPath, fileName), output);
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }
    }
}