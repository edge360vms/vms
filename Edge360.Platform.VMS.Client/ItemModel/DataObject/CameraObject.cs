﻿using System;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Cameras;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Interfaces;
using ServiceStack;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.ItemModel.DataObject
{
    public class CameraObject : NodeObject, ICamera
    {
        private bool _actualRecordingStatusInd;
        private string _address;
        private CameraInformationItemModel _cameraInformation;

        private string _description;
        private Guid _id;
        private string _liveStreamUrl;
        private string _password;
        private string _path;
        private string _port;
        private string _username;
        private string _zone;
        private Guid _zoneId;
        private string _assetId;
        private string _cameraIp;
        private string _status;
        private bool? _isCameraEnabled;
        private int _propertyFlags;

        public override Guid? ObjectId => Id;

        public CameraInformationItemModel CameraInformation
        {
            get
            {
                if (_cameraInformation == null)
                {
                    if (!DirectModeMgr.IsOfflineMode)
                    {
                        //var _ = Task.Run(async () =>
                        //{
                        //    try
                        //    {
                        //        var managerByZoneId = GetManagerByZoneId();
                        //        if (managerByZoneId != null)
                        //        {
                        //            var info = await managerByZoneId
                        //                .CameraAdapterService.GetCameraInfo(Id);
                        //            if (info != null)
                        //            {
                        //                _cameraInformation =
                        //                    info?.CameraInformation.ConvertTo<CameraInformationItemModel>();
                        //            }
                        //        }
                        //    }
                        //    catch (Exception e)
                        //    {
                                
                        //    }
                        //});
                    }
                    else
                    {
                        
                    }
                }

                if (_cameraInformation != null)
                {
                    return _cameraInformation;
                }

                return null;
            }
            set
            {
                if (Equals(value, _cameraInformation))
                {
                    return;
                }

                _cameraInformation = value;
                OnPropertyChanged();
            }
        }

        public string Zone
        {
            get => _zone;
            set
            {
                if (_zone != value)
                {
                    _zone = value;
                    OnPropertyChanged();
                }
            }
        }

        public CameraObject()
        {
            Initialize();
        }

        public bool ActualRecordingStatusInd
        {
            get => _actualRecordingStatusInd;
            set
            {
                if (value == _actualRecordingStatusInd)
                {
                    return;
                }

                _actualRecordingStatusInd = value;
                OnPropertyChanged();
            }
        }

        public int PropertyFlags
        {
            get => _propertyFlags;
            set
            {
                if (value == _propertyFlags) return;
                _propertyFlags = value;
                OnPropertyChanged();
            }
        }

        public virtual string Address
        {
            get => _address;
            set
            {
                if (_address != value)
                {
                    _address = value;
                    OnPropertyChanged();
                }
            }
        }

        public string AssetId
        {
            get => _assetId;
            set
            {
                if (value == _assetId) return;
                _assetId = value;
                OnPropertyChanged();
            }
        }

        public string CameraIp
        {
            get => _cameraIp;
            set
            {
                if (value == _cameraIp) return;
                _cameraIp = value;
                OnPropertyChanged();
            }
        }

        public virtual string Description
        {
            get => _description;
            set
            {
                if (_description != value)
                {
                    _description = value;
                    OnPropertyChanged();
                }
            }
        }

        public Guid Id
        {
            get => _id;
            set
            {
                _id = value;
                ObjectId = _id;
                OnPropertyChanged();
                OnPropertyChanged(nameof(ObjectId));
            }
        }

        public string LiveStreamType { get; set; }

        public virtual string LiveStreamUrl
        {
            get => _liveStreamUrl;
            set
            {
                if (value == _liveStreamUrl)
                {
                    return;
                }

                _liveStreamUrl = value;
                OnPropertyChanged();
            }
        }

        public virtual string Password
        {
            get => _password;
            set
            {
                if (_password != value)
                {
                    _password = value;
                    OnPropertyChanged();
                }
            }
        }

        public virtual string Path
        {
            get => _path;
            set
            {
                if (_path != value)
                {
                    _path = value;
                    OnPropertyChanged();
                }
            }
        }

        public virtual string Port
        {
            get => _port;
            set
            {
                if (_port != value)
                {
                    _port = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool? IsCameraEnabled
        {
            get
            {
                if (_isCameraEnabled == null)
                {
                    _isCameraEnabled = string.IsNullOrEmpty(Status) || Status == "Enabled";
                }

                return _isCameraEnabled;
            }
            set
            {
                if (value == _isCameraEnabled) return;

                Status = value ?? false ? "Enabled" : "Disabled";
                
                _isCameraEnabled = value;
                OnPropertyChanged();
            }
        }

        public string Status
        {
            get => _status;
            set
            {
                if (value == _status) return;
                _status = value;
                OnPropertyChanged(nameof(IsCameraEnabled));
                OnPropertyChanged();
            }
        }

        public virtual string Username
        {
            get => _username;
            set
            {
                if (_username != value)
                {
                    _username = value;
                    OnPropertyChanged();
                }
            }
        }

        public Guid ZoneId
        {
            get { return base.ZoneId ?? Guid.Empty; }
            set
            {
                base.ZoneId = value;
            }
        }

        public virtual void Initialize()
        {
        }

        public static implicit operator CameraObject(CameraDescriptor descriptor)
        {
            return descriptor.ConvertTo<CameraObject>();
        }

        public static implicit operator CameraDescriptor(CameraObject obj)
        {
            return obj.ConvertTo<CameraDescriptor>();
        }
    }
}