﻿namespace Edge360.Platform.VMS.Client.ItemModel.DataObject
{
    //public class RecordingProfileObject : NodeObject, INotifyPropertyChanged, IRecordingProfileDescriptor
    //{
    //    private Guid _cameraId;
    //    private int _height;
    //    private Guid _id;
    //    private bool _isEnabled;
    //    private string _label;
    //    private string _parameters;
    //    private string _rtspAddress;
    //    private List<Guid> _volumeIds;
    //    private int _width;
    //    private RecordingProfileDescriptor _descriptor;

    //    public Task<string> ResolvedCameraLabel => ResolveCamera();

    //    public List<VolumeItemModel> ResolvedVolumes => GetVolumeDescriptorsAsync();

    //    public Guid CameraId
    //    {
    //        get => _cameraId;
    //        set
    //        {
    //            _cameraId = value;
    //            OnPropertyChanged();
    //        }
    //    }

    //    public int Height
    //    {
    //        get => _height;
    //        set
    //        {
    //            _height = value;
    //            OnPropertyChanged();
    //        }
    //    }

    //    public Guid Id
    //    {
    //        get => _id;
    //        set
    //        {
    //            _id = value;
    //            OnPropertyChanged();
    //        }
    //    }

    //    public bool IsEnabled
    //    {
    //        get => _isEnabled;
    //        set
    //        {
    //            _isEnabled = value;
    //            OnPropertyChanged();
    //        }
    //    }

    //    public string Parameters
    //    {
    //        get => _parameters;
    //        set
    //        {
    //            _parameters = value;
    //            OnPropertyChanged();
    //        }
    //    }

    //    public string Label
    //    {
    //        get => _label;
    //        set
    //        {
    //            _label = value;
    //            OnPropertyChanged();
    //        }
    //    }

    //    public string RtspAddress
    //    {
    //        get => _rtspAddress;
    //        set
    //        {
    //            _rtspAddress = value;
    //            OnPropertyChanged();
    //        }
    //    }

    //    public List<Guid> VolumeIds
    //    {
    //        get => _volumeIds;
    //        set
    //        {
    //            _volumeIds = value;
    //            OnPropertyChanged();
    //            OnPropertyChanged(nameof(ResolvedVolumes));
    //        }
    //    }

    //    public int Width
    //    {
    //        get => _width;
    //        set
    //        {
    //            _width = value;
    //            OnPropertyChanged();
    //        }
    //    }

    //    public async Task<string> ResolveCamera()
    //    {
    //        if (CameraId == Guid.Empty)
    //        {
    //            return "None";
    //        }

    //        var found = await ServiceManagers[HARD_CODED_ZONE_ID].CameraService.GetCamera(CameraId);
    //        var f = found?.Camera;
    //        var model = f?.ConvertTo<CameraItemModel>();
    //        RtspAddress = model?.CameraUrl;
    //        return f?.Label ?? "None";
    //    }

    //    public List<VolumeItemModel> GetVolumeDescriptorsAsync()
    //    {
    //        return Task.Run(() =>
    //        {
    //            return VolumeIds == null || !VolumeIds.Any()
    //                ? new List<VolumeItemModel>()
    //                : VolumeIds.Select(guid => VolumeCache.Instance.GetAsync(guid)
    //                        .Result)
    //                    .Select(t => (VolumeItemModel) t)
    //                    .ToList();
    //        }).Result;
    //    }

    //    public static implicit operator RecordingProfileDescriptor(RecordingProfileObject obj)
    //    {
    //        return obj.ConvertTo<RecordingProfileDescriptor>();
    //    }


    //}
}