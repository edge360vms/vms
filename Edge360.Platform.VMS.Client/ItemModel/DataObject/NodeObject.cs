﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Newtonsoft.Json;
using Unity;

namespace Edge360.Platform.VMS.Client.ItemModel.DataObject
{
    public class NodeObject : INotifyPropertyChanged
    {
        private ObservableCollection<NodeObject> _children = new ObservableCollection<NodeObject>();
        private DateTime _dateCreated;
        private string _filterText = string.Empty;
        private string _label;
        private DateTime _lastModified;
        private Guid? _objectId;
        private Guid? _parentId;
        private string _type;
        private Guid? _zoneId;
        private CancellationTokenSource _filterCts;
        private Task _filterTask;

        [JsonIgnore]
        public ObservableCollection<NodeObject> Children
        {
            get => _children;
            set
            {
                if (_children != value)
                {
                    _children = value;
                    OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public CollectionViewSource ChildrenCollectionViewSource { get; set; } = new CollectionViewSource
        {
            SortDescriptions =
            {
                new SortDescription {PropertyName = "Type", Direction = ListSortDirection.Descending},
                new SortDescription {PropertyName = "Label"}
            }
        };

        public DateTime DateCreated
        {
            get => _dateCreated;
            set
            {
                if (_dateCreated != value)
                {
                    _dateCreated = value;
                    OnPropertyChanged();
                }
            }
        }

        [JsonIgnore]
        public string FilterText
        {
            get => _filterText;
            set
            {
                if (value == _filterText)
                {
                    return;
                }

                _filterText = value;
                OnPropertyChanged();
                _filterCts?.Cancel();
                _filterCts = new CancellationTokenSource();
                var cts = _filterCts;
                _filterTask = Task.Run(async () =>
                {
                    await Task.Delay(125);
                    if (!cts.IsCancellationRequested)
                    {
                        App.Current?.Dispatcher?.InvokeAsync(() =>
                        {
                            ChildrenCollectionViewSource?.View?.Refresh();
                        });
                    }
                }, cts.Token);
            }
        }

        public virtual string Label
        {
            get => _label;
            set
            {
                if (_label != value)
                {
                    _label = value;
                    OnPropertyChanged();
                }
            }
        }

        public DateTime LastModified
        {
            get => _lastModified;
            set
            {
                if (_lastModified != value)
                {
                    _lastModified = value;
                    OnPropertyChanged();
                }
            }
        }

        public virtual Guid? ObjectId
        {
            get => _objectId;
            set
            {
                if (_objectId != value)
                {
                    _objectId = value;
                    OnPropertyChanged();
                }
            }
        }

        public Guid? ParentId
        {
            get => _parentId;
            set
            {
                if (_parentId != value)
                {
                    _parentId = value;
                    OnPropertyChanged();
                }
            }
        }

        public Guid? ZoneId
        {
            get => _zoneId;
            set
            {
                if (Nullable.Equals(value, _zoneId)) return;
                _zoneId = value;
                OnPropertyChanged();
            }
        }

        public virtual string Type
        {
            get => _type;
            set
            {
                if (_type != value)
                {
                    _type = value;
                    OnPropertyChanged();
                }
            }
        }

        public NodeObject()
        {
            try
            {
                DirectModeMgr = App.GlobalUnityContainer?.Resolve<DirectModeManager>();
                _ = this.TryUiAsync(() =>
                {
                    //BindingOperations.SetBinding(ChildrenCollectionViewSource,
                    //    CollectionViewSource.SourceProperty,
                    //    new Binding(nameof(Children)) { Source = this, Mode = BindingMode.OneWay });
                    var previousChildren = Children.ToList();
                    Children = new ObservableCollection<NodeObject>(previousChildren);
                    ChildrenCollectionViewSource = new CollectionViewSource {Source = Children,  SortDescriptions =
                    {
                        new SortDescription {PropertyName = "Type", Direction = ListSortDirection.Descending},
                        new SortDescription {PropertyName = "Label"}
                    }};
                    ChildrenCollectionViewSource.Filter += ChildrenCollectionViewSourceOnFilter;
                    //BindingOperations.EnableCollectionSynchronization(_children, _childListLock);
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                
            }
        }

        [JsonIgnore]
        public DirectModeManager DirectModeMgr { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return
                $"{nameof(DateCreated)}: {DateCreated}, {nameof(Label)}: {Label}, {nameof(LastModified)}: {LastModified}, {nameof(ObjectId)}: {ObjectId}, {nameof(ParentId)}: {ParentId}, {nameof(Type)}: {Type}";
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            //Console.WriteLine($"OnPropertyChanged {propertyName}");
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ChildrenCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            try
            {
                if (e.Item is NodeObject item)
                {
                    e.Accepted = FilterText.PartialContain(item.Label, item.ObjectId.ToString());
                }
            }
            catch (Exception exception)
            {
                
            }
        }
    }
}