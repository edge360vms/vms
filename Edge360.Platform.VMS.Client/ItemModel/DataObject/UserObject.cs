﻿using System;
using System.ComponentModel;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Users;

namespace Edge360.Platform.VMS.Client.ItemModel.DataObject
{
    /// <summary>
    ///     Object representing Users inside of the VMS
    /// </summary>
    public class UserObject : NodeObject, INotifyPropertyChanged
    {
        private UserDetails _descriptor;

        private string _groups;
        private Guid? _id;
        private string _roles;
        private string _username;

        public UserDetails Descriptor
        {
            get => _descriptor;
            set => _descriptor = value;
        }

        public string Groups
        {
            get => _groups;
            set
            {
                if (_groups != value)
                {
                    _groups = value;
                    OnPropertyChanged();
                }
            }
        }

        public Guid? Id
        {
            get => _id;
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Roles
        {
            get => _roles;
            set
            {
                if (_roles != value)
                {
                    _roles = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Username
        {
            get => _username;
            set
            {
                if (_username != value)
                {
                    _username = value;
                    OnPropertyChanged();
                }
            }
        }

        //public UserObject(UserDetails descriptor)
        //{
        //    _descriptor = descriptor;
        //    _username = descriptor.Username;
        //    _id = descriptor.UserId;
        //}

        public override string ToString()
        {
            return
                $"{base.ToString()}, {nameof(_descriptor)}: {_descriptor}, {nameof(_groups)}: {_groups}, {nameof(_id)}: {_id}, {nameof(_roles)}: {_roles}, {nameof(_username)}: {_username}, {nameof(Descriptor)}: {Descriptor}, {nameof(Groups)}: {Groups}, {nameof(Id)}: {Id}, {nameof(Roles)}: {Roles}, {nameof(Username)}: {Username}";
        }
    }
}