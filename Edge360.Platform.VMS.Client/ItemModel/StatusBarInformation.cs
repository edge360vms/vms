﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Common.Interfaces.Authority;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Communication;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Users;
using Edge360.Vms.AuthorityService.SharedModel.Enums;

namespace Edge360.Platform.VMS.Client.ViewModel.View
{
    public class StatusBarInformation : INotifyPropertyChanged, IDisposable
    {
        private readonly Timer _timer;
        private DateTime _currentTime = DateTime.Now;

        public DateTime CurrentTime
        {
            get => _currentTime;
            set
            {
                if (value.Equals(_currentTime))
                {
                    return;
                }

                _currentTime = value;
                OnPropertyChanged();
            }
        }

        public UserDetails CurrentUserDetails => CommunicationManager.CurrentUser;

        public UserLogin CurrentUserItemModel => SettingsManager.Settings.Login.Credentials;

        public ERoles EffectivePermission => CommunicationManager.EffectivePermissions;

        public StatusBarInformation()
        {
            _timer = new Timer(Callback, null, TimeSpan.FromMilliseconds(250), TimeSpan.FromMilliseconds(250));
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Callback(object state)
        {
            CurrentTime = DateTime.Now;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}