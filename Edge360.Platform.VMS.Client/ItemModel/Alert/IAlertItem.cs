﻿namespace Edge360.Platform.VMS.Client.ItemModel.Alert
{
    public interface IAlertItem
    {
        //RelayCommand Command { get; set; }
        string Description { get; set; }

        string Glyph { get; set; }

        bool IsHandled { get; set; }
        string Title { get; set; }
    }
}