﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Threading;
using CameraService.Adapters.Shared.Enums;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Management;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Util;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.ItemModel.Authority
{
    public class ZoneItemModel : INotifyPropertyChanged
    {
        private CollectionViewSource _cameraCollectionViewSource;
        private RelayCommand _checkConnectionCommand;

        private bool _isExpanded;
        private bool _isRefreshingCameras;
        private string _label;
        private DelegateCommand _refreshServersCommand;
        private object _selectedZoneItem;
        private CollectionViewSource _serverCollectionViewSource;
        private ObservableCollection<VolumeItemModel> _volumes;

        private Guid _zoneId;
        private DelegateCommand _refreshCamerasCommand;


        public CollectionViewSource CameraCollectionViewSource { get; set; }

        public RelayCommand CheckZoneConnectionCommand => _checkConnectionCommand ??= new RelayCommand(
            CheckZoneConnection,
            o => true);

        public ZoneCameraManagementViewModel GetZoneCameraViewModel => ObjectResolver
            .ResolveAll<ZoneCameraManagementViewModel>()
            ?.FirstOrDefault(o => o?.SelectedZone == this);

        public bool IsExpanded
        {
            get => _isExpanded;
            set
            {
                if (value == _isExpanded)
                {
                    return;
                }

                _isExpanded = value;
                OnPropertyChanged();
            }
        }

        public bool IsRefreshingCameras
        {
            get => _isRefreshingCameras;
            set
            {
                if (value == _isRefreshingCameras)
                {
                    return;
                }

                _isRefreshingCameras = value;
                OnPropertyChanged();
            }
        }

        public string Label
        {
            get => _label;
            set
            {
                if (value == _label)
                {
                    return;
                }

                _label = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand RefreshCamerasCommand => _refreshCamerasCommand ??= new DelegateCommand(RefreshCameras);

        private async void RefreshCameras(object obj)
        {
            try
            {
                IsRefreshingCameras = true;

                await Task.Run(async () =>
                {
                    await CameraItemModelManager.Manager.RefreshCameras(ZoneId, parameters: new ItemManagerRequestParameters(){AcceptedCacheDuration = TimeSpan.Zero});
                });

                IsRefreshingCameras = false;
            }
            catch (Exception e)
            {
                
            }
        }

        public DelegateCommand RefreshServersCommand => _refreshServersCommand ??= new DelegateCommand(RefreshServers);

        public object SelectedZoneItem
        {
            get => _selectedZoneItem;
            set
            {
                if (value is RadTreeViewItem)
                {
                    return;
                }

                if (Equals(value, _selectedZoneItem))
                {
                    return;
                }

                if (value == null)
                {
                    return;
                }

                _selectedZoneItem = value;
                OnPropertyChanged();
            }
        }

        public CollectionViewSource ServerCollectionViewSource { get; set; }

        public Guid ZoneId
        {
            get => _zoneId;
            set
            {
                if (value.Equals(_zoneId))
                {
                    return;
                }

                _zoneId = value;
                OnPropertyChanged();
            }
        }

        public object Servers => ServerItemManager.Manager.ItemCollection;

        public ZoneItemModel()
        {
            _ = this.TryUiAsync(() =>
            {
                ServerCollectionViewSource = new CollectionViewSource
                {
                    Source = ServerItemManager.Manager.ItemCollection
                };
                CameraCollectionViewSource = new CollectionViewSource() { Source = CameraItemModelManager.Manager.ItemCollection, SortDescriptions = { new SortDescription { PropertyName = "Value.Label" } } };
                ServerCollectionViewSource.Filter += ServerCollectionViewSourceOnFilter;
                CameraCollectionViewSource.Filter += CameraCollectionViewSourceOnFilter;
            }, DispatcherPriority.Background).ConfigureAwait(false);

            //BindingOperations.SetBinding(ServerCollectionViewSource,
            //    CollectionViewSource.SourceProperty,
            //    new Binding(nameof(Servers)) { Source = this, Mode = BindingMode.OneWay });

            // ServerCollectionViewSource.View.Refresh();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private async void RefreshServers(object obj)
        {
            try
            {
                var _ = Task.Run(async () =>
                {
                    await ZoneItemManager.GetManagerByZoneIdConnectIfFail(ZoneId);
                    await ServerItemManager.Manager.RefreshServers(ZoneId,
                        new ItemManagerRequestParameters {AcceptedCacheDuration = TimeSpan.Zero});
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
            }
        }


        private async void CheckZoneConnection(object o)
        {
            await ZoneItemManager.Manager.RefreshZone(ZoneId);
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ServerCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is KeyValuePair<Tuple<Guid, Guid>, ServerItemModel> model)
            {
                e.Accepted = model.Key.Item2 == ZoneId;
            }
        }

        private void CameraCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            try
            {
                if ( e.Item is KeyValuePair<Guid, CameraItemModel> item)
                {
                    var camera = item.Value;
                    if (camera == null)
                    {
                        return;
                    }

                var zoneCameraManagementViewModel = GetZoneCameraViewModel;
                if (camera.ZoneId != ZoneId)
                {
                    e.Accepted = false;
                    return;
                }

                var matched = zoneCameraManagementViewModel == null ||
                              zoneCameraManagementViewModel.CameraFilterText == string.Empty || camera != null &&
                              zoneCameraManagementViewModel.CameraFilterText.ToLowerInvariant()
                                  .PartialContain(camera.Label, camera.Address, camera.Id.ToString(),
                                      camera?.ParentId?.ToString());

                if (!matched)
                {
                    if (camera != null)
                    {
                        //check RecordingProfileId
                        var recordings =
                            camera.RecordingProfileItemMgr.ItemCollection.Values.Where(o => o.CameraId == camera.Id)
                                .ToList();
                        matched = recordings.Any(o =>
                            zoneCameraManagementViewModel.CameraFilterText.PartialContain(o.Id.ToString()));

                        if (!matched)
                        {
                            //check volumeId
                            foreach (var r in recordings)
                            {
                                matched = r.VolumeIds.Any(o =>
                                    zoneCameraManagementViewModel.CameraFilterText.PartialContain(o.ToString()));
                                if (matched)
                                {
                                    break;
                                }
                            }

                            if (!matched)
                            {
                                //check liveProfileId
                                matched = camera.StreamProfileItemMgr.ItemCollection
                                    .Where(o => o.Key.Item2 == camera.Id).Any(o =>
                                        zoneCameraManagementViewModel.CameraFilterText.PartialContain(
                                            o.Value.Id.ToString()));
                            }
                        }
                    }
                }

                if (matched && camera != null && zoneCameraManagementViewModel != null)
                {
                    if (zoneCameraManagementViewModel.IsFilteringOnline &&
                        (string.IsNullOrEmpty(camera.Status) || camera.Status == "Enabled") &&
                        (camera.CameraStatus == null || camera.CameraStatus.Status == (int) ECameraStatus.Online))
                    {
                        matched = false;
                    }
                    else if (zoneCameraManagementViewModel.IsFilteringDisabled && camera.Status == "Disabled")
                    {
                        matched = false;
                    }
                    else if (zoneCameraManagementViewModel.IsFilteringOffline && camera.CameraStatus != null &&
                             camera.CameraStatus.Status == (int) ECameraStatus.Offline)
                    {
                        matched = false;
                    }
                }


                e.Accepted = matched;
                }
            }
            catch (Exception exception)
            {
            }
        }
    }
}