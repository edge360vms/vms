﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Roles;
using Edge360.Vms.AuthorityService.SharedModel;
using Edge360.Vms.AuthorityService.SharedModel.Enums;
using ServiceStack;

namespace Edge360.Platform.VMS.Client.ItemModel.Authority
{
    public class RoleItemModel : IRole, INotifyPropertyChanged
    {
        private ServerItemModel _resolvedServer;
        public string AvailableRolesPreview => StringifyAvailableRoles();
        public bool IsRequestingZone { get; set; }

        public ServerItemModel ResolvedServer
        {
            get => _resolvedServer;
            set
            {
                if (Equals(value, _resolvedServer))
                {
                    return;
                }

                _resolvedServer = value;
                OnPropertyChanged();
            }
        }

        public List<ERoles> RolesList => Enum.GetValues(typeof(ERoles)).OfType<ERoles>().ToList().RemoveFirstAndLast();

        public event PropertyChangedEventHandler PropertyChanged;

        public Guid? GroupId { get; set; }
        public Guid? ZoneId { get; set; }
        public int RoleValue { get; set; }


        private string StringifyAvailableRoles()
        {
            var values = Enum.GetValues(typeof(ERoles)).OfType<ERoles>().ToList().RemoveFirstAndLast();
            var list = from e in values where ((ERoles) RoleValue).HasRoleAny(e) select $"{e}";

            var output = string.Join(", ", list).CollapseWhitespace().TrimEnd(',');
            return output;
        }

        public static implicit operator RoleItemModel(Role descriptor)
        {
            return descriptor.ConvertTo<RoleItemModel>();
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}