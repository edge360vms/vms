﻿using System;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.AdDomain;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.ItemModel.Authority
{
    public class ADDomainItemModel : AdDomain
    {
        private RelayCommand _deleteDomainCommand;

        public RelayCommand DeleteDomainCommand =>
            _deleteDomainCommand ??= new RelayCommand(p => DeleteDomain());


        private async void DeleteDomain()
        {
            var confirmed = DialogUtil.ConfirmDialog($"Are you sure you wish to delete this Domain '{Domain}'?", true);
            if (confirmed)
            {
                var delete = await GetManagerByZoneId().AdDomainService.DeleteDomainByName(Domain);
                Console.WriteLine($"Delete: {delete}");
            }
        }
    }
}