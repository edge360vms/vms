﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Dialog;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Servers;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.AppSetting;
using Prism.Commands;

namespace Edge360.Platform.VMS.Client.ItemModel.Authority
{
    public class ServerItemModel : IServer, INotifyPropertyChanged
    {
        private DelegateCommand _addAppSettingsCommand;
        private string _address;
        private Dictionary<string, string> _appSettings;
        private ServerConfigItemModel _archiver;
        private CollectionViewSource _archiverCollectionViewSource = new CollectionViewSource();
        private CollectionViewSource _cameraCollectionViewSource;
        private RelayCommand _checkConnectionCommand;
        private EConnectionStatus _connectionStatus;
        private DelegateCommand<ListBox> _deleteAppSettingsCommand;
        private string _description;
        private string _filterText = string.Empty;
        private bool _isRequestingAppSettings;
        private string _label;
        private string _password;
        private DelegateCommand<object> _refreshAppSettingsCommand;
        private DelegateCommand _saveAppSettingsCommand;
        private object _selectedServerItem;
        private Guid _serverId;
        private string _username;

        private bool isCheckingConnection;
        private ObservableCollection<ServerConfigItemModel> _archivers = new ObservableCollection<ServerConfigItemModel>();
        //private readonly object _childListLock = new object();

        public DelegateCommand AddAppSettingsCommand => _addAppSettingsCommand ??= new DelegateCommand(AddAppSetting);

        [Required]
        public virtual string Address
        {
            get => _address;
            set
            {
                if (_address != value)
                {
                    _address = value;
                    OnPropertyChanged();
                }
            }
        }

        public Dictionary<string, string> AppSettings
        {
            get => _appSettings;
            set
            {
                if (Equals(value, _appSettings))
                {
                    return;
                }

                _appSettings = value;
                OnPropertyChanged();
            }
        }

        public ServerConfigItemModel Archiver
        {
            get => _archiver;
            set
            {
                if (_archiver != value)
                {
                    _archiver = value;
                    OnPropertyChanged();
                }
            }
        }

        public CollectionViewSource ArchiverCollectionViewSource
        {
            get => _archiverCollectionViewSource;
            set
            {
                if (Equals(value, _archiverCollectionViewSource))
                {
                    return;
                }

                _archiverCollectionViewSource = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<ServerConfigItemModel> Archivers
        {
            get => _archivers;
            set => _archivers = value;
        }


        public CollectionViewSource CameraCollectionViewSource
        {
            get => _cameraCollectionViewSource;
            set
            {
                if (Equals(value, _cameraCollectionViewSource))
                {
                    return;
                }

                _cameraCollectionViewSource = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand CheckZoneConnectionCommand => _checkConnectionCommand ??= new RelayCommand(
            CheckZoneConnection,
            CanCheckConnection);

        public virtual EConnectionStatus ConnectionStatus
        {
            get => _connectionStatus;
            set
            {
                if (_connectionStatus != value)
                {
                    _connectionStatus = value;
                    OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<CoordinatorAppSettingItemModel> CoordinatorAppSettings { get; set; } =
            new ObservableCollection<CoordinatorAppSettingItemModel>();

        public DelegateCommand<ListBox> DeleteAppSettingsCommand =>
            _deleteAppSettingsCommand ??= new DelegateCommand<ListBox>(DeleteAppSettings);

        public virtual string Description
        {
            get => _description;
            set
            {
                if (_description != value)
                {
                    _description = value;
                    OnPropertyChanged();
                }
            }
        }

        public string FilterText
        {
            get => _filterText;
            set
            {
                if (value == _filterText)
                {
                    return;
                }

                _filterText = value;
                OnPropertyChanged();
            }
        }

        public bool IsRequestingAppSettings
        {
            get => _isRequestingAppSettings;
            set
            {
                if (value == _isRequestingAppSettings)
                {
                    return;
                }

                _isRequestingAppSettings = value;
                OnPropertyChanged();
            }
        }

        [Required]
        public virtual string Label
        {
            get => _label;
            set
            {
                if (_label != value)
                {
                    _label = value;
                    OnPropertyChanged();
                }
            }
        }

        [Required]
        public virtual string Password
        {
            get => _password;
            set
            {
                if (value == _password)
                {
                    return;
                }

                _password = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand<object> RefreshAppSettingsCommand =>
            _refreshAppSettingsCommand ??= new DelegateCommand<object>(RefreshAppSettings);

        public DelegateCommand SaveAppSettingsCommand =>
            _saveAppSettingsCommand ??= new DelegateCommand(SaveAppSettings);

        public ObservableCollection<object> ServerObjects { get; set; } = new ObservableCollection<object>();


        //public object SelectedServerItem
        //{
        //    get => _selectedServerItem;
        //    set
        //    {
        //        if (_selectedServerItem != value)
        //        {
        //            _selectedServerItem = value;
        //            if (_selectedServerItem is CameraItemModel cam)
        //            {
        //                cam.RecordingCollectionViewSource?.View?.Refresh();
        //            }

        //            OnPropertyChanged();
        //        }
        //    }
        //}

        [Required]
        public virtual string Username
        {
            get => _username;
            set
            {
                if (value == _username)
                {
                    return;
                }

                _username = value;
                OnPropertyChanged();
            }
        }

        public ServerItemModel()
        {
            Dispatcher.CurrentDispatcher.InvokeAsync(() =>
            {
                BindingOperations.SetBinding(ArchiverCollectionViewSource,
                    CollectionViewSource.SourceProperty,
                    new Binding(nameof(Archivers)) { Source = this, Mode = BindingMode.OneWay });
                //BindingOperations.EnableCollectionSynchronization(_archivers, _childListLock);
                ArchiverCollectionViewSource.Filter += ArchiverCollectionViewSourceOnFilter;
            }, DispatcherPriority.Background);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual Guid ServerId
        {
            get => _serverId;
            set
            {
                if (value.Equals(_serverId))
                {
                    return;
                }

                _serverId = value;
                OnPropertyChanged();
            }
        }

        public Guid ZoneId { get; set; }

        public string ServerName { get; set; }
        public string HostName { get; set; }
        public int PortApi { get; set; }
        public int PortSso { get; set; }
        public int PortLive { get; set; }
        public int PortArchive { get; set; }
        public int PortArchiveControl { get; set; }

        private async void RefreshAppSettings(object obj)
        {
            try
            {
                IsRequestingAppSettings = true;
                var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(ZoneId);
                var resp = await svc.AppSettingService.GetAppSettings(new GetAppSettings());
                if (resp != null)
                {
                    AppSettings = resp.AppSettings;
                    CoordinatorAppSettings.Clear();
                    if (resp.AppSettings != null)
                    {
                        foreach (var setting in resp.AppSettings)
                        {
                            var coordinatorAppSettingItemModel = new CoordinatorAppSettingItemModel
                            {
                                Name = setting.Key,
                                Value = setting.Value
                            };
                            coordinatorAppSettingItemModel.HasInitialized = true;
                            CoordinatorAppSettings.Add(coordinatorAppSettingItemModel);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                IsRequestingAppSettings = false;
            }
        }

        private async void AddAppSetting()
        {
            try
            {
                var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(ZoneId);
                var focusedManagement = TabUtil.FindFocusedTab<ManagementTab>();
                var dialog = new CreateAppSettingDialog();
                dialog.SetModalView(focusedManagement.ManagementView.DialogOverlayGrid.ModalContentPresenter);

                dialog.Closed += async (sender, args) =>
                {
                    try
                    {
                        var item = dialog.Model?.DialogItem;
                        if (dialog != null && dialog.Result && item != null
                            &&
                            !string.IsNullOrEmpty(item.Name) &&
                            !string.IsNullOrEmpty(item.Value))
                        {
                            _ = this.TryUiAsync(async () =>
                            {
                                try
                                {
                                    var resp = await svc.AppSettingService.SaveAppSetting(new SaveAppSetting
                                        {Name = item.Name, Value = item.Value});

                                    if (resp.Success)
                                    {
                                        RefreshAppSettings(null);
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }, DispatcherPriority.Background);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                };

                //focusedManagement.ManagementView.DialogOverlayGrid.ModalContentPresenter
            }
            catch (Exception e)
            {
            }
        }

        private async void DeleteAppSettings(ListBox obj)
        {
            try
            {
                var coordinatorAppSettingItemModels =
                    obj.SelectedItems.OfType<CoordinatorAppSettingItemModel>().ToList();
                var focusedManagement = TabUtil.FindFocusedTab<ManagementTab>();
                var dialog = await DialogUtil.EmbeddedModalConfirmDialog(
                    focusedManagement.ManagementView.DialogOverlayGrid.ModalContentPresenter,
                    new EmbeddedDialogParameters
                    {
                        Description =
                            $"Are you sure you wish to delete AppSettings?\n\n{string.Join(", ", coordinatorAppSettingItemModels.Select(o => o.Name))}",
                        DialogButtons = EDialogButtons.YesNo,

                        Header = "Delete App Settings"
                    });
                if (dialog)
                {
                    _ = this.TryUiAsync(async () =>
                    {
                        var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(ZoneId);
                        var batchRequest = coordinatorAppSettingItemModels.Select(o => new DeleteAppSetting
                        {
                            Name = o.Name
                        });
                        var resp = await svc.AppSettingService.Client.Request(batchRequest
                            .ToArray()); //(new SaveAppSetting(){Name = "New Setting", Value = ""});
                        RefreshAppSettings(null);
                    }, DispatcherPriority.Background);
                }
            }
            //focusedManagement.ManagementView.DialogOverlayGrid.ModalContentPresenter

            catch (Exception e)
            {
            }
        }

        private async void SaveAppSettings()
        {
            try
            {
                if (CoordinatorAppSettings.Count > 0)
                {
                    var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(ZoneId);
                    var batchRequest = CoordinatorAppSettings.Where(o => o.IsDirty).Select(o => new SaveAppSetting
                    {
                        Name = o.Name,
                        Value = o.Value
                    });
                    var resp = await svc.AppSettingService.Client.Request(batchRequest
                        .ToArray()); //(new SaveAppSetting(){Name = "New Setting", Value = ""});
                    RefreshAppSettings(null);
                    //AppSettings = resp.AppSettings;

                    //if (resp.Success)
                    //{
                    //    CoordinatorAppSettings.Add(new CoordinatorAppSettingItemModel
                    //    {
                    //        Name = "New Setting",
                    //        Value = "",
                    //    });
                    //}
                }
            }
            catch (Exception e)
            {
            }
        }


        private async void CheckZoneConnection(object o)
        {
            try
            {
                if (o == null)
                {
                    o = Address;
                }

                isCheckingConnection = true;
                await ZoneItemManager.Manager.RefreshZone(ZoneId);
                isCheckingConnection = false;
                Trace.WriteLine($"ServerItemModel 484 InvalidateRequerySuggested");
                OnPropertyChanged(nameof(CheckZoneConnectionCommand));
            }
            catch (Exception e)
            {
            }
        }

        private bool CanCheckConnection(object obj)
        {
            return !isCheckingConnection;
        }


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ArchiverCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            e.Accepted = string.IsNullOrEmpty(FilterText) ||
                         e.Item is ServerConfigItemModel model && FilterText.PartialContain(model.Label);
        }
    }

    public class CoordinatorAppSettingItemModel : INotifyPropertyChanged
    {
        private bool _isDirty;

        private string _name;
        private string _value;

        public bool HasInitialized { get; set; }

        public bool IsDirty
        {
            get => _isDirty;
            set
            {
                if (value == _isDirty)
                {
                    return;
                }

                if (HasInitialized)
                {
                    _isDirty = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                if (value == _name)
                {
                    return;
                }

                _name = value;
                IsDirty = true;
                OnPropertyChanged();
            }
        }

        public string Value
        {
            get => _value;
            set
            {
                if (value == _value)
                {
                    return;
                }

                _value = value;
                IsDirty = true;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}