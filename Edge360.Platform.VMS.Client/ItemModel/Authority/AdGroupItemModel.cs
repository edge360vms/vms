﻿using System;
using System.Collections.Generic;
using System.Linq;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.AdConfigZone;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Groups;
using ServiceStack;

namespace Edge360.Platform.VMS.Client.ItemModel.Authority
{
    public class AdGroupItemModel : IAdGroup
    {
        public Guid? GroupId { get; set; }
        public string CommonName { get; set; }
        public Guid? ActiveDirectoryId { get; set; }
        public string DistinguishedName { get; set; }

        public void AddToGroup(GroupItemModel selectedGroup)
        {
            selectedGroup.Members.Add(GroupId);
            if (selectedGroup.MemberDetails == null)
            {
                selectedGroup.MemberDetails = new List<GroupMemberDetails>();
            }

            selectedGroup.MemberDetails.Add(new GroupMemberDetails
            {
                MemberId = GroupId,
                Name = selectedGroup.GroupName,
                Type = "adgroup"
            });
            selectedGroup.AdGroupsDirty = true;

            //selectedGroup.Members.Add(UserId);
            //var addGroup = await GroupsService.UpdateGroup(selectedGroup.ConvertTo<Group>());
        }

        public void RemoveFromGroup(GroupItemModel selectedGroup)
        {
            if (selectedGroup.Members.Contains(GroupId))
            {
                selectedGroup.Members.Remove(GroupId);
            }

            if (selectedGroup.MemberDetails == null)
            {
                selectedGroup.MemberDetails = new List<GroupMemberDetails>();
            }

            var existingMember = selectedGroup.MemberDetails.FirstOrDefault(o => o.MemberId == GroupId);
            if (existingMember != null)
            {
                selectedGroup.MemberDetails.Remove(existingMember);
            }

            selectedGroup.AdGroupsDirty = true;
        }

        public static implicit operator AdGroupItemModel(AdGroup group)
        {
            var adGroupItemModel = group.ConvertTo<AdGroupItemModel>();

            return adGroupItemModel;
        }
    }
}