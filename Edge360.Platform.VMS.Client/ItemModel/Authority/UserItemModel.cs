﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.DataObject;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Dialog;
using Edge360.Platform.VMS.Client.View.Control.Management;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Client.View.Window;
using Edge360.Platform.VMS.Client.View.Window.Management;
using Edge360.Platform.VMS.Client.ViewModel.View;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Groups;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Users;
using log4net;
using ServiceStack;
using Telerik.Windows.Controls;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.ItemModel.Authority
{
    public class UserItemModel : UserObject, IUserDetails
    {
        ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private RelayCommand _changePasswordUserCommand;
        private ICommand _clearSessionsCommand;
        private ICommand _deleteCommand;
        private string _displayName;
        private bool _enabled;
        private bool _isClearingSessions;
        private bool _isEditable;
        private bool _isUpdatingUser;
        private RelayCommand _updateUserCommand;

        private ICommand _viewUserCommand;
        private DelegateCommand _clearCacheCommand;

        public ICommand ChangePasswordUserCommand
        {
            get
            {
                if (_changePasswordUserCommand == null)
                {
                    _changePasswordUserCommand = new RelayCommand(
                        param => ChangePassword(),
                        param => CanChangePassword()
                    );
                }

                return _changePasswordUserCommand;
            }
        }


        public ICommand ClearSessionsCommand => _clearSessionsCommand ??= new RelayCommand(ClearSessions,
            o => !IsClearingSessions);

        public ICommand DeleteUserCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        param => DeleteUser(),
                        param => CanDelete()
                    );
                }

                return _deleteCommand;
            }
        }

        public bool IsClearingSessions
        {
            get => _isClearingSessions;
            set
            {
                if (_isClearingSessions != value)
                {
                    _isClearingSessions = value;

                    OnPropertyChanged();
                    Trace.WriteLine($"CameraItemModel 89 InvalidateRequerySuggested");
                    //CommandManager.InvalidateRequerySuggested();
                    OnPropertyChanged(nameof(ClearSessionsCommand));
                }
            }
        }

        public bool IsDirty => true; // DisplayName != Descriptor.DisplayName || Enabled != Descriptor.Enabled;

        public bool IsEditable
        {
            get => _isEditable;
            set
            {
                if (value == _isEditable)
                {
                    return;
                }

                _isEditable = value;
                if (!_isEditable)
                {
                    if (Descriptor != null)
                    {
                        DisplayName = Descriptor.DisplayName;
                        Enabled = Descriptor.Enabled;
                    }
                }

                OnPropertyChanged();
                OnPropertyChanged(nameof(UpdateUserCommand));
            }
        }

        public bool IsUpdatingUser
        {
            get => _isUpdatingUser;
            set
            {
                if (value == _isUpdatingUser)
                {
                    return;
                }

                _isUpdatingUser = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(UpdateUserCommand));
            }
        }

        public RelayCommand UpdateUserCommand => _updateUserCommand ??= new RelayCommand(UpdateUser,
            p => IsEditable && IsDirty && !IsUpdatingUser);

        public ICommand ViewUserCommand
        {
            get
            {
                if (_viewUserCommand == null)
                {
                    _viewUserCommand = new RelayCommand(
                        param => ViewUser(),
                        param => CanView()
                    );
                }

                return _viewUserCommand;
            }
        }

        //public UserItemModel(UserDetails descriptor) : base(descriptor)
        //{
        //}

        public Guid? UserId { get; set; }

        public string DisplayName
        {
            get => _displayName;
            set
            {
                if (value == _displayName)
                {
                    return;
                }

                _displayName = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsDirty));
            }
        }

        public string UserType { get; set; }

        public bool Enabled
        {
            get => _enabled;
            set
            {
                if (value == _enabled)
                {
                    return;
                }

                _enabled = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsDirty));
            }
        }

        private async void UpdateUser(object obj)
        {
            IsUpdatingUser = true;
            var update = await GetManagerByZoneId().UserService
                .UpdateUser(DisplayName, Enabled, UserId, Username);
            if (update != null)
            {
                Descriptor = this.ConvertTo<UserDetails>();
            }

            IsEditable = false;
            IsUpdatingUser = false;
        }


        private async void ClearSessions(object obj)
        {
            IsClearingSessions = true;
            if (Id != null && Id != Guid.Empty)
            {
                var revokeToken = await GetManagerByZoneId().UserService.RevokeToken(Id.Value);
                Console.WriteLine($"Token Revoke: {revokeToken.Success}");
            }

            IsClearingSessions = false;
        }

        private bool CanChangePassword()
        {
            return true;
        }

        private async void ChangePassword()
        {
            var management = ObjectResolver.ResolveAll<ManagementViewModel>()
                .FirstOrDefault(o => o.View.TabIsFocused());
            if (management != null)
            {
                var dialog = new ChangeAdminUserPasswordDialog();

                if (UserId != null)
                {
                    dialog.Model.DialogItem.UserId = UserId.Value;
                }

                if (management.View != null)
                {
                    dialog.SetModalView(management.View.DialogOverlayGrid.ModalContentPresenter);
                }

                dialog.Closed += async (sender, args) =>
                {
                    if (dialog.DialogResult)
                    {
                        if (dialog != null && !string.IsNullOrEmpty(dialog.Model?.DialogItem?.NewPassword) &&
                            !string.IsNullOrEmpty(dialog.Model?.DialogItem?.RepeatPassword))
                        {
                            try
                            {
                                //var item = dialog.Model?.DialogItem;
                                //await ServiceManagers[HARD_CODED_ZONE_ID].UserService
                                //    .ChangeUserPassword(item.NewPassword, item.CurrentPassword);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }
                        }
                    }
                };
            }
            else
            {
                var dialog = new ChangeUserPasswordDialog();
                if (dialog.ShowDialog().HasValue && dialog.Result)
                {
                    if (dialog != null && !string.IsNullOrEmpty(dialog.Model?.DialogItem?.NewPassword) &&
                        !string.IsNullOrEmpty(dialog.Model?.DialogItem?.RepeatPassword))
                    {
                        try
                        {
                            var item = dialog.Model?.DialogItem;
                            await GetManagerByZoneId().UserService
                                .ChangeUserPassword(item.NewPassword, item.CurrentPassword);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                }
            }
        }

        private bool CanView()
        {
            return true;
        }

        private void ViewUser()
        {
            var window = WindowUtil.ShowWindow<UserAuditWindow>();
            window.AuditView.GetModel().SetUser(this);
        }

        private bool CanDelete()
        {
            return true;
        }

        public event Action UserDeleted;

        private async void DeleteUser()
        {
            if (Descriptor != null)
            {
                var dialog =
                    await DialogUtil.EmbeddedModalConfirmDialog(
                        TabUtil.FindFocusedTab<ManagementTab>().ManagementView.DialogOverlayGrid.ModalContentPresenter,
                        new EmbeddedDialogParameters
                        {
                            DialogButtons = EDialogButtons.YesNo,
                            Description = $"Do you wish to delete the user?\n\"{Descriptor.Username}\""
                        });
                if (dialog)
                {
                    try
                    {
                        var resp = await GetManagerByZoneId().UserService.DeleteUser(Descriptor);
                        _log?.Info($"{resp}");
                        UserDeleted?.Invoke();
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
        }

        public static implicit operator UserItemModel(UserDetails details)
        {
            var item = details.ConvertTo<UserItemModel>();
            if (item != null)
            {
                item.Descriptor = details;
                return item;
            }

            return default;
        }

        public void AddToGroup(GroupItemModel selectedGroup)
        {
            selectedGroup.Members.Add(UserId);
            if (selectedGroup.MemberDetails == null)
            {
                selectedGroup.MemberDetails = new List<GroupMemberDetails>();
            }

            selectedGroup.MemberDetails.Add(new GroupMemberDetails
            {
                MemberId = UserId,
                Name = selectedGroup.GroupName,
                Type = "user"
            });
            selectedGroup.UsersDirty = true;

            //selectedGroup.Members.Add(UserId);
            //var addGroup = await GroupsService.UpdateGroup(selectedGroup.ConvertTo<Group>());
        }

        public void RemoveFromGroup(GroupItemModel selectedGroup)
        {
            if (selectedGroup.Members.Contains(UserId))
            {
                selectedGroup.Members.Remove(UserId);
            }

            if (selectedGroup.MemberDetails == null)
            {
                selectedGroup.MemberDetails = new List<GroupMemberDetails>();
            }

            var existingMember = selectedGroup.MemberDetails.FirstOrDefault(o => o.MemberId == UserId);
            if (existingMember != null)
            {
                selectedGroup.MemberDetails.Remove(existingMember);
            }

            selectedGroup.UsersDirty = true;
        }
    }
}