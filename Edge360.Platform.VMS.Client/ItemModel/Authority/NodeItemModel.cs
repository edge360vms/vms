﻿using System;
using System.Reflection;
using Edge360.Platform.VMS.Client.ItemModel.DataObject;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Nodes;
using log4net;
using Newtonsoft.Json;

namespace Edge360.Platform.VMS.Client.ItemModel.Authority
{
    public class NodeItemModel : NodeObject
    {
        private RelayCommand _addCommand;
        private RelayCommand _deleteCommand;
        private string _fullPath;
        private bool _isExpanded;
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private PermissionItemModel _permissions = new PermissionItemModel();

        public RelayCommand AddCommand => _addCommand ??= new RelayCommand(p => Add(), p => CanAdd());

        public RelayCommand DeleteCommand =>
            _deleteCommand ??= new RelayCommand(p => Delete(), p => CanDelete());

        public NodeDescriptor Descriptor { get; set; }

        public bool IsExpanded
        {
            get => _isExpanded;
            set
            {
                if (_isExpanded != value)
                {
                    _isExpanded = value;
                    OnPropertyChanged();
                }
            }
        }

        [JsonIgnore] public DateTimeOffset LastCachedTime { get; set; }

        public string Path { get; set; }

        public PermissionItemModel Permissions
        {
            get => _permissions;
            set
            {
                if (Equals(value, _permissions))
                {
                    return;
                }

                _permissions = value;
                OnPropertyChanged();
            }
        }

        public NodeItemModel(NodeDescriptor desc)
        {
            Descriptor = desc;
            ObjectId = desc.Id;
            ParentId = desc.ParentId;
            Label = desc.Label;
            ZoneId = desc.ZoneId;
            Type = desc.Type;
            LastCachedTime = DateTimeOffset.UtcNow;
        }

        private async void Add()
        {
            var input = DialogUtil.InputDialog("Please enter a label for the new Folder.");
            if (input.Result && !string.IsNullOrEmpty(input.Response))
            {
                try
                {
                    await NodeObjectItemManager.NodeManager.CreateNode(new NodeDescriptor
                    {
                        Label = input.Response,
                        ParentId = ObjectId,
                        Type = "Node"
                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private bool CanAdd()
        {
            return true;
        }

        public async void Delete()
        {
            if (Descriptor != null)
            {
                if (DialogUtil.ConfirmDialog($"Do you wish to delete the node?\n{Descriptor.Label}", true))
                {
                    try
                    {
                        await NodeObjectItemManager.NodeManager.RemoveNode(false, (Guid) Descriptor.Id);
                    }
                    catch (Exception e)
                    {
                        _log?.Info(e);
                    }
                }
            }
        }

        private bool CanDelete()
        {
            return true;
        }

        public override string ToString()
        {
            return $"{base.ToString()}";
        }

        public async void UpdateNodeLabel()
        {
            var newDescriptor = Descriptor;
            newDescriptor.Label = Label;
            await NodeObjectItemManager.NodeManager.UpdateNodeLabel(newDescriptor);
        }
    }
}