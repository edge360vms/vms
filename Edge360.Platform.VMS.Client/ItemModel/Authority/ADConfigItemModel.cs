﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.View.Control.Dialog;
using Edge360.Platform.VMS.Client.View.Control.Management;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.AdConfigZone;
using ServiceStack;
using Telerik.Windows.Controls;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.ItemModel.Authority
{
    public class ADConfigItemModel : IAdConfigZone, INotifyPropertyChanged
    {
        private RelayCommand _addDomainCommand;
        private EConnectionStatus _connectionStatus;
        private RelayCommand _deleteConfigCommand;
        private bool _isTestingConnection;
        private RelayCommand _testConnectionCommand;

        public RelayCommand AddDomainCommand =>
            _addDomainCommand ??= new RelayCommand(p => AddDomain(p), p => CanAddDomain());


        public EConnectionStatus ConnectionStatus
        {
            get => _connectionStatus;
            set
            {
                if (_connectionStatus != value)
                {
                    _connectionStatus = value;
                    OnPropertyChanged();
                }
            }
        }

        public RelayCommand DeleteConfigCommand =>
            _deleteConfigCommand ??= new RelayCommand(p => DeleteConfig());

        public bool IsTestingConnection
        {
            get => _isTestingConnection;
            set
            {
                if (_isTestingConnection != value)
                {
                    _isTestingConnection = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(TestConnectionCommand));
                    // CommandManager.InvalidateRequerySuggested();
                }
            }
        }


        public RelayCommand TestConnectionCommand => _testConnectionCommand ??= new RelayCommand(
            async o => await TestConnectionPrompt(),
            o => CanTestConnection());

        public string Urls { get; set; }
        public Guid? ActiveDirectoryId { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public Guid? ZoneId { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public async Task<EConnectionStatus> TestConnection()
        {
            return await Task.Run(async () =>
            {
                IsTestingConnection = true;
                var checkConnection = await GetManagerByZoneId().AdConfigZoneService
                    .TestConfigZone(ZoneId, ActiveDirectoryId);
                ConnectionStatus = checkConnection != null && checkConnection.Success
                    ? EConnectionStatus.Connected
                    : EConnectionStatus.Timeout;
                IsTestingConnection = false;
                return ConnectionStatus;
            });
        }

        public async Task TestConnectionPrompt()
        {
            var reply = await TestConnection();
            Console.WriteLine($"TestConnectionPrompt {reply}");
            // RadWindow.Alert($"Test Connection Response {reply}", null);
        }

        private bool CanTestConnection()
        {
            return !IsTestingConnection;
        }

        private bool CanAddDomain()
        {
            return true;
        }

        private void AddDomain(object obj)
        {
            if (obj is ActiveDirectoryManagementViewModel viewModel)
            {
                var management = viewModel.View.ParentOfType<ManagementView>();
                if (management != null)
                {
                    var dialog = new CreateAdDomainDialog();

                    dialog.SetModalView(management.DialogOverlayGrid.ModalContentPresenter);

                    dialog.Closed += async (sender, args) =>
                    {
                        if (dialog.Result &&
                            dialog.Model?.DialogItem is CreateAdDomainDialogItemModel dialogItem &&
                            !string.IsNullOrEmpty(dialogItem.Domain))
                        {
                            await GetManagerByZoneId().AdDomainService
                                .CreateDomain(dialogItem.Domain, ActiveDirectoryId ?? Guid.Empty);
                        }
                    };
                }
            }
        }

        public static implicit operator ADConfigItemModel(AdConfigZone config)
        {
            return config.ConvertTo<ADConfigItemModel>();
        }

        private async void DeleteConfig()
        {
            var confirmed = DialogUtil.ConfirmDialog("Are you sure you wish to delete this Config?", true);
            if (confirmed)
            {
                var delete = await GetManagerByZoneId().AdConfigZoneService
                    .DeleteConfigById(Guid.Empty, ActiveDirectoryId);
                Console.WriteLine($"DeleteConfig: {delete}");
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum EConnectionStatus
    {
        None,
        Timeout,
        Unauthorized,
        Connected
    }
}