﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Groups;
using ServiceStack;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.ItemModel.Authority
{
    public class GroupItemModel : IGroup, INotifyPropertyChanged
    {
        private bool _adGroupsDirty;
        private RelayCommand _deleteGroupCommand;
        private List<GroupMemberDetails> _originalMemberDetails;
        private List<Guid?> _originalMembers;
        private RelayCommand _resetUsersCommand;
        private RelayCommand _saveAdGroupsCommand;
        private RelayCommand _saveUsersCommand;
        private bool _usersDirty;
        private Guid? _zoneId;

        public List<GroupMemberDetails> AdGroups
        {
            get
            {
                var adGroups = new List<GroupMemberDetails>();
                if (MemberDetails != null)
                {
                    adGroups.AddRange(MemberDetails.Where(o => o.Type == "adgroup"));
                }

                return adGroups;
            }
        }

        public bool AdGroupsDirty
        {
            get => _adGroupsDirty;
            set
            {
                if (_adGroupsDirty != value)
                {
                    _adGroupsDirty = value;
                    Trace.WriteLine($"GroupItemModel 54 InvalidateRequerySuggested");
                    //CommandManager.InvalidateRequerySuggested();
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(SaveAdGroupsCommand));
                }
            }
        }

        public RelayCommand DeleteGroupCommand =>
            _deleteGroupCommand ??= new RelayCommand(p => DeleteGroup());

        public List<GroupMemberDetails> OriginalMemberDetails
        {
            get => _originalMemberDetails;
            set
            {
                if (Equals(value, _originalMemberDetails))
                {
                    return;
                }

                _originalMemberDetails = value;
                OnPropertyChanged();
            }
        }

        public List<Guid?> OriginalMembers
        {
            get => _originalMembers;
            set
            {
                if (Equals(value, _originalMembers))
                {
                    return;
                }

                _originalMembers = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand SaveAdGroupsCommand =>
            _saveAdGroupsCommand ??= new RelayCommand(p => SaveGroup(), p => AdGroupsDirty);


        public RelayCommand SaveUsersCommand =>
            _saveUsersCommand ??= new RelayCommand(p => SaveGroup(), p => UsersDirty);


        public List<GroupMemberDetails> Users
        {
            get
            {
                var users = new List<GroupMemberDetails>();
                if (MemberDetails != null)
                {
                    users.AddRange(MemberDetails.Where(o => o.Type == "user"));
                }

                return users;
            }
        }

        public bool UsersDirty
        {
            get => _usersDirty;
            set
            {
                if (_usersDirty != value)
                {
                    _usersDirty = value;
                    //CommandManager.InvalidateRequerySuggested();
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(SaveAdGroupsCommand));
                    OnPropertyChanged(nameof(SaveUsersCommand));
                }
            }
        }

        public Guid? ZoneId
        {
            get => _zoneId;
            set
            {
                if (Nullable.Equals(value, _zoneId))
                {
                    return;
                }

                _zoneId = value;
                OnPropertyChanged();
            }
        }

        public Guid? GroupId { get; set; }
        public string GroupName { get; set; }
        public List<Guid?> Members { get; set; } = new List<Guid?>();
        public List<GroupMemberDetails> MemberDetails { get; set; } = new List<GroupMemberDetails>();

        public event PropertyChangedEventHandler PropertyChanged;

        private async void SaveGroup()
        {
            try
            {
                var convertTo = this.ConvertTo<Group>();
                var groupMembers = new List<IGroupMember>(MemberDetails);
                var resp = await GetManagerByZoneId().GroupsService
                    .UpdateGroup(convertTo,
                        groupMembers);
                if (resp != null && resp.Success)
                {
                    UsersDirty = false;
                    AdGroupsDirty = false;

                    Console.WriteLine("Group saving succeeded!");
                }
            }
            catch (Exception e)
            {
                
            }
        }

        public static implicit operator GroupItemModel(Group group)
        {
            var groupConverted = group.ConvertTo<GroupItemModel>();
            groupConverted.ZoneId = ConnectedZoneId;
            groupConverted.OriginalMembers = new List<Guid?>(groupConverted.Members);
            groupConverted.OriginalMemberDetails = new List<GroupMemberDetails>(groupConverted.MemberDetails);
            return groupConverted;
        }

        private async void DeleteGroup()
        {
            var modalContentPresenter = ObjectResolver.ResolveAll<ManagementTab>()
                ?.FirstOrDefault(o => o != null && o.TabIsFocused())
                ?.ManagementView?.DialogOverlayGrid?.ModalContentPresenter;
            var resp = await DialogUtil.EmbeddedModalConfirmDialog(modalContentPresenter, new EmbeddedDialogParameters
            {
                DialogButtons = EDialogButtons.YesNo,
                Description = $"Are you sure you wish to delete {GroupName}?",
            });
            if (resp)
            {
                try
                {
                    var delete = await GetManagerByZoneId().GroupsService
                        .DeleteGroupById(GroupId);
                    Console.WriteLine($"DeleteGroup: {delete}");
                }
                catch (Exception e)
                {
                }
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}