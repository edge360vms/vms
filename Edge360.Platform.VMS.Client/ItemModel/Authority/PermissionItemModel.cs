﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Permissions;
using Edge360.Vms.AuthorityService.SharedModel.Enums;
using Prism.Commands;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.ItemModel.Authority
{
    public class PermissionItemModel : IGroupPermissionDescriptor, INotifyPropertyChanged
    {
        private EPermissionsCamera _cameraPermission;

        private string _groupId;
        private string _objectId;
        private int _value;

        public EPermissionsCamera CameraPermission => (EPermissionsCamera) Value;

        public EPermissionsFolder FolderPermission => (EPermissionsFolder) Value;

        public EPermissionsLayout LayoutPermission => (EPermissionsLayout) Value;

        public string GroupId
        {
            get => _groupId;
            set
            {
                if (value == _groupId)
                {
                    return;
                }

                _groupId = value;
                OnPropertyChanged();
            }
        }

        public string ObjectId
        {
            get => _objectId;
            set
            {
                if (value == _objectId)
                {
                    return;
                }

                _objectId = value;
                OnPropertyChanged();
            }
        }

        public int Value
        {
            get => _value;
            set
            {
                if (value == _value)
                {
                    return;
                }

                _value = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(CameraPermission));
                OnPropertyChanged(nameof(FolderPermission));
                OnPropertyChanged(nameof(LayoutPermission));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return $"{nameof(GroupId)}: {GroupId}, {nameof(ObjectId)}: {ObjectId}, {nameof(Value)}: {Value}";
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class AvailableLayoutPermissionItemModel : INotifyPropertyChanged
    {
        private DelegateCommand<object> _checkPermission;

        private bool _isChecked;
        private EPermissionsLayout _layoutPermission;

        public static List<EPermissionsLayout> PermissionList => Enum.GetValues(typeof(EPermissionsLayout))
            .OfType<EPermissionsLayout>().ToList().RemoveFirstAndLast();

        public DelegateCommand<object> CheckPermissionCommand =>
            _checkPermission ??= new DelegateCommand<object>(Check);

        public bool IsChecked
        {
            get => _isChecked;
            set
            {
                if (value == _isChecked)
                {
                    return;
                }

                _isChecked = value;
                OnPropertyChanged();
            }
        }

        public EPermissionsLayout LayoutPermission
        {
            get => _layoutPermission;
            set
            {
                if (value == _layoutPermission)
                {
                    return;
                }

                _layoutPermission = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Check(object obj)
        {
            try
            {
                if (obj is RadComboBox comboBox)
                {
                    var gridView = comboBox.GetVisualParent<RadGridView>();
                    if (gridView != null)
                    {
                        var selectedCameras = gridView.SelectedItems.Select(o =>
                        {
                            dynamic keyPair = o;
                            return keyPair.Value;
                        }).OfType<LayoutItemModel>().ToList();

                        foreach (var p in selectedCameras)
                        {
                            var permissionValue = (EPermissionsLayout) p.Permissions.Value;

                            if (IsChecked)
                            {
                                permissionValue |= LayoutPermission;
                            }
                            else
                            {
                                permissionValue &= ~LayoutPermission;
                            }

                            p.Permissions.Value = (int) permissionValue;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class AvailableCameraPermissionItemModel : INotifyPropertyChanged
    {
        private EPermissionsCamera _cameraPermission;
        private DelegateCommand<object> _checkPermission;
        private bool _isChecked;

        public static List<EPermissionsCamera> PermissionList => Enum.GetValues(typeof(EPermissionsCamera))
            .OfType<EPermissionsCamera>().ToList().RemoveFirstAndLast();

        public EPermissionsCamera CameraPermission
        {
            get => _cameraPermission;
            set
            {
                if (value == _cameraPermission)
                {
                    return;
                }

                _cameraPermission = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand<object> CheckPermissionCommand =>
            _checkPermission ??= new DelegateCommand<object>(Check);

        public bool IsChecked
        {
            get => _isChecked;
            set
            {
                if (value == _isChecked)
                {
                    return;
                }

                _isChecked = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Check(object obj)
        {
            try
            {
                if (obj is RadComboBox comboBox)
                {
                    var gridView = comboBox.GetVisualParent<RadGridView>();
                    if (gridView != null)
                    {
                        var selectedCameras = gridView.SelectedItems.Select(o =>
                        {
                            dynamic keyPair = o;
                            return keyPair.Value;
                        }).OfType<CameraItemModel>().ToList();

                        foreach (var p in selectedCameras)
                        {
                            var permissionValue = (EPermissionsCamera) p.Permissions.Value;

                            if (IsChecked)
                            {
                                permissionValue |= CameraPermission;
                            }
                            else
                            {
                                permissionValue &= ~CameraPermission;
                            }

                            p.Permissions.Value = (int) permissionValue;
                        }
                    }

                    //

                    //foreach (var p in cameraPermissionItemModels)
                    //{ 
                    //    p.IsChecked = selectedCameras.Any(o =>
                    //        ((EPermissionsCamera) o.Permissions.Value).HasFlag(p.CameraPermission));
                    //}
                }
            }
            catch (Exception exception)
            {
            }

            //if (obj is IEnumerable<CameraItemModel> cameras)
            //{
            //    foreach (var p in cameras)
            //    {
            //        var permissionValue = (EPermissionsCamera)p.Permissions.Value;
            //        permissionValue ^= CameraPermission;
            //        p.Permissions.Value = (int)permissionValue;
            //    }
            //}
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }


    public class AvailableFolderPermissionItemModel : INotifyPropertyChanged
    {
        private DelegateCommand<object> _checkPermission;


        private EPermissionsFolder _folderPermission;
        private bool _isChecked;

        public static List<EPermissionsFolder> PermissionList => Enum.GetValues(typeof(EPermissionsFolder))
            .OfType<EPermissionsFolder>().ToList().RemoveFirstAndLast();

        public DelegateCommand<object> CheckPermissionCommand =>
            _checkPermission ??= new DelegateCommand<object>(Check);

        public EPermissionsFolder FolderPermission
        {
            get => _folderPermission;
            set
            {
                if (value == _folderPermission)
                {
                    return;
                }

                _folderPermission = value;
                OnPropertyChanged();
            }
        }

        public bool IsChecked
        {
            get => _isChecked;
            set
            {
                if (value == _isChecked)
                {
                    return;
                }

                _isChecked = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Check(object obj)
        {
            try
            {
                if (obj is RadComboBox comboBox)
                {
                    var gridView = comboBox.GetVisualParent<RadGridView>();
                    if (gridView != null)
                    {
                        var selectedCameras = gridView.SelectedItems.Select(o =>
                        {
                            dynamic keyPair = o;
                            return keyPair.Value;
                        }).OfType<NodeItemModel>().ToList();

                        foreach (var p in selectedCameras)
                        {
                            var permissionValue = (EPermissionsFolder) p.Permissions.Value;

                            if (IsChecked)
                            {
                                permissionValue |= FolderPermission;
                            }
                            else
                            {
                                permissionValue &= ~FolderPermission;
                            }

                            p.Permissions.Value = (int) permissionValue;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}