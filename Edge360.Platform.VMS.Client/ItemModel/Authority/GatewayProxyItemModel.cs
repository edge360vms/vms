﻿namespace Edge360.Platform.VMS.Client.ItemModel.Authority
{
    public class GatewayProxyItemModel
    {
        public string Address { get; set; }
        public short AddressPort { get; set; }
        public short ListeningPort { get; set; }
        public string PreviewUrl { get; set; }
        public bool UseAuthentication { get; set; }
    }
}