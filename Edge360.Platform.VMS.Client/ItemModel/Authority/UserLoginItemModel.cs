﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.View.Control.Dialog;
using Edge360.Platform.VMS.Client.View.Window;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Interfaces.Authority;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.UI.Common.Util;
using Newtonsoft.Json;

namespace Edge360.Platform.VMS.Client.ItemModel.Authority
{
    public enum EUserActionCommand
    {
        Logout,
        ClearSessions,
        ChangePassword
    }

    [Serializable]
    public class UserLoginItemModel : UserLogin, INotifyPropertyChanged
    {
        public UserLoginItemModel()
        {
            
        }

        public void Initialize()
        {
            HasInitialized = true;
        }

        public bool HasInitialized
        {
            get => _hasInitialized;
            set
            {
                if (value == _hasInitialized) return;
                _hasInitialized = value;
                OnPropertyChanged();
            }
        }

        private ushort _apiPort;
        private string _bearerToken;

        private string _domain;
        private bool _ignoreValidation;
        private bool _isBearerLogin;

        private ObservableCollection<string> _loginServers =
            new ObservableCollection<string>(SettingsManager.Settings.Login.LoginServers);

        private string _password;
        private string _refreshToken;
        private RelayCommand _userActionCommand;
        private string _userName;
        private bool _isWindowsLogin;
        private string _windowsId;
        private ObservableCollection<string> _adLoginServers;
        private bool _hasInitialized;

        public override ushort ApiPort
        {
            get => _apiPort;
            set
            {
                if (value == _apiPort)
                {
                    return;
                }

                _apiPort = value;
                OnPropertyChanged();
            }
        }

        public override string BearerToken
        {
            get => _bearerToken;
            set
            {
                if (value == _bearerToken)
                {
                    return;
                }

                _bearerToken = value;
                OnPropertyChanged();
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Address is required.")]
        public override string Domain
        {
            get => _domain;
            set
            {
                if (_domain != value)
                {
                    _domain = value;

                    //Console.WriteLine($"Domain changed... {_domain}");
                    if (HasInitialized)
                    {
                        ClearTokenLogin();
                        ClearBlankPassword();
                    }

                    OnPropertyChanged();
                    if (!IgnoreValidation)
                    {
                        ValidateField(value);
                    }
                }
            }
        }

        private void ClearBlankPassword()
        {
            try
            {
                if (Password == "**********")
                {
                    Password = string.Empty;
                }
            }
            catch (Exception e)
            {
            }
        }

        public override bool IsBearerLogin
        {
            get => _isBearerLogin;
            set
            {
                if (value == _isBearerLogin)
                {
                    return;
                }

                _isBearerLogin = value;
                OnPropertyChanged();
            }
        }

        [MinLength(5, ErrorMessage = "Password must be 5 characters or more.")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Password is required.")]
        [JsonIgnore]
        public override string Password
        {
            get => _password;
            set
            {
                if (_password != value)
                {
                    _password = value;
                    ClearTokenLogin();
                    OnPropertyChanged();
                    if (!TabbedWindow.IsForceClosing && !IgnoreValidation && !IsWindowsLogin)
                    {
                        ValidateField(value);
                    }
                }
            }
        }

        public override string RefreshToken
        {
            get => _refreshToken;
            set
            {
                if (value == _refreshToken)
                {
                    return;
                }

                _refreshToken = value;
                OnPropertyChanged();
            }
        }


        [DisplayName("Remember Me")] public override bool RememberMe { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Username is required.")]
        public override string UserName
        {
            get => _userName;
            set
            {
                if (_userName != value)
                {
                    ClearTokenLogin();
                    _userName = value;

                    OnPropertyChanged();
                    if (!IgnoreValidation)
                    {
                        ValidateField(value);
                    }
                }
            }
        }

        [JsonIgnore] public UserItemModel CurrentUser => CommunicationManager.CurrentUser;

        public bool IgnoreValidation
        {
            get => _ignoreValidation;
            set
            {
                if (value == _ignoreValidation)
                {
                    return;
                }

                _ignoreValidation = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<string> AdLoginServers
        {
            get => _adLoginServers;
            set
            {
                if (Equals(value, _adLoginServers))
                {
                    return;
                }

                _adLoginServers = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<string> LoginServers
        {
            get => _loginServers;
            set
            {
                if (Equals(value, _loginServers))
                {
                    return;
                }

                _loginServers = value;
                OnPropertyChanged();
            }
        }

        public override bool IsWindowsLogin
        {
            get => _isWindowsLogin;
            set
            {
                if (value == _isWindowsLogin) return;
                _isWindowsLogin = value;
                OnPropertyChanged();
            }
        }

        public override string WindowsId
        {
            get => _windowsId;
            set
            {
                if (value == _windowsId) return;
                _windowsId = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void ClearTokenLogin()
        {
            IgnoreValidation = false;
            // changing these will get saved..
            // RefreshToken = string.Empty;
            // BearerToken = string.Empty;
            IsBearerLogin = false;
            
        }


        public event Action<string, string> Validation;

        private void ValidateField(object obj, [CallerMemberName] string memberName = null)
        {
            try
            {
                this.TryValidate(obj, memberName);
                Validation?.Invoke(memberName, "");
                OnPropertyChanged(memberName);
            }
            catch (ValidationException e)
            {
                Validation?.Invoke(memberName, e.Message);
                OnPropertyChanged(memberName);
                throw;
            }
        }


        public async Task ChangePassword()
        {
            var dialog = new ChangeUserPasswordDialog();
            if (dialog.ShowDialog().HasValue && dialog.Result)
            {
                if (dialog != null && !string.IsNullOrEmpty(dialog.Model?.DialogItem?.NewPassword) &&
                    !string.IsNullOrEmpty(dialog.Model?.DialogItem?.RepeatPassword))
                {
                    try
                    {
                        //var item = dialog.Model?.DialogItem;
                        //await ServiceManagers[HARD_CODED_ZONE_ID].UserService
                        //    .ChangeUserPassword(item.NewPassword, item.CurrentPassword);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}