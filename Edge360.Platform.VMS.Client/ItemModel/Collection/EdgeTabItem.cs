﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.UI.Common.Util;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.ItemModel.Collection
{
    public class EdgeTabItem : RadTabItem, INotifyPropertyChanged, IDisposable
    {
        private string _iconGlyph;

        public static readonly DependencyProperty LeftStatusContentProperty = DependencyProperty.Register(
            "LeftStatusContent", typeof(object), typeof(EdgeTabItem), new PropertyMetadata(default(object)));

        public object LeftStatusContent
        {
            get { return (object) GetValue(LeftStatusContentProperty); }
            set { SetValue(LeftStatusContentProperty, value); }
        }


        public static readonly DependencyProperty RightStatusContentProperty = DependencyProperty.Register(
            "RightStatusContent", typeof(object), typeof(EdgeTabItem), new PropertyMetadata(default(object)));

        public object RightStatusContent
        {
            get { return (object) GetValue(RightStatusContentProperty); }
            set { SetValue(RightStatusContentProperty, value); }
        }

        public string IconFont { get; set; } = "TelerikWebUI";

        public string IconGlyph
        {
            get => _iconGlyph;
            set
            {
                if (_iconGlyph != value)
                {
                    _iconGlyph = value;
                    OnPropertyChanged();
                }
            }
        }

        public int IconSize { get; set; } = 22;

        public bool IsDisposed { get; set; }

        public EdgeTabItem()
        {
            DataContext = this;
        }

        public virtual void Dispose()
        {
            try
            {
                //this.TryUiAsync(() =>
                //{
                if (Content != null && Content is UIElement ui)
                {
                    foreach (var u in ui.ChildrenOfType<UIElement>())
                    {
                        if (u is IDisposable d)
                        {
                            d.Dispose();
                        }
                    }
                }
                //});

                IsDisposed = true;
            }
            catch (Exception e)
            {
                
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool FocusTab(EdgeTabItem edgeTab)
        {
            var parent = this.ParentOfType<RadTabbedWindow>();
            if (parent != null)
            {
                if (parent.WindowState == WindowState.Minimized)
                {
                    parent.WindowState = WindowState.Normal;
                }

                parent.BringToFront();

                parent.SelectedItem = edgeTab;
                return true;
            }

            return false;
        }

        public virtual bool CloseConfirm()
        {
            if (FocusTab(this))
            {
                var dialog =
                    DialogUtil.ConfirmDialog($"Are you sure you wish to close {Header}?\nYou will lose your changes.",
                        true);
                if (dialog)
                {
                    Dispose();
                    return true;
                }
            }

            return false;
        }

        public virtual bool CheckClose()
        {
            return true;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}