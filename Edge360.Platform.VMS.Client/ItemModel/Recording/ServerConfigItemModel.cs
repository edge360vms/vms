using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Dialog;
using Edge360.Platform.VMS.Client.View.Control.Management;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Common.Enums.Management;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Nodes;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Servers;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Cameras;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.Server;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.Volumes;
using ServiceStack;
using static Edge360.Platform.VMS.Communication.CommunicationManager;
using Size = System.Drawing.Size;

namespace Edge360.Platform.VMS.Client.ItemModel.Recording
{
    public class ServerConfigItemModel : IServer, INotifyPropertyChanged
    {
        private ObservableCollection<CameraItemModel> _activeCameras = new ObservableCollection<CameraItemModel>();
        private RelayCommand _archiverActionCommand;
        private string _archiverDescription;

        private ObservableCollection<RecordingProfileItemModel> _archivingProfiles =
            new ObservableCollection<RecordingProfileItemModel>();

        private bool _automaticCleanup;
        private int _automaticCleanupAfterDays = 30;
        private string _cameraFilterText = string.Empty;
        private EConnectionStatus _connectionStatus = EConnectionStatus.Connected;
        private Tuple<Size, Size> _defaultRecordingResolutionRange;

        private ObservableCollection<Tuple<Size, Size>> _defaultRecordingResolutionRanges =
            new ObservableCollection<Tuple<Size, Size>>();

        private int _defaultVideoRecordingFramerate;
        private CancellationTokenSource _filterCts;
        private string _hostName;
        private bool _isDirty;
        private bool _isEditable;
        private bool _isRefreshingAll;
        private bool _isRefreshingCameras;
        private bool _isRefreshingParents;
        private bool _isRefreshingVolumes;
        private string _label;
        private ObservableCollection<NodeItemModel> _parentObjects = new ObservableCollection<NodeItemModel>();
        private int _portApi;
        private int _portArchive;
        private int _portArchiveControl;
        private int _portLive;
        private int _portSso;
        private bool _recordAudio;
        private RelayCommand _refreshAllCommand;
        private RelayCommand _refreshCamerasCommand;
        private RelayCommand _refreshParentsCommand;
        private RelayCommand _refreshVolumesCommand;
        private VolumeItemModel _selectedVolume;
        private Guid _serverId;
        private string _serverName;
        private ObservableCollection<VolumeItemModel> _volumes = new ObservableCollection<VolumeItemModel>();
        private Guid _zoneId;

        //public CollectionViewSource ActiveCameraCollectionViewSource { get; set; }

        public ObservableCollection<CameraItemModel> ActiveCameras
        {
            get => _activeCameras;
            set
            {
                if (_activeCameras != value)
                {
                    _activeCameras = value;
                    OnPropertyChanged();
                }
            }
        }

        public RelayCommand ArchiverActionCommand =>
            _archiverActionCommand ??= new RelayCommand(ArchiverAction, CanExecute);

        public string ArchiverDescription
        {
            get => _archiverDescription;
            set
            {
                if (_archiverDescription != value)
                {
                    _archiverDescription = value;
                    OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<RecordingProfileItemModel> ArchivingProfiles
        {
            get => _archivingProfiles;
            set
            {
                if (_archivingProfiles != value)
                {
                    _archivingProfiles = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool AutomaticCleanup
        {
            get => _automaticCleanup;
            set
            {
                if (_automaticCleanup != value)
                {
                    _automaticCleanup = value;
                    OnPropertyChanged();
                }
            }
        }

        public int AutomaticCleanupAfterDays
        {
            get => _automaticCleanupAfterDays;
            set
            {
                if (_automaticCleanupAfterDays != value)
                {
                    _automaticCleanupAfterDays = value;
                    OnPropertyChanged();
                }
            }
        }

        public EConnectionStatus ConnectionStatus
        {
            get => _connectionStatus;
            set
            {
                if (_connectionStatus != value)
                {
                    _connectionStatus = value;
                    OnPropertyChanged();
                }
            }
        }

        public string DatabaseName { get; set; }
        public string DatabaseServer { get; set; }

        public Tuple<Size, Size> DefaultRecordingResolutionRange
        {
            get => _defaultRecordingResolutionRange;
            set
            {
                if (!Equals(_defaultRecordingResolutionRange, value))
                {
                    _defaultRecordingResolutionRange = value;
                    OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<Tuple<Size, Size>> DefaultRecordingResolutionRanges
        {
            get => _defaultRecordingResolutionRanges;
            set
            {
                if (_defaultRecordingResolutionRanges != value)
                {
                    _defaultRecordingResolutionRanges = value;
                    OnPropertyChanged();
                }
            }
        }

        public int DefaultVideoRecordingFramerate
        {
            get => _defaultVideoRecordingFramerate;
            set
            {
                if (_defaultVideoRecordingFramerate != value)
                {
                    _defaultVideoRecordingFramerate = value;
                    OnPropertyChanged();
                }
            }
        }

        //public static readonly DependencyProperty ZoneViewProperty = DependencyProperty.RegisterAttached(
        //    "ZoneView", typeof(ZoneManagementView), typeof(ArchiveItemModel), new PropertyMetadata(default(ZoneManagementView)));

        //public static void SetZoneView(DependencyObject element, ZoneManagementView value)
        //{
        //    element.SetValue(ZoneViewProperty, value);
        //}

        //public static ZoneManagementView GetZoneView(DependencyObject element)
        //{
        //    return (ZoneManagementView) element.GetValue(ZoneViewProperty);
        //}


        public ZoneCameraManagementViewModel GetZoneCameraViewModel => ObjectResolver
            .ResolveAll<ZoneCameraManagementViewModel>()
            ?.FirstOrDefault(o => o?.SelectedServer?.Archiver == this);

        public bool IsDirty => true; // ZoneDescriptor.ZoneId != ZoneId || ZoneDescriptor.Label != Label;

        public bool IsEditable
        {
            get => _isEditable;
            set
            {
                if (value == _isEditable)
                {
                    return;
                }

                _isEditable = value;
                if (!_isEditable) // reset fields
                {
                    //if (ServerDescriptor.ZoneId != null)
                    //{
                    //    ZoneId = (Guid) ServerDescriptor.ZoneId;
                    //}

                    //Label = ///ServerDescriptor.Label;
                }

                OnPropertyChanged();
                //OnPropertyChanged(command);
                //CommandManager.InvalidateRequerySuggested();
            }
        }

        public bool IsRefreshingAll
        {
            get => _isRefreshingAll;
            set
            {
                if (_isRefreshingAll != value)
                {
                    _isRefreshingAll = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(RefreshAllCommand));
                    //CommandManager.InvalidateRequerySuggested();
                }
            }
        }

        public bool IsRefreshingCameras
        {
            get => _isRefreshingCameras;
            set
            {
                if (_isRefreshingCameras != value)
                {
                    _isRefreshingCameras = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool IsRefreshingParents
        {
            get => _isRefreshingParents;
            set
            {
                if (_isRefreshingParents != value)
                {
                    _isRefreshingParents = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(RefreshParentsCommand));
                    //CommandManager.InvalidateRequerySuggested();
                }
            }
        }

        public bool IsRefreshingVolumes
        {
            get => _isRefreshingVolumes;
            set
            {
                if (_isRefreshingVolumes != value)
                {
                    _isRefreshingVolumes = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(RefreshVolumesCommand));
                    //CommandManager.InvalidateRequerySuggested();
                }
            }
        }

        public string Label
        {
            get => _label;
            set
            {
                if (_label != value)
                {
                    _label = value;
                    OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<NodeItemModel> ParentObjects
        {
            get => _parentObjects;
            set
            {
                if (_parentObjects != value)
                {
                    _parentObjects = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool RecordAudio
        {
            get => _recordAudio;
            set
            {
                if (_recordAudio != value)
                {
                    _recordAudio = value;
                    OnPropertyChanged();
                }
            }
        }

        public RelayCommand RefreshAllCommand =>
            _refreshAllCommand ??= new RelayCommand(o => RefreshAll(this), o => !IsRefreshingAll);

        public RelayCommand RefreshCamerasCommand => _refreshCamerasCommand ??= new RelayCommand(
            async o => await RefreshCameras(o),
            o => !IsRefreshingCameras);

        public RelayCommand RefreshParentsCommand => _refreshParentsCommand ??= new RelayCommand(
            async o => await RefreshParents(o),
            o => !IsRefreshingParents);

        public RelayCommand RefreshVolumesCommand => _refreshVolumesCommand ??= new RelayCommand(
            async o => RefreshVolumes(this),
            o => !IsRefreshingVolumes);

        public VolumeItemModel SelectedVolume
        {
            get => _selectedVolume;
            set
            {
                if (Equals(value, _selectedVolume))
                {
                    return;
                }

                _selectedVolume = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<VolumeItemModel> Volumes
        {
            get => _volumes;
            set
            {
                if (_volumes != value)
                {
                    _volumes = value;
                    OnPropertyChanged();
                }
            }
        }

        public ServerConfigItemModel()
        {
            //if (cameraManagement)
            //{
            try
            {
                Task.Run(async () =>
                {
                    await RefreshCameras();
                    RefreshVolumes();
                    await RefreshParents();
                });
            }
            catch (Exception e)
            {
            }

            //}
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Guid ServerId
        {
            get => _serverId;
            set
            {
                if (value.Equals(_serverId))
                {
                    return;
                }

                _serverId = value;
                OnPropertyChanged();
            }
        }

        public Guid ZoneId
        {
            get => _zoneId;
            set
            {
                if (value.Equals(_zoneId))
                {
                    return;
                }

                _zoneId = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsDirty));
            }
        }

        public string ServerName
        {
            get => _serverName;
            set
            {
                if (value == _serverName)
                {
                    return;
                }

                _serverName = value;
                OnPropertyChanged();
            }
        }

        public string HostName
        {
            get => _hostName;
            set
            {
                if (value == _hostName)
                {
                    return;
                }

                _hostName = value;
                OnPropertyChanged();
            }
        }

        public int PortApi
        {
            get => _portApi;
            set
            {
                if (value == _portApi)
                {
                    return;
                }

                _portApi = value;
                OnPropertyChanged();
            }
        }

        public int PortSso
        {
            get => _portSso;
            set
            {
                if (value == _portSso)
                {
                    return;
                }

                _portSso = value;
                OnPropertyChanged();
            }
        }

        public int PortLive
        {
            get => _portLive;
            set
            {
                if (value == _portLive)
                {
                    return;
                }

                _portLive = value;
                OnPropertyChanged();
            }
        }

        public int PortArchive
        {
            get => _portArchive;
            set
            {
                if (value == _portArchive)
                {
                    return;
                }

                _portArchive = value;
                OnPropertyChanged();
            }
        }

        public int PortArchiveControl
        {
            get => _portArchiveControl;
            set
            {
                if (value == _portArchiveControl)
                {
                    return;
                }

                _portArchiveControl = value;
                OnPropertyChanged();
            }
        }

        public static implicit operator ServerConfigItemModel(ServerItemModel item)
        {
            var converted = item.ConvertTo<ServerConfigItemModel>();
            return converted;
        }

        private async void RefreshAll(object obj)
        {
            IsRefreshingAll = true;
            await Task.Run(async () =>
            {
                try
                {
                    RefreshVolumes(obj);
                    await RefreshCameras(obj);
                    await RefreshParents(obj);
                }
                catch (Exception e)
                {
                }
            });
            IsRefreshingAll = false;
        }

        private async Task GetParentChildren(NodeItemModel node)
        {
            if (node != null)
            {
                try
                {
                    await Task.Run(async () =>
                    {
                        //  await Task.Run(async () =>
                        {
                            var nodeChildren = await NodeObjectItemManager.NodeManager.RefreshNodeChildren(
                                new ItemManagerRequestParameters
                                    {AcceptedCacheDuration = TimeSpan.FromSeconds(3), CacheDuration = TimeSpan.FromSeconds(3)},
                                node.ObjectId);
                            await App.Current?.Dispatcher?.InvokeAsync((async () =>
                            {
                                if (nodeChildren != null)
                                {
                                    foreach (var n in nodeChildren.Where(n => n.Type == null || !n.Type.StartsWith("Camera")))
                                    {
                                        // the parent node comes through?
                                        if (n.ObjectId != node.ObjectId)
                                        {
                                            var childNode = n;

                                            node.Children.Add(childNode);

                                            await GetParentChildren(childNode);
                                        }
                                    }
                                }
                            }), DispatcherPriority.Background);
                        }
                        // );
                    });
                }
                catch (Exception e)
                {
                    _log?.Info(e);
                }
            }
        }

        private async Task RefreshParents(object obj = null)
        {
            Trace.WriteLine("ArchiveItemModel RefreshParents");
            if (!IsRefreshingParents)
            {
                IsRefreshingParents = true;
                await Task.Run(async () =>
                {
                    try
                    {
                        Trace.WriteLine($"Refreshing Parents of {ZoneId}");
                        var zoneRoot =
                            await NodeObjectItemManager.NodeManager
                                .RefreshNodeChildren(
                                    new ItemManagerRequestParameters
                                    {
                                        AcceptedCacheDuration = TimeSpan.FromSeconds(30),
                                        CacheDuration = TimeSpan.FromSeconds(30)
                                    }, ZoneId);
                        if (zoneRoot != null)
                        {

                            Application.Current.Dispatcher?.InvokeAsync(async () =>
                            {
                                try
                                {
                                    var node = new NodeItemModel(new NodeDescriptor
                                    {
                                        Id = ZoneId,
                                        Label = $"Zone Root ({Label})",
                                        ParentId = Guid.Empty,
                                        Type = "ZoneRoot"
                                    });
                                    var nodeList = new List<NodeItemModel> {node};

                                    foreach (var n in zoneRoot)
                                    {
                                        var convertedNode = n;
                                        node.Children.Add(convertedNode);
                                        await GetParentChildren(convertedNode);
                                    }
                                    ParentObjects.Clear();
                                    ParentObjects.AddRange(nodeList);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                    throw;
                                }
                            }, DispatcherPriority.Background);

                            //ParentObjects = new ObservableCollection<NodeItemModel>(nodeList);
                            //OnPropertyChanged(nameof(ParentObjects));
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }).ConfigureAwait(false);
            }

            IsRefreshingParents = false;
        }

        private async Task RefreshCameras(object obj = null)
        {
            if (!IsRefreshingCameras)
            {
                IsRefreshingCameras = true;
                _ = Task.Run(async () =>
                {
                    try
                    {
                        var requestParams = obj != null
                            ? new ItemManagerRequestParameters {AcceptedCacheDuration = TimeSpan.Zero}
                            : default;

                        var managerByZoneId =
                            await ZoneItemManager
                                .GetManagerByZoneIdConnectIfFail(ZoneId); // GetManagerByZoneId(ZoneId);

                        var cameras = await CameraItemModelManager.Manager.RefreshCameras(ZoneId,
                            parameters: requestParams);
                        if (managerByZoneId != null)
                        {
                            var recordingProfiles =
                                await RecordingProfileItemManager.Manager.ListRecordingProfiles(ZoneId,
                                    requestParams); //managerByZoneId.RecordingProfileService.GetRecordingProfiles();
                            var convertedCameras = cameras?.Select(o => o)?.ToList();
                            // convertedCameras.ForEach(o => o.IsEditable = true);
                            if (convertedCameras != null)
                            {
                                ResolveRecordingProfileVolumes(convertedCameras);

                                _ = CameraItemHelper.InitCameraProfiles(convertedCameras).ConfigureAwait(false);

                                var recordingProfileItemModels = recordingProfiles?.RecordingProfiles
                                    ?.Where(o => o.IsEnabled && !string.IsNullOrEmpty(o.RtspAddress))
                                    ?.Select(o => (RecordingProfileItemModel) o)?.ToList();
                                _ = Application.Current.Dispatcher.InvokeAsync(() =>
                                {
                                    ActiveCameras = new ObservableCollection<CameraItemModel>(convertedCameras);
                                    var profileItemModels = recordingProfileItemModels?.Where(o =>
                                        o.VolumeIds.Any(v =>
                                        {
                                            var volumeItemModels = Volumes;
                                            return volumeItemModels.Any(b => b.Id == v);
                                        }));
                                    ArchivingProfiles =
                                        new ObservableCollection<RecordingProfileItemModel>(
                                            profileItemModels ??
                                            new List<RecordingProfileItemModel>());
                                });
                            }
                        }
                    }
                    catch (Exception e)
                    {
                    }
                    finally
                    {
                        await Task.Delay(500);
                        IsRefreshingCameras = false;
                    }
                }).ConfigureAwait(false);
            }
        }

        private void ResolveRecordingProfileVolumes(List<CameraItemModel> convertedCameras)
        {
            try
            {
                if (convertedCameras != null)
                {
                    foreach (var c in convertedCameras)
                    {
                        if (c != null)
                        {
                            if (c.RecordingProfileItemMgr != null)
                            {
                                foreach (var r in c.RecordingProfileItemMgr.ItemCollection?.Values?.Where(o =>
                                    o != null && o?.CameraId == c?.Id))
                                {
                                    if (r?.VolumeIds != null)
                                    {
                                        foreach (var volumeGuid in r.VolumeIds.Distinct().ToList())
                                        {
                                            var matchingVolume = Volumes?.FirstOrDefault(o => o?.Id == volumeGuid);
                                            if (matchingVolume != null)
                                            {
                                                r.SelectedVolume = matchingVolume;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private bool CanExecute(object action)
        {
            if (action is EArchiverConfigCommand command)
            {
                switch (command)
                {
                    case EArchiverConfigCommand.OpenNotifications:
                        break;
                    case EArchiverConfigCommand.OpenStatistics:
                        break;
                    case EArchiverConfigCommand.OpenAdvancedSettings:
                        break;
                    case EArchiverConfigCommand.DeleteDatabase:
                        break;
                    case EArchiverConfigCommand.CreateDatabase:
                        break;
                    case EArchiverConfigCommand.DatabaseInfo:
                        break;
                    case EArchiverConfigCommand.BackupRestore:
                        break;
                    case EArchiverConfigCommand.CreateVolume:
                        break;
                    case EArchiverConfigCommand.DeleteVolume:
                        break;
                    case EArchiverConfigCommand.EditVolume:
                        break;
                    case EArchiverConfigCommand.RefreshVolumes:
                        break;
                    case EArchiverConfigCommand.RefreshCameras:
                        break;
                    case EArchiverConfigCommand.CreateCamera:
                        break;
                    case EArchiverConfigCommand.RefreshParents:
                        break;
                    case EArchiverConfigCommand.CreateZone:
                        break;
                    case EArchiverConfigCommand.DeleteZone:
                        break;
                    case EArchiverConfigCommand.EditZone:
                        return !IsEditable && IsDirty;
                    case EArchiverConfigCommand.ManageFolders:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return true;
        }

        private async void ArchiverAction(object action)
        {
            if (action is EArchiverConfigCommand command)
            {
                switch (command)
                {
                    case EArchiverConfigCommand.OpenNotifications:
                        break;
                    case EArchiverConfigCommand.OpenStatistics:
                        break;
                    case EArchiverConfigCommand.OpenAdvancedSettings:
                        break;
                    case EArchiverConfigCommand.DeleteDatabase:
                        break;
                    case EArchiverConfigCommand.CreateDatabase:
                        break;
                    case EArchiverConfigCommand.DatabaseInfo:
                        break;
                    case EArchiverConfigCommand.BackupRestore:
                        break;
                    case EArchiverConfigCommand.CreateVolume:
                        // await GetManagerByZoneId().VolumeService.Test(new EndpointsAdd());

                        var volume = await GetManagerByZoneId().VolumeService
                            .CreateVolume(new CreateVolume
                            {
                                ServerId = ServerId,
                                DateModified = DateTimeOffset.UtcNow,
                                Label = "New",
                                LocalStoragePath = "D:/Video",
                                MinFreeSpaceMb = 0,
                                MountingPath = "C:/video1",
                                RetentionDays = 30
                            });
                        if (volume != Guid.Empty)
                        {
                            RefreshVolumes();
                        }

                        break;
                    case EArchiverConfigCommand.DeleteVolume:
                        if (SelectedVolume != null)
                        {
                            await GetManagerByZoneId().VolumeService
                                .DeleteVolume(SelectedVolume.Id, ServerId);
                        }

                        break;
                    case EArchiverConfigCommand.EditVolume:
                        if (SelectedVolume != null)
                        {
                            var updateVolume = await GetManagerByZoneId().VolumeService
                                .UpdateVolume(SelectedVolume.ConvertTo<UpdateVolume>());
                            if (updateVolume != Guid.Empty)
                            {
                                RefreshVolumes();
                            }
                        }

                        break;
                    case EArchiverConfigCommand.RefreshVolumes:
                        RefreshVolumes();
                        break;
                    case EArchiverConfigCommand.RefreshCameras:
                        await RefreshCameras();
                        break;
                    case EArchiverConfigCommand.RefreshParents:
                        await RefreshParents();
                        break;
                    case EArchiverConfigCommand.CreateCamera:
                        CreateCamera();
                        break;
                    case EArchiverConfigCommand.CreateZone:
                        CreateZone();
                        break;
                    case EArchiverConfigCommand.DeleteZone:
                        break;
                    case EArchiverConfigCommand.EditZone:
                        EditZone();
                        break;
                    case EArchiverConfigCommand.ManageFolders:
                        ManageFolders();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void ManageFolders()
        {
            var management = TabUtil.FindFocusedTab<ManagementTab>();
            if (management != null)
            {
                var dialog = new ManageFoldersDialog();
                if (dialog.Model != null)
                {
                    dialog.Model.DialogItem.Archiver = this;
                    dialog.SetModalView(management.ManagementView.DialogOverlayGrid.ModalContentPresenter);
                    dialog.Closed += async (sender, args) => { };
                }
            }
        }

        private async void EditZone()
        {
            try
            {
                await GetManagerByZoneId(ZoneId).ServerService
                    .UpdateServer(this.ConvertTo<UpdateServer>());

                //ServerDescriptor.Label = Label;
                var checkDirty = IsDirty;
                IsEditable = true;
                Console.WriteLine($"EditZone IsDirty: {checkDirty}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void CreateZone(object obj = null)
        {
        }

        private void CreateCamera()
        {
            var management = ObjectResolver.ResolveAll<ManagementViewModel>()
                .FirstOrDefault(o => (o?.View).TabIsFocused());
            if (management != null)
            {
                var dialog = new CreateCameraDialog(){ZoneCameraManagementViewModel = management.View.ZoneCameraManagementView.Model};
                if (dialog.Model != null)
                {
                    dialog.Model.Archiver = this;
                    var dialogModel = dialog.Model;
                    dialog.SetModalView(management.View.DialogOverlayGrid.ModalContentPresenter);
                    dialog.Closed += async (sender, args) =>
                    {
                        if (dialog != null && dialog.Model?.DialogItem is CreateCameraItemModel item)
                        {
                            if (dialog.Result)
                            {
                                try
                                {
                                    if (dialogModel?.CreatedCameras?.Count > 0)
                                    {
                                        foreach (var c in dialogModel?.CreatedCameras)
                                        {
                                            try
                                            {
                                                var convertedCreate = c.ConvertTo<CreateCamera>();
                                                convertedCreate.ParentId = c.Parent.ObjectId;
                                                convertedCreate.ZoneId = ZoneId;
                                                var serverId = ServerId;
                                                var serverSpecificClient = GetManagerByZoneId(ZoneId).CameraService
                                                    .ServerSpecificClient(serverId);
                                                await serverSpecificClient.Request(
                                                    convertedCreate, useSameClient: true);
                                            }
                                            catch (Exception e)
                                            {
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var nearestNode = dialog.Model?.DialogItem?.Parent;

                                        if (nearestNode != null && nearestNode.ObjectId != Guid.Empty)
                                        {
                                            if (nearestNode.ObjectId != null)
                                            {
                                                var convertedCreate = item.ConvertTo<CreateCamera>();
                                                convertedCreate.Type = item.Type;
                                                convertedCreate.ParentId = nearestNode.ObjectId;
                                                convertedCreate.ZoneId = ZoneId;
                                                var serverId = ServerId;
                                                var serverSpecificClient = GetManagerByZoneId(ZoneId).CameraService
                                                    .ServerSpecificClient(serverId);
                                                await serverSpecificClient.Request(
                                                    convertedCreate, useSameClient: true);
                                            }
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }
                    };
                }
            }
        }


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void RefreshVolumes(object obj = null)
        {
            if (!IsRefreshingVolumes)
            {
                IsRefreshingVolumes = true;
                try
                {
                    await Task.Run(async () =>
                    {
                        var volumes = await VolumeItemManager.Manager.ListVolumes(ZoneId, ServerId, obj == null);
                        var convertedVolumes = volumes?.Results?.Where(o => o.ServerId == ServerId)
                            ?.Select(o => (VolumeItemModel) o)?.ToList();
                        if (convertedVolumes != null)
                        {
                            await Application.Current.Dispatcher.InvokeAsync(() =>
                            {
                                Volumes = new ObservableCollection<VolumeItemModel>(convertedVolumes);
                            });
                        }
                    });
                    //var svc = await ServerItemManager.GetManagerByZoneIdConnectIfFail(ZoneId);
                    //var volumes = await svc.VolumeService.GetVolumes();
                }
                catch (Exception e)
                {
                }
                finally
                {
                    IsRefreshingVolumes = false;
                }
            }
        }
    }

    public class ServerDescriptor
    {
        public string Label { get; set; }
        public Guid? ServerId { get; set; }
        public Guid? ZoneId { get; set; }
    }
}