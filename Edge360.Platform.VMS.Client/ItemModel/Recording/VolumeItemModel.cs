﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.ItemModel.DataObject;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.Volumes;
using log4net;
using ServiceStack;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.ItemModel.Recording
{
    public class VolumeItemModel : NodeObject, INotifyPropertyChanged, IVolumeDescriptor
    {
        ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private RelayCommand _addCommand;
        private DateTimeOffset _dateModified;
        private RelayCommand _deleteCommand;
        private Guid _id;
        private string _localStoragePath;
        private int _minFreeSpaceMb;
        private string _mountingPath;
        private RelayCommand _openCommand;
        private int _retentionDays;
        private Guid _serverId;
        private string _serverName;
        private bool _isSelected;
        private GetVolumeInfoResponse _volumeInfo;

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                if (value == _isSelected) return;
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand AddCommand => _addCommand ??= new RelayCommand(p => Add(), p => CanAdd());

        public string ComputedLabel
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Label))
                {
                    return LocalStoragePath + "@" + ServerName;
                }

                return Label;
            }
        }

        public RelayCommand DeleteCommand =>
            _deleteCommand ??= new RelayCommand(p => Delete(), p => CanDelete());

        public RelayCommand OpenCommand =>
            _openCommand ??= new RelayCommand(p => Open(), p => CanOpen());

        public string ServerName
        {
            get => _serverName;
            set
            {
                _serverName = value;
                OnPropertyChanged();
            }
        }

        public DateTimeOffset DateModified
        {
            get => _dateModified;
            set
            {
                _dateModified = value;
                OnPropertyChanged();
            }
        }


        public Guid Id
        {
            get => _id;
            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }

        public string LocalStoragePath
        {
            get => _localStoragePath;
            set
            {
                _localStoragePath = value;
                OnPropertyChanged();
            }
        }

        public int MinFreeSpaceMb
        {
            get => _minFreeSpaceMb;
            set
            {
                _minFreeSpaceMb = value;
                OnPropertyChanged();
            }
        }

        public string MountingPath
        {
            get => _mountingPath;
            set
            {
                _mountingPath = value;
                OnPropertyChanged();
            }
        }

        public int RetentionDays
        {
            get => _retentionDays;
            set
            {
                _retentionDays = value;
                OnPropertyChanged();
            }
        }

        public Guid ServerId
        {
            get => _serverId;
            set
            {
                if (value.Equals(_serverId))
                {
                    return;
                }

                _serverId = value;
                OnPropertyChanged();
            }
        }

        public string DiskInformation
        {
            get
            {
                try
                {
                    if (VolumeInfo == null)
                    {
                        GetVolumeInfo(this);
                    }

                    if (VolumeInfo != null)
                    {
                        var volumeInfoTotalDiskSize = (float) ((float)((float)VolumeInfo.AvailableBytes / (float)VolumeInfo.TotalDiskSize));
                        return $"{volumeInfoTotalDiskSize:P} Available: {VolumeInfo.AvailableBytes.ToFileSizeApi()} DiskSize: {VolumeInfo.TotalDiskSize.ToFileSizeApi()}";
                    }

                    return "?";
                }
                catch (Exception e)
                {
                    return "?";
                }
            }
        }

        public async void Delete()
        {
            if (DialogUtil.ConfirmDialog($"Do you wish to delete the volume?\n{ComputedLabel}", true))
            {
                try
                {
                    await GetManagerByZoneId(ConnectedZoneId).VolumeService.DeleteVolume(Id);
                }
                catch (Exception e)
                {
                    _log?.Info(e);
                }

                //_log?.Info($"{resp.Message}");
                //CameraDeleted?.Invoke();
            }
        }

        public override string ToString()
        {
            return $"{base.ToString()}";
        }

        private void Add()
        {
        }

        private bool CanAdd()
        {
            return true;
        }

        private bool CanDelete()
        {
            return true;
        }

        private bool CanOpen()
        {
            return true;
        }

        private void Open()
        {
            try
            {
                if (Directory.Exists(MountingPath))
                {
                    Process.Start(MountingPath);
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        public static implicit operator VolumeDescriptor(VolumeItemModel model)
        {
            return model.ConvertTo<VolumeDescriptor>();
        }

        public static implicit operator VolumeItemModel(VolumeDescriptor desc)
        {
            var volumeItemModel = desc.ConvertTo<VolumeItemModel>();
            GetVolumeInfo(volumeItemModel);
            return volumeItemModel;
        }

        private static async void GetVolumeInfo(VolumeItemModel model)
        {
            try
            {
                await Task.Run(async () =>
                {
                    if (model != null)
                    {
                        model.VolumeInfo = await VolumeItemManager.Manager.GetVolumeInfo(model.Id, model.ServerId, new ItemManagerRequestParameters() {AcceptedCacheDuration = TimeSpan.Zero});
                    }
                });
            }
            catch (Exception e)
            {
                
            }
        }

        public GetVolumeInfoResponse VolumeInfo
        {
            get => _volumeInfo;
            set
            {
                if (Equals(value, _volumeInfo)) return;
                _volumeInfo = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DiskInformation));
                OnPropertyChanged(nameof(DiskUsagePercent));
            }
        }

        public float DiskUsagePercent
        {
            get
            {
                try
                {
                    if (VolumeInfo != null)
                    {
                        var volumeInfoTotalDiskSize = 1 - (float) ((float)((float)VolumeInfo.AvailableBytes / (float)VolumeInfo.TotalDiskSize));
                        return volumeInfoTotalDiskSize;
                    }

                    return -1;
                }
                catch (Exception e)
                {
                    return -1;
                }
            }
        }
    }
}