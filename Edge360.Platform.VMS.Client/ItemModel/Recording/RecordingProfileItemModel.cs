﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Caches.Implementation;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Dialog.ObjectResolverDialog.View;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.RecordingProfiles;
using log4net;
using Newtonsoft.Json;
using RecordingService.ServiceModel.Api.RecordingProfile;
using RecordingService.ServiceModel.Enums;
using ServiceStack;
using Telerik.Windows.Controls;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.ItemModel.Recording
{
    public class RecordingProfileItemModel : IRecordingProfileDescriptor, INotifyPropertyChanged
    {
        private Guid _cameraId;
        private CameraItemModelManager _cameraItemModelMgr;
        private ICommand _deleteRecordingProfileCommand;
        private RecordingProfileDescriptor _descriptor;
        private ICommand _editRecordingProfileCommand;
        private int _height;
        private Guid _id;
        private bool _isEditable;
        private bool _isEnabled;
        private bool _isRefreshingProfile;
        private bool _isSelected;
        private string _label;
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private OnvifProfileItemManager _onvifProfileMgr;
        private CameraItemModel _owner;
        private string _parameters;
        private ERecordingProfileProperty _propertyFlags;
        private RelayCommand _refreshProfileCommand;
        private string _rtspAddress;
        private CameraItemModel _selectedCamera;
        private BaseAdapterCameraProfileItemModel _selectedOnvifProfile;
        private VolumeItemModel _selectedVolume;
        private ObservableCollection<VolumeItemModel> _selectedVolumes = new ObservableCollection<VolumeItemModel>();
        private List<Guid> _volumeIds;
        private int _width;

        public CameraItemModelManager CameraItemModelMgr
        {
            get => _cameraItemModelMgr;
            set
            {
                if (Equals(value, _cameraItemModelMgr))
                {
                    return;
                }

                _cameraItemModelMgr = value;
                OnPropertyChanged();
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                return _deleteRecordingProfileCommand ??=
                    new RelayCommand(p => DeleteRecordingProfile(p), p => CanDeleteProfile());
            }
        }

        public RecordingProfileDescriptor Descriptor
        {
            get => _descriptor;
            set
            {
                if (Equals(value, _descriptor))
                {
                    return;
                }

                _descriptor = value;
                OnPropertyChanged();
            }
        }

        public ICommand EditCommand
        {
            get
            {
                return _editRecordingProfileCommand ??=
                    new RelayCommand(p => EditRecordingProfile(p), p => IsEditable && IsDirty);
            }
        }

        public bool IsDirty => true;
        //Descriptor != null &&
        //                       (OriginalVolume != SelectedVolume || Descriptor.Label != Label || OriginalOnvifProfile != SelectedOnvifProfile ||
        //                        Descriptor.Height != Height || Descriptor.Width != Width ||
        //                        Descriptor.Parameters != Parameters || Descriptor.IsEnabled != IsEnabled);

        public bool IsEditable
        {
            get => _isEditable;
            set
            {
                if (value == _isEditable)
                {
                    return;
                }

                _isEditable = value;

                if (!_isEditable) // reset fields
                {
                    if (Descriptor != null)
                    {
                        IsEnabled = Descriptor.IsEnabled;
                        Label = Descriptor.Label;
                        VolumeIds = Descriptor.VolumeIds;
                        RtspAddress = Descriptor.RtspAddress;
                        Height = Descriptor.Height;
                        Width = Descriptor.Width;
                        if (OriginalOnvifProfile != null)
                        {
                            SelectedOnvifProfile = OriginalOnvifProfile;
                        }

                        SelectedVolume = OriginalVolume;
                    }
                }

                OnPropertyChanged();
                OnPropertyChanged(nameof(EditCommand));
            }
        }

        public bool IsRefreshingProfile
        {
            get => _isRefreshingProfile;
            set
            {
                if (value == _isRefreshingProfile)
                {
                    return;
                }

                _isRefreshingProfile = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(RefreshProfileCommand));
            }
        }

        [JsonIgnore]
        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                if (value == _isSelected)
                {
                    return;
                }

                _isSelected = value;
                OnPropertyChanged();
            }
        }

        public CollectionViewSource OnvifProfileCollectionViewSource { get; set; }

        public OnvifProfileItemManager OnvifProfileMgr
        {
            get => _onvifProfileMgr;
            set
            {
                if (Equals(value, _onvifProfileMgr))
                {
                    return;
                }

                _onvifProfileMgr = value;
                OnPropertyChanged();
            }
        }

        public BaseAdapterCameraProfileItemModel OriginalOnvifProfile { get; set; }

        public VolumeItemModel OriginalVolume { get; set; }

        [Browsable(false)]
        public ObservableCollection<RecordingProfilePropertyItemModel> ProfileProperties { get; set; } =
            new ObservableCollection<RecordingProfilePropertyItemModel>();

        public RecordingProfileItemManager RecordingProfileItemMgr { get; set; }

        [Browsable(false)] public ERecordingProfileProperty RecordingProfileProperty => PropertyFlags;


        public RelayCommand RefreshProfileCommand =>
            _refreshProfileCommand ??= new RelayCommand(
                o => RefreshProfile(o), o => !IsRefreshingProfile);

        public ICommand RemoveVolume =>
            new RelayCommand(o =>
            {
                var volumeDescriptor = o as VolumeItemModel;
                if (volumeDescriptor != null)
                {
                    VolumeIds = VolumeIds.Except(new[] {volumeDescriptor.Id}).ToList();
                }
            }, o => true);

        public CameraItemModel SelectedCamera
        {
            get => _selectedCamera;
            set
            {
                if (_selectedCamera != value)
                {
                    _selectedCamera = value;
                    OnPropertyChanged();
                }
            }
        }

        public BaseAdapterCameraProfileItemModel SelectedOnvifProfile
        {
            get => _selectedOnvifProfile;
            set
            {
                if (Equals(value, _selectedOnvifProfile))
                {
                    return;
                }

                _selectedOnvifProfile = value;
                try
                {
                    if (_selectedOnvifProfile != null)
                    {
                        Width = _selectedOnvifProfile.VideoEncoderConfiguration.Resolution.Width;
                        Height = _selectedOnvifProfile.VideoEncoderConfiguration.Resolution.Height;
                    }
                }
                catch (Exception e)
                {
                }

                OnPropertyChanged();
            }
        }


        public VolumeItemModel SelectedVolume
        {
            get =>
                //if (_selectedVolume == null && !isRequestingVolume)
                //{
                //    Application.Current.Dispatcher.InvokeAsync(async () =>
                //    {
                //        var volume = await GetVolume(ConnectedZoneId);
                //        if (volume != null && volume != _selectedVolume)
                //        {
                //            _selectedVolume = volume;
                //        }
                //    });
                //}
                _selectedVolume;
            set
            {
                if (Equals(value, _selectedVolume))
                {
                    return;
                }

                _selectedVolume = value;

                //if (_selectedVolume != null && VolumeIds != null && !VolumeIds.Contains(_selectedVolume.Id))
                //{
                //    //VolumeIds.Clear();
                //    VolumeIds.Add(_selectedVolume.Id);
                //}

                //SelectedVolumes.Clear();
                //var volumeItemModels = SelectedVolumes.Where(o => VolumeIds.Contains(o.Id));
                //SelectedVolumes.Add(volumeItemModels);
                //{
                //    VolumeIds.Remove(_selectedVolume.Id);
                //}
                OnPropertyChanged();
                OnPropertyChanged(nameof(VolumeIds));
            }
        }

        public ObservableCollection<VolumeItemModel> SelectedVolumes
        {
            get => _selectedVolumes;
            set
            {
                if (Equals(value, _selectedVolumes))
                {
                    return;
                }

                _selectedVolumes = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand SelectVolumes => new RelayCommand(o =>
        {
            var resolverDialog = new ObjectResolverDialog<VolumeCache, VolumeItemModel>();

            if (resolverDialog.ShowDialog() != true)
            {
                return;
            }

            var descriptors = resolverDialog.GetSelected();
            VolumeIds = descriptors.Select(t => t.Id).ToList();
        }, o => true);

        private bool isRequestingVolume { get; set; }

        //public RelayCommand SelectCamera => new RelayCommand(o =>
        //{
        //    var resolverDialog = new ObjectResolverDialog<CameraCache, CameraItemModel>();

        //    if (resolverDialog.ShowDialog() != true)
        //    {
        //        return;
        //    }

        //    var descriptors = resolverDialog.GetSelected().ToList();
        //    SelectedCamera = descriptors.FirstOrDefault();
        //    var guid = descriptors.Select(t => t.Id).FirstOrDefault();
        //    if (guid != null)
        //    {
        //        CameraId = (Guid) guid;
        //    }


        //    OnPropertyChanged(nameof(ResolvedCameraLabel));
        //}, o => true);

        public RecordingProfileItemModel()
        {
            OnvifProfileMgr = OnvifProfileItemManager.Manager;
            RecordingProfileItemMgr = RecordingProfileItemManager.Manager;
            CameraItemModelMgr = CameraItemModelManager.Manager;

            if (Id != Guid.Empty && !RecordingProfileItemMgr.ItemCollection.Any(o => o.Key == Id && !string.IsNullOrEmpty(o.Value.RtspAddress)))
            {
                RecordingProfileItemMgr.Update(new KeyValuePair<Guid, RecordingProfileItemModel>(Id, this));
            }

            Application.Current.Dispatcher.InvokeAsync(() =>
            {
                
                OnvifProfileCollectionViewSource = new CollectionViewSource
                {
                    Source = OnvifProfileMgr.ItemCollection
                };

                OnvifProfileCollectionViewSource.Filter += OnvifProfileCollectionViewSourceOnFilter;
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [Editable(false)]
        [ReadOnly(true)]
        [Browsable(false)]
        public ERecordingProfileProperty PropertyFlags
        {
            get => _propertyFlags;
            set
            {
                if (value == _propertyFlags)
                {
                    return;
                }

                _propertyFlags = value;
                OnPropertyChanged(nameof(RecordingProfileProperty));
                OnPropertyChanged();
            }
        }

        public Guid CameraId
        {
            get => _cameraId;
            set
            {
                if (value.Equals(_cameraId))
                {
                    return;
                }

                _cameraId = value;
                OnPropertyChanged();
            }
        }

        public int Height
        {
            get => _height;
            set
            {
                if (value == _height)
                {
                    return;
                }

                _height = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsDirty));
            }
        }

        public Guid Id
        {
            get => _id;
            set
            {
                if (value.Equals(_id))
                {
                    return;
                }

                _id = value;
                OnPropertyChanged();
            }
        }

        public bool IsEnabled
        {
            get => _isEnabled;
            set
            {
                if (value == _isEnabled)
                {
                    return;
                }

                _isEnabled = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsDirty));
            }
        }

        public string Parameters
        {
            get => _parameters;
            set
            {
                if (value == _parameters)
                {
                    return;
                }

                _parameters = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsDirty));
            }
        }

        public string Label
        {
            get => _label;
            set
            {
                if (value == _label)
                {
                    return;
                }

                _label = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsDirty));
            }
        }

        public string RtspAddress
        {
            get => _rtspAddress;
            set
            {
                if (value == _rtspAddress)
                {
                    return;
                }

                _rtspAddress = value;
                OnPropertyChanged();
            }
        }

        public List<Guid> VolumeIds
        {
            get => _volumeIds;
            set
            {
                if (Equals(value, _volumeIds))
                {
                    return;
                }

                _volumeIds = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(RemoveVolume));
            }
        }

        public int Width
        {
            get => _width;
            set
            {
                if (value == _width)
                {
                    return;
                }

                _width = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsDirty));
            }
        }

        public override string ToString()
        {
            return
                $"{nameof(CameraId)}: {CameraId}, {nameof(Height)}: {Height}, {nameof(Id)}: {Id}, {nameof(IsEnabled)}: {IsEnabled}, {nameof(Label)}: {Label}, {nameof(Width)}: {Width}";
        }

        public static RecordingProfileItemModel CreateInstance()
        {
            return new RecordingProfileItemModel();
        }

        public static event Action<RadTabControl> ManagementUpdateRequest;

        private async Task RefreshProfile(object obj)
        {
            if (!IsRefreshingProfile)
            {
                IsRefreshingProfile = true;
                try
                {
                    await Task.Run(async () =>
                    {
                        if (RecordingProfileItemMgr != null)
                        {
                            await RecordingProfileItemMgr.GetRecordingProfiles(this);
                        }

                        if (obj is RadTabControl tabControl)
                        {
                            try
                            {
                                ManagementUpdateRequest?.Invoke(tabControl);
                                //var tabControlSelectedContent = (tabControl.SelectedContent as ContentPresenter);
                                //var recordingVolumeManagementTreeView = tabControlSelectedContent?.ChildrenOfType<RecordingVolumeManagementTreeView>().FirstOrDefault();
                                //if (recordingVolumeManagementTreeView != null)
                                //{
                                //    if (tabControl.SelectedItem is RecordingProfileItemModel recordingProfileItemModel)
                                //    {
                                //        RecordingVolumeManagementTreeView.UpdateSelectedVolumes(recordingProfileItemModel,
                                //            recordingProfileItemModel.SelectedVolumes.ToList());
                                //    }
                                //}
                            }
                            catch (Exception e)
                            {
                            }

                            //tabControl.SelectedItem = tabControl.Items.OfType<RecordingProfileItemModel>()
                            //    .FirstOrDefault(o => o.Id == Id);
                            try
                            {
                                var cameraItemModel = CameraItemModelMgr.ItemCollection
                                    .FirstOrDefault(o => o.Key == CameraId).Value;
                                OnvifProfileMgr?.RefreshProfiles(
                                    new ItemManagerRequestParameters(), cameraItemModel);
                            }
                            catch (Exception e)
                            {
                            }

                            await FindMatchingOnvifProfile();
                        }
                    });
                }
                catch (Exception e)
                {
                }
                finally
                {
                    IsRefreshingProfile = false;
                }
            }
        }

        public async Task FindMatchingOnvifProfile()
        {
            try
            {
                using (var cts = new CancellationTokenSource(10000))
                {
                    await Task.Run(async () =>
                    {
                        while (!cts.IsCancellationRequested &&
                               OnvifProfileMgr.ItemCollection.Keys.All(o => o.Item1 != CameraId) ||
                               CameraItemModelMgr.ItemCollection.Keys.All(o => o != CameraId))
                        {
                            await Task.Delay(250);
                        }

                        var camera = CameraItemModelMgr.ItemCollection.FirstOrDefault(o => o.Key == CameraId).Value;
                        if (camera != null)
                        {
                            var baseAdapterCameraProfileItemModel = OnvifProfileMgr.ItemCollection
                                .FirstOrDefault(o =>
                                    CameraItemModel.AddAuthUri(camera.Username, camera.Password, o.Value.StreamUri) ==
                                    RtspAddress).Value;

                            if (baseAdapterCameraProfileItemModel != null)
                            {
                                Application.Current?.Dispatcher?.InvokeAsync(() =>
                                {
                                    OnvifProfileCollectionViewSource.View.Refresh();
                                    OriginalOnvifProfile =
                                        SelectedOnvifProfile = baseAdapterCameraProfileItemModel;
                                });
                            }
                        }

                        //OnvifProfileMgr.ItemCollection.Values.FirstOrDefault(o => o == RtspAddress);
                    }, cts.Token);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }


        private async Task<VolumeItemModel> ResolvedVolume(Guid? zoneId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (Id != null)
                    {
                        if (zoneId != null)
                        {
                            var zone = zoneId.Value;

                            var resp = await VolumeItemManager.Manager.ListVolumes(zone, default);

                            //await ServiceManagers[zone].VolumeService.GetVolumes()
                            //.TaskTimeoutAfter(TimeSpan.FromSeconds(2));

                            if (resp?.Results.Count > 0)
                            {
                                var volume =
                                    resp.Results.FirstOrDefault(o => VolumeIds != null && VolumeIds.Contains(o.Id));
                                if (volume != null)
                                {
                                    return volume;
                                }
                            }
                        }
                    }

                    return default;
                });
            }
            catch (Exception)
            {
            }

            return default;
        }

        private Task<VolumeItemModel> GetVolume(Guid? zoneId)
        {
            return Task.Run(async () =>
            {
                try
                {
                    if (!isRequestingVolume)
                    {
                        isRequestingVolume = true;
                        OriginalVolume = await ResolvedVolume(zoneId);
                        SelectedVolume = OriginalVolume;
                        isRequestingVolume = false;
                    }


                    return OriginalVolume;
                }
                catch (Exception)
                {
                    return default;
                }
            });
        }


        public static implicit operator RecordingProfileItemModel(RecordingProfile obj)
        {
            var convertedProfileObject = obj.ConvertTo<RecordingProfileItemModel>();
            convertedProfileObject.Id = obj.RecordingProfileId;
            convertedProfileObject.Label = obj.ProfileName;
            convertedProfileObject.Descriptor = new RecordingProfileDescriptor();
            convertedProfileObject.InitPropertyFlags(new RecordingProfileDescriptor());
            return convertedProfileObject;
        }


        public static implicit operator RecordingProfileItemModel(RecordingProfileDescriptor obj)
        {
            var convertedProfileObject = obj.ConvertTo<RecordingProfileItemModel>();
            convertedProfileObject.Descriptor = obj;
            convertedProfileObject.InitPropertyFlags(obj);
            return convertedProfileObject;
        }

        private async void DeleteRecordingProfile(object o)
        {
            try
            {
                var resp = await DialogUtil.EmbeddedModalConfirmDialog(
                    TabUtil.FindFocusedTab<ManagementTab>().ManagementView.DialogOverlayGrid.ModalContentPresenter,
                    new EmbeddedDialogParameters
                    {
                        DialogButtons = EDialogButtons.OkCancel,
                        Description = $"Are you sure you wish to delete this Recording Profile?\n '{Label}'"
                    });
                if (resp)
                {
                    await GetManagerByZoneId().RecordingProfileService
                        .DeleteRecordingProfile(this);
                    RefreshProfile(o);
                    // Owner?.RefreshRecordingProfilesCommand?.Execute(null);
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        private async void EditRecordingProfile(object o)
        {
            try
            {
                var profile = this.ConvertTo<UpdateRecordingProfile>();

                if (SelectedOnvifProfile != null)
                {
                    OriginalOnvifProfile = SelectedOnvifProfile;
                }


                if (SelectedOnvifProfile != null)
                {
                    var cameraItem = CameraItemModelMgr.ItemCollection.FirstOrDefault(b => b.Key == CameraId).Value;
                    if (cameraItem != null)
                    {
                        //var presets =
                        //    cameraItem.OnvifProfileMgr.ItemCollection.Where(b => b.Key.Item1 == cameraItem.Id);
                        //foreach (var p in presets)
                        //{
                        //    OnvifProfileMgr.Update(new KeyValuePair<Tuple<Guid, string>, BaseAdapterCameraProfileItemModel>(p.Key, SelectedOnvifProfile));
                        //}

                        var profileRtspAddress = CameraItemModel.AddAuthUri(cameraItem.Username, cameraItem.Password,
                            SelectedOnvifProfile.StreamUri);
                        profile.RtspAddress = profileRtspAddress;
                    }
                }

                var resp = await GetManagerByZoneId().RecordingProfileService
                    .UpdateRecordingProfile(profile);
                if (resp != null)
                {
                    if (Descriptor != null)
                    {
                        Descriptor.Label = Label;
                        Descriptor.Height = Height;
                        Descriptor.Width = Width;
                        Descriptor.Parameters = Parameters;
                        Descriptor.IsEnabled = IsEnabled;
                    }


                    //if (Owner != null)
                    {
                        await RefreshProfile(o);
                        // Owner.RefreshRecordingProfilesCommand?.Execute(null);
                    }
                }

                IsEditable = false;
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        private bool CanDeleteProfile()
        {
            return true;
        }


        private void InitPropertyFlags(RecordingProfileDescriptor config)
        {
            try
            {
                foreach (var e in Enum.GetValues(typeof(ERecordingProfileProperty)).OfType<ERecordingProfileProperty>())
                {
                    try
                    {
                        if (e == ERecordingProfileProperty.None)
                        {
                            continue;
                        }

                        var streamProfilePropertyItemModel = new RecordingProfilePropertyItemModel
                            {Flag = e, IsChecked = config.PropertyFlags.HasFlag(e)};
                        streamProfilePropertyItemModel.FlagChanged += StreamProfilePropertyItemModelOnFlagChanged;
                        ProfileProperties.Add(streamProfilePropertyItemModel);
                    }
                    catch (Exception exception)
                    {
                    }
                }
            }
            catch (Exception e)
            {
            }
        }


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnvifProfileCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is KeyValuePair<Tuple<Guid, string>, BaseAdapterCameraProfileItemModel> model)
            {
                e.Accepted = model.Key.Item1 == CameraId;
            }
        }

        private void StreamProfilePropertyItemModelOnFlagChanged(ERecordingProfileProperty flag, bool enabled)
        {
            var existingFlag = PropertyFlags;
            if (enabled)
            {
                existingFlag |= flag;
            }
            else
            {
                existingFlag ^= flag;
            }

            PropertyFlags = existingFlag;
        }
    }


    public class RecordingProfilePropertyItemModel : INotifyPropertyChanged
    {
        private ERecordingProfileProperty _flag;
        private bool _isChecked;

        public ERecordingProfileProperty Flag
        {
            get => _flag;
            set
            {
                if (value == _flag)
                {
                    return;
                }

                _flag = value;
                OnPropertyChanged();
            }
        }

        public bool IsChecked
        {
            get => _isChecked;
            set
            {
                if (value == _isChecked)
                {
                    return;
                }

                _isChecked = value;
                FlagChanged?.Invoke(Flag, value);
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public event Action<ERecordingProfileProperty, bool> FlagChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}