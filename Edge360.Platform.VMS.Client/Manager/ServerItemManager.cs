﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.Properties;
using Edge360.Platform.VMS.Common.Interfaces.Authority;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Communication.Polly;
using Edge360.Platform.VMS.Communication.Services;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Servers;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using Unity;
using WispFramework.Patterns.Generators;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.Manager
{
    public class ServerListCacheFactory : BaseCacheFactory<ListServersResponse, Guid>
    {
        public ServerListCacheFactory() : base(TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(3))
        {
        }

        public override async Task<ListServersResponse> UncachedRequest(Guid zoneId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var managerByZoneId = GetManagerByZoneId();
                    if (managerByZoneId != null)
                    {
                        var resp = await managerByZoneId.GlobalServerService
                            .ListServers(new ListServers {ZoneId = zoneId});
                        var serverList = resp?.Servers;
                        try
                        {
                            if (serverList != null)
                            {
                                var zones = serverList.GroupBy(o => o.ZoneId).ToDictionary(
                                    o => o.Key,
                                    model => serverList.Where(o => o.ZoneId == model.Key));

                                var currentConnectionSvc = GetManagerByZoneId(Guid.Empty);
                                var servers = new Dictionary<Tuple<Guid, Guid>, ServerItemModel>();
                                foreach (var z in zones)
                                {
                                    foreach (var s in z.Value.ToList())
                                    {
                                        var server = new ServerItemModel
                                        {
                                            HostName = s.HostName,
                                            ServerName = s.ServerName,
                                            PortApi = s.PortApi,
                                            PortSso = s.PortSso,
                                            PortLive = s.PortLive,
                                            PortArchive = s.PortArchive,
                                            PortArchiveControl = s.PortArchiveControl,
                                            Address = $"{s.HostName}:{s.PortApi}",
                                            Label = $"{s.ServerName}",
                                            ServerId = s.ServerId,
                                            ConnectionStatus = EConnectionStatus.None,
                                            Username = SettingsManager.Settings.Login.Credentials.UserName,
                                            Password = SettingsManager.Settings.Login.Credentials.Password,
                                            ZoneId = s.ZoneId,
                                        };
                                        servers.Add(new Tuple<Guid, Guid>(s.ServerId, s.ZoneId), server);
                                        if (true)
                                        {
                                            if (server.ZoneId == ConnectedZoneId || s.ServerId == ConnectedServerId)
                                            {
                                                server.ConnectionStatus = EConnectionStatus.Connected;

                                                var config = (ServerConfigItemModel) server;

                                                server.Archiver = config;

                                                server.Archivers.Add(server.Archiver);
                                            }
                                            else
                                            {
                                                var alreadyConnected = GetManagerByZoneId(server.ZoneId);
                                                if (alreadyConnected != null)
                                                {
                                                    server.ConnectionStatus = EConnectionStatus.Connected;

                                                    try
                                                    {
                                                        var config = (ServerConfigItemModel) server;

                                                        server.Archiver = config;
                                                        server.Archivers.Add(server.Archiver);
                                                    }
                                                    catch (Exception e)
                                                    {
                                                    }
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        var config = (ServerConfigItemModel) server;

                                                        server.Archiver = config;
                                                        server.Archivers.Add(server.Archiver);
                                                    }
                                                    catch (Exception e)
                                                    {
                                                    }

                                                    var _ = Task.Run(async () =>
                                                    {
                                                        try
                                                        {
                                                            _log?.Info($"CreateServerConnection {s.ZoneId} {s.ServerId}");
                                                           
                                                            var svc = await ServerItemManager.CreateServerConnection(s,
                                                                new UserLogin
                                                                {
                                                                    ApiPort = (ushort) s.PortApi,
                                                                    Domain = s.HostName,
                                                                    UserName = server.Username,
                                                                    Password = server.Password,
                                                                    BearerToken = currentConnectionSvc?.Client?.BearerToken,
                                                                    RefreshToken = currentConnectionSvc?.Client?.RefreshToken,
                                                                    IsBearerLogin = true
                                                                });

                                                            if (server.ConnectionStatus !=
                                                                EConnectionStatus.Unauthorized)
                                                            {
                                                                server.ConnectionStatus =
                                                                    svc == null
                                                                        ? EConnectionStatus.Timeout
                                                                        : EConnectionStatus.Connected;
                                                            }
                                                        }
                                                        catch (Exception e)
                                                        {
                                                        }
                                                    }).ConfigureAwait(false);
                                                }
                                            }
                                        }

                                        // server?.ArchiverCollectionViewSource?.View?.Refresh();
                                    }
                                }

                                var nonExistingServers = ServerItemManager.Manager.ItemCollection.Where(o =>
                                    o.Value.ZoneId == zoneId && servers.All(s => s.Value.ServerId != o.Key.Item1));
                                ServerItemManager.Manager.Remove(nonExistingServers.ToArray());
                                resp.Servers = new List<IServer>(servers.Values.ToList());
                                ServerItemManager.Manager.Update(servers.ToArray());
                            }
                        }
                        catch (Exception e)
                        {
                            return default;
                        }

                        return resp;
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
            }

            return default;
        }
    }

    /// <summary>
    ///     ServerId, ZoneId
    /// </summary>
    public class ServerItemManager : BaseItemManager<ServerItemModel, Guid, Guid>
    {
        private ZoneItemManager _zoneItemMgr;

        public static ServerItemManager Manager =>
            App.GlobalUnityContainer.Resolve<IBaseManager>(nameof(ServerItemManager)) as ServerItemManager;

        public static ServerListCacheFactory ServerListCacheFactory { get; set; } = new ServerListCacheFactory();

        public override List<ICacheFactory> CacheFactories { get; set; } = new List<ICacheFactory>
        {
            ServerListCacheFactory,
        };

        public ServerItemManager(ILog log = null, ZoneItemManager zoneItemManager = null)
        {
            _log = log;
            _zoneItemMgr = zoneItemManager;
            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;
        }

        public async Task EditServer(ServerItemModel model)
        {
        }


        public ServerItemModel GetServer(Guid serverId = default)
        {
            try
            {
                if (serverId == default)
                {
                    serverId = ConnectedServerId;
                }

                var server = ItemCollection.FirstOrDefault(o => o.Key.Item1 == serverId).Value;
                return server;
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public List<ServerItemModel> GetServersByZoneId(Guid zoneId)
        {
            try
            {
                var servers = ItemCollection.Where(o => o.Key.Item2 == zoneId).Select(o => o.Value);
                return servers.ToList();
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public async Task RemoveServer(Guid serverId)
        {
            var server = ItemCollection.FirstOrDefault(o => o.Value.ServerId == serverId);
            if (server.Value != null)
            {
                Remove(server);
            }
        }

        public async Task RefreshServer(Guid serverId, Guid zoneId = default)
        {
        }

        public async Task<ListServersResponse> ListServers(Guid zoneId = default,
            ItemManagerRequestParameters parameters = default)
        {
            return await Task.Run(async () =>
            {
                var listServersResponse = HasCacheTimeZero(parameters)
                    ? await ServerListCacheFactory.UncachedRequest(zoneId)
                    : await ServerListCacheFactory.Request(zoneId);
                return listServersResponse;
            });
        }

        public async Task RefreshServers(Guid zoneId = default, ItemManagerRequestParameters parameters = default)
        {
            //await App.Current?.Dispatcher?.InvokeAsync((async () =>
            await Task.Run(async () =>
            {
                try
                {
                    if (zoneId == default)
                    {
                        zoneId = ConnectedZoneId;
                    }

                    var managerByZoneId = GetManagerByZoneId(zoneId);
                    var serverList = (await ListServers(managerByZoneId.ZoneId, parameters))?.Servers;
                }
                catch (Exception e)
                {
                    _log?.Info(e);
                }
            });

            //}));
        }


        internal static async Task<ServiceManager> CreateServerConnection(IServer s, UserLogin login)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var sw = new Stopwatch();
                    sw.Start();
                    var client = new CustomServiceClient(
                        $"https://{login.ResolveDomain()}/authority");
                    //client.BearerToken = login.BearerToken;
                    //client.RefreshToken = login.RefreshToken;
                    
                    var cl = await ConnectAsync(client, login,
                        s.ZoneId, 3);
                    var svc = new ServiceManager(cl, $"{s.HostName}:{s.PortApi}");
                    var initialized = await InitializeServiceManager(svc, exception =>
                    {
                        Trace.WriteLine(
                            $"Server Connection Error {exception.GetType()} {exception.Message}");
                    });
                    Trace.WriteLine($"CreateServerConnection took {sw.ElapsedMilliseconds}ms");
                    if (initialized)
                    {
                        return svc;
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
                Trace.WriteLine($"CreateServerConnection {e}");
                return default;
            }
        }

    

        private void HubManagerOnSignalRClientEvent(ESignalRClientEvent clientEvent, Guid zoneId, string data)
        {
            if (clientEvent.Equals(ESignalRClientEvent.ServerCreated))
            {
            }
            else if (clientEvent.Equals(ESignalRClientEvent.ServerDeleted))
            {
            }
            else if (clientEvent.Equals(ESignalRClientEvent.ServerEdited))
            {
            }
        }
    }
}