﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.UI.Common.Interfaces.Ipc;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using Newtonsoft.Json;

namespace Edge360.Platform.VMS.Client.Manager
{
    /// <summary>
    ///     UserId
    /// </summary>
    public class UserItemManager : BaseItemManager<UserItemModel, Guid>
    {
        public FileStorage<ObservableConcurrentDictionary<Guid, UserItemModel>> UserCache =
            new FileStorage<ObservableConcurrentDictionary<Guid, UserItemModel>> {FileName = "UserCache"};

        public DirectModeManager DirectModeMgr { get; set; }

        public ObservableConcurrentDictionary<Guid, UserItemModel> UserCacheDictionary { get; set; } =
            new ObservableConcurrentDictionary<Guid, UserItemModel>();

        public UserItemManager(ILog log = null, DirectModeManager directModeManager = null)
        {
            DirectModeMgr = directModeManager;
            _log = log;
            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;
        }

        public void RequestCacheFile(UserLoginItemModel credentials)
        {
        }

        public async Task<string> BuildCache(UserLoginItemModel userLogin = null)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var serializeObject =
                        JsonConvert.SerializeObject(ItemCollection, ClientJsonUtil.IgnoreExceptionSettings());
                    return serializeObject;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public async Task<bool> SaveCache(UserLoginItemModel userLogin)
        {
            try
            {
                return await Task.Run(async () =>
                    {
                        UserCache.Login = userLogin;
                        UserCache.Storage = ItemCollection;
                        if (ItemCollection.Any())
                        {
                            return UserCache.Save();
                        }

                        return false;
                    }
                );
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return false;
        }

        public async Task<bool> LoadCacheFile(UserLoginItemModel userLogin)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    UserCache.Login = userLogin;
                    UserCache.Load();
                    DirectModeMgr.IsLayoutCacheLoaded = true;
                    return true;
                });
            }
            catch (Exception e)
            {
                DirectModeMgr.IsLayoutCacheLoaded = false;
            }

            return false;
        }

        public async Task EditUser(UserItemModel user)
        {
        }

        public async Task RemoveUser(Guid volumeId)
        {
        }

        public async Task RefreshUser(Guid userId)
        {
        }

        public async Task RefreshUsers()
        {
        }


        private void HubManagerOnSignalRClientEvent(ESignalRClientEvent clientEvent, Guid zoneId, string data)
        {
        }
    }
}