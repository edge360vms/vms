﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.View.Test;
using Edge360.Platform.VMS.Communication;
using Edge360.Raven.Vms.RecordingServiceAgent.ServiceModel.Api.Thumbs;
using Edge360.Raven.Vms.RecordingServiceAgent.ServiceModel.Enums;
using Unity;

namespace Edge360.Platform.VMS.Client.Manager
{
    public class ThumbnailCacheFactory : BaseCacheFactory<ListThumbnailsResponse, DateTimeOffset, Guid>
    {
        public ThumbnailCacheFactory() : base(TimeSpan.FromSeconds(15),
            TimeSpan.FromSeconds(1))
        {
        }

        public override async Task<ListThumbnailsResponse> UncachedRequest((DateTimeOffset, Guid) tuple)
        {
            return await Task.Run(async () =>
            {
                try
                {
                    var svc = CommunicationManager.GetManagerByZoneId();
                    var resp = await svc.ThumbService.ListThumbnails(new ListThumbnails
                    {
                        RecordingProfileId = tuple.Item2,
                        Scale = EThumbnailScale.Minute,
                        StartTime = tuple.Item1
                    });
                    return resp;
                }
                catch (Exception e)
                {
                }

                return default;
            });
        }
        //public override async Task<ListThumbnailsResponse> InternalRequest((DateTimeOffset startTime, Guid recordingProfileId))
        //{
        //    try
        //    {
        //        var svc = CommunicationManager.GetManagerByZoneId(zoneId);
        //        var resp = await svc.ThumbService.ListThumbnails(new ListThumbnails
        //        {
        //            RecordingProfileId = recordingProfileId,
        //            Scale = EThumbnailScale.Minute, StartTime = startTime
        //        });
        //        return resp;
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    return default;
        //}
    }

    public class ThumbnailItemManager : BaseItemManager<ThumbnailItemModel, DateTimeOffset, Guid>
    {
        public static ThumbnailCacheFactory ThumbnailCacheFactory = new ThumbnailCacheFactory();

        public static ThumbnailItemManager Manager =>
            App.GlobalUnityContainer.Resolve<IBaseManager>(nameof(ThumbnailItemManager)) as ThumbnailItemManager;

        public override List<ICacheFactory> CacheFactories { get; set; } = new List<ICacheFactory>
        {
            ThumbnailCacheFactory,
        };

        public async Task<ListThumbnailsResponse> RequestThumbnails(Guid recordingProfile, DateTimeOffset dateTime)
        {
            return await Task.Run(async () =>
            {
                return await ThumbnailCacheFactory.Request((dateTime, recordingProfile));
            });
        }
    }
}