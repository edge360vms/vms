﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using log4net;
using Unity;

namespace Edge360.Platform.VMS.Client.Manager
{
    public class BaseItemManager : IBaseManager
    {
        public ILog _log;

        public DirectModeManager DirectModeMgr { get; set; }

        public BaseItemManager(ILog log = null)
        {
            DirectModeMgr = App.GlobalUnityContainer.Resolve<DirectModeManager>();
            var type = GetType();
            _log = log;
        }

        public virtual List<ICacheFactory> CacheFactories { get; set; }

        public bool HasCacheTimeZero(ItemManagerRequestParameters parameters)
        {
            return parameters?.AcceptedCacheDuration == TimeSpan.Zero;
        }
    }

    public interface IBaseManager
    {
        List<ICacheFactory> CacheFactories { get; set; }
    }

    public class BaseItemManager<T1, T2> : BaseItemManager, IDisposable where T1 : class
    {
        public ObservableConcurrentDictionary<T2, T1> ItemCollection { get; set; } =
            new ObservableConcurrentDictionary<T2, T1>();

        public void Dispose()
        {
            ItemCollection = new ObservableConcurrentDictionary<T2, T1>();
            try
            {
                if (CacheFactories != null)
                {
                    foreach (var c in CacheFactories)
                    {
                        c.Dispose();
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        public virtual void Add(params KeyValuePair<T2, T1>[] items)
        {
            //_log?.Info("Add");
            //Application.Current?.Dispatcher?.InvokeAsync(() =>
            //{
            foreach (var i in items)
            {
                ItemCollection.Add(i.Key, i.Value);
            }

            //});
        }

        public virtual void Remove(params T2[] items)
        {
            //_log?.Info("Remove");
            //Application.Current?.Dispatcher?.InvokeAsync(() =>
            //{
            foreach (var i in items)
            {
                var success = ItemCollection.Remove(i);
            }

            //});
        }

        public virtual void Remove(params KeyValuePair<T2, T1>[] items)
        {
            //_log?.Info("Remove");
            //Application.Current?.Dispatcher?.InvokeAsync(() =>
            //{
            foreach (var i in items)
            {
                var success = ItemCollection.Remove(i.Key);
            }

//            });
        }

        public virtual void Update(params KeyValuePair<T2, T1>[] items)
        {
            //_log?.Info("Update");
            //Application.Current?.Dispatcher?.InvokeAsync(() =>
            //{
            foreach (var i in items)
            {
                ItemCollection.Remove(i.Key);
                ItemCollection.Add(i.Key, i.Value); //,// (tuple, arg2) => i.Value);
            }

            //});
        }
    }

    public class ItemManagerRequestParameters
    {
        public TimeSpan AcceptedCacheDuration { get; set; }
        public TimeSpan CacheDuration { get; set; }
        public bool? FailToRemoteQuery { get; set; }
        public bool? ForceRemoteQuery { get; set; }
        public Guid? GroupId { get; set; }
        public bool RequestDirect { get; set; }
    }

    public class BaseItemManager<T1, T2, T3> : BaseItemManager, IDisposable where T1 : class
    {
        public ObservableConcurrentDictionary<Tuple<T2, T3>, T1> ItemCollection { get; set; } =
            new ObservableConcurrentDictionary<Tuple<T2, T3>, T1>();

        public void Dispose()
        {
            ItemCollection = new ObservableConcurrentDictionary<Tuple<T2, T3>, T1>();
            try
            {
                if (CacheFactories != null)
                {
                    foreach (var c in CacheFactories)
                    {
                        c.Dispose();
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        public virtual void Add(params KeyValuePair<Tuple<T2, T3>, T1>[] items)
        {
            //_log?.Info("Add");
            //Application.Current?.Dispatcher?.InvokeAsync(() =>
            //{
            foreach (var i in items)
            {
                ItemCollection.Add(i.Key, i.Value);
            }

            //});
        }

        public virtual void Remove(params Tuple<T2, T3>[] items)
        {
            // _log?.Info("Remove");
            //Application.Current?.Dispatcher?.InvokeAsync(() =>
            //{
            foreach (var i in items)
            {
                var success = ItemCollection.Remove(i);
            }

            //});
        }

        public virtual void Remove(params KeyValuePair<Tuple<T2, T3>, T1>[] items)
        {
            // _log?.Info("Remove");
            //Application.Current?.Dispatcher?.InvokeAsync(() =>
            //{
            foreach (var i in items)
            {
                var success = ItemCollection.Remove(i.Key);
            }

            //});
        }

        public virtual void Update(params KeyValuePair<Tuple<T2, T3>, T1>[] items)
        {
            // _log?.Info("Update");
            //Application.Current?.Dispatcher?.InvokeAsync(() =>
            //{
            foreach (var i in items)
            {
                ItemCollection.Remove(i.Key);
                ItemCollection.Add(i.Key, i.Value); //,// (tuple, arg2) => i.Value);
            }

            //});
        }
    }
}