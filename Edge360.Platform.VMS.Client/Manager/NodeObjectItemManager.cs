﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.UI.Common.Interfaces.Ipc;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Nodes;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using Newtonsoft.Json;
using Unity;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.Manager
{
    public class NodeBatchRequestCache : BaseCacheFactory<GetNodesResponse, string>
    {
        public NodeBatchRequestCache() : base(TimeSpan.FromSeconds(15), TimeSpan.FromSeconds(15))
        {
        }

        public override async Task<GetNodesResponse> UncachedRequest(string nodes)
        {
            return await Task.Run(async () =>
            {
                return await GetNodesResponse(nodes);
            });
        }

        private async Task<GetNodesResponse> GetNodesResponse(string nodes)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    //Trace.WriteLine($"{DateTimeOffset.UtcNow:u} {nodes}");
                    var resp = await GetManagerByZoneId().NodeService.GetNodeChildren(nodes.Split(',')
                        .Select(o =>
                        {
                            Guid.TryParse(o, out var guid);
                            return (Guid?) guid;
                        }).ToArray()
                    );
                    if (resp != null)
                    {
                        //RequestCacheDictionary.TryAdd(nodes,
                        //    new Tuple<DateTimeOffset, GetNodesResponse>(DateTimeOffset.UtcNow, resp));
                        return resp;
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }
    }

    /// <summary>
    ///     ObjectId
    /// </summary>
    public class NodeObjectItemManager : BaseItemManager<NodeItemModel, Guid>
    {
        public static NodeBatchRequestCache NodeBatchRequestCache { get; set; } = new NodeBatchRequestCache();

        public static NodeObjectItemManager NodeManager =>
            App.GlobalUnityContainer.Resolve<IBaseManager>(nameof(NodeObjectItemManager)) as NodeObjectItemManager;

        public override List<ICacheFactory> CacheFactories { get; set; } = new List<ICacheFactory>
        {
            NodeBatchRequestCache
        };

        public DirectModeManager DirectModeMgr { get; set; }

        public FileStorage<Dictionary<Guid, NodeDescriptor>> NodeCacheDictionary { get; set; } =
            new FileStorage<Dictionary<Guid, NodeDescriptor>> {FileName = "NodeCache"};

        public ConcurrentDictionary<string, Tuple<DateTimeOffset, TaskCompletionSource<GetNodesResponse>>>
            RequestCacheDictionary { get; set; } =
            new ConcurrentDictionary<string, Tuple<DateTimeOffset, TaskCompletionSource<GetNodesResponse>>>();

        public NodeObjectItemManager(ILog log = null, DirectModeManager directModeManager = null)
        {
            _log = log;
            //CountDownFactory = new LazyFactory<string, Task<CountdownOperation>>(async s =>
            //{
            //    if (!CountDownFactory.TryGetValue(s, out var result))
            //    {
            //        return new Dynamic<Task<CountdownOperation>>(async() =>
            //        {

            //            return default;
            //        });
            //    }
            //    var tcs = new TaskCompletionSource<CountdownOperation>();
            //    {

            //    }
            //    var complete = await result;
            //    return await tcs.Task;
            //});

            //if (countdownOperation == null)
            //{
            //    countdownOperation = new CountdownOperation().WithDelay(TimeSpan.FromMilliseconds(300))
            //        .WithStartCallback(async () => { await HandleJoystickAction(value, action); })
            //        .WithCompletedCallback(async () =>
            //        {
            //            await HandleJoystickAction(0, action);
            //            ActiveOperations.TryRemove(action, out _);
            //        }).Begin();
            //    ActiveOperations[action] = countdownOperation;
            //}
            //else
            //{
            //    countdownOperation.WithStartCallback(
            //        async () => { await HandleJoystickAction(value, action); });
            //    // countdownOperation.Restart();
            //    //countdownOperation.Reset();
            //}


            DirectModeMgr = directModeManager;
            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;
        }

        public async Task RemoveNode(bool local = false, params Guid[] objectId)
        {
            await Task.Run(async () =>
            {
                if (DirectModeMgr.IsOfflineMode)
                {
                    return;
                }

                try
                {
                    foreach (var id in objectId)
                    {
                        var existing = ItemCollection.FirstOrDefault(o => o.Key == id);
                        if (!local)
                        {
                            if (existing.Value != null)
                            {
                                await GetManagerByZoneId().NodeService.DeleteNode(existing.Value.Descriptor);
                            }
                        }

                        Remove(existing);
                    }
                }
                catch (Exception e)
                {
                }
            });
        }

        public async Task CreateNode(NodeDescriptor descriptor)
        {
            await Task.Run(async () =>
            {
                if (DirectModeMgr.IsOfflineMode)
                {
                    return;
                }

                try
                {
                    var resp = await GetManagerByZoneId().NodeService.CreateNode(descriptor);
                    if (resp != null)
                    {
                        //Update();
                    }
                }
                catch (Exception e)
                {
                }
            });

            //return default;
        }

        public async Task<NodeItemModel> RefreshNode(Guid objectId, ItemManagerRequestParameters parameters)
        {
            return await Task.Run(async () =>
            {
                if (DirectModeMgr.IsOfflineMode)
                {
                    var cachedDescriptors = NodeCacheDictionary.Storage.Values.FirstOrDefault(o => o.Id == objectId);
                    var resp = new NodeItemModel(cachedDescriptors);
                    return resp;
                    //if (!DirectModeMgr.IsNodeCacheLoaded)
                    //{
                    //    LoadCacheFile();
                    //}
                }

                try
                {
                    if (parameters != null && parameters.AcceptedCacheDuration != TimeSpan.Zero)
                    {
                        var nodeItemModel = ItemCollection.FirstOrDefault(o =>
                            o.Key == objectId && o.Value.LastCachedTime >
                            o.Value.LastCachedTime - parameters.AcceptedCacheDuration).Value;
                        if (nodeItemModel != null)
                        {
                            return nodeItemModel;
                        }
                    }

                    var resp = await GetManagerByZoneId().NodeService.GetNode(objectId);
                    if (resp != null)
                    {
                        var node = resp
                            .Node; //.ToDictionary(o => (Guid)o.Id, descriptor => new NodeItemModel(descriptor));
                        var nodeItemModel = new NodeItemModel(node);
                        Update(new KeyValuePair<Guid, NodeItemModel>((Guid) node.Id, nodeItemModel));

                        return nodeItemModel;
                    }
                }
                catch (Exception e)
                {
                }

                return default;
            });
        }

        public async Task<string> BuildCache()
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var managerByZoneId = GetManagerByZoneId();
                    if (managerByZoneId != null)
                    {
                        var allNodes =
                            await managerByZoneId.NodeService.GetNodeTree(new GetNodeTree {NodeId = Guid.Empty});
                        if (allNodes != null)
                        {
                            allNodes.Nodes.Remove(Guid.Empty);
                            NodeCacheDictionary.Storage = allNodes.Nodes;
                            var serializeObject =
                                JsonConvert.SerializeObject(allNodes.Nodes, ClientJsonUtil.IgnoreExceptionSettings());
                            return serializeObject;
                        }
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
            }

            return default;
        }

        public async Task<bool> SaveCache(UserLoginItemModel userLogin)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var cache = await BuildCache();
                    NodeCacheDictionary.Login = userLogin;
                    if (NodeCacheDictionary.Storage.Any())
                    {
                        return NodeCacheDictionary.Save();
                    }

                    return false;
                });


                //if (!string.IsNullOrEmpty(cache))
                //{
                //    var path = Path.Combine(SettingsManager.VMSAppDataPath,"cache", $"nodeCache-{userLogin.DomainWithoutPort}.dat");
                //    var fStream = new FileStream(path, FileMode.OpenOrCreate);
                //    CacheUtil.EncryptDataToStream(Encoding.ASCII.GetBytes(cache),
                //        Encoding.ASCII.GetBytes(userLogin.GetEntropy()), DataProtectionScope.CurrentUser,
                //        fStream);
                //    fStream.Close();
                //    return true;
                //}
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return false;
        }

        public async Task<bool> LoadCacheFile(UserLoginItemModel userLogin)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    NodeCacheDictionary.Login = userLogin;
                    NodeCacheDictionary.Load();
                    DirectModeMgr.IsNodeCacheLoaded = true;
                    return true;
                });
            }
            catch (Exception e)
            {
                DirectModeMgr.IsNodeCacheLoaded = false;
            }

            return false;
        }

        public async Task<List<NodeItemModel>> RefreshNodeChildren(ItemManagerRequestParameters parameters = null,
            params Guid?[] parentId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var parentsToParse = parentId.ToList();
                    if (DirectModeMgr.IsOfflineMode)
                    {
                        if (!DirectModeMgr.IsNodeCacheLoaded)
                        {
                            return default;
                        }

                        return NodeCacheDictionary.Storage.Values.Where(o => parentsToParse.Contains(o.ParentId))
                            .Select(d => new NodeItemModel(d)).ToList();
                    }

                    var output = new List<NodeItemModel>();

                    if (parameters != null && parameters.AcceptedCacheDuration != TimeSpan.Zero)
                    {
                        foreach (var p in parentsToParse.ToList())
                        {
                            var cachedParent = ItemCollection.FirstOrDefault(o =>
                                o.Key == p && o.Value.Children.Count > 0 && o.Value.LastCachedTime >
                                DateTimeOffset.UtcNow - parameters.AcceptedCacheDuration).Value;
                            if (cachedParent != null)
                            {
                                output.Add(cachedParent);
                                parentsToParse.Remove(p);
                            }
                        }

                        //return ItemCollection.Where(o => o.Value)?.Values?.ToList();
                    }

                    if (parentsToParse.Count > 0)
                    {
                        var array = parentsToParse.Where(o => o != null).Select(g => g.Value)
                            .OrderByDescending(o => o.ToString()).ToArray();
                        var str = string.Join(",", array);
                        var resp = await NodeBatchRequestCache
                            .Request(str); // NodeRequestFactory[str]// await GetManagerByZoneId().NodeService.GetNodeChildren(parentsToParse.ToArray());
                        if (resp != null)
                        {
                            output.AddRange(resp.Nodes.Select(o => new NodeItemModel(o)));
                            var dictionary =
                                resp.Nodes.ToDictionary(o => (Guid) o.Id, descriptor => new NodeItemModel(descriptor));
                            Update(dictionary.ToArray());
                        }
                    }

                    return output;
                });
            }
            catch (Exception e)
            {
            }

            return default;
        }

        public async Task UpdateNodeLabel(NodeDescriptor desc)
        {
            try
            {
                await Task.Run(async () => { await GetManagerByZoneId().NodeService.UpdateNodeLabel(desc); });
            }
            catch (Exception e)
            {
            }
        }

        private async void HubManagerOnSignalRClientEvent(ESignalRClientEvent clientEvent, Guid zoneId, string data)
        {
            try
            {
                await Task.Run(async () =>
                {
                    if (!string.IsNullOrEmpty(data))
                    {
                        var guid = Guid.Parse(data);
                        if (clientEvent.Equals(ESignalRClientEvent.NodeDeleted))
                        {
                            await RemoveNode(true, guid);
                        }
                        else if (clientEvent.Equals(ESignalRClientEvent.NodesUpdated))
                        {
                            await RefreshNode(guid, new ItemManagerRequestParameters());
                        }
                    }
                });
            }
            catch (Exception e)
            {
            }
        }
    }
}