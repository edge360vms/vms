﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.Extensions.CommandLine;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Common.Interfaces.Communication.IPC.Recovery;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.UI.Common.Interfaces.Ipc;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using Newtonsoft.Json;
using Application = System.Windows.Application;

namespace Edge360.Platform.VMS.Client.Manager
{
    public class LayoutItemManager : BaseItemManager<LayoutItemModel, Guid>
    {
        public FileStorage<ObservableConcurrentDictionary<Guid, LayoutItemModel>> LayoutCacheDictionary =
            new FileStorage<ObservableConcurrentDictionary<Guid, LayoutItemModel>> {FileName = "LayoutCache"};

        //public ActiveWindowService ActiveWindowService { get; }
        public DirectModeManager DirectModeMgr { get; set; }

        public FileStorage<LayoutRecoveryCache> LayoutRecoveryCache { get; set; } =
            new FileStorage<LayoutRecoveryCache>();

        public LayoutItemManager(ILog log = null,
            DirectModeManager directModeManager = null)
        {
            _log = log;
            //ActiveWindowService = activeWindowService;
            DirectModeMgr = directModeManager;
            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;
        }

        public async Task<bool> BuildLayoutRecoveryCache()
        {
            return await Task.Run(async () =>
            {
                var monitoringTabs = ObjectResolver.ResolveAll<MonitoringTab>();
                if (monitoringTabs.Count == 0 || CommandLineManager.StartupArgs.LoadRecoveryLayout)
                {
                    return false;
                }

                LayoutRecoveryCache.Storage.RecoveryCacheList.Clear();
                foreach (var m in monitoringTabs.Distinct())
                {
                    if (m.IsVisible)
                    {
                        try
                        {
                            if (Application.Current.Dispatcher != null)
                            {
                                var resp = await Application.Current.Dispatcher.InvokeAsync(() =>
                                {
                                    try
                                    {
                                        var visualParent = m.GetParentWindow<Window>();
                                        if (visualParent != null)
                                        {
                                            var windowInteropHelper = new WindowInteropHelper(visualParent);
                                            var matchingDisplay = Screen.FromHandle(windowInteropHelper.Handle);
                                            var screenIndex = Screen.AllScreens.ToList().IndexOf(matchingDisplay);
                                            var tileGrid = m.MonitoringView.TileGrid.Model;
                                            if (tileGrid != null && tileGrid.Components.OfType<EdgeVideoControl>()
                                                .Any(o => o.VideoObject != null))
                                            {
                                                var itemModel = new RecoveryMonitoringWindowItemModel
                                                {
                                                    DisplayIndex = screenIndex,
                                                    Bounds = visualParent.RenderSize,
                                                    GridSize = new Point(tileGrid.GridSize.Width,
                                                        tileGrid.GridSize.Height),
                                                    RecoveryTiles = new List<RecoveryTileItemModel>()
                                                };

                                                foreach (var videoControl in tileGrid.Components
                                                    .OfType<EdgeVideoControl>().Where(o => o.VideoObject != null))
                                                {
                                                    itemModel.RecoveryTiles.Add(new RecoveryTileItemModel
                                                    {
                                                        GridLocation = new Point(videoControl.TileColumn,
                                                            videoControl.TileRow),
                                                        CameraId = videoControl.VideoObject.Id
                                                    });
                                                }

                                                return itemModel;
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                    }

                                    return default;
                                });

                                if (resp != null)
                                {
                                    LayoutRecoveryCache.Storage.RecoveryCacheList.Add(resp);
                                }
                            }

                            //var matchingDisplay = Screen.AllScreens.FirstOrDefault(o =>
                            //{

                            //    return o.Bounds.IntersectsWith(rectangle);
                            //})
                        }
                        catch (Exception e)
                        {
                        }
                    }
                }

                return true;
            });
        }


        public async Task<bool> SaveRecoveryCache(UserLoginItemModel userLogin)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var built = await BuildLayoutRecoveryCache();
                    if (built)
                    {
                        LayoutRecoveryCache.Login = userLogin;
                        LayoutRecoveryCache.FolderPath =
                            Path.Combine(SettingsManager.VMSAppDataPath, "cache", "recovery");
                        LayoutRecoveryCache.FileName = $"LayoutRecovery-{Process.GetCurrentProcess().Id}";
                        LayoutRecoveryCache.RawJson = true;
                        if (LayoutRecoveryCache?.Storage?.RecoveryCacheList?.Count > 0)
                        {
                            LayoutRecoveryCache?.Save();
                        }

                        var directory =
                            new DirectoryInfo(LayoutRecoveryCache.FolderPath).GetFiles(
                                $"*-{userLogin.DomainWithoutPort}.json");

                        foreach (var c in directory)
                        {
                            try
                            {
                                if (c.LastWriteTimeUtc < DateTime.UtcNow.Subtract(
                                    TimeSpan.FromMinutes(SettingsManager.Settings.Recovery.MaximumLayoutCacheAge ?? 3)))
                                {
                                    File.Delete(c.FullName);
                                }
                            }
                            catch (Exception e)
                            {
                            }
                        }
                    }

                    return true;
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return false;
        }


        public async Task<bool> LoadRecoveryLayout(UserLoginItemModel userLogin)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    Trace.WriteLine("Loading Recovery...");
                    LayoutRecoveryCache.Login = userLogin;
                    LayoutRecoveryCache.RawJson = true;
                    var completeList = new List<RecoveryMonitoringWindowItemModel>();
                    LayoutRecoveryCache.FolderPath = Path.Combine(SettingsManager.VMSAppDataPath, "cache", "recovery");
                    var directory =
                        new DirectoryInfo(LayoutRecoveryCache.FolderPath)
                            .GetFiles("*"); //-{userLogin.DomainWithoutPort}.json");
                    _log?.Info($"Found {directory.Length} cache files!");
                    foreach (var c in directory)
                    {
                        if (c.LastWriteTimeUtc < DateTime.UtcNow.Subtract(
                            TimeSpan.FromMinutes(SettingsManager.Settings?.Recovery?.MaximumLayoutCacheAge ?? 3)))
                        {
                            _log?.Info($"{c.Name} was too old...");
                            continue;
                        }

                        LayoutRecoveryCache.FileName =
                            c.Name; //c.Name.Replace($"-{userLogin.DomainWithoutPort}.json","");

                        var response = await LoadLayoutRecoveryCache(c); // LayoutRecoveryCache.Load();
                        if (response != null)
                        {
                            _log?.Info(
                                $"Adding Response from {c.Name} RecoveryCacheList?.Count:{response?.RecoveryCacheList?.Count}");
                            if (response.RecoveryCacheList != null)
                            {
                                completeList.AddRange(response.RecoveryCacheList);
                            }
                        }
                    }

                    LayoutRecoveryCache.Storage.RecoveryCacheList = completeList;
                    DirectModeMgr.IsLayoutRecoveryCacheLoaded = true;
                    return true;
                });
            }
            catch (Exception e)
            {
                DirectModeMgr.IsLayoutRecoveryCacheLoaded = false;
            }

            return false;
        }

        private async Task<LayoutRecoveryCache> LoadLayoutRecoveryCache(FileInfo c)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var path = c.FullName;
                    var data = File.ReadAllText(path);
                    var layoutRecoveryCache = JsonConvert.DeserializeObject<LayoutRecoveryCache>(data);
                    LayoutRecoveryCache.Storage = layoutRecoveryCache;
                    LayoutRecoveryCache.HasStorageLoaded = true;
                    return layoutRecoveryCache;
                });
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task RemoveLayout(bool local = false, params Guid[] objectId)
        {
            try
            {
                await Task.Run(async () =>
                {
                    if (DirectModeMgr.IsOfflineMode)
                    {
                        return; //LayoutCacheDictionary.Values.ToList();
                    }

                    foreach (var id in objectId)
                    {
                        var existing = ItemCollection.FirstOrDefault(o => o.Key == id);
                        if (!local)
                        {
                            await CommunicationManager.GetManagerByZoneId().GridLayoutService.DeleteGridLayout(id);
                        }

                        Remove(existing);
                    }
                });
            }
            catch (Exception e)
            {
            }
        }

        //public async Task RefreshLayout(Guid objectId)
        //{
        //    try
        //    {
        //        var resp = await CommunicationManager.GetManagerByZoneId().GridLayoutService.GetGridLayout(objectId);
        //        //TODO: this is an interface...
        //        if (resp != null)
        //        {
        //            //var entry = 
        //           // Update(new KeyValuePair<Guid, GridLayoutDescriptor>(resp.Id, resp));
        //          //  return resp;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //    }

        //   // return default;
        //}

        public async Task<string> BuildCache()
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var serializeObject =
                        JsonConvert.SerializeObject(ItemCollection, ClientJsonUtil.IgnoreExceptionSettings());
                    return serializeObject;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public async Task<bool> SaveCache(UserLoginItemModel userLogin)
        {
            try
            {
                return await Task.Run(async () =>
                    {
                        LayoutCacheDictionary.Login = userLogin;
                        LayoutCacheDictionary.Storage = ItemCollection;
                        if (ItemCollection.Any())
                        {
                            return LayoutCacheDictionary.Save();
                        }

                        return false;
                    }
                );
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return false;
        }

        public async Task<bool> LoadCacheFile(UserLoginItemModel userLogin)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    LayoutCacheDictionary.Login = userLogin;
                    LayoutCacheDictionary.Load();
                    DirectModeMgr.IsLayoutCacheLoaded = true;
                    return true;
                });
            }
            catch (Exception e)
            {
                DirectModeMgr.IsLayoutCacheLoaded = false;
            }

            return false;
        }

        public async Task<List<LayoutItemModel>> RefreshLayouts(
            ItemManagerRequestParameters parameters = default)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (DirectModeMgr.IsOfflineMode)
                    {
                        return LayoutCacheDictionary.Storage.Values.ToList();
                    }

                    var resp = await CommunicationManager.GetManagerByZoneId().GridLayoutService.ListGridLayouts();
                    if (resp != null)
                    {
                        var itemManagerRequestParameters = parameters;
                        if (itemManagerRequestParameters != null)
                        {
                            if ((bool) itemManagerRequestParameters.ForceRemoteQuery && parameters.GroupId != null)
                            {
                                await PermissionItemManager.PermissionManager.RequestPermissions(
                                    (Guid) parameters.GroupId,
                                    Guid.Empty);
                                //await PermissionItemManager.PermissionManager.RequestPermissionsBatch(
                                //    (Guid) parameters.GroupId,
                                //    resp.Select(o => o.Id).ToArray());
                            }

                            itemManagerRequestParameters.ForceRemoteQuery = false;
                        }

                        var nodeModels = new List<LayoutItemModel>();
                        foreach (var gridLayout in resp)
                        {
                            try
                            {
                                var deserialized = JsonConvert.DeserializeObject<LayoutItemModel>(gridLayout.LayoutData,
                                    JsonUtil.IgnoreExceptionSettings());
                                if (deserialized != null)
                                {
                                    deserialized.InitGridLayout(gridLayout);

                                    if (itemManagerRequestParameters != null)
                                    {
                                        var permissionItemModel =
                                            await PermissionItemManager.PermissionManager.FindPermission(
                                                deserialized.Id,
                                                (Guid) itemManagerRequestParameters.GroupId,
                                                itemManagerRequestParameters);
                                        deserialized.Permissions =
                                            permissionItemModel ?? new PermissionItemModel();
                                    }

                                    nodeModels.Add(deserialized);
                                }
                            }
                            catch (Exception e)
                            {
                            }
                        }

                        var dictionary = nodeModels.ToDictionary(o => o.Id, descriptor => descriptor);
                        var removedItems = ItemCollection.Where(o => !dictionary.ContainsKey(o.Key));

                        Remove(removedItems.ToArray());
                        Update(dictionary.ToArray());
                        //BuildCache();
                        return nodeModels;
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
            }

            return default;
        }

        private async void HubManagerOnSignalRClientEvent(ESignalRClientEvent clientEvent, Guid zoneId, string data)
        {
            try
            {
                await Task.Run(async () =>
                {
                    if (!string.IsNullOrEmpty(data))
                    {
                        var guid = Guid.Parse(data);
                        if (clientEvent.Equals(ESignalRClientEvent.GridLayoutUpdated))
                        {
                            await RefreshLayouts();
                        }
                    }
                });
            }
            catch (Exception e)
            {
            }
        }
    }
}