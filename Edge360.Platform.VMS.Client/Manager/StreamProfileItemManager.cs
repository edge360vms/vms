﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.ItemModel.Stream;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.RecordingProfiles;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.StreamProfiles;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.StreamingProfile;
using Edge360.Raven.Vms.CameraService.ServiceModel.Enums;
using Edge360.Vms.AuthorityService.SharedModel.Enums;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using ServiceStack;
using Unity;

namespace Edge360.Platform.VMS.Client.Manager
{
    public class ListGlobalStreamProfileCache : BaseCacheFactory<ListStreamProfilesResponse, Guid, Guid>
    {
        public ListGlobalStreamProfileCache() : base(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(3))
        {
        }

        public override async Task<ListStreamProfilesResponse> UncachedRequest((Guid, Guid ) tuple)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var managerByZoneId = CommunicationManager.GetManagerByZoneId(Guid.Empty); //tuple.Item1
                    if (managerByZoneId != null)
                    {
                        var profiles = await managerByZoneId.GlobalStreamProfileService
                            .ListStreamingProfiles(new ListStreamProfiles {CameraId =  tuple.Item2});

                        if (profiles != null)
                        {
                            return profiles;
                        }
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
            }

            return default;
        }
    }

    public class ListStreamProfileCache : BaseCacheFactory<ListStreamingProfilesResponse, Guid, Guid>
    {
        public ListStreamProfileCache() : base(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(3))
        {
        }

        public override async Task<ListStreamingProfilesResponse> UncachedRequest((Guid, Guid ) tuple)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var managerByZoneId = CommunicationManager.GetManagerByZoneId(tuple.Item1);
                    if (managerByZoneId != null)
                    {
                        var profiles = await managerByZoneId.StreamProfileService
                            .ListStreamingProfiles(new ListStreamingProfiles {CameraId = tuple.Item2});

                        if (profiles != null)
                        {
                            return profiles;
                        }
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
            }

            return default;
        }
    }


    /// <summary>
    ///     StreamProfileId, CameraId
    /// </summary>
    public class StreamProfileItemManager : BaseItemManager<StreamProfileItemModel, Guid, Guid>
    {
        
        public static bool FilterRemoteStream(Guid zoneId, KeyValuePair<Tuple<Guid, Guid>, StreamProfileItemModel> model)
        {
            if (zoneId != CommunicationManager.ConnectedZoneId)
            {
                if ((CommunicationManager.EffectivePermissions & ERoles.RemoteStreamingOverride) == 0)
                {
                    var valuePropertyFlags = (EStreamProfileProperty) model.Value.PropertyFlags;
                    if (!valuePropertyFlags.HasFlag(EStreamProfileProperty.IsRemoteProfile))
                    {
                        return false;
                    }
                }
            }

            return true;
        }  


        public async Task<ListStreamProfilesResponse> RefreshGlobalProfiles(Guid cameraId, Guid zoneId = default)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (zoneId == Guid.Empty)
                    {
                        zoneId = CommunicationManager.ConnectedZoneId;
                    }

                    var profiles = await ListGlobalStreamProfileCache.Request((zoneId, cameraId));
                    if (profiles != null)
                    {
                        var streamProfileItemModels =
                            profiles.StreamProfiles.ToDictionary(o => new Tuple<Guid, Guid>(o.ProfileId, o.CameraId),
                                b => (StreamProfileItemModel) b);

                        Update(streamProfileItemModels.Where(o => o.Key.Item1 != Guid.Empty && !ItemCollection.Any(remote => remote.Key.Item1 == o.Key.Item1 && !string.IsNullOrEmpty(o.Value.RtspUrl))).ToArray());
                    }

                    return profiles;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }


        public static ListGlobalStreamProfileCache ListGlobalStreamProfileCache { get; set; } = new ListGlobalStreamProfileCache();

        public static ListStreamProfileCache ListStreamProfileCache { get; set; } = new ListStreamProfileCache();

        public static StreamProfileItemManager Manager =>
            App.GlobalUnityContainer.Resolve<IBaseManager>(
                nameof(StreamProfileItemManager)) as StreamProfileItemManager;

        public override List<ICacheFactory> CacheFactories { get; set; } = new List<ICacheFactory>
        {
            ListStreamProfileCache,
            ListGlobalStreamProfileCache,
        };

        public StreamProfileItemManager(ILog log = null)
        {
            _log = log;

            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;
        }

        public StreamProfileItemModel GetNearestStreamProfileForResolution(CameraItemModel cameraItem, double ownerWidth)
        {
            try
            {
                if (DirectModeMgr.IsOfflineMode)
                {
                    return default;
                }

                var profileForOnvif = ItemCollection.Where(o =>
                    {
                        return o.Key.Item2 == cameraItem.Id && FilterRemoteStream(cameraItem.ZoneId, o) &&
                               !o.Value.StreamProfileProperty.HasFlag(EStreamProfileProperty.IsAutoSelectDisabled);
                    })
                    .OrderByDescending(o => o.Value.Format != null && o.Value != null && o.Value.Format.ToLowerInvariant().Contains("h264"))
                    .ThenBy(o => { return Math.Abs(ownerWidth - o.Value.ResolutionWidth); }).FirstOrDefault().Value;
                return profileForOnvif ?? ItemCollection?.FirstOrDefault(o => o.Key.Item2 == cameraItem.Id).Value;
            }
            catch (Exception e)
            {
                _log?.Info(e);
                return default;
            }
        }

        public async Task<bool> EditProfile(StreamProfileItemModel profile)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (DirectModeMgr.IsOfflineMode)
                    {
                        return false;
                    }

                    if (profile != null)
                    {
                        var updateStreamingProfile = profile.ConvertTo<UpdateStreamingProfile>();
                        updateStreamingProfile.User = profile.Username;
                        var resp = await CommunicationManager.GetManagerByZoneId(profile.ZoneId).StreamProfileService
                            .UpdateStreamingProfile(updateStreamingProfile);
                        if (resp != null)
                        {
                            await RefreshProfile(profile.ZoneId, resp.Id);
                        }

                        return true;
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
            }

            return false;
        }

        public async Task RefreshProfile(Guid zoneId, Guid id)
        {
            await Task.Run(async () =>
            {
                if (DirectModeMgr.IsOfflineMode)
                {
                    return;
                }

                if (zoneId != Guid.Empty)
                {
                    var profiles = await CommunicationManager.GetManagerByZoneId(zoneId).StreamProfileService
                        .GetStreamingProfile(new GetStreamingProfile {Id = id});
                    var convertedProfiles = profiles.StreamingProfile.Select(o =>
                    {
                        var itemModel = (StreamProfileItemModel) o;
                        itemModel.ZoneId = zoneId;
                        return itemModel;
                    }).ToDictionary(o => new Tuple<Guid, Guid>(o.Id, o.CameraId), profile => profile);
                    Update(convertedProfiles.ToArray());
                }
            });
        }

        public async Task RefreshProfiles(CameraItemModel camera, ItemManagerRequestParameters parameters = default)
        {
            if (camera != null)
            {
                try
                {
                    await Task.Run(async () =>
                    {
                        if (DirectModeMgr.IsOfflineMode)
                        {
                        }
                        else
                        {
                            //var profilesOfCamera = ItemCollection.Keys.Where(o => o.Item2 == camera.Id);

                            var managerByZoneId = CommunicationManager.GetManagerByZoneId(camera.ZoneId);
                            if (managerByZoneId != null)
                            {
                                var profiles = parameters?.AcceptedCacheDuration == TimeSpan.Zero
                                    ? await ListStreamProfileCache.UncachedRequest((camera.ZoneId, camera.Id))
                                    : await ListStreamProfileCache.Request((camera.ZoneId, camera.Id));

                                //StreamProfiles have no permissions.
                                //var itemManagerRequestParameters = parameters;

                                //if (itemManagerRequestParameters != null)
                                //{
                                //    if ((bool) itemManagerRequestParameters.ForceRemoteQuery && parameters.GroupId != null)
                                //    {
                                //        await PermissionItemManager.PermissionManager.RequestPermissionsBatch((Guid) parameters.GroupId,
                                //            profiles.StreamingProfile.Select(o => o.Id).ToArray());
                                //    }
                                //    itemManagerRequestParameters.ForceRemoteQuery = false;
                                //}

                                if (profiles != null)
                                {
                                    var convertedProfiles = new List<StreamProfileItemModel>();
                                    //Remove(profilesOfCamera.ToArray());
                                    if (profiles.StreamingProfile != null)
                                    {
                                        foreach (var s in profiles.StreamingProfile)
                                        {
                                            var itemModel = await AddProperties(camera, s, parameters);
                                            convertedProfiles.Add(itemModel);
                                        }
                                    }

                                    var output =
                                        convertedProfiles.ToDictionary(o => new Tuple<Guid, Guid>(o.Id, o.CameraId),
                                            profile => profile);
                                    Update(output.ToArray());
                                }
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        }

        private static async Task<StreamProfileItemModel> AddProperties(CameraItemModel camera,
            StreamingProfileDescriptor streamProfile, ItemManagerRequestParameters parameters)
        {
            return await Task.Run(async () =>
            {
                var itemModel = (StreamProfileItemModel) streamProfile;
                itemModel.ZoneId = camera.ZoneId;
                //itemModel.Permissions = await PermissionItemManager.PermissionManager.FindPermission(camera.Id, (Guid) parameters?.GroupId) ?? new PermissionItemModel();
                return itemModel;
            });
        }

        public async Task RemoveProfile(StreamProfileItemModel profile)
        {
            try
            {
                await Task.Run(async () =>
                {
                    if (DirectModeMgr.IsOfflineMode)
                    {
                        return;
                    }

                    if (profile != null)
                    {
                        var resp = await CommunicationManager.GetManagerByZoneId(profile.ZoneId)
                            .StreamProfileService
                            .DeleteStreamingProfile(new DeleteStreamingProfile {Id = profile.Id});
                        if (resp != null)
                        {
                            Remove(new Tuple<Guid, Guid>(profile.Id, profile.CameraId));
                        }
                    }
                });
            }
            catch (Exception e)
            {
            }
        }

        private async void HubManagerOnSignalRClientEvent(ESignalRClientEvent eventClient, Guid zoneId, string data)
        {
            try
            {
                await Task.Run(async () =>
                {
                    if (DirectModeMgr.IsOfflineMode)
                    {
                        return;
                    }

                    if (!string.IsNullOrEmpty(data))
                    {
                        var guid = Guid.Parse(data);
                        if (guid != Guid.Empty)
                        {
                            if (eventClient.Equals(ESignalRClientEvent.StreamProfileEdited))
                            {
                                await RefreshProfile(zoneId, guid);
                            }
                            else if (eventClient.Equals(ESignalRClientEvent.StreamProfileDeleted))
                            {
                                var existing = ItemCollection.FirstOrDefault(o => o.Value.Id == guid);
                                if (existing.Value != null)
                                {
                                    await RemoveProfile(existing.Value);
                                }
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
            }
        }
    }
}