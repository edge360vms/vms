﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Common.Interfaces.Authority;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Communication.Services;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Zones;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using Unity;

namespace Edge360.Platform.VMS.Client.Manager
{
    public class ZoneServiceManagerCacheFactory : BaseCacheFactory<ServiceManager, Guid>
    {
        public ZoneServiceManagerCacheFactory() : base(TimeSpan.FromDays(10), TimeSpan.FromSeconds(3))
        {
        }

        public override Task<ServiceManager> UncachedRequest(Guid zoneId)
        {
            return GetManagerByZoneIdConnectIfFailInternal(zoneId);
        }

        private static async Task<ServiceManager> GetManagerByZoneIdConnectIfFailInternal(Guid? zoneId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var managerByZoneId = CommunicationManager.GetManagerByZoneId(zoneId ?? Guid.Empty);
                    if (managerByZoneId == null)
                    {
                        var currentConnectionSvc = CommunicationManager.GetManagerByZoneId(Guid.Empty);
                        var connectToOtherZone =
                            await ServerItemManager.Manager.ListServers(zoneId ?? Guid.Empty);
                        if (connectToOtherZone != null)
                        {
                            var server = connectToOtherZone.Servers.FirstOrDefault();

                            if (server != null)
                            {
                                managerByZoneId = await ServerItemManager.CreateServerConnection(server, new UserLogin
                                {
                                    Domain = server.HostName,
                                    IsBearerLogin = true,
                                    UserName = CommunicationManager.CurrentUser.Username,
                                    BearerToken = currentConnectionSvc.Client.BearerToken,
                                    RefreshToken = currentConnectionSvc.Client.RefreshToken,
                                    ApiPort = (ushort) server.PortApi
                                });
                            }
                        }
                    }

                    return managerByZoneId;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }
    }

    public class ZoneListCacheFactory : BaseCacheFactory<ListZonesResponse, string>
    {
        public ZoneListCacheFactory() : base(TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(3))
        {
        }

        public override async Task<ListZonesResponse> UncachedRequest(string str)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var managerByZoneId = CommunicationManager.GetManagerByZoneId();
                    var zoneList = await managerByZoneId.GlobalZoneService
                        .ListZones();
                    foreach (var z in zoneList.Zones)
                    {
                        if (z != null)
                        {
                            var zoneItemModel = new ZoneItemModel
                            {
                                Label = z.ZoneName,
                                ZoneId = z.ZoneId
                            };
                            ZoneItemManager.Manager.Update(
                                new KeyValuePair<Guid, ZoneItemModel>(z.ZoneId, zoneItemModel));
                        }
                    }

                    return zoneList;
                });
            }
            catch (Exception e)
            {
            }

            return default;
        }
    }

    /// <summary>
    ///     ZoneId
    /// </summary>
    public class ZoneItemManager : BaseItemManager<ZoneItemModel, Guid>
    {
        public static ZoneItemManager Manager =>
            App.GlobalUnityContainer.Resolve<IBaseManager>(nameof(ZoneItemManager)) as ZoneItemManager;

        public static ZoneListCacheFactory ZoneListCacheFactory { get; set; } = new ZoneListCacheFactory();

        public static ZoneServiceManagerCacheFactory ZoneServiceManagerCacheFactory { get; set; } =
            new ZoneServiceManagerCacheFactory();

        public override List<ICacheFactory> CacheFactories { get; set; } = new List<ICacheFactory>
        {
            ZoneListCacheFactory,
            ZoneServiceManagerCacheFactory,
        };

        public bool IsRefreshingZones { get; set; }

        public ZoneItemManager(ILog log = null)
        {
            _log = log;
            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;
        }


        public static async Task<ServiceManager> GetManagerByZoneIdConnectIfFail(Guid? zoneId)
        {
            return await Task.Run(async () =>
            {
                if (zoneId != null)
                {
                    if (zoneId == null || zoneId == Guid.Empty)
                    {
                        zoneId = CommunicationManager.ConnectedZoneId;
                    }

                    var request = zoneId ?? Guid.Empty;
                    var serviceManager =
                        await ZoneServiceManagerCacheFactory.Request(request); //ZoneRequestFactory[request].Value;
                    return serviceManager;
                }

                return default;
            });
        }

        public ZoneItemModel GetZone(Guid zoneId = default)
        {
            return ItemCollection
                ?.FirstOrDefault(o => o.Key == (zoneId == default ? CommunicationManager.ConnectedZoneId : zoneId))
                .Value ?? default;
        }

        public async Task RefreshZone(Guid zoneId)
        {
            await Task.Run(async () =>
            {
                var svc = await GetManagerByZoneIdConnectIfFail(zoneId);
                var zone = await svc.GlobalZoneService.GetZone(new GetZone {ZoneId = zoneId});
                if (zone != null)
                {
                    //var zoneItemModel = new ZoneItemModel
                    //{
                    //    Label = zone.Zone.ZoneName,
                    //    ZoneId = zone.Zone.ZoneId
                    //};

                    await ServerItemManager.Manager.RefreshServers(zone.Zone.ZoneId);
                }
            });
        }

        public async Task<ListZonesResponse> ListZones(
            ItemManagerRequestParameters parameters = default)
        {
            return await Task.Run(async () =>
            {
                var listServersResponse = HasCacheTimeZero(parameters)
                    ? await ZoneListCacheFactory.UncachedRequest("global")
                    : await ZoneListCacheFactory.Request("global");
                return listServersResponse;
            });
        }


        public async Task<List<ZoneItemModel>> RefreshZones(bool includeServerUpdate = true, bool noCache = false)
        {
            if (!IsRefreshingZones)
            {
                Trace.WriteLine($"RefreshZones {includeServerUpdate} {noCache}");

                IsRefreshingZones = true;
                await Task.Run(async () =>
                {
                    try
                    {
                        //zones.Clear();
                        //   var zoneItemModels = new ObservableCollection<ZoneItemModel>();
                        // await LoadZones(zoneItemModels, false);
                        await ListZones(noCache
                            ? new ItemManagerRequestParameters {AcceptedCacheDuration = TimeSpan.Zero}
                            : null);
                    }
                    catch (Exception e)
                    {
                    }
                    finally
                    {
                        IsRefreshingZones = false;
                    }
                });
            }

            return default;
        }

        private void HubManagerOnSignalRClientEvent(ESignalRClientEvent clientEvent, Guid zoneId, string data)
        {
        }
    }
}