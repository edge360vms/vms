﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Permissions;
using Unity;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.Manager
{
    /// <summary>
    ///     PermissionId, GroupId
    /// </summary>
    public class PermissionItemManager : BaseItemManager<PermissionItemModel, Guid, Guid>
    {
        public static PermissionItemManager PermissionManager =>
            App.GlobalUnityContainer.Resolve<IBaseManager>(nameof(PermissionItemManager)) as PermissionItemManager;


        //public async Task<List<PermissionItemModel>> RequestPermissionsBatch(Guid groupId, params Guid[] objectIds)
        //{
        //    try
        //    {
        //        var output = new List<PermissionItemModel>();
        //        if (objectIds.Length == 1)
        //        {
        //            var getPermissionsByGroupResponse = await RequestPermissions(groupId, objectIds.FirstOrDefault());

        //            foreach (var permission in getPermissionsByGroupResponse)
        //            {
        //                output.Add(permission);
        //                Update(new KeyValuePair<Tuple<Guid, Guid>, PermissionItemModel>(new Tuple<Guid, Guid>(Guid.Parse(permission.ObjectId) , Guid.Parse(permission.GroupId)), permission.Convert<PermissionItemModel>()));
        //            }
        //            return output;
        //        }
        //        else
        //        {
        //            var requests = objectIds.Select(o => new GetPermissionsByGroup()
        //            {
        //                GroupId = groupId.ToString(),
        //                Id = o.ToString()
        //            }).ToArray();
        //            var resp = await GetManagerByZoneId().PermissionService.Client.Request(requests);

        //            foreach (var r in resp.Where(o => o != null))
        //            {
        //                //var index = resp.IndexOf(r);
        //                //var request = requests[index];
        //                foreach (var permission in r.Results)
        //                {
        //                    var permissionItemModel = permission.Convert<PermissionItemModel>();
        //                    output.Add(permissionItemModel);
        //                    Update(new KeyValuePair<Tuple<Guid, Guid>, PermissionItemModel>(new Tuple<Guid, Guid>(Guid.Parse(permission.ObjectId) , Guid.Parse(permission.GroupId)), permissionItemModel));
        //                }
        //            }

        //            return output;
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        return default;
        //    }
        //}


        public async Task<List<PermissionItemModel>> RequestPermissions(Guid groupId, Guid objectId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var resp = await GetManagerByZoneId().PermissionService.GetPermissionsByGroup(
                        new GetPermissionsByGroup
                        {
                            GroupId = groupId.ToString(),
                            Id = objectId.ToString()
                        });

                    var output = new List<PermissionItemModel>();
                    if (resp?.Results != null)
                    {
                        foreach (var permission in resp.Results)
                        {
                            var permissionItemModel = permission.Convert<PermissionItemModel>();
                            output.Add(permissionItemModel);
                            Update(new KeyValuePair<Tuple<Guid, Guid>, PermissionItemModel>(
                                new Tuple<Guid, Guid>(Guid.Parse(permission.ObjectId), Guid.Parse(permission.GroupId)),
                                permissionItemModel));
                        }
                    }

                    return output;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public async Task<PermissionItemModel> FindPermission(Guid objectId, Guid? groupId,
            ItemManagerRequestParameters parameters = default)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (parameters != null && (bool) parameters.ForceRemoteQuery)
                    {
                        if (groupId != null)
                        {
                            var resp = await RequestPermissions(objectId, (Guid) groupId);
                        }

                        //if (resp != null)
                        //{
                        //    return resp.Where(o => o != null && !string.IsNullOrEmpty(o.ObjectId)).FirstOrDefault(o =>
                        //    {
                        //        var argObjectId = Guid.Parse(o.ObjectId);
                        //        return argObjectId == objectId;
                        //    });
                        //}
                    }

                    var permissionItemModel = ItemCollection
                        ?.FirstOrDefault(o => o.Key.Item1 == objectId && o.Key.Item2 == groupId).Value;
                    return permissionItemModel;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public async Task<DeletePermissionsResponse> RemovePermissions(Guid groupId, Guid permissionId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var resp = await GetManagerByZoneId().PermissionService.DeletePermissions(new DeletePermissions
                    {
                        GroupId = groupId.ToString(),
                        Id = permissionId.ToString()
                    });
                    return resp;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public async Task<SetPermissionResponse> UpdatePermissions(Guid groupId, Guid permissionId, int value)
        {
            try
            {
                return await Task.Run(async () =>
                    {
                        var resp = await GetManagerByZoneId().PermissionService.SetPermission(new SetPermission
                        {
                            GroupId = groupId.ToString(),
                            ObjectId = permissionId.ToString(),
                            Value = value
                        });
                        return resp;
                    }
                );
            }
            catch (Exception e)
            {
                return default;
            }
        }
    }
}