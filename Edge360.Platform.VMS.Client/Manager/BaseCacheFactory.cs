﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using WispFramework.Patterns.Generators;

namespace Edge360.Platform.VMS.Client.Manager
{
    public abstract class BaseCacheFactory<T1, T2, T3> : ICacheFactory where T1 : new()
    {
        public LazyFactory<(T2, T3),
            Dynamic<Task<T1>>> CacheRequestFactory;

        public TimeSpan CacheDuration { get; set; }

        public char Delimiter { get; set; } = '×';

        public ConcurrentDictionary<(T2, T3), Tuple<DateTimeOffset, TaskCompletionSource<T1>>>
            RequestCacheDictionary { get; set; } =
            new ConcurrentDictionary<(T2, T3), Tuple<DateTimeOffset, TaskCompletionSource<T1>>>();

        public TimeSpan ThrottleSpan { get; set; }

        public BaseCacheFactory(TimeSpan cacheDuration = default, TimeSpan throttleDuration = default)
        {
            ThrottleSpan = throttleDuration == TimeSpan.Zero ? TimeSpan.FromMilliseconds(100) : throttleDuration;
            CacheDuration = cacheDuration == TimeSpan.Zero ? TimeSpan.FromSeconds(10) : cacheDuration;

            CacheRequestFactory =
                new LazyFactory<(T2, T3),
                    Dynamic<Task<T1>>>(
                    tuple =>
                    {
                        return new Dynamic<Task<T1>>(async () =>
                        {
                            return await Task.Run(async () =>
                            {
                                if (!CacheEmptyResults)
                                {
                                    foreach (var c in RequestCacheDictionary.Where(o => o.Value == null))
                                    {
                                        RequestCacheDictionary.TryRemove(c.Key, out _);
                                    }
                                }

                                foreach (var c in RequestCacheDictionary.Where(o =>
                                    o.Value.Item1 < DateTimeOffset.UtcNow - cacheDuration))
                                {
                                    RequestCacheDictionary.TryRemove(c.Key, out _);
                                }

                                if (RequestCacheDictionary.TryGetValue(tuple, out var result))
                                {
                                    return await result.Item2.Task;
                                }

                                var tcs = new TaskCompletionSource<T1>();
                                if (RequestCacheDictionary.TryAdd(tuple,
                                    new Tuple<DateTimeOffset, TaskCompletionSource<T1>>(DateTimeOffset.UtcNow,
                                        tcs)))
                                {
                                }

                                var getDataResponse = await UncachedRequest(tuple);
                                tcs.SetResult(getDataResponse);
                                return getDataResponse;
                            });
                        }).Throttled(ThrottleSpan);
                    });
        }

        public void Dispose()
        {
            RequestCacheDictionary?.Clear();
            CacheRequestFactory?.Clear();
        }

        public bool CacheEmptyResults { get; set; }

        public async Task<T1> Request((T2, T3) tuple)
        {
            return await Task.Run(async () => { return await CacheRequestFactory[tuple].Value; });
        }

        public abstract Task<T1> UncachedRequest((T2, T3) tuple);
    }


    public abstract class BaseCacheFactory<T1, T2> : ICacheFactory
    {
        public LazyFactory<T2,
            Dynamic<Task<T1>>> CacheRequestFactory;

        public TimeSpan CacheDuration { get; set; }
        public string Delimiter { get; set; } = "×";

        public ConcurrentDictionary<T2, Tuple<DateTimeOffset, TaskCompletionSource<T1>>>
            RequestCacheDictionary { get; set; } =
            new ConcurrentDictionary<T2, Tuple<DateTimeOffset, TaskCompletionSource<T1>>>();

        public TimeSpan ThrottleSpan { get; set; }

        public BaseCacheFactory(TimeSpan cacheDuration = default, TimeSpan throttleDuration = default)
        {
            ThrottleSpan = throttleDuration == TimeSpan.Zero ? TimeSpan.FromSeconds(10) : throttleDuration;
            CacheDuration = cacheDuration == TimeSpan.Zero ? TimeSpan.FromSeconds(10) : cacheDuration;

            CacheRequestFactory =
                new LazyFactory<T2,
                    Dynamic<Task<T1>>>(
                    tuple =>
                    {
                        return new Dynamic<Task<T1>>(async () =>
                        {
                            return await Task.Run(async () =>
                            {
                                if (!CacheEmptyResults)
                                {
                                    foreach (var c in RequestCacheDictionary.Where(o => o.Value == null))
                                    {
                                        RequestCacheDictionary.TryRemove(c.Key, out _);
                                    }
                                }

                                foreach (var c in RequestCacheDictionary.Where(o =>
                                    o.Value.Item1 < DateTimeOffset.UtcNow - cacheDuration))
                                {
                                    RequestCacheDictionary.TryRemove(c.Key, out _);
                                }

                                if (RequestCacheDictionary.TryGetValue(tuple, out var result))
                                {
                                    return await result.Item2.Task;
                                }

                                var tcs = new TaskCompletionSource<T1>();
                                if (RequestCacheDictionary.TryAdd(tuple,
                                    new Tuple<DateTimeOffset, TaskCompletionSource<T1>>(DateTimeOffset.UtcNow,
                                        tcs)))
                                {
                                }

                                var getDataResponse = await UncachedRequest(tuple);
                                tcs.SetResult(getDataResponse);
                                return getDataResponse;
                            });
                        }).Throttled(ThrottleSpan);
                    });
        }

        public void Dispose()
        {
            CacheRequestFactory?.Clear();
            RequestCacheDictionary?.Clear();
        }

        public bool CacheEmptyResults { get; set; }

        public async Task<T1> Request(T2 key)
        {
            return await Task.Run(async () => {return await CacheRequestFactory[key].Value;});
        }

        public abstract Task<T1> UncachedRequest(T2 zoneId);
    }

    public interface ICacheFactory : IDisposable
    {
        bool CacheEmptyResults { get; set; }
    }
}