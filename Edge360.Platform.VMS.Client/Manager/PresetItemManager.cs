﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.CameraAdapter;
using Edge360.Raven.Vms.CameraService.ServiceModel.ItemModels;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using ServiceStack;
using Unity;

namespace Edge360.Platform.VMS.Client.Manager
{
    public class PresetCache : BaseCacheFactory<List<PtzPreset>, Guid, Guid>
    {
        public PresetCache() : base(TimeSpan.FromSeconds(30), TimeSpan.FromSeconds(10))
        {
            
        }

        public override Task<List<PtzPreset>> UncachedRequest((Guid, Guid) tuple)
        {
            return null;
        }
    }

    public class PresetBatchCache : BaseCacheFactory<List<PtzPreset>, string, Guid>
    {
        public PresetBatchCache() : base(TimeSpan.FromMinutes(5), TimeSpan.FromSeconds(10))
        {
        }

        public override async Task<List<PtzPreset>> UncachedRequest((string, Guid) tuple)
        {
            return await Task.Run(async () =>
            {
                var guids = tuple.Item1.Split(',').Select(o => Guid.Parse(
                    o)).ToArray();
                return await PresetItemManager.Manager.RefreshPresetsInternal(tuple.Item2, default, guids);
            });
        }
    }

    /// <summary>
    ///     CameraId, PresetToken
    /// </summary>
    public class PresetItemManager : BaseItemManager<PtzPresetItemModel, Guid, string>
    {
        public static PresetItemManager Manager =>
            App.GlobalUnityContainer.Resolve<IBaseManager>(nameof(PresetItemManager)) as PresetItemManager;

        public static PresetBatchCache PresetBatchCache { get; set; } = new PresetBatchCache();
        public static PresetCache PresetCache { get; set; } = new PresetCache();
        public override List<ICacheFactory> CacheFactories { get; set; } = new List<ICacheFactory>
        {
            PresetBatchCache,
            PresetCache,
        };


        //public LazyFactory<(string, Guid, ItemManagerRequestParameters),
        //    Dynamic<Task<List<PtzPreset>>>> PresetRequestFactory;

        public ConcurrentDictionary<string, Tuple<DateTimeOffset, TaskCompletionSource<List<PtzPreset>>>>
            RequestCacheDictionary { get; set; } =
            new ConcurrentDictionary<string, Tuple<DateTimeOffset, TaskCompletionSource<List<PtzPreset>>>>();

        public PresetItemManager(ILog log = null)
        {
            _log = log;
            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;
        }

        //public async Task RefreshPresets(CameraItemModel camera, ItemManagerRequestParameters parameters)
        //{
        //    if (DirectModeMgr.IsDirectMode)
        //    {
        //        var previousValues =
        //            camera.PresetItemManager.ItemCollection.Where(o => o.Key.Item1 == camera.Id)
        //                .Select(o => o.Key).ToArray();
        //        camera.PresetItemManager.Remove(previousValues);

        //        var ptzManager = new ONVIFManager(camera.Address, camera.Username, camera.Password);
        //        await ptzManager.RefreshPresets();

        //        var resp = ptzManager?.OnvifCameraConnection
        //            .CameraPresets;

        //        if (resp != null)
        //        {
        //            foreach (var r in resp.Preset)
        //            {
        //                camera.PresetItemManager.Add(new KeyValuePair<Tuple<Guid, string>, PtzPresetItemModel>(
        //                    new Tuple<Guid, string>(camera.Id, r.token), new PtzPresetItemModel
        //                    {
        //                        Label = r.Name,
        //                        Pan = r.PTZPosition.PanTilt.x,
        //                        Tilt = r.PTZPosition.PanTilt.y,
        //                        Token = r.token,
        //                        Zoom = r.PTZPosition.Zoom.x,
        //                        CachedTime = DateTimeOffset.UtcNow
        //                    }));
        //            }
        //        }
        //    }
        //    else
        //    {
        //        var previousValues =
        //            camera.PresetItemManager.ItemCollection.Where(o => o.Key.Item1 == camera.Id)
        //                .Select(o => o.Key).ToArray();

        //        var resp = await CommunicationManager.GetManagerByZoneId(camera.ZoneId).CameraAdapterService
        //            .ListPtzPresets(camera.Id);
        //        if (resp != null && resp.Presets != null)
        //        {
        //            camera.PresetItemManager.Remove(previousValues);
        //            try
        //            {
        //                foreach (var r in resp.Presets)
        //                {
        //                    var ptzPresetItemModel = r.ConvertTo<PtzPresetItemModel>();
        //                    ptzPresetItemModel.CachedTime = DateTimeOffset.UtcNow;
        //                    camera.PresetItemManager.Add(
        //                        new KeyValuePair<Tuple<Guid, string>, PtzPresetItemModel>(
        //                            new Tuple<Guid, string>(camera.Id, r.Token), ptzPresetItemModel));
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //            }
        //        }
        //    }
        //}

        public async Task<List<PtzPreset>> RefreshPresets(Guid zoneId = default,
            ItemManagerRequestParameters parameters = default, params Guid[] cameras)
        {
            return await Task.Run(async () =>
            {
                if (cameras.Length > 0)
                {
                    var array = cameras.Select(g => g)
                        .OrderByDescending(o => o.ToString()).ToArray();
                    var str = string.Join(",", array);
                    return
                        parameters != null && parameters.AcceptedCacheDuration == TimeSpan.Zero
                            ? await PresetBatchCache.UncachedRequest((str, zoneId))
                            : await PresetBatchCache.Request((str,
                                zoneId)); //  PresetRequestFactory[(str, zoneId, parameters)].Value;
                }

                return default;
            });
        }

        public async Task<List<PtzPreset>> RefreshPresetsInternal(Guid zoneId = default,
            ItemManagerRequestParameters parameters = default, params Guid[] cameras)
        {
            return await Task.Run(async () =>
            {
                var outputPresets = new List<PtzPreset>();
                try
                {
                    if (zoneId == default)
                    {
                        zoneId = CommunicationManager.ConnectedZoneId;
                    }

                    if (DirectModeMgr.IsDirectMode)
                    {
                        foreach (var c in cameras)
                        {
                            if (c != null)
                            {
                                RefreshPresets(c, parameters);
                            }
                        }
                    }
                    else
                    {
                        var camerasToSearch = cameras.ToList();
                        var remoteQuery = parameters?.ForceRemoteQuery ?? false;
                        if (parameters != null && !remoteQuery && !parameters.RequestDirect &&
                            parameters.AcceptedCacheDuration > TimeSpan.Zero)
                        {
                            foreach (var c in cameras.ToList())
                            {
                                var matchingPtz = ItemCollection.FirstOrDefault(o => o.Key.Item1 == c).Value;

                                if (matchingPtz != null)
                                {
                                    if (matchingPtz.CachedTime >
                                        DateTimeOffset.UtcNow - parameters?.AcceptedCacheDuration)
                                    {
                                        camerasToSearch.Remove(c);
                                    }
                                }
                            }
                        }


                        var cameraItemModels = camerasToSearch.ToList();
                        var cameraPtzBatchRequest = cameraItemModels?.Select(o => new ListPtzPresets {CameraId = o})
                            .ToArray();
                        var managerByZoneId = CommunicationManager.GetManagerByZoneId(zoneId);
                        if (managerByZoneId != null && cameraItemModels.Count > 0)
                        {
                            var presets = await managerByZoneId
                                .CameraAdapterService.Client
                                .Request(cameraPtzBatchRequest);

                            if (presets != null)
                            {
                                int profileIndex = 0;
                                foreach (var p in presets)
                                {
                                    var matchingCamera = cameraItemModels.ElementAt(profileIndex);
                                    profileIndex++;
                                    if (matchingCamera != null)
                                    {

                                        if (p == null || p.Presets == null || p.Presets.Count == 0)
                                        {
                                            Trace.WriteLine($"NO PRESETS! FOR {matchingCamera}");
                                            var ptzPresetItemModel = new PtzPresetItemModel
                                                {IsBlank = true, CachedTime = DateTimeOffset.UtcNow};
                                            Update(
                                                new KeyValuePair<Tuple<Guid, string>, PtzPresetItemModel>(
                                                    new Tuple<Guid, string>(matchingCamera, "0"),
                                                    ptzPresetItemModel));
                                            continue;
                                        }

                                        if (p != null)
                                        {
                                            // matchingCamera.Presets.Clear();
                                            outputPresets.AddRange(p.Presets
                                                .Select(o => o)
                                                .OrderByDescending(o => o.Label));

                                            foreach (var o in outputPresets)
                                            {
                                                var ptzPresetItemModel = o.ConvertTo<PtzPresetItemModel>();
                                                ptzPresetItemModel.CachedTime = DateTimeOffset.UtcNow;
                                                Update(
                                                    new KeyValuePair<Tuple<Guid, string>, PtzPresetItemModel>(
                                                        new Tuple<Guid, string>(matchingCamera, o.Token),
                                                        ptzPresetItemModel));
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                }

                return outputPresets;
            });
        }

        //public async Task EditPreset(PtzPreset preset)
        //{
        //}

        //public async Task RemovePreset(Guid presetId)
        //{
        //}

        private void HubManagerOnSignalRClientEvent(ESignalRClientEvent clientEvent, Guid zoneId, string data)
        {
        }
    }
}