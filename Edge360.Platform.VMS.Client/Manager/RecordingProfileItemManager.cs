﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.CameraService.ServiceModel.Enums;
using Edge360.Vms.AuthorityService.SharedModel.Enums;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using RecordingService.ServiceModel.Api.RecordingProfile;
using RecordingService.ServiceModel.Enums;
using Unity;
using ListRecordingProfiles =
    Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.RecordingProfiles.ListRecordingProfiles;
using ListRecordingProfilesResponse =
    Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.RecordingProfiles.ListRecordingProfilesResponse;

namespace Edge360.Platform.VMS.Client.Manager
{
    public class
        RecordingProfileBatchRequestCache : BaseCacheFactory<List<GetRecordingProfilesByCameraIdResponse>, string, Guid>
    {
        public RecordingProfileBatchRequestCache() : base(TimeSpan.FromMinutes(5),
            TimeSpan.FromSeconds(3))
        {
        }

        public override async Task<List<GetRecordingProfilesByCameraIdResponse>> UncachedRequest((string, Guid) tuple)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(tuple.Item2);
                    var guids = tuple.Item1.Split(Delimiter).Select(o => Guid.Parse(o)).ToArray();
                    if (svc != null)
                    {
                        var cameraGuids = guids
                            .Select(o => new GetRecordingProfilesByCameraId {Id = o})
                            .ToList().Distinct().ToArray();
                        var profiles = await svc
                            .RecordingProfileService.Client
                            .Request(cameraGuids);
                        return profiles;
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }
    }


    public class GlobalRecordingProfileListCache : BaseCacheFactory<ListRecordingProfilesResponse, Guid, Guid>
    {
        public GlobalRecordingProfileListCache() : base(TimeSpan.FromSeconds(15), TimeSpan.FromSeconds(3))
        {
        }

     
        public override async Task<ListRecordingProfilesResponse> UncachedRequest((Guid, Guid) tuple)
        {
            try
            {
                return await Task.Run(async () =>
                    {
                        var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(Guid.Empty);//tuple.Item1
                        if (svc != null)
                        {
                            return await svc.GlobalRecordingService.ListRecordingProfiles(new ListRecordingProfiles() { CameraId = tuple.Item2});
                        }

                        return default;
                    }
                );
            }
            catch (Exception e)
            {
            }

            return default;
        }
    }

    public class RecordingProfileListCache : BaseCacheFactory<
        RecordingService.ServiceModel.Api.RecordingProfile.ListRecordingProfilesResponse, Guid>
    {
        public RecordingProfileListCache() : base(TimeSpan.FromSeconds(15), TimeSpan.FromSeconds(3))
        {
        }

        public override async Task<RecordingService.ServiceModel.Api.RecordingProfile.ListRecordingProfilesResponse>
            UncachedRequest(Guid zoneId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(zoneId);
                    if (svc != null)
                    {
                        return await svc.RecordingProfileService.GetRecordingProfiles();
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
            }

            return default;
        }
    }

    public class RefreshProfileByCameraIdCache : BaseCacheFactory<GetRecordingProfilesByCameraIdResponse, Guid, Guid>
    {
        public RefreshProfileByCameraIdCache() : base(TimeSpan.FromSeconds(15), TimeSpan.FromSeconds(3))
        {
        }

        public override async Task<GetRecordingProfilesByCameraIdResponse> UncachedRequest((Guid, Guid) tuple)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(tuple.Item1);

                    if (svc != null)
                    {
                        var profiles = await svc.RecordingProfileService
                            .GetRecordingProfileByCameraId(tuple.Item2);
                        return profiles;
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }
    }

    //public class RecordingProfileBatchCache : BaseCacheFactory<ListRecordingProfilesResponse, Guid, string>
    //{
    //    public override async Task<ListRecordingProfilesResponse> InternalRequest(Guid zoneId, string guids)
    //    {
    //        try
    //        {
    //            var svc = await ServerItemManager.GetManagerByZoneIdConnectIfFail(zoneId);
    //            if (svc != null)
    //            {
    //                return await svc.RecordingProfileService.GetRecordingProfiles();
    //            }
    //        }
    //        catch (Exception e)
    //        {

    //        }
    //        return default;
    //    }
    //}


    /// <summary>
    ///     RecordingProfileId
    /// </summary>
    public class RecordingProfileItemManager : BaseItemManager<RecordingProfileItemModel, Guid>
    {
        public static GlobalRecordingProfileListCache GlobalRecordingProfileListCache { get; set; } =
            new GlobalRecordingProfileListCache();

        public static RecordingProfileItemManager Manager =>
            App.GlobalUnityContainer.Resolve<IBaseManager>(nameof(RecordingProfileItemManager)) as
                RecordingProfileItemManager;

        public static RecordingProfileBatchRequestCache RecordingProfileBatchRequestCache { get; set; } =
            new RecordingProfileBatchRequestCache();

        public static RecordingProfileListCache RecordingProfileListCache { get; set; } =
            new RecordingProfileListCache();

        public static RefreshProfileByCameraIdCache RefreshProfileByCameraIdCache { get; set; } =
            new RefreshProfileByCameraIdCache();

        public override List<ICacheFactory> CacheFactories { get; set; } = new List<ICacheFactory>
        {
            RefreshProfileByCameraIdCache,
            RecordingProfileListCache,
            RecordingProfileBatchRequestCache,
            GlobalRecordingProfileListCache,
        };

        public RecordingProfileItemManager(ILog log = null)
        {
            _log = log;
            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;
        }

        public static bool FilterRemoteStream(Guid zoneId, KeyValuePair<Guid, RecordingProfileItemModel> model)
        {
            if (zoneId != CommunicationManager.ConnectedZoneId)
            {
                if ((CommunicationManager.EffectivePermissions & ERoles.RemoteStreamingOverride) == 0)
                {
                    var valuePropertyFlags = (ERecordingProfileProperty) model.Value.PropertyFlags;
                    if (!valuePropertyFlags.HasFlag(ERecordingProfileProperty.IsRemoteProfile))
                    {
                        return false;
                    }
                }
            }

            return true;
        }  

        public RecordingProfileItemModel GetNearestRecordingProfileForResolution(CameraItemModel camera, double ownerWidth)
        {
            try
            {
                if (DirectModeMgr.IsOfflineMode)
                {
                    return default;
                }

                var recording = ItemCollection.Where(o =>
                        o.Value.CameraId == camera.Id && FilterRemoteStream(camera.ZoneId, o) &&
                        !o.Value.PropertyFlags.HasFlag(ERecordingProfileProperty.IsAutoSelectDisabled))
                    .OrderByDescending(o => o.Value.IsEnabled)
                    .ThenBy(o => Math.Abs(ownerWidth - o.Value.Width)).FirstOrDefault().Value;
                return recording ?? ItemCollection?.FirstOrDefault(o => o.Value.CameraId == camera.Id).Value;
            }
            catch (Exception e)
            {
                _log?.Info(e);
                return default;
            }
        }

        public async Task<List<GetRecordingProfilesByCameraIdResponse>> RequestRecordingProfileBatch(
            params CameraItemModel[] array)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (DirectModeMgr.IsOfflineMode)
                    {
                        return default;
                    }

                    var sortByZone = array.Distinct((a, b) => a?.ZoneId == b?.ZoneId).ToList();
                    var resp = new List<GetRecordingProfilesByCameraIdResponse>();
                    foreach (var n in sortByZone.Where(o => o != null))
                    {
                        try
                        {
                            var guidList = array.Where(o => o?.ZoneId == n?.ZoneId).Select(o => o?.ObjectId.ToString())
                                .ToArray();
                            var strings = string.Join(RecordingProfileBatchRequestCache.Delimiter.ToString(), guidList);
                            var recordingProfiles =
                                await RecordingProfileBatchRequestCache.Request((strings, n.ZoneId));
                            if (recordingProfiles != null)
                            {
                                try
                                {
                                    var recordingProfileItemModels = recordingProfiles
                                        ?.Where(o => o.RecordingProfiles.Length > 0)
                                        ?.Select(o => o.RecordingProfiles)?.SelectMany(r => r)
                                        .Where(o => o.Id != Guid.Empty)?.Distinct((a, b) => a?.Id == b?.Id)
                                        ?.ToDictionary(o => o.Id, b => (RecordingProfileItemModel) b);
                                    if (recordingProfileItemModels != null)
                                    {
                                        Update(recordingProfileItemModels.ToArray());
                                    }
                                }
                                catch (Exception e)
                                {
                                }

                                resp.AddRange(recordingProfiles);
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    return resp;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public async Task<ListRecordingProfilesResponse> RefreshGlobalProfiles(Guid cameraId, Guid zoneId = default)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (zoneId == Guid.Empty)
                    {
                        zoneId = CommunicationManager.ConnectedZoneId;
                    }

                    var profiles = await GlobalRecordingProfileListCache.Request((zoneId, cameraId));
                    if (profiles != null)
                    {
                        var recordingProfileItemModels =
                            profiles.RecordingProfiles.ToDictionary(o => o.RecordingProfileId,
                                b => (RecordingProfileItemModel) b);
                    
                        Update(recordingProfileItemModels.Where(o => o.Key != Guid.Empty && !ItemCollection.Any(remote => remote.Key == o.Key && !string.IsNullOrEmpty(o.Value.RtspAddress))).ToArray());
                    }

                    return profiles;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }


        public async Task<RecordingService.ServiceModel.Api.RecordingProfile.ListRecordingProfilesResponse>
            RefreshProfiles(Guid zoneId = default)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (zoneId == Guid.Empty)
                    {
                        zoneId = CommunicationManager.ConnectedZoneId;
                    }

                    var profiles = await RecordingProfileListCache.Request(zoneId);
                    var recordingProfileItemModels =
                        profiles.RecordingProfiles.ToDictionary(o => o.Id, b => (RecordingProfileItemModel) b);
                    Update(recordingProfileItemModels.Where(o => o.Key != Guid.Empty).ToArray());
                    return profiles;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public async Task<GetRecordingProfileResponse> RefreshProfileByRecordingProfileId(Guid zoneId, Guid recordingId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (zoneId == Guid.Empty)
                    {
                        zoneId = CommunicationManager.ConnectedZoneId;
                    }

                    var profiles = await CommunicationManager.GetManagerByZoneId(zoneId).RecordingProfileService
                        .GetRecordingProfile(recordingId);
                    var recordingProfileItemModels =
                        profiles.RecordingProfile;
                    if (recordingProfileItemModels.Id != Guid.Empty)
                    {
                        Update(new KeyValuePair<Guid, RecordingProfileItemModel>(recordingProfileItemModels.Id,
                            recordingProfileItemModels));
                    }

                    return profiles;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public async Task EditRecordingProfile(RecordingProfileItemModel item)
        {
        }



        public async Task<GetRecordingProfilesByCameraIdResponse> RefreshProfileByCameraId(Guid zoneId, Guid cameraId, ItemManagerRequestParameters parameters = null)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (zoneId == Guid.Empty)
                    {
                        zoneId = GetZoneIdFromCameraId(cameraId) ?? CommunicationManager.ConnectedZoneId;
                    }

                    var profiles = parameters != null && parameters.AcceptedCacheDuration == TimeSpan.Zero ? await RefreshProfileByCameraIdCache.UncachedRequest((zoneId, cameraId)) : await RefreshProfileByCameraIdCache.Request((zoneId, cameraId));
                    if (profiles != null)
                    {
                        var recordingProfileItemModels =
                            profiles.RecordingProfiles.ToDictionary(o => o.Id, b => (RecordingProfileItemModel) b);
                        Update(recordingProfileItemModels.Where(o => o.Key != Guid.Empty).ToArray());
                    }

                    return profiles;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public static Guid? GetZoneIdFromCameraId(Guid cameraId)
        {
            return CameraItemModelManager.Manager?.ItemCollection?.FirstOrDefault(o => o.Key == cameraId).Value?.ZoneId;
        }

        public async Task<RecordingService.ServiceModel.Api.RecordingProfile.ListRecordingProfilesResponse>
            ListRecordingProfiles(Guid zoneId = default,
                ItemManagerRequestParameters parameters = null)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    return HasCacheTimeZero(parameters)
                        ? await RecordingProfileListCache.UncachedRequest(zoneId)
                        : await RecordingProfileListCache.Request(zoneId);
                });
            }
            catch (Exception e)
            {
            }

            return default;
            //await RefreshProfileByCameraId(default, recordingProfileItemModel.CameraId);
        }

        public async Task GetRecordingProfiles(RecordingProfileItemModel recordingProfileItemModel, bool useCache = false)
        {
            await Task.Run(async () =>
            {
                await RefreshProfileByCameraId(default, recordingProfileItemModel.CameraId, useCache ? new ItemManagerRequestParameters(){AcceptedCacheDuration =  TimeSpan.Zero} : null);
            });
        }

        public async Task RemoveRecordingProfile(Guid profileId)
        {
            ItemCollection.Remove(profileId);
        }

        private async void HubManagerOnSignalRClientEvent(ESignalRClientEvent clientEvent, Guid zoneId, string data)
        {
            try
            {
                await Task.Run(async () =>
                {
                    var id = Guid.Parse(data);
                    if (clientEvent.Equals(ESignalRClientEvent.RecordingProfileDeleted))
                    {
                        await RemoveRecordingProfile(id);
                    }
                    else if (clientEvent.Equals(ESignalRClientEvent.RecordingProfileCreated))
                    {
                        await RefreshProfileByRecordingProfileId(zoneId, id);
                    }
                    else if (clientEvent.Equals(ESignalRClientEvent.RecordingProfileEdited))
                    {
                        await RefreshProfileByCameraId(zoneId, id);
                    }
                });
            }
            catch (Exception e)
            {
            }
        }
    }
}