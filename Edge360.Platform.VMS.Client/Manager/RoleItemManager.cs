﻿using System;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using log4net;

namespace Edge360.Platform.VMS.Client.Manager
{
    /// <summary>
    ///     RoleId
    /// </summary>
    public class RoleItemManager : BaseItemManager<RoleItemModel, Guid, Guid>
    {
        public RoleItemManager(ILog log = null)
        {
            _log = log;
        }

        public async Task RefreshRole(Guid groupId, Guid roleId)
        {
        }

        public async Task RefreshRoles(Guid groupId)
        {
        }
    }
}