﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.UI.Common.Interfaces.Ipc;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Cameras;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Nodes;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.CameraAdapter;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Cameras;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.CameraStatus;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using ServiceStack;
using Unity;
using WispFramework.Extensions.Common;
using static Edge360.Platform.VMS.Communication.CommunicationManager;
using GetCameraResponse = Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Cameras.GetCameraResponse;
using ListCamerasResponse = Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Cameras.ListCamerasResponse;

namespace Edge360.Platform.VMS.Client.Manager
{
    public class GlobalCameraBatchRequestCache : BaseCacheFactory<List<GetCameraResponse>, string, Guid>
    {
        public GlobalCameraBatchRequestCache() : base(TimeSpan.FromMinutes(5),
            TimeSpan.FromSeconds(15))
        {
        }

        public override async Task<List<GetCameraResponse>> UncachedRequest((string, Guid) tuple)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(tuple.Item2);
                    var guids = tuple.Item1.Split(Delimiter).Select(o => Guid.Parse(o)).ToArray();
                    if (svc != null && guids != null)
                    {
                        if (svc.GlobalCameraService != null)
                        {
                            var cameras = await svc.GlobalCameraService.GetCameraBatch(guids);
                            return cameras;
                        }
                    }

                    return default;
                });     
            }
            catch (Exception e)
            {
                return default;
            }
        }
    }

    public class CameraBatchRequestCache : BaseCacheFactory<
        List<Raven.Vms.CameraService.ServiceModel.Api.Cameras.GetCameraResponse>, string, Guid>
    {
        public CameraBatchRequestCache() : base(TimeSpan.FromSeconds(15),
            TimeSpan.FromSeconds(15))
        {
        }

        public override async Task<List<Raven.Vms.CameraService.ServiceModel.Api.Cameras.GetCameraResponse>>
            UncachedRequest((string, Guid) tuple)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(tuple.Item2);
                    var guids = tuple.Item1.Split(Delimiter).Select(o => Guid.Parse(o)).ToArray();
                    if (svc != null && guids != null)
                    {
                        var cameras = await svc.CameraService.GetCameraBatch(guids);
                        return cameras;
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }
    }

    public class CameraDeviceInformationCache : BaseCacheFactory<GetCameraDetailsResponse, Guid, Guid>
    {
        public CameraDeviceInformationCache() : base(TimeSpan.FromMinutes(5),
            TimeSpan.FromSeconds(5))
        {
        }

        public override async Task<GetCameraDetailsResponse> UncachedRequest((Guid, Guid) tuple)
        {
            // Trace.WriteLine($"{DateTimeOffset.UtcNow:u} Uncached Request! {Environment.StackTrace}");
            try
            {
                return await Task.Run(async () =>
                {
                    var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(tuple.Item1);
                    if (svc != null)
                    {
                        return await GetManagerByZoneId(tuple.Item1).CameraAdapterService
                            .GetCameraDetails(new GetCameraDetails {CameraId = tuple.Item2});
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }
    }

    public class CameraStatusCache : BaseCacheFactory<List<GetCameraStatusResponse>, string, Guid>
    {
        public CameraStatusCache() : base(TimeSpan.FromSeconds(5),
            TimeSpan.FromSeconds(5))
        {
        }

        public override async Task<List<GetCameraStatusResponse>> UncachedRequest((string, Guid) tuple)
        {
            // Trace.WriteLine($"{DateTimeOffset.UtcNow:u} Uncached Request! {Environment.StackTrace}");
            try
            {
                return await Task.Run(async () =>
                {
                    var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(tuple.Item2);
                    var guids = tuple.Item1.Split(Delimiter).Select(o => Guid.Parse(o)).ToArray();
                    if (svc != null && guids != null)
                    {
                        var cameras =
                            await svc.CameraService.GetCameraStatusBatch(guids);
                        return cameras;
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }
    }

    public class GlobalCameraListCache : BaseCacheFactory<ListCamerasResponse, string>
    {
        public GlobalCameraListCache() : base(TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(3))
        {
        }

        public override async Task<ListCamerasResponse> UncachedRequest(string zoneId)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var svc = GetManagerByZoneId();
                    var resp = await svc.GlobalCameraService.ListCameras();
                    return resp;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }
    }

    public class
        CameraListRefreshCache : BaseCacheFactory<Raven.Vms.CameraService.ServiceModel.Api.Cameras.ListCamerasResponse,
            Guid>
    {
        public CameraListRefreshCache() : base(TimeSpan.FromSeconds(15),
            TimeSpan.FromSeconds(5))
        {
        }

        public override async Task<Raven.Vms.CameraService.ServiceModel.Api.Cameras.ListCamerasResponse>
            UncachedRequest(Guid zoneId)
        {
            // Trace.WriteLine($"{DateTimeOffset.UtcNow:u} Uncached Request! {Environment.StackTrace}");
            try
            {
                return await Task.Run(async () =>
                {
                    var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(zoneId);
                    if (svc != null)
                    {
                        if (svc.CameraService != null)
                        {
                            var cameras = await svc.CameraService.ListCameras();
                            return cameras;
                        }
                    }
                
                    return default;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }
    }

    /// <summary>
    ///     CameraId
    /// </summary>
    public class CameraItemModelManager : BaseItemManager<CameraItemModel, Guid>
    {
        private ServerItemManager _serverItemMgr;
        private ZoneItemManager _zoneItemMgr;

        public FileStorage<ObservableConcurrentDictionary<Guid, CameraItemModel>> CameraCache =
            new FileStorage<ObservableConcurrentDictionary<Guid, CameraItemModel>> {FileName = "CameraCache"};

        public static CameraBatchRequestCache CameraBatchRequestCache { get; set; } = new CameraBatchRequestCache();

        public static CameraDeviceInformationCache CameraDeviceInformationCache { get; set; } =
            new CameraDeviceInformationCache();

        public static CameraListRefreshCache CameraListRefreshCache { get; set; } = new CameraListRefreshCache();
        public static CameraStatusCache CameraStatusCache { get; set; } = new CameraStatusCache();

        public static GlobalCameraBatchRequestCache GlobalCameraBatchRequestCache { get; set; } =
            new GlobalCameraBatchRequestCache();

        public static GlobalCameraListCache GlobalCameraListCache { get; set; } = new GlobalCameraListCache();

        public static CameraItemModelManager Manager => App.GlobalUnityContainer.Resolve<CameraItemModelManager>();

        public override List<ICacheFactory> CacheFactories { get; set; } = new List<ICacheFactory>
        {
            CameraDeviceInformationCache,
            CameraBatchRequestCache,
            CameraStatusCache,
            CameraListRefreshCache,
            GlobalCameraListCache,
            GlobalCameraBatchRequestCache,
        };

        public PermissionItemManager PermissionItemManager { get; set; }

        static CameraItemModelManager()
        {
            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;
        }

        public CameraItemModelManager(ILog log = null, PermissionItemManager permissionItemManager = null,
            DirectModeManager directModeManager = null, ZoneItemManager zoneItemManager = null,
            ServerItemManager serverItemManager = null)
        {
            _log = log;
            _serverItemMgr = serverItemManager;
            _zoneItemMgr = zoneItemManager;
            DirectModeMgr = directModeManager;
            PermissionItemManager = permissionItemManager;
        }

        public async Task<ListCamerasResponse> RefreshGlobalCameras(ItemManagerRequestParameters parameters = default)
        {
            return await Task.Run(async () =>
            {
                return parameters?.AcceptedCacheDuration == TimeSpan.Zero
                    ? await GlobalCameraListCache.UncachedRequest("global")
                    : await GlobalCameraListCache.Request("global");
            });
        }

        public async Task<GetCameraDetailsResponse> GetCameraDetails(Guid zoneId, Guid cameraId,
            ItemManagerRequestParameters parameters = default)
        {
            return await Task.Run(async () =>
            {
                return HasCacheTimeZero(parameters)
                    ? await CameraDeviceInformationCache.UncachedRequest((zoneId, cameraId))
                    : await CameraDeviceInformationCache.Request((zoneId, cameraId));
            });
        }

        public async Task<bool> SaveCache(UserLoginItemModel userLogin)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    CameraCache.Storage = ItemCollection;
                    CameraCache.Login = userLogin;
                    if (ItemCollection.Any())
                    {
                        return CameraCache.Save();
                    }

                    return false;
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return false;
        }

        public async Task<bool> LoadCacheFile(UserLoginItemModel userLogin)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    CameraCache.Login = userLogin;
                    CameraCache.Load();
                    DirectModeMgr.IsCameraCacheLoaded = true;
                    return true;
                });
            }
            catch (Exception e)
            {
                DirectModeMgr.IsCameraCacheLoaded = false;
            }

            return false;
        }

        public async Task RemoveCamera(Guid cameraId)
        {
        }

        public async Task<List<CameraItemModel>> RefreshGlobalCameras(Guid zoneId = default, Guid serverId = default,
            ItemManagerRequestParameters parameters = default)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    _log?.Info($"REFRESH CAMERAS {parameters?.AcceptedCacheDuration}");
                    if (zoneId == Guid.Empty)
                    {
                        zoneId = ConnectedZoneId;
                    }

                    if (DirectModeMgr.IsOfflineMode)
                    {
                        Update(Enumerable.ToArray(CameraCache.Storage));
                        return CameraCache.Storage.Values.ToList();
                    }

                    var profiles = parameters != null && parameters.AcceptedCacheDuration == TimeSpan.Zero
                        ? await GlobalCameraListCache.UncachedRequest("global")
                        : await GlobalCameraListCache.Request("global");
                    //var profiles = await GetManagerByZoneId(zoneId).CameraService
                    //    .ListCameras();

                    var itemManagerRequestParameters = parameters;

                    if (itemManagerRequestParameters != null)
                    {
                        if (itemManagerRequestParameters.ForceRemoteQuery != null &&
                            (bool) itemManagerRequestParameters.ForceRemoteQuery && parameters.GroupId != null)
                        {
                            await PermissionItemManager.RequestPermissions((Guid) parameters.GroupId, Guid.Empty);
                            //await PermissionItemManager.RequestPermissionsBatch((Guid) parameters.GroupId,
                            //    profiles.Cameras.Select(o => o.Id).ToArray());
                        }

                        itemManagerRequestParameters.ForceRemoteQuery = false;
                    }

                    var convertedProfiles = new List<CameraItemModel>();
                    if (profiles != null && profiles.Cameras != null)
                    {
                        foreach (var p in profiles.Cameras)
                        {
                            try
                            {
                                var cameraItemModel = await AddProperties(p as Camera, itemManagerRequestParameters);
                                convertedProfiles.Add(cameraItemModel);
                            }
                            catch (Exception e)
                            {
                            }
                        }
                    }

                    var recordingProfileItemModels =
                        convertedProfiles.ToDictionary(o => o.Id, n => n);
                    Update(Enumerable.ToArray(recordingProfileItemModels));
                    //BuildCache();
                    return convertedProfiles.OrderBy(o => o.Label).ToList();
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }


        public async Task<List<CameraItemModel>> RefreshCameras(Guid zoneId = default, Guid serverId = default,
            ItemManagerRequestParameters parameters = default)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    _log?.Info($"REFRESH CAMERAS {parameters?.AcceptedCacheDuration}");
                    if (zoneId == Guid.Empty)
                    {
                        zoneId = ConnectedZoneId;
                    }

                    if (DirectModeMgr.IsOfflineMode)
                    {
                        Update(Enumerable.ToArray(CameraCache.Storage));
                        return CameraCache.Storage.Values.ToList();
                    }

                    var profiles = parameters != null && parameters.AcceptedCacheDuration == TimeSpan.Zero
                        ? await CameraListRefreshCache.UncachedRequest(zoneId)
                        : await CameraListRefreshCache.Request(zoneId);

                    var itemManagerRequestParameters = parameters;

                    if (itemManagerRequestParameters != null)
                    {
                        if (itemManagerRequestParameters.ForceRemoteQuery != null &&
                            (bool) itemManagerRequestParameters.ForceRemoteQuery && parameters.GroupId != null)
                        {
                            await PermissionItemManager.RequestPermissions((Guid) parameters.GroupId, Guid.Empty);
                            //await PermissionItemManager.RequestPermissionsBatch((Guid) parameters.GroupId,
                            //    profiles.Cameras.Select(o => o.Id).ToArray());
                        }

                        itemManagerRequestParameters.ForceRemoteQuery = false;
                    }

                    var convertedProfiles = new List<CameraItemModel>();
                    if (profiles != null && profiles.Cameras != null)
                    {
                        foreach (var p in profiles.Cameras)
                        {
                            var cameraItemModel = await AddProperties(p, itemManagerRequestParameters);
                            convertedProfiles.Add(cameraItemModel);
                        }
                    }

                    var cameraItemModels =
                        convertedProfiles.ToDictionary(o => o.Id, n => n);
                    Update(Enumerable.ToArray(cameraItemModels));
                    return convertedProfiles.OrderBy(o => o.Label).ToList();
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        private async Task<CameraItemModel> AddProperties(Camera b, ItemManagerRequestParameters parameters)
        {
            return await Task.Run(async () =>
            {
                var cameraItemModel = (CameraItemModel) b;
                if (parameters?.GroupId != null)
                {
                    var permissionItemModel =
                        await PermissionItemManager.FindPermission(b.CameraId, (Guid) parameters?.GroupId, parameters);
                    cameraItemModel.Permissions =
                        permissionItemModel ?? new PermissionItemModel();
                }

                return cameraItemModel;
            });
        }

        private async Task<CameraItemModel> AddProperties(CameraDescriptor b, ItemManagerRequestParameters parameters)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var cameraItemModel = (CameraItemModel) b;
                    if (parameters?.GroupId != null)
                    {
                        var permissionItemModel =
                            await PermissionItemManager.FindPermission(b.Id, (Guid) parameters?.GroupId, parameters);
                        cameraItemModel.Permissions =
                            permissionItemModel ?? new PermissionItemModel();
                    }

                    return cameraItemModel;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public async Task<List<CameraItemModel>> RequestOfflineCameras(params NodeItemModel[] array)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var observableConcurrentDictionary = CameraCache.Storage;
                    return observableConcurrentDictionary.Values.Where(o => array.Any(c => c.ObjectId == o.Id))
                        .ToList();
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }


        public async Task<List<GetCameraStatusResponse>> RequestCameraStatuses(params CameraItemModel[] array)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (DirectModeMgr.IsOfflineMode)
                    {
                        return default;
                    }

                    var sortByZone = array.Distinct((a, b) => a?.ZoneId == b?.ZoneId).ToList();
                    var resp = new List<GetCameraStatusResponse>();
                    foreach (var n in sortByZone)
                    {
                        try
                        {
                            var guidList = array.Where(o => o != null && o?.ZoneId == n?.ZoneId)
                                .Select(o => o?.ObjectId?.ToString())
                                .ToArray();
                            var strings = string.Join(CameraStatusCache.Delimiter.ToString(), guidList);
                            if (!string.IsNullOrEmpty(strings))
                            {
                                var cameras = await CameraStatusCache.Request((strings, n.ZoneId));
                                if (cameras != null)
                                {
                                    resp.AddRange(cameras);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    return resp;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public async Task<List<GetCameraResponse>> RequestGlobeCameras(params NodeItemModel[] array)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (DirectModeMgr.IsOfflineMode)
                    {
                        return CameraCache.Storage.Values.Where(o => array.Any(c => c.ObjectId == o.Id))
                            .Select(o =>
                            {
                                var camera = o.ConvertTo<Camera>();
                                return new GetCameraResponse
                                    {Camera = camera};
                            }).ToList();
                    }

                    var sortByZone = array.Distinct((a, b) => a?.ZoneId == b?.ZoneId).ToList();
                    var resp = new List<GetCameraResponse>();
                    foreach (var n in sortByZone)
                    {
                        try
                        {
                            var guidList = array.Where(o => o != null && o.ZoneId == n?.ZoneId)
                                .Select(o => o?.ObjectId?.ToString())
                                .ToArray();
                            var strings = string.Join(GlobalCameraBatchRequestCache.Delimiter.ToString(), guidList);
                            if (strings.IsNotEmpty())
                            {
                                var cameras = await GlobalCameraBatchRequestCache.Request((strings, (Guid) n.ZoneId));
                                if (cameras != null)
                                {
                                    resp.AddRange(cameras);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }


                    return resp;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public async Task<List<Raven.Vms.CameraService.ServiceModel.Api.Cameras.GetCameraResponse>> RequestCameras(
            params NodeItemModel[] array)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (DirectModeMgr.IsOfflineMode)
                    {
                        return CameraCache.Storage.Values.Where(o => array.Any(c => c.ObjectId == o.Id))
                            .Select(o => new Raven.Vms.CameraService.ServiceModel.Api.Cameras.GetCameraResponse
                                {Camera = o.Descriptor}).ToList();
                    }

                    var sortByZone = array.Distinct((a, b) => a?.ZoneId == b?.ZoneId).ToList();
                    var resp = new List<Raven.Vms.CameraService.ServiceModel.Api.Cameras.GetCameraResponse>();
                    foreach (var n in sortByZone)
                    {
                        try
                        {
                            var guidList = array.Where(o => o != null && o.ZoneId == n?.ZoneId)
                                .Select(o => o?.ObjectId?.ToString())
                                .ToArray();
                            var strings = string.Join(CameraBatchRequestCache.Delimiter.ToString(), guidList);
                            if (strings.IsNotEmpty())
                            {
                                var cameras = await CameraBatchRequestCache.Request((strings, (Guid) n.ZoneId));
                                if (cameras != null)
                                {
                                    resp.AddRange(cameras);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }


                    return resp;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public static event Action CameraRefreshRequired;

        private static void HubManagerOnSignalRClientEvent(ESignalRClientEvent clientEvent, Guid zoneId, string data)
        {
            try
            {
                Task.Run(async () =>
                {
                    if (Manager != null)
                    {
                        if (Guid.TryParse(data, out var guid))
                        {
                            var _ = Task.Run(async () =>
                            {
                                if (clientEvent.Equals(ESignalRClientEvent.CameraDeleted))
                                {
                                    Manager?.ItemCollection.Remove(guid);
                                    CameraRefreshRequired?.Invoke();
                                }
                                else if (clientEvent.Equals(ESignalRClientEvent.CamerasUpdated) ||
                                         clientEvent.Equals(ESignalRClientEvent.CamerasUpdated))
                                {
                                    await Manager?.RequestCameras(
                                        new NodeItemModel(new NodeDescriptor {ZoneId = zoneId, Id = guid})
                                            {ZoneId = zoneId, ObjectId = guid});
                                    CameraRefreshRequired?.Invoke();
                                }
                            }).ConfigureAwait(false);
                        }
                    }
                });
            }
            catch (Exception e)
            {
            }
        }
    }
}