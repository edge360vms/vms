﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.Volumes;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using Unity;

namespace Edge360.Platform.VMS.Client.Manager
{
    public class ListVolumeInfoCache : BaseCacheFactory<GetVolumesResponse, Guid, Guid>
    {
        public ListVolumeInfoCache() : base(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(3))
        {
        }

        public override async Task<GetVolumesResponse> UncachedRequest((Guid, Guid) tuple)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var svc = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(tuple.Item1);
                    if (svc != null)
                    {
                        if (svc.VolumeService != null)
                        {
                            var volumes = await svc.VolumeService.GetVolumes();
                            return volumes;
                        }
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
            }

            return default;
        }
    }

    public class GetVolumeInfoCache : BaseCacheFactory<GetVolumeInfoResponse, Guid, Guid>
    {
        public GetVolumeInfoCache() : base(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(3))
        {
        }

        public override async Task<GetVolumeInfoResponse> UncachedRequest((Guid, Guid ) tuple)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var svc = CommunicationManager.GetManagerByServerId(tuple.Item2);
                    if (svc != null)
                    {
                        var getVolumeInfoResponse =
                            await svc.VolumeService.GetVolumeInfo(new GetVolumeInfo {VolumeId = tuple.Item1},
                                tuple.Item2);
                        return getVolumeInfoResponse;
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
            }

            return default;
        }
    }

    /// <summary>
    ///     VolumeId, ServerId
    /// </summary>
    public class VolumeItemManager : BaseItemManager<VolumeItemModel, Guid, Guid>
    {
        public static GetVolumeInfoCache GetVolumeInfoCache { get; set; } = new GetVolumeInfoCache();
        public static ListVolumeInfoCache ListVolumeInfoCache { get; set; } = new ListVolumeInfoCache();

        public static VolumeItemManager Manager =>
            App.GlobalUnityContainer.Resolve<IBaseManager>(nameof(VolumeItemManager)) as VolumeItemManager;

        public override List<ICacheFactory> CacheFactories { get; set; } = new List<ICacheFactory>
        {
            GetVolumeInfoCache,
            ListVolumeInfoCache,
        };

        public VolumeItemManager(ILog log = null)
        {
            _log = log;
            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;
        }

        //public async Task<GetVolumeResponse> ListVolumes(Guid zoneId, Guid serverId)
        //{
        //    return await ListVolumeInfoCache.Request((zoneId, serverId));
        //}

        public async Task<GetVolumesResponse> ListVolumes(Guid zoneId, Guid serverId, bool useCache = true)
        {
            return await Task.Run(async () =>
            {
                return useCache
                    ? await ListVolumeInfoCache.Request((zoneId, serverId))
                    : await ListVolumeInfoCache.UncachedRequest((zoneId, serverId));
            });
        }


        public async Task<GetVolumeInfoResponse> GetVolumeInfo(Guid volumeId, Guid serverId,
            ItemManagerRequestParameters itemManagerRequestParameters)
        {
            return await Task.Run(async () =>
            {
                return HasCacheTimeZero(itemManagerRequestParameters)
                    ? await GetVolumeInfoCache.UncachedRequest((volumeId, serverId))
                    : await GetVolumeInfoCache.Request((volumeId, serverId));
            });
        }

        private async void HubManagerOnSignalRClientEvent(ESignalRClientEvent clientEvent, Guid zoneId, string data)
        {
            if (Guid.TryParse(data, out var guid))
            {
                await Task.Run(async () =>
                    {
                        if (clientEvent.Equals(ESignalRClientEvent.VolumeDeleted))
                        {
                            ItemCollection.Remove(new Tuple<Guid, Guid>(zoneId, guid));
                        }
                        else if (clientEvent.Equals(ESignalRClientEvent.VolumeCreated) ||
                                 clientEvent.Equals(ESignalRClientEvent.VolumeEdited))
                        {
                            await ListVolumes(zoneId, default, false);
                            //  await RequestCameras(new NodeItemModel(new NodeDescriptor(){ZoneId = zoneId, Id = guid}) {ZoneId = zoneId, ObjectId = guid});
                        }
                    }
                );
            }
        }

        //public async Task EditVolume(VolumeItemModel volume)
        //{

        //}

        //public async Task RemoveVolume(Guid volumeId)
        //{

        //}

        //public async Task RefreshVolume(Guid volumeId, Guid serverId = default)
        //{
        //}

        //public async Task RefreshVolumes(Guid serverId)
        //{
        //}
    }
}