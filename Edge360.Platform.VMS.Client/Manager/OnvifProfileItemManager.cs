﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Platform.VMS.Monitoring.CameraComponents.Axis.PTZ;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.CameraAdapter;
using log4net;
using Mictlanix.DotNet.Onvif.Common;
using Newtonsoft.Json;
using ServiceStack;
using Unity;
using WispFramework.Extensions.Common;

namespace Edge360.Platform.VMS.Client.Manager
{
    public class OnvifCacheProfileCache : BaseCacheFactory<GetCameraInfoResponse, Guid, Guid>
    {
        public OnvifCacheProfileCache() : base(TimeSpan.FromMinutes(3) , TimeSpan.FromMinutes(3))
        {
            
        }

        public override async Task<GetCameraInfoResponse> UncachedRequest((Guid, Guid) tuple)
        {
            return await Task.Run(async () =>
            {
                var managerByZoneId = CommunicationManager.GetManagerByZoneId(tuple.Item1);
                Trace.WriteLine($"Making Uncached Request for {tuple.Item1} {tuple.Item2}");
                try
                {
                    return await managerByZoneId.CameraAdapterService
                        .Client
                        .Request(new GetCameraInfo {CameraId = tuple.Item2}); //.GetCameraInfo(tuple.Item2);
                }
                catch (Exception e)
                {
                    Trace.WriteLine($"GetCameraInfoError {e.GetType()} {e.Message}");
                    return new GetCameraInfoResponse();
                }
            });
        }
    }

    /// <summary>
    ///     CameraId, PresetToken
    /// </summary>
    public class OnvifProfileItemManager : BaseItemManager<BaseAdapterCameraProfileItemModel, Guid, string>
    {
        private readonly DirectModeManager _directModeMgr;

        public static OnvifProfileItemManager Manager =>
            App.GlobalUnityContainer.Resolve<IBaseManager>(nameof(OnvifProfileItemManager)) as OnvifProfileItemManager;

        public static OnvifCacheProfileCache OnvifCacheProfileCache { get; set; } = new OnvifCacheProfileCache();

        public override List<ICacheFactory> CacheFactories { get; set; } = new List<ICacheFactory>
        {
            OnvifCacheProfileCache,
        };


        public OnvifProfileItemManager(DirectModeManager directModeMgrManager, ILog log = null)
        {
            _log = log;
            _directModeMgr = directModeMgrManager;
        }

        public BaseAdapterCameraProfileItemModel GetNearestOnvifProfileForResolution(Guid videoObjectId,
            double actualWidth)
        {
            try
            {
                var profileForOnvif = ItemCollection.Where(o => { return o.Key.Item1 == videoObjectId; })
                    .OrderByDescending(o => o.Value.VideoEncoderConfiguration.H264 != null)
                    .ThenBy(o => { return Math.Abs(actualWidth - o.Value.Resolution.Width); }).FirstOrDefault().Value;
                return profileForOnvif;
            }
            catch (Exception e)
            {
                _log?.Info(e);
                return default;
            }
        }

        public async Task<GetCameraInfoResponse> GetProfile(CameraItemModel camera)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var resp = await OnvifCacheProfileCache.Request((camera.ZoneId, camera.Id));
                    return resp;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public async Task RefreshProfiles(ItemManagerRequestParameters parameters, params CameraItemModel[] cameras)
        {
            await Task.WhenAll(cameras.Select(async c =>
              {
                  if (!_directModeMgr.IsDirectMode && !parameters.RequestDirect)
                  {
                      try
                      {
                          if (c != null)
                          {
                              if (parameters.AcceptedCacheDuration > TimeSpan.Zero)
                              {
                                  if (ItemCollection.Any(o => o.Key.Item1 == c.Id &&
                                                              o.Value.LastQueried > DateTimeOffset.UtcNow -
                                                              parameters.AcceptedCacheDuration))
                                  {
                                      return;
                                  }
                              }

                              var managerByZoneId = CommunicationManager.GetManagerByZoneId(c.ZoneId);
                              if (managerByZoneId != null)
                              {
                                  var resp =  parameters != null && parameters.AcceptedCacheDuration == TimeSpan.Zero ? await OnvifCacheProfileCache.UncachedRequest((c.ZoneId, c.Id)) :  await OnvifCacheProfileCache.Request((c.ZoneId, c.Id));
                                  if (resp != null && !string.IsNullOrEmpty(resp.SerializedCameraInfo))
                                  {
                                      var cameraInfo =
                                          JsonConvert.DeserializeObject<CameraInformation>(
                                              resp.SerializedCameraInfo,
                                              JsonUtil.IgnoreExceptionSettings());
                                      if (cameraInfo != null)
                                      {
                                          var cameraProfiles = cameraInfo.Profile;
                                          var convertedProfiles = cameraProfiles
                                              .Select(o =>
                                              {
                                                  var itemModel =
                                                      o.ConvertTo<BaseAdapterCameraProfileItemModel>();
                                                  itemModel.LastQueried = DateTimeOffset.UtcNow;
                                                  return itemModel;
                                              }).OrderByDescending(o => o.VideoEncoderConfiguration.H264 != null)
                                              .ThenBy(o => o.Resolution.Width).ToList();
                                          foreach (var p in convertedProfiles)
                                          {
                                              p.Resolution =
                                                  new Size(
                                                      p.VideoEncoderConfiguration.Resolution
                                                          .Width,
                                                      p.VideoEncoderConfiguration.Resolution
                                                          .Height);
                                          }

                                          if (cameraProfiles != null)
                                          {
                                              var getProfilesResponses = convertedProfiles.Select(o => o)
                                                  .ToDictionary(o => new Tuple<Guid, string>(c.Id, o.Name),
                                                      profile => profile)
                                                  .Where(o => !string.IsNullOrEmpty(o.Value.StreamUri))
                                                  .OrderBy(o => o.Value.IsH264).ThenByDescending(o =>
                                                      o.Value.Resolution.Width + o.Value.Resolution.Height);
                                              Update(getProfilesResponses.ToArray());
                                          }
                                      }
                                  }
                              }
                          }
                      }
                      catch (Exception e)
                      {
                      }
                  }
                  else
                  {
                      try
                      {
                          if (cameras != null)
                          {
                              if (parameters.AcceptedCacheDuration > TimeSpan.Zero)
                              {
                                  if (ItemCollection.Any(o => o.Key.Item1 == c.Id &&
                                                              o.Value.LastQueried > DateTimeOffset.UtcNow -
                                                              parameters.AcceptedCacheDuration))
                                  {
                                      return;
                                  }
                              }

                              var timeStarted = DateTimeOffset.UtcNow;
                              var onvifManager = new ONVIFManager(c.Address, c.Username, c.Password);
                              while (onvifManager.OnvifCameraConnection == null ||
                                     onvifManager.OnvifCameraConnection.MediaClient == null &&
                                     timeStarted > DateTimeOffset.UtcNow - TimeSpan.FromSeconds(15))
                              {
                                  await Task.Delay(100);
                              }

                              if (onvifManager.OnvifCameraConnection.MediaClient == null)
                              {
                                  return;
                              }

                              var mediaClient = onvifManager.OnvifCameraConnection.MediaClient;
                              var profiles = await mediaClient.GetProfilesAsync();
                              var convertedProfiles = profiles.Profiles
                                  .Select(o =>
                                  {
                                      var itemModel = o.ConvertTo<BaseAdapterCameraProfileItemModel>();
                                      itemModel.LastQueried = DateTimeOffset.UtcNow;
                                      return itemModel;
                                  }).ToList();
                              foreach (var p in convertedProfiles)
                              {
                                  if (p.token.IsNotEmpty())
                                  {
                                      if (p.VideoEncoderConfiguration.Encoding != VideoEncoding.H264)
                                      {
                                          continue;
                                      }

                                      try
                                      {
                                          var uri =
                                              await onvifManager.OnvifCameraConnection.MediaClient
                                                  .GetStreamUriAsync(
                                                      new StreamSetup
                                                      {
                                                          Stream = StreamType.RTPUnicast,
                                                          Transport = new Transport
                                                              {Protocol = TransportProtocol.RTSP}
                                                      }, p.token);
                                          if (uri.Uri.IsNotEmpty())
                                          {
                                              p.StreamUri = uri.Uri;
                                              try
                                              {
                                                  p.Resolution =
                                                      new Size(
                                                          p.VideoEncoderConfiguration.Resolution
                                                              .Width,
                                                          p.VideoEncoderConfiguration.Resolution
                                                              .Height);
                                              }
                                              catch (Exception e)
                                              {
                                                  // exceptionOut?.Invoke(e);
                                              }

                                              //if (firstProfile)
                                              //{
                                              //    c.CurrentOnvifProfile = p;
                                              //    firstProfile = false;
                                              //}
                                          }
                                      }
                                      catch (Exception e)
                                      {
                                          Console.WriteLine($"GetStreamUriAsync {e}");
                                          //  exceptionOut?.Invoke(e);
                                      }
                                  }
                              }

                              if (profiles != null)
                              {
                                  var getProfilesResponses = convertedProfiles.Select(o => o)
                                      .ToDictionary(o => new Tuple<Guid, string>(c.Id, o.Name),
                                          profile => profile)
                                      .Where(o => !string.IsNullOrEmpty(o.Value.StreamUri))
                                      .OrderBy(o => o.Value.IsH264).ThenByDescending(o =>
                                          o.Value.Resolution.Width + o.Value.Resolution.Height);
                                  Update(getProfilesResponses.ToArray());
                              }
                          }
                      }
                      catch (Exception e)
                      {
                      }
                  }
              }
          ));
        }
    }
}