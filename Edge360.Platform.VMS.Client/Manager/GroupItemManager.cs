﻿using System;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;

namespace Edge360.Platform.VMS.Client.Manager
{
    /// <summary>
    ///     GroupId
    /// </summary>
    public class GroupItemManager : BaseItemManager<GroupItemModel, Guid>
    {
        public GroupItemManager(ILog log = null)
        {
            _log = log;
            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;
        }

        public async Task EditGroup(GroupItemModel group)
        {
        }

        public async Task RemoveGroup(Guid groupId)
        {
        }

        public async Task RefreshGroup(Guid groupId)
        {
        }

        public async Task RefreshGroups()
        {
        }

        private void HubManagerOnSignalRClientEvent(ESignalRClientEvent clientEvent, Guid zoneId, string data)
        {
        }
    }
}