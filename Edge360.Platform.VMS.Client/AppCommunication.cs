﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.Extensions.CommandLine;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control;
using Edge360.Platform.VMS.Client.View.Control.Layouts;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Client.View.Window;
using Edge360.Platform.VMS.Common.Interfaces.Communication.IPC;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Cameras;
using ServiceStack;
using Telerik.WinControls.UI;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.TabbedWindow;
using Unity;
using static Edge360.Platform.VMS.Communication.CommunicationManager;
using Application = System.Windows.Application;

namespace Edge360.Platform.VMS.Client
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant,
        InstanceContextMode = InstanceContextMode.Single)]
    public class AppCommunication : IAppCommunication
    {
        private Window _getVisualParent;

        public async void StartupArgs(string[] args)
        {
            CommandLineManager.Start(args);

            var startupArgs = CommandLineManager.StartupArgs;


            var handleCreateCache = await HandleCreateCache(startupArgs);
            if (handleCreateCache)
            {
                return;
            }

            var offlineMode = await HandleOfflineMode(CommandLineManager.StartupArgs);
            var handleLayout = await HandleRecoveryLayout(startupArgs);

            try
            {
                var allWindows = ObjectResolver.ResolveAll<TabbedWindow>()?.ToList();
                if (allWindows != null)
                {
                    foreach (var t in allWindows)
                    {
                        try
                        {
                            _ = t.TryUiAsync(() =>
                            {
                                if (t.WindowState == WindowState.Minimized)
                                {
                                    t.WindowState = WindowState.Normal;
                                }

                                _getVisualParent = t.GetVisualParent<Window>();
                                _getVisualParent.GlobalActivate();
                            }).ConfigureAwait(false);
                        }
                        catch (Exception e)
                        {
                            _log?.Error(e);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }

            if (startupArgs.Layout != Guid.Empty)
            {
                try
                {
                    Application.Current.Dispatcher?.Invoke(async () =>
                    {
                        var gridLayout = await GetManagerByZoneId().GridLayoutService
                            .GetGridLayout(startupArgs.Layout);
                        if (gridLayout != null)
                        {
                            var monitoringTab = ObjectResolver.ResolveAll<MonitoringTab>()
                                ?.FirstOrDefault(o => o.TabIsFocused()) ?? await ObjectResolver
                                .Resolve<TabbedWindow>()?.OpenTab<MonitoringTab>();

                            if (monitoringTab != null)
                            {
                                LayoutTreeViewModel.LayoutBeenInitialized = false;
                                monitoringTab.MonitoringView?.MonitorTreeControl?.LayoutTreeView?.Model
                                    ?.HandleStartupLayout(gridLayout);
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            //Console.WriteLine(string.Join(", ", args).CollapseWhitespace().TrimEnd(','));
        }

        public void WatchdogPing(long timestamp)
        {
            //  Console.WriteLine($"Watchdog Pinged us at {timestamp}");
            OnWatchdogPing?.Invoke(timestamp);
        }


        public static async Task<bool> HandleOfflineMode(CommandLineManager.CommandLineArgs startupArgs)
        {
            if (startupArgs.OfflineLogin)
            {
                var directMgr = App.GlobalUnityContainer?.Resolve<DirectModeManager>();
                if (directMgr != null)
                {
                    directMgr.IsOfflineMode = true;
                    return true;
                }
            }

            return false;
        }

        public static async Task<bool> HandleRecoveryLayout(CommandLineManager.CommandLineArgs startupArgs)
        {
            try
            {
                if (startupArgs.LoadRecoveryLayout)
                {
                    try
                    {
                        StartCommunicationManager();
                        var loginModel = CreateLayoutLogin(startupArgs);
                        await loginModel.Connect(loginModel.DirectModeMgr.IsOfflineMode ? loginModel : null);

                        var loadRecoveryLayout = await loginModel.LayoutItemMgr.LoadRecoveryLayout(loginModel.Credential);
                        await loginModel.CameraItemMgr.LoadCacheFile(loginModel.Credential);
                        if (loadRecoveryLayout)
                        {
                            _log?.Info(
                                $"Successful Recovery {loginModel?.LayoutItemMgr?.LayoutRecoveryCache?.Storage?.RecoveryCacheList?.Count}");
                            var mainTabbedWindowStarted = false;
                            await Application.Current.Dispatcher.InvokeAsync(async () =>
                            {
                                var tabbedWindow =
                                    ObjectResolver.Resolve<TabbedWindow>() ??
                                    App.GlobalUnityContainer?.Resolve<TabbedWindow>();
                                var monitoringTab = tabbedWindow?.Items.OfType<MonitoringTab>().FirstOrDefault( ) ?? await tabbedWindow?.Model?.TabWindow?.OpenTab<MonitoringTab>();
                                var recoveryMonitoringWindowItemModels = loginModel.LayoutItemMgr.LayoutRecoveryCache.Storage
                                    .RecoveryCacheList.OrderBy(o => o.DisplayIndex).Where(o => o.RecoveryTiles.Count > 0).ToList();
                                if (recoveryMonitoringWindowItemModels.Count == 0)
                                {
                                    monitoringTab.MonitoringView.TileGrid.Model.GridSize = new Size(3,3);
                                }
                                foreach (var recovery in recoveryMonitoringWindowItemModels)
                                {
                                    _log?.Info(
                                        $"Loading Recovery Pane {mainTabbedWindowStarted} GridSize:{recovery.GridSize} DisplayIndex:{recovery.DisplayIndex} RecoveryTiles.Count:{recovery.RecoveryTiles.Count}");
                                    try
                                    {

                                        if (tabbedWindow != null)
                                        {
                                            if (!mainTabbedWindowStarted)
                                            {
                                                mainTabbedWindowStarted = true;
                                                tabbedWindow.SelectedItem = monitoringTab;
                                                
                                                await Task.Delay(10);
                                                if (monitoringTab != null)
                                                {
                                                    monitoringTab.MonitoringView.TileGrid.Model.GridSize =
                                                        new Size(recovery.GridSize.X, recovery.GridSize.Y);
                                                    try
                                                    {
                                                        var allScreen = Screen.AllScreens[recovery.DisplayIndex];
                                                        tabbedWindow.WindowState = WindowState.Normal;
                                                        _log?.Info($"Setting Window Location {allScreen.Bounds} {allScreen.Primary} {allScreen.WorkingArea}");
                                                        tabbedWindow.Left = allScreen.Bounds.X + 8;
                                                        tabbedWindow.Top = allScreen.Bounds.Y + 8;
                                                        tabbedWindow.WindowState = WindowState.Maximized;
                                                    }
                                                    catch (Exception e)
                                                    {

                                                    }

                                                    var recoveryTileItemModels = recovery.RecoveryTiles.Select(o =>
                                                    {
                                                        //var camera = loginModel.CameraItemMgr
                                                        //    .CameraCacheDictionary
                                                        //    .FirstOrDefault(c => c.Key == o.CameraId).Value ?? loginModel.CameraItemMgr
                                                        //    .ItemCollection
                                                        //    .FirstOrDefault(c => c.Key == o.CameraId).Value;
                                                        var cameraDescriptor = new CameraDescriptor
                                                            {Id = o.CameraId};
                                                        var item1 = new Size(o.GridLocation.X, o.GridLocation.Y);
                                                        return new Tuple<Size, CameraDescriptor>(
                                                            item1, cameraDescriptor);
                                                    });
                                                    var tileGridViewModel = monitoringTab.MonitoringView.TileGrid.Model;
                                                    var _ = Task.Run(async () =>
                                                    {
                                                        //await Task.Delay(200);
                                                       // await App.Current.Dispatcher.Invoke(async () =>
                                                      //  {
                                                      try
                                                      {
                                                          _log?.Info($"Launching TileGrid Main Layout");
                                                          //await App.Current.Dispatcher.Invoke(async () =>
                                                          //{
                                                          var layoutItemModel = new LayoutItemModel
                                                          {
                                                              GridSize = new Size(recovery.GridSize.X,
                                                                  recovery.GridSize.Y),
                                                              CameraCollection =
                                                                  new ObservableCollection<
                                                                      Tuple<Size, CameraDescriptor>>(
                                                                      recoveryTileItemModels)
                                                          };
                                                          tileGridViewModel.GridSize = new Size(recovery.GridSize.X,recovery.GridSize.Y);
                                                          await tileGridViewModel
                                                              .HandleLayoutItem(
                                                                  layoutItemModel);
                                                          //});
                                                          // });
                                                      }
                                                      catch (Exception e)
                                                      {
                                                          _log?.Error($"Error: {e.GetType()} {e.Message}");
                                                      }
                                                    });

                                                }

                                                tabbedWindow.Show();

                                            }
                                            else
                                            {
                                                _log?.Info($"Spawning ");
                                                var tw = new RadTabbedWindow {WindowState = WindowState.Normal};
                                              

                                                tabbedWindow.OnTabbedWindowCreating(tw,
                                                    new TabbedWindowCreatingEventArgs(tabbedWindow, tw, null, null,
                                                        tabbedWindow));
                                                try
                                                {
                                                    
                                                    var allScreen = Screen.AllScreens[recovery.DisplayIndex];
                                                    _log?.Info($"Setting Window Location {allScreen.Bounds} {allScreen.Primary} {allScreen.WorkingArea}");
                                                    tw.Left = allScreen.Bounds.X + 8;
                                                    tw.Top = allScreen.Bounds.Y + 8;
                                                }
                                                catch (Exception e)
                                                {

                                                }
                                                var tabbedWindowViewModel = tw.DataContext as TabbedWindowViewModel;
                                                var monTab = await tabbedWindowViewModel?.TabWindow?.OpenTab<MonitoringTab>();
                                                if (monTab != null)
                                                {
                                                    monTab.MonitoringView.TileGrid.Model.GridSize =
                                                        new Size(recovery.GridSize.X, recovery.GridSize.Y);
                                                    var recoveryTileItemModels = recovery.RecoveryTiles.Select(o =>
                                                    {
                                                        //var camera = loginModel.CameraItemMgr
                                                        //    .CameraCacheDictionary
                                                        //    .FirstOrDefault(c => c.Key == o.CameraId).Value ?? loginModel.CameraItemMgr
                                                        //    .ItemCollection
                                                        //    .FirstOrDefault(c => c.Key == o.CameraId).Value;
                                                        var cameraDescriptor = new CameraDescriptor
                                                            {Id = o.CameraId};
                                                        var item1 = new Size(o.GridLocation.X, o.GridLocation.Y);
                                                        return new Tuple<Size, CameraDescriptor>(
                                                            item1, cameraDescriptor);
                                                    });
                                                    var tileGridViewModel = monTab.MonitoringView.TileGrid.Model;
                                                    var _ = Task.Run(async () =>
                                                    {
                                                        try
                                                        {
                                                            //await Task.Delay(200);
                                                            // await App.Current.Dispatcher.Invoke(async () =>
                                                            //{
                                                            _log?.Info($"Launching TileGrid Layout");
                                                            //await App.Current.Dispatcher.Invoke(async() =>
                                                            //{
                                                            var layoutItemModel = new LayoutItemModel
                                                            {
                                                                GridSize = new Size(recovery.GridSize.X,
                                                                    recovery.GridSize.Y),
                                                                CameraCollection =
                                                                    new ObservableCollection<
                                                                        Tuple<Size, CameraDescriptor>>(
                                                                        recoveryTileItemModels)
                                                            };
                                                          
                                                            await tileGridViewModel
                                                                .HandleLayoutItem(
                                                                    layoutItemModel);
                                                            //});
                                                          
                                                            // });
                                                        }
                                                        catch (Exception e)
                                                        {
                                                            _log?.Error($"Error: {e.GetType()} {e.Message}");
                                                        }
                                                    });
                                                }

                                                tw.WindowState = WindowState.Maximized;
                                                tw.Show();
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        _log?.Error($"Error: {e.GetType()} {e.Message}");
                                    }
                                }
                            });
                        }


                        SettingsManager.SaveSettings();
                    }
                    catch (Exception e)
                    {
                        _log?.Error($"Error: {e.GetType()} {e.Message}");
                    }

                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                _log?.Error($"Error: {e.GetType()} {e.Message}");
                return false;
            }
        }

        private static LoginViewModel CreateLayoutLogin(CommandLineManager.CommandLineArgs startupArgs)
        {
            var loginModel = App.GlobalUnityContainer?.Resolve<LoginViewModel>(); // ?? new LoginViewModel();
            loginModel.Credential =
                SettingsManager.Settings.Login.Credentials.ConvertTo<UserLoginItemModel>();
            //new UserLoginItemModel
            loginModel.Credential.PasswordLength = 8;
            if (!string.IsNullOrEmpty(startupArgs.BearerToken))
            {
                loginModel.Credential.BearerToken = startupArgs.BearerToken;
            }

            if (!string.IsNullOrEmpty(startupArgs.ServerAddress))
            {
                loginModel.Credential.Domain = startupArgs.ServerAddress;
            }

            loginModel.Credential.Password = "*********";
            if (!string.IsNullOrEmpty(startupArgs.RefreshToken))
            {
                loginModel.Credential.RefreshToken = startupArgs.RefreshToken;
            }

            loginModel.Credential.RememberMe = true;
            if (!string.IsNullOrEmpty(startupArgs.Username))
            {
                loginModel.Credential.UserName = startupArgs.Username;
            }

            loginModel.Credential.IgnoreValidation = true;
            loginModel.Credential.IsBearerLogin = true;
            SettingsManager.Settings.Login.Credentials = loginModel.Credential;
            return loginModel;
        }

        public static async Task<bool> HandleCreateCache(CommandLineManager.CommandLineArgs startupArgs)
        {
            if (startupArgs.CacheCreate && !string.IsNullOrEmpty(startupArgs.BearerToken))
            {
                try
                {
                    StartCommunicationManager();
                    var loginModel = App.GlobalUnityContainer?.Resolve<LoginViewModel>() ?? new LoginViewModel();
                    loginModel.Credential = SettingsManager.Settings.Login.Credentials.ConvertTo<UserLoginItemModel>();
                    //new UserLoginItemModel
                    loginModel.Credential.PasswordLength = 8;
                    loginModel.Credential.BearerToken = startupArgs.BearerToken;
                    loginModel.Credential.Domain = startupArgs.ServerAddress;
                    loginModel.Credential.Password = "*********";
                    loginModel.Credential.RefreshToken = startupArgs.RefreshToken;
                    loginModel.Credential.RememberMe = true;
                    loginModel.Credential.UserName = startupArgs.Username;
                    loginModel.Credential.IgnoreValidation = true;
                    loginModel.Credential.IsBearerLogin = true;
                    SettingsManager.Settings.Login.Credentials = loginModel.Credential;
                    await loginModel.Connect(null, false);
                    SettingsManager.SaveSettings();
                }
                catch (Exception e)
                {
                }

                return true;
            }

            return false;
        }

        public static void SendIpcCommunication(string[] commandLineArgs)
        {
            var pipeFactory =
                new ChannelFactory<IAppCommunication>(
                    new NetNamedPipeBinding(),
                    new EndpointAddress("net.pipe://localhost/edge360_vms_client"));

            var service = pipeFactory.CreateChannel();

            service.StartupArgs(commandLineArgs);
        }

        public static event Action<long> OnWatchdogPing;

        public static async Task SendCloseHeartbeatWatchdog()
        {
            try
            {
                var pipeFactory =
                    new ChannelFactory<IWatchdogCommunication>(
                        new NetNamedPipeBinding(),
                        new EndpointAddress("net.pipe://localhost/edge360_vms_watchdog"));

                var service = pipeFactory.CreateChannel();

                service.CloseConnection(Process.GetCurrentProcess().Id);
            }
            catch (Exception e)
            {
                //  Console.WriteLine(e);
            }
        }

        public static async Task SendOpenHeartbeatWatchdog()
        {
            bool isConnected = false;
            while (!isConnected)
            {
                try
                {
                    var pipeFactory =
                        new ChannelFactory<IWatchdogCommunication>(
                            new NetNamedPipeBinding(),
                            new EndpointAddress("net.pipe://localhost/edge360_vms_watchdog"));

                    var service = pipeFactory.CreateChannel();

                    var currentProcess = Process.GetCurrentProcess();
                    if (currentProcess.MainModule != null)
                    {
                        service.OpenConnection(currentProcess.Id, currentProcess.MainModule.FileName);
                        StartWatchdogHeartbeat();
                        isConnected = true;
                    }
                }
                catch (Exception e)
                {
                    await Task.Delay(1000);
                    // Console.WriteLine(e);
                }

                await Task.Delay(10000);
            }
        }

        public static async void StartWatchdogHeartbeat()
        {
            var _ = await Task.Factory.StartNew(async () =>
            {
                while (!App.IsExiting)
                {
                    try
                    {
                        var pipeFactory =
                            new ChannelFactory<IWatchdogCommunication>(
                                new NetNamedPipeBinding(),
                                new EndpointAddress("net.pipe://localhost/edge360_vms_watchdog"));

                        var service = pipeFactory.CreateChannel();

                        service.SendHeartbeat(Process.GetCurrentProcess().Id, DateTimeOffset.UtcNow.UtcTicks);
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine(e);
                    }

                    await Task.Delay(3000);
                }
            }, TaskCreationOptions.LongRunning).ConfigureAwait(false);
        }

        public static async void InitIpcCommunication()
        {
            var _ = await Task.Factory.StartNew(async () =>
            {
                try
                {
                    using (var _namedPipe = new ServiceHost(new AppCommunication()))
                    {
                        _namedPipe.AddServiceEndpoint(typeof(IAppCommunication),
                            new NetNamedPipeBinding(), "net.pipe://localhost/edge360_vms_client");
                        _namedPipe.Open();
                        while (true)
                        {
                            await Task.Delay(1000);
                        }
                    }
                }
                catch (Exception e)
                {
                    //  _log?.Info(e);
                    await Task.Delay(5000);
                    InitIpcCommunication();
                }
            }, TaskCreationOptions.LongRunning).ConfigureAwait(false);
        }

        public static void InitWatchdogCommunication()
        {
            Task.Run(async () =>
            {
                try
                {
                    using (var _namedPipe = new ServiceHost(new AppCommunication()))
                    {
                        _namedPipe.AddServiceEndpoint(typeof(IAppCommunication),
                            new NetNamedPipeBinding(), "net.pipe://localhost/edge360_vms_watchdog");
                        _namedPipe.Open();
                        while (true)
                        {
                            await Task.Delay(35);
                        }
                    }
                }
                catch (Exception e)
                {
                    // _log?.Info(e);
                    await Task.Delay(5000);
                    InitWatchdogCommunication();
                }
            });
        }
    }
}