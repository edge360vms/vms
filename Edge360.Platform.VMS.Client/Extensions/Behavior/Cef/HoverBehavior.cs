﻿using System;
using System.Windows;
using System.Windows.Interactivity;
using CefSharp.Wpf;

namespace CefSharp.MinimalExample.Wpf.Behaviours
{
    public class DownloadHandler : IDownloadHandler
    {
        public void OnBeforeDownload(IWebBrowser chromiumWebBrowser, IBrowser browser, DownloadItem downloadItem,
            IBeforeDownloadCallback callback)
        {
            if (!callback.IsDisposed)
            {
                using (callback)
                {
                    callback.Continue(downloadItem.SuggestedFileName, true);
                }
            }
        }

        public void OnDownloadUpdated(IWebBrowser chromiumWebBrowser, IBrowser browser, DownloadItem downloadItem,
            IDownloadItemCallback callback)
        {
        }
    }

    public class HoverLinkBehaviour : Behavior<ChromiumWebBrowser>
    {
        // Using a DependencyProperty as the backing store for HoverLink. This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HoverLinkProperty = DependencyProperty.Register("HoverLink",
            typeof(string), typeof(HoverLinkBehaviour), new PropertyMetadata(string.Empty));

        public string HoverLink
        {
            get => (string) GetValue(HoverLinkProperty);
            set => SetValue(HoverLinkProperty, value);
        }

        protected override void OnAttached()
        {
            AssociatedObject.StatusMessage += OnStatusMessageChanged;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.StatusMessage -= OnStatusMessageChanged;
        }

        private void OnStatusMessageChanged(object sender, StatusMessageEventArgs e)
        {
            var chromiumWebBrowser = sender as ChromiumWebBrowser;
            chromiumWebBrowser.Dispatcher.BeginInvoke((Action) (() => HoverLink = e.Value));
        }
    }
}