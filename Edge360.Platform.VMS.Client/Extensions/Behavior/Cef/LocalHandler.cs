﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using CefSharp;

public class CustomProtocolSchemeHandlerFactory : ISchemeHandlerFactory
{
    public const string SchemeName = "local";

    public IResourceHandler Create(IBrowser browser, IFrame frame, string schemeName, IRequest request)
    {
        var uri = new Uri(request.Url);
        var file = uri.AbsolutePath;

        var fileExtension = Path.GetExtension(file);
        var mimeType = ResourceHandler.GetMimeType(fileExtension);
        return ResourceHandler.FromFilePath(file, mimeType);
    }
}

public class LocalSchemeHandler : ResourceHandler
{
    public override CefReturnValue ProcessRequestAsync(IRequest request, ICallback callback)
    {
        var uri = new Uri(request.Url);
        var file = uri.AbsolutePath;

        Task.Run(() =>
        {
            using (callback)
            {
                if (!File.Exists(file))
                {
                    callback.Cancel();
                    return;
                }

                byte[] bytes = File.ReadAllBytes(file);

                var stream = new MemoryStream(bytes);
                if (stream == null)
                {
                    callback.Cancel();
                }
                else
                {
                    stream.Position = 0;
                    ResponseLength = stream.Length;

                    var fileExtension = Path.GetExtension(file);
                    MimeType = GetMimeType(fileExtension);
                    StatusCode = (int)HttpStatusCode.OK;
                    Stream = stream;

                    callback.Continue();
                }
            }
        });
        return base.ProcessRequestAsync(request, callback);
    }
    //public override bool ProcessRequestAsync(IRequest request, ICallback callback)
    //{
    //    var uri = new Uri(request.Url);
    //    var file = uri.AbsolutePath;

    //    Task.Run(() =>
    //    {
    //        using (callback)
    //        {
    //            if (!File.Exists(file))
    //            {
    //                callback.Cancel();
    //                return;
    //            }

    //            byte[] bytes = File.ReadAllBytes(file);

    //            var stream = new MemoryStream(bytes);
    //            if (stream == null)
    //            {
    //                callback.Cancel();
    //            }
    //            else
    //            {
    //                stream.Position = 0;
    //                ResponseLength = stream.Length;

    //                var fileExtension = Path.GetExtension(file);
    //                MimeType = GetMimeType(fileExtension);
    //                StatusCode = (int)HttpStatusCode.OK;
    //                Stream = stream;

    //                callback.Continue();
    //            }
    //        }
    //    });

    //    return true;
    //}
}