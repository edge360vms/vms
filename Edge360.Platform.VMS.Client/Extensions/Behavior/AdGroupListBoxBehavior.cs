﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Management;
using Edge360.Platform.VMS.Client.View.Tab;
using Telerik.Windows.DragDrop.Behaviors;
using Unity;

namespace Edge360.Platform.VMS.Client.Extensions.Behavior
{
    public class AdGroupListBoxBehavior : ListBoxDragDropBehavior
    {
        public static readonly DependencyProperty GroupManagementViewModelProperty = DependencyProperty.Register(
            "GroupManagementViewModel", typeof(GroupManagementViewModel), typeof(AdGroupListBoxBehavior),
            new PropertyMetadata(default(GroupManagementViewModel), PropertyChangedCallback));

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is GroupManagementView)
            {

            }
        }

        public GroupManagementViewModel GroupManagementViewModel
        {
            get => (GroupManagementViewModel)GetValue(GroupManagementViewModelProperty);
            set => SetValue(GroupManagementViewModelProperty, value);
        }

        protected override IEnumerable<object> CopyDraggedItems(DragDropState state)
        {
            DragDropCanceled(state);
            return null;
            //return base.CopyDraggedItems(state);
        }

        public override void Drop(DragDropState state)
        {
            base.DragDropCanceled(state);
            try
            {
                var tabUtil = GroupManagementViewModel ??= TabUtil.FindFocusedTab<ManagementTab>().ManagementView.GroupManagementView.Model;
                if (GroupManagementViewModel?.SelectedGroup != null)
                {
                    if (!state.DestinationItemsSource.Contains(state.DraggedItems.OfType<object>().FirstOrDefault()))
                    {
                        var draggedGroup = state.DraggedItems.OfType<AdGroupItemModel>();
                        foreach (var ad in draggedGroup)
                        {
                            if (ad != null)
                            {
                                if (GroupManagementViewModel.AvailableAdGroupList.Any(
                                    o => o.GroupId == ad.GroupId))
                                {
                                    //Console.WriteLine($"AddAdGroup {draggedGroup.CommonName}");
                                    GroupManagementViewModel.AddAdGroup(ad);
                                }
                                else
                                {
                                    //Console.WriteLine($"RemoveAdGroup {draggedGroup.CommonName}");
                                    GroupManagementViewModel.RemoveAdGroup(ad);
                                }
                            }
                        }
                      

                        var draggedUsers = state.DraggedItems.OfType<UserItemModel>();
                        foreach (var u in draggedUsers)
                        {
                            if (u != null)
                            {
                                if (GroupManagementViewModel.AvailableUsersList.Any(o => o.UserId == u.UserId))
                                {
                                    //Console.WriteLine($"AddMember {draggedUser.Username}");
                                    GroupManagementViewModel.AddMember(u);
                                }
                                else
                                {
                                    //Console.WriteLine($"RemoveMember {draggedUser.Username}");
                                    GroupManagementViewModel.RemoveMember(u);
                                }
                            }
                        }
                 
                    }
                }
            }
            catch
            {
            }

            // DragDropCanceled(state);
        }
    }
}