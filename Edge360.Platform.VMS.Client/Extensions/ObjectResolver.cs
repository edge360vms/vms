﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Edge360.Platform.VMS.Client.Extensions
{
    public static class ObjectResolver
    {
        public static ConcurrentDictionary<string, WeakReference> ObjectDictionary { get; set; } =
            new ConcurrentDictionary<string, WeakReference>();

        public static IEnumerable<object> ResolveMany(Type type, string name = null)
        {
            if (string.IsNullOrEmpty(name))
            {
            }

            return ObjectDictionary.Select(o => o.Value.Target).Where(o => o != null && o.GetType() == type);
        }

        public static IEnumerable<T> ResolveMany<T>(string name = null) where T : class
        {
            if (string.IsNullOrEmpty(name))
            {
            }

            return ObjectDictionary.Select(o => o.Value.Target).OfType<T>();
        }

        public static void Register<T>(object o, string name = null, bool overwrite = false) where T : class
        {
            RegisterType<T>(o, name, overwrite);
        }

        public static void Register(Type type,object o, string name = null, bool overwrite = false)
        {
            RegisterType(type,o, name, overwrite);
        }


        private static void RegisterType(Type type,object o, string name, bool overwrite)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = type.FullName;
            }

            var tabCount = overwrite || ResolveMany(type)?.Count() == 0
                ? ""
                : (ResolveMany(type)?.Count() + 1).ToString();
            name = $"{name}{tabCount}";
           // _log?.Info($"Adding {name} to Dictionary...");
            if (!ObjectDictionary.TryAdd(name, new WeakReference(o)))
            {
                ObjectDictionary[name] = new WeakReference(o);
            }
        }

        private static void RegisterType<T>(object o, string name, bool overwrite) where T : class
        {
            if (string.IsNullOrEmpty(name))
            {
                name = typeof(T).FullName;
            }

            var tabCount = overwrite || ResolveMany<T>()?.Count() == 0
                ? ""
                : (ResolveMany<T>()?.Count() + 1).ToString();
            name = $"{name}{tabCount}";
            //_log?.Info($"Adding {name} to Dictionary...");
            if (!ObjectDictionary.TryAdd(name, new WeakReference(o)))
            {
                ObjectDictionary[name] = new WeakReference(o);
            }
        }

        public static List<T> ResolveAll<T>(string name = null) where T : class
        {
            if (string.IsNullOrEmpty(name))
            {
                return ObjectDictionary?.Where(o => o.Value?.Target?.GetType() == typeof(T)).Select(o => o.Value.Target)
                    .OfType<T>().ToList();
            }

            return ObjectDictionary?.Where(o => o.Value?.Target?.GetType() == typeof(T) && o.Key == name)
                .Select(o => o.Value.Target).OfType<T>().ToList();
        }

        public static T Resolve<T>(string name = null) where T : class
        {
            if (string.IsNullOrEmpty(name))
            {
                name = typeof(T).FullName;
            }

            return ObjectDictionary?.FirstOrDefault(o => o.Value?.Target?.GetType() == typeof(T) && o.Key == name).Value
                ?.Target as T;
        }

        public static void RemoveEntries<T>()
        {
            if (ObjectDictionary == null || ObjectDictionary.IsEmpty)
            {
                return;
            }

            var objectsToRemove = ObjectDictionary.Where(o => o.Value?.GetType() == typeof(T));
            foreach (var w in objectsToRemove)
            {
                ObjectDictionary.TryRemove(w.Key, out _);
            }
        }

        public static void RemoveEntry(object obj)
        {
            var entry = ObjectDictionary.FirstOrDefault(o => o.Value?.Target == obj);
            if (entry.Value != null)
            {
                ObjectDictionary?.TryRemove(entry.Key, out _);
            }
        }

        public static void RemoveEntry<T>(string name = "")
        {
            if (ObjectDictionary == null || ObjectDictionary.IsEmpty)
            {
                return;
            }

            var objectsToRemove = string.IsNullOrEmpty(name)
                ? ObjectDictionary.FirstOrDefault(o => o.Value?.GetType() == typeof(T))
                : ObjectDictionary.FirstOrDefault(o => o.Value?.GetType() == typeof(T) && o.Key == name);

            if (objectsToRemove.Value != null)
            {
                ObjectDictionary?.TryRemove(objectsToRemove.Key, out _);
            }
        }
    }
}