﻿using Prism.Commands;

namespace Edge360.Platform.VMS.Client.Extensions.Commands
{
    public interface IApplicationCommands
    {
        CompositeCommand SelectCameraSearchCommand { get; }
    }

    public class EdgeApplicationCommands : IApplicationCommands
    {
        private CompositeCommand _selectCameraSearchCommand = new CompositeCommand();
        public CompositeCommand SelectCameraSearchCommand
        {
            get { return _selectCameraSearchCommand; }
        }
    }
}