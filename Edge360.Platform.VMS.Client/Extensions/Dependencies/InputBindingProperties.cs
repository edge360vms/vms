﻿using System.Windows;
using System.Windows.Input;

namespace Edge360.Platform.VMS.Client.Extensions.Dependencies
{
    public class InputBindingProperties
    {
        public static readonly DependencyProperty TabbedWindowInputProperty = DependencyProperty.RegisterAttached(
            "TabbedWindowInput", typeof(InputBindingCollection), typeof(InputBindingProperties),
            new FrameworkPropertyMetadata(new InputBindingCollection(),
                (sender, e) =>
                {
                    var element = sender as UIElement;
                    if (element == null)
                    {
                        return;
                    }

                    element.InputBindings.Clear();
                    element.InputBindings.AddRange((InputBindingCollection) e.NewValue);
                }));

        public static void SetTabbedWindowInput(DependencyObject element, InputBindingCollection value)
        {
            element.SetValue(TabbedWindowInputProperty, value);
        }

        public static InputBindingCollection GetTabbedWindowInput(DependencyObject element)
        {
            return (InputBindingCollection) element.GetValue(TabbedWindowInputProperty);
        }
    }
}