﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using ControlzEx.Standard;
using log4net;
using NDesk.Options;

namespace Edge360.Platform.VMS.Client.Extensions.CommandLine
{
    public class CommandLineManager
    {
        static ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static CommandLineArgs StartupArgs { get; set; }

        public static void Start(string[] args)
        {
            CheckInvalidArgs(args);
        }

        public static bool CheckInvalidArgs(string[] args)
        {
            StartupArgs = new CommandLineArgs();

            var p = new OptionSet
            {
                {
                    "export=", "<RecordingProfileId|> Launches QuickExport of RecordingProfiles",
                    v => StartupArgs.QuickExportProfiles = v
                },
                {
                    "wl", "Enables WindowsLogin",
                    v => StartupArgs.UseWindowsLogin = v != null
                },
                {
                    "offline", "Enables OfflineMode",
                    v => StartupArgs.OfflineLogin = v != null
                },
                {
                    "cache", "Creates a cache",
                    v => StartupArgs.CacheCreate = v != null
                },
                {
                    "recovery", "Loads a recovered save",
                    v => StartupArgs.LoadRecoveryLayout = v != null
                },
                {
                    "h|help", "Shows help",
                    v => StartupArgs.ShowHelp = v != null
                },
                {
                    "u=", "the {USERNAME} to login with.",
                    v => StartupArgs.Username = v
                },
                {
                    "al", "Autologin if RefreshToken is Valid",
                    v => StartupArgs.AutoLogin = v != null
                },
                {
                    "b=", "the {BEARERTOKEN} to login with.",
                    v => StartupArgs.BearerToken = v
                },
                {
                    "r=", "the {REFRESHTOKEN} to login with.",
                    v => StartupArgs.RefreshToken = v
                },
                {
                    "p=",
                    "The {PASSWORD} to login with.",
                    v => StartupArgs.Password = v
                },
                {
                    "s=", "The {SERVER ADDRESS} to login with",
                    v => StartupArgs.ServerAddress = v
                },
                {
                    "preset=", "The {PRESET} to display",
                    (int v) => StartupArgs.Preset = v
                },
                {
                    "l|layout=", "The {PRESET} to display",
                    v => StartupArgs.Layout = Guid.Parse(v)
                },
                {
                    "silent", "Hides the console",
                    v => StartupArgs.HideConsole = v != null
                },
                {
                    "m|multi", "Allows for Multiple Instances",
                    v => StartupArgs.AllowMultiInstance = v != null
                }
            };

            try
            {
                p.Parse(args);
            }
            catch (OptionException optionException)
            {
                _log?.Info(optionException.Message);
                return true;
            }

            if (StartupArgs.ShowHelp)
            {
                p.WriteOptionDescriptions(Console.Out);

                return false;
            }

            if (string.IsNullOrEmpty(StartupArgs.Username) ||
                string.IsNullOrEmpty(StartupArgs.ServerAddress) ||
                string.IsNullOrEmpty(StartupArgs.Password))
            {
                p.WriteOptionDescriptions(Console.Out);
                return true;
            }

            return false;
        }

        public class CommandLineArgs
        {

            public bool AllowMultiInstance { get; set; }
            public bool AutoLogin { get; set; }
            public string BearerToken { get; set; }
            public bool HideConsole { get; set; }
            public Guid Layout { get; set; }
            public string Password { get; set; }
            public int Preset { get; set; }
            public string RefreshToken { get; set; }
            public string ServerAddress { get; set; }
            public bool ShowHelp { get; set; }
            public string Username { get; set; }
            public bool LoadCache { get; set; }
            public bool OfflineLogin { get; set; }
            public bool LoadRecoveryLayout { get; set; }
            public bool CacheCreate { get; set; }
            public bool UseWindowsLogin { get; set; }
            public string QuickExportProfiles { get; set; }
        }
    }
}