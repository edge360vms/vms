﻿using System.Windows;
using System.Windows.Data;
using Edge360.Platform.VMS.Client.Extensions.Helpers;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.Extensions.Control
{
    public class DataFormPasswordField : DataFormDataField
    {
        protected override DependencyProperty GetControlBindingProperty()
        {
            return PasswordBoxHelper.BoundPassword;
        }

        protected override System.Windows.Controls.Control GetControl()
        {
            var dependencyProperty = GetControlBindingProperty();
            var passwordBox = new RadPasswordBox();
            PasswordBoxHelper.SetBindPassword(passwordBox, true);

            if (DataMemberBinding != null)
            {
                var binding = DataMemberBinding;
                PasswordBoxHelper.SetBoundPassword(passwordBox, passwordBox.Password);
                passwordBox.SetBinding(dependencyProperty, binding);
            }

            passwordBox.SetBinding(IsEnabledProperty,
                new Binding("IsReadOnly") {Source = this, Converter = new InvertedBooleanConverter()});

            return passwordBox;
        }
    }
}