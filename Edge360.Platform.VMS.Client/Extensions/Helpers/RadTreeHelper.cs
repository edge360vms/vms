﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Common.Util;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.Extensions.Helpers
{
    public static class PopupHelper
    {
        public static readonly DependencyProperty AlwaysFollowPlacementTargetProperty =
            DependencyProperty.RegisterAttached(
                "AlwaysFollowPlacementTarget", typeof(bool), typeof(PopupHelper),
                new PropertyMetadata(default(bool), OnFollowPropertyChangedCallback));

        private static void OnFollowPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool) e.NewValue && d is Popup popup)
            {
                void OnPopupOnIsVisibleChanged(object sender, RoutedEventArgs routedEventArgs)
                {
                    var w = Window.GetWindow(popup);
                    if (w != null)
                    {
                        if (popup.PlacementTarget is Panel splitter)
                        {
                            splitter.SizeChanged += (o, args) =>
                            {
                                if (popup.IsOpen)
                                {
                                    var mi = typeof(Popup).GetMethod("UpdatePosition",
                                        BindingFlags.NonPublic |
                                        BindingFlags.Instance);
                                    if (mi != null)
                                    {
                                        mi.Invoke(popup, null);
                                    }
                                }
                            };
                        }

                        w.LocationChanged += delegate
                        {
                            if (popup.IsOpen)
                            {
                                var mi = typeof(Popup).GetMethod("UpdatePosition",
                                    BindingFlags.NonPublic | BindingFlags.Instance);
                                if (mi != null)
                                {
                                    mi.Invoke(popup, null);
                                }
                            }
                        };
                        // Also handle the window being resized (so the popup's position stays
                        //  relative to its target element if the target element moves upon 
                        //  window resize)
                        //w.SizeChanged += delegate
                        //{
                        //    if (popup.IsOpen)
                        //    {
                        //        var mi = typeof(Popup).GetMethod("UpdatePosition",
                        //            System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                        //        if (mi != null)
                        //        {
                        //            Console.WriteLine($"SizeChanged...");
                        //            mi.Invoke(popup, null);
                        //        }
                        //    }
                        //};
                    }

                    popup.Loaded -= OnPopupOnIsVisibleChanged;
                }

                popup.Loaded += OnPopupOnIsVisibleChanged;
            }
        }

        public static void SetAlwaysFollowPlacementTarget(DependencyObject element, bool value)
        {
            element.SetValue(AlwaysFollowPlacementTargetProperty, value);
        }

        public static bool GetAlwaysFollowPlacementTarget(DependencyObject element)
        {
            return (bool) element.GetValue(AlwaysFollowPlacementTargetProperty);
        }
    }

    public static class TextBoxHelper
    {
        public static readonly DependencyProperty SelectAllOnClickProperty = DependencyProperty.RegisterAttached(
            "SelectAllOnClick", typeof(bool), typeof(TextBoxHelper),
            new PropertyMetadata(default(bool), SelectAllOnClickPropertyChangedCallback));

        public static readonly DependencyProperty IgnoreClickEventProperty = DependencyProperty.RegisterAttached(
            "IgnoreClickEvent", typeof(bool), typeof(TextBoxHelper), new PropertyMetadata(default(bool)));

        private static void SelectAllOnClickPropertyChangedCallback(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if (d is TextBox txtBox)
            {
                txtBox.RemoveHandler(UIElement.MouseDownEvent,
                    new MouseButtonEventHandler(TxtBoxOnMouseLeftButtonDown));
                txtBox.AddHandler(UIElement.MouseDownEvent, new MouseButtonEventHandler(TxtBoxOnMouseLeftButtonDown),
                    true);
                // txtBox.MouseLeftButtonDown -= TxtBoxOnMouseLeftButtonDown;
                // txtBox.MouseLeftButtonDown += TxtBoxOnMouseLeftButtonDown;
            }
        }

        public static void SetSelectAllOnClick(DependencyObject element, bool value)
        {
            element.SetValue(SelectAllOnClickProperty, value);
        }

        public static bool GetSelectAllOnClick(DependencyObject element)
        {
            return (bool) element.GetValue(SelectAllOnClickProperty);
        }

        public static void SetIgnoreClickEvent(DependencyObject element, bool value)
        {
            element.SetValue(IgnoreClickEventProperty, value);
        }

        public static bool GetIgnoreClickEvent(DependencyObject element)
        {
            return (bool) element.GetValue(IgnoreClickEventProperty);
        }

        private static void TxtBoxOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is TextBox txtBox)
            {
                txtBox.SelectAll();

                e.Handled = true;
                txtBox.ReleaseMouseCapture();
            }
        }
    }

    public static class ButtonHelper
    {
        public static readonly DependencyProperty ClearWatermarkOnClickProperty = DependencyProperty.RegisterAttached(
            "ClearWatermarkOnClick", typeof(bool), typeof(ButtonHelper),
            new PropertyMetadata(default(bool), ClearWatermarkOnClickPropertyChangedCallback));

        private static void ClearWatermarkOnClickPropertyChangedCallback(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is bool val && val)
            {
                if (d is Button button)
                {
                    button.Click -= ClearWatermarkOnClick;
                    button.Click += ClearWatermarkOnClick;
                }
            }
        }

        public static void SetClearWatermarkOnClick(DependencyObject element, bool value)
        {
            element.SetValue(ClearWatermarkOnClickProperty, value);
        }

        public static bool GetClearWatermarkOnClick(DependencyObject element)
        {
            return (bool) element.GetValue(ClearWatermarkOnClickProperty);
        }

        private static void ClearWatermarkOnClick(object sender, RoutedEventArgs e)
        {
            if (sender is Button button)
            {
                var watermark = button.GetVisualParent<RadWatermarkTextBox>();
                if (watermark != null)
                {
                    watermark.Text = string.Empty;
                }
            }
        }
    }

    public static class ItemsControlHelper
    {
        public static readonly DependencyProperty AutoSelectFirstOnChangeProperty = DependencyProperty.RegisterAttached(
            "AutoSelectFirstOnChange", typeof(bool), typeof(ItemsControlHelper),
            new PropertyMetadata(default(bool), OnAutoSelectPropertyChangedCallback));

        private static void OnAutoSelectPropertyChangedCallback(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if ((bool) e.NewValue)
            {
                if (d is MultiSelector tabControl)
                {
                    tabControl.Loaded -= AutoSelectLastTabControlOnDataContextChanged;
                    tabControl.Loaded += AutoSelectLastTabControlOnDataContextChanged;
                    SelectTabDelayed(tabControl);
                    //tabControl.DataContextChanged -= TabControlOnDataContextChanged;
                    //tabControl.DataContextChanged += TabControlOnDataContextChanged;

                    //tabControl.IsVisibleChanged += TabControlOnIsVisibleChanged;
                }
            }
        }

        private static void TabControlOnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (sender is MultiSelector tabControl)
            {
                
                {
                    try
                    {
                        SelectTabDelayed(tabControl);
                        //Math.Max(0,
                        //tabControl.Items[tabControl.Items.Count - 1] ??
                        //((dynamic) tabControl.Items[tabControl.Items.Count - 1]).Value ?? 0);
                    }
                    catch (Exception exception)
                    {
                    }
                }
            }
        }

        private static void TabControlOnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {   if (sender is MultiSelector tabControl)
            {
                
                {
                    try
                    {
                        SelectTabDelayed(tabControl);
                        //Math.Max(0,
                        //tabControl.Items[tabControl.Items.Count - 1] ??
                        //((dynamic) tabControl.Items[tabControl.Items.Count - 1]).Value ?? 0);
                    }
                    catch (Exception exception)
                    {
                    }
                }
            }
        }

        public static void SetAutoSelectFirstOnChange(DependencyObject element, bool value)
        {
            element.SetValue(AutoSelectFirstOnChangeProperty, value);
        }

        public static bool GetAutoSelectFirstOnChange(DependencyObject element)
        {
            return (bool) element.GetValue(AutoSelectFirstOnChangeProperty);
        }

        private static void AutoSelectLastTabControlOnDataContextChanged(object sender, EventArgs e)
        {
            if (sender is MultiSelector tabControl)
            {
                var dpd = DependencyPropertyDescriptor.FromProperty(ItemsControl.ItemsSourceProperty,
                    typeof(MultiSelector));
                if (dpd != null)
                {
                    try
                    {
                        dpd.RemoveValueChanged(tabControl, ItemSourceChanged);
                    }
                    catch (Exception exception)
                    {
                    }
                    SelectTabDelayed(tabControl);
                    dpd.AddValueChanged(tabControl, ItemSourceChanged);
                }
            }
        }

        private static void ItemSourceChanged(object sender, EventArgs e)
        {
            if (sender is MultiSelector tabControl)
            {
                
                {
                    try
                    {
                        SelectTabDelayed(tabControl);

                        //Math.Max(0,
                        //tabControl.Items[tabControl.Items.Count - 1] ??
                        //((dynamic) tabControl.Items[tabControl.Items.Count - 1]).Value ?? 0);
                    }
                    catch (Exception exception)
                    {
                    }
                }
            }
        }

        private static void SelectTabDelayed(MultiSelector tabControl, int index = 0)
        {
            try
            {
                if (tabControl == null || tabControl.SelectedItem != null)
                {
                    return;
                }

                using var cts = new CancellationTokenSource(15000);
                cts.Token.ThrowIfCancellationRequested();
                App.Current?.TryUiAsync((async () =>
                {
                    try
                    {
                        Trace.WriteLine("SelectTabDelayed");
                        await Task.Run(async () =>
                        {
                            while (tabControl.Items.Count == 0)
                            {
                                await Task.Delay(400);
                            }
                            await Task.Delay(35);
                        });
                        tabControl.SelectedIndex = index;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }), DispatcherPriority.Background, cts.Token);
            }
            catch (Exception e)
            {
                
            }
        }
    }

    public static class RadTreeHelper
    {
        public static readonly DependencyProperty AutoSelectFirstProperty = DependencyProperty.RegisterAttached(
            "AutoSelectFirst", typeof(bool), typeof(RadTreeHelper),
            new PropertyMetadata(default(bool), PropertyChangedCallback));

        public static readonly DependencyProperty AlwaysExpandProperty = DependencyProperty.RegisterAttached(
            "AlwaysExpand", typeof(bool), typeof(RadTreeHelper),
            new PropertyMetadata(default(bool), AlwaysExpandPropertyChangedCallback));

        public static readonly DependencyProperty AutoExpandProperty = DependencyProperty.RegisterAttached(
            "AutoExpand", typeof(bool), typeof(RadTreeHelper),
            new PropertyMetadata(default(bool), AutoExpandPropertyChangedCallback));

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is bool val && val)
            {
                if (d is RadTreeView tree)
                {
                    tree.Loaded -= AutoSelectTreeOnLoaded;
                    tree.Loaded += AutoSelectTreeOnLoaded;
                    tree.DataContextChanged += (o, args) => AutoSelectTreeOnLoaded(o, null);
                }
            }
        }

        public static void SetAutoSelectFirst(DependencyObject element, bool value)
        {
            element.SetValue(AutoSelectFirstProperty, value);
        }

        public static bool GetAutoSelectFirst(DependencyObject element)
        {
            return (bool) element.GetValue(AutoSelectFirstProperty);
        }

        private static void AlwaysExpandPropertyChangedCallback(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if ((bool) e.NewValue)
            {
                if (d is RadTreeView tree)
                {
                    tree.Loaded -= ExpandTreeOnLoaded;
                    tree.Loaded += ExpandTreeOnLoaded;

                    tree.DataContextChanged += (o, args) => ExpandTreeOnLoaded(tree, null);
                }
            }
        }

        public static void SetAlwaysExpand(DependencyObject element, bool value)
        {
            element.SetValue(AlwaysExpandProperty, value);
        }

        public static bool GetAlwaysExpand(DependencyObject element)
        {
            return (bool) element.GetValue(AlwaysExpandProperty);
        }

        private static void AutoExpandPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool) e.NewValue)
            {
                if (d is RadTreeView tree)
                {
                    tree.Loaded -= ExpandTreeOnLoaded;
                    tree.Loaded += ExpandTreeOnLoaded;
                }
            }
        }

        public static void SetAutoExpand(DependencyObject element, bool value)
        {
            element.SetValue(AutoExpandProperty, value);
        }

        public static bool GetAutoExpand(DependencyObject element)
        {
            return (bool) element.GetValue(AutoExpandProperty);
        }

        private static async void AutoSelectTreeOnLoaded(object sender, RoutedEventArgs e)
        {
            if (sender is RadTreeView tree)
            {
                try
                {
                    await Task.Run(async () =>
                    {
                        if (e == null)
                        {
                            await Task.Delay(35);
                        }

                        if (tree.Items.Count > 0 && tree.SelectedItem == null)
                        {
                            _ = tree.TryUiAsync(() => { tree.SelectedItem = tree.Items[0]; }, DispatcherPriority.Background);
                        }
                    });
                }
                catch (Exception)
                {
                }
            }
        }

        private static async void ExpandTreeOnLoaded(object sender, RoutedEventArgs e)
        {
            if (sender is RadTreeView tree)
            {
                await Task.Run(async () =>
                {
                    if (e == null)
                    {
                        await Task.Delay(35);
                    }

                    _ = tree.TryUiAsync(() =>
                    {
                        try
                        {
                            tree.ExpandAll();
                        }
                        catch (Exception exception)
                        {
                        }
                    },DispatcherPriority.Background);


                });
                //tree.Loaded -= ExpandTreeOnLoaded;
            }
        }
    }
}