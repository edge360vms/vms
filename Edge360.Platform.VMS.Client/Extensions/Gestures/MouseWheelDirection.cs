﻿namespace Edge360.Platform.VMS.Client.Extensions.Gestures
{
    public enum MouseWheelDirection
    {
        Up,
        Down
    }
}