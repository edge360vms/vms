﻿using System;
using System.Windows.Input;
using System.Windows.Markup;

namespace Edge360.Platform.VMS.Client.Extensions.Gestures
{
    public class MouseWheel : MarkupExtension
    {
        public MouseWheelDirection Direction { get; set; }
        public ModifierKeys Keys { get; set; }

        public MouseWheel()
        {
            Keys = ModifierKeys.None;
            Direction = MouseWheelDirection.Down;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new MouseWheelGesture(Keys, Direction);
        }
    }
}