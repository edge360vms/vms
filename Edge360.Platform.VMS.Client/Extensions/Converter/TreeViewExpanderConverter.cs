﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;

namespace Edge360.Platform.VMS.Client.Extensions.Converter
{
    public class TreeViewExpanderConverter : IValueConverter
    {
        public Style BlankStyle { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is CameraItemModel)
            {
                return BlankStyle;
            }

            if (value is LayoutItemModel)
            {
                return BlankStyle;
            }

            return parameter;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class VolumeTreeViewExpanderConverter : IValueConverter
    {
        public Style BlankStyle { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ServerItemModel)
            {
                return BlankStyle;
            }

            //if (value is LayoutItemModel)
            //{
            //    return BlankStyle;
            //}

            return parameter;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}