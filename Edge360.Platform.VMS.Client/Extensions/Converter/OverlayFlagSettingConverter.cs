﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Edge360.Platform.VMS.Common.Enums.Video;

namespace Edge360.Platform.VMS.Client.Extensions.Converter
{
    public class OverlayFlagSettingConverter : IValueConverter
    {
        public EControlOverlaySettings MatchSetting { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is EControlOverlaySettings setting)
            {
                if (setting.HasFlag(MatchSetting))
                {
                    return Visibility.Visible;
                }
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}