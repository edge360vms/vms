﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Edge360.Platform.VMS.Client.Extensions.Converter
{
    [ValueConversion(typeof(bool), typeof(bool))]
    public class InverseBooleanConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            if (targetType != typeof(bool))
            {
                return false;
                //  throw new InvalidOperationException("The target must be a boolean");
            }

            return value != null && !(bool) value;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}