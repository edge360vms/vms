﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using Telerik.Windows.Controls.Rating;

namespace Edge360.Platform.VMS.Client.Extensions.Converter
{
    public class StringNullOrEmptyToVisibilityConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.IsNullOrEmpty(value as string)
                ? Visibility.Collapsed
                : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class KeyValueSelectorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value != null)
                {

                    return value;
                }
            }
            catch (Exception e)
            {

            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            {
                try
                {
                    if (value != null)
                    {
                        var val = value.GetType().IsGenericType;
                        var valu = value.GetType().IsConstructedGenericType;

                        var vas = value.GetType() == typeof(KeyValuePair<,>);

                        return value.GetType() == typeof(KeyValuePair<,>) ? ((dynamic) value).Value : value;
                    }
                }
                catch (Exception e)
                {

                }

                return null;
            }
        }
    }
}