﻿using System.Windows;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.ItemModel.Camera;

namespace Edge360.Platform.VMS.Client.Extensions.Styles
{
    internal class ItemContainerStyleSelector : StyleSelector
    {
        public Style RootStyle { get; set; }

        public override Style SelectStyle(object item, DependencyObject container)
        {
            if (item is CameraItemModel)
            {
                base.SelectStyle(item, container);

                return RootStyle;
            }

            return base.SelectStyle(item, container);
        }
    }
}