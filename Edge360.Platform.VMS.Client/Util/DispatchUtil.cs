﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Edge360.Platform.VMS.Client.Util
{
    public static class DispatchUtil
    {
        public static async Task TryUiAsync(this object obj, Action a, DispatcherPriority priority = DispatcherPriority.Normal, CancellationToken token = default, Action<Exception> outException = null)
        {
            if (a == null)
            {
                return;
            }

            try
            {
                await Application.Current?.Dispatcher?.InvokeAsync(async () =>
                {
                    try
                    {
                        a.Invoke();
                    }
                    catch (Exception e)
                    {
                        outException?.Invoke(e);
                    }
                },priority, token);
            }
            catch (Exception e)
            {
                outException?.Invoke(e);
            }
        }
    }
}