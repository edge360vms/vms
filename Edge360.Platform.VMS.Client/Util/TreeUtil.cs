﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Edge360.Platform.VMS.Client.ItemModel.DataObject;

namespace Edge360.Platform.VMS.Client.Util
{
    public static class TreeUtil
    {
        public static List<T> RemoveFirstAndLast<T>(this List<T> list)
        {
            try
            {
                list.RemoveAt(0);
                list.RemoveAt(list.Count - 1);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return list;
        }

        public static T FindNodeByParentId<T>(this ObservableCollection<NodeObject> nodeList, Guid id)
            where T : NodeObject
        {
            foreach (var node in nodeList)
            {
                var foundChild = node.Children.Any(o => o.ObjectId == id);
                if (foundChild && node is T nodeParent)
                {
                    return nodeParent;
                }

                if (node.Children.FindNodeByParentId<T>(id) is T n)
                {
                    return n;
                }
            }

            return null;
        }

        public static T FindParentNodeByChildId<T>(this ObservableCollection<NodeObject> nodeList, Guid id)
            where T : NodeObject
        {
            foreach (var node in nodeList)
            {
                var foundChild = node.Children.Any(o => o.ParentId == id);
                if (foundChild && node is T nodeParent)
                {
                    return nodeParent;
                }

                if (node.Children.FindNodeByParentId<T>(id) is T n)
                {
                    return n;
                }
            }

            return null;
        }

        public static T FindNodeById<T>(this ObservableCollection<NodeObject> nodeList, Guid? id) where T : NodeObject
        {
            foreach (var node in nodeList)
            {
                if (node is T nodeType)
                {
                    if (node.ObjectId == id)
                    {
                        return nodeType;
                    }
                }

                if (node.Children.FindNodeById<T>(id) is T n)
                {
                    return n;
                }
            }

            return null;
        }

        public static IEnumerable<T> GetAllChildren<T>(this T node) where T : NodeObject
        {
            if (node != null)
            {
                yield return node;

                foreach (var o in node.Children)
                {
                    var child = (T) o;
                    foreach (var childChild in GetAllChildren(child))
                    {
                        yield return childChild;
                    }
                }
            }
        }
    }
}