﻿using System;
using System.Net.NetworkInformation;

namespace Edge360.Platform.VMS.Client.Util
{
    public static class NetworkUtil
    {
        public static NetworkInterface[] PrintNetworkInformation()
        {
            var networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (var network in networkInterfaces)
            {
                if (network.NetworkInterfaceType == NetworkInterfaceType.Loopback)
                {
                    continue;
                }

                if (network.OperationalStatus != OperationalStatus.Up)
                {
                    continue;
                }

                Console.WriteLine(network.Description);
                var unicastIpInfoCol = network.GetIPProperties().UnicastAddresses;
                foreach (var unicastIpInfo in unicastIpInfoCol)
                {
                    Console.WriteLine("\tIP Address is {0}", unicastIpInfo.Address);
                    Console.WriteLine("\tSubnet Mask is {0}", unicastIpInfo.IPv4Mask);
                }
            }

            return networkInterfaces;
        }
    }
}