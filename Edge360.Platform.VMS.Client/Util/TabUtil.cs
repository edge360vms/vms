﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Collection;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Vms.Client.VideoExport.Common.Events;
using Telerik.Windows.Controls;
using WispFramework.Extensions.Tasks;

namespace Edge360.Platform.VMS.Client.Util
{
    public static class TabUtil
    {
        public static T FindFocusedTab<T>() where T : EdgeTabItem
        {
            var edgeTabItems = ObjectResolver.ResolveAll<T>();
            return edgeTabItems.FirstOrDefault(o => o != null && o.TabIsFocused());
        }

        public static async Task ExportVideo(this UIElement element, Guid cameraId, Guid recordingProfileId,
            DateTime? SelectedTime = null, TimeSpan? selectedTimeSpan = null, TimeSpan? startRange = null)
        {
            if (SelectedTime == null || SelectedTime == DateTime.MinValue)
            {
                SelectedTime = DateTime.Now;
            }

            if (selectedTimeSpan == null || selectedTimeSpan == TimeSpan.Zero)
            {
                selectedTimeSpan = TimeSpan.FromSeconds(120);
            }

            if (startRange == null || startRange == TimeSpan.Zero)
            {
                startRange = TimeSpan.FromMinutes(15);
            }

            try
            {
                var tabbedWindow = element?.ParentOfType<RadTabbedWindow>();
                if (tabbedWindow != null)
                {
                    var tab = await tabbedWindow.FindTab<ExportArchiveTab>() ??
                              await tabbedWindow.OpenTab<ExportArchiveTab>();
                    if (tab != null)
                    {
                        tab.FocusTab(tab);
                        try
                        {
                            _ = Task.Run(async () =>
                            {
                                try
                                {
                                    using (var cts = new CancellationTokenSource(12000))
                                    {
                                        await Application.Current.Dispatcher.InvokeAsync(async () =>
                                        {
                                            var sw = new Stopwatch();
                                            sw.Start();
                                            while (tab.ExportArchiveView.Model.EventAggregator == null ||
                                                   !tab.ExportArchiveView.Model.IsConnected)
                                            {
                                                await Task.Delay(100);
                                            }

                                            sw.Stop();
                                            Console.WriteLine(
                                                $"Waited {sw.ElapsedMilliseconds}ms for ExportTabViewModel");
                                            await Task.Delay(1200);
                                            tab.ExportArchiveView.Model.EventAggregator.GetEvent<QuickExportEvent>()
                                                .Publish(
                                                    new QuickExport
                                                    {
                                                        CameraId = cameraId,
                                                        RecordingProfileId = recordingProfileId,
                                                        SelectionStart =
                                                            SelectedTime.Value - selectedTimeSpan.Value,
                                                        SelectionEnd =
                                                            SelectedTime.Value + selectedTimeSpan.Value,
                                                        SelectionStartRange =
                                                            SelectedTime.Value - startRange.Value,
                                                        SelectionEndRange =
                                                            SelectedTime.Value + startRange.Value
                                                    });
                                        }, DispatcherPriority.Background ,cts.Token);
                                    }
                                }
                                catch (Exception e)
                                {
                                }
                            }).ConfigureAwait(false);
                        }
                        catch (Exception e)
                        {
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }
    }
}