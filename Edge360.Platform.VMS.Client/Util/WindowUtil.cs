﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.ItemModel.Collection;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Client.View.Window;
using Edge360.Platform.VMS.Common.Taskbar;
using Edge360.Platform.VMS.UI.Common.Util;
using log4net;
using Telerik.Windows.Controls;
using static Edge360.Platform.VMS.Client.Extensions.ObjectResolver;

namespace Edge360.Platform.VMS.Client.Util
{
    public static class WindowUtil
    {
        static ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static async Task<T> FindTab<T>(this RadTabbedWindow window, bool setFocused = false)
            where T : EdgeTabItem, new()
        {
            return await App.Current.Dispatcher.InvokeAsync(() =>
            {
                var existingTab = window?.Items?.OfType<T>().FirstOrDefault();
                if (existingTab != null && setFocused)
                {
                    window.SelectedItem = existingTab;
                    return existingTab;
                }

                return default;
            });
        }

        public static bool TabIsFocused(this FrameworkElement view)
        {
            try
            {
                return App.Current.Dispatcher.Invoke(() =>
                {
                    return view.IsVisible && view.GetVisualParent<Window>() ==
                        Application.Current.Windows.OfType<Window>().FirstOrDefault(x => x.IsActive);
                }, DispatcherPriority.Background);
                //if (view.ParentOfType<RadTabbedWindow>() == null && view.ParentOfType<TabbedWindow>() == null)
                //{
                //    return false;
                //}
            }
            catch (Exception)
            {
            }

            return false;
        }

        public static T GetParentTab<T>(this DependencyObject child) where T : DependencyObject
        {
            if (child == null)
            {
                return null;
            }

            if (VisualTreeHelper.GetParent(child) is T)
            {
                return VisualTreeHelper.GetParent(child) as T;
            }

            var parent = VisualTreeHelper.GetParent(child);
            return GetParentTab<T>(parent);
        }

        public static T GetParentRadWindow<T>(this DependencyObject child) where T : RadWindow
        {
            if (child == null)
            {
                return default;
            }

            var parentObject = VisualTreeHelper.GetParent(child);

            if (parentObject == null)
            {
                return default;
            }

            if (parentObject is T parent)
            {
                return parent;
            }

            return GetParentRadWindow<T>(parentObject);
        }


        public static async Task<T> OpenTab<T>(this RadTabbedWindow window, string name = null, bool forceCreate = true)
            where T : EdgeTabItem, new()
        {
            try
            {
                return await App.Current.Dispatcher.InvokeAsync(() =>
                {
                    //_log?.Info($"OpenTab {Environment.StackTrace}");
                    if (string.IsNullOrEmpty(name))
                    {
                        name = typeof(T).FullName;
                        //   _log?.Info(name);
                    }

                    var tab = Resolve<T>(forceCreate ? name : null);
                    if (tab == null || tab.IsDisposed || forceCreate)
                    {
                        tab = new T();
                        Register<T>(tab, name, !forceCreate);
                        window.Items.Add(tab);
                    }

                    window.SelectedItem = tab;
                    return tab;
                });;
            }
            catch (Exception e)
            {
                return default;
            }
        }


        public static void CloseWindow<T>(string name = null, bool closeAll = false) where T : RadWindow
        {
            if (string.IsNullOrEmpty(name))
            {
                name = typeof(T).FullName;
                //   _log?.Info(name);
            }

            if (closeAll)
            {
                var windows =
                    ResolveMany<T>();

                foreach (var w in windows)
                {
                    if (!(w.GetType() != typeof(T)))
                    {
                        try
                        {
                            w.Close();
                        }
                        catch (Exception)
                        {
                        }
                    }

                    if (w.DataContext != null)
                    {
                        RemoveEntry(w.DataContext);
                    }
                }

                RemoveEntries<T>();
            }
            else
            {
                var window = Resolve<T>(name);
                if (window != null)
                {
                    try
                    {
                        window.Close();
                    }
                    catch (Exception)
                    {
                    }

                    RemoveEntry<T>(name);
                    if (window.DataContext != null)
                    {
                        RemoveEntry(window.DataContext);
                    }

                    _log?.Info($"Removing Entry {name}");
                }
            }
        }


        public static T ShowWindow<T>(string name = null, bool forceCreate = false) where T : RadWindow, new()
        {
            if (string.IsNullOrEmpty(name))
            {
                name = typeof(T).FullName;
                //   _log?.Info(name);
            }

            var window =
                Resolve<T>(name); // App.MainContainer.Resolve<T>(name);

            if (window == null)
            {
                _log?.Info($"{name} was null!");
            }

            try
            {
                if (window == null || forceCreate)
                {
                    if (forceCreate)
                    {
                        var tabCount = ResolveMany<T>()?.Count() ?? 0;
                        name = $"{name}{tabCount + 1}";
                    }

                    window = new T();
                    Register<T>(window, name, !forceCreate);
                }
                else if (window.WindowState == WindowState.Minimized)
                {
                    window.WindowState = WindowState.Normal;
                }


                window.BringToFront();
                window.Show();
                if (window.Header != null && window.Tag?.ToString() != null &&
                    !string.IsNullOrEmpty(window.Tag?.ToString()))
                {
                    try
                    {
                        window.SetTaskbarVisibility(true, window.Tag?.ToString());
                    }
                    catch (Exception e)
                    {
                    }
                }

                try
                {
                    TaskbarUtil.SetLaunchTarget(new WindowInteropHelper(window.GetParentWindow<Window>()).Handle);
                }
                catch (Exception e)
                {
                    _log?.Info(e);
                }
            }
            catch (Exception e)
            {
            }

            return window;
        }
    }
}