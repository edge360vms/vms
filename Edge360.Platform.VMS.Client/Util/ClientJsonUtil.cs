﻿using System.Reflection;
using System.Windows.Controls;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.Manager;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Prism.Commands;

namespace Edge360.Platform.VMS.Client.Util
{
    public static class ClientJsonUtil
    {
        public static JsonSerializerSettings IgnoreExceptionSettings()
        {
            return new JsonSerializerSettings
            {
                ContractResolver = ShouldSerializeContractResolver.Instance,
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore,
                Error = (sender, args) => args.ErrorContext.Handled = true
            };
        }

        public class ShouldSerializeContractResolver : DefaultContractResolver
        {
            public static readonly ShouldSerializeContractResolver Instance =
                new ShouldSerializeContractResolver();

            protected override JsonProperty CreateProperty(MemberInfo member,
                MemberSerialization memberSerialization)
            {
                var property = base.CreateProperty(member, memberSerialization);

                var type = property?.PropertyType?.GetType();
                if (type != null && (type.BaseType == typeof(DelegateCommandBase) || type.IsAssignableFrom(typeof(ILog)) || type.IsAssignableFrom(typeof(ICommand)) || type.IsAssignableFrom(typeof(Control)) || type.BaseType == typeof(BaseItemManager)))
                {
                    property.ShouldSerialize = instance => { return false; };
                }

                return property;
            }
        }
    }
}