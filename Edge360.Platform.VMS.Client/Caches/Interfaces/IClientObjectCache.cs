﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Edge360.Platform.VMS.Client.Caches.Interfaces
{
    public interface IClientObjectCache
    {
        Task<IEnumerable<object>> GetAll();
    }
}