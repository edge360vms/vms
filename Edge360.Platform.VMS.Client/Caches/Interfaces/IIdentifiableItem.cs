﻿namespace Edge360.Platform.VMS.Client.Caches.Interfaces
{
    public interface IIdentifiableItem<TKeyType>
    {
        TKeyType Id { get; set; }
    }
}