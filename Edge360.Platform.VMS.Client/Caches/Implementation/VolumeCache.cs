﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.Caches.Base;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.Volumes;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.Caches.Implementation
{
    public class VolumeCache : ClientObjectCacheBase<VolumeCache, Guid, VolumeDescriptor>
    {
        protected override async Task<IEnumerable<VolumeDescriptor>> ResolveAll()
        {
            var result = await GetManagerByZoneId().VolumeService.GetVolumes();
            return result.Results;
        }

        protected override async Task<VolumeDescriptor> Resolver(Guid id)
        {
            try
            {
                // fix single selection so this doesnt freeze
                var response = await GetManagerByZoneId().VolumeService.GetVolumes(id);

                return response.Results.FirstOrDefault();
            }
            catch
            {
                return new VolumeDescriptor();
            }
        }
    }
}