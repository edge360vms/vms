﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.Caches.Base;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Cameras;

namespace Edge360.Platform.VMS.Client.Caches.Implementation
{
    public class CameraCache : ClientObjectCacheBase<CameraCache, Guid, CameraDescriptor>
    {
        //protected override async Task<IEnumerable<CameraDescriptor>> ResolveAll()
        //{
        //    var result = await GetManagerByZoneId()
        //        .CameraService.ListCameras();
        //    return result;
        //}

        //protected override async Task<CameraDescriptor> Resolver(Guid id)
        //{
        //    try
        //    {
        //        // fix single selection so this doesnt freeze
        //        return await Task.Run(async () =>
        //        {
        //            var response = await GetManagerByZoneId()
        //                .CameraService.GetCamera(id);
        //            return response.Camera;
        //        }).TimeoutAfter(TimeSpan.FromSeconds(1));
        //    }
        //    catch
        //    {
        //        return new CameraDescriptor {Label = "None"};
        //    }
        //}
        protected override Task<IEnumerable<CameraDescriptor>> ResolveAll()
        {
            return null;
        }

        protected override Task<CameraDescriptor> Resolver(Guid id)
        {
            return null;
        }
    }
}