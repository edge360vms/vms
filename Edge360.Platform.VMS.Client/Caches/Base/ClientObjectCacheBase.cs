﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.Caches.Interfaces;
using ServiceStack;
using WispFramework.Patterns.Generators;

namespace Edge360.Platform.VMS.Client.Caches.Base
{
    public abstract class ClientObjectCacheBase<TChildType, TKeyType, TDataType> : IClientObjectCache
        where TChildType : new()
    {
        private static TChildType _instance;

        public static TChildType Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new TChildType();
                }

                return _instance;
            }
        }

        public virtual TimeSpan ExpiryTime => TimeSpan.FromSeconds(30);

        public LazyFactory<TKeyType, Dynamic<TDataType>> FactoryCache { get; set; }

        private Dynamic<IEnumerable<TDataType>> ResolveAllDynamic { get; } =
            new Dynamic<IEnumerable<TDataType>>();

        protected ClientObjectCacheBase()
        {
            ResolveAllDynamic.WithAsyncEvaluator(async () => await ResolveAll()).Throttled(ExpiryTime);
            FactoryCache = new LazyFactory<TKeyType, Dynamic<TDataType>>(
                guid => new Dynamic<TDataType>().WithAsyncEvaluator(async () => await Resolver(guid))
                    .Throttled(ExpiryTime)
            );
        }

        public async Task<IEnumerable<object>> GetAll()
        {
            var all = await ResolveAllDynamic.GetValueAsync();
            foreach (var item in all)
            {
                var identifiable = GetIdentifiableFromObject(item);
                FactoryCache[identifiable.Id].Set(item);
            }

            return FactoryCache.InternalCache.Values.Select(async t => await t.GetValueAsync()).Select(t => t as object)
                .ToList();
        }

        public void Contaminate(TKeyType id)
        {
            if (FactoryCache.TryGetValue(id, out var result))
            {
                result.Poison();
            }
        }

        public async Task<TDataType> GetAsync(TKeyType id)
        {
            return await FactoryCache[id].GetValueAsync();
        }

        private static IIdentifiableItem<TKeyType> GetIdentifiableFromObject(TDataType item)
        {
            IIdentifiableItem<TKeyType> identifiable;
            try
            {
                identifiable = item.ConvertTo<IdentifiableItem<TKeyType>>();
            }
            catch (Exception)
            {
                throw new Exception("Object data type does not have Id property or it is not a TKeyType type");
            }

            return identifiable;
        }


        protected abstract Task<IEnumerable<TDataType>> ResolveAll();

        protected abstract Task<TDataType> Resolver(TKeyType id);
    }
}