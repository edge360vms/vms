﻿using Edge360.Platform.VMS.Client.Caches.Interfaces;

namespace Edge360.Platform.VMS.Client.Caches.Base
{
    public class IdentifiableItem<TKeyType> : IIdentifiableItem<TKeyType>
    {
        public TKeyType Id { get; set; }
    }
}