﻿using System;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.Extensions.CommandLine;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.View.Control;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using log4net;
using Unity;

namespace Edge360.Platform.VMS.Client.Services
{
    public class CacheUpdateService : IntervalService
    {
        private readonly CameraItemModelManager _cameraItemModel;
        private readonly DirectModeManager _directModeManager;
        private readonly LayoutItemManager _layoutManager;
        private readonly NodeObjectItemManager _nodeObjectManager;
        private readonly UserItemManager _userModeManager;

        public override TimeSpan TimeoutInterval { get; set; } = TimeSpan.FromSeconds(15);
        public override TimeSpan UpdateInterval { get; set; } = TimeSpan.FromSeconds(10);


        public CacheUpdateService(ILog log = null, DirectModeManager directModeManager = null,
            UserItemManager userModeManager = null, NodeObjectItemManager nodeObjectManager = null,
            CameraItemModelManager cameraItemModel = null, LayoutItemManager layoutManager = null)
        {
            _directModeManager = directModeManager;
            _userModeManager = userModeManager;
            _nodeObjectManager = nodeObjectManager;
            _cameraItemModel = cameraItemModel;
            _layoutManager = layoutManager;
        }

        public override async Task Update()
        {
            try
            {
                var currentLogin = ObjectResolver.Resolve<LoginViewModel>()?.Credential ??
                                   App.GlobalUnityContainer.Resolve<LoginViewModel>().Credential;
                await Task.Run(() =>
                {
                    try
                    {
                        if (currentLogin != null && !_directModeManager.IsOfflineMode &&
                            (LoginViewModel.IsLoggedIn || CommandLineManager.StartupArgs.CacheCreate))
                        {
                            if (_nodeObjectManager != null)
                            {
                            }

                            try
                            {
                                if (_layoutManager != null)
                                {
                                    _layoutManager.SaveRecoveryCache(currentLogin);
                                    _layoutManager.SaveCache(currentLogin);
                                }
                            }
                            catch (Exception e)
                            {
                            }

                            try
                            {
                                if (_cameraItemModel != null)
                                {
                                    _cameraItemModel.SaveCache(currentLogin);
                                }
                            }
                            catch (Exception e)
                            {
                            }

                            try
                            {
                                if (_userModeManager != null)
                                {
                                    _userModeManager.SaveCache(currentLogin);
                                }
                            }
                            catch (Exception e)
                            {
                            }
                        }
                    }
                    catch (Exception e)
                    {
                    }
                });
            }
            catch (Exception e)
            {
            }
        }
    }
}