﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.Services
{
    public interface IIntervalService
    {
        CancellationTokenSource ShutdownCancellationTokenSource { get; set; }
        CancellationTokenSource TimeoutCancellationTokenSource { get; set; }
        TimeSpan TimeoutInterval { get; set; }
        TimeSpan TimeoutIntervalMinimum { get; set; }
        TimeSpan UpdateInterval { get; set; }
        TimeSpan UpdateIntervalMinimum { get; set; }
        void Dispose();
        void Initialize();
        Task Update();
        void Shutdown();
    }

    public abstract class IntervalService : IDisposable, IIntervalService
    {
        public CancellationTokenSource ShutdownCancellationTokenSource { get; set; }
        public CancellationTokenSource TimeoutCancellationTokenSource { get; set; }
        public abstract TimeSpan TimeoutInterval { get; set; }
        public TimeSpan TimeoutIntervalMinimum { get; set; } = TimeSpan.FromMinutes(35);

        public abstract TimeSpan UpdateInterval { get; set; }
        public TimeSpan UpdateIntervalMinimum { get; set; } = TimeSpan.FromMilliseconds(35);

        public void Dispose()
        {
            ShutdownCancellationTokenSource?.Dispose();
            TimeoutCancellationTokenSource?.Dispose();
        }

        public virtual void Initialize()
        {
            ShutdownCancellationTokenSource = new CancellationTokenSource();

            Task.Factory.StartNew(async () =>
            {
                while (!ShutdownCancellationTokenSource.IsCancellationRequested)
                {
                    TimeoutCancellationTokenSource = new CancellationTokenSource();
                    var timeoutIntervalMinimum = TimeoutInterval < TimeoutIntervalMinimum
                        ? TimeoutIntervalMinimum
                        : TimeoutInterval;
                    TimeoutCancellationTokenSource.CancelAfter(timeoutIntervalMinimum);
                    await Task.Run(async () =>
                    {
                        await Update();
                        UpdateFinished?.Invoke();
                    }, TimeoutCancellationTokenSource.Token);
                    var updateIntervalMinimum =
                        UpdateInterval < UpdateIntervalMinimum ? UpdateIntervalMinimum : UpdateInterval;
                    await Task.Delay(updateIntervalMinimum);
                }
            }, ShutdownCancellationTokenSource.Token);
        }

        public event Action UpdateFinished; 
        public abstract Task Update();

        public virtual void Shutdown()
        {
            Dispose();
        }
    }
}