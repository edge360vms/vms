﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using Edge360.Platform.VMS.Client.Annotations;
using Prism.Commands;
using Application = System.Windows.Application;

namespace Edge360.Platform.VMS.Client.Services
{
    public class ActiveWindowService : IntervalService, INotifyPropertyChanged
    {
        public override TimeSpan TimeoutInterval { get; set; } = TimeSpan.FromSeconds(5);
        public override TimeSpan UpdateInterval { get; set; } = TimeSpan.FromSeconds(5);

        public ObservableCollection<WindowItemModel> ActiveWindows { get; } =
            new ObservableCollection<WindowItemModel>();

        public event PropertyChangedEventHandler PropertyChanged;

        public override async Task Update()
        {
            Application.Current?.Dispatcher?.InvokeAsync(() =>
            {
                try
                {
                    var currentWindows = Application.Current.Windows.OfType<Window>().ToList();
                    var models = currentWindows.Select(o =>
                    {
                        var model = new WindowItemModel();
                        var inter = new WindowInteropHelper(o);
                        model.Handle = inter.Handle;
                        model.Initialize(o);
                        return model;
                    });
                    var modelsToRemove = ActiveWindows?.Where(o => currentWindows.All(w => w != o?.OriginalWindow))
                        .ToList();
                    if (modelsToRemove != null)
                    {
                        foreach (var gone in modelsToRemove)
                        {
                            ActiveWindows.Remove(gone);
                        }
                    }

                    ActiveWindows.AddRange(models);
                }
                catch (Exception e)
                {
                }
            });
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class WindowItemModel : INotifyPropertyChanged
    {
        private Screen _currentScreen;
        private Window _originalWindow;

        private DelegateCommand _switchToNextScreen;

        public Screen CurrentScreen
        {
            get => _currentScreen;
            set
            {
                if (Equals(value, _currentScreen))
                {
                    return;
                }

                _currentScreen = value;
                OnPropertyChanged();
            }
        }

        public IntPtr Handle { get; set; }

        public Window OriginalWindow
        {
            get => _originalWindow;
            private set
            {
                if (Equals(value, _originalWindow))
                {
                    return;
                }

                _originalWindow = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand SwitchToNextScreen => _switchToNextScreen ??= new DelegateCommand(SwitchScreen);

        public event PropertyChangedEventHandler PropertyChanged;


        private void SwitchScreen()
        {
            // if (CurrentScreen.Bounds.IntersectsWith())
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Initialize(Window original)
        {
            try
            {
                OriginalWindow = original;

                CurrentScreen = Screen.FromHandle(Handle);
            }
            catch (Exception e)
            {
            }
        }
    }
}