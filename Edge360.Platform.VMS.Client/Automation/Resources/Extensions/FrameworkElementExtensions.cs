﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows;
using Edge360.Platform.VMS.Client.View.Control.Dialog.ObjectResolverDialog.Attributes;

namespace Edge360.Platform.VMS.Client.Automation.Resources.Extensions
{
    public static class FrameworkElementExtensions
    {
        public static void LoadResourceDictionaryByAttribute<TAttribute>(this FrameworkElement frameworkElement)
            where TAttribute : Attribute
        {
            var types = frameworkElement
                .GetType()
                .Assembly
                .GetTypes()
                .Where(t => t.GetCustomAttribute(typeof(DescriptorDrawerAttribute)) != null);
            foreach (var type in types)
            {
                if (Activator.CreateInstance(type) is ResourceDictionary dictionary)
                {
                    frameworkElement.Resources.MergedDictionaries.Add
                    (
                        dictionary
                    );
                }
            }
        }
    }
}