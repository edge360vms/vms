﻿namespace Edge360.Platform.VMS.Client.View.Test
{
    /// <summary>
    ///     Interaction logic for ThumbnailWindow.xaml
    /// </summary>
    public partial class ThumbnailWindow
    {
        public ThumbnailWindow()
        {
            InitializeComponent();
            Model.View = this;
        }

        public ThumbnailWindowViewModel Model => DataContext as ThumbnailWindowViewModel;
    }
}