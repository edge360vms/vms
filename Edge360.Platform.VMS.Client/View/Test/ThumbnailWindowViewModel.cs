using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Common.Enums.Video;
using Edge360.Platform.VMS.Common.Interfaces.Video;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication.Services;
using Edge360.Raven.Vms.RecordingServiceAgent.ServiceModel.Api.Thumbs;
using Prism.Commands;

namespace Edge360.Platform.VMS.Client.View.Test
{
    public class ThumbnailWindowViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<ThumbnailItemModel> _itemsCollection =
            new ObservableCollection<ThumbnailItemModel>();

        private DelegateCommand<object> _jumpToTime;
        private ListThumbnailsResponse _listThumbnailsResponse;
        private CameraItemModel _videoObject;
        private ThumbnailWindow _view;

        public ObservableCollection<ThumbnailItemModel> ItemsCollection
        {
            get => _itemsCollection;
            set
            {
                if (Equals(value, _itemsCollection))
                {
                    return;
                }

                _itemsCollection = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand<object> JumpToTime => _jumpToTime ??= new DelegateCommand<object>(Jump);

        public ListThumbnailsResponse ListThumbnailsResponse
        {
            get => _listThumbnailsResponse;
            set
            {
                if (Equals(value, _listThumbnailsResponse))
                {
                    return;
                }

                _listThumbnailsResponse = value;
                ItemsCollection.Clear();
                foreach (var r in _listThumbnailsResponse.Thumbnails)
                {
                    using (var ms = new MemoryStream(r.ImageBytes))
                    {
                        var bmp = Image.FromStream(ms) as Bitmap;
                        ItemsCollection.Add(new ThumbnailItemModel
                        {
                            Thumbnail = ImageUtil.BitmapToSource(bmp),
                            DateTime = r.ThumbnailDateTime.ToLocalTime(),
                        });
                    }
                }

                OnPropertyChanged();
            }
        }

        //public CameraItemModel VideoObject
        //{
        //    get => _videoObject;
        //    set
        //    {
        //        if (Equals(value, _videoObject))
        //        {
        //            return;
        //        }

        //        _videoObject = value;
        //        View?.EdgeVideoControl?.Model?.PlayLive();
        //        OnPropertyChanged();
        //    }
        //}

        public ThumbnailWindow View
        {
            get => _view;
            set
            {
                if (Equals(value, _view))
                {
                    return;
                }

                _view = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Jump(object obj)
        {
            if (obj is DateTimeOffset dateTime)
            {
                View.EdgeVideoControl.JumpToDate(TimeUtil.FixServiceStackTime(dateTime.DateTime));
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ThumbnailItemModel : INotifyPropertyChanged,ITimelineEvent
    {
        private bool _isHovered;
        private bool _isOpen;
        public DateTimeOffset DateTime { get; set; }

        public bool IsHovered
        {
            get => _isHovered;
            set
            {
                if (value == _isHovered)
                {
                    return;
                }

                _isHovered = value;
                OnPropertyChanged();
            }
        }

        public bool IsOpen
        {
            get => _isOpen;
            set
            {
                if (value == _isOpen)
                {
                    return;
                }

                _isOpen = value;
                OnPropertyChanged();
            }
        }

        public BitmapSource Thumbnail { get; set; }

        public Action<DateTime?> ThumbnailSelected;

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Description { get; set; }
        public TimeSpan Duration { get; set; } = TimeSpan.FromSeconds(5);
        public DateTime EventTime
        {
            get
            {
                return (DateTime) TimeUtil.FixServiceStackTime(DateTime.DateTime);
            }
            set { }
        }

        public ETimelineEventType EventType { get; set; }
        public string Name { get; set; }
        public Guid ObjectId { get; set; }
        public TimeZoneInfo TimeZone { get; set; }
        public Visual GetVisual(Rect constraint)
        {
            var visual = new DrawingVisual();
            var dc = visual.RenderOpen();

            var rect = constraint;
            rect.Height = 10; //constraint.Height * 15 / 100;
            rect.Width = IsHovered ? 4 : 2;
            rect.Y = 0; /// constraint.Height - rect.Height;

            rect.X -= rect.Width / 2;
            var color = IsHovered ? Colors.White : Colors.LightSkyBlue;
            color.A = (byte) (IsHovered ? 255 : 192);
            dc.DrawRectangle(new SolidColorBrush(color), null, rect);
            dc.Close();

            return visual;
        }

    }
}