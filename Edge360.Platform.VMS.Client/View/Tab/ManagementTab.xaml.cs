﻿namespace Edge360.Platform.VMS.Client.View.Tab
{
    /// <summary>
    ///     Interaction logic for MonitoringTab.xaml
    /// </summary>
    public partial class ManagementTab
    {
        public ManagementTab()
        {
            InitializeComponent();
        }

        public override bool CheckClose()
        {
            if (ManagementView?.Model?.ModalBase != null && ManagementView.Model.ModalBase.IsModalMode)
            {
                return false;
            }

            return base.CheckClose();
        }

        public override bool CloseConfirm()
        {
            if (ManagementView?.Model?.ModalBase != null && ManagementView.Model.ModalBase.IsModalMode)
            {
                FocusTab(this);
                return false;
            }

            return base.CloseConfirm();
        }
    }
}