﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Interop;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Common.Taskbar;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.VMS.Client.ImageEditor.ImageEditor;
using MahApps.Metro.Controls.Dialogs;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Tab
{
    /// <summary>
    ///     Interaction logic for MonitoringTab.xaml
    /// </summary>
    public partial class ImageEditorTab : INotifyPropertyChanged, IDisposable
    {
        private DialogCoordinator _dialogCoordinator;
        private bool _dirty;


        public bool Dirty
        {
            get => _dirty;
            set
            {
                if (_dirty != value)
                {
                    _dirty = value;
                    Header = $"Image Editor{(_dirty ? "*" : "")}";
                    OnPropertyChanged();
                }
            }
        }

        public override void Dispose()
        {
            try
            {
                ImageEditor?.Dispose();
            }
            catch (Exception e)
            {
                
            }
            
            base.Dispose();
        }

        public bool RedoStates => ImageEditor.EditorElement.CommandElement.RedoButton.Enabled;

        public bool UndoStates => ImageEditor.EditorElement.CommandElement.UndoButton.Enabled;

        public ImageEditorTab()
        {
            CustomRadImageEditor.HIDE_SAVE_BUTTON = false;
            InitializeComponent();
            ImageEditor.EditorElement.ImageChanged += ImageChanged;
            ImageEditor.EditorElement.ImageSaved += EditorElementOnImageSaved;
            //   DataContext = new ImageEditorViewModel();
        }

        public ImageEditorTabViewModel Model => DataContext as ImageEditorTabViewModel;

        private void ImageChanged()
        {
            Dirty = true;
        }

        public bool SaveDialog()
        {
            bool? result = false;

            var dialogParams = new DialogParameters
            {
                
                Content = "Are you sure you wish to close this Image Editor?\nYou will lose your changes.", OkButtonContent = "Save",
                CancelButtonContent = "Don't Save"
            };
            dialogParams.Opened += (sender, args) =>
            {
                if (sender is RadWindow window)
                {
                    window.IsVisibleChanged += (o, eventArgs) =>
                    {
                        window.SetTaskbarVisibility(true, "ImageEditor Unsaved Changes", false);
                        try
                        {
                            TaskbarUtil.SetLaunchTarget(
                                new WindowInteropHelper(window.GetParentWindow<System.Windows.Window>()).Handle);
                        }
                        catch (Exception e)
                        {
                        }

                        // window.SetTaskbarVisibility(true, caption ?? "", false);
                    };
                }
            };
            dialogParams.Closed += (sender, args) => result = args.DialogResult;

            RadWindow.Confirm(dialogParams);
            return result != null && result.Value;
        }

        

        public override bool CloseConfirm()
        {
            var parent = this.ParentOfType<RadTabbedWindow>();
            if (parent != null)
            {
                parent.SelectedItem = this;
                if (!SaveDialog())
                {
                    Dispose();
                    return true;
                }
                else
                {
                    ImageEditor.SaveImage();
                    Dispose();
                    return true;
                }
                //var resp = Model.DialogCoordinator.ShowModalMessageExternal(Model, "Unsaved Image Changes",
                //    "Are you sure you wish to close this Image Editor?\nYou will lose your changes.",
                //    MessageDialogStyle.AffirmativeAndNegativeAndSingleAuxiliary, new MetroDialogSettings
                //    {
                //        AffirmativeButtonText = "Save",
                //        AnimateHide = true,
                //        AnimateShow = true,
                //        ColorScheme = MetroDialogColorScheme.Theme,
                //        DefaultButtonFocus = MessageDialogResult.FirstAuxiliary,
                //        DefaultText = "Save",
                //        DialogButtonFontSize = 0,
                //        DialogMessageFontSize = 0,
                //        //DialogResultOnCancel = null,
                //        DialogTitleFontSize = 0,
                //        FirstAuxiliaryButtonText = "Cancel",
                //        NegativeButtonText = "Don't Save",
                //        //SecondAuxiliaryButtonText = null
                //    });
                //if (resp == MessageDialogResult.Affirmative)
                //{
                //    ImageEditor.EditorElement.SaveImageAs();
                //    return false;
                //}
                //if (resp == MessageDialogResult.Negative)
                //{
                //    Dispose();
                //    return true;
                //}
                //if (resp == MessageDialogResult.FirstAuxiliary)
                //{
                //    return false;
                //}

                //var dialog = DialogUtil.EmbeddedModalConfirmDialog(DialogOverlayGrid.ModalContentPresenter,
                //    new EmbeddedDialogParameters
                //    {
                //        Description = "Are you sure you wish to close this Image Editor?\nYou will lose your changes.",
                //        DialogButtons = EDialogButtons.SaveDontSaveCancel,
                //    });
                //if (dialog.Result)
                //{
                //    Dispose();
                //    return true;
                //  ////  var action = dialog.Result.Model.DialogAction;
                //  //  switch (action)
                //  //  {
                //  //      case EDialogAction.Affirmative:
                //  //          return true;
                //  //          break;
                //  //      case EDialogAction.Negative:
                //  //          Dispose();
                //  //          break;
                //  //      case EDialogAction.Cancel:
                //  //          return false;
                //  //  }
                //}
            }

            return false;
            //return base.CloseConfirm();
        }

        public override bool CheckClose()
        {
            if (Dirty)
            {
                return false;
            }

            return true;
        }

        private void EditorElementOnImageSaved(object sender, EventArgs e)
        {
            Dirty = false;
        }
    }

    public class ImageEditorTabViewModel : INotifyPropertyChanged
    {
        private DialogCoordinator _dialogCoordinator;

        public ImageEditorTabViewModel(DialogCoordinator dialogCoordinatorCoordinator = null)
        {
            DialogCoordinator = dialogCoordinatorCoordinator;
        }

        public DialogCoordinator DialogCoordinator
        {
            get => _dialogCoordinator;
            set
            {
                if (Equals(value, _dialogCoordinator)) return;
                _dialogCoordinator = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}