using System.Windows;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Common.Extensions.Commands;

namespace Edge360.Platform.VMS.Client.View.Tab
{
    /// <summary>
    ///     Interaction logic for MonitoringTab.xaml
    /// </summary>
    public partial class MonitoringTab
    {
        public MonitoringTab()
        {
            InitializeComponent();
            Model.View = this;
            // ObjectResolver.Register<MonitoringTab>(this);
        }

        public MonitoringTabViewModel Model => DataContext as MonitoringTabViewModel;

        public override bool CheckClose()
        {
            return base.CheckClose();
        }

        public override bool CloseConfirm()
        {
            return base.CloseConfirm();
        }
    }

    public class MonitoringTabViewModel
    {
        private RelayCommand _toggleRightPaneCommand;
        private RelayCommand _toggleLeftPaneCommand;
        private MonitoringTab _view;


        private void ToggleLeftPane()
        {
            View.MonitoringView.Model.ToggleLeftPane();
        }

        private bool CanToggleLeftPane()
        {
            return  View.MonitoringView.Model.CanToggleLeftPane();
        }


        public MonitoringTab View
        {
            get => _view;
            set => _view = value;
        }

        private void ToggleRightPane()
        {
            View.MonitoringView.Model.ToggleRightPane();
        }

        private bool CanToggleRightPane()
        {
            return View.MonitoringView.Model.CanToggleRightPane();
        }
    }
}