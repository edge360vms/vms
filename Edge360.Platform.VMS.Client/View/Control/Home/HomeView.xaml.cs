﻿using System.Windows.Controls;
using log4net;

namespace Edge360.Platform.VMS.Client.View.Control.Home
{
    /// <summary>
    ///     Interaction logic for AlertView.xaml
    /// </summary>
    public partial class HomeView : UserControl, IView
    {
        public HomeView()
        {
            InitializeComponent();
            Model.View = this;

        }
        public HomeViewModel Model => DataContext as HomeViewModel;
    }

    public class HomeViewModel
    {
        private ILog _log;

        public HomeViewModel(ILog log = null)
        {
            _log = log;
        }

        public HomeView View { get; set; }
    }
}