﻿namespace Edge360.Platform.VMS.Client.View.Control.Analytic.DataGrid
{
    /// <summary>
    ///     Interaction logic for AnalyticDataGridControl.xaml
    /// </summary>
    public partial class AnalyticDataGridControl : IView
    {
        public AnalyticDataGridControl()
        {
            InitializeComponent();
            DataContext = new AnalyticDataGridViewModel();
        }
    }
}