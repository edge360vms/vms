﻿using Edge360.Platform.VMS.Client.ViewModel.View;

namespace Edge360.Platform.VMS.Client.View.Control.Analytic
{
    /// <summary>
    ///     Interaction logic for AnalyticView.xaml
    /// </summary>
    public partial class AnalyticView : IView
    {
        public AnalyticView()
        {
            InitializeComponent();
            DataContext = new AnalyticViewModel();
        }
    }
}