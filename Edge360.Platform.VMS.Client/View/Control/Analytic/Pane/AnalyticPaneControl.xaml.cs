﻿using Edge360.Platform.VMS.Client.ViewModel.View;

namespace Edge360.Platform.VMS.Client.View.Control.Analytic.Pane
{
    /// <summary>
    ///     Interaction logic for AnalyticPaneControl.xaml
    /// </summary>
    public partial class AnalyticPaneControl : IView
    {
        public AnalyticPaneControl()
        {
            InitializeComponent();
            DataContext = new AnalyticViewModel();
        }
    }
}