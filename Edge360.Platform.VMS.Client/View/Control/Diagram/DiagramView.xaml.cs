﻿using System.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Diagram
{
    /// <summary>
    ///     Interaction logic for DiagramView.xaml
    /// </summary>
    public partial class DiagramView : UserControl, IView
    {
        public DiagramView()
        {
            InitializeComponent();
        }
    }
}