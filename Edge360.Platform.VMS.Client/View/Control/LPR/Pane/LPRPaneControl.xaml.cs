﻿using System.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.LPR.Pane
{
    /// <summary>
    ///     Interaction logic for LPRPaneControl.xaml
    /// </summary>
    public partial class LPRPaneControl : UserControl, IView
    {
        public LPRPaneControl()
        {
            InitializeComponent();
            DataContext = new LPRPaneViewModel();
        }
    }
}