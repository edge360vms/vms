﻿using System.Windows.Controls;
using Edge360.Platform.VMS.Client.ViewModel.View;

namespace Edge360.Platform.VMS.Client.View.Control.LPR
{
    /// <summary>
    ///     Interaction logic for LPRView.xaml
    /// </summary>
    public partial class LPRView : UserControl, IView
    {
        public LPRView()
        {
            InitializeComponent();
            DataContext = new LPRViewModel();
        }
    }
}