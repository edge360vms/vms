﻿using System.Windows.Controls;
using Edge360.Platform.VMS.Client.View.Control.Alert.DataGrid;

namespace Edge360.Platform.VMS.Client.View.Control.LPR.DataGrid
{
    /// <summary>
    ///     Interaction logic for AlertDataGridControl.xaml
    /// </summary>
    public partial class AlertDataGridControl : UserControl, IView
    {
        public AlertDataGridControl()
        {
            InitializeComponent();
            DataContext = new AlertDataGridViewModel();
        }
    }
}