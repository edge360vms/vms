﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Client.ViewModel.View;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Interfaces.Monitoring;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring
{
    public class MonitoringViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private bool _isHotkeyHelpVisible;
        private bool _noExistingMonitor;
        private RelayCommand _toggleLeftPaneCommand;
        private RelayCommand _toggleRightPaneCommand;
        private DirectModeManager _directModeManager;
        private MonitoringView _view;

        public bool IsHotkeyHelpVisible
        {
            get => _isHotkeyHelpVisible;
            set
            {
                if (value == _isHotkeyHelpVisible)
                {
                    return;
                }

                _isHotkeyHelpVisible = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand ToggleLeftPaneCommand => _toggleLeftPaneCommand ??= new RelayCommand(p => ToggleLeftPane(),
            o => true);
        public RelayCommand ToggleRightPaneCommand => _toggleRightPaneCommand ??= new RelayCommand(p => ToggleRightPane(),
            o => true);


        public bool NoExistingMonitor
        {
            get
            {
                var amount = ObjectResolver.ResolveAll<MonitoringTab>().Count;
                var val = amount <= 2;

                return val;
            }
        }

        public StatusBarInformation StatusBarInformation { get; set; } = new StatusBarInformation();


        public MonitoringView View
        {
            get => _view;
            set
            {
                if (Equals(value, _view)) return;
                _view = value;
                if (_view != null)
                {
                    _view.IsVisibleChanged += ViewOnIsVisibleChanged;
                }
                OnPropertyChanged();
            }
        }

        public MonitoringViewModel(DirectModeManager directModeManager = null)
        {
            DirectModeManager = directModeManager;
           
        }

        public DirectModeManager DirectModeManager
        {
            get => _directModeManager;
            set
            {
                if (Equals(value, _directModeManager)) return;
                _directModeManager = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        public void ToggleRightPane()
        {
            View.RightExpander.IsExpanded = !View.RightExpander.IsExpanded;
        }

        public bool CanToggleRightPane()
        {
            return true;
        }

        public void ToggleLeftPane()
        {
            View.LeftExpander.IsExpanded = !View.LeftExpander.IsExpanded;
        }

        public bool CanToggleLeftPane()
        {
            return true;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ViewOnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool) e.NewValue)
            {
                foreach (var c in View.TileGridControl.DynamicGrid.Children.OfType<EdgeVideoControl>())
                {
                    if (c?.ContentControl?.Content is IAirspaceContent content)
                    {
                        try
                        {
                            content.UpdateLocation();
                            //  content.Redraw();
                        }
                        catch (Exception exception)
                        {
                        }
                    }
                }
            }
        }
    }
}