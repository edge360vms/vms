﻿using System.Windows.Controls;
using Edge360.Platform.VMS.Client.Extensions;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring
{
    /// <summary>
    ///     Interaction logic for MonitorTreeControl.xaml
    /// </summary>
    public partial class MonitorTreeView : UserControl
    {
        public MonitorTreeViewModel Model => DataContext as MonitorTreeViewModel;

        public MonitorTreeView()
        {
            InitializeComponent();
            Model.View = this;
            //DataContext = new MonitorTreeControlViewModel(this);
            //ObjectResolver.Register<MonitorTreeControlViewModel>(DataContext);
        }
    }
}