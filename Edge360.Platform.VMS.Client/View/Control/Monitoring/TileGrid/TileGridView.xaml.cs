﻿using System;
using System.Windows;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.Extensions.CommandLine;
using Edge360.Platform.VMS.Client.View.Tab;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring.TileGrid
{
    /// <summary>
    ///     Interaction logic for TileGridControl.xaml
    /// </summary>
    public partial class TileGridView : UserControl, IView
    {
        public static bool StartupLayoutBeenInitialized { get; set; }
        public TileGridViewModel Model => DataContext as TileGridViewModel;

        public TileGridView()
        {
            InitializeComponent();
            if (Model != null)
            {
                Model.TileGridView = this;
            }

            if (!CommandLineManager.StartupArgs.LoadRecoveryLayout  && (StartupLayoutBeenInitialized || CommandLineManager.StartupArgs.Layout == Guid.Empty))
            {
                if (Model != null)
                {
                  
                    Model.GridSize = new Size(3, 3);
                }
            }

            // DataContext = new TileGridViewModel(this);
            //ObjectResolver.Register<TileGridViewModel>(DataContext);
        }

        //public TileGridViewModel GetModel()
        //{
        //    return (TileGridViewModel) DataContext;
        //}
        private void TileGridView_OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
          //      Model.PruneTiles(Model.GridSize);
            }
            catch (Exception exception)
            {
                
            }
        }
    }
}