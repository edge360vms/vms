using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.Extensions.Helpers;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.DataObject;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Alert;
using Edge360.Platform.VMS.Client.View.Control.Camera;
using Edge360.Platform.VMS.Client.View.Control.Dialog;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Client.View.Control.VideoControl.Timeline;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Client.View.Window.Bookmarks;
using Edge360.Platform.VMS.Common.Enums.Bookmarks;
using Edge360.Platform.VMS.Common.Enums.Camera;
using Edge360.Platform.VMS.Common.Enums.Presets;
using Edge360.Platform.VMS.Common.Enums.Tiles;
using Edge360.Platform.VMS.Common.Enums.Video;
using Edge360.Platform.VMS.Common.Enums.Video.Playback;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Interfaces.Monitoring;
using Edge360.Platform.VMS.Common.Interfaces.Video;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Common.Taskbar;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication.Auth.Headers;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Platform.VMS.Monitoring.CameraComponents.Axis.PTZ;
using Edge360.Platform.VMS.Monitoring.Input;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.Monitoring.VideoComponents.MPV;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Nodes;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.CameraAdapter;
using Edge360.Raven.Vms.CameraService.ServiceModel.ItemModels;
using Edge360.Vms.Client.VideoExport.Common.Utility;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using Mictlanix.DotNet.Onvif.Ptz;
using Prism.Commands;
using RecordingService.ServiceModel.Api.TimelineEvents;
using ServiceStack.Text;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.FileDialogs.DragDrop;
using Telerik.Windows.Controls.TreeView;
using Telerik.Windows.DragDrop;
using WispFramework.Extensions.Tasks;
using WispFramework.Patterns.Monitors;
using static Edge360.Platform.VMS.Communication.CommunicationManager;
using static Telerik.Windows.DragDrop.DragDropManager;
using static Telerik.Windows.DragDrop.DragDropPayloadManager;
using Brushes = System.Windows.Media.Brushes;
using DelegateCommand = Prism.Commands.DelegateCommand;
using DragEventArgs = Telerik.Windows.DragDrop.DragEventArgs;
using Image = System.Windows.Controls.Image;
using Point = System.Windows.Point;
using Size = System.Windows.Size;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring.TileGrid
{
    public class TileGridViewModel : ViewModelBase, ITileGrid, INotifyPropertyChanged
    {
        private readonly ILog _log;
        private DelegateCommand _addPresetCommand;
        private RelayCommand _bookmarkControlCommand;
        private BookmarkManagerView _bookmarkWindow;

        private RelayCommand _cameraPresetControlCommand;
        private DelegateCommand<PtzPreset> _deletePresetCommand;
        private DelegateCommand _directModeCommand;
        private DirectModeManager _directModeManager;
        private DelegateCommand _editPresetCommand;
        private DelegateCommand<PtzPreset> _gotoPresetCommand;
        private Size _gridSize;
        private bool _invertYJoystick;
        private PresetItemManager _presetItemManager;
        private DelegateCommand _RefreshPresetsCommand;
        private RelayCommand _selectAllControlsCommand;
        private EdgeVideoControl _selectedControl;
        private DelegateCommand _setHomeCommand;
        private RelayCommand _snapshotCommand;
        private RelayCommand _tileLayoutCommand;
        private RelayCommand _videoControlCommand;

        public ConcurrentDictionary<EJoystickAction, CountdownOperation> ActiveOperations =
            new ConcurrentDictionary<EJoystickAction, CountdownOperation>();

        public TileGridView TileGridView;

        public DelegateCommand AddPresetCommand => _addPresetCommand ??= new DelegateCommand(() => AddPreset());

        public RelayCommand BookmarkControlCommand => _bookmarkControlCommand ??= new RelayCommand(ExecuteBookmark,
            p => CanUseVideoCommand());

        public CameraItemModelManager CameraItemMgr { get; set; }

        public RelayCommand CameraPresetControlCommand => _cameraPresetControlCommand ??= new RelayCommand(
            ExecutePreset,
            p => CanUseVideoCommand());

        public ObservableCollection<ITileComponent> Components { get; set; } =
            new ObservableCollection<ITileComponent>();

        public DelegateCommand<PtzPreset> DeletePresetCommand => _deletePresetCommand ??=
            new DelegateCommand<PtzPreset>(
                preset => DeletePreset(preset));

        public DelegateCommand DirectModeCommand => _directModeCommand ??= new DelegateCommand(DirectModeToggle);

        public DirectModeManager DirectModeManager
        {
            get => _directModeManager;
            set
            {
                if (Equals(value, _directModeManager))
                {
                    return;
                }

                _directModeManager = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand EditPresetCommand => _editPresetCommand ??= new DelegateCommand(() => AddPreset(true));

        public DelegateCommand<PtzPreset> GotoPresetCommand =>
            _gotoPresetCommand ??= new DelegateCommand<PtzPreset>(GotoPreset);

        public Size GridSize
        {
            get => _gridSize;
            set
            {
                if (_gridSize != value)
                {
                    _gridSize = value;
                    OnPropertyChanged();

                    Task.Run(() =>
                    {
                        try
                        {
                            SetGridSize(_gridSize);
                        }
                        catch (Exception e)
                        {
                        }
                    });
                }
            }
        }

        public bool InvertYJoystick
        {
            get => _invertYJoystick;
            set
            {
                if (value == _invertYJoystick)
                {
                    return;
                }

                _invertYJoystick = value;
                OnPropertyChanged();
            }
        }

        public bool IsSettingGridSize { get; set; }

        public EdgeVideoControl LastControl => SelectedComponents?.OfType<EdgeVideoControl>()?.LastOrDefault();

        public CollectionViewSource PresetCollectionViewSource { get; set; } = new CollectionViewSource();

        public PresetItemManager PresetItemManager
        {
            get => _presetItemManager;
            set
            {
                if (Equals(value, _presetItemManager))
                {
                    return;
                }

                _presetItemManager = value;
                OnPropertyChanged();
            }
        }

        public Size PreviousGridSize { get; set; }
        public DelegateCommand RefreshPresetsCommand => _RefreshPresetsCommand ??= new DelegateCommand(RefreshPresets);

        public RelayCommand SelectAllControlsCommand => _selectAllControlsCommand ??= new RelayCommand(
            p => SelectAllComponents(),
            p => CanSelectAll());

        public ObservableCollection<ITileComponent> SelectedComponents { get; set; } =
            new ObservableCollection<ITileComponent>();

        public EdgeVideoControl SelectedControl
        {
            get => _selectedControl;
            set
            {
                if (Equals(value, _selectedControl))
                {
                    return;
                }

                _selectedControl = value;
                Application.Current.Dispatcher.InvokeAsync(() => { PresetCollectionViewSource?.View?.Refresh(); });

                OnPropertyChanged();
            }
        }

        public DelegateCommand SetHomeCommand => _setHomeCommand ??= new DelegateCommand(SetHome);

        public RelayCommand SnapshotCommand => _snapshotCommand ??= new RelayCommand(Snapshot,
            p => CanSnapshotControl());

        public RelayCommand TileLayoutCommand => _tileLayoutCommand ??= new RelayCommand(ExecuteTileCommand,
            o => CanExecuteTileCommand());


        private EdgeVideoControl LastComponent
        {
            get
            {
                //   var tileGridControl = _view?.GetVisualParent<MonitoringView>()?.TileGridControl;
                var lastControl = SelectedComponents.LastOrDefault() as EdgeVideoControl;
                if (lastControl == null)
                {
                    SelectedComponents?.Add(Components
                        .OfType<EdgeVideoControl>()
                        .FirstOrDefault(o =>
                            o != null && (o.IsFullscreen || o.TileColumn == 0 && o.TileRow == 0)));
                }

                return lastControl;
            }
        }

        public TileGridViewModel(ILog log = null, CameraItemModelManager cameraItemMgr = null,
            DirectModeManager directModeManager = null,
            PresetItemManager presetItemManager = null)
        {
            _log = log;
            if (directModeManager != null)
            {
                directModeManager.DirectModeChanged += OnDirectModeChanged;
                DirectModeManager = directModeManager;
            }

            CameraItemMgr = cameraItemMgr;
            // TileGridView = tileGridView;
            DirectInputManager.RequestedInput += OnRequestedInput;
            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;
            PresetItemManager = presetItemManager;
            if (PresetItemManager != null)
            {
                PresetCollectionViewSource.Source = PresetItemManager.ItemCollection;
            }

            PresetCollectionViewSource.Filter += CollectionViewSourceOnFilter;
            SelectedComponents.CollectionChanged += SelectedComponentsOnCollectionChanged;
        }

        public event PropertyChangedEventHandler PropertyChanged;


        public RelayCommand VideoControlCommand => _videoControlCommand ??= new RelayCommand(ExecuteVideoCommand,
            p => CanUseVideoCommand());

        private async void SetHome()
        {
            try
            {
                if (SelectedControl != null)
                {
                    var dialog = DialogUtil.ConfirmDialog(
                        "Are you sure you want to set the view to the current Home position?", true);
                    if (dialog)
                    {
                        if (DirectModeManager.IsDirectMode)
                        {
                            var onvifCameraConnectionItemModel =
                                ((ONVIFManager) SelectedControl.VideoComponent.PtzManager)?.OnvifCameraConnection;
                            if (onvifCameraConnectionItemModel?.PtzClient != null)
                            {
                                await onvifCameraConnectionItemModel.PtzClient.SetHomePositionAsync(
                                    onvifCameraConnectionItemModel.Token);
                            }
                        }
                        else
                        {
                            var resp = await GetManagerByZoneId(SelectedControl.VideoObject.ZoneId)
                                .CameraAdapterService
                                .SetHomePtzPreset(
                                    new SetHomePtzPreset {CameraId = SelectedControl.VideoObject.Id});
                            //if (resp != null && resp.Success)
                            //{
                            //    RefreshPresets();
                            //}
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private void DirectModeToggle()
        {
            if (DirectModeManager.IsDirectMode)
            {
                if (DirectModeManager.IsOfflineMode)
                {
                    var dialog = DialogUtil.ConfirmDialog(
                        "You are currently in 'Offline Mode'?\nDo you wish to login?", true);
                    if (dialog)
                    {
                        var tabbedWindow = ObjectResolver.Resolve<Window.TabbedWindow>();
                        tabbedWindow.SkipLogOutPrompt = true;
                        tabbedWindow?.Model?.LogOut(true);
                        DirectModeManager.IsOfflineMode = false;
                    }
                    else
                    {
                        return;
                    }
                }

                DirectModeManager.IsDirectMode = false;
            }
            else
            {
                var dialog = DialogUtil.ConfirmDialog(
                    "Are you sure you wish to enable 'Direct-Mode'?\nThis will force direct communication to devices and run in an offline mode.",
                    true);
                if (dialog)
                {
                    DirectModeManager.IsDirectMode = true;
                }
            }
        }

        private async void OnRequestedInput(int button, int value)
        {
            try
            {
                var action = (EJoystickAction) button;
                var item = ActiveOperations.TryGetValue(action, out var countdownOperation);
                if (countdownOperation == null)
                {
                    countdownOperation = new CountdownOperation().WithDelay(TimeSpan.FromMilliseconds(300))
                        .WithStartCallback(async () => { await HandleJoystickAction(value, action); })
                        .WithCompletedCallback(async () =>
                        {
                            await HandleJoystickAction(0, action);
                            ActiveOperations.TryRemove(action, out _);
                        }).Begin();
                    ActiveOperations[action] = countdownOperation;
                }
                else
                {
                    countdownOperation.WithStartCallback(
                        async () => { await HandleJoystickAction(value, action); });
                    // countdownOperation.Restart();
                    //countdownOperation.Reset();
                }
            }
            catch (Exception e)
            {
            }
        }

        private async Task HandleJoystickAction(int value, EJoystickAction button)
        {
            await Application.Current.Dispatcher.InvokeAsync(async () =>
            {
                try
                {
                    var lastControl = LastControl;
                    if (lastControl == null)
                    {
                        SelectedComponents?.Add(Components.OfType<EdgeVideoControl>()
                            .FirstOrDefault(o =>
                                o != null && (o.IsFullscreen || o.TileColumn == 0 && o.TileRow == 0)));
                    }

                    if (lastControl != null && lastControl?.VideoComponent?.PtzManager != null)
                    {
                        var directionalValue = value;

                        if (Math.Abs(value) < 2)
                        {
                            directionalValue = 0;
                        }

                        var inputType = button;
                        switch (inputType)
                        {
                            case EJoystickAction.X:
                                await lastControl.VideoComponent.PtzManager.PanCamera(new Point(directionalValue,
                                    InvertYJoystick
                                        ? -DirectInputManager.LastKnownY
                                        : DirectInputManager.LastKnownY));
                                break;
                            case EJoystickAction.Y:
                                await lastControl.VideoComponent.PtzManager.PanCamera(new Point(
                                    DirectInputManager.LastKnownX,
                                    InvertYJoystick ? -directionalValue : directionalValue));
                                break;
                            case EJoystickAction.Z:
                                _log?.Info($"DirectionalValue {directionalValue}");
                                await lastControl.VideoComponent.PtzManager.Zoom(directionalValue < 0,
                                    new Point(0, 0),
                                    Math.Abs(directionalValue) / 100f);
                                break;
                            case EJoystickAction.Buttons0:
                                if (value > 0)
                                {
                                    await lastControl.VideoComponent?.PtzManager.SetPreset(0);
                                }

                                break;
                            case EJoystickAction.Buttons1:
                                if (value > 0)
                                {
                                    await lastControl.VideoComponent?.PtzManager.SetPreset(EPtzPreset.Preset1);
                                }

                                break;
                            case EJoystickAction.Buttons2:
                                if (value > 0)
                                {
                                    await lastControl.VideoComponent?.PtzManager.SetPreset(EPtzPreset.Preset2);
                                }

                                break;
                            case EJoystickAction.Buttons3:
                                if (value > 0)
                                {
                                    await lastControl.VideoComponent?.PtzManager.SetPreset(EPtzPreset.Preset3);
                                }

                                break;
                            case EJoystickAction.Buttons4:
                                if (value > 0)
                                {
                                    await lastControl.VideoComponent?.PtzManager.SetPreset(EPtzPreset.Preset4);
                                }

                                break;
                            case EJoystickAction.Buttons5:
                                if (value > 0)
                                {
                                    InvertYJoystick = !InvertYJoystick;
                                }

                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                }
            });
        }

        private void ExecuteTileCommand(object obj)
        {
            if (obj is ETileCommand tileCommand)
            {
                var videoControl = SelectedComponents?.LastOrDefault() as EdgeVideoControl;
                switch (tileCommand)
                {
                    case ETileCommand.Fullscreen:
                        if (videoControl != null)
                        {
                            ToggleFullscreenTile(videoControl);
                        }

                        break;
                    case ETileCommand.Layout1x1:
                        GridSize = new Size(1, 1);
                        break;
                    case ETileCommand.Layout2x2:
                        GridSize = new Size(2, 2);
                        break;
                    case ETileCommand.Layout3x3:
                        GridSize = new Size(3, 3);
                        break;
                    case ETileCommand.Layout4x4:
                        GridSize = new Size(4, 4);
                        break;
                    case ETileCommand.SearchAnalytics:
                        break;
                    case ETileCommand.ManageBookmarks:
                        break;
                    case ETileCommand.ToggleThumbnails:

                        break;
                    case ETileCommand.ToggleTimeline:

                        ToggleTimeline(videoControl);

                        break;
                    case ETileCommand.CancelPtz:
                        videoControl?.VideoComponent?.PtzManager.StopPtz();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private static void ToggleTimeline(EdgeVideoControl videoControl)
        {
            if (videoControl?.VideoComponent.PlayerState != EPlayerState.Stopped &&
                videoControl?.VideoComponent.PlayerState != EPlayerState.None)
            {
                if (videoControl != null)
                {
                    videoControl.Model.ControlOverlaySettings ^= EControlOverlaySettings.Timeline;
                }
            }
        }

        private bool CanExecuteTileCommand()
        {
            return true;
        }

        private void ExecutePreset(object obj)
        {
            if (obj is EPresetCommand preset)
            {
                var videoControl = SelectedComponents
                        ?.LastOrDefault() as
                    EdgeVideoControl;
                if (videoControl?.ContentControl?.Content is IVideoComponent videoComponent)
                {
                    videoComponent.PtzManager?.SetPreset((EPtzPreset) preset);
                }
            }
        }

        private async void RefreshPresets()
        {
            try
            {
                var lastComponentVideoObject = LastComponent?.VideoObject;
                var previousSelected = lastComponentVideoObject?.SelectedPreset;
                if (lastComponentVideoObject != null)
                {
                    await lastComponentVideoObject.PresetItemManager.RefreshPresets(lastComponentVideoObject.ZoneId,
                        new ItemManagerRequestParameters {AcceptedCacheDuration = TimeSpan.Zero},
                        lastComponentVideoObject.Id);
                    if (previousSelected != null)
                    {
                        lastComponentVideoObject.SelectedPreset =
                            lastComponentVideoObject.PresetItemManager.ItemCollection.FirstOrDefault(o =>
                                o.Value.Label == previousSelected.Label && o.Value.Token == previousSelected.Token &&
                                o.Value.Pan == previousSelected.Pan && o.Value.Tilt == previousSelected.Tilt &&
                                o.Value.Zoom == previousSelected.Zoom).Value;
                    }
                }
            }
            catch (Exception e)
            {
            }
        }


        private async void AddPreset(bool isEdit = false)
        {
            try
            {
                if (isEdit && SelectedControl.VideoObject.SelectedPreset == null)
                {
                    return;
                }

                var dialog =
                    DialogUtil.InputDialog(
                        $"Enter a name for the preset.{(isEdit ? $"\nPrevious name {SelectedControl.VideoObject.SelectedPreset.Label}" : "")}");
                if (dialog.Result && !string.IsNullOrEmpty(dialog.Response))
                {
                    dialog.Response = dialog.Response.Replace(" ", "_");
                    if (SelectedControl != null)
                    {
                        if (DirectModeManager.IsDirectMode)
                        {
                            var onvifCameraConnectionItemModel =
                                ((ONVIFManager) SelectedControl.VideoComponent.PtzManager)?.OnvifCameraConnection;
                            if (onvifCameraConnectionItemModel?.PtzClient != null)
                            {
                                var resp = await onvifCameraConnectionItemModel.PtzClient.SetPresetAsync(
                                    new SetPresetRequest(onvifCameraConnectionItemModel?.Token, dialog?.Response,
                                        isEdit ? SelectedControl.VideoObject.SelectedPreset.Token : null));
                                if (resp != null && resp.PresetToken != null)
                                {
                                    RefreshPresets();
                                }
                            }
                        }
                        else
                        {
                            if (isEdit)
                            {
                                var resp = await GetManagerByZoneId(SelectedControl.VideoObject.ZoneId)
                                    .CameraAdapterService
                                    .UpdatePtzPreset(SelectedControl.VideoObject.Id,
                                        SelectedControl.VideoObject.SelectedPreset.Token, dialog.Response);
                                if (resp != null && resp.Success)
                                {
                                    RefreshPresets();
                                }
                            }
                            else
                            {
                                var resp = await GetManagerByZoneId(SelectedControl.VideoObject.ZoneId)
                                    .CameraAdapterService
                                    .AddPtzPreset(SelectedControl.VideoObject.Id, dialog.Response);
                                if (resp != null && resp.Success)
                                {
                                    RefreshPresets();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private async Task DeletePreset(PtzPreset preset, bool ignorePrompt = false)
        {
            try
            {
                if (preset != null && (ignorePrompt ||
                                       DialogUtil.ConfirmDialog(
                                           $"Are you sure you wish to delete this preset?\n'{preset.Label}'", true)))
                {
                    if (SelectedControl != null)
                    {
                        if (DirectModeManager.IsDirectMode)
                        {
                            var onvifCameraConnectionItemModel =
                                ((ONVIFManager) SelectedControl.VideoComponent.PtzManager)?.OnvifCameraConnection;
                            if (onvifCameraConnectionItemModel?.PtzClient != null)
                            {
                                await onvifCameraConnectionItemModel.PtzClient.RemovePresetAsync(
                                    onvifCameraConnectionItemModel.Token,
                                    SelectedControl.VideoObject.SelectedPreset.Token);
                                RefreshPresets();
                            }
                        }
                        else
                        {
                            var resp = await GetManagerByZoneId(SelectedControl.VideoObject.ZoneId).CameraAdapterService
                                .RemovePtzPreset(SelectedControl.VideoObject.Id, preset.Token);
                            if (resp != null && resp.Success)
                            {
                                RefreshPresets();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private async void GotoPreset(PtzPreset preset)
        {
            try
            {
                if (DirectModeManager.IsDirectMode)
                {
                    if (SelectedControl?.VideoObject != null)
                    {
                        if (preset != null)
                        {
                            if (SelectedControl.VideoComponent.PtzManager is ONVIFManager onvif)
                            {
                                await onvif.OnvifCameraConnection.PtzClient.GotoPresetAsync(
                                    onvif.OnvifCameraConnection.Token,
                                    preset.Token
                                    , ONVIFManager.CreateSpeed(100, 100));
                            }
                        }
                    }
                }
                else if (SelectedControl?.VideoObject != null)
                {
                    if (preset != null)
                    {
                        var resp = await GetManagerByZoneId(SelectedControl.VideoObject.ZoneId).CameraAdapterService
                            .GoToPtzPreset(SelectedControl.VideoObject.Id, preset.Token, null, 100f, 100f, 100f);
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private async void ExecuteBookmark(object o)
        {
            if (o is EBookmarkCommand command)
            {
                var modelSpawnControl = LastControl ?? LastComponent;
                var camera = modelSpawnControl?.VideoObject;
                if (camera != null && ServiceManagers.ContainsKey(camera.ZoneId))
                {
                    var serviceManager = GetManagerByZoneId(camera.ZoneId);
                    if (serviceManager == null)
                    {
                        return;
                    }

                    switch (command)
                    {
                        case EBookmarkCommand.None:
                            break;
                        case EBookmarkCommand.BookmarkManager:
                            try
                            {
                                Application.Current?.Dispatcher?.InvokeAsync(() =>
                                {
                                    _bookmarkWindow ??= new BookmarkManagerView();
                                    if (_bookmarkWindow.Model != null)
                                    {
                                        _bookmarkWindow.Model.SelectedCamera = camera;
                                        _bookmarkWindow.Model.SpawnControl = modelSpawnControl;
                                    }

                                    _bookmarkWindow?.SetTaskbarVisibleOnLoaded();

                                    _bookmarkWindow?.Show();
                                    TaskbarUtil.SetLaunchTarget(
                                        new WindowInteropHelper(
                                            _bookmarkWindow.GetParentWindow<System.Windows.Window>()).Handle);
                                    _bookmarkWindow.GetParentWindow<System.Windows.Window>().GlobalActivate();
                                });
                            }
                            catch (Exception e)
                            {
                            }

                            break;
                        case EBookmarkCommand.AddBookmark:
                            try
                            {
                                //TODO: This won't work globally
                                var dateTime = modelSpawnControl.EdgeVideoTimeline.CursorTime;
                                var dialog = DialogUtil.InputDialog(
                                    $"Please enter a description of this new Bookmark.\n{camera.Label} at {dateTime:u}");
                                if (dialog.Result)
                                {
                                    var resp = await serviceManager?.TimelineEventService.CreateTimelineEvent(
                                        new CreateTimelineEvent
                                        {
                                            CameraId = camera.Id,
                                            Description = dialog.Response,
                                            Duration = 1,
                                            EventType = "Bookmark",
                                            Name = $"{camera.Label} Bookmark",
                                            TimestampUtc = dateTime
                                        });
                                    if (resp != null)
                                    {
                                        //Application.Current?.Dispatcher?.InvokeAsync(() =>
                                        //{
                                        //    //var diag = DialogUtil.ConfirmDialog(
                                        //    //    $"{resp.TimelineEventId} {resp.ResponseStatus}");
                                        //});
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                            }

                            break;
                        case EBookmarkCommand.PreviousBookmark:

                            modelSpawnControl?.GotoPreviousBookmark();
                            break;
                        case EBookmarkCommand.NextBookmark:
                            modelSpawnControl?.GotoNextBookmark();
                            break;
                    }
                }
            }
        }

        private bool CanSnapshotControl()
        {
            return true;
        }

        private async void Snapshot(object obj)
        {
            if (SelectedComponents.LastOrDefault() is EdgeVideoControl videoControl)
            {
                SnapshotControl(videoControl);
            }
        }

        private async void SnapshotControl(EdgeVideoControl videoControl)
        {
            try
            {
                if (videoControl?.ContentControl?.Content is MPVVideoComponent component)
                {
                    var snap = videoControl.VideoComponent.IsLivePlayback
                        ? await OnvifSnapshot(videoControl) ?? component.VideoView?.WindowsFormsHost?.Child.Screenshot()
                        : component.VideoView?.WindowsFormsHost?.Child.Screenshot(); //.SnapshotRenderTargetBitmapPng();

                    if (snap != null)
                    {
                        var exportingOutputDirectory = SettingsManager.Settings.Exporting.OutputDirectory ??
                                                       ExportSettings.DefaultExportPath;
                        if (!Directory.Exists(exportingOutputDirectory))
                        {
                            Directory.CreateDirectory(exportingOutputDirectory);
                        }

                        var savePath = Path.Combine(exportingOutputDirectory,
                            $"{DateTime.UtcNow:yyyy-MM-dd_HH-mm-ss}.jpg");
                        try
                        {
                            snap.Save($"{savePath}");
                        }
                        catch (Exception a)
                        {
                        }

                        var loginView = ObjectResolver.Resolve<LoginViewModel>();
                        var snapshotAlertItemView = new SnapshotAlertItemView();
                        snapshotAlertItemView.Model.ImageSource = ImageUtil.BitmapToSource(snap);
                        snapshotAlertItemView.Model.Timestamp = DateTimeOffset.UtcNow;
                        snapshotAlertItemView.Model.Description = "Test";
                        snapshotAlertItemView.Model.FilePath = savePath;
                        snapshotAlertItemView.Model.ImageClicked += () =>
                        {
                            Application.Current.Dispatcher.InvokeAsync(async () =>
                            {
                                try
                                {
                                    var tabbedWindow = TileGridView?.ParentOfType<RadTabbedWindow>();
                                    if (tabbedWindow != null)
                                    {
                                        var tab = await tabbedWindow.OpenTab<ImageEditorTab>();
                                        if (tab != null)
                                        {
                                            tab.Dirty = true;
                                            tab.ImageEditor.OpenImage(snap);
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                }
                            });
                        };
                        loginView.TryUiAsync(() =>
                        {
                            loginView.AlertManager.ShowAlert(snapshotAlertItemView);
                        });
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private async Task<Bitmap> OnvifSnapshot(EdgeVideoControl videoControl)
        {
            try
            {
                byte[] bytes = null;
                if (!DirectModeManager.IsDirectMode)
                {
                    var apiSnapshot = await GetManagerByZoneId(videoControl.VideoObject.ZoneId).CameraAdapterService
                        .GetSnapshot(videoControl.VideoObject.Id);

                    if (apiSnapshot?.Result != null && apiSnapshot.Result.Length > 0)
                    {
                        var onvifSnapshot = new Bitmap(new MemoryStream(apiSnapshot.Result));
                        if (onvifSnapshot != null)
                        {
                            return onvifSnapshot;
                        }
                    }
                }

                var onvifCameraConnectionItemModel =
                    ((ONVIFManager) videoControl?.VideoComponent?.PtzManager)?.OnvifCameraConnection;
                if (onvifCameraConnectionItemModel?.MediaClient != null)
                {
                    var snapshot = await onvifCameraConnectionItemModel.MediaClient
                        .GetSnapshotUriAsync(
                            videoControl.VideoObject
                                .CurrentOnvifProfile.token ?? onvifCameraConnectionItemModel.Token);
                    var host =
                        $"http://{videoControl.VideoObject.Address}"; // new Uri(uri.GetLeftPart(UriPartial.Authority)).ToString();
                    var digest = new DigestAuthFixer(host,
                        videoControl.VideoObject.Username,
                        videoControl.VideoObject.Password);
                    var strReturn = digest.GrabImage(snapshot.Uri.Replace(host, ""));
                    return strReturn;
                }

                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private void ExecuteVideoCommand(object obj)
        {
            if (obj is EVideoPlayerCommand command)
            {
                var lastControl = LastControl;
                if (lastControl == null)
                {
                    SelectedComponents?.Add(Components.OfType<EdgeVideoControl>()
                        .FirstOrDefault(o => o != null && (o.IsFullscreen || o.TileColumn == 0 && o.TileRow == 0)));
                }


                switch (command)
                {
                    case EVideoPlayerCommand.None:
                        break;
                    case EVideoPlayerCommand.Stop:
                        if (SelectedComponents != null)
                        {
                            foreach (var c in SelectedComponents)
                            {
                                if (c is EdgeVideoControl videoControl)
                                {
                                    videoControl?.Model?.Stop();
                                }
                            }
                        }

                        break;
                    case EVideoPlayerCommand.Play:
                        if (SelectedComponents != null)
                        {
                            foreach (var c in SelectedComponents.OfType<EdgeVideoControl>())
                            {
                                if (c.VideoComponent.IsLivePlayback)
                                {
                                    c.Model?.PlayLive();
                                }
                                else
                                {
                                    c.Model?.Play();
                                }
                            }
                        }

                        break;
                    case EVideoPlayerCommand.Reverse:
                        lastControl?.SetPlayback(EPlaybackState.Reverse);
                        break;
                    case EVideoPlayerCommand.Forward:
                        lastControl?.SetPlayback(EPlaybackState.Forward);
                        break;
                    case EVideoPlayerCommand.Pause:
                        if (SelectedComponents != null)
                        {
                            foreach (var c in SelectedComponents)
                            {
                                if (c is EdgeVideoControl videoControl)
                                {
                                    videoControl.Model?.Pause();
                                }
                            }
                        }

                        break;
                    case EVideoPlayerCommand.PlayPause:
                        if (lastControl != null)
                        {
                            if (lastControl.IsPlaying)
                            {
                                lastControl.Model?.Pause();
                            }
                            else
                            {
                                if (lastControl.VideoComponent.IsLivePlayback)
                                {
                                    lastControl.Model?.PlayLive();
                                }
                                else
                                {
                                    lastControl.Model?.Play();
                                }
                            }
                        }

                        break;
                    case EVideoPlayerCommand.TimelineBack:
                        lastControl?.EdgeVideoTimeline.OnButtonScrollLeft(null, null);
                        break;
                    case EVideoPlayerCommand.TimelineForward:
                        lastControl?.EdgeVideoTimeline.OnButtonScrollRight(null, null);
                        break;
                    case EVideoPlayerCommand.SkipBackward:
                        lastControl?.JumpToDate(lastControl.EdgeVideoTimeline.CursorTime - TimeSpan.FromSeconds(5),
                            lastControl.IsPlaying);
                        break;
                    case EVideoPlayerCommand.SkipForward:
                        lastControl?.JumpToDate(lastControl.EdgeVideoTimeline.CursorTime + TimeSpan.FromSeconds(5),
                            lastControl.IsPlaying);
                        break;
                    case EVideoPlayerCommand.PlayLive:
                        lastControl?.Model?.PlayLive();
                        lastControl?.EdgeVideoTimeline?.JumpToLiveCommand?.Execute(null);
                        break;
                    case EVideoPlayerCommand.Snapshot:
                        SnapshotControl(lastControl);
                        break;
                    case EVideoPlayerCommand.OpenBookmarkManager:
                        break;
                    case EVideoPlayerCommand.Search:
                        break;
                    case EVideoPlayerCommand.Fullscreen:
                        ToggleFullscreenTile(lastControl);
                        break;
                    case EVideoPlayerCommand.ToggleTimeline:
                        ToggleTimeline(lastControl);
                        break;
                    case EVideoPlayerCommand.ToggleThumbnails:
                        //ToggleThumbnails();
                        break;
                    case EVideoPlayerCommand.ExportVideo:
                        ExportHighlight(lastControl);
                        break;
                    case EVideoPlayerCommand.KeyboardHelp:
                        ToggleKeyboardHelp(true);
                        break;
                    case EVideoPlayerCommand.JumpToArchive:
                        if (SelectedComponents != null)
                        {
                            JumpToArchiveDialog(SelectedComponents?.OfType<EdgeVideoControl>()?.ToArray());
                        }

                        break;
                    case EVideoPlayerCommand.ConfigureStream:
                        break;

                    case EVideoPlayerCommand.RecenterTimeline:
                        lastControl?.EdgeVideoTimeline.RecenterCommand.Execute(null);
                        break;
                }
            }
        }

        private async void ExportHighlight(EdgeVideoControl lastControl)
        {
            if (lastControl != null)
            {
                try
                {
                    var modelSelectedRecordingProfile =
                        lastControl?.Model?.SelectedRecordingProfile ?? RecordingProfileItemManager.Manager
                            .ItemCollection.Values.Where(o =>
                                o.CameraId == lastControl.VideoObject.Id).FirstOrDefault();
                    if (modelSelectedRecordingProfile != null)
                    {
                        if (lastControl.EdgeVideoTimeline != null)
                        {
                            if (lastControl.Model != null)
                            {
                                await TileGridView.ExportVideo(lastControl.VideoObject.Id,
                                    modelSelectedRecordingProfile.Id,
                                    lastControl.EdgeVideoTimeline.CursorTime);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }
        }


        private void ToggleKeyboardHelp(bool visibile)
        {
            var monitoringPane = TileGridView.ParentOfType<MonitoringView>();
            if (monitoringPane != null)
            {
                monitoringPane.Model.IsHotkeyHelpVisible = visibile;
                monitoringPane.KeyboardHotkeyPopup.Focus();
            }
        }

        private void JumpToArchiveDialog(params EdgeVideoControl[] videoControls)
        {
            if (videoControls != null && videoControls.Length > 0)
            {
                var dialog = new ChooseCalendarPlaybackDialog();
                var firstControl = videoControls.FirstOrDefault();
                if (firstControl != null)
                {
                    dialog.SetStartDate(firstControl.EdgeVideoTimeline.CursorTime);
                    if (dialog.ShowDialog().HasValue &&
                        dialog.DialogItem is ChooseCalendarPlaybackDialogItemModel item &&
                        item.SelectedDate != DateTime.MinValue)
                    {
                        foreach (var control in videoControls)
                        {
                            control.JumpToDate(item.SelectedDate, item.PausePlayback);
                        }
                    }
                }
            }
        }

        private bool CanUseVideoCommand()
        {
            return true;
        }

        private void SelectAllComponents()
        {
            if (SelectedComponents.Count == Components.Count)
            {
                SelectedComponents.Clear();
            }
            else
            {
                foreach (var c in Components.Where(o => !SelectedComponents.Contains(o)))
                {
                    SelectedComponents.Add(c);
                }
            }
        }

        private bool CanSelectAll()
        {
            return true;
        }

        public bool ToggleFullscreenTile(ITileComponent tile)
        {
            if (tile == null || GridSize == new Size(1, 1))
            {
                return false;
            }

            tile.IsFullscreen = !tile.IsFullscreen;
            _ = TileGridView.TryUiAsync(() =>
            {
                if (tile.IsFullscreen)
                {
                    foreach (var c in Components.OfType<EdgeVideoControl>().Where(o => o != tile))
                    {
                        if (c.Visibility != Visibility.Hidden)
                        {
                            c.Visibility = Visibility.Hidden;
                        }
                    }

                    PreviousGridSize = GridSize;
                    _log?.Info($"Setting Previous to GridSize {PreviousGridSize}");
                    GridSize = new Size(0, 0);
                }
                else
                {
                    //foreach (var c in Components.OfType<EdgeVideoControl>().Where(o => o != tile))
                    //{
                    //    if (c.Visibility != Visibility.Visible)
                    //    {
                    //        c.Visibility = Visibility.Visible;
                    //    }
                    //}

                    _log?.Info($"Setting GridSize Back to Previous {PreviousGridSize}");
                    GridSize = PreviousGridSize;
                    PreviousGridSize = Size.Empty;
                }
            });

            return tile.IsFullscreen;
        }

        //public int GetGridColumnCount()
        //{
        //    return TileGridView?.DynamicGrid?.ColumnDefinitions?.Count ?? 0;
        //}

        //public int GetGridRowCount()
        //{
        //    return TileGridView?.DynamicGrid?.RowDefinitions?.Count ?? 0;
        //}


        private async void SetGridSize(Size size)
        {
            if (!IsSettingGridSize)
            {
                var sw = new Stopwatch();
                sw.Start();
                await Application.Current?.Dispatcher?.InvokeAsync(async () =>
                {
                    IsSettingGridSize = true;
                    try
                    {
                        //var sw2 = new Stopwatch();
                        //sw2.Start();
                        //_log.Info($"sw2 {sw2.ElapsedMilliseconds}ms!");
                        //sw2.Restart();
                        //_log?.Info($"Setting GridSize {size}");
                        TileGridView.DynamicGrid.SetValue(GridHelpers.ColumnCountProperty, (int) size.Width);
                        TileGridView.DynamicGrid.SetValue(GridHelpers.RowCountProperty, (int) size.Height);
                        //lock (LockObject)
                        //Trace.WriteLine($"SetGridSize {size}");
                        using (var d = TileGridView?.Dispatcher?.DisableProcessing())
                        {
                            if (size != new Size(0, 0))
                            {
                                var fullScreened = Components.Where(o => o.IsFullscreen);
                                if (fullScreened != null)
                                {
                                    PreviousGridSize = size;
                                    foreach (var f in fullScreened)
                                    {
                                        f.IsFullscreen = false;
                                    }

                                    //ToggleFullscreenTile(fullScreened);
                                    //return;
                                }
                            }

                            //_log.Info($"sw2 {sw2.ElapsedMilliseconds}ms!");
                            //sw2.Restart();
                            var tileComponents = TileGridView.DynamicGrid.Children.OfType<ITileComponent>()
                                .OrderBy(o => o.TileRow)
                                .ThenBy(o => o.TileColumn).ToList();

                            var panelIndex = 0;

                            for (var row = 0; row < size.Width; row++)
                            {
                                for (var col = 0; col < size.Height; col++)
                                {
                                    panelIndex++;

                                    Trace.WriteLine(panelIndex);
                                    try
                                    {
                                        if (tileComponents.Count < panelIndex)
                                        {
                                            InitGridControl(panelIndex, row, col);
                                        }

                                        var alterLocation = tileComponents.ElementAtOrDefault(panelIndex - 1);
                                        if (alterLocation != null)
                                        {
                                            var alterLocationTileIndex = alterLocation.TileIndex;
                                            if (alterLocationTileIndex <= size.Width * size.Height)
                                            {
                                                //Trace.WriteLine($"Setting {alterLocationTileIndex} to {panelIndex}");
                                                alterLocation.TileIndex = panelIndex;
                                                alterLocation.TileRow = row;
                                                alterLocation.TileColumn = col;
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        //_log?.Info($"InitTiles {e}");
                                    }
                                }
                            }

                            //_log.Info($"sw2 tileComponents {sw2.ElapsedMilliseconds}ms!");
                            //sw2.Restart();
                            try
                            {
                                if (!TileGridView.DynamicGrid.Children.OfType<ITileComponent>()
                                    .Any(o => o.IsFullscreen))
                                {
                                    var controlstoRemove = TileGridView.DynamicGrid.Children.OfType<EdgeVideoControl>()
                                        .Where(o => o.TileIndex > size.Height * size.Width).ToList();

                                    foreach (var r in controlstoRemove)
                                    {
                                        Trace.WriteLine($"Pruning {r.TileIndex}");
                                        try
                                        {
                                            if (r.Visibility != Visibility.Hidden)
                                            {
                                                r.Visibility = Visibility.Hidden;
                                            }

                                            if (r?.Model?.GetMpvVideoComponent?.VideoView?.VideoContent != null &&
                                                r.Model.GetMpvVideoComponent.VideoView.VideoContent?.Visibility !=
                                                Visibility.Hidden)
                                            {
                                                r.Model.GetMpvVideoComponent.VideoView.VideoContent.Visibility =
                                                    Visibility.Hidden;
                                            }

                                            if (r.Model != null)
                                            {
                                                r.Model.Stop();
                                            }

                                            if (SelectedComponents.Contains(r))
                                            {
                                                SelectedComponents.Remove(r);
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            _log?.Error($"{e.GetType()} {e.Message}");
                                        }
                                    }
                                }

                                //_log.Info($"sw2 PruneTiles {sw2.ElapsedMilliseconds}ms!");
                                //sw2.Restart();
                                var edgeVideoControls = TileGridView.DynamicGrid.Children
                                    .OfType<EdgeVideoControl>().Where(o => o.TileIndex <= size.Height * size.Width)
                                    .ToList();

                                foreach (var c in edgeVideoControls)
                                {
                                    try
                                    {
                                        Trace.WriteLine($"Adding {c.TileIndex}");
                                        if (c.Visibility != Visibility.Visible)
                                        {
                                            c.Visibility = Visibility.Visible;
                                            _log?.Info($"Setting {c.TileRow}x{c.TileColumn} to Visible");
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        _log?.Error($"{e.GetType()} {e.Message}");
                                    }
                                }

                                //_log.Info($"sw2 WhenAll {sw2.ElapsedMilliseconds}ms!");
                                //sw2.Restart();
                            }
                            catch (Exception e)
                            {
                                _log?.Info(e);
                            }

                            Components =
                                new ObservableCollection<ITileComponent>(
                                    TileGridView.DynamicGrid.Children.OfType<ITileComponent>());
                            //_log.Info($"sw2 Components {sw2.ElapsedMilliseconds}ms!");
                            //sw2.Restart();
                        }
                    }
                    catch (Exception e)
                    {
                        _log?.Error($"{e.GetType()} {e.Message}");
                    }
                    finally
                    {
                        IsSettingGridSize = false;
                        sw.Stop();
                        _log?.Info($"SETGRIDSIZE TOOK {sw.ElapsedMilliseconds}ms!");
                    }
                });
            }
        }

        private void InitGridControl(int index, int row, int col)
        {
            //Application.Current.Dispatcher.InvokeAsync(() =>
            //{
            var control = new EdgeVideoControl {TileRow = row, TileColumn = col, TileIndex = index};
            TileGridView.DynamicGrid.Children.Add(control);
            // control.MainButton.Drop += (sender, args) => ControlOnDrop(control,args);
            // control.MainButton.DragOver += (sender, args) => DragOverEvent(control,args);
            //control.PtzOverlay.AddHandler(UIElement.MouseDownEvent, new MouseButtonEventHandler((sender, args) => control.Model.DoMouseDown(args)),true);
            //control.PtzOverlay.AddHandler(UIElement.MouseUpEvent, new MouseButtonEventHandler((sender, args) =>  control.Model.DoMouseUp(args)), true);
            AddDropHandler(control.MainButton,
                (sender, args) => ControlOnDropTelerik(control, args));
            AddDragOverHandler(control.MainButton,
                (sender, args) => DragOverEventTelerik(control, args));
            AddDragInitializeHandler(control.DragButton,
                (sender, args) => OnDragInitialize(control, args));
            SetAllowCapturedDrag(control.DragButton, true);
            SetAllowDrag(control.DragButton, true);
            control.ClickableAreaButton.PreviewMouseLeftButtonDown +=
                (sender, args) => control.Model.DoMouseDown(args);
            control.ClickableAreaButton.PreviewMouseLeftButtonUp +=
                (sender, args) => control.Model.DoMouseUp(args);
            control.ClickableAreaButton.MouseWheel += (o, args) => MainButtonOnMouseWheel(control, args);
            control.ClickableAreaButton.Click += (sender, args) => VideoControlClick(control, args);
            control.ClickableAreaButton.MouseDoubleClick +=
                (sender, args) => VideoDoubleControlClick(control, args);


            control.ClickableAreaButton.PreviewKeyDown += (sender, args) =>
            {
                if (args.Key == Key.Space)
                {
                    args.Handled = true;
                    ExecuteVideoCommand(EVideoPlayerCommand.PlayPause);
                }
            };
            control.ClickableAreaButton.KeyDown += (sender, args) =>
            {
                control.RaiseEvent(
                    args
                );
                control.GetParentRadWindow<RadWindow>()?.RaiseEvent(args);
                args.Handled = true;
            };
            control.ClickableAreaButton.KeyUp += (sender, args) =>
            {
                control.RaiseEvent(
                    args
                );
                control.GetParentRadWindow<RadWindow>()?.RaiseEvent(args);
                args.Handled = true;
            };
            // });
        }

        private async void ControlOnDropTelerik(EdgeVideoControl sender, DragEventArgs e)
        {
            try
            {
                if (sender is EdgeVideoControl videoControl)
                {
                    // Handles Swapping Players when Dragging
                    if (GetDataFromObject(e.Data, "DraggedData") is
                        EdgeVideoControl
                        videoControlDrop)
                    {
                        if (Keyboard.IsKeyDown(Key.LeftCtrl))
                        {
                            e.Effects |= DragDropEffects.Copy;
                            videoControl.VideoObject = videoControlDrop.VideoObject;
                            videoControl.Model?.PlayLive();
                        }
                        else
                        {
                            HandleTileSwap(videoControlDrop, videoControl);
                        }
                    }
                    // Handles TreeView DragDrop of CameraItem into a tile
                    else if (GetDataFromObject(e.Data, TreeViewDragDropOptions.Key) is
                        TreeViewDragDropOptions
                        treeViewDrop)
                    {
                        var dropItem = treeViewDrop.DragSourceItem.Item;
                        if (dropItem != null)
                        {
                            if (dropItem is CameraItemModel camItem)
                            {
                                var cameraItemModels = treeViewDrop?.DraggedItems?.OfType<CameraItemModel>()?.ToList();
                                if (cameraItemModels != null && cameraItemModels.Count > 1)
                                {
                                    HandleMultipleCameraLoading(cameraItemModels, videoControl.TileIndex);
                                    return;
                                }

                                //if (!string.IsNullOrEmpty(camItem.Address))
                                {
                                    videoControl.VideoObject =
                                        camItem;

                                    videoControl.Model?.PlayLive();
                                    SelectedComponents.Remove(videoControl);
                                    //Application.Current.Dispatcher.InvokeAsync(() =>
                                    //{
                                    //    PresetCollectionViewSource?.View?.Refresh();
                                    //});
                                }
                            }
                            else if (dropItem is LayoutItemModel layoutItem)
                            {
                                await HandleLayoutItem(layoutItem);
                            }
                            else if (dropItem is NodeItemModel node)
                            {
                                HandleNodeFolderDrop(node, treeViewDrop, videoControl);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
            }
        }

        private void HandleTileSwap(EdgeVideoControl videoControlDrop, EdgeVideoControl videoControl)
        {
            if (Components.Contains(videoControlDrop) && videoControl != videoControlDrop)
            {
                var oldIndex = videoControl.TileIndex;
                var oldX = videoControl.TileRow;
                var oldY = videoControl.TileColumn;

                var newIndex = videoControlDrop.TileIndex;
                var newX = videoControlDrop.TileRow;
                var newY = videoControlDrop.TileColumn;

                videoControlDrop.TileRow = oldX;
                videoControlDrop.TileColumn = oldY;
                videoControlDrop.TileIndex = oldIndex;
                videoControl.TileRow = newX;
                videoControl.TileColumn = newY;
                videoControl.TileIndex = newIndex;

                //Event triggers when controls have swapped places
                async void UpdateOverlayLocations(object s, EventArgs args)
                {
                    await videoControl.TryUiAsync(() =>
                    {
                        if (videoControl?.ContentControl?.Content is IAirspaceContent content)
                        {
                            content.Redraw();
                        }

                        if (videoControlDrop?.ContentControl?.Content is IAirspaceContent targetContent)
                        {
                            targetContent.Redraw();
                        }
                    });
                    TileGridView.LayoutUpdated -= UpdateOverlayLocations;
                }

                TileGridView.LayoutUpdated += UpdateOverlayLocations;


                //Console.WriteLine($"Swapping {oldX}x{oldY} with {newX}x{newY}");
            }
        }

        private async void HandleNodeFolderDrop(NodeItemModel node, TreeViewDragDropOptions treeviewDrag,
            EdgeVideoControl videoControl)
        {
            try
            {
                if (!node.IsExpanded)
                {
                    try
                    {
                        await Application.Current.Dispatcher.Invoke(async () =>
                        {
                            var dataContext =
                                treeviewDrag.DragSourceItem.ParentTreeView.DataContext as CameraTreeViewModel;
                            var childrenLoaded = dataContext != null &&
                                                 await dataContext.GetChildren(node, treeviewDrag.DragSourceItem);
                        });
                        //TODO: change this 
                        //using (var cts = new CancellationTokenSource(12000))
                        //{
                        //    await Task.Run(async () =>
                        //    {
                        //        while (node.Children.Count == 0)
                        //        {
                        //            await Task.Delay(35);
                        //        }
                        //    }, cts.Token);
                        //}
                    }
                    catch (Exception e)
                    {
                        _log?.Info(e);
                    }
                }

                var cameras = node.Children?.OrderBy(o => o.Label)?.OfType<CameraItemModel>()?.Take(16).ToList();

                HandleMultipleCameraLoading(cameras, videoControl.TileIndex);
            }
            catch (Exception e)
            {
            }
        }
        // private void HandleMultipleCameraLoadingOld(List<CameraItemModel> cameras, Size size)
        //{
        //    if (cameras != null)
        //    {
        //        TileGridView.TryUiAsync(async () =>
        //        {
        //            try
        //            {
        //                if (cameras.Count() > 9)
        //                {
        //                    if (GridSize.Width < 4)
        //                    {
        //                        GridSize = new Size(4, 4);
        //                    }
        //                }
        //                else if (cameras.Count() > 4)
        //                {
        //                    if (GridSize.Width < 3)
        //                    {
        //                        GridSize = new Size(3, 3);
        //                    }
        //                }
        //                else if (cameras.Count() > 2)
        //                {
        //                    if (GridSize.Width < 2)
        //                    {
        //                        GridSize = new Size(2, 2);
        //                    }
        //                }
        //                else if (cameras.Count() == 1)
        //                {
        //                    if (GridSize.Width < 1)
        //                    {
        //                        GridSize = new Size(1, 1);
        //                    }
        //                }

        //                await Task.Delay(125);
        //                var controls = TileGridView?.DynamicGrid?.Children
        //                    ?.OfType<EdgeVideoControl>().Where(z => z.IsVisible).OrderBy(o => o.TileRow)
        //                    .ThenBy(o => o.TileColumn)
        //                    .Where(v => v.VideoObject == null)
        //                    .ToList();
        //                var i = 0;

        //                if (controls != null)
        //                {
        //                    foreach (var c in controls)
        //                    {
        //                        try
        //                        {
        //                            if (c != null)
        //                            {
        //                                c.VideoObject = cameras.ElementAtOrDefault(i);
        //                                i++;
        //                                c.Model?.PlayLive();
        //                            }
        //                        }
        //                        catch (Exception e)
        //                        {
        //                        }
        //                    }

        //                    TileGridView.Model.SelectedComponents.Clear();
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //            }
        //        });
        //    }
        //}
        private async void HandleMultipleCameraLoading(List<CameraItemModel> cameras, int tileIndex)
        {
            if (cameras != null)
            {
                var _ = TileGridView.TryUiAsync(async () =>
                {
                    try
                    {
                        var count = cameras.Count() + (tileIndex - 1);
                        if (count > 9)
                        {
                            if (GridSize.Width < 4)
                            {
                                GridSize = new Size(4, 4);
                            }
                        }
                        else if (count > 4)
                        {
                            if (GridSize.Width < 3)
                            {
                                GridSize = new Size(3, 3);
                            }
                        }
                        else if (count > 2)
                        {
                            if (GridSize.Width < 2)
                            {
                                GridSize = new Size(2, 2);
                            }
                        }
                        else if (count == 1)
                        {
                            if (GridSize.Width < 1)
                            {
                                GridSize = new Size(1, 1);
                            }
                        }

                        await Task.Delay(125);
                        var controls = TileGridView?.DynamicGrid?.Children
                            ?.OfType<EdgeVideoControl>().Where(z => z.IsVisible).OrderBy(o => o.TileRow)
                            .ThenBy(o => o.TileColumn)
                            // .Where(v => v.VideoObject == null)
                            .ToList();
                        var i = 0;
                        var startingTile = tileIndex;
                        if (controls != null)
                        {
                            foreach (var c in controls)
                            {
                                if (c.TileIndex < startingTile)
                                {
                                    continue;
                                }

                                try
                                {
                                    if (c != null)
                                    {
                                        c.VideoObject = cameras.ElementAtOrDefault(i);
                                        i++;
                                        c.Model?.PlayLive();
                                    }
                                }
                                catch (Exception e)
                                {
                                }
                            }

                            TileGridView.Model.SelectedComponents.Clear();
                        }
                    }
                    catch (Exception e)
                    {
                    }
                });
            }
        }

        public async Task HandleLayoutItem(LayoutItemModel layoutItem)
        {
            //var sw = new Stopwatch();
            //sw.Start();
            _log?.Info($"HandleLayoutItem {layoutItem.Id} {layoutItem?.GridSize}");
            var tcs = new TaskCompletionSource<List<CameraItemModel>>();

            var _ = Task.Run(async () =>
            {
                try
                {
                    var sw = new Stopwatch();
                    sw.Start();
                    var list = layoutItem.CameraCollection.Distinct((a, b) => a?.Item2.Id == b?.Item2.Id).Select(o =>
                        new NodeItemModel(o.Item2.Convert<NodeDescriptor>())
                            {ObjectId = o.Item2.Id, ZoneId = o.Item2.ZoneId}).ToArray();
                    var cameraBatch = await CameraItemMgr.RequestCameras(list);
                    sw.Stop();
                    _log?.Info($"Requesting Cameras took {sw.ElapsedMilliseconds}ms");
                    sw.Reset();
                    sw.Start();
                    //while (IsSettingGridSize)
                    //{
                    //    await Task.Delay(35);
                    //}

                    //while (IsSettingGridSize)
                    //{
                    //    await Task.Delay(35);
                    //}

                    _log?.Info($"GridSize Is Set! {layoutItem.Id} {layoutItem?.GridSize}");

                    if (cameraBatch != null)
                    {
                        var output = cameraBatch.Select(o => (CameraItemModel) o?.Camera).ToList();
                        var _ = Task.Run(() =>
                        {
                            var _ = CameraItemHelper.InitCameraProfiles(output);
                        });

                        foreach (var cam in cameraBatch)
                        {
                            if (cam?.Camera != null)
                            {
                                CameraItemModel camModel = cam.Camera;

                                output.Add(camModel);
                            }
                        }

                        {
                        }

                        tcs.SetResult(output);
                        //Console.WriteLine($"Time elapsed after cameraBatch {sw.ElapsedMilliseconds}ms");
                    }
                    //if (GridSize.Height != layoutItem.GridSize.Height || GridSize.Width != layoutItem.GridSize.Width)

                    _log?.Info($"Layout Handling took {sw.ElapsedMilliseconds}ms");
                    sw.Stop();

                    sw = null;
                    return;
                }
                catch (Exception e)
                {
                    _log?.Info($"Error :{e.GetType()} {e.Message}");
                }

                tcs.SetResult(null);
            });
            try
            {
                await TileGridView.TryUiAsync(async () => { GridSize = layoutItem.GridSize; });
                var cameras = (await tcs.Task).Where(o => o != null).ToList();

                if (cameras != null)
                {
                    //Console.WriteLine($"Time elapsed after gridsize {sw.ElapsedMilliseconds}ms");
                    await Task.Delay(50);
                    foreach (var t in Components.OfType<EdgeVideoControl>())
                    {
                        //t.Dispose();
                        var matchingTile =
                            layoutItem.CameraCollection.FirstOrDefault(o =>
                                o.Item1 == new Size(t.TileColumn, t.TileRow));
                        if (matchingTile != null)
                        {
                            //t.VideoObject = matchingTile.Item2;
                            //t.WpfUiThread(() =>
                            //{
                            //    t.Model?.PlayLive();
                            //});

                            t.VideoObject = cameras?.FirstOrDefault(o => o.Id == matchingTile.Item2.Id);

                            if (t.VideoObject != null)
                            {
                                _log?.Info($"Playing {t.VideoObject.LiveStreamUrl}");
                            }

                            _ = t.TryUiAsync(() => { t.Model?.PlayLive(); });


                            if (SelectedComponents.Contains(t))
                            {
                                SelectedComponents.Remove(t);
                            }
                        }
                        else
                        {
                            _ = t.TryUiAsync(() => { t.Model?.Stop(); });
                        }
                    }

                    await Task.Delay(3000);
                    _ = this.TryUiAsync(() => { PresetCollectionViewSource?.View?.Refresh(); },
                        DispatcherPriority.Background);


                    //var urlBatch = await GetManagerByZoneId().CameraService
                    //    .Client.Request(cameras.Select(o => new GetCameraInfo {CameraId = o.Id}).ToArray());

                    ////correct any layout that might have had the wrong URL


                    //if (urlBatch != null)
                    //{
                    //    foreach (var c in urlBatch)
                    //    {
                    //        if (c != null && c.CameraInformation != null)
                    //        {
                    //            var matchinInfo = cameras.ElementAtOrDefault(urlBatch.IndexOf(c));
                    //            var cameraProfile = c.CameraInformation.Profile.FirstOrDefault();
                    //            if (matchinInfo != null && !string.IsNullOrEmpty(cameraProfile?.StreamUri) &&
                    //                matchinInfo.LiveStreamUrl != cameraProfile.StreamUri)
                    //            {
                    //                if (matchinInfo != null)
                    //                {
                    //                    foreach (var e in Components.OfType<EdgeVideoControl>().Where(o =>
                    //                        o != null && o.VideoObject != null &&
                    //                        o.VideoObject.LiveStreamUrl == matchinInfo.LiveStreamUrl &&
                    //                        o.VideoObject.LiveStreamUrl != cameraProfile.StreamUri && o.IsPlaying &&
                    //                        o.VideoComponent.IsLivePlayback))
                    //                    {
                    //                        e.VideoComponent.Open(cameraProfile.StreamUri);
                    //                        e.VideoComponent.Play();
                    //                    }


                    //                    matchinInfo.CameraUrl = cameraProfile.StreamUri;
                    //                    matchinInfo.LiveStreamUrl = cameraProfile.StreamUri;

                    //                    matchinInfo.CameraInformation =
                    //                        c.CameraInformation.Convert<CameraInformationItemModel>();
                    //                    matchinInfo.UpdateCamera(force: true);
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                }
            }
            catch (Exception e)
            {
                _log?.Error($"{e.GetType()}: {e.Message}");
            }

            //Console.WriteLine($"Time elapsed after entire layout {sw.ElapsedMilliseconds}ms");
            //sw.Stop();
        }

        private void DragOverEventTelerik(EdgeVideoControl sender, DragEventArgs e)
        {
            try
            {
                if (sender is EdgeVideoControl videoControl &&
                    GetDataFromObject(e.Data, TreeViewDragDropOptions.Key) is TreeViewDragDropOptions
                        options)
                {
                    if (options.DragSourceItem.Item is NodeObject)
                    {
                        if (SelectedComponents.Count > 0)
                        {
                            SelectedComponents.Clear();
                        }

                        if (!SelectedComponents.Contains(videoControl))
                        {
                            SelectedComponents.Add(videoControl);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
            }
        }

        private void VideoDoubleControlClick(ITileComponent control, MouseButtonEventArgs args)
        {
            if (control != null)
            {
                if (control is EdgeVideoControl videoControl)
                {
                    videoControl.Model?.DoMouseUp(args);
                }

                ToggleFullscreenTile(control);
            }
        }

        public async Task PruneTiles(Size size)
        {
            if (TileGridView.DynamicGrid.Children.OfType<ITileComponent>().Any(o => o.IsFullscreen))
            {
                return;
            }

            //var tileComponents = TileGridView.DynamicGrid.Children.OfType<ITileComponent>().OrderBy(o => o.TileRow)
            //    .ThenBy(o => o.TileColumn).ToList();
            var controlstoRemove = TileGridView.DynamicGrid.Children.OfType<EdgeVideoControl>()
                .Where(o => o.TileIndex > size.Height * size.Width).ToList();
            //var controlstoRemove = TileGridView.DynamicGrid.Children.OfType<EdgeVideoControl>()
            //    .Where(o => size.Width - 1 < o.TileRow || size.Height - 1 < o.TileColumn).ToList();

            await Application.Current.Dispatcher.InvokeAsync(() =>
            {
                foreach (var r in controlstoRemove)
                {
                    //await Task.WhenAll(controlstoRemove.Select(async r =>
                    //{
                    //    await Application.Current.Dispatcher.InvokeAsync(() =>
                    //    {
                    try
                    {
                        r.Model.Stop();
                        if (SelectedComponents.Contains(r))
                        {
                            SelectedComponents.Remove(r);
                        }

                        if (r.Visibility != Visibility.Hidden)
                        {
                            r.Visibility = Visibility.Hidden;
                        }

                        if (r?.Model?.GetMpvVideoComponent?.VideoView?.VideoContent != null &&
                            r.Model.GetMpvVideoComponent.VideoView.VideoContent?.Visibility !=
                            Visibility.Hidden)
                        {
                            r.Model.GetMpvVideoComponent.VideoView.VideoContent.Visibility = Visibility.Hidden;
                        }
                    }
                    catch (Exception e)
                    {
                        _log?.Info($"Prune Failed: {e.GetType()} {e.Message}");
                    }

                    //    });
                    //}));
                    // TileGridControl.DynamicGrid.Children.Remove(r);
                }
            });
            //  });
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void CollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is KeyValuePair<Tuple<Guid, string>, PtzPresetItemModel> preset)
            {
                e.Accepted = SelectedControl != null && SelectedControl.VideoObject != null &&
                             preset.Key.Item1 == SelectedControl.VideoObject.Id;
            }
        }

        private void OnDirectModeChanged(bool directMode)
        {
            if (directMode)
            {
                //foreach (var edgeVideoControl in Components.OfType<EdgeVideoControl>())
                //{
                //    App.Current?.Dispatcher?.InvokeAsync(() =>
                //    {
                //     //   edgeVideoControl.Model.ControlOverlaySettings &= ~(EControlOverlaySettings.Timeline);
                //    });

                //    if (!edgeVideoControl.VideoComponent.IsLivePlayback)
                //    {

                //        edgeVideoControl.Model.PlayLive();
                //    }
                //}
            }
        }

        private async void HubManagerOnSignalRClientEvent(ESignalRClientEvent clientEvent, Guid ids, string data)
        {
            if (clientEvent.Equals(ESignalRClientEvent.TimelineEventCreated))
            {
                try
                {
                    var (cameraId, timelineId) = JsonSerializer.DeserializeFromString<Tuple<Guid, Guid>>(data);
                    var controlsWithTimeline = Components.OfType<EdgeVideoControl>()
                        .Where(o => o.VideoObject != null && o.VideoObject.Id == cameraId);
                    if (controlsWithTimeline != null)
                    {
                        var timelineEvent = await GetManagerByZoneId().TimelineEventService
                            .GetTimelineEvent(new GetTimelineEvent {TimelineEventId = timelineId});
                        if (timelineEvent != null)
                        {
                            foreach (var c in controlsWithTimeline)
                            {
                                c.EdgeVideoTimeline.DataProvider.Bookmarks.Add(
                                    (TimelineEventItemModel) timelineEvent.TimelineEvent);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }
            else if (clientEvent.Equals(ESignalRClientEvent.TimelineEventDeleted))
            {
                try
                {
                    var (cameraId, timelineId) = JsonSerializer.DeserializeFromString<Tuple<Guid, Guid>>(data);
                    var controlsWithTimeline = Components.OfType<EdgeVideoControl>()
                        .Where(o => o.VideoObject != null && o.VideoObject.Id == cameraId);
                    foreach (var c in controlsWithTimeline)
                    {
                        c.EdgeVideoTimeline.DataProvider.Bookmarks =
                            c.EdgeVideoTimeline.DataProvider.Bookmarks.Where(o => o.ObjectId == timelineId).ToList();
                    }
                }
                catch (Exception e)
                {
                }
            }
        }

        private void MainButtonOnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (sender is EdgeVideoControl videoControl)
            {
                videoControl.DigitalZoomCanvasView?.Model?.Zoom(e.Delta > 0, e.GetPosition(videoControl));
            }
        }

        private void OnDragInitialize(object sender, DragInitializeEventArgs e)
        {
            var details = new DropIndicationDetails();

            var dragPayload = GeneratePayload(null);

            dragPayload.SetData("DropDetails", details);

            e.Data = dragPayload;

            if (sender is EdgeVideoControl videoControl)
            {
                dragPayload.SetData("DraggedData", videoControl);
                var grid = new Grid {Background = Brushes.Black, Opacity = .5f};
                var image = videoControl.LoadingSplash.Source;

                if (videoControl.VideoComponent is MPVVideoComponent mpvVideoComponent)
                {
                    if (Keyboard.IsKeyDown(Key.LeftCtrl))
                    {
                    }
                    else
                    {
                        var img = mpvVideoComponent.VideoView.WindowsFormsHost.Child
                            .Screenshot(); //.SnapshotRenderTargetBitmapPng();
                        if (img != null)
                        {
                            var img2 = ImageUtil.BitmapToSource(img);
                            // Clipboard.SetImage(img2);
                            image = img2;
                        }
                    }
                }

                grid.Children.Add(new Image
                {
                    Opacity = .7f,
                    Height = videoControl.MainGrid.ActualHeight,
                    Width = videoControl.MainGrid.ActualWidth,
                    Source = image
                });
                e.DragVisual = grid;
            }

            e.DragVisualOffset = new Point(e.RelativeStartPoint.X - 40, e.RelativeStartPoint.Y - 22);
            e.AllowedEffects = DragDropEffects.All;
            e.Handled = true;
        }

        private void SelectedComponentsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                //Console.WriteLine($"New Items {e.NewItems.Count}");
                //foreach (var c in Components.OfType<EdgeVideoControl>().Where(o => !e.NewItems.Contains(o) && (e.OldItems == null || !e.OldItems.Contains(o))))
                //{
                //    c.SetSelected(false);
                //}

                foreach (var c in e.NewItems.OfType<EdgeVideoControl>())
                {
                    SelectedControl = c;
                    c.SetSelected(true);
                }
            }
            else
            {
                var componentsToRemove = new List<ITileComponent>();
                foreach (var c in Components.OfType<EdgeVideoControl>())
                {
                    c.SetSelected(false);
                    if (SelectedComponents.Contains(c))
                    {
                        componentsToRemove.Add(c);
                    }
                }

                foreach (var c in componentsToRemove)
                {
                    SelectedComponents.Remove(c);
                }
            }
        }

        private void VideoControlClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (sender is EdgeVideoControl videoControl)
            {
                videoControl.GetVisualParent<System.Windows.Window>()?.GlobalActivate();
                TileGridView.Focus();

                videoControl.Model?.DoMouseUp(null);
                // Select / Deselect of Control
                if (SelectedComponents.Contains(videoControl))
                {
                    SelectedComponents.Remove(videoControl);
                }
                else
                {
                    try
                    {
                        if (!Keyboard.IsKeyDown(Key.LeftCtrl))
                        {
                            foreach (var s in SelectedComponents.ToList())
                            {
                                SelectedComponents.Remove(s);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                    }

                    SelectedComponents.Add(videoControl);
                }
            }
        }
    }
}