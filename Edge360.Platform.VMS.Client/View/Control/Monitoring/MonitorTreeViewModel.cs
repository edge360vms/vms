﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.DataObject;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Dialog;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Cameras;
using Edge360.Raven.Vms.CameraService.ServiceModel.Enums;
using Newtonsoft.Json;
using ServiceStack;
using Telerik.Windows.Controls;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring
{
    public class MonitorTreeViewModel : ViewModelBase, INotifyPropertyChanged
    {
        public MonitorTreeView View { get; set; }

        private RelayCommand _addLayoutCommand;
        private DirectModeManager _directModeManager;

        public RelayCommand AddLayoutCommand => _addLayoutCommand ??= new RelayCommand(async p => await AddLayout(),
            p => CanAddLayout());


        public MonitorTreeViewModel(DirectModeManager directModeManager = null)
        {
            DirectModeManager = directModeManager;
        }

        public DirectModeManager DirectModeManager
        {
            get => _directModeManager;
            set
            {
                if (Equals(value, _directModeManager)) return;
                _directModeManager = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        private bool CanAddLayout()
        {
            return true;
        }

        private async Task AddLayout()
        {
            var input = new CreateLayoutDialog();

            if (input.ShowDialog().HasValue)
            {
                if (input.Result && input.Model.DialogItem is CreateLayoutItemModel dialogItem &&
                    !string.IsNullOrEmpty(dialogItem.Label))
                {
                    if (View != null)
                    {
                        var monitoringView = View?.GetVisualParent<MonitoringView>();
                        var layoutCameras = new ObservableCollection<Tuple<Size, CameraDescriptor>>();
                        if (monitoringView != null)
                        {
                            //monitoringView?.WpfUiThread(async () =>
                            //{
                            foreach (var tileComponent in monitoringView.TileGrid.Model.Components)
                            {
                                if (tileComponent is EdgeVideoControl edgeVideoControl)
                                {
                                    if (edgeVideoControl.VideoObject != null)
                                    {
                                        layoutCameras.Add(new Tuple<Size, CameraDescriptor>(
                                            new Size(tileComponent.TileColumn, tileComponent.TileRow),
                                            edgeVideoControl.VideoObject.ConvertTo<CameraDescriptor>()));
                                    }
                                }
                            }

                            NodeObject containerNode = null;
                            //if (monitoringView.MonitorTreeControl.CameraTreeView.Model.SelectedItem is NodeObject node)
                            //{
                            //    if (node is NodeItemModel folderItemModel)
                            //    {
                            //        containerNode = folderItemModel;
                            //    }
                            //    else if (node.ObjectId != null && node.ObjectId != Guid.Empty)
                            //    {
                            //        containerNode = monitoringView?.MonitorTreeControl?.CameraTreeView?.Model?.Nodes?
                            //            .FindParentNodeByChildId<NodeItemModel>(node.ObjectId.Value);
                            //    }
                            //}

                            //if (containerNode != null)
                            //{


                            // await Task.Run(async () =>
                            //  {

                            var layout = new LayoutItemModel
                            {
                                DateCreated = DateTime.UtcNow,
                                Label = dialogItem.Label,
                                Description = dialogItem.Description,
                                UpdatedUtc = DateTime.UtcNow,
                                CreatedUtc = DateTime.Now,


                                LastModified = DateTime.UtcNow,
                                ObjectId = Guid.NewGuid(),
                                ParentId = containerNode != null ? containerNode.ObjectId : Guid.Empty,
                                Type = "Layout",
                                CameraCollection = layoutCameras,
                                GridSize = monitoringView.TileGrid.Model.GridSize
                            };
                            var serializedLayout =
                                JsonConvert.SerializeObject(layout, ClientJsonUtil.IgnoreExceptionSettings());
                            var saved = await GetManagerByZoneId().GridLayoutService
                                .CreateGridLayout(
                                    dialogItem.Label,
                                    dialogItem.Description, serializedLayout
                                    , (int) EGridLayoutType.Camera,
                                    (int) dialogItem.ShareType);

                            if (saved != null)
                            {
                                layout.OwnerId = saved.GridLayout.OwnerId;
                                layout.Id = saved.GridLayout.Id;
                            }

                            //});
                            // });
                        }
                    }
                    //LayoutManager.Layouts.Layouts.Add(layout);

                    //LayoutManager.SaveLayouts();
                }
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}