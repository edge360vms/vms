﻿using System.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring.Pane.Playback
{
    /// <summary>
    ///     Interaction logic for MonitorPanePlaybackControl.xaml
    /// </summary>
    public partial class MonitorPanePlaybackControl : UserControl, IView
    {
        public MonitorPanePlaybackControl()
        {
            InitializeComponent();
            DataContext = new MonitorPanePlaybackViewModel();
        }
    }
}