﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring.Pane.Slider
{
    /// <summary>
    ///     Interaction logic for ColoredSlider.xaml
    /// </summary>
    public partial class ColoredSlider : UserControl, IView
    {
        public Action<double> ValueChanged;

        public ColoredSlider()
        {
            InitializeComponent();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ValueChanged?.Invoke(e.NewValue);
        }
    }
}