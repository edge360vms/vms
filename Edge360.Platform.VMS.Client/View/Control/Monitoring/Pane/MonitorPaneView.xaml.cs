﻿using System.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring.Pane
{
    /// <summary>
    ///     Interaction logic for MonitorPaneControl.xaml
    /// </summary>
    public partial class MonitorPaneView : UserControl, IView
    {
        public MonitorPaneViewModel Model => DataContext as MonitorPaneViewModel;

        public MonitorPaneView()
        {
            InitializeComponent();
            Model.View = this;
        }
    }
}