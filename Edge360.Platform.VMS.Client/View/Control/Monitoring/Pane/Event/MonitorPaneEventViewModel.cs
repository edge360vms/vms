﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.View.Control.Monitoring.TileGrid;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring.Pane.Event
{
    public class MonitorPaneEventViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private readonly MonitorPaneEventControl _view;

        public RelayCommand Grid1X1Command
        {
            get { return new RelayCommand(p => SetGrid(1, 1), p => CanChangeGrid()); }
        }

        public RelayCommand Grid2X2Command
        {
            get { return new RelayCommand(p => SetGrid(2, 2), p => CanChangeGrid()); }
        }


        public RelayCommand Grid3X3Command
        {
            get { return new RelayCommand(p => SetGrid(3, 3), p => CanChangeGrid()); }
        }


        public RelayCommand Grid4X4Command
        {
            get { return new RelayCommand(p => SetGrid(4, 4), p => CanChangeGrid()); }
        }


        public MonitorPaneEventViewModel(MonitorPaneEventControl view)
        {
            _view = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void SetGrid(int col, int row)
        {
            var monitoringView = _view.GetVisualParent<MonitoringView>();
            var tileGridViewModels = ObjectResolver.ResolveAll<MonitoringViewModel>();
            var tileGridModel = tileGridViewModels.FirstOrDefault(o =>
                o.View == monitoringView);

            if (tileGridModel != null)
            {
                tileGridModel.View.TileGrid.Model.GridSize = new Size(col, row);
            }
        }

        private bool CanChangeGrid()
        {
            return true;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}