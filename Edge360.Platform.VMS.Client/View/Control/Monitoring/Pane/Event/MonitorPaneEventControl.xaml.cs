﻿using System.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring.Pane.Event
{
    /// <summary>
    ///     Interaction logic for MonitorPaneEventControl.xaml
    /// </summary>
    public partial class MonitorPaneEventControl : UserControl, IView
    {
        public MonitorPaneEventControl()
        {
            InitializeComponent();
            DataContext = new MonitorPaneEventViewModel(this);
        }

        public MonitorPaneEventViewModel GetModel()
        {
            return DataContext as MonitorPaneEventViewModel;
        }
    }
}