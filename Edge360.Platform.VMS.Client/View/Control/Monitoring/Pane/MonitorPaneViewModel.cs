﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.View.Control.Monitoring.Pane.Ptz;
using Edge360.Platform.VMS.Client.View.Control.Monitoring.TileGrid;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Common.Enums.Camera;
using Edge360.Platform.VMS.Common.Interfaces.Video;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring.Pane
{
    public class MonitorPaneViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private DirectModeManager _directModeManager;
        private MonitorPaneView _view;

        public DirectModeManager DirectModeManager
        {
            get => _directModeManager;
            set
            {
                if (Equals(value, _directModeManager))
                {
                    return;
                }

                _directModeManager = value;
                OnPropertyChanged();
            }
        }

        public MonitorPaneView View
        {
            get => _view;
            set
            {
                if (Equals(value, _view))
                {
                    return;
                }

                _view = value;
                if (_view != null)
                {
                    AddPtzCircleEvents(_view.MonitorPanePtzControl.PtzCircleControl);
                }

                OnPropertyChanged();
            }
        }

        private EdgeVideoControl LastComponent
        {
            get
            {
                var lastControl = TileGrid?.Model?.SelectedComponents.LastOrDefault() as EdgeVideoControl;
                if (lastControl == null)
                {
                    TileGrid?.Model?.SelectedComponents?.Add(TileGrid.Model.Components.OfType<EdgeVideoControl>()
                        .FirstOrDefault(o =>
                            o != null && (o.IsFullscreen || o.TileColumn == 0 && o.TileRow == 0)));
                }

                return lastControl;
            }
        }

        private IVideoComponent LastVideoComponent => LastComponent?.ContentControl?.Content as IVideoComponent;

        private int PanSpeed { get; set; } = 50;
        private TileGridView TileGrid => View?.GetVisualParent<MonitoringView>()?.TileGridControl;

        private int ZoomSpeed { get; set; } = 50;


        public MonitorPaneViewModel(DirectModeManager directModeManager = null)
        {
            DirectModeManager = directModeManager;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void AddPtzCircleEvents(PtzCircleControl circle)
        {
            circle.ZoomSliderValueChanged += ZoomSliderValueChanged;
            circle.ButtonAction += ButtonAction;
        }

        private void ButtonAction(EPtzCircleOperations action)
        {
            switch (action)
            {
                case EPtzCircleOperations.EZoomOutPress:
                    ZoomOut();
                    break;
                case EPtzCircleOperations.EZoomOutRelease:
                    ZoomRelease();
                    break;
                case EPtzCircleOperations.EPanDownLeftPress:
                    Pan(EPanDirection.DownLeft);
                    break;
                case EPtzCircleOperations.EPanDownLeftRelease:
                    Pan(EPanDirection.Release);
                    break;
                case EPtzCircleOperations.EPanDownPress:
                    Pan(EPanDirection.Down);
                    break;
                case EPtzCircleOperations.EPanDownRelease:
                    Pan(EPanDirection.Release);
                    break;
                case EPtzCircleOperations.EPanDownRightPress:
                    Pan(EPanDirection.DownRight);
                    break;
                case EPtzCircleOperations.EPanDownRightRelease:
                    Pan(EPanDirection.Release);
                    break;
                case EPtzCircleOperations.EPanLeftPress:
                    Pan(EPanDirection.Left);
                    break;
                case EPtzCircleOperations.EPanLeftRelease:
                    Pan(EPanDirection.Release);
                    break;
                case EPtzCircleOperations.EPanRightPress:
                    Pan(EPanDirection.Right);
                    break;
                case EPtzCircleOperations.EPanRightRelease:
                    Pan(EPanDirection.Release);
                    break;
                case EPtzCircleOperations.EZoomInPress:
                    ZoomIn();
                    break;
                case EPtzCircleOperations.EZoomInRelease:
                    ZoomRelease();
                    break;
                case EPtzCircleOperations.EPanUpLeftPress:
                    Pan(EPanDirection.UpLeft);
                    break;
                case EPtzCircleOperations.EPanUpLeftRelease:
                    Pan(EPanDirection.Release);
                    break;
                case EPtzCircleOperations.EPanUpPress:
                    Pan(EPanDirection.Up);
                    break;
                case EPtzCircleOperations.EPanUpRelease:
                    Pan(EPanDirection.Release);
                    break;
                case EPtzCircleOperations.EPanUpRightPress:
                    Pan(EPanDirection.UpRight);
                    break;
                case EPtzCircleOperations.EPanUpRightRelease:
                    Pan(EPanDirection.Release);
                    break;
                default:
                    return;
            }
        }

        private void ZoomRelease()
        {
            if (!Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                LastVideoComponent?.PtzManager?.ZoomRelease();
            }
        }

        private void ZoomIn()
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                LastVideoComponent?.PtzManager?.MaxZoomAsync(true);
            }
            else
            {
                if (LastVideoComponent?.PtzManager != null)
                {
                    LastVideoComponent.PtzManager.ZoomSpeed = ZoomSpeed;
                    LastVideoComponent.PtzManager?.ZoomCamera(EPtzCommand.ZoomIn);
                }
            }
        }

        private void Pan(EPanDirection dir, int speed = -1)
        {
            if (LastVideoComponent?.PtzManager == null || !LastVideoComponent.IsLivePlayback)
            {
                return;
            }

            if (speed == -1)
            {
                speed = PanSpeed;
            }

            LastVideoComponent.PtzManager.PanSpeed = speed;

            switch (dir)
            {
                case EPanDirection.Up:
                    LastVideoComponent?.PtzManager?.PanCamera(EPtzCommand.PanUp);
                    break;
                case EPanDirection.Down:
                    LastVideoComponent?.PtzManager?.PanCamera(EPtzCommand.PanDown);
                    break;
                case EPanDirection.Left:
                    LastVideoComponent?.PtzManager?.PanCamera(EPtzCommand.PanLeft);
                    break;
                case EPanDirection.Right:
                    LastVideoComponent?.PtzManager?.PanCamera(EPtzCommand.PanRight);
                    break;
                case EPanDirection.UpRight:
                    LastVideoComponent?.PtzManager?.PanCamera(EPtzCommand.PanUpRight);
                    break;
                case EPanDirection.UpLeft:
                    LastVideoComponent?.PtzManager?.PanCamera(EPtzCommand.PanUpLeft);
                    break;
                case EPanDirection.DownLeft:
                    LastVideoComponent?.PtzManager?.PanCamera(EPtzCommand.PanDownLeft);
                    break;
                case EPanDirection.DownRight:
                    LastVideoComponent?.PtzManager?.PanCamera(EPtzCommand.PanDownRight);
                    break;
                case EPanDirection.Release:
                    LastVideoComponent?.PtzManager?.PanRelease();
                    break;
            }
        }

        private void ZoomOut()
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                LastVideoComponent?.PtzManager?.MaxZoomAsync(false);
            }
            else
            {
                if (LastVideoComponent?.PtzManager != null)
                {
                    LastVideoComponent.PtzManager.ZoomSpeed = ZoomSpeed;
                    LastVideoComponent?.PtzManager?.ZoomCamera(EPtzCommand.ZoomOut);
                }
            }
        }

        private void ZoomSliderValueChanged(double speed)
        {
            ZoomSpeed = (int) speed;
            PanSpeed = (int) speed;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    internal enum EPanDirection
    {
        Up,
        Down,
        Left,
        Right,
        UpRight,
        UpLeft,
        DownLeft,
        DownRight,
        Release
    }
}