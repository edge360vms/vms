﻿using System.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring.Pane.Ptz
{
    /// <summary>
    ///     Interaction logic for MonitorPanePtzControl.xaml
    /// </summary>
    public partial class MonitorPanePtzControl : UserControl, IView
    {
        public MonitorPanePtzControl()
        {
            InitializeComponent();
            DataContext = new MonitorPanePtzViewModel(this);
        }
    }
}