﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring.Pane.Ptz
{
    public enum EPtzCircleOperations
    {
        EZoomOutPress,
        EZoomOutRelease,
        EPanDownLeftPress,
        EPanDownLeftRelease,
        EPanDownPress,
        EPanDownRelease,
        EPanDownRightPress,
        EPanDownRightRelease,
        EPanLeftPress,
        EPanLeftRelease,
        EPanRightPress,
        EPanRightRelease,
        EZoomInPress,
        EZoomInRelease,
        EPanUpLeftPress,
        EPanUpLeftRelease,
        EPanUpPress,
        EPanUpRelease,
        EPanUpRightPress,
        EPanUpRightRelease,
    }

    /// <summary>
    ///     Interaction logic for PtzCircleControl.xaml
    /// </summary>
    public partial class PtzCircleControl : UserControl, IView
    {
        public Action<EPtzCircleOperations> ButtonAction;

        public Action<double> ZoomSliderValueChanged;

        public PtzCircleControl()
        {
            InitializeComponent();
            RootGrid.VerticalAlignment = VerticalAlignment.Bottom;
            RootGrid.HorizontalAlignment = HorizontalAlignment.Right;
            RootGrid.AddHandler(MouseUpEvent, new MouseButtonEventHandler(DoMouseUpEvent), true);
            RootGrid.AddHandler(MouseLeftButtonUpEvent, new MouseButtonEventHandler(DoMouseUpEvent), true);
            RootGrid.AddHandler(PreviewMouseUpEvent, new MouseButtonEventHandler(DoMouseUpEvent), true);
            RootGrid.AddHandler(PreviewMouseLeftButtonUpEvent, new MouseButtonEventHandler(DoMouseUpEvent),
                true);

            TopMiddle.AddHandler(PreviewMouseLeftButtonUpEvent, new MouseButtonEventHandler(TopMiddle_MouseUp),
                true);
            TopLeft.AddHandler(PreviewMouseLeftButtonUpEvent, new MouseButtonEventHandler(TopLeft_MouseUp),
                true);
            TopRight.AddHandler(PreviewMouseLeftButtonUpEvent, new MouseButtonEventHandler(TopRight_MouseUp),
                true);
            TopCenter.AddHandler(PreviewMouseLeftButtonUpEvent, new MouseButtonEventHandler(TopCenter_MouseUp),
                true);
            BottomMiddle.AddHandler(PreviewMouseLeftButtonUpEvent, new MouseButtonEventHandler(BottomMiddle_MouseUp),
                true);
            BottomCenter.AddHandler(PreviewMouseLeftButtonUpEvent, new MouseButtonEventHandler(BottomCenter_MouseUp),
                true);
            BottomRight.AddHandler(PreviewMouseLeftButtonUpEvent, new MouseButtonEventHandler(BottomRight_MouseUp),
                true);
            LeftMiddle.AddHandler(PreviewMouseLeftButtonUpEvent, new MouseButtonEventHandler(LeftMiddle_MouseUp),
                true);
            RightMiddle.AddHandler(PreviewMouseLeftButtonUpEvent, new MouseButtonEventHandler(RightMiddle_MouseUp),
                true);
            BottomLeft.AddHandler(PreviewMouseLeftButtonUpEvent, new MouseButtonEventHandler(BottomLeft_MouseUp),
                true);

            ZoomSlider.ValueChanged += ZoomValueChanged;
        }

        private void ZoomValueChanged(double speed)
        {
            ZoomSliderValueChanged?.Invoke(speed);
        }

        private void DoMouseUpEvent(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            ((UIElement) sender)?.ReleaseMouseCapture();
        }

        private void TopMiddle_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            TopMiddle?.ReleaseMouseCapture();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanUpRelease);
        }

        private void BottomMiddle_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            BottomMiddle?.ReleaseMouseCapture();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanDownRelease);
        }

        private void LeftMiddle_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            LeftMiddle?.ReleaseMouseCapture();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanLeftRelease);
        }

        private void RightMiddle_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            RightMiddle?.ReleaseMouseCapture();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanRightRelease);
        }

        private void BottomLeft_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            BottomLeft?.ReleaseMouseCapture();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanDownLeftRelease);
        }

        private void TopLeft_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            TopLeft?.ReleaseMouseCapture();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanUpLeftRelease);
        }

        private void TopRight_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            TopRight?.ReleaseMouseCapture();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanUpRightRelease);
        }

        private void BottomRight_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            BottomRight?.ReleaseMouseCapture();

            ButtonAction?.Invoke(EPtzCircleOperations.EPanDownRightRelease);
        }

        private void TopCenter_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;

            TopCenter?.ReleaseMouseCapture();
            ButtonAction?.Invoke(EPtzCircleOperations.EZoomInRelease);
        }

        private void BottomCenter_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            BottomCenter?.ReleaseMouseCapture();
            ButtonAction?.Invoke(EPtzCircleOperations.EZoomOutRelease);
        }

        private void TopMiddle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            TopMiddle?.CaptureMouse();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanUpPress);
        }

        private void BottomRight_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            BottomRight?.CaptureMouse();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanDownRightPress);
        }

        private void TopRight_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            TopRight?.CaptureMouse();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanUpRightPress);
        }

        private void BottomLeft_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            BottomLeft?.CaptureMouse();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanDownLeftPress);
        }

        private void BottomMiddle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            BottomMiddle?.CaptureMouse();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanDownPress);
        }

        private void LeftMiddle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            LeftMiddle?.CaptureMouse();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanLeftPress);
        }

        private void RightMiddle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            RightMiddle?.CaptureMouse();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanRightPress);
        }

        private void TopCenter_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            TopCenter?.CaptureMouse();
            ButtonAction?.Invoke(EPtzCircleOperations.EZoomInPress);
        }

        private void BottomCenter_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            BottomCenter?.CaptureMouse();
            ButtonAction?.Invoke(EPtzCircleOperations.EZoomOutPress);
        }

        private void TopLeft_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton != MouseButton.Left)
            {
                return;
            }

            e.Handled = true;
            TopLeft?.CaptureMouse();
            ButtonAction?.Invoke(EPtzCircleOperations.EPanUpLeftPress);
        }
    }
}