﻿using System;
using System.Linq;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Common.Enums.Camera;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Interfaces.Video;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring.Pane.Ptz
{
    public class MonitorPanePtzViewModel : ViewModelBase
    {
        private readonly MonitorPanePtzControl _view;
        private RelayCommand _gotoPresetCommand;

        public RelayCommand GotoPresetCommand => _gotoPresetCommand ??= new RelayCommand(o => GotoPreset(o),
            o => CanGotoPreset());

        private EdgeVideoControl LastComponent
        {
            get
            {
                var tileGridControl = _view?.GetVisualParent<MonitoringView>()?.TileGridControl;
                var lastControl = tileGridControl?.Model?.SelectedComponents.LastOrDefault() as EdgeVideoControl;
                if (lastControl == null)
                {
                    tileGridControl?.Model?.SelectedComponents?.Add(tileGridControl.Model.Components
                        .OfType<EdgeVideoControl>()
                        .FirstOrDefault(o =>
                            o != null && (o.IsFullscreen || o.TileColumn == 0 && o.TileRow == 0)));
                }

                return lastControl;
            }
        }

        public MonitorPanePtzViewModel(MonitorPanePtzControl view)
        {
            _view = view;
        }

        private void GotoPreset(object i)
        {
            try
            {
                if (int.TryParse((string) i, out var preset))
                {
                    var videoControl =
                        LastComponent;
                    if (videoControl?.ContentControl?.Content is IVideoComponent videoComponent)
                    {
                        videoComponent?.PtzManager?.SetPreset((EPtzPreset) preset);
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        private bool CanGotoPreset()
        {
            return true;
        }
    }
}