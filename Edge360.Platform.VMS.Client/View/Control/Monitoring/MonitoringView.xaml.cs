﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.View.Control.Monitoring.TileGrid;
using Edge360.Platform.VMS.Client.ViewModel.View;

namespace Edge360.Platform.VMS.Client.View.Control.Monitoring
{
    /// <summary>
    ///     Interaction logic for MonitoringView.xaml
    /// </summary>
    public partial class MonitoringView : UserControl, IView
    {
        public static readonly DependencyProperty TileGridProperty = DependencyProperty.Register(
            "TileGrid", typeof(TileGridView), typeof(MonitoringView),
            new PropertyMetadata(default(TileGridView)));

        public static readonly DependencyProperty LayoutSizeProperty = DependencyProperty.Register("LayoutSize", typeof(Size), typeof(MonitoringView), new PropertyMetadata(default(Size)));

        public MonitoringViewModel Model => DataContext as MonitoringViewModel;

        public TileGridView TileGrid
        {
            get => (TileGridView) GetValue(TileGridProperty);
            set => SetValue(TileGridProperty, value);
        }

        public Size LayoutSize
        {
            get { return (Size) GetValue(LayoutSizeProperty); }
            set { SetValue(LayoutSizeProperty, value); }
        }


        public MonitoringView()
        {
            InitializeComponent();
           // DataContext = new MonitoringViewModel(this);
            Model.View = this;
            TileGrid = TileGridControl;
        }


        private void KeyboardHotkeyPopup_OnLostFocus(object sender, RoutedEventArgs e)
        {
            if (Model != null)
            {
                Model.IsHotkeyHelpVisible = false;
            }
        }

        private void KeyboardHotkeyPopup_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            Console.WriteLine("Hello");
        }
    }
}