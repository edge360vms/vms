﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.ItemModel.Stream;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.StreamingProfile;
using MahApps.Metro.Controls;
using Prism.Commands;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Data.DataForm;
using static Edge360.Platform.VMS.Communication.CommunicationManager;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Edge360.Platform.VMS.Client.View.Control.Management.Streaming
{
    /// <summary>
    ///     Interaction logic for StreamProfilesView.xaml
    /// </summary>
    public partial class StreamProfilesView : UserControl
    {
        public StreamProfilesViewModel Model => DataContext as StreamProfilesViewModel;

        public static readonly DependencyProperty ViewSelectedCameraProperty = DependencyProperty.Register(
            "ViewSelectedCamera", typeof(CameraItemModel), typeof(StreamProfilesView), new PropertyMetadata(default(CameraItemModel), PropertyChangedCallback));

        private RadDataForm _radDataForm;

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is StreamProfilesView obj)
            {
                obj.Model.SelectedCamera = obj.ViewSelectedCamera;
            }
        }

        public CameraItemModel ViewSelectedCamera
        {
            get
            {
                return (CameraItemModel) GetValue(ViewSelectedCameraProperty);
            }
            set
            {
                SetValue(ViewSelectedCameraProperty, value);
            }
        }
         
        public StreamProfilesView()
        {
            InitializeComponent();

            //try
            //{
            //    var binding = new Binding("SelectedCamera") { Mode = BindingMode.TwoWay };
            //    this.SetBinding(ViewSelectedCameraProperty, binding);
            //}
            //catch (Exception e)
            //{

            //}
        }

        private void RadDataForm_OnEditEnded(object sender, EditEndedEventArgs e)
        {
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is RadToggleButton button)
                {
                    if (!Model.SelectedStream.IsEditable)
                    {
                        var form = _radDataForm ?? button.TryFindParent<RadDataForm>();
                        form.CancelEdit();
                     
                    }
                }
            }
            catch (Exception exception)
            {
                
            }
        }

        private void RadDataForm_OnLoaded(object sender, RoutedEventArgs e)
        {
            _radDataForm = sender as RadDataForm;
        }

        private void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var form = _radDataForm;
                form.CommitEdit();
                
                Model.SelectedStream.EditCommand.Execute();
            }
            catch (Exception exception)
            {
                
            }
        }
    }

    public class StreamProfilesViewModel : INotifyPropertyChanged
    {
        private DelegateCommand<object> _addStreamProfileCommand;
        private DelegateCommand<StreamProfileItemModel> _deleteStreamProfileCommand;
        private OnvifProfileItemManager _onvifMgr;
        private DelegateCommand _refreshStreamProfilesCommand;
        private CameraItemModel _selectedCamera;
        private BaseAdapterCameraProfileItemModel _selectedOnvifProfile;
        private ServerItemModel _selectedServer;
        private StreamProfileItemModel _selectedStream;
        private object _selectedTreeItem;
        private ZoneItemModel _selectedZone;
        private StreamProfileItemManager _streamProfileItemManager;
        private ObservableCollection<ZoneItemModel> _zones = new ObservableCollection<ZoneItemModel>();
        private bool _isRefreshingStreams;
        private BaseAdapterCameraProfileItemModel _selectedNewOnvifProfile;

        public DelegateCommand<object> AddStreamProfileCommand =>
            _addStreamProfileCommand ??=
                new DelegateCommand<object>(o =>  AddStreamProfile(o), (o) => SelectedNewOnvifProfile != null && SelectedCamera != null).ObservesProperty(() => SelectedCamera).ObservesProperty(() => SelectedNewOnvifProfile);

        public DelegateCommand<StreamProfileItemModel> DeleteStreamProfileCommand => _deleteStreamProfileCommand ??=
            new DelegateCommand<StreamProfileItemModel>(DeleteStreamProfile);

        public OnvifProfileItemManager OnvifMgr
        {
            get => _onvifMgr;
            set
            {
                if (Equals(value, _onvifMgr))
                {
                    return;
                }

                _onvifMgr = value;
                OnPropertyChanged();
            }
        }

        public CollectionViewSource OnvifProfileCollectionViewSource { get; set; }

        public DelegateCommand RefreshStreamProfilesCommand =>
            _refreshStreamProfilesCommand ??=
                new DelegateCommand(() => RefreshStreamProfiles(true), () => { return SelectedCamera != null; }).ObservesProperty(
                    () => SelectedCamera);

        public CameraItemModel SelectedCamera
        {
            get => _selectedCamera;
            set
            {
                if (Equals(value, _selectedCamera))
                {
                    return;
                }
               
                _selectedCamera = value;
                OnPropertyChanged();

                RefreshStreamProfiles(StreamProfileCollectionViewSource.View.IsEmpty);
                //OnvifProfileCollectionViewSource.View.Refresh();
                //StreamProfileCollectionViewSource.View.Refresh();
            }
        }

        public BaseAdapterCameraProfileItemModel SelectedOnvifProfile
        {
            get => _selectedOnvifProfile;
            set
            {
                if (Equals(value, _selectedOnvifProfile))
                {
                    return;
                }

                _selectedOnvifProfile = value;
                OnPropertyChanged();
            }
        }

        public ServerItemModel SelectedServer
        {
            get => _selectedServer;
            set
            {
                if (Equals(value, _selectedServer))
                {
                    return;
                }

                _selectedServer = value;
                OnPropertyChanged();
            }
        }

        public StreamProfileItemModel SelectedStream
        {
            get => _selectedStream;
            set
            {
                if (Equals(value, _selectedStream))
                {
                    return;
                }

                _selectedStream = value;
                OnPropertyChanged();
            }
        }

        public object SelectedTreeItem
        {
            get => _selectedTreeItem;
            set
            {
                if (Equals(value, _selectedTreeItem))
                {
                    return;
                }
                var obj = value as dynamic;
                if (obj?.Value is ServerItemModel serverValue)
                {
                    SelectedServer = serverValue;
                } else
                if (value is CameraItemModel cam)
                {
                    SelectedCamera = cam;
                }
                else if (value is ServerItemModel server)
                {
                    SelectedServer = server;
                }
                else if (value is ZoneItemModel zone)
                {
                    SelectedZone = zone;
                }

                _selectedTreeItem = value;
                OnPropertyChanged();
            }
        }

        public ZoneItemModel SelectedZone
        {
            get => _selectedZone;
            set
            {
                if (Equals(value, _selectedZone))
                {
                    return;
                }

                _selectedZone = value;
                OnPropertyChanged();
            }
        }

        public CollectionViewSource StreamProfileCollectionViewSource { get; set; }

        public StreamProfileItemManager StreamProfileItemManager
        {
            get => _streamProfileItemManager;
            set
            {
                if (Equals(value, _streamProfileItemManager))
                {
                    return;
                }

                _streamProfileItemManager = value;
                OnPropertyChanged();
            }
        }

        //public ObservableCollection<ZoneItemModel> Zones
        //{
        //    get => _zones;
        //    set
        //    {
        //        if (Equals(value, _zones))
        //        {
        //            return;
        //        }

        //        _zones = value;
        //        OnPropertyChanged();
        //    }
        //}

        public bool IsRefreshingStreams
        {
            get => _isRefreshingStreams;
            set
            {
                if (value == _isRefreshingStreams) return;
                _isRefreshingStreams = value;
                OnPropertyChanged();
            }
        }

        public BaseAdapterCameraProfileItemModel SelectedNewOnvifProfile
        {
            get => _selectedNewOnvifProfile;
            set
            {
                if (Equals(value, _selectedNewOnvifProfile)) return;
                _selectedNewOnvifProfile = value;
                OnPropertyChanged();
            }
        }

        public StreamProfilesViewModel(StreamProfileItemManager streamProfileItemManager = null,
            OnvifProfileItemManager onvifProfileItemManager = null)
        {
            OnvifMgr = onvifProfileItemManager;
            if (OnvifMgr != null)
            {
                OnvifProfileCollectionViewSource = new CollectionViewSource
                {
                    Source = OnvifMgr.ItemCollection
                };
                OnvifProfileCollectionViewSource.Filter += OnvifProfileCollectionViewSourceOnFilter;
            }

            StreamProfileItemManager = streamProfileItemManager;
            if (StreamProfileItemManager != null)
            {
                StreamProfileCollectionViewSource = new CollectionViewSource
                {
                    Source = StreamProfileItemManager.ItemCollection
                };
                StreamProfileCollectionViewSource.Filter += StreamProfileCollectionViewSourceOnFilter;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private async void RefreshStreamProfiles(bool ignoreCache = false)
        {
            //Trace.WriteLine($"Refresh Stream Profiles");
            if (!IsRefreshingStreams)
            {
                await Task.Run(async() =>
                {
                    IsRefreshingStreams = true;
                    try
                    {
                        var previousStream = SelectedStream;
                        var previousOnvif = SelectedOnvifProfile;

                        var itemManagerRequestParameters = new ItemManagerRequestParameters()
                        {
                            AcceptedCacheDuration = ignoreCache ? TimeSpan.Zero : TimeSpan.FromMinutes(5),
                            CacheDuration = TimeSpan.FromMinutes(5)
                        };
                        await OnvifMgr.RefreshProfiles(itemManagerRequestParameters,SelectedCamera).ConfigureAwait(false);
                        await StreamProfileItemManager.RefreshProfiles(SelectedCamera,
                            ignoreCache
                                ? new ItemManagerRequestParameters() {AcceptedCacheDuration = TimeSpan.Zero}
                                : null).ConfigureAwait(false);
                        // var collection = StreamProfileItemManager?.ItemCollection?.Values;

                        await App.Current.Dispatcher.InvokeAsync(async () =>
                        {
                            try
                            {
                                StreamProfileCollectionViewSource?.View?.Refresh();
                                OnvifProfileCollectionViewSource?.View?.Refresh();
                                await Task.Delay(100);
                                if (previousStream != null)
                                {
                                    var streamProfileItemModel = StreamProfileItemManager.ItemCollection
                                        .FirstOrDefault(o => o.Value.Id == previousStream.Id).Value;
                                    SelectedStream =
                                        streamProfileItemModel;
                                }

                                if (previousOnvif != null)
                                {
                                    var selectedOnvifProfile = OnvifMgr.ItemCollection
                                        .FirstOrDefault(o => o.Value.Name == previousOnvif.Name).Value;
                                    SelectedOnvifProfile =
                                        selectedOnvifProfile;
                                }
                            }
                            catch (Exception e)
                            {
                                
                            }
                        });
                    }
                    catch (Exception e)
                    {
                    }
                    finally
                    {
                        IsRefreshingStreams = false;
                    }
                }).ConfigureAwait(false);
            }
        }

        private void DeleteStreamProfile(StreamProfileItemModel streamProfileItemModel)
        {
            if (SelectedCamera != null)
            {
                streamProfileItemModel.Delete();
            }
        }


        private async void AddStreamProfile(object o)
        {
            try
            {
                if (SelectedCamera != null && o is BaseAdapterCameraProfileItemModel streamProfile)
                {
                    //var h264Configuration = SelectedOnvifProfile.VideoEncoderConfiguration.H264;
                    var managerByZoneId = await ZoneItemManager.GetManagerByZoneIdConnectIfFail(SelectedCamera.ZoneId);
                    if (managerByZoneId != null)
                    {
                        var resp = await managerByZoneId.StreamProfileService.CreateStreamingProfile(
                            new CreateStreamingProfile
                            {
                                CameraId = SelectedCamera.Id,
                                DriverProfileId = $"{streamProfile.token}",
                                Format = "h264",
                                IsStatic = false,
                                Label =
                                    $"{streamProfile.Name}",
                                MaxConcurrentViewers = 5,
                                MaxViews = 5,
                                Password = SelectedCamera.Password,
                                ResolutionHeight = (int) streamProfile.Resolution.Height,
                                ResolutionWidth = (int) streamProfile.Resolution.Width,
                                RtspUrl = CameraItemModel.AddAuthUri(SelectedCamera.Username, SelectedCamera.Password,  streamProfile.StreamUri),
                                Type = "Onvif",
                                Username = SelectedCamera.Username
                            });
                        if (resp != null && resp.Id != Guid.Empty)
                        {
                            await StreamProfileItemManager.RefreshProfile(SelectedCamera.ZoneId, resp.Id);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnvifProfileCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is KeyValuePair<Tuple<Guid, string>, BaseAdapterCameraProfileItemModel> model)
            {
                e.Accepted = SelectedCamera != null && model.Key.Item1 == SelectedCamera.Id;
            }
        }

        private void StreamProfileCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            if (e.Item is KeyValuePair<Tuple<Guid, Guid>, StreamProfileItemModel> model)
            {
                e.Accepted = SelectedCamera != null && model.Key.Item2 == SelectedCamera.Id;
            }
        }
    }
}