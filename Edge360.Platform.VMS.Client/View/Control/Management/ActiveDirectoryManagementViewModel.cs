using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Dialog;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.AdConfigZone;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using ServiceStack;
using Telerik.Windows.Controls;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    public class ActiveDirectoryManagementViewModel : INotifyPropertyChanged, IDisposable
    {
        public ActiveDirectoryManagementView View;
        private RelayCommand _addConfigCommand;

        private ObservableCollection<ADConfigItemModel> _configs = new ObservableCollection<ADConfigItemModel>();
        private ObservableCollection<ADDomainItemModel> _domains = new ObservableCollection<ADDomainItemModel>();
        private string _filterConfigText = string.Empty;

        private CollectionViewSource _filteredConfigs = new CollectionViewSource
            {SortDescriptions = {new SortDescription {PropertyName = "Urls"}}};

        private ObservableCollection<UserItemModel> _membersList = new ObservableCollection<UserItemModel>();
        private RelayCommand _refreshConfigsCommand;
        private ADConfigItemModel _selectedConfig;
        private ADDomainItemModel _selectedDomain;
        private ILog _log;
        private DelegateCommand _editConnectionCommand;

        public RelayCommand AddConfigCommand =>
            _addConfigCommand ??= new RelayCommand(p => AddConfig(), p => CanAddConfig());

        public ObservableCollection<ADConfigItemModel> Configs
        {
            get => _configs;
            set
            {
                if (_configs != value)
                {
                    _configs = value;
                    OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<ADDomainItemModel> Domains
        {
            get => _domains;
            set
            {
                if (_domains != value)
                {
                    _domains = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(SelectedDomains));
                }
            }
        }

        public string FilterConfigText
        {
            get => _filterConfigText;
            set
            {
                if (_filterConfigText != value)
                {
                    _filterConfigText = value;
                    OnPropertyChanged();
                    FilteredConfigs.View.Refresh();
                }
            }
        }

        public CollectionViewSource FilteredConfigs
        {
            get => _filteredConfigs;
            set
            {
                if (_filteredConfigs != value)
                {
                    _filteredConfigs = value;
                    OnPropertyChanged();
                }
            }
        }

        public DelegateCommand EditConnectionCommand => _editConnectionCommand ??= new DelegateCommand(EditConnection);

        private void EditConnection(object obj)
        {
            try
            {
                if (SelectedConfig != null)
                {
                    var window =
                        new EditADConfigDialog {HideMinimizeButton = true, HideMaximizeButton = true};
                    window.Model.DialogItem = new EditADConfigDialogItemModel
                    {
                       // Password = SelectedConfig.Password,
                        Username = SelectedConfig.Username,
                        Urls = SelectedConfig.Urls,
                        ConnectionStatus = SelectedConfig.ConnectionStatus,
                        ZoneId = SelectedConfig.ZoneId
                    };
                    var mgmt = View.ParentOfType<ManagementView>();
                    window.SetModalView(mgmt.DialogOverlayGrid.ModalContentPresenter);
                    window.Closed += async (sender, args) =>
                    {
                        try
                        {
                            if (window.Result &&
                                window.DialogItem is EditADConfigDialogItemModel dialogItem &&
                                !string.IsNullOrEmpty(dialogItem.Urls))
                            {
                                var resp = await GetManagerByZoneId().AdConfigZoneService.UpdateConfig(new UpdateAdConfigZone
                                {
                                    ActiveDirectoryId = SelectedConfig.ActiveDirectoryId,
                                    Password = dialogItem.Password,
                                    Urls = dialogItem.Urls,
                                    Username = dialogItem.Username,
                                    ZoneId = dialogItem.ZoneId
                                });
                                //var domains = dialogItem.Domains?.Split(' ');
                                //if (resp != null && domains != null)
                                //{
                                //    foreach (var dom in domains)
                                //    {
                                //        try
                                //        {
                                //            var result = await GetManagerByZoneId().AdDomainService
                                //                .CreateDomain(dom, SelectedConfig.ActiveDirectoryId.Value);
                                //            if (result)
                                //            {
                                //                Console.WriteLine($"Domain Created! {dom}");
                                //            }
                                //        }
                                //        catch (Exception e)
                                //        {
                                    
                                //        }
                                //    }
                                //}
                            }
                        }
                        catch (Exception e)
                        {
                            
                        }
                    };
                }
            }
            catch (Exception e)
            {
                
            }
        }

        public RelayCommand RefreshConfigsCommand =>
            _refreshConfigsCommand ??= new RelayCommand(async p => await RefreshConnections(), p => CanRefreshConfigs());


        public ADConfigItemModel SelectedConfig
        {
            get => _selectedConfig;
            set
            {
                if (_selectedConfig != value)
                {
                    _selectedConfig = value;
                    OnPropertyChanged();
                    UpdateConfigs();
                    OnPropertyChanged(nameof(SelectedDomains));
                }
            }
        }

        public ADDomainItemModel SelectedDomain
        {
            get => _selectedDomain;
            set
            {
                if (_selectedDomain != value)
                {
                    _selectedDomain = value;
                    OnPropertyChanged();
                }
            }
        }

        public List<ADDomainItemModel> SelectedDomains
        {
            get
            {
                var domains = new List<ADDomainItemModel>();
                if (SelectedConfig != null)
                {
                    domains.AddRange(Domains.Where(o => o.ActiveDirectoryId == SelectedConfig.ActiveDirectoryId));
                }

                return domains;
            }
        }

        

        public ActiveDirectoryManagementViewModel(ILog log = null)
        {
            _log = log;
            Dispatcher.CurrentDispatcher.InvokeAsync(async () =>
            {
                var binding = new Binding(nameof(Configs)) {Source = this, Mode = BindingMode.OneWay};
                BindingOperations.SetBinding(FilteredConfigs,
                    CollectionViewSource.SourceProperty,
                    binding);
                FilteredConfigs.Filter += FilteredConfigsOnFilter;
            }, DispatcherPriority.Background);

            try
            {
           //     HubManager.SignalRClientEvent += OnHubManagerOnSignalRClientEvent;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Initialize();
        }

        private async void OnHubManagerOnSignalRClientEvent(ESignalRClientEvent e, Guid zone, string data)
        {
            if (Equals(e, ESignalRClientEvent.AdDomainCreated))
            {
                HubManagerOnDomainCreated(zone, data);
            }
            else if (Equals(e, ESignalRClientEvent.AdDomainDeleted))
            {
                HubManagerOnDomainDeleted(zone, data);
            }
            else if (Equals(e, ESignalRClientEvent.AdConfigZoneCreated))
            {
                HubManagerOnADConfigCreated(zone, data);
            }
            else if (Equals(e, ESignalRClientEvent.AdConfigZoneDeleted))
            {
                HubManagerOnADConfigDeleted(zone, data);
            }
            else if (Equals(e, ESignalRClientEvent.AdConfigZoneEdited))
            {
                HubManagerOnADConfigEdited(zone, data);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<ADConfigItemModel> GetConfigs()
        {
            return Configs;
        }

        private bool CanRefreshConfigs()
        {
            return true;
        }

        private async void Initialize()
        {
            try
            {
                _ = Task.Run(async() =>
                {
                    var configLoaded = await LoadConfigs();
                    if (configLoaded)
                    {
                        SelectedConfig = Configs.FirstOrDefault();
                        await RefreshConnections();
                    }

                    LoadDomains();
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }

        }

        private async Task RefreshConnections()
        {
            _ = Task.Run(async () =>
            {
                foreach (var c in Configs)
                {
                    await c.TestConnection();
                }
            }).ConfigureAwait(false);
        }


        private void UpdateConfigs()
        {
            OnPropertyChanged(nameof(SelectedDomains));
        }

        private bool CanAddConfig()
        {
            return true;
        }

        private async Task<bool> LoadConfigs()
        {
            try
            {
                var configs = await GetManagerByZoneId().AdConfigZoneService.ListConfigs();

                if (configs != null)
                {
                    var list = configs.Select(g => g.ConvertTo<ADConfigItemModel>()).ToList();
                    Configs = new ObservableCollection<ADConfigItemModel>(list);
                    return true;
                }

            }
            catch (Exception e)
            {
                _log.Info(e);
            }
            return false;
        }

        private async void LoadDomains()
        {
            try
            {
                var domains = await GetManagerByZoneId().AdDomainService.ListDomains();

                if (domains != null)
                {
                    var list = domains.Select(g => g.ConvertTo<ADDomainItemModel>()).ToList();
                    Domains = new ObservableCollection<ADDomainItemModel>(list);
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        private void AddConfig()
        {
            try
            {
                var window =
                    new CreateAdConfigDialog {HideMinimizeButton = true, HideMaximizeButton = true};

                var mgmt = View.ParentOfType<ManagementView>();
                window.SetModalView(mgmt.DialogOverlayGrid.ModalContentPresenter);
                window.Closed += async (sender, args) =>
                {
                    if (window.Result &&
                        window.DialogItem is CreateAdConfigDialogItemModel dialogItem &&
                        !string.IsNullOrEmpty(dialogItem.Urls))
                    {
                        _ = Task.Run(async() =>
                        {
                            var resp = await GetManagerByZoneId().AdConfigZoneService.CreateConfig(
                                dialogItem.ZoneId ?? Guid.Empty,
                                dialogItem.Username,
                                dialogItem.Password,
                                dialogItem.Urls);
                            var domains = dialogItem.Domains?.Split(' ');
                            if (resp != null && domains != null)
                            {
                                foreach (var dom in domains)
                                {
                                    var result = await GetManagerByZoneId().AdDomainService
                                        .CreateDomain(dom, resp.Value);
                                    if (result)
                                    {
                                        Console.WriteLine($"Domain Created! {dom}");
                                    }
                                }
                            }
                        }).ConfigureAwait(false);
                    }
                };
            }
            catch (Exception e)
            {
                _log?.Error(e);
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void HubManagerOnADConfigEdited(Guid guid1, string id)
        {
            Console.WriteLine($"HubManagerOnADConfigEdited: {id}");
            if (!string.IsNullOrEmpty(id))
            {
                var guid = Guid.Parse(id);
                if (guid != Guid.Empty)
                {
                    var config = await GetManagerByZoneId().AdConfigZoneService
                        .GetConfig(Guid.Empty, guid);
                    if (config != null)
                    {
                        var oldConfig = Configs.FirstOrDefault(o => o.ActiveDirectoryId == guid);
                        _ = View.TryUiAsync(() =>
                        {
                            if (oldConfig != null)
                            {
                                Configs.Remove(oldConfig);
                            }

                            Configs.Add(config);
                            UpdateConfigs();
                        }, DispatcherPriority.Background).ConfigureAwait(false);
                    }
                }
            }
        }

        private void HubManagerOnADConfigDeleted(Guid guid1, string id)
        {
            try
            {
                Console.WriteLine($"HubManagerOnADConfigDeleted: {id}");
                if (!string.IsNullOrEmpty(id))
                {
                    var guid = Guid.Parse(id);
                    if (guid != Guid.Empty)
                    {
                        var config = Configs.FirstOrDefault(o => o.ActiveDirectoryId == guid);
                        if (config != null)
                        {
                            _ = View?.TryUiAsync(() =>
                            {
                                Configs.Remove(config);
                                UpdateConfigs();
                            }, DispatcherPriority.Background).ConfigureAwait(false);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _log?.Error(e);
            }
        }

        private async void HubManagerOnADConfigCreated(Guid guid1, string id)
        {
            try
            {
                Console.WriteLine($"HubManagerOnADConfigCreated: {id}");

                if (!string.IsNullOrEmpty(id))
                {
                    var guid = Guid.Parse(id);
                    if (guid != Guid.Empty)
                    {
                        var config = await GetManagerByZoneId().AdConfigZoneService
                            .GetConfig(Guid.Empty, guid);
                        if (config != null)
                        {
                            _ = View?.TryUiAsync(() =>
                            {
                                Configs.Add(config);
                                UpdateConfigs();
                            }, DispatcherPriority.Background).ConfigureAwait(false);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _log?.Error(e);
            }
        }

        private void HubManagerOnDomainDeleted(Guid guid, string id)
        {
            try
            {
                Console.WriteLine($"HubManagerOnDomainDeleted: {id}");
                if (!string.IsNullOrEmpty(id))
                {
                    var domain = Domains.FirstOrDefault(o => o.Domain == id);
                    if (domain != null)
                    {
                        _ = View.TryUiAsync(() =>
                        {
                            Domains.Remove(domain);
                            UpdateConfigs();
                        }, DispatcherPriority.Background).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception e)
            {
                _log?.Error(e);
            }
        }

        private async void HubManagerOnDomainCreated(Guid guid, string id)
        {
            try
            {
                Console.WriteLine($"HubManagerOnDomainCreated: {id}");
                if (!string.IsNullOrEmpty(id))
                {
                    var domain = await GetManagerByZoneId().AdDomainService.GetDomainByName(id);
                    var domItemModel = domain?.ConvertTo<ADDomainItemModel>();
                    if (domItemModel != null)
                    {
                        Domains.Add(domItemModel);
                        UpdateConfigs();
                    }
                }
            }
            catch (Exception e)
            {
                _log?.Error(e);
            }
        }

        private void FilteredConfigsOnFilter(object sender, FilterEventArgs e)
        {
            e.Accepted = e.Item is ADConfigItemModel config &&
                         FilterConfigText.PartialContain(config.Urls, config.Username);
        }

        public void Dispose()
        {
            HubManager.SignalRClientEvent -= OnHubManagerOnSignalRClientEvent;
        }
    }
}