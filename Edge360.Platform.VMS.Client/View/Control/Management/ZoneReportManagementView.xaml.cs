﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Controls;
using CefSharp.MinimalExample.Wpf.Behaviours;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication;
using log4net;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes.Charts;
using MigraDoc.DocumentObjectModel.Tables;
using Prism.Commands;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    /// <summary>
    ///     Interaction logic for VolumeManagementView.xaml
    /// </summary>
    public partial class ZoneReportManagementView : UserControl, IView, IDisposable
    {
        public ZoneReportManagementViewModel Model => DataContext as ZoneReportManagementViewModel;

        public ZoneReportManagementView()
        {
            InitializeComponent();
            Model.View = this;
            var downloadHandler = new DownloadHandler();
            Browser.DownloadHandler = downloadHandler;
        }

        public void Dispose()
        {
            Browser?.Dispose();
        }
    }

    public class ReportGenerator
    {
        public ReportGenerator(ILog log = null)
        {
        }

        public async Task<Document> CreateReport()
        {
            // Create a new MigraDoc document
            var document = new Document
            {
                Info =
                {
                    Title = $"Zone Report - {DateTimeOffset.UtcNow:u}",
                    Subject = "Edge36-VMS-Client",
                    Author = "Edge360"
                }
            };

            DefineStyles(document);
            DefineCover(document);
            await DefineTables(document);

            //  DefineCharts(document);
            //  DefineContentSection(document);
            //  DefineTableOfContents(document);
            // DefineParagraphs(document);

            return document;
        }

        /// <summary>
        ///     Defines the cover page.
        /// </summary>
        public void DefineCover(Document document)
        {
            var section = document.AddSection();

            var paragraph = section.AddParagraph();
            paragraph.Format.SpaceAfter = ".2cm";

            var image = section.AddImage("./resources/images/edgeLogo.png");
            image.Width = "3cm";
            image.Height = "3cm";
            paragraph = section.AddParagraph("Edge360 VMS Zone Management Report");
            paragraph.Format.Font.Size = 16;
            paragraph.Format.Font.Color = Colors.DarkRed;
            paragraph.Format.SpaceBefore = "8cm";
            paragraph.Format.SpaceAfter = "3cm";

            paragraph = section.AddParagraph("Report date: ");

            paragraph.AddDateField();
        }

        public async Task DefineTables(Document document)
        {
            var paragraph = document.LastSection.AddParagraph("Report Overview", "Heading1");
            paragraph.AddBookmark("Report");

            await GenerateCameraTable(document);
            //DemonstrateAlignment(document);
            //DemonstrateCellMerge(document);
        }

        public void DefineCharts(Document document)
        {
            var paragraph = document.LastSection.AddParagraph("Chart Overview", "Heading1");
            paragraph.AddBookmark("Charts");

            document.LastSection.AddParagraph("Sample Chart", "Heading2");

            var chart = new Chart();
            chart.Left = 0;

            chart.Width = Unit.FromCentimeter(16);
            chart.Height = Unit.FromCentimeter(12);
            var series = chart.SeriesCollection.AddSeries();
            series.ChartType = ChartType.Column2D;
            series.Add(1, 17, 45, 5, 3, 20, 11, 23, 8, 19);
            series.HasDataLabel = true;

            series = chart.SeriesCollection.AddSeries();
            series.ChartType = ChartType.Line;
            series.Add(41, 7, 5, 45, 13, 10, 21, 13, 18, 9);

            var xseries = chart.XValues.AddXSeries();
            xseries.Add("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N");

            chart.XAxis.MajorTickMark = TickMarkType.Outside;
            chart.XAxis.Title.Caption = "X-Axis";

            chart.YAxis.MajorTickMark = TickMarkType.Outside;
            chart.YAxis.HasMajorGridlines = true;

            chart.PlotArea.LineFormat.Color = Colors.DarkGray;
            chart.PlotArea.LineFormat.Width = 1;

            document.LastSection.Add(chart);
        }

        public async Task GenerateCameraTable(Document document)
        {
            //document.LastSection.AddParagraph("Camera Overview", "Heading2");

            var table = new Table {Borders = {Width = 0.75}};
            var column = table.AddColumn(Unit.FromCentimeter(2));
            column.Format.Alignment = ParagraphAlignment.Left;
            column.Format.Font.Size = ".22cm";
            var address = table.AddColumn(Unit.FromCentimeter(2));
            address.Format.Font.Size = ".2cm";
            var assetId = table.AddColumn(Unit.FromCentimeter(2));
            assetId.Format.Font.Size = ".2cm";
            var objectId = table.AddColumn(Unit.FromCentimeter(2));
            objectId.Format.Font.Size = ".2cm";
            var zoneId = table.AddColumn(Unit.FromCentimeter(2));
            zoneId.Format.Font.Size = ".2cm";
            var status = table.AddColumn(Unit.FromCentimeter(2));
            status.Format.Font.Size = ".2cm";
            var model = table.AddColumn(Unit.FromCentimeter(2));
            model.Format.Font.Size = ".2cm";
            var serialNumber = table.AddColumn(Unit.FromCentimeter(2));
            serialNumber.Format.Font.Size = ".2cm";
            var row = table.AddRow();
            row.Shading.Color = Colors.PaleGoldenrod;
            row.Cells[0].AddParagraph("Label");
            row.Cells[1].AddParagraph("Address");
            row.Cells[2].AddParagraph("AssetId");
            row.Cells[3].AddParagraph("ObjectId");
            row.Cells[4].AddParagraph("ZoneId");
            row.Cells[5].AddParagraph("Status");
            row.Cells[6].AddParagraph("Model");
            row.Cells[7].AddParagraph("SerialNumber");
            var rowCount = 0;
            var itemCollectionValues =
                CameraItemModelManager.Manager.ItemCollection.Values.OrderBy(o => o?.Label).ToList();

            await Task.WhenAll(itemCollectionValues.Select(async camera =>
            {
                try
                {
                    if (camera.DeviceInformation == null)
                    {
                        var getCameraDetailsResponse =
                            await CameraItemModelManager.Manager.GetCameraDetails(camera.ZoneId, camera.Id);
                        camera.DeviceInformation = getCameraDetailsResponse.CameraDetails;
                    }
                }
                catch (Exception e)
                {
                    //_zoneReportManagementViewModel._log?.Info($"Error Getting Details {e.GetType()} {e.Message}");
                }
            }));

            foreach (var c in itemCollectionValues.Where(o => o != null))
            {
                try
                {
                    row = table.AddRow();
                    rowCount++;
                    row.Cells[0].AddParagraph(c.Label ?? "");
                    row.Cells[1].AddParagraph(c.Address ?? "");
                    row.Cells[2].AddParagraph(c.AssetId ?? "");
                    row.Cells[3].AddParagraph(c.Id.ToString() ?? "");
                    row.Cells[4].AddParagraph(c.ZoneId.ToString() ?? "");
                    row.Cells[5].AddParagraph(c.Status ?? "Enabled");
                    row.Cells[6].AddParagraph(c.DeviceInformation?.Model ?? "");
                    row.Cells[7].AddParagraph(c.DeviceInformation?.CameraName ?? "");
                }
                catch (Exception e)
                {
                }
            }
            //foreach (var c in SelectedServer.Archiver.ArchivingProfiles)
            //{
            //    row = table.AddRow();
            //    cell = row.Cells[0];
            //    cell.AddParagraph(c.Label);
            //    cell = row.Cells[1];
            //    cell.AddParagraph("test");

            //    row = table.AddRow();
            //    cell = row.Cells[0];
            //    cell.AddParagraph("2");
            //    cell = row.Cells[1];
            //    cell.AddParagraph("test2");

            //}
            //row = table.AddRow();
            //cell = row.Cells[0];
            //cell.AddParagraph("1");
            //cell = row.Cells[1];
            //cell.AddParagraph("test");

            //row = table.AddRow();
            //cell = row.Cells[0];
            //cell.AddParagraph("2");
            //cell = row.Cells[1];
            //cell.AddParagraph("test2");

            table.SetEdge(0, 0, 2, rowCount, Edge.Box, BorderStyle.Single, 1, Colors.Black);

            document.LastSection.Add(table);
        }

        public void DemonstrateCellMerge(Document document)
        {
            document.LastSection.AddParagraph("Cell Merge", "Heading2");

            var table = document.LastSection.AddTable();
            table.Borders.Visible = true;
            table.TopPadding = 5;
            table.BottomPadding = 5;

            var column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Left;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn();
            column.Format.Alignment = ParagraphAlignment.Right;

            table.Rows.Height = 35;

            var row = table.AddRow();
            row.Cells[0].AddParagraph("Merge Right");
            row.Cells[0].MergeRight = 1;

            row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[0].MergeDown = 1;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[0].AddParagraph("Merge Down");

            table.AddRow();
        }

        public static void DefineParagraphs(Document document)
        {
            var paragraph = document.LastSection.AddParagraph("Paragraph Layout Overview", "Heading1");
            paragraph.AddBookmark("Paragraphs");

            DemonstrateAlignment(document);
            DemonstrateIndent(document);
            DemonstrateFormattedText(document);
            DemonstrateBordersAndShading(document);
        }

        private static void DemonstrateAlignment(Document document)
        {
            document.LastSection.AddParagraph("Alignment", "Heading2");

            document.LastSection.AddParagraph("Left Aligned", "Heading3");

            var paragraph = document.LastSection.AddParagraph();
            paragraph.Format.Alignment = ParagraphAlignment.Left;
            var text = "hello";
            paragraph.AddText(text);

            document.LastSection.AddParagraph("Right Aligned", "Heading3");

            paragraph = document.LastSection.AddParagraph();
            paragraph.Format.Alignment = ParagraphAlignment.Right;
            paragraph.AddText(text);

            document.LastSection.AddParagraph("Centered", "Heading3");

            paragraph = document.LastSection.AddParagraph();
            paragraph.Format.Alignment = ParagraphAlignment.Center;
            paragraph.AddText(text);

            document.LastSection.AddParagraph("Justified", "Heading3");

            paragraph = document.LastSection.AddParagraph();
            paragraph.Format.Alignment = ParagraphAlignment.Justify;
            paragraph.AddText("Hello1");
        }

        private static void DemonstrateIndent(Document document)
        {
            document.LastSection.AddParagraph("Indent", "Heading2");

            document.LastSection.AddParagraph("Left Indent", "Heading3");

            var paragraph = document.LastSection.AddParagraph();
            paragraph.Format.LeftIndent = "2cm";
            var text = "hello";
            paragraph.AddText(text);

            document.LastSection.AddParagraph("Right Indent", "Heading3");

            paragraph = document.LastSection.AddParagraph();
            paragraph.Format.RightIndent = "1in";
            paragraph.AddText(text);

            document.LastSection.AddParagraph("First Line Indent", "Heading3");

            paragraph = document.LastSection.AddParagraph();
            paragraph.Format.FirstLineIndent = "12mm";
            paragraph.AddText(text);

            document.LastSection.AddParagraph("First Line Negative Indent", "Heading3");

            paragraph = document.LastSection.AddParagraph();
            paragraph.Format.LeftIndent = "1.5cm";
            paragraph.Format.FirstLineIndent = "-1.5cm";
            paragraph.AddText(text);
        }

        private static void DemonstrateFormattedText(Document document)
        {
            document.LastSection.AddParagraph("Formatted Text", "Heading2");

            //document.LastSection.AddParagraph("Left Aligned", "Heading3");

            var paragraph = document.LastSection.AddParagraph();
            paragraph.AddText("Text can be formatted ");
            paragraph.AddFormattedText("bold", TextFormat.Bold);
            paragraph.AddText(", ");
            paragraph.AddFormattedText("italic", TextFormat.Italic);
            paragraph.AddText(", or ");
            paragraph.AddFormattedText("bold & italic", TextFormat.Bold | TextFormat.Italic);
            paragraph.AddText(".");
            paragraph.AddLineBreak();
            paragraph.AddText("You can set the ");
            var formattedText = paragraph.AddFormattedText("size ");
            formattedText.Size = 15;
            paragraph.AddText("the ");
            formattedText = paragraph.AddFormattedText("color ");
            formattedText.Color = Colors.Firebrick;
            paragraph.AddText("the ");
            formattedText = paragraph.AddFormattedText("font", new Font("Verdana"));
            paragraph.AddText(".");
            paragraph.AddLineBreak();
            paragraph.AddText("You can set the ");
            formattedText = paragraph.AddFormattedText("subscript");
            formattedText.Subscript = true;
            paragraph.AddText(" or ");
            formattedText = paragraph.AddFormattedText("superscript");
            formattedText.Superscript = true;
            paragraph.AddText(".");
        }

        private static void DemonstrateBordersAndShading(Document document)
        {
            document.LastSection.AddPageBreak();
            document.LastSection.AddParagraph("Borders and Shading", "Heading2");

            document.LastSection.AddParagraph("Border around Paragraph", "Heading3");

            var paragraph = document.LastSection.AddParagraph();
            paragraph.Format.Borders.Width = 2.5;
            paragraph.Format.Borders.Color = Colors.Navy;
            paragraph.Format.Borders.Distance = 3;
            paragraph.AddText("Hello");

            document.LastSection.AddParagraph("Shading", "Heading3");

            paragraph = document.LastSection.AddParagraph();
            paragraph.Format.Shading.Color = Colors.LightCoral;
            paragraph.AddText("Test");

            document.LastSection.AddParagraph("Borders & Shading", "Heading3");

            paragraph = document.LastSection.AddParagraph();
            paragraph.Style = "TextBox";
            paragraph.AddText("123");
        }

        /// <summary>
        ///     Defines page setup, headers, and footers.
        /// </summary>
        private static void DefineContentSection(Document document)
        {
            var section = document.AddSection();
            section.PageSetup.OddAndEvenPagesHeaderFooter = true;
            section.PageSetup.StartingNumber = 1;

            var header = section.Headers.Primary;
            header.AddParagraph("\tOdd Page Header");

            header = section.Headers.EvenPage;
            header.AddParagraph("Even Page Header");

            // Create a paragraph with centered page number. See definition of style "Footer".
            var paragraph = new Paragraph();
            paragraph.AddTab();
            paragraph.AddPageField();

            // Add paragraph to footer for odd pages.
            section.Footers.Primary.Add(paragraph);
            // Add clone of paragraph to footer for odd pages. Cloning is necessary because an object must
            // not belong to more than one other object. If you forget cloning an exception is thrown.
            section.Footers.EvenPage.Add(paragraph.Clone());
        }

        /// <summary>
        ///     Defines the cover page.
        /// </summary>
        public static void DefineTableOfContents(Document document)
        {
            var section = document.LastSection;

            section.AddPageBreak();
            var paragraph = section.AddParagraph("Table of Contents");
            paragraph.Format.Font.Size = 14;
            paragraph.Format.Font.Bold = true;
            paragraph.Format.SpaceAfter = 24;
            paragraph.Format.OutlineLevel = OutlineLevel.Level1;

            paragraph = section.AddParagraph();
            paragraph.Style = "TOC";
            var hyperlink = paragraph.AddHyperlink("Paragraphs");
            hyperlink.AddText("Paragraphs\t");
            hyperlink.AddPageRefField("Paragraphs");

            paragraph = section.AddParagraph();
            paragraph.Style = "TOC";
            hyperlink = paragraph.AddHyperlink("Tables");
            hyperlink.AddText("Tables\t");
            hyperlink.AddPageRefField("Tables");

            paragraph = section.AddParagraph();
            paragraph.Style = "TOC";
            hyperlink = paragraph.AddHyperlink("Charts");
            hyperlink.AddText("Charts\t");
            hyperlink.AddPageRefField("Charts");
        }

        /// <summary>
        ///     Defines the styles used in the document.
        /// </summary>
        public static void DefineStyles(Document document)
        {
            // Get the predefined style Normal.
            var style = document.Styles["Normal"];
            // Because all styles are derived from Normal, the next line changes the 
            // font of the whole document. Or, more exactly, it changes the font of
            // all styles and paragraphs that do not redefine the font.
            style.Font.Name = "Times New Roman";

            // Heading1 to Heading9 are predefined styles with an outline level. An outline level
            // other than OutlineLevel.BodyText automatically creates the outline (or bookmarks) 
            // in PDF.

            style = document.Styles["Heading1"];
            style.Font.Name = "Tahoma";
            style.Font.Size = 14;
            style.Font.Bold = true;
            style.Font.Color = Colors.DarkBlue;
            style.ParagraphFormat.PageBreakBefore = true;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading2"];
            style.Font.Size = 12;
            style.Font.Bold = true;
            style.ParagraphFormat.PageBreakBefore = false;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading3"];
            style.Font.Size = 10;
            style.Font.Bold = true;
            style.Font.Italic = true;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 3;

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);

            // Create a new style called TextBox based on style Normal
            style = document.Styles.AddStyle("TextBox", "Normal");
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            style.ParagraphFormat.Borders.Width = 2.5;
            style.ParagraphFormat.Borders.Distance = "3pt";
            style.ParagraphFormat.Shading.Color = Colors.SkyBlue;

            // Create a new style called TOC based on style Normal
            style = document.Styles.AddStyle("TOC", "Normal");
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right, TabLeader.Dots);
            style.ParagraphFormat.Font.Color = Colors.Blue;
        }
    }

    public class ZoneReportManagementViewModel : ZoneManagementViewModel, INotifyPropertyChanged
    {
        private readonly ReportGenerator _reportGenerator;
        private ReportExportItemModel _exportType;

        private string _filterZonesText = string.Empty;
        private bool _isGeneratingReport;
        private ILog _log;
        private EdgeVideoControl _popoutVideoPlayer;
        private RelayCommand _refreshZonesCommand;

        private DelegateCommand<object> _reportActionCommand;
        private ServerItemModel _selectedServer;
        private dynamic _selectedTreeItem;
        private ZoneItemModel _selectedZone;
        private RelayCommand _zoneActionConfigureCommand;
        private ObservableCollection<ZoneItemModel> _zones = new ObservableCollection<ZoneItemModel>();
        public ZoneReportManagementView View;

        public ReportExportItemModel ExportType
        {
            get => _exportType;
            set
            {
                if (value == _exportType)
                {
                    return;
                }

                _exportType = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<ReportExportItemModel> ExportTypes { get; set; } =
            new ObservableCollection<ReportExportItemModel>
            {
                new ReportExportItemModel {DisplayName = "Pdf"},
                new ReportExportItemModel {DisplayName = "Csv"}
            };

        public bool IsGeneratingReport
        {
            get => _isGeneratingReport;
            set
            {
                if (value == _isGeneratingReport)
                {
                    return;
                }

                _isGeneratingReport = value;
                OnPropertyChanged();
            }
        }

        public EdgeVideoControl PopoutVideoPlayer
        {
            get => _popoutVideoPlayer;
            set
            {
                if (Equals(value, _popoutVideoPlayer))
                {
                    return;
                }

                _popoutVideoPlayer = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand<object> ReportActionCommand =>
            _reportActionCommand ??= new DelegateCommand<object>(ReportAction);

        public ZoneReportManagementViewModel(ILog log = null, ZoneItemManager zoneItemManager = null)
        {
            _log = log;
            Init();
            _reportGenerator = new ReportGenerator();
        }

        private async void ReportAction(object obj)
        {
            if (obj is EReportAction action)
            {
                var fileName = Path.Combine(Path.GetTempPath(), "report.pdf");
                switch (action)
                {
                    case EReportAction.GenerateInventoryReport:

                        try
                        {
                            IsGeneratingReport = true;
                            //// Create a MigraDoc document
                            //var document = await _reportGenerator.CreateReport();

                            ////string ddl = MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToString(document);
                            //DdlWriter.WriteToFile(document, "report.mdddl");

                            //var renderer = new PdfDocumentRenderer(true) {Document = document};
                            //renderer.RenderDocument();

                            //// Save the document...
                            //var filename = "report.pdf";
                            //renderer.PdfDocument.Save(filename);

                            var str =
                                $"{CommunicationManager.GetManagerByZoneId(SelectedServer?.ZoneId ?? Guid.Empty).CameraService.Client.BaseUri}/report/inventory";
                            var client = new WebClient();
                            client.DownloadFileAsync(new Uri(str), fileName);
                            await Task.Delay(1000);
                            if (File.Exists(fileName))
                            {
                                var fi = new FileInfo(fileName);
                                while (fi.IsFileLocked())
                                {
                                    await Task.Delay(300);
                                }
                            }

                            Trace.WriteLine(str);
                            // View.Browser.Address = str;
                            // ...and start a viewer.
                            View.Browser.Address =
                                "file:///" + fileName;
                        }
                        catch (Exception e)
                        {
                        }
                        finally
                        {
                            IsGeneratingReport = false;
                        }

                        break;
                    case EReportAction.CancelReport:
                        break;
                    case EReportAction.ExportReport:
                        if (File.Exists(fileName))
                        {
                        }

                        break;
                    //default:
                    //    throw new ArgumentOutOfRangeException();
                    case EReportAction.GenerateStatusReport:
                        try
                        {
                            IsGeneratingReport = true;
                            //// Create a MigraDoc document
                            //var document = await _reportGenerator.CreateReport();

                            ////string ddl = MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToString(document);
                            //DdlWriter.WriteToFile(document, "report.mdddl");

                            //var renderer = new PdfDocumentRenderer(true) {Document = document};
                            //renderer.RenderDocument();

                            //// Save the document...
                            //var filename = "report.pdf";
                            //renderer.PdfDocument.Save(filename);

                            var str =
                                $"{CommunicationManager.GetManagerByZoneId(SelectedServer?.ZoneId ?? Guid.Empty).CameraService.Client.BaseUri}/report/status";
                            var client = new WebClient();
                            client.DownloadFileAsync(new Uri(str), fileName);
                            await Task.Delay(1000);
                            if (File.Exists(fileName))
                            {
                                var fi = new FileInfo(fileName);
                                while (fi.IsFileLocked())
                                {
                                    await Task.Delay(300);
                                }
                            }

                            Trace.WriteLine(str);
                            // View.Browser.Address = str;
                            // ...and start a viewer.
                            View.Browser.Address =
                                "file:///" + fileName;
                        }
                        catch (Exception e)
                        {
                        }
                        finally
                        {
                            IsGeneratingReport = false;
                        }

                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private async void Init()
        {
            //   await ZoneItemMgr.RefreshZones();
            // await ZoneCameraManagementViewModel.PopulateZones(Zones, false, SelectedServer);
            try
            {
                SelectedZone =
                    ZoneItemManager.Manager.GetZone();
                SelectedZone.SelectedZoneItem =
                    ZoneItemManager.Manager.GetZone();
            }
            catch (Exception e)
            {
                                
            }
        }
    }

    public class ReportExportItemModel : INotifyPropertyChanged
    {
        private string _displayName;

        public string DisplayName
        {
            get => _displayName;
            set
            {
                if (value == _displayName)
                {
                    return;
                }

                _displayName = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}