﻿using System.Windows.Controls;
using static Edge360.Platform.VMS.Client.Extensions.ObjectResolver;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    /// <summary>
    ///     Interaction logic for UserManagementView.xaml
    /// </summary>
    public partial class UserManagementView : UserControl, IView
    {
        public UserManagementView()
        {
            InitializeComponent();
            Model.View = this;
        }

        public UserManagementViewModel Model => DataContext as UserManagementViewModel;

    }
}