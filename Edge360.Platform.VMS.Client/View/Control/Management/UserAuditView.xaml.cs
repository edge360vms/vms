﻿using System.Windows.Controls;
using Edge360.Platform.VMS.Client.ViewModel.View;
using static Edge360.Platform.VMS.Client.Extensions.ObjectResolver;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    /// <summary>
    ///     Interaction logic for UserManagementView.xaml
    /// </summary>
    public partial class UserAuditView : UserControl, IView
    {
        public UserAuditView()
        {
            InitializeComponent();
            GetModel().View = this;
        }

        public UserAuditViewModel GetModel()
        {
            return DataContext as UserAuditViewModel;
        }
    }
}