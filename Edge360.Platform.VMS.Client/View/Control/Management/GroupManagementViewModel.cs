using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Dialog;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.AdConfigZone;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Groups;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Roles;
using Edge360.Vms.Shared.MessageModel.Enums;
using Telerik.Windows.Controls;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    public class GroupManagementViewModel : INotifyPropertyChanged
    {
        private RelayCommand _addAdGroupCommand;
        private RelayCommand _addGroupCommand;
        private RelayCommand _addMemberCommand;
        private ObservableCollection<AdGroupItemModel> _adGroupList = new ObservableCollection<AdGroupItemModel>();

        private ObservableCollection<AdGroupItemModel> _availableAdGroupList =
            new ObservableCollection<AdGroupItemModel>();

        private ObservableCollection<UserItemModel> _availableUsersList;
        private string _filterAdGroupsText = string.Empty;
        private string _filterAvailableAdGroupsText = string.Empty;
        private string _filterAvailableUsersText = string.Empty;
        private string _filterGroupsText = string.Empty;
        private string _filterUsersText = string.Empty;
        private ObservableCollection<GroupItemModel> _groups = new ObservableCollection<GroupItemModel>();
        private bool _isConfiguringRoles;
        private bool _isRefreshingRoles;

        private ObservableCollection<UserItemModel> _originalAvailableMembersList =
            new ObservableCollection<UserItemModel>();

        private ObservableCollection<UserItemModel> _originalMembersList = new ObservableCollection<UserItemModel>();
        private RelayCommand _refreshAdGroupsCommand;
        private RelayCommand _refreshRolesCommand;
        private RelayCommand _refreshUsersCommand;
        private RelayCommand _removeAdGroupCommand;
        private RelayCommand _removeMemberCommand;
        private RelayCommand _resetAdGroupsCommand;
        private RelayCommand _resetUsersCommand;
        private ObservableCollection<RoleItemModel> _roles = new ObservableCollection<RoleItemModel>();
        private RelayCommand _rolesConfigCommand;
        private GroupItemModel _selectedGroup;
        private RoleItemModel _selectedRole;
        private ObservableCollection<RoleItemModel> _selectedRoles;
        private ObservableCollection<UserItemModel> _usersList;
        private GroupManagementView _view;


        public Action SelectedGroupChanged;

        public RelayCommand AddAdGroupCommand =>
            _addAdGroupCommand ??= new RelayCommand(AddAdGroup);

        public RelayCommand AddGroupCommand =>
            _addGroupCommand ??= new RelayCommand(p => AddGroup(), p => CanAddGroup());

        public RelayCommand AddMemberCommand => _addMemberCommand ??= new RelayCommand(AddMember);

        public ObservableCollection<AdGroupItemModel> AdGroupList
        {
            get => _adGroupList;
            set
            {
                if (_adGroupList != value)
                {
                    _adGroupList = value;
                    OnPropertyChanged();
                }
            }
        }

        public CollectionViewSource AdGroupViewSource { get; set; } = new CollectionViewSource
            {SortDescriptions = {new SortDescription {PropertyName = "CommonName"}}};

        public ObservableCollection<AdGroupItemModel> AvailableAdGroupList
        {
            get => _availableAdGroupList;
            set
            {
                if (_availableAdGroupList != value)
                {
                    _availableAdGroupList = value;
                    OnPropertyChanged();
                }
            }
        }

        public CollectionViewSource AvailableAdGroupViewSource { get; set; } = new CollectionViewSource
            {SortDescriptions = {new SortDescription {PropertyName = "CommonName"}}};


        public CollectionViewSource AvailableMembersViewSource { get; set; } = new CollectionViewSource
            {SortDescriptions = {new SortDescription {PropertyName = "Username"}}};

        public ObservableCollection<UserItemModel> AvailableUsersList
        {
            get => _availableUsersList;
            set
            {
                if (_availableUsersList != value)
                {
                    _availableUsersList = value;
                    OnPropertyChanged();
                }
            }
        }

        public string FilterAdGroupsText
        {
            get => _filterAdGroupsText;
            set
            {
                if (_filterAdGroupsText != value)
                {
                    _filterAdGroupsText = value;
                    OnPropertyChanged();
                    AdGroupViewSource.View.Refresh();
                }
            }
        }

        public string FilterAvailableAdGroupsText
        {
            get => _filterAvailableAdGroupsText;
            set
            {
                if (_filterAvailableAdGroupsText != value)
                {
                    _filterAvailableAdGroupsText = value;
                    OnPropertyChanged();
                    AvailableAdGroupViewSource.View.Refresh();
                }
            }
        }

        public string FilterAvailableUsersText
        {
            get => _filterAvailableUsersText;
            set
            {
                if (_filterAvailableUsersText != value)
                {
                    _filterAvailableUsersText = value;
                    OnPropertyChanged();
                    AvailableMembersViewSource.View.Refresh();
                }
            }
        }

        public string FilterGroupsText
        {
            get => _filterGroupsText;
            set
            {
                if (_filterGroupsText != value)
                {
                    _filterGroupsText = value;
                    OnPropertyChanged();
                    GroupsViewSource.View.Refresh();
                }
            }
        }

        public string FilterUsersText
        {
            get => _filterUsersText;
            set
            {
                if (_filterUsersText != value)
                {
                    _filterUsersText = value;
                    OnPropertyChanged();
                    MembersViewSource.View.Refresh();
                }
            }
        }

        public ObservableCollection<GroupItemModel> Groups
        {
            get => _groups;
            set
            {
                if (_groups != value)
                {
                    _groups = value;
                    OnPropertyChanged();
                }
            }
        }

        public CollectionViewSource GroupsViewSource { get; set; } = new CollectionViewSource
            {SortDescriptions = {new SortDescription {PropertyName = "GroupName"}}};

        public bool IsConfiguringRoles
        {
            get => _isConfiguringRoles;
            set
            {
                if (value == _isConfiguringRoles)
                {
                    return;
                }

                _isConfiguringRoles = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(RolesConfigCommand));
            }
        }

        public bool IsRefreshingRoles
        {
            get => _isRefreshingRoles;
            set
            {
                if (value == _isRefreshingRoles)
                {
                    return;
                }

                _isRefreshingRoles = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(RefreshRolesCommand));
            }
        }

        public bool IsUpdatingAdGroups { get; set; }

        public bool IsUpdatingMembers { get; set; }

        public CollectionViewSource MembersViewSource { get; set; } = new CollectionViewSource
            {SortDescriptions = {new SortDescription {PropertyName = "Username"}}};

        public ObservableCollection<AdGroupItemModel> OriginalAdGroupList { get; set; } =
            new ObservableCollection<AdGroupItemModel>();

        public ObservableCollection<AdGroupItemModel> OriginalAvailableAdGroupList { get; set; } =
            new ObservableCollection<AdGroupItemModel>();

        public ObservableCollection<UserItemModel> OriginalAvailableMembersList
        {
            get => _originalAvailableMembersList;
            set
            {
                if (Equals(value, _originalAvailableMembersList))
                {
                    return;
                }

                _originalAvailableMembersList = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<UserItemModel> OriginalMembersList
        {
            get => _originalMembersList;
            set
            {
                if (Equals(value, _originalMembersList))
                {
                    return;
                }

                _originalMembersList = value;
                OnPropertyChanged();
            }
        }


        public RelayCommand RefreshAdGroupsCommand =>
            _refreshAdGroupsCommand ??= new RelayCommand(p => UpdateAdGroups(this));


        public RelayCommand RefreshRolesCommand =>
            _refreshRolesCommand ??= new RelayCommand(p => UpdateRoles(), p => !IsRefreshingRoles);

        public RelayCommand RefreshUsersCommand =>
            _refreshUsersCommand ??= new RelayCommand(p => UpdateMembers());


        public RelayCommand RemoveAdGroupCommand =>
            _removeAdGroupCommand ??= new RelayCommand(RemoveAdGroup);

        public RelayCommand RemoveMemberCommand =>
            _removeMemberCommand ??= new RelayCommand(RemoveMember);

        public RelayCommand ResetAdGroupsCommand =>
            _resetAdGroupsCommand ??= new RelayCommand(p => ResetAdGroups(),
                p => SelectedGroup != null && SelectedGroup.AdGroupsDirty);

        public RelayCommand ResetUsersCommand =>
            _resetUsersCommand ??= new RelayCommand(p => ResetUsers(),
                p => SelectedGroup != null && SelectedGroup.UsersDirty);


        public ObservableCollection<RoleItemModel> Roles
        {
            get => _roles;
            set
            {
                if (Equals(value, _roles))
                {
                    return;
                }

                _roles = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand RolesConfigCommand =>
            _rolesConfigCommand ??= new RelayCommand(ConfigureRoles, p => !IsConfiguringRoles);

        public GroupItemModel SelectedGroup
        {
            get => _selectedGroup;
            set
            {
                if (_selectedGroup != value)
                {
                    _selectedGroup = value;

                    OnPropertyChanged();
                    SelectedGroupChanged?.Invoke();
                    Task.Run(() =>
                    {
                        try
                        {
                            if (_selectedGroup != null)
                            {
                                _ = this.TryUiAsync(() =>
                                {
                                    View?.LoggingManagementView?.Model?.FilterAudit(_selectedGroup?.GroupId);
                                }).ConfigureAwait(false);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                        UpdateMembers().ConfigureAwait(false);
                        UpdateAdGroups().ConfigureAwait(false);
                        UpdateRoles().ConfigureAwait(false);
                    }).ConfigureAwait(false);
                }
            }
        }

        public RoleItemModel SelectedRole
        {
            get => _selectedRole;
            set
            {
                if (Equals(value, _selectedRole))
                {
                    return;
                }

                _selectedRole = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<RoleItemModel> SelectedRoles
        {
            get => _selectedRoles;
            set
            {
                if (Equals(value, _selectedRoles))
                {
                    return;
                }

                _selectedRoles = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<UserItemModel> UsersList
        {
            get => _usersList;
            set
            {
                if (_usersList != value)
                {
                    _usersList = value;
                    OnPropertyChanged();
                }
            }
        }

        public GroupManagementView View
        {
            get => _view;
            set
            {
                if (_view != value)
                {
                    _view = value;
                    OnPropertyChanged();
                }
            }
        }

        public GroupManagementViewModel()
        {
            Dispatcher.CurrentDispatcher.InvokeAsync(async () =>
            {
                var groupBinding = new Binding(nameof(Groups)) {Source = this, Mode = BindingMode.OneWay};
                BindingOperations.SetBinding(GroupsViewSource,
                    CollectionViewSource.SourceProperty,
                    groupBinding);
                GroupsViewSource.Filter += GroupsViewSourceOnFilter;

                var memberBinding = new Binding(nameof(UsersList)) {Source = this, Mode = BindingMode.OneWay};
                BindingOperations.SetBinding(MembersViewSource,
                    CollectionViewSource.SourceProperty,
                    memberBinding);
                MembersViewSource.Filter += MembersViewSourceOnFilter;

                BindingOperations.SetBinding(AvailableMembersViewSource,
                    CollectionViewSource.SourceProperty,
                    new Binding(nameof(AvailableUsersList)) {Source = this, Mode = BindingMode.OneWay});
                AvailableMembersViewSource.Filter += AvailableMembersViewSourceOnFilter;

                var availAdBinding = new Binding(nameof(AvailableAdGroupList))
                    {Source = this, Mode = BindingMode.OneWay};
                BindingOperations.SetBinding(AvailableAdGroupViewSource,
                    CollectionViewSource.SourceProperty, availAdBinding
                );
                AvailableAdGroupViewSource.Filter += AvailableAdGroupViewSourceOnFilter;

                var adGroupBinding = new Binding(nameof(AdGroupList)) {Source = this, Mode = BindingMode.OneWay};
                BindingOperations.SetBinding(AdGroupViewSource,
                    CollectionViewSource.SourceProperty,
                    adGroupBinding);
                AdGroupViewSource.Filter += AdGroupViewSourceOnFilter;
            }, DispatcherPriority.Background);

            try
            {
                HubManager.SignalRClientEvent += async (e, zone, data) =>
                {
                    if (Equals(e, ESignalRClientEvent.GroupCreated))
                    {
                        HubManagerOnGroupCreated(zone, data);
                    }
                    else if (Equals(e, ESignalRClientEvent.GroupDeleted))
                    {
                        HubManagerOnGroupDeleted(zone, data);
                    }
                    else if (Equals(e, ESignalRClientEvent.GroupEdited) || Equals(e, ESignalRClientEvent.GroupUpdated))
                    {
                        HubManagerOnGroupUpdated(zone, data);
                    }
                };
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            //   View = view;
            Initialize();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void ResetAdGroups()
        {
            AdGroupList = new ObservableCollection<AdGroupItemModel>(OriginalAdGroupList);
            AvailableAdGroupList = new ObservableCollection<AdGroupItemModel>(OriginalAvailableAdGroupList);

            if (SelectedGroup != null)
            {
                SelectedGroup.Members = new List<Guid?>(SelectedGroup.OriginalMembers);
                SelectedGroup.MemberDetails = new List<GroupMemberDetails>(SelectedGroup.OriginalMemberDetails);
                SelectedGroup.AdGroupsDirty = false;
                OnPropertyChanged(nameof(ResetAdGroupsCommand));
                OnPropertyChanged(nameof(SelectedGroup.SaveAdGroupsCommand));
            }
        }

        private void ResetUsers()
        {
            UsersList = new ObservableCollection<UserItemModel>(OriginalMembersList);
            AvailableUsersList = new ObservableCollection<UserItemModel>(OriginalAvailableMembersList);

            if (SelectedGroup != null)
            {
                SelectedGroup.Members = new List<Guid?>(SelectedGroup.OriginalMembers);
                SelectedGroup.MemberDetails = new List<GroupMemberDetails>(SelectedGroup.OriginalMemberDetails);
                SelectedGroup.UsersDirty = false;
                OnPropertyChanged(nameof(ResetUsersCommand));
                OnPropertyChanged(nameof(SelectedGroup.SaveUsersCommand));
            }
        }

        private async void ConfigureRoles(object obj)
        {
            if (obj is ERoleConfigureCommand command)
            {
                IsConfiguringRoles = true;
                switch (command)
                {
                    case ERoleConfigureCommand.AddRole:
                        await AddRole();
                        break;
                    case ERoleConfigureCommand.EditRole:
                        await EditRole();
                        break;
                    case ERoleConfigureCommand.DeleteRole:
                        await DeleteRole();
                        break;
                    default:
                        IsConfiguringRoles = false;
                        throw new ArgumentOutOfRangeException();
                }

                IsConfiguringRoles = false;
            }
        }

        private async Task DeleteRole()
        {
            if (SelectedRole == null)
            {
                return;
            }

            SelectedRole.ResolvedServer ??= ServerItemManager.Manager.ItemCollection.Values.FirstOrDefault(o =>
                o.ZoneId == SelectedRole.ZoneId);

            if (SelectedRole.ResolvedServer == null)
            {
                await Task.Run(async () =>
                {
                    await ServerItemManager.Manager.RefreshServers((Guid) SelectedRole.ZoneId);
                    SelectedRole.ResolvedServer ??= ServerItemManager.Manager.ItemCollection.Values.FirstOrDefault(o =>
                        o.ZoneId == SelectedRole.ZoneId);
                });
                if (SelectedRole.ResolvedServer == null)
                {
                    return;
                }
            }

            var dialog = await DialogUtil.EmbeddedModalConfirmDialog(
                View?.ParentOfType<ManagementView>()?.DialogOverlayGrid?.ModalContentPresenter,
                new EmbeddedDialogParameters
                {
                    DialogButtons = EDialogButtons.YesNo,
                    Description =
                        $"Are you sure you wish to delete this role?\nZone: {SelectedRole.ResolvedServer.Label}\nPermissions: {SelectedRole.AvailableRolesPreview}"
                });
            if (dialog)
            {
                try
                {
                    var delete = await GetManagerByZoneId().RolesService
                        .RemoveRole(SelectedRole.GroupId, SelectedRole.ZoneId);
                    if (delete.Success)
                    {
                        RefreshRolesCommand.Execute(null);
                    }
                }
                catch (Exception e)
                {
                }
            }
        }

        private async Task EditRole()
        {
            if (SelectedRole == null)
            {
                return;
            }

            var dialog = new CreateGroupRoleDialog {Header = "Edge360 VMS - Edit Role"};
            if (SelectedGroup != null)
            {
                dialog.Model.DialogItem.GroupId = SelectedGroup.GroupId;
                dialog.Model.DialogItem.ZoneId = SelectedRole.ZoneId;
                dialog.Model.DialogItem.OriginalRoles = Roles;
            }

            if (SelectedRole != null)
            {
                dialog.Model.DialogItem.RoleValue = SelectedRole.RoleValue;
            }

            dialog.Model.DialogItem.Init();
            var managementView = View.ParentOfType<ManagementView>();
            if (managementView != null)
            {
                dialog.SetModalView(managementView.DialogOverlayGrid.ModalContentPresenter);

                dialog.Closed += async (sender, args) =>
                {
                    if (dialog.Result && dialog.DialogItem is CreateGroupRoleItemModel roleItem)
                    {
                        var effectiveRoles =
                            roleItem.AvailableRoles?.Where(o => o.IsChecked)?.Select(o => (int) o.Role);
                        if (effectiveRoles != null)
                        {
                            var roleValue = effectiveRoles.Sum();

                            if (roleItem.AvailableZones != null)
                            {
                                var zonesToModify = roleItem.AvailableZones.Where(o => o.IsChecked).ToList();
                                if (zonesToModify.Count() > 1)
                                {
                                    var zoneRoles = zonesToModify.Where(z => z.ZoneId != null)
                                        .ToDictionary(z => z.ZoneId, z => roleValue);

                                    var batchedRoles = await GetManagerByZoneId().RolesService
                                        .AssignRoleBatch(roleItem.GroupId, zoneRoles);
                                    if (batchedRoles != null)
                                    {
                                        RefreshRolesCommand.Execute(null);
                                    }
                                }
                                else
                                {
                                    var zone = zonesToModify?.FirstOrDefault();
                                    if (zone != null)
                                    {
                                        var added = await GetManagerByZoneId().RolesService.AssignRole(
                                            roleItem.GroupId, roleValue,
                                            zone.ZoneId);
                                        if (added != null && added.Success)
                                        {
                                            RefreshRolesCommand.Execute(null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                };
            }
        }

        private async Task AddRole()
        {
            var dialog = new CreateGroupRoleDialog {Header = "Edge360 VMS - Create Role"};
            dialog.Model.DialogItem.GroupId = SelectedGroup.GroupId;
            dialog.Model.DialogItem.ZoneId = SelectedGroup.ZoneId;
            dialog.Model.DialogItem.OriginalRoles = Roles;

            var existingRole = Roles.FirstOrDefault(o => o.ZoneId == SelectedGroup.ZoneId);
            if (existingRole != null)
            {
                SelectedRole = existingRole;
                dialog.Model.DialogItem.RoleValue = SelectedRole.RoleValue;
            }

            dialog.Model.DialogItem.Init();
            var managementView = View.ParentOfType<ManagementView>();
            if (managementView != null)
            {
                dialog.SetModalView(managementView.DialogOverlayGrid.ModalContentPresenter);

                dialog.Closed += async (sender, args) =>
                {
                    try
                    {
                        if (dialog.Result && dialog.DialogItem is CreateGroupRoleItemModel roleItem)
                        {
                            var effectiveRoles =
                                roleItem.AvailableRoles?.Where(o => o.IsChecked)?.Select(o => (int) o.Role);
                            if (effectiveRoles != null)
                            {
                                var roleValue = effectiveRoles.Sum();

                                if (roleItem.AvailableZones != null)
                                {
                                    var zonesToModify = roleItem.AvailableZones.Where(o => o.IsChecked).ToList();
                                    if (zonesToModify.Count() > 1)
                                    {
                                        var zoneRoles = zonesToModify.Where(z => z.ZoneId != null)
                                            .ToDictionary(z => z.ZoneId, z => roleValue);

                                        var batchedRoles = await GetManagerByZoneId().RolesService
                                            .AssignRoleBatch(roleItem.GroupId, zoneRoles);
                                        if (batchedRoles != null)
                                        {
                                            RefreshRolesCommand.Execute(null);
                                        }
                                    }
                                    else
                                    {
                                        var zone = zonesToModify?.FirstOrDefault();
                                        if (zone != null)
                                        {
                                            var added = await GetManagerByZoneId().RolesService.AssignRole(
                                                roleItem.GroupId, roleValue,
                                                zone.ZoneId);
                                            if (added != null && added.Success)
                                            {
                                                RefreshRolesCommand.Execute(null);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                    }
                };
            }
        }

        private async Task UpdateRoles()
        {
            await Task.Run(async () =>
            {
                if (!IsRefreshingRoles)
                {
                    IsRefreshingRoles = true;

                    try
                    {
                        var previouslySelected = SelectedGroup;
                        var zoneList = new ObservableCollection<ServerItemModel>();
                        await ServerItemManager.Manager.RefreshServers();
                        // await ZoneCameraManagementViewModel.LoadServers(zoneList);

                        if (SelectedGroup != null)
                        {
                            var roles = await GetManagerByZoneId().RolesService.GetRolesByGroup(
                                SelectedGroup.GroupId,
                                ConnectedZoneId);
                            if (roles != null && SelectedGroup == previouslySelected)
                            {
                                var roleItemModels = roles.Roles.OfType<Role>()
                                    .Select(o => (RoleItemModel) o).ToList();
                                foreach (var r in roleItemModels)
                                {
                                    var resolvedZone = zoneList.FirstOrDefault(o => o.ZoneId == r.ZoneId) ??
                                                       ServerItemManager.Manager.ItemCollection.Values.FirstOrDefault(
                                                           o => o.ZoneId == r.ZoneId);
                                    if (resolvedZone != null)
                                    {
                                        r.ResolvedServer = resolvedZone;
                                    }
                                    else if (r.ZoneId == Guid.Empty)
                                    {
                                        r.ResolvedServer = new ServerItemModel {Label = "Global"};
                                    }
                                }

                                Roles = new ObservableCollection<RoleItemModel>(roleItemModels);
                            }
                            else
                            {
                                Roles = new ObservableCollection<RoleItemModel>();
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                    finally
                    {
                        IsRefreshingRoles = false;
                    }
                }
            }).ConfigureAwait(false);
        }

        public void RemoveAdGroup(object sender)
        {
            if (sender is AdGroupItemModel groupItemModel)
            {
                if (groupItemModel != null && SelectedGroup != null)
                {
                    groupItemModel.RemoveFromGroup(SelectedGroup);
                    AdGroupList.Remove(groupItemModel);
                    AvailableAdGroupList.Add(groupItemModel);
                }
            }
        }

        public void AddAdGroup(object sender)
        {
            if (sender is AdGroupItemModel groupItemModel)
            {
                if (groupItemModel != null && SelectedGroup != null)
                {
                    groupItemModel.AddToGroup(SelectedGroup);
                    AvailableAdGroupList.Remove(groupItemModel);
                    AdGroupList.Add(groupItemModel);
                }
            }
        }

        public void RemoveMember(object sender)
        {
            if (sender is UserItemModel userItemModel)
            {
                if (userItemModel != null && SelectedGroup != null)
                {
                    userItemModel.RemoveFromGroup(SelectedGroup);
                    UsersList.Remove(userItemModel);
                    AvailableUsersList.Add(userItemModel);
                }
            }
        }


        public void AddMember(object sender)
        {
            if (sender is UserItemModel userItemModel)
            {
                if (userItemModel != null && SelectedGroup != null)
                {
                    userItemModel.AddToGroup(SelectedGroup);
                    AvailableUsersList.Remove(userItemModel);
                    UsersList.Add(userItemModel);
                }
            }
        }

        private async void Initialize()
        {
            try
            {
                _ = Task.Run(async () =>
                {
                    var groupsLoaded = await LoadGroups();
                    if (groupsLoaded)
                    {
                        if (SelectedGroup == null)
                        {
                            SelectedGroup = Groups.FirstOrDefault();
                        }
                    }
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        private async Task UpdateAdGroups(object obj = null)
        {
            await Task.Run(async () =>
            {
                if (!IsUpdatingAdGroups)
                {
                    IsUpdatingAdGroups = true;
                    try
                    {
                        var availableGroups = new List<AdGroupItemModel>();
                        if (SelectedGroup != null)
                        {
                            var matchingGroups = new List<AdGroupItemModel>(SelectedGroup.MemberDetails
                                .Where(o => o.Type == "adgroup").Select(m => new AdGroupItemModel
                                {
                                    GroupId = m.MemberId,
                                    CommonName = m.Name ?? m.MemberId.ToString(),
                                    ActiveDirectoryId = Guid.Empty,
                                    DistinguishedName = m.Name ?? m.MemberId.ToString()
                                }).ToList());
                            var originalAvailableGroups = new List<AdGroupItemModel>();
                            var originalMatchingGroups = new List<AdGroupItemModel>(SelectedGroup.OriginalMemberDetails
                                .Where(o => o.Type == "adgroup").Select(m => new AdGroupItemModel
                                {
                                    GroupId = m.MemberId,
                                    CommonName = m.Name ?? m.MemberId.ToString(),
                                    ActiveDirectoryId = Guid.Empty,
                                    DistinguishedName = m.Name ?? m.MemberId.ToString()
                                }).ToList());

                            //show the users before they are hydrated
                            AdGroupList = new ObservableCollection<AdGroupItemModel>(matchingGroups);
                            AvailableAdGroupList = new ObservableCollection<AdGroupItemModel>(availableGroups);

                            OriginalAdGroupList = new ObservableCollection<AdGroupItemModel>(originalMatchingGroups);
                            OriginalAvailableAdGroupList =
                                new ObservableCollection<AdGroupItemModel>(originalAvailableGroups);
                            var adGroups = await GetManagerByZoneId().AdConfigZoneService.ListConfigs();
                            if (adGroups != null)
                            {
                                foreach (var ad in adGroups)
                                {
                                    var groups = await GetManagerByZoneId().AdConfigZoneService
                                        .QueryAdGroups(new QueryAdGroupsV2
                                        {
                                            ActiveDirectoryId = ad.ActiveDirectoryId,
                                            GroupNameMatch = "*",
                                            UseCache = obj != null
                                        }); //(ad.ActiveDirectoryId, "*");

                                    if (groups?.AdGroups != null)
                                    {
                                        originalMatchingGroups.RemoveAll(b =>
                                            groups.AdGroups.Any(c => c.GroupId == b.GroupId)); // 
                                        matchingGroups.RemoveAll(b =>
                                            groups.AdGroups.Any(c => c.GroupId == b.GroupId)); // 

                                        availableGroups.AddRange(groups.AdGroups
                                            .Where(o => SelectedGroup == null || SelectedGroup.Members == null ||
                                                        !SelectedGroup.Members.Contains(o.GroupId))
                                            .Select(m => (AdGroupItemModel) m)
                                            .ToList());
                                        matchingGroups.AddRange(groups.AdGroups
                                            .Where(o => SelectedGroup != null &&
                                                        SelectedGroup.Members.Contains(o.GroupId))
                                            .Select(m => (AdGroupItemModel) m)
                                            .ToList());
                                        originalAvailableGroups.AddRange(groups.AdGroups
                                            .Where(o => SelectedGroup == null ||
                                                        SelectedGroup.OriginalMembers == null ||
                                                        !SelectedGroup.OriginalMembers.Contains(o.GroupId))
                                            .Select(m => (AdGroupItemModel) m)
                                            .ToList());
                                        originalMatchingGroups.AddRange(groups.AdGroups
                                            .Where(o => SelectedGroup != null &&
                                                        SelectedGroup.OriginalMembers.Contains(o.GroupId))
                                            .Select(m => (AdGroupItemModel) m)
                                            .ToList());
                                    }

                                    //foreach (var g in groups.AdGroups)
                                    //{
                                    //    //Console.WriteLine($"{g.CommonName} {g.DistinguishedName} {g.GroupId} {g.ActiveDirectoryId}");
                                    //}
                                }
                            }

                            AdGroupList = new ObservableCollection<AdGroupItemModel>(matchingGroups);
                            AvailableAdGroupList = new ObservableCollection<AdGroupItemModel>(availableGroups);

                            OriginalAdGroupList = new ObservableCollection<AdGroupItemModel>(originalMatchingGroups);
                            OriginalAvailableAdGroupList =
                                new ObservableCollection<AdGroupItemModel>(originalAvailableGroups);
                        }
                    }
                    catch (Exception e)
                    {
                    }
                    finally
                    {
                        IsUpdatingAdGroups = false;
                    }
                }
            }).ConfigureAwait(false);
        }

        private async Task UpdateMembers()
        {
            await Task.Run(async () =>
            {
                if (!IsUpdatingMembers)
                {
                    IsUpdatingMembers = true;
                    try
                    {
                        var availableGroups = new List<UserItemModel>();
                        if (SelectedGroup != null)
                        {
                            var matchingGroups = new List<UserItemModel>(SelectedGroup.MemberDetails
                                .Where(o => o.Type == "user").Select(m => new UserItemModel
                                {
                                    Username = m.Name ?? m.MemberId.ToString(),
                                    Id = m.MemberId,
                                    UserId = m.MemberId,
                                    DisplayName = m.Name ?? m.MemberId.ToString(),
                                }).ToList());
                            var originalAvailableGroups = new List<UserItemModel>();
                            var originalMatchingGroups = new List<UserItemModel>(SelectedGroup.OriginalMemberDetails
                                .Where(o => o.Type == "user").Select(m => new UserItemModel
                                {
                                    Username = m.Name ?? m.MemberId.ToString(),
                                    Id = m.MemberId,
                                    UserId = m.MemberId,
                                    DisplayName = m.Name ?? m.MemberId.ToString(),
                                }).ToList());

                            //show the users before they are hydrated
                            UsersList = new ObservableCollection<UserItemModel>(matchingGroups);
                            AvailableUsersList = new ObservableCollection<UserItemModel>(availableGroups);

                            OriginalMembersList = new ObservableCollection<UserItemModel>(originalMatchingGroups);
                            OriginalAvailableMembersList =
                                new ObservableCollection<UserItemModel>(originalAvailableGroups);
                            var members = await GetManagerByZoneId().UserService.GetUsers();
                            if (members != null)
                            {
                                originalMatchingGroups.RemoveAll(b => members.Any(c => c.UserId == b.UserId)); // 
                                matchingGroups.RemoveAll(b => members.Any(c => c.UserId == b.UserId)); // 
                                availableGroups.AddRange(members
                                    .Where(o => SelectedGroup == null || SelectedGroup.Members == null ||
                                                !SelectedGroup.Members.Contains(o.UserId))
                                    .Select(m => (UserItemModel) m)
                                    .ToList());
                                matchingGroups.AddRange(members
                                    .Where(o => SelectedGroup != null && SelectedGroup.Members.Contains(o.UserId))
                                    .Select(m => (UserItemModel) m)
                                    .ToList());
                                originalAvailableGroups.AddRange(members
                                    .Where(o => SelectedGroup == null || SelectedGroup.OriginalMembers == null ||
                                                !_selectedGroup.OriginalMembers.Contains(o.UserId))
                                    .Select(m => (UserItemModel) m)
                                    .ToList());
                                originalMatchingGroups.AddRange(members
                                    .Where(o => SelectedGroup != null &&
                                                SelectedGroup.OriginalMembers.Contains(o.UserId))
                                    .Select(m => (UserItemModel) m)
                                    .ToList());

                                if (SelectedGroup?.Members?.Count > 0)
                                {
                                    var availableMembers =
                                        members.Where(o => !SelectedGroup.Members.Contains(o.UserId)).ToList();
                                    var resolvedMembers =
                                        members.Where(o => SelectedGroup.Members.Contains(o.UserId)).ToList();
                                    var originalAvailableMembers =
                                        members.Where(o => !SelectedGroup.OriginalMembers.Contains(o.UserId)).ToList();
                                    var originalMembers = members
                                        .Where(o => SelectedGroup.OriginalMembers.Contains(o.UserId))
                                        .ToList();

                                    UsersList = new ObservableCollection<UserItemModel>(
                                        resolvedMembers.Select(m => (UserItemModel) m)?.ToList() ??
                                        new List<UserItemModel>());
                                    AvailableUsersList = new ObservableCollection<UserItemModel>(
                                        availableMembers?.Select(m => (UserItemModel) m)?.ToList() ??
                                        new List<UserItemModel>());

                                    OriginalMembersList = new ObservableCollection<UserItemModel>(
                                        originalMembers.Select(m => (UserItemModel) m)?.ToList() ??
                                        new List<UserItemModel>());
                                    OriginalAvailableMembersList = new ObservableCollection<UserItemModel>(
                                        originalAvailableMembers?.Select(m => (UserItemModel) m)?.ToList() ??
                                        new List<UserItemModel>());
                                }
                            }


                            UsersList = new ObservableCollection<UserItemModel>(matchingGroups);
                            AvailableUsersList = new ObservableCollection<UserItemModel>(availableGroups);

                            OriginalMembersList = new ObservableCollection<UserItemModel>(originalMatchingGroups);
                            OriginalAvailableMembersList =
                                new ObservableCollection<UserItemModel>(originalAvailableGroups);
                        }
                    }
                    catch (Exception e)
                    {
                    }
                    finally
                    {
                        IsUpdatingMembers = false;
                    }
                }
            }).ConfigureAwait(false);
        }

        private bool CanAddGroup()
        {
            return true;
        }

        private async Task<bool> LoadGroups()
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var groups = await GetManagerByZoneId().GroupsService.ListGroups(true, true);

                    if (groups != null)
                    {
                        var list = groups.Select(g => (GroupItemModel) g).ToList();
                        Groups = new ObservableCollection<GroupItemModel>(list);
                    }

                    return true;
                });
            }
            catch (Exception e)
            {
            }

            return false;
        }

        private void AddGroup()
        {
            var window = new CreateGroupDialog();

            var mgmt = View.ParentOfType<ManagementView>();
            window.SetModalView(mgmt.DialogOverlayGrid.ModalContentPresenter);
            window.Closed += async (sender, args) =>
            {
                if (window != null && window.Result &&
                    window.Model?.DialogItem is CreateGroupDialogItemModel dialogItem &&
                    !string.IsNullOrEmpty(dialogItem.GroupName))
                {
                    await GetManagerByZoneId().GroupsService.CreateGroup(dialogItem.GroupName, null).ConfigureAwait(false);
                }
            };
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async Task UpdateGroupById(Guid guid)
        {
            var updatedGroup = await GetManagerByZoneId().GroupsService.GetGroupById(guid, true, true);
            if (updatedGroup != null)
            {
                GroupItemModel previousSelected = null;
                if (Groups.FirstOrDefault(o => o.GroupId == guid) is GroupItemModel g)
                {
                    if (g == SelectedGroup)
                    {
                        previousSelected = g;
                    }

                    Groups.Remove(g);
                }


                Groups.Add(updatedGroup);
                if (previousSelected != null)
                {
                    SelectedGroup = Groups.FirstOrDefault(o => o.GroupId == previousSelected.GroupId);
                }
            }
        }

        private async void HubManagerOnGroupUpdated(Guid guid1, string id)
        {
            try
            {
                Console.WriteLine($"GroupUpdated: {id}");
                if (!string.IsNullOrEmpty(id))
                {
                    var guid = Guid.Parse(id);
                    if (guid != Guid.Empty)
                    {
                        _ = View?.TryUiAsync(async () => { await UpdateGroupById(guid); },
                            DispatcherPriority.Background).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception e)
            {
                _log?.Error(e);
            }
        }

        private void HubManagerOnGroupDeleted(Guid guid1, string id)
        {
            try
            {
                Console.WriteLine($"GroupDeleted: {id}");
                if (!string.IsNullOrEmpty(id))
                {
                    var guid = Guid.Parse(id);
                    if (guid != Guid.Empty)
                    {
                        _ = View.TryUiAsync(() =>
                        {
                            if (Groups.FirstOrDefault(o => o.GroupId == guid) is GroupItemModel g)
                            {
                                Groups.Remove(g);
                            }
                        }, DispatcherPriority.Background).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception e)
            {
                _log?.Error(e);
            }
        }

        private async void HubManagerOnGroupCreated(Guid guid1, string id)
        {
            try
            {
                Console.WriteLine($"GroupCreated: {id}");
                if (!string.IsNullOrEmpty(id))
                {
                    var guid = Guid.Parse(id);
                    if (guid != Guid.Empty)
                    {
                        _ = View?.TryUiAsync(async () =>
                        {
                            var createdGroup = await GetManagerByZoneId().GroupsService
                                .GetGroupById(guid, true, true);
                            if (createdGroup != null)
                            {
                                Groups.Add(createdGroup);
                            }
                        }, DispatcherPriority.Background).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception e)
            {
                _log?.Error(e);
            }
        }

        private void AdGroupViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            e.Accepted = e.Item is AdGroupItemModel item &&
                         FilterAdGroupsText.PartialContain(item.CommonName);
        }

        private void AvailableMembersViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            e.Accepted = e.Item is UserItemModel item &&
                         FilterAvailableUsersText.PartialContain(item.Username, item.DisplayName,
                             item.UserId?.ToString());
        }

        private void AvailableAdGroupViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            e.Accepted = e.Item is AdGroupItemModel item &&
                         FilterAvailableAdGroupsText.PartialContain(item.CommonName);
        }

        private void MembersViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            e.Accepted = e.Item is UserItemModel item &&
                         FilterUsersText.PartialContain(item.Username, item.DisplayName, item.UserId?.ToString());
        }

        private void GroupsViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            e.Accepted = e.Item is GroupItemModel item &&
                         FilterGroupsText.PartialContain(item.GroupName);
        }
    }

    internal enum ERoleConfigureCommand
    {
        AddRole,
        EditRole,
        DeleteRole
    }
}