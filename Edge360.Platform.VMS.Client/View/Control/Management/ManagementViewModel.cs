﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.UI.Common.View.Dialog;
using log4net;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    public class ManagementViewModel : ViewModelBase, INotifyPropertyChanged, IDisposable
    {
        private bool _isModalMode;
        private string _modalWindowHeader;
        private object _selectedPanelItem;
        private ILog _log;

        public DialogBase ModalBase =>
            View?.DialogOverlayGrid.ModalContentPresenter.Content as DialogBase ?? new DialogBase();

        public object SelectedPanelItem
        {
            get => _selectedPanelItem;
            set
            {
                if (Equals(value, _selectedPanelItem))
                {
                    return;
                }

                _selectedPanelItem = value;
                OnPropertyChanged();
            }
        }


        public ManagementView View { get; set; }

        public ManagementViewModel(ILog log = null)
        {
            _log = log;

        }

        public event PropertyChangedEventHandler PropertyChanged;


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void ModalContentPresenterOnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dialog = ModalBase;
            OnPropertyChanged(nameof(ModalBase));
        }

        public void Dispose()
        {
            
        }
    }
}