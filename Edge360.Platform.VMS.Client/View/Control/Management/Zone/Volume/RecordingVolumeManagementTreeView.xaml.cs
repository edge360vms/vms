﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.Extensions.Helpers;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Nodes;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Management.Zone.Volume
{
    /// <summary>
    ///     Interaction logic for RecordingVolumeManagementTreeView.xaml
    /// </summary>
    public partial class RecordingVolumeManagementTreeView : UserControl
    {
        public RadTabControl TabControlParent { get; set; }
        //public RecordingVolumeManagementTreeViewModel Model => DataContext as RecordingVolumeManagementTreeViewModel;

        public RecordingVolumeManagementTreeView()
        {
            InitializeComponent();
            try
            {
                TabControlParent = this.GetVisualParent<RadTabControl>();
            }
            catch (Exception e)
            {
            }

            RecordingProfileItemModel.ManagementUpdateRequest += RecordingProfileItemModelOnManagementUpdateRequest;
        }

        public static void UpdateSelectedVolumes(RecordingProfileItemModel model,
            List<VolumeItemModel> volumeItemModels)
        {
            try
            {
                model.SelectedVolumes.Clear();

                foreach (var r in volumeItemModels)
                {
                    if (r.IsSelected && model.SelectedVolumes.All(o => o.Id != r.Id))
                    {
                        model.SelectedVolumes.Add(r);
                    }
                }

                model.VolumeIds = new List<Guid>(model.SelectedVolumes.Select(o => o.Id).Distinct());
            }
            catch (Exception e)
            {
            }
        }

        private async void RecordingProfileItemModelOnManagementUpdateRequest(RadTabControl obj)
        {
            try
            {
                if (this.GetVisualParent<RadTabControl>() == obj)
                {
                    if (DataContext is RecordingProfileItemModel model)
                    {
                        
                        //if (Tag is ZoneCameraManagementViewModel cameraManagement)
                        //{
                        //    cameraManagement.SelectedZone.ServerCollectionViewSource.View.Refresh();
                        //}
                        var cam = CameraItemModelManager.Manager.ItemCollection.FirstOrDefault(o =>
                            o.Key == model.CameraId).Value;


                        if (cam == null)
                        {
                            await CameraItemModelManager.Manager.RequestCameras(new NodeItemModel(new NodeDescriptor()
                                {Id = model.CameraId}));

                            cam = CameraItemModelManager.Manager.ItemCollection.FirstOrDefault(o =>
                                o.Key == model.CameraId).Value;
                        }

                        if (cam != null)
                        {
                            var volumes =
                                await RecordingProfileItemManager.Manager.RefreshProfileByRecordingProfileId(cam.ZoneId,
                                    model.Id);
                            var activeVolumes = new List<VolumeItemModel>();
                            foreach (var server in VolumeSelectionTree.Items
                                .OfType<KeyValuePair<Tuple<Guid, Guid>, ServerItemModel>>())
                            {
                                if (server.Value.Archiver != null)
                                {
                                    foreach (var volume in server.Value.Archiver.Volumes)
                                    {
                                        var volumeSelected = volumes.RecordingProfile.VolumeIds.Contains(volume.Id);
                                        volume.IsSelected = volumeSelected;
                                        if (volumeSelected)
                                        {
                                            activeVolumes.Add(volume);
                                        }
                                    }
                                }
                            }
                        
                            UpdateSelectedVolumes(model, activeVolumes);
                        }

                        model.IsSelected = true;
                        obj.SelectedItem = model;
                        ItemsControlHelper.SetAutoSelectFirstOnChange(obj, false);
                        ItemsControlHelper.SetAutoSelectFirstOnChange(obj, true);
                        //model.VolumeIds
                        //   model.SelectedVolumes = new ObservableCollection<VolumeItemModel>(model);
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private void VolumeSelectionTree_OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DataContext is RecordingProfileItemModel model)
                {
                    var activeVolumes = new List<VolumeItemModel>();
                    foreach (var server in VolumeSelectionTree.Items.OfType<KeyValuePair<Tuple<Guid, Guid>, ServerItemModel>>())
                    {
                        if (server.Value.Archiver != null)
                        {
                            foreach (var volume in server.Value.Archiver.Volumes)
                            {
                                var volumeSelected = model.VolumeIds != null && model.VolumeIds.Contains(volume.Id);
                                volume.IsSelected = volumeSelected;
                                if (volumeSelected)
                                {
                                    activeVolumes.Add(volume);
                                }
                            }
                        }
                    }
                    
                    UpdateSelectedVolumes(model, activeVolumes);
                    model.IsSelected = true;

                    //model.VolumeIds
                    //   model.SelectedVolumes = new ObservableCollection<VolumeItemModel>(model);
                }
            }
            catch (Exception exception)
            {
            }
        }

        private void VolumeSelectionTree_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataContext is RecordingProfileItemModel model)
            {
                Application.Current?.Dispatcher?.InvokeAsync(() =>
                {
                    try
                    {
                        var volumeItemModels = VolumeSelectionTree.SelectedItems.OfType<VolumeItemModel>().ToList();
                        UpdateSelectedVolumes(model, volumeItemModels);
                    }
                    catch (Exception exception)
                    {
                    }
                });
            }
        }
    }

    //public class RecordingVolumeManagementTreeViewModel : INotifyPropertyChanged
    //{
    //    public event PropertyChangedEventHandler PropertyChanged;

    //    [NotifyPropertyChangedInvocator]
    //    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    //    {
    //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    //    }
    //}
}