﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Camera;

namespace Edge360.Platform.VMS.Client.View.Control.Management.Zone.Recording
{
    /// <summary>
    ///     Interaction logic for RecordingProfilesView.xaml
    /// </summary>
    public partial class RecordingProfilesView : UserControl
    {
        public static readonly DependencyProperty SelectedCameraProperty = DependencyProperty.Register(
            "SelectedCamera", typeof(CameraItemModel), typeof(RecordingProfilesView),
            new PropertyMetadata(default(CameraItemModel), PropertyChangedCallback));

        public RecordingProfilesViewModel Model => DataContext as RecordingProfilesViewModel;

        public bool QueueVisibileCommand { get; set; }

        public CameraItemModel SelectedCamera
        {
            get => (CameraItemModel) GetValue(SelectedCameraProperty);
            set => SetValue(SelectedCameraProperty, value);
        }

        public RecordingProfilesView()
        {
            InitializeComponent();
            IsVisibleChanged += OnIsVisibleChanged;
        }

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RecordingProfilesView view)
            {
                view.Model.SelectedCamera = view.SelectedCamera;
                if (!view.IsVisible)
                {
                    view.QueueVisibileCommand = true;
                }
                else
                {
                    HandleVisible(view);
                }
            }
        }

        private static void HandleVisible(RecordingProfilesView view)
        {
            if (view != null)
            {
                if (view.SelectedCamera != null)
                {
                    view.SelectedCamera.GetRecordingProfiles().ConfigureAwait(false);
                }

                view.QueueVisibileCommand = false;
            }
        }

        private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (IsVisible)
            {
                HandleVisible(this);
            }
        }
    }

    public class RecordingProfilesViewModel : INotifyPropertyChanged
    {
        private CameraItemModel _selectedCamera;

        public CameraItemModel SelectedCamera
        {
            get => _selectedCamera;
            set
            {
                if (Equals(value, _selectedCamera))
                {
                    return;
                }

                _selectedCamera = value;

                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}