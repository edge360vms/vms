﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Nodes;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Dialog.Login;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Communication.Services;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.Cluster;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.Server;
using log4net;
using Prism.Commands;
using ServiceStack;

namespace Edge360.Platform.VMS.Client.View.Control.Management.Zone.ClusterNode
{
    /// <summary>
    ///     Interaction logic for ClusterNodeView.xaml
    /// </summary>
    public partial class ClusterNodeView : UserControl
    {
        public static readonly DependencyProperty SelectedArchiverProperty =
            DependencyProperty.Register("SelectedArchiver", typeof(ServerConfigItemModel), typeof(ClusterNodeView),
                new PropertyMetadata(default(ServerConfigItemModel), PropertyChangedCallback));

        public ClusterNodeViewModel Model => DataContext as ClusterNodeViewModel;

        public ServerConfigItemModel SelectedArchiver
        {
            get => (ServerConfigItemModel) GetValue(SelectedArchiverProperty);
            set => SetValue(SelectedArchiverProperty, value);
        }

        public ClusterNodeView()
        {
            InitializeComponent();
            Model.View = this;
        }

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ClusterNodeView view)
            {
                view.Model.SelectedArchiver = view.SelectedArchiver;
            }
        }
    }


    public class ClusterNodeViewModel : INotifyPropertyChanged
    {
        private readonly ILog _log;

        private ObservableCollection<CassandraNodeItemModel> _cassandraNodes =
            new ObservableCollection<CassandraNodeItemModel>();

        private bool _isLoggedIn;

        private bool _isRefreshingNodes;
        private bool _isRefreshingServers;

        private DelegateCommand<object> _nodeViewActionCommand;
        private ServerConfigItemModel _selectedArchiver;
        private CassandraNodeItemModel _selectedNodeItemModel;
        private ServerItemModel _selectedServer;
        private string _statusMessage;
        private ClusterNodeView _view;
        private CollectionViewSource _serverCollectionViewSource;
        private dynamic _selectedServerGridItem;

        public ObservableCollection<CassandraNodeItemModel> CassandraNodes
        {
            get => _cassandraNodes;
            set
            {
                if (Equals(value, _cassandraNodes))
                {
                    return;
                }

                _cassandraNodes = value;
                OnPropertyChanged();
            }
        }

        public bool IsRefreshingNodes

        {
            get => _isRefreshingNodes;
            set
            {
                if (value == _isRefreshingNodes)
                {
                    return;
                }

                _isRefreshingNodes = value;
                OnPropertyChanged();
            }
        }

        public bool IsRefreshingServers
        {
            get => _isRefreshingServers;
            set
            {
                if (value == _isRefreshingServers)
                {
                    return;
                }

                _isRefreshingServers = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand<object> NodeViewActionCommand =>
            _nodeViewActionCommand ??=
                new DelegateCommand<object>(NodeViewAction, CanNodeActionExecute).ObservesProperty(() =>
                    SelectedNodeItemModel);

        public ServerConfigItemModel SelectedArchiver
        {
            get => _selectedArchiver;
            set
            {
                if (Equals(value, _selectedArchiver))
                {
                    return;
                }

                _selectedArchiver = value;
                OnPropertyChanged();
            }
        }

        public CassandraNodeItemModel SelectedNodeItemModel
        {
            get => _selectedNodeItemModel;
            set
            {
                if (Equals(value, _selectedNodeItemModel))
                {
                    return;
                }

                _selectedNodeItemModel = value;
                OnPropertyChanged();
            }
        }

        public ServerItemModel SelectedServer
        {
            get => _selectedServer;
            set
            {
                if (Equals(value, _selectedServer))
                {
                    return;
                }

                _selectedServer = value;
                OnPropertyChanged();
            }
        }

 
        public CollectionViewSource ServerCollectionViewSource => _serverCollectionViewSource ??=  new CollectionViewSource(){Source = ServerItemManager.Manager.ItemCollection};

        public ServiceManager ServiceManager
        {
            get
            {
                if (SelectedArchiver != null)
                {
                    return CommunicationManager.GetManagerByZoneId(SelectedArchiver.ZoneId);
                }

                return default;
            }
        }

        public string StatusMessage
        {
            get => _statusMessage;
            set
            {
                if (value == _statusMessage)
                {
                    return;
                }

                _statusMessage = value;
                OnPropertyChanged();
            }
        }

        public ClusterNodeView View
        {
            get => _view;
            set
            {
                if (Equals(value, _view))
                {
                    return;
                }

                _view = value;
                OnPropertyChanged();
            }
        }

        private ContentPresenter _dialogPresenter => TabUtil.FindFocusedTab<ManagementTab>().ManagementView
            .DialogOverlayGrid.ModalContentPresenter;

        public dynamic SelectedServerGridItem
        {
            get => _selectedServerGridItem;
            set
            {
                if (Equals(value, _selectedServerGridItem)) return;
                
                _selectedServerGridItem = value;
                try
                {
                    if (_selectedServerGridItem.Value is ServerItemModel server)
                    {
                        SelectedServer = server;
                    }
                }
                catch (Exception e)
                {
                }
                OnPropertyChanged();
            }
        }

        public ClusterNodeViewModel(ILog log = null)
        {
            _log = log;
            //  MainWindowViewModel = mainWindowViewModel;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private bool CanNodeActionExecute(object arg)
        {
            if (arg is ENodeViewAction action)
            {
                switch (action)
                {
                    case ENodeViewAction.RemoveNode:
                        return SelectedNodeItemModel != null;
                    case ENodeViewAction.RemoveServer:
                        break;
                    case ENodeViewAction.RefreshServers:
                        break;
                    case ENodeViewAction.GetZoneClusterStatus:
                        break;
                    case ENodeViewAction.UpdateReplication:
                        return SelectedNodeItemModel != null;
                    default:
                        return true;
                }
            }

            return true;
        }

        private async Task RefreshServers()
        {
            if (!IsRefreshingServers)
            {
                IsRefreshingServers = true;
                try
                {
                    if (ServiceManager != null)
                    {
                        await Task.Run(async () =>
                        {
                            await ServerItemManager.Manager.RefreshServers(SelectedArchiver.ZoneId);
                        });
                       // await ZoneCameraManagementViewModel.LoadServers(Servers, SelectedArchiver.ZoneId);
                        //var resp = await ServiceManager.ServerService.GetServers(new GetServers());
                        //if (resp != null)
                        //{
                        //    var output = resp.Select(s => s.ConvertTo<ServerItemModel>()).ToList();
                        //    Servers = new ObservableCollection<ServerItemModel>(output);
                        //}
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                finally
                {
                    IsRefreshingServers = false;
                }
            }
        }


        private async void NodeViewAction(object obj)
        {
            try
            {
                if (obj is ENodeViewAction action)
                {
                    switch (action)
                    {
                        case ENodeViewAction.RemoveNode:
                            await RemoveNode();
                            break;
                        case ENodeViewAction.RemoveServer:
                            await RemoveServer();
                            break;
                        case ENodeViewAction.RefreshServers:
                            await RefreshServers();
                            break;
                        case ENodeViewAction.GetZoneClusterStatus:
                            await GetZoneClusterStatus(ServiceManager);
                            break;

                        case ENodeViewAction.UpdateReplication:
                            await UpdateReplication();
                            break;
                        default:
                            return;
                    }
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        private async Task RemoveServer()
        {
            var dialog = await DialogUtil.EmbeddedModalConfirmDialog(
                _dialogPresenter, new EmbeddedDialogParameters
                {
                    Description =
                        "Are you sure you wish to remove the server from the cluster?\n\nThis is a destructive action.",
                    DialogButtons = EDialogButtons.YesNo,

                    Header = "Remove Server"
                });
            if (dialog)
            {
                if (ServiceManager != null)
                {
                    var resp = await ServiceManager.ServerService.DeleteServer(new DeleteServer
                    {
                        ServerId = SelectedServer.ServerId
                    });
                    if (resp != null)
                    {
                    }
                }
            }
        }

        private async Task UpdateReplication()
        {
            try
            {
                var selectedNode = SelectedNodeItemModel;
                var zoneId = selectedNode?.ZoneId ?? Guid.Empty;
                if (zoneId == Guid.Empty || selectedNode == null)
                {
                    return;
                }

                var clusterType = selectedNode.ClusterType;
                var svc = CommunicationManager.GetManagerByZoneId(zoneId);
                var updateReplication = new UpdateReplication {ClusterType = clusterType, ZoneId = selectedNode.ZoneId};
                if (svc == null)
                {
                    var dialog = await DialogUtil.EmbeddedModalConfirmDialog(_dialogPresenter,
                        new EmbeddedDialogParameters
                        {
                            Description = "Connection to Server not found.\nDo you wish to login?",
                            DialogButtons = EDialogButtons.YesNo
                        });
                    if (dialog)
                    {
                        var tcs = new TaskCompletionSource<bool>();

                        if (_dialogPresenter != null)
                        {
                            var login = new LoginDialog();
                            login.SetModalView(_dialogPresenter);
                            login.Model.LoginItemModel.Domain = selectedNode.Address;
                            login.Model.LoginItemModel.UserName = CommunicationManager.CurrentUser.Username;

                            login.Closed += async (sender, args) => { tcs.SetResult(login.Result); };

                            var response = await tcs.Task;
                            if (response && login.Model.ServiceManager != null)
                            {
                                svc = login.Model.ServiceManager;

                            }
                        }
                    }
                }

                if (svc != null)
                {
                    var dialog = await DialogUtil.EmbeddedModalConfirmDialog(_dialogPresenter,
                        new EmbeddedDialogParameters()
                        {
                            Description =
                                $"Are you sure you wish to continue?\n\nThis action will set the replication factor of the Cluster and may interrupt connectivity."
                        });
                    if (dialog)
                    {
                        var resp = await svc.ZoneClusterService.UpdateReplication(updateReplication);
                        if (resp != null)
                        {
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private async Task RemoveNode()
        {
            var dialog = DialogUtil.InputDialog(
                $"Are you sure you wish to eject node? \nHostId: '{SelectedNodeItemModel.HostId}'\nAddress: '{SelectedNodeItemModel.Address}'\nThis action can disrupt database activity.\nEnter '{SelectedNodeItemModel.ClusterType}' to confirm.");
            if (dialog.Result && dialog.Response.ToLowerInvariant() ==
                SelectedNodeItemModel.ClusterType.ToString().ToLowerInvariant())
            {
                ServiceManager?.ZoneClusterService.UpdateClusterInfo(new UpdateClusterInfo
                {
                    Action = EUpdateClusterAction.RemoveNode,
                    Arguments = SelectedNodeItemModel.HostId.ToString(),
                    ClusterType = SelectedNodeItemModel.ClusterType,
                    ZoneId = Guid.Empty
                });

                //refresh
                await GetZoneClusterStatus(ServiceManager);
            }

            //await DialogUtil.EmbeddedModalConfirmDialog(
            //_dialogPresenter, new EmbeddedDialogParameters
            //{
            //    Description =
            //        "Are you sure you wish to remove the node from the cluster?\n\nThis is a destructive action.",
            //    DialogButtons = EDialogButtons.YesNo,

            //    Header = "Removing Node from Cluster"
            //});
            //if (dialog)
            //{

            //}
        }

        private async Task GetZoneClusterStatus(ServiceManager svcMgr)
        {
            if (!IsRefreshingNodes)
            {
                try
                {
                    IsRefreshingNodes = true;
                    if (svcMgr != null)
                    {
                        var resp = await svcMgr.ZoneClusterService.GetZoneClusterStatus(
                            new GetClusterInfo
                            {
                                Action = EGetClusterAction.Status,
                                ClusterType = EClusterType.Global,
                                ZoneId = Guid.Empty
                            });
                        if (resp != null)
                        {
                            Trace.WriteLine("Response Received.");
                            var responseStatus = resp.GetResponseStatus();
                            if (responseStatus != null && !string.IsNullOrEmpty(responseStatus.ErrorCode))
                            {
                                Trace.WriteLine($"{responseStatus.ErrorCode}");
                            }
                            else if (resp.NodeStatusResponses != null)
                            {
                                var globalNodeStatuses = resp
                                    .NodeStatusResponses.Values.SelectMany(o => o);
                                var cassandraNodeItemModels = globalNodeStatuses.Select(o =>
                                    {
                                        var cassandraNodeItemModel = (CassandraNodeItemModel) o;
                                        cassandraNodeItemModel.ClusterType = EClusterType.Global;
                                        return cassandraNodeItemModel;
                                    })
                                    .ToList();

                                var zoneClusterStatus =
                                    await svcMgr.ZoneClusterService.GetZoneClusterStatus(
                                        new GetClusterInfo
                                        {
                                            Action = EGetClusterAction.Status,
                                            ClusterType = EClusterType.Zone,
                                            ZoneId = Guid.Empty
                                        });

                                if (zoneClusterStatus != null)
                                {
                                    if (zoneClusterStatus
                                        .NodeStatusResponses != null)
                                    {
                                        var zoneNodeStatuses = zoneClusterStatus
                                            .NodeStatusResponses.Values.SelectMany(o => o);
                                        var cassandraZoneNodeItemModels = zoneNodeStatuses.Select(o =>
                                            {
                                                var cassandraNodeItemModel = (CassandraNodeItemModel) o;
                                                cassandraNodeItemModel.ClusterType = EClusterType.Zone;
                                                return cassandraNodeItemModel;
                                            })
                                            .ToList();

                                        cassandraNodeItemModels.AddRange(cassandraZoneNodeItemModels);
                                    }
                                }

                                CassandraNodes =
                                    new ObservableCollection<CassandraNodeItemModel>(
                                        cassandraNodeItemModels);
                                foreach (var n in cassandraNodeItemModels)
                                {
                                    Trace.WriteLine(n);
                                }
                            }
                        }
                    }
                }

                catch (Exception e)
                {
                    StatusMessage =
                        $"{e?.GetResponseStatus()?.ErrorCode} {e?.GetResponseStatus()?.Message} {e?.Message}";
                    Trace.WriteLine(e);
                }
                finally
                {
                    IsRefreshingNodes = false;
                }
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}