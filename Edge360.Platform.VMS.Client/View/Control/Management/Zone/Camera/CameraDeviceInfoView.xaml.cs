﻿using System;
using System.Windows;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Raven.Vms.CameraService.ServiceModel.Enums;

namespace Edge360.Platform.VMS.Client.View.Control.Management.Zone.Camera
{
    /// <summary>
    ///     Interaction logic for CameraDeviceInfoView.xaml
    /// </summary>
    public partial class CameraDeviceInfoView : UserControl
    {
        public CameraDeviceInfoView()
        {
            InitializeComponent();
            IsVisibleChanged += OnIsVisibleChanged;
        }

        private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (IsVisible)
            {
                HandleVisibleObjectChange(this);
            }
        }

        public static readonly DependencyProperty SelectedCameraProperty = DependencyProperty.Register(
            "SelectedCamera", typeof(CameraItemModel), typeof(CameraDeviceInfoView), new PropertyMetadata(default(CameraItemModel), PropertyChangedCallback));

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (d is CameraDeviceInfoView view)
                {
                    view?.SelectedCameraChanged?.Invoke();
                    if (view.IsVisible)
                    {
                        HandleVisibleObjectChange(view);
                    }
                    else
                    {
                        view.HasVisibleReloadRequested = true;
                    }
                }
            }
            catch (Exception exception)
            {
                
            }
        }

        private static void HandleVisibleObjectChange(CameraDeviceInfoView view)
        {
            view.SelectedCamera.ConfigureHardwareCommand.Execute(ECameraHardwareConfigure.UpdateInformation);
            view.HasVisibleReloadRequested = false;
        }

        public bool HasVisibleReloadRequested { get; set; }

        public event Action SelectedCameraChanged;

        public CameraItemModel SelectedCamera
        {
            get { return (CameraItemModel) GetValue(SelectedCameraProperty); }
            set { SetValue(SelectedCameraProperty, value); }
        }
    }
}