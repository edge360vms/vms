﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Communication;
using Edge360.Raven.Vms.AuthorityService.MessageModel.Model;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Cameras;
using ServiceStack;
using Telerik.Windows.Controls.Data.DataForm;
using NodeDescriptor = Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Nodes.NodeDescriptor;

namespace Edge360.Platform.VMS.Client.View.Control.Management.Zone.Folders
{
    /// <summary>
    ///     Interaction logic for FolderManagementView.xaml
    /// </summary>
    public partial class ZoneFolderManagementView : UserControl
    {
        public ZoneFolderManagementViewModel Model => DataContext as ZoneFolderManagementViewModel;

        public ZoneFolderManagementView()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty ZoneCameraManagementViewModelProperty = DependencyProperty.Register(
            "ZoneCameraManagementViewModel", typeof(ZoneCameraManagementViewModel), typeof(ZoneFolderManagementView), new PropertyMetadata(default(ZoneCameraManagementViewModel), PropertyChangedCallback));

        public static readonly DependencyProperty ServerConfigProperty = DependencyProperty.Register("ServerConfig", typeof(ServerConfigItemModel), typeof(ZoneFolderManagementView), new PropertyMetadata(default(ServerConfigItemModel), PropertyChangedCallback));

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ZoneFolderManagementView view)
            {
                view.Model.ZoneCameraManagementViewModel = view.ZoneCameraManagementViewModel;
            }
        }

        public ZoneCameraManagementViewModel ZoneCameraManagementViewModel
        {
            get { return (ZoneCameraManagementViewModel) GetValue(ZoneCameraManagementViewModelProperty); }
            set { SetValue(ZoneCameraManagementViewModelProperty, value); }
        }

        private async void RadDataForm_OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction == EditAction.Commit)
            {
                try
                {
                    var node = CameraTreeView.Model.SelectedItem;
                    if (node is CameraItemModel camera)
                    {
                        camera.UpdateCameraLabel();
                    }
                    else
                    {
                        var nodeDescriptor = node.ConvertTo<NodeDescriptor>();
                        nodeDescriptor.Id = node.ObjectId;
                        var resp = await CommunicationManager.GetManagerByZoneId(node.ZoneId ?? Guid.Empty).NodeService
                            .UpdateNode(nodeDescriptor);
                        if (resp != null)
                        {

                        }
                    }
                }
                catch (Exception exception)
                {
                    
                }
            }
        }
    }

    public class ZoneFolderManagementViewModel : INotifyPropertyChanged
    {
        private ZoneCameraManagementViewModel _zoneCameraManagementViewModel;
        private bool _isEditing;

        public ZoneCameraManagementViewModel ZoneCameraManagementViewModel
        {
            get => _zoneCameraManagementViewModel;
            set
            {
                if (Equals(value, _zoneCameraManagementViewModel)) return;
                _zoneCameraManagementViewModel = value;
                OnPropertyChanged();
            }
        }

        public bool IsEditing
        {
            get => _isEditing;
            set
            {
                if (value == _isEditing) return;
                _isEditing = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}