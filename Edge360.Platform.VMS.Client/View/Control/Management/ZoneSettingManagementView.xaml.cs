﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Common.Enums.Management;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Interfaces.Authority;
using Edge360.Platform.VMS.Common.Interfaces.Monitoring;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication.Polly;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Servers;
using log4net;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    /// <summary>
    ///     Interaction logic for VolumeManagementView.xaml
    /// </summary>
    public partial class ZoneSettingManagementView : UserControl, IView
    {
        public ZoneSettingManagementViewModel Model => DataContext as ZoneSettingManagementViewModel;

        public ZoneSettingManagementView()
        {
            InitializeComponent();
            Model.View = this;
        }
    }

    public class ZoneSettingManagementViewModel : ZoneManagementViewModel, INotifyPropertyChanged
    {
        private string _filterZonesText = string.Empty;
        private EdgeVideoControl _popoutVideoPlayer;
        private RelayCommand _refreshZonesCommand;
        private ServerItemModel _selectedServer;
        private dynamic _selectedTreeItem;
        private ZoneItemModel _selectedZone;
        private CollectionViewSource _serversViewSource = new CollectionViewSource();
        private RelayCommand _zoneActionConfigureCommand;
        private ObservableCollection<ZoneItemModel> _zones = new ObservableCollection<ZoneItemModel>();
        public ZoneSettingManagementView View;
        private ILog _log;

        public EdgeVideoControl PopoutVideoPlayer
        {
            get => _popoutVideoPlayer;
            set
            {
                if (Equals(value, _popoutVideoPlayer))
                {
                    return;
                }

                _popoutVideoPlayer = value;
                OnPropertyChanged();
            }
        }

        public CollectionViewSource ServersViewSource
        {
            get => _serversViewSource;
            set
            {
                if (Equals(value, _serversViewSource))
                {
                    return;
                }

                _serversViewSource = value;
                OnPropertyChanged();
            }
        }

        public ZoneSettingManagementViewModel(ILog log = null, ZoneItemManager zoneItemManager = null)
        {
            _log = log;
            Init();
        }

        private async void Init()
        {
            //await ZoneItemMgr.RefreshZones();
           // await ZoneCameraManagementViewModel.PopulateZones(Zones, false, SelectedServer);
           try
           {
               _ = Task.Run(() =>
               {
                   try
                   {
                       var serverItemModel = ServerItemMgr.GetServer();// Zones?.FirstOrDefault(o => o?.ZoneId == ConnectedZoneId)?.Servers.FirstOrDefault();
                       if (serverItemModel != null)
                       {
                           SelectedServer = serverItemModel;
                           try
                           {
                               SelectedZone =
                                   ZoneItemManager.Manager.GetZone();
                               SelectedZone.SelectedZoneItem =
                                   ZoneItemManager.Manager.GetZone();
                           }
                           catch (Exception e)
                           {
                                
                           }
                       }
                   }
                   catch (Exception e)
                   {
                   }
               });
           }
           catch (Exception e)
           {
               _log?.Info(e);
           }
        }
    }
}