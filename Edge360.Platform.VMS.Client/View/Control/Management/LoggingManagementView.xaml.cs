using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Polly;
using Edge360.Vms.Client.AuditLog.Common.Types.Events;
using Prism.Events;
using Prism.Regions;
using WispFramework.Extensions.Tasks;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    public partial class LoggingManagementView : UserControl
    {
        public static readonly DependencyProperty SelectedServerProperty = DependencyProperty.RegisterAttached(
            "SelectedServer", typeof(ServerItemModel), typeof(LoggingManagementView),
            new PropertyMetadata(default(ServerItemModel)));

        public static readonly DependencyProperty MaxAvailableHeightProperty = DependencyProperty.Register(
            "MaxAvailableHeight", typeof(double), typeof(LoggingManagementView), new PropertyMetadata(default(double)));

        public static readonly DependencyProperty PropertyTypeProperty = DependencyProperty.Register(
            "PropertyType", typeof(double), typeof(LoggingManagementView), new PropertyMetadata(default(double)));

        public static readonly DependencyProperty WatchedObjectProperty = DependencyProperty.Register("WatchedObject", typeof(Guid), typeof(LoggingManagementView), new PropertyMetadata(default(Guid), PropertyChangedCallback));

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (d is LoggingManagementView view)
                {
                    if (view.WatchedObject != Guid.Empty)
                    {
                        App.Current.Dispatcher.InvokeAsync(() =>
                        {
                            view?.Model?.FilterAudit(view?.WatchedObject);
                        }, DispatcherPriority.ApplicationIdle);
                    }
                }
            }
            catch (Exception exception)
            {
                _log?.Info(exception);
            }
        }

        public double MaxAvailableHeight
        {
            get => (double) GetValue(MaxAvailableHeightProperty);
            set => SetValue(MaxAvailableHeightProperty, value);
        }

        public double MaxAvailableWidth
        {
            get => (double) GetValue(PropertyTypeProperty);
            set => SetValue(PropertyTypeProperty, value);
        }

        public LoggingManagementViewModel Model => DataContext as LoggingManagementViewModel;

        public Guid WatchedObject
        {
            get { return (Guid) GetValue(WatchedObjectProperty); }
            set { SetValue(WatchedObjectProperty, value); }
        }

        public LoggingManagementView()
        {
            InitializeComponent();
            //DataContext = new LoggingManagementViewModel(this);
            Loaded += OnLoaded;
            IsVisibleChanged += OnIsVisibleChanged;
            SizeChanged += OnSizeChanged;
        }

        public static void SetSelectedServer(DependencyObject element, ServerItemModel value)
        {
            element.SetValue(SelectedServerProperty, value);
        }

        public static ServerItemModel GetSelectedServer(DependencyObject element)
        {
            return (ServerItemModel) element.GetValue(SelectedServerProperty);
        }

        private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Model?.VisibilityChanged((bool) e.NewValue);
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                MaxAvailableHeight = RenderSize.Height;
                MaxAvailableWidth = RenderSize.Width;
            }
            catch (Exception exception)
            {
            }
        }


        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            Model?.LoadedView();
            //  LoginWindow.InitializeAuditLoggingEvents();
        }
    }

    public class LoggingManagementViewModel : INotifyPropertyChanged
    {
        private readonly IEventAggregator _eventAggregator;

        private readonly IRegionManager _regionManager;
        private Dictionary<string, object> _filterDictionary = new Dictionary<string, object>();
        private bool _isConnected;
        private CustomServiceClient previousClient;

        public bool IsConnected
        {
            get => _isConnected;
            set
            {
                if (value == _isConnected)
                {
                    return;
                }

                _isConnected = value;
                OnPropertyChanged();
            }
        }

        public LoggingManagementViewModel(IEventAggregator eventAggregator = null, IRegionManager regionManager = null)
        {
            //  return;
            _regionManager = regionManager;
            //  View = view;
            _eventAggregator = eventAggregator;

            if (_regionManager != null)
            {
                try
                {
                    //var auditTool =
                    //    _regionManager?.Regions["AuditLogTool"]?.ActiveViews?.FirstOrDefault() as AuditLogGridView;
                    //if (auditTool != null)
                    //{
                    //    Console.WriteLine("Audit Tool!");
                    //}
                }
                catch (Exception e)
                {
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public async void LoadedView()
        {
            try
            {
                await Task.Run(async () =>
                {
                    //_eventAggregator.GetEvent<AuditLogColumnsSelected>().Publish(new[]
                    //{
                    //    "Action",
                    //    "Message",
                    //    "TimestampUtc",
                    //    "Success"
                    //});
                    previousClient = GetManagerByZoneId().ServiceClients[EClientType.Management].GetClient();
                    _eventAggregator.GetEvent<ServiceClientActivatedEvent>()
                        .Publish(GetManagerByZoneId());
                    IsConnected = true;
                    //var client = GetManagerByZoneId().ServiceClients[EClientType.Management];
                    //if (previousClient != client)
                    //{
                    //    //Task.Run(async () =>
                    //    //{
                    //    //    try
                    //    //    {
                    //    //        var abc = await GetManagerByZoneId().AuditLogService.SearchAuditLog(new SearchAuditLog
                    //    //        {
                    //    //            DayBucketUtc = DateTime.UtcNow,
                    //    //            UserId = null
                    //    //        });
                    //    //        if (abc != null)
                    //    //        {
                    //    //            Console.WriteLine($"{abc.AuditLogs.Count}");
                    //    //        }
                    //    //    }
                    //    //    catch (Exception e)
                    //    //    {
                    //    //        Console.WriteLine(e);
                    //    //    }

                    //    //});


                    //    // FilterAudit(CurrentUser.UserId);
                    //}
                });
            }
            catch (Exception e)
            {
            }
        }

        public async void FilterAudit(Guid? userId = null, string action = null)
        {
            await Task.Run(async () =>
            {
                if (_eventAggregator != null)
                {
                    try
                    {
                        _filterDictionary = new Dictionary<string, object>();
                        if (userId != null)
                        {
                            _filterDictionary.Add("UserId", userId.Value);
                        }

                        if (!string.IsNullOrEmpty(action))
                        {
                            _filterDictionary.Add("Action", action);
                        }

                        _eventAggregator.GetEvent<AuditLogColumnsSelected>().Publish(new[]
                        {
                            "UserId",
                            "ObjectId",
                            "Action",
                            "Message",
                            "TimestampUtc",
                            "Success"
                        });
                        UpdateFilters();
                    }
                    catch (Exception e)
                    {
                    }
                }
            });
        }

        public void VisibilityChanged(bool visible)
        {
            if (visible)
            {
                if (_eventAggregator != null)
                {
                    //clear filters when visible
                    UpdateFilters();
                }
            }
        }

        private void UpdateFilters()
        {
            try
            {
                if (SettingsManager.Settings.Miscellaneous.UseAuditTool)
                {
                    using (var cts = new CancellationTokenSource(15000))
                    {

                        _ = Task.Run(async () =>
                        {
                            try
                            {
                                var sw = new Stopwatch();
                                sw.Start();
                                while (!IsConnected)
                                {
                                    await Task.Delay(250);
                                }

                                sw.Stop();
                                Console.WriteLine(
                                    $"Waited {sw.ElapsedMilliseconds}ms for LoggingManagementView to Connect!");
                                _eventAggregator.GetEvent<AuditLogLockFilterChanged>()
                                    .Publish(_filterDictionary?.Count == 0 ? null : _filterDictionary);
                                _eventAggregator.GetEvent<AuditLogRefreshRequest>().Publish();
                            }
                            catch (Exception e)
                            {
                            }
                        }, cts.Token).ConfigureAwait(false);
                    }

                }
             
            }
            catch (Exception e)
            {
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}