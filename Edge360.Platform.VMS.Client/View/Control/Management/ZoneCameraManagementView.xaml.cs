﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions.Commands;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Common.Enums.Management;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Interfaces.Monitoring;
using Edge360.Platform.VMS.Common.Util;
using log4net;
using Prism.Commands;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    /// <summary>
    ///     Interaction logic for VolumeManagementView.xaml
    /// </summary>
    public partial class ZoneCameraManagementView : UserControl, IView
    {
        public ZoneCameraManagementViewModel Model => DataContext as ZoneCameraManagementViewModel;

        public ZoneCameraManagementView()
        {
            InitializeComponent();
            Model.View = this;
        }

        private void CommandBinding_OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void SelectFocus(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                if (e.Parameter is TextBox textBox)
                {
                    textBox.SelectAll();
                    Keyboard.Focus(textBox);
                }
            }
            catch (Exception exception)
            {
            }
        }
    }

    public class ZoneCameraManagementViewModel : ZoneManagementViewModel, INotifyPropertyChanged, IDisposable
    {
        private static ILog _log;
        private IApplicationCommands _applicationCommands;

        private string _cameraFilterText = string.Empty;
        private RelayCommand _closePopoutVideoCommand;

        private CancellationTokenSource _filterCts;
        private string _filterZonesText = string.Empty;
        private bool _isFilteringDisabled;
        private bool _isFilteringOffline;
        private bool _isFilteringOnline;
        private EdgeVideoControl _popoutVideoPlayer;
        private ObservableCollection<ZoneItemModel> _zones = new ObservableCollection<ZoneItemModel>();
        public ZoneCameraManagementView View;
        private DelegateCommand _selectSearchBoxCommand;

        public override void ZoneConfigure(object obj = null)
        {
            base.ZoneConfigure(obj);
            IsConfiguring = true;
            if (obj is EZoneActionConfigCommand command)
            {
                switch (command)
                {
                    case EZoneActionConfigCommand.PreviewPopoutPlayer:
                        PlayPreviewPlayer();
                        break;
                }
            }

            IsConfiguring = false;
        }

        public string CameraFilterText
        {
            get => _cameraFilterText;
            set
            {
                if (value == _cameraFilterText)
                {
                    return;
                }

                _cameraFilterText = value;
                OnPropertyChanged();
                _filterCts?.Cancel();
                _filterCts = new CancellationTokenSource();
                var cts = _filterCts;
                Task.Run(async () =>
                {
                    await Task.Delay(400);
                    if (!cts.IsCancellationRequested)
                    {
                        Application.Current?.Dispatcher?.InvokeAsync(() =>
                        {
                            SelectedZone?.CameraCollectionViewSource?.View?.Refresh();
                        });
                    }
                }, cts.Token);
            }
        }
        // private DelegateCommand<ZoneItemModel> _refreshZone;

        public RelayCommand ClosePopoutVideoCommand =>
            _closePopoutVideoCommand ??= new RelayCommand(ClosePopout);

        public bool IsFilteringDisabled
        {
            get => _isFilteringDisabled;
            set
            {
                if (value == _isFilteringDisabled)
                {
                    return;
                }

                _isFilteringDisabled = value;
                OnPropertyChanged();
                Task.Run(() =>
                {
                    Application.Current?.Dispatcher?.InvokeAsync(() =>
                    {
                        SelectedZone?.CameraCollectionViewSource?.View?.Refresh();
                    });
                });
            }
        }

        public bool IsFilteringOffline
        {
            get => _isFilteringOffline;
            set
            {
                if (value == _isFilteringOffline)
                {
                    return;
                }

                _isFilteringOffline = value;
                OnPropertyChanged();
                Task.Run(() =>
                {
                    Application.Current?.Dispatcher?.InvokeAsync(() =>
                    {
                        SelectedZone?.CameraCollectionViewSource?.View?.Refresh();
                    });
                });
            }
        }

        //public DelegateCommand<ZoneItemModel> RefreshZoneCommand =>
        //    _refreshZone ??= new DelegateCommand<ZoneItemModel>(RefreshZone);

        //private async void RefreshZone(ZoneItemModel obj)
        //{
        //    await PopulateZone(obj.ZoneId);
        //}

        public bool IsFilteringOnline
        {
            get => _isFilteringOnline;
            set
            {
                if (value == _isFilteringOnline)
                {
                    return;
                }

                _isFilteringOnline = value;
                OnPropertyChanged();
                Task.Run(() =>
                {
                    Application.Current?.Dispatcher?.InvokeAsync(() =>
                    {
                        SelectedZone?.CameraCollectionViewSource?.View?.Refresh();
                    });
                });
            }
        }

        public EdgeVideoControl PopoutVideoPlayer
        {
            get => _popoutVideoPlayer;
            set
            {
                if (Equals(value, _popoutVideoPlayer))
                {
                    return;
                }

                _popoutVideoPlayer = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand SelectSearchBoxCommand =>
            _selectSearchBoxCommand ??= new DelegateCommand(SearchBoxSelect);

        public ZoneCameraManagementViewModel(ILog log = null, ZoneItemManager zoneItemManager = null,
            IApplicationCommands appCommands = null)
        {
            _log = log;
            try
            {
                _applicationCommands = appCommands;
            }
            catch (Exception e)
            {
            }

            ZonesViewSource.Filter += ZonesViewSourceOnFilter;
            Init();
        }

        public override void Dispose()
        {
            _filterCts?.Dispose();
            _popoutVideoPlayer?.Dispose();
        }

        private void SearchBoxSelect()
        {
            //Trace.WriteLine("Hello");
        }

        private async void Init()
        {
            //await PopulateZones(Zones, IsLoadingZones, SelectedServer);
            try
            {
                var cts = new CancellationTokenSource(30000);
                {
                    try
                    {
                        var _ = Task.Run(async () =>
                        {
                            try
                            {
                                cts.Token.ThrowIfCancellationRequested();
                                while (!cts.IsCancellationRequested && SelectedServer == null &&
                                       ServerItemMgr.GetServer() == null)
                                {
                                    await Task.Delay(500);
                                }

                                if (ServerItemMgr.GetServer() != null && SelectedServer == null)
                                {
                                    SelectedServer = ServerItemMgr.GetServer();
                                    try
                                    {
                                        try
                                        {
                                            SelectedZone =
                                                ZoneItemManager.Manager.GetZone();
                                            SelectedZone.SelectedZoneItem =
                                                ZoneItemManager.Manager.GetZone();
                                        }
                                        catch (Exception e)
                                        {
                                
                                        }
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }
                            }
                            catch (Exception e)
                            {
                            }
                            finally
                            {
                                cts?.Dispose();
                            }
                        }, cts.Token);
                    }
                    catch (Exception e)
                    {
                    }
                
                }
            }
            catch (Exception e)
            {
            }
        }

        //private static void SelectConnectedServer(ServerItemModel serverItemModel,
        //    ObservableCollection<ZoneItemModel> zoneItemModels, bool force = false)
        //{
        //    Application.Current.Dispatcher.InvokeAsync(() =>
        //    {
        //        try
        //        {
        //            if (serverItemModel == null || force)
        //            {
        //                var zone = zoneItemModels?.FirstOrDefault(o => o.ZoneId == ConnectedZoneId);
        //                if (zone != null)
        //                {
        //                    zone.IsExpanded = true;

        //                    serverItemModel =
        //                        ServerItemManager.Manager?.ItemCollection?.Values?.FirstOrDefault(o =>
        //                            o.ServerId == ConnectedServerId);
        //                }
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //        }
        //    });
        //}

        private void ClosePopout(object obj)
        {
            try
            {
                View?.TryUiAsync(() =>
                {
                    if (PopoutVideoPlayer != null)
                    {
                        PopoutVideoPlayer.Dispose();
                        PopoutVideoPlayer = null;
                    }
                });
            }
            catch (Exception e)
            {
                _log.Error(e);
            }
        }

        private void PlayPreviewPlayer()
        {
            try
            {
                if (((dynamic) SelectedZone.SelectedZoneItem).Value is CameraItemModel camera)
                {
                    var _ = View.TryUiAsync(async () =>
                    {
                        if (PopoutVideoPlayer != null)
                        {
                            PopoutVideoPlayer.VideoObject = camera;
                        }
                        else
                        {
                            PopoutVideoPlayer = new EdgeVideoControl
                            {
                                Width = 16 * 35,
                                Height = 9 * 35
                            };
                            await Task.Delay(100);
                            View.SizeChanged += (sender, args) =>
                            {
                                if (PopoutVideoPlayer?.ContentControl?.Content is IAirspaceContent content)
                                {
                                    content?.Redraw();
                                }
                            };
                            View.IsVisibleChanged += (sender, args) =>
                            {
                                if (PopoutVideoPlayer?.ContentControl?.Content is IAirspaceContent content)
                                {
                                    content?.UpdateLocation();
                                }
                            };
                            PopoutVideoPlayer.VideoObject = camera;
                        }

                        PopoutVideoPlayer.Model.PlayLive();
                    });
                }
            }
            catch (Exception e)
            {
            }
        }

        private void ZonesViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            if (FilterZonesText == string.Empty)
            {
                e.Accepted = true;
                return;
            }

            if (e.Item is KeyValuePair<Guid, ZoneItemModel> item)
            {
                var match = FilterZonesText.PartialContain(item.Value.Label, item.Value.ZoneId.ToString());
                if (!match)
                {
                    var childServers = ServerItemMgr.ItemCollection.Values.Where(o => o.ZoneId == item.Key);
                    foreach (var s in childServers)
                    {
                        if (FilterZonesText.PartialContain(s.Label, s.ServerId.ToString()))
                        {
                            match = true;
                            break;
                        }
                    }
                }

                e.Accepted = match;
            }
            else
            {
                e.Accepted = false;
            }
        }
    }

    internal enum EReportAction
    {
        GenerateInventoryReport,
        GenerateStatusReport,
        CancelReport,
        ExportReport,
    }
}