using System.Windows;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Authority;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    public partial class ActiveDirectoryManagementView : UserControl, IView
    {
        public static readonly DependencyProperty SelectedZoneProperty =
            DependencyProperty.RegisterAttached("SelectedZone", typeof(ServerItemModel),
                typeof(ActiveDirectoryManagementView), new PropertyMetadata(default(ServerItemModel)));

        public ActiveDirectoryManagementViewModel Model => DataContext as ActiveDirectoryManagementViewModel;

        public ActiveDirectoryManagementView()
        {
            InitializeComponent();
            Model.View = this;
        }

        public static ServerItemModel GetSelectedZone(UIElement element)
        {
            return (ServerItemModel) element.GetValue(SelectedZoneProperty);
        }

        public static void SetSelectedZone(UIElement element, ServerItemModel value)
        {
            element.SetValue(SelectedZoneProperty, value);
        }
    }
}