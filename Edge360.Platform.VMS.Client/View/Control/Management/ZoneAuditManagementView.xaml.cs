﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Common.Enums.Management;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using log4net;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    /// <summary>
    ///     Interaction logic for VolumeManagementView.xaml
    /// </summary>
    public partial class ZoneAuditManagementView : UserControl, IView
    {
        public ZoneAuditManagementViewModel Model => DataContext as ZoneAuditManagementViewModel;

        public ZoneAuditManagementView()
        {
            InitializeComponent();
            Model.View = this;
        }
    }

    public class ZoneAuditManagementViewModel : ZoneManagementViewModel, INotifyPropertyChanged
    {
        private string _filterZonesText = string.Empty;
        private ILog _log;
        private EdgeVideoControl _popoutVideoPlayer;
        private RelayCommand _refreshZonesCommand;
        private ServerItemModel _selectedServer;
        private RelayCommand _zoneActionConfigureCommand;
        private ObservableCollection<ZoneItemModel> _zones = new ObservableCollection<ZoneItemModel>();
        public ZoneAuditManagementView View;

        //public ObservableCollection<ZoneItemModel> Zones
        //{
        //    get => _zones;
        //    set
        //    {
        //        if (_zones != value)
        //        {
        //            _zones = value;
        //            OnPropertyChanged();
        //            ZonesViewSource.View.Refresh();
        //        }
        //    }
        //}

        public ZoneAuditManagementViewModel(ILog log = null, ZoneItemManager zoneItemManager = null)
        {
            _log = log;
            Init();
        }

        private async void Init()
        {
            //await ZoneItemMgr.RefreshZones();
            // await ZoneCameraManagementViewModel.PopulateZones(Zones, false, SelectedServer);
            try
            {
                SelectedZone =
                    ZoneItemManager.Manager.GetZone();
                SelectedZone.SelectedZoneItem =
                    ZoneItemManager.Manager.GetZone();
            }
            catch (Exception e)
            {
                                
            }
        }

        private bool CanExecute(object obj = null)
        {
            if (obj is EZoneActionConfigCommand command)
            {
                switch (command)
                {
                    case EZoneActionConfigCommand.CreateZone:

                        break;
                    case EZoneActionConfigCommand.EditZone:

                        break;
                    case EZoneActionConfigCommand.DisconnectZone:

                        break;
                    case EZoneActionConfigCommand.RefreshZone:
                        break;
                    case EZoneActionConfigCommand.RefreshAllZones:
                        break;
                    case EZoneActionConfigCommand.PreviewPopoutPlayer:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return !IsConfiguring;
        }


        private bool CanRefreshZones(object obj)
        {
            return true;
        }
    }
}