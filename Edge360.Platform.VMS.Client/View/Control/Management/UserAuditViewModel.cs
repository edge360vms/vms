﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using log4net;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    public class UserAuditViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private UserItemModel _user;
        private ILog _log;

        public UserItemModel User
        {
            get => _user;
            set
            {
                if (_user != value)
                {
                    _user = value;
                    OnPropertyChanged();
                }
            }
        }

        public UserAuditView View { get; set; }

        public UserAuditViewModel(ILog log = null)
        {
            _log = log;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void SetUser(UserItemModel userItem)
        {
            if (_user == null || _user.Id != userItem.Id)
            {
                _user = userItem;
                GetUserAudits();
            }
        }

        public async void GetUserAudits()
        {
            await Task.Run(async () =>
            {
                try
                {
                    if (_user != null)
                    {
                        // way of getting users...
                        var dbUsers = await GetManagerByZoneId().UserService.GetUsers();
                        if (dbUsers != null)
                        {
                            var existingUser = dbUsers.FirstOrDefault(c => c.UserId == _user.Id);
                            User = existingUser;
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            });     
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}