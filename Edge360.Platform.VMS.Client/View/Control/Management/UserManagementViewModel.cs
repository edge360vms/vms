﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.View.Control.Dialog;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Users;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using Telerik.Windows.Controls;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    public class UserManagementViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private bool _canCreateUser = true;
        private ICommand _createUserCommand;
        private string _filterUsersText = string.Empty;
        private UserItemModel _selectedUser;

        //private ObservableCollection<RoleNodeItemModel> _roles;
        private ObservableCollection<UserItemModel> _users;
        private ILog _log;

        public ICommand CreateUserCommand
        {
            get
            {
                if (_createUserCommand == null)
                {
                    _createUserCommand = new RelayCommand(
                        param => CreateUser(),
                        param => CanCreateUser()
                    );
                }

                return _createUserCommand;
            }
        }

        public string FilterUsersText
        {
            get => _filterUsersText;
            set
            {
                if (_filterUsersText != value)
                {
                    _filterUsersText = value;
                    OnPropertyChanged();
                    UsersViewSource.View.Refresh();
                }
            }
        }

        public UserItemModel SelectedUser
        {
            get => _selectedUser;
            set
            {
                if (_selectedUser != value)
                {
                    _selectedUser = value;
                    try
                    {
                        App.Current.Dispatcher.InvokeAsync(() =>
                        {
                            View.LoggingManagementView.Model.FilterAudit(_selectedUser.UserId);
                        });
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }

                    OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<UserItemModel> Users
        {
            get => _users;
            set
            {
                if (_users != value)
                {
                    _users = value;
                    OnPropertyChanged();
                }
            }
        }

        public CollectionViewSource UsersViewSource { get; set; } = new CollectionViewSource();

        public string ViewModelName { get; set; }
        public UserManagementView View { get; set; }

        public UserManagementViewModel(ILog log = null)
        {
            _log = log;
            Dispatcher.CurrentDispatcher.InvokeAsync(async () =>
            {
                BindingOperations.SetBinding(UsersViewSource,
                    CollectionViewSource.SourceProperty,
                    new Binding(nameof(Users)) {Source = this, Mode = BindingMode.OneWay});
                UsersViewSource.Filter += UsersViewSourceOnFilter;
            }, DispatcherPriority.Background);

            ViewModelName = "User Management";
            Initialize();
            // CommunicationManager.SignalRUsersUpdate += CommunicationManagerOnSignalRUsersUpdate;


            try
            {
                HubManager.SignalRClientEvent += async (e, zone, data) =>
                {
                    if (Equals(e, ESignalRClientEvent.UsersUpdated))
                    {
                        CommunicationManagerOnUsersUpdated(zone, data);
                    }
                };
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private async void Initialize()
        {
            try
            {
                _ = Task.Run(async () =>
                {
                    var usersLoaded = await GetUsers();
                    if (usersLoaded)
                    {
                        SelectedUser = Users.FirstOrDefault();
                    }
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        private bool CanCreateUser()
        {
            return _canCreateUser;
        }

        private void CreateUser()
        {
            var dialog = new CreateUserDialog();
            var mgmt = View.ParentOfType<ManagementView>();
            dialog.SetModalView(mgmt.DialogOverlayGrid.ModalContentPresenter);

            dialog.Closed += async (sender, args) =>
            {
                try
                {
                    if (dialog.Result &&
                        dialog.Model?.DialogItem is CreateUserDialogItemModel dialogItem &&
                        !string.IsNullOrEmpty(dialogItem.Username))
                    {
                        _canCreateUser = false;
                        if (dialogItem != null)
                        {
                            await GetManagerByZoneId().UserService.CreateUser(dialogItem.Username,
                                dialogItem.Password, dialogItem.DisplayName);
                        }

                        _canCreateUser = true;
                        Trace.WriteLine($"UserManagementViewmodel 190 InvalidateRequerySuggested");
                        //CommandManager.InvalidateRequerySuggested();
                        OnPropertyChanged(nameof(CreateUserCommand));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            };
        }

        private async Task<bool> GetUsers(List<UserDetails> response = null)
        {
            try
            {
                var dbUsers = response ?? await GetManagerByZoneId().UserService.GetUsers();
                if (dbUsers == null)
                {
                    return false;
                }

                var newUsers = new List<UserItemModel>();
                foreach (var userDetails in dbUsers) //.Where(o => o.Roles == role).ToList())
                {
                    var user = (UserItemModel) userDetails;
                    user.UserDeleted += UserOnUserDeleted;
                    newUsers.Add(user);
                }

                Users = new ObservableCollection<UserItemModel>(newUsers);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

            return true;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void CommunicationManagerOnUsersUpdated(Guid guid, string s)
        {
            await GetUsers();
        }

        private void UsersViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            e.Accepted = e.Item is UserItemModel item &&
                         FilterUsersText.PartialContain(item.Username, item.Descriptor?.DisplayName,
                             item.UserId?.ToString());
        }

        private void UserOnUserDeleted()
        {
            // GetUsers();
        }
    }
}