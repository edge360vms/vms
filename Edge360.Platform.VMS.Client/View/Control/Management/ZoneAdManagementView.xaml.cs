﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Common.Enums.Management;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Interfaces.Authority;
using Edge360.Platform.VMS.Common.Interfaces.Monitoring;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication.Polly;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Servers;
using log4net;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    /// <summary>
    ///     Interaction logic for VolumeManagementView.xaml
    /// </summary>
    public partial class ZoneAdManagementView : UserControl, IView
    {
        public ZoneAdManagementViewModel Model => DataContext as ZoneAdManagementViewModel;

        public ZoneAdManagementView()
        {
            InitializeComponent();
            Model.View = this;
        }
    }

    public class  ZoneAdManagementViewModel : ZoneManagementViewModel, INotifyPropertyChanged
    {
        private CollectionViewSource _serversViewSource = new CollectionViewSource();
        private ObservableCollection<ZoneItemModel> _zones = new ObservableCollection<ZoneItemModel>();
        public ZoneAdManagementView View;
        private ILog _log;

        public CollectionViewSource ServersViewSource
        {
            get => _serversViewSource;
            set
            {
                if (Equals(value, _serversViewSource))
                {
                    return;
                }

                _serversViewSource = value;
                OnPropertyChanged();
            }
        }

        //public ObservableCollection<ZoneItemModel> Zones
        //{
        //    get => _zones;
        //    set
        //    {
        //        if (_zones != value)
        //        {
        //            _zones = value;
        //            OnPropertyChanged();
        //            ZonesViewSource.View.Refresh();
        //        }
        //    }
        //}

        public ZoneAdManagementViewModel(ILog log = null, ZoneItemManager zoneItemManager = null)
        {
            _log = log;
            //if (zoneItemManager != null)
            //{
            //    ZoneItemMgr = zoneItemManager;
            //    //ZonesViewSource.Source = zoneItemManager.ItemCollection;
            //}

            //BindingOperations.SetBinding(ZonesViewSource,
            //    CollectionViewSource.SourceProperty,
            //    new Binding(nameof(Zones)) {Source = this, Mode = BindingMode.OneWay});
            //ZonesViewSource.Filter += ZonesViewSourceOnFilter;
            Init();
        }

        private async void Init()
        {
            try
            {
                _ = Task.Run(async () =>
                {
                    await ZoneItemManager.Manager.RefreshZones();
                    // await ZoneCameraManagementViewModel.PopulateZones(Zones, false, SelectedServer);

                    try
                    {
                        var serverItemModel = ServerItemMgr.GetServer();// Zones?.FirstOrDefault(o => o?.ZoneId == ConnectedZoneId)?.Servers.FirstOrDefault();
                        if (serverItemModel != null)
                        {
                            SelectedServer = serverItemModel;
                            try
                            {
                                SelectedZone =
                                    ZoneItemManager.Manager.GetZone();
                                SelectedZone.SelectedZoneItem =
                                    ZoneItemManager.Manager.GetZone();
                            }
                            catch (Exception e)
                            {
                                
                            }
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }


        private bool CanExecute(object obj = null)
        {
            if (obj is EZoneActionConfigCommand command)
            {
                switch (command)
                {
                    case EZoneActionConfigCommand.CreateZone:

                        break;
                    case EZoneActionConfigCommand.EditZone:

                        break;
                    case EZoneActionConfigCommand.DisconnectZone:

                        break;
                    case EZoneActionConfigCommand.RefreshZone:
                        break;
                    case EZoneActionConfigCommand.RefreshAllZones:
                        break;
                    case EZoneActionConfigCommand.PreviewPopoutPlayer:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return !IsConfiguring;
        }

        private bool CanRefreshZones(object obj)
        {
            return true;
        }

        private async void RefreshZones(object obj = null)
        {
            await Task.Run(async () =>
            {
                await ZoneItemManager.Manager.RefreshZones(true,obj != null);
            });
            //await ZoneCameraManagementViewModel.PopulateZones(Zones, false, SelectedServer);
            //await ZoneManagementViewModel.LoadServers(Servers);
            //await ZoneManagementViewModel.PopulateZones(Zones, false, SelectedServer);
        }

        //private void ZonesViewSourceOnFilter(object sender, FilterEventArgs e)
        //{
        //    e.Accepted = e.Accepted = e.Item is ZoneItemModel || e.Item is ServerItemModel item &&
        //        FilterZonesText.PartialContain(item.Label, item.Address, item.ZoneId.ToString());
        //}
    }
}