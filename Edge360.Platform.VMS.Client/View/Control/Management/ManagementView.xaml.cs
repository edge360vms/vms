﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ViewModel.View;
using log4net;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    /// <summary>
    ///     Interaction logic for AlertView.xaml
    /// </summary>
    public partial class ManagementView : UserControl, IView, IDisposable
    {
        ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public bool HasLoadedItem { get; set; }
        public ManagementViewModel Model => DataContext as ManagementViewModel;

        public ManagementView()
        {
            InitializeComponent();
            Model.View = this;
            Initialized += OnInitialized;
            Loaded += OnLoaded;
            DialogOverlayGrid.ModalContentPresenter.DataContextChanged +=
                ModalContentPresenterOnDataContextChanged;
        }

        private void ModalContentPresenterOnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Model.ModalContentPresenterOnDataContextChanged(sender, e);
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (!HasLoadedItem)
            {
                try
                {
                    HasLoadedItem = true;
                    var userPanelItem = UsersPanelItem.GetVisualParent<RadPanelBarItem>();
                    userPanelItem.IsSelected = true;
                    Model.SelectedPanelItem = UsersPanelItem;
                    // RadPanelBar.SelectedItem = userPanelItem;
                    // MainContentPresenter.Content = UsersPanelItem.Content;
                    //   Model.SelectedPanelItem = radPanelBarItem;
                }
                catch (Exception exception)
                {
                    _log?.Info(exception);
                }
            }
        }

        private void OnInitialized(object sender, EventArgs e)
        {
        }

        public void Dispose()
        {
            try
            {
                ZoneReportManagementView?.Dispose();
                foreach (var d in RadPanelBar.Items.OfType<IDisposable>())
                {
                    d.Dispose();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}