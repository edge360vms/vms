﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Common.Enums.Management;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Communication;
using log4net;
using Prism.Commands;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    public class ZoneManagementViewModel : INotifyPropertyChanged, IDisposable
    {
        public ZoneManagementViewModel(ILog log = null)
        {
            _log = log;
        }

        private static ILog _log;
        private string _filterZonesText = string.Empty;
        private RelayCommand _refreshZonesCommand;
        private ServerItemModel _selectedServer;
        private dynamic _selectedTreeItem;
        private ZoneItemModel _selectedZone;
        private RelayCommand _zoneActionConfigureCommand;
        private DelegateCommand _selectDefaultServerCommand;

        public string FilterZonesText
        {
            get => _filterZonesText;
            set
            {
                if (_filterZonesText != value)
                {
                    _filterZonesText = value;
                    OnPropertyChanged();
                    ZonesViewSource.View.Refresh();
                }
            }
        }

        public bool IsConfiguring { get; set; }
        public bool IsLoadingZones { get; set; }

        public RelayCommand RefreshZonesCommand =>
            _refreshZonesCommand ??= new RelayCommand(RefreshZones, CanRefreshZones);

        public ServerItemModel SelectedServer
        {
            get => _selectedServer;
            set
            {
                if (_selectedServer != value)
                {
                    _selectedServer = value;
                    if (_selectedServer != null)
                    {
                        try
                        {
                            SelectedZone = ZoneItemManager.Manager.ItemCollection
                                .FirstOrDefault(o => o.Key == _selectedServer.ZoneId)
                                .Value;
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    OnPropertyChanged();
                }
            }
        }

        public dynamic SelectedTreeItem
        {
            get => _selectedTreeItem;
            set
            {
                if (Equals(value, _selectedTreeItem))
                {
                    return;
                }

                _selectedTreeItem = value;
                try
                {
                    Task.Run(async () =>
                    {
                        if (_selectedTreeItem.Value is ZoneItemModel zone)
                        {
                            SelectedZone = zone;
                            SelectedServer =
                                ServerItemMgr?.ItemCollection?.FirstOrDefault(o => o.Value.ZoneId == zone.ZoneId).Value;
                            try
                            {
                                Dispatcher.CurrentDispatcher.InvokeAsync(async () =>
                                {
                                    SelectedZone.CameraCollectionViewSource.View.Refresh();
                                    SelectedZone.ServerCollectionViewSource.View.Refresh();
                                }, DispatcherPriority.Background);
                            }
                            catch (Exception e)
                            {
                            }
                        }
                   
                        else if (SelectedTreeItem is ServerItemModel serverItem)
                        {
                            SelectedServer = serverItem;
                        }
                        else if (_selectedTreeItem.Value is ServerItemModel server)
                        {
                            SelectedServer = server;
                        }
                        else if (SelectedTreeItem is ZoneItemModel zoneItem)
                        {
                            SelectedZone = zoneItem;

                            SelectedServer =
                                ServerItemMgr?.ItemCollection?.FirstOrDefault(o => o.Value.ZoneId == zoneItem.ZoneId).Value;
                            try
                            {
                                SelectedZone.CameraCollectionViewSource.View.Refresh();
                            }
                            catch (Exception e)
                            {
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }

                OnPropertyChanged();
            }
        }

        public ZoneItemModel SelectedZone
        {
            get => _selectedZone;
            set
            {
                if (Equals(value, _selectedZone))
                {
                    return;
                }

                _selectedZone = value;
                OnPropertyChanged();
            }
        }

        public ServerItemManager ServerItemMgr => ServerItemManager.Manager;

        public RelayCommand ZoneConfigureCommand =>
            _zoneActionConfigureCommand ??= new RelayCommand(ZoneConfigure, CanExecute);

        public CollectionViewSource ZonesViewSource { get; set; } = new CollectionViewSource
        {
            Source = ZoneItemManager.Manager.ItemCollection,
            SortDescriptions = {new SortDescription {PropertyName = "Label"}}
        };

        public DelegateCommand SelectDefaultServerCommand =>
            _selectDefaultServerCommand ??= new DelegateCommand(SelectDefaultServer );

        public virtual void Dispose()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        private bool CanExecute(object obj = null)
        {
            //if (obj is EZoneActionConfigCommand command)
            //{
            //    switch (command)
            //    {
            //        case EZoneActionConfigCommand.CreateZone:
            //            break;
            //        case EZoneActionConfigCommand.EditZone:
            //            break;
            //        case EZoneActionConfigCommand.DisconnectZone:
            //            break;
            //        case EZoneActionConfigCommand.RefreshZone:
            //            break;
            //        case EZoneActionConfigCommand.RefreshAllZones:
            //            break;
            //        case EZoneActionConfigCommand.PreviewPopoutPlayer:
            //            break;
            //    }
            //}

            return true; //!IsConfiguring;
        }

        public virtual async void ZoneConfigure(object obj = null)
        {
            IsConfiguring = true;
            if (obj is EZoneActionConfigCommand command)
            {
                switch (command)
                {
                    case EZoneActionConfigCommand.RefreshZone:
                        // await ZoneItemManager.Manager.RefreshZone()
                        break;
                    case EZoneActionConfigCommand.RefreshAllZones:
                        RefreshZones();
                        break;
                    case EZoneActionConfigCommand.PreviewPopoutPlayer:
                        break;
                }
            }

            IsConfiguring = false;
        }


        public static async Task LoadZones<T>(ObservableCollection<T> zones, bool includeServers = false)
            where T : ZoneItemModel, new()
        {
            try
            {
                await Task.Run(async () =>
                {
                    zones.Clear();

                    if (!includeServers)
                    {
                        var zoneList = (await CommunicationManager.GetManagerByZoneId().GlobalZoneService
                            .ListZones())?.Zones;
                        if (zoneList != null)
                        {
                            foreach (var z in zoneList)
                            {
                                var zoneItemModel = new T
                                {
                                    Label = z.ZoneName,
                                    ZoneId = z.ZoneId,
                                };

                                zones.Add(zoneItemModel);
                            }
                        }
                    }
                    else
                    {
                        var listServersResponse = await ServerItemManager.Manager.ListServers();
                        var serverList = listServersResponse?.Servers;
                        if (serverList != null)
                        {
                            var zonesDictionary = serverList.GroupBy(o => o.ZoneId).ToDictionary(
                                o => o.Key,
                                model => serverList.Where(o => o.ZoneId == model.Key));
                            foreach (var z in zonesDictionary)
                            {
                                var zoneName = "";
                                foreach (var s in z.Value) //.ToList())
                                {
                                    zoneName = s.ServerName;
                                    //servers.Add(new ServerItemModel
                                    //{
                                    //    Address = s.HostName,
                                    //    ConnectionStatus = EConnectionStatus.None,
                                    //    Description = null,
                                    //    Label = s.ServerName,
                                    //    ServerId = s.ServerId,
                                    //    ZoneId = z.Key
                                    //});
                                }

                                var zoneItemModel = new T
                                {
                                    Label = zoneName,

                                    //Address = $"{s.HostName}:{s.PortApi}", Label = $"{s.ServerName}",
                                    //ServerId = s.ServerId,
                                    //ConnectionStatus = EConnectionStatus.None,
                                    //Username = SettingsManager.Settings.Login.Credentials.UserName,
                                    //Password = SettingsManager.Settings.Login.Credentials.Password,
                                    ZoneId = z.Key,
                                };

                                zones.Add(zoneItemModel);
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        private bool CanRefreshZones(object obj)
        {
            return true;
        }

        private async void RefreshZones(object obj = null)
        {
            try
            {
                await Task.Run(async () =>
                {
                    await ZoneItemManager.Manager.RefreshZones();
                });
                SelectDefaultServer();
            }
            catch (Exception e)
            {
                
            }
            //await LoadZones(Zones);
        }

        private void SelectDefaultServer()
        {
            try
            {
                if (SelectedZone == null)
                {
                    var connectedZone = ZoneItemManager.Manager.ItemCollection.FirstOrDefault(o =>
                        o.Key == CommunicationManager.ConnectedZoneId).Value;
                    if (connectedZone != null)
                    {
                        SelectedTreeItem = connectedZone;
                        SelectedZone =
                            connectedZone;
                    }
                }
            }
            catch (Exception e)
            {
                
            }
        }
    }
}