﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.ItemModel.Stream;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Platform.VMS.UI.Common.View.Dialog.Overlay;
using Edge360.Vms.AuthorityService.SharedModel.Enums;
using Prism.Commands;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Management.Permission
{
    /// <summary>
    ///     Interaction logic for GroupPermissionView.xaml
    /// </summary>
    public partial class GroupPermissionView : UserControl
    {
        public static readonly DependencyProperty GroupManagementProperty =
            DependencyProperty.Register("GroupManagement", typeof(GroupManagementViewModel),
                typeof(GroupPermissionView),
                new PropertyMetadata(default(GroupManagementViewModel), PropertyChangedCallback));

        private ObservableCollection<CameraItemModel> _selectedCameras;
        private CancellationTokenSource _filterCts;

        public GroupManagementViewModel GroupManagement
        {
            get => (GroupManagementViewModel) GetValue(GroupManagementProperty);
            set => SetValue(GroupManagementProperty, value);
        }

        public GroupPermissionViewModel Model => DataContext as GroupPermissionViewModel;

        public GroupPermissionView()
        {
            InitializeComponent();
            CameraTreeView.CameraTree.SelectionMode = SelectionMode.Extended;
            CameraTreeView.CameraTree.SelectionChanged += CameraTreeOnSelectionChanged;

            Model.View = this;
        }

        private void SelectedGroupOnPropertyChanged()
        {
            try
            {
                if (IsVisible)
                {
                    var model = Model; //.GroupPermissionActionCommand;
                    Task.Run(() =>
                    {
                        try
                        {
                            if (model != null)
                            {
                                model.GroupPermissionAction(EGroupPermissionAction.RefreshLayouts, false);
                                model.GroupPermissionAction(EGroupPermissionAction.RefreshFolders, false);
                                model.GroupPermissionAction(EGroupPermissionAction.RefreshCameras, false);
                            }
                        }
                        catch (Exception e)
                        {
                        
                        }
                    });
                }
            }
            catch (Exception e)
            {
                
            }
        }

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is GroupPermissionView view)
            {
                view.Model.GroupManagementViewModel = view.GroupManagement;
                view.GroupManagement.SelectedGroupChanged += view.SelectedGroupOnPropertyChanged;
            }
        }

        private void CameraTreeOnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedNodes = CameraTreeView.CameraTree.SelectedItems.OfType<NodeItemModel>().ToList();

            Model.SelectedNodes =
                selectedNodes.Count > 0 ? new ObservableCollection<NodeItemModel>(selectedNodes) : null;

            var selectedCameraNodes = CameraTreeView.CameraTree.SelectedItems.OfType<CameraItemModel>().ToList();

            Model.SelectedCameraNodes =
                selectedCameraNodes.Count > 0 ? new ObservableCollection<CameraItemModel>(selectedCameraNodes) : null;


            _filterCts?.Cancel();
            _filterCts = new CancellationTokenSource();
            var cts = _filterCts;
            Task.Run(async () =>
            {
                await Task.Delay(125);
                if (!cts.IsCancellationRequested)
                {
                    App.Current?.Dispatcher?.InvokeAsync(() =>
                    {
                        Model.NodeObjectCollectionViewSource?.View?.Refresh();
                        Model.NodeCameraObjectCollectionViewSource?.View?.Refresh();
                    });
                }
            }, cts.Token);
        }

        private async void CameraTreeSelection_OnLoadOnDemand(object sender, RadRoutedEventArgs e)
        {
            try
            {
                if (e.OriginalSource is RadTreeViewItem treeItem)
                {
                    try
                    {
                        treeItem.IsLoadingOnDemand = true;
                        dynamic keyvalue = treeItem.Item;

                        if (keyvalue.Value is ServerItemModel serverItem)
                        {
                            var listCameras = await CameraItemModelManager.Manager.RefreshCameras((Guid)serverItem.ZoneId);
                            serverItem.Archiver ??= new ServerConfigItemModel();
                            serverItem.Archiver.ActiveCameras = new ObservableCollection<CameraItemModel>(
                                listCameras?.Select(o => (CameraItemModel) o) ?? new List<CameraItemModel>());
                        }
                        else if (treeItem.Item is ZoneItemModel zoneItem)
                        {
                        }
                        else if (treeItem.Item is CameraItemModel cameraItem)
                        {
                        }
                    }
                    catch (Exception exception)
                    {
                    }
                    finally
                    {
                        await Task.Delay(100);
                        treeItem.IsLoadingOnDemand = false;
                    }
                }
            }
            catch (Exception exception)
            {
            }
        }


        private void CameraTreeSelection_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selectedCams = ZoneItemSelectionTree.SelectedItems.OfType<CameraItemModel>().ToList();
                //var selectedLayouts = ZoneItemSelectionTree.SelectedItems.OfType<LayoutItemModel>();
                //var selectedZones = ZoneItemSelectionTree.SelectedItems.OfType<ZoneItemModel>();
                var dictionaryObjects = ZoneItemSelectionTree.SelectedItems
                    .Where(s => s != null && s.GetType().GetGenericArguments().Length > 0).Select(o =>
                    {
                        var value = (dynamic) o;
                        return value.Value;
                    }).ToList();
                var selectedServers = dictionaryObjects.OfType<ServerItemModel>().ToList();
                //   var selectedLayouts = dictionaryObjects.OfType<LayoutItemModel>().ToList();
                //    var selectedNodes = dictionaryObjects.OfType<NodeItemModel>().ToList();

                Model.SelectedCameras = selectedCams.Count > 0
                    ? new ObservableCollection<CameraItemModel>(selectedCams)
                    : null;
                Model.SelectedServers = selectedServers.Count > 0
                    ? new ObservableCollection<ServerItemModel>(selectedServers)
                    : null;
                //   Model.SelectedNodes =  selectedNodes.Count > 0 ? new ObservableCollection<NodeItemModel>(selectedNodes) : null;
                //Model.SelectedLayouts = selectedLayouts.Count > 0
                //    ? new ObservableCollection<LayoutItemModel>(selectedLayouts)
                //    : null;
                _filterCts?.Cancel();
                _filterCts = new CancellationTokenSource();
                var cts = _filterCts;
                Task.Run(async () =>
                {
                    await Task.Delay(125);
                    if (!cts.IsCancellationRequested)
                    {
                        App.Current?.Dispatcher?.InvokeAsync(() =>
                        {
                            Model.StreamProfileCollectionViewSource.View.Refresh();
                            Model.CameraProfileCollectionViewSource.View.Refresh();
                            Model.RecordingProfileCollectionViewSource.View.Refresh();
                            Model.PresetProfileCollectionViewSource.View.Refresh();
                            // Model.LayoutCollectionViewSource.View.Refresh();
                            Model.NodeObjectCollectionViewSource.View.Refresh();
                            Model.NodeCameraObjectCollectionViewSource.View.Refresh();
                        });
                    }
                }, cts.Token);
            }
            catch (Exception exception)
            {
            }

            //   Model.SelectedCameras = selectedCams.Count > 0 ? new ObservableCollection<CameraItemModel>(selectedCams) : null;
        }

        private void FrameworkElement_OnLoaded(object sender, EventArgs eventArgs)
        {
            try
            {
                if (sender is RadComboBox comboBox)
                {
                    var gridView = comboBox.GetVisualParent<RadGridView>();

                    try
                    {
                        if (!gridView.SelectedItems.Contains(comboBox.DataContext))
                        {
                            gridView.SelectedItems.Add(comboBox.DataContext);
                            gridView.Select(gridView.SelectedItems);
                        }
                    }
                    catch (Exception e)
                    {
                    }


                    var cameraPermissionItemModels =
                        comboBox.Items.OfType<AvailableCameraPermissionItemModel>().ToList();

                    if (cameraPermissionItemModels.Count > 0)
                    {
                        var selectedCameras = gridView.SelectedItems.Select(o =>
                        {
                            dynamic keyPair = o;
                            return keyPair.Value;
                        }).OfType<CameraItemModel>().ToList();
                        foreach (var p in cameraPermissionItemModels)
                        {
                            p.IsChecked = selectedCameras.Any(o => o != null &&
                                                                   ((EPermissionsCamera) o.Permissions.Value).HasFlag(
                                                                       p.CameraPermission));
                        }
                    }

                    var folderPermissionItemModels =
                        comboBox.Items.OfType<AvailableFolderPermissionItemModel>().ToList();

                    if (folderPermissionItemModels.Count > 0)
                    {
                        var selectedNodes = gridView.SelectedItems.Select(o =>
                        {
                            dynamic keyPair = o;
                            return keyPair.Value;
                        }).OfType<NodeItemModel>().ToList();
                        foreach (var p in folderPermissionItemModels)
                        {
                            p.IsChecked = selectedNodes.Any(o => o != null &&
                                                                 ((EPermissionsFolder) o.Permissions.Value).HasFlag(
                                                                     p.FolderPermission));
                        }
                    }

                    var layoutPermissionItemModels =
                        comboBox.Items.OfType<AvailableLayoutPermissionItemModel>().ToList();

                    if (layoutPermissionItemModels.Count > 0)
                    {
                        var selectedLayouts = gridView.SelectedItems.Select(o =>
                        {
                            dynamic keyPair = o;
                            return keyPair.Value;
                        }).OfType<LayoutItemModel>().ToList();
                        foreach (var p in layoutPermissionItemModels)
                        {
                            p.IsChecked = selectedLayouts.Any(o => o != null &&
                                                                   ((EPermissionsLayout) o.Permissions.Value).HasFlag(
                                                                       p.LayoutPermission));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
            }
        }

        //private void UIElement_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    if (sender is RadComboBox comboBox)
        //    {
        //        var grid = comboBox.GetVisualParent<RadGridView>();
        //        if (((dynamic)comboBox.DataContext).Value is CameraItemModel camera)
        //        {
        //            if (!grid.SelectedItems.Contains(camera))
        //            {
        //                grid.SelectedItems.Add(camera);
        //            }
        //        }
        //    }
        //}
        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (sender is RadComboBox box)
                {
                    //Command="{Binding CheckPermissionCommand}"
                    //CommandParameter="{Binding RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type telerik:RadComboBox}}}"

                    foreach (var a in e.AddedItems.OfType<AvailableFolderPermissionItemModel>()
                        .Union(e.RemovedItems.OfType<AvailableFolderPermissionItemModel>()).ToList())
                    {
                        a.CheckPermissionCommand.Execute(box);
                        // var b =;
                    }

                    foreach (var a in e.AddedItems.OfType<AvailableLayoutPermissionItemModel>()
                        .Union(e.RemovedItems.OfType<AvailableLayoutPermissionItemModel>()).ToList())
                    {
                        a.CheckPermissionCommand.Execute(box);
                        // var b =;
                    }

                    foreach (var a in e.AddedItems.OfType<AvailableCameraPermissionItemModel>()
                        .Union(e.RemovedItems.OfType<AvailableCameraPermissionItemModel>()).ToList())
                    {
                        a.CheckPermissionCommand.Execute(box);
                        // var b =;
                    }
                }
            }
            catch (Exception exception)
            {
                
            }
        }

        private void LayoutSelector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                // var selectedCams = ZoneItemSelectionTree.SelectedItems.OfType<CameraItemModel>().ToList();
                var dictionaryObjects = LayoutItemSelectionListBox.SelectedItems.OfType<object>()
                    .Where(s => s != null && s.GetType().GetGenericArguments().Length > 0).Select(o =>
                    {
                        var value = (dynamic) o;
                        return value.Value;
                    }).ToList();
                var selectedLayouts = dictionaryObjects.OfType<LayoutItemModel>().ToList();

                Model.SelectedLayouts = selectedLayouts.Count > 0
                    ? new ObservableCollection<LayoutItemModel>(selectedLayouts)
                    : null;
                Model.LayoutCollectionViewSource.View.Refresh();
                Model.LayoutGridCollectionViewSource.View.Refresh();
            }
            catch (Exception exception)
            {
            }
        }
    }

    public enum EPermissionAction
    {
        Delete,
        Add,
        Refresh,
    }

    public class GroupPermissionViewModel : INotifyPropertyChanged
    {
        private CameraItemModelManager _cameraItemModelManager;
        private GroupManagementViewModel _groupManagementViewModel;
        private DelegateCommand<object> _groupPermissionActionCommand;
        private bool _isRefreshingCameras;
        private bool _isRefreshingFolders;
        private bool _isRefreshingLayouts;
        private LayoutItemManager _layoutItmMgr;
        private NodeObjectItemManager _nodeObjectItemMgr;
        private DelegateCommand<object> _permissionActionCommand;
        private PresetItemManager _presetItemManager;
        private RecordingProfileItemManager _recordingProfileItemManager;
        private CameraItemModel _selectedCamera;
        private CameraItemModel _selectedCameraProfile;
        private object _selectedItem;
        private ObservableCollection<LayoutItemModel> _selectedLayouts;
        private ObservableCollection<NodeItemModel> _selectedNodes;
        private ServerItemModel _selectedServer;
        private ObservableCollection<ServerItemModel> _selectedServers;
        private object _selectedTreeItem;
        private object _selectedViewSource;
        private ZoneItemModel _selectedZone;
        private StreamProfileItemManager _streamProfileItemManager;
        private GroupPermissionView _view;
        private ObservableCollection<ZoneItemModel> _zones = new ObservableCollection<ZoneItemModel>();
        private ObservableCollection<CameraItemModel> _selectedCameraNodes = new ObservableCollection<CameraItemModel>();

        public ObservableCollection<AvailableCameraPermissionItemModel> AvailableCameraPermission { get; set; } =
            new ObservableCollection<AvailableCameraPermissionItemModel>();

        public ObservableCollection<AvailableFolderPermissionItemModel> AvailableFolderPermission { get; set; } =
            new ObservableCollection<AvailableFolderPermissionItemModel>();

        public ObservableCollection<AvailableLayoutPermissionItemModel> AvailableLayoutPermission { get; set; } =
            new ObservableCollection<AvailableLayoutPermissionItemModel>();

        public CameraItemModelManager CameraItemModelManager
        {
            get => _cameraItemModelManager;
            set
            {
                if (Equals(value, _cameraItemModelManager))
                {
                    return;
                }

                _cameraItemModelManager = value;
                OnPropertyChanged();
            }
        }

        public CollectionViewSource CameraProfileCollectionViewSource { get; set; }

        public GroupManagementViewModel GroupManagementViewModel
        {
            get => _groupManagementViewModel;
            set
            {
                if (Equals(value, _groupManagementViewModel))
                {
                    return;
                }

                _groupManagementViewModel = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand<object> GroupPermissionActionCommand =>
            _groupPermissionActionCommand ??= new DelegateCommand<object>(o => GroupPermissionAction(o, true));

        public bool IsRefreshingCameras
        {
            get => _isRefreshingCameras;
            set
            {
                if (value == _isRefreshingCameras)
                {
                    return;
                }

                _isRefreshingCameras = value;
                OnPropertyChanged();
            }
        }

        public bool IsRefreshingFolders
        {
            get => _isRefreshingFolders;
            set
            {
                if (value == _isRefreshingFolders)
                {
                    return;
                }

                _isRefreshingFolders = value;
                OnPropertyChanged();
            }
        }

        public bool IsRefreshingLayouts
        {
            get => _isRefreshingLayouts;
            set
            {
                if (value == _isRefreshingLayouts)
                {
                    return;
                }

                _isRefreshingLayouts = value;
                OnPropertyChanged();
            }
        }

        public CollectionViewSource LayoutCollectionViewSource { get; set; } = new CollectionViewSource();

        public CollectionViewSource LayoutGridCollectionViewSource { get; set; }

        public LayoutItemManager LayoutItmMgr
        {
            get => _layoutItmMgr;
            set
            {
                if (Equals(value, _layoutItmMgr))
                {
                    return;
                }

                _layoutItmMgr = value;
                OnPropertyChanged();
            }
        }
        public CollectionViewSource NodeCameraObjectCollectionViewSource { get; set; } = new CollectionViewSource();

        public CollectionViewSource NodeObjectCollectionViewSource { get; set; } = new CollectionViewSource();

        public NodeObjectItemManager NodeObjectItemMgr
        {
            get => _nodeObjectItemMgr;
            set
            {
                if (Equals(value, _nodeObjectItemMgr))
                {
                    return;
                }

                _nodeObjectItemMgr = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand<object> PermissionActionCommand =>
            _permissionActionCommand ??= new DelegateCommand<object>(PermissionAction);

        public PermissionItemManager PermissionItemManager { get; set; }

        public CollectionViewSource PermissionViewSource { get; set; }

        public PresetItemManager PresetItemManager
        {
            get => _presetItemManager;
            set
            {
                if (Equals(value, _presetItemManager))
                {
                    return;
                }

                _presetItemManager = value;
                OnPropertyChanged();
            }
        }

        public CollectionViewSource PresetProfileCollectionViewSource { get; set; }


        public CollectionViewSource RecordingProfileCollectionViewSource { get; set; }

        public RecordingProfileItemManager RecordingProfileItemManager
        {
            get => _recordingProfileItemManager;
            set
            {
                if (Equals(value, _recordingProfileItemManager))
                {
                    return;
                }

                _recordingProfileItemManager = value;
                OnPropertyChanged();
            }
        }

        public CameraItemModel SelectedCameraProfile
        {
            get => _selectedCameraProfile;
            set
            {
                if (Equals(value, _selectedCameraProfile))
                {
                    return;
                }

                _selectedCameraProfile = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<CameraItemModel> SelectedCameras { get; set; }

        public object SelectedItem
        {
            get => _selectedItem;
            set
            {
                if (Equals(value, _selectedItem))
                {
                    return;
                }

                _selectedItem = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<LayoutItemModel> SelectedLayouts
        {
            get => _selectedLayouts;
            set
            {
                if (Equals(value, _selectedLayouts))
                {
                    return;
                }

                _selectedLayouts = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<CameraItemModel> SelectedCameraNodes
        {
            get => _selectedCameraNodes;
            set
            {
                if (Equals(value, _selectedCameraNodes))
                {
                    return;
                }

                _selectedCameraNodes = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<NodeItemModel> SelectedNodes
        {
            get => _selectedNodes;
            set
            {
                if (Equals(value, _selectedNodes))
                {
                    return;
                }

                _selectedNodes = value;
                OnPropertyChanged();
            }
        }

        //public CameraItemModel SelectedCamera
        //{
        //    get => _selectedCamera;
        //    set
        //    {
        //        if (Equals(value, _selectedCamera)) return;
        //        _selectedCamera = value;
        //        OnPropertyChanged();
        //        StreamProfileCollectionViewSource.View.Refresh();
        //        CameraProfileCollectionViewSource.View.Refresh();
        //        RecordingProfileCollectionViewSource.View.Refresh();
        //        PresetProfileCollectionViewSource.View.Refresh();
        //    }
        //}

        public ServerItemModel SelectedServer
        {
            get => _selectedServer;
            set
            {
                if (Equals(value, _selectedServer))
                {
                    return;
                }

                _selectedServer = value;
                RefreshItems();
                OnPropertyChanged();
            }
        }

        public ObservableCollection<ServerItemModel> SelectedServers
        {
            get => _selectedServers;
            set
            {
                if (Equals(value, _selectedServers))
                {
                    return;
                }

                _selectedServers = value;
                OnPropertyChanged();
            }
        }

        public object SelectedTreeItem
        {
            get => _selectedTreeItem;
            set
            {
                if (Equals(value, _selectedTreeItem))
                {
                    return;
                }

                _selectedTreeItem = value;
                try
                {
                    dynamic selectedKeyPair = _selectedTreeItem;
                    //   if (_selectedTreeItem is CameraItemModel cameraItem)
                    //   {
                    ////       SelectedCamera = cameraItem;
                    //   }
                    //   else if (selectedKeyPair?.Value is CameraItemModel camera)
                    //   {
                    //   //    SelectedCamera = camera;
                    //   }
                    //   else 
                    if (_selectedTreeItem is ServerItemModel server)
                    {
                        SelectedServer = server;
                        SelectedZone = ZoneItemMgr.ItemCollection.FirstOrDefault(o => o.Key == server.ZoneId).Value;
                    }
                    else if (_selectedTreeItem is ZoneItemModel zone)
                    {
                        SelectedZone = zone;
                    }
                }
                catch (Exception e)
                {
                }

                OnPropertyChanged();
            }
        }

        public object SelectedViewSource
        {
            get => _selectedViewSource;
            set
            {
                if (Equals(value, _selectedViewSource))
                {
                    return;
                }

                _selectedViewSource = value;
                OnPropertyChanged();
            }
        }

        public ZoneItemModel SelectedZone
        {
            get => _selectedZone;
            set
            {
                if (Equals(value, _selectedZone))
                {
                    return;
                }

                _selectedZone = value;


                OnPropertyChanged();
            }
        }

        public CollectionViewSource StreamProfileCollectionViewSource { get; set; }

        public StreamProfileItemManager StreamProfileItemManager
        {
            get => _streamProfileItemManager;
            set
            {
                if (Equals(value, _streamProfileItemManager))
                {
                    return;
                }

                _streamProfileItemManager = value;
                OnPropertyChanged();
            }
        }

        public GroupPermissionView View
        {
            get => _view;
            set
            {
                if (Equals(value, _view))
                {
                    return;
                }

                _view = value;
                OnPropertyChanged();
            }
        }

        //public ObservableCollection<ZoneItemModel> Zones
        //{
        //    get => _zones;
        //    set
        //    {
        //        if (Equals(value, _zones))
        //        {
        //            return;
        //        }

        //        _zones = value;
        //        OnPropertyChanged();
        //        ZonesViewSource.View.Refresh();
        //    }
        //}

        public CollectionViewSource ZonesViewSource { get; set; }

        private DialogOverlayGrid _dialogOverlay =>
            TabUtil.FindFocusedTab<ManagementTab>()?.ManagementView?.DialogOverlayGrid;

        public GroupPermissionViewModel(CameraItemModelManager cameraItemModelManager = null,
            PermissionItemManager permissionItemManager = null,
            RecordingProfileItemManager recordingProfileItemManager = null, ZoneItemManager zoneItemManager = null,
            StreamProfileItemManager streamProfileItemManager = null, PresetItemManager presetItemManager = null,
            NodeObjectItemManager nodeObjectItemManager = null, LayoutItemManager layoutItemManager = null)
        {
            ZoneItemMgr = zoneItemManager;
            PermissionItemManager = permissionItemManager;
            CameraItemModelManager = cameraItemModelManager;
            RecordingProfileItemManager = recordingProfileItemManager;
            StreamProfileItemManager = streamProfileItemManager;
            PresetItemManager = presetItemManager;
            NodeObjectItemMgr = nodeObjectItemManager;
            LayoutItmMgr = layoutItemManager;
            if (LayoutItmMgr != null)
            {
                LayoutGridCollectionViewSource = new CollectionViewSource
                    {Source = LayoutItmMgr.ItemCollection};
                LayoutGridCollectionViewSource.Filter += LayoutGridCollectionViewSourceOnFilter;

                LayoutCollectionViewSource = new CollectionViewSource
                    {Source = LayoutItmMgr.ItemCollection};
                LayoutCollectionViewSource.Filter += LayoutCollectionViewSourceOnFilter;
            }

            if (NodeObjectItemMgr != null)
            {
                NodeObjectCollectionViewSource.Source = NodeObjectItemMgr.ItemCollection;
                NodeObjectCollectionViewSource.Filter += NodeObjectCollectionViewSourceOnFilter;

            }

            if (PermissionItemManager != null)
            {
                PermissionViewSource = new CollectionViewSource
                    {Source = PermissionItemManager.ItemCollection};
                PermissionViewSource.Filter += PermissionViewSourceOnFilter;
            }

            if (PresetItemManager != null)
            {
                PresetProfileCollectionViewSource = new CollectionViewSource
                    {Source = PresetItemManager.ItemCollection};
                PresetProfileCollectionViewSource.Filter += PresetProfileCollectionViewSourceOnFilter;
            }

            if (StreamProfileItemManager != null)
            {
                StreamProfileCollectionViewSource = new CollectionViewSource
                    {Source = StreamProfileItemManager.ItemCollection};
                StreamProfileCollectionViewSource.Filter += StreamProfileCollectionViewSourceOnFilter;
            }

            if (RecordingProfileItemManager != null)
            {
                RecordingProfileCollectionViewSource = new CollectionViewSource
                    {Source = RecordingProfileItemManager.ItemCollection};
                RecordingProfileCollectionViewSource.Filter += RecordingProfileCollectionViewSourceOnFilter;
            }

            if (CameraItemModelManager != null)
            {
                CameraProfileCollectionViewSource = new CollectionViewSource
                    {Source = CameraItemModelManager.ItemCollection};
                CameraProfileCollectionViewSource.Filter += CameraProfileCollectionViewSourceOnFilter;
                NodeCameraObjectCollectionViewSource.Source = CameraItemModelManager.ItemCollection;
                NodeCameraObjectCollectionViewSource.Filter += NodeCameraObjectCollectionViewSourceOnFilter;
            }

            if (zoneItemManager != null)
            {
                ZonesViewSource = new CollectionViewSource(){Source = zoneItemManager.ItemCollection};
                ZonesViewSource.Filter += ZonesViewSourceOnFilter;
            }

            Init();
        }

        private void NodeCameraObjectCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            try
            {
                if (SelectedCameraNodes != null)
                {
                    e.Accepted = e.Item is KeyValuePair<Guid, CameraItemModel> model &&
                                 SelectedCameraNodes.Any(c => c.Id == model.Value.Id);
                }
                else
                {
                    e.Accepted = false;
                }
            }
            catch (Exception exception)
            {
                e.Accepted = false;
            }
        }

        public ZoneItemManager ZoneItemMgr { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public async void GroupPermissionAction(object obj, bool ignoreCache = true)
        {
            try
            {
                if (obj is EGroupPermissionAction action)
                {
                    var groupItemModel = GroupManagementViewModel.SelectedGroup;
                    if (groupItemModel == null)
                    {
                        return;
                    }
                    var selectedGroupId = groupItemModel.GroupId ?? Guid.Empty;
                    var itemManagerRequestParameters = ignoreCache ? new ItemManagerRequestParameters
                        {GroupId = selectedGroupId, ForceRemoteQuery = true, AcceptedCacheDuration = TimeSpan.Zero} : null;
                    switch (action)
                    {
                        case EGroupPermissionAction.RefreshSummary:
                            await RecordingProfileItemManager.RefreshProfiles();
                            if (SelectedCameras == null)
                            {
                                var server = SelectedServers.FirstOrDefault();
                                var zoneCameras =
                                    CameraItemModelManager.ItemCollection.Values.Where(o =>
                                        server != null && o.ZoneId == server.ZoneId);
                                foreach (var c in zoneCameras)
                                {
                                    await StreamProfileItemManager.RefreshProfiles(c, itemManagerRequestParameters);
                                }
                            }
                            else
                            {
                                foreach (var c in SelectedCameras)
                                {
                                    await StreamProfileItemManager.RefreshProfiles(c, itemManagerRequestParameters);
                                }
                            }
                            
                            break;
                        case EGroupPermissionAction.RefreshCameras:
                            try
                            {
                                IsRefreshingCameras = true;
                                //if (SelectedCameras?.Count > 0)
                                //{
                                //    var updatePermissions = await PermissionItemManager.RequestPermissionsBatch(
                                //        selectedGroupId, SelectedCameras.Select(o => (Guid)o.ObjectId).ToArray());

                                //}

                                await CameraItemModelManager.RefreshCameras(parameters:
                                    itemManagerRequestParameters);
                            }
                            finally
                            {
                                IsRefreshingCameras = false;
                            }

                            break;
                        case EGroupPermissionAction.RefreshFolders:
                            try
                            {
                                IsRefreshingFolders = true;
                                if (SelectedCameraNodes != null)
                                {
                                    if (SelectedCameraNodes.Count > 0)
                                    {
                                        await CameraItemModelManager.RefreshCameras(parameters:
                                            itemManagerRequestParameters);
                                    }
                                }

                                if (SelectedNodes != null)
                                {
                                    var permissions =
                                        await PermissionItemManager.RequestPermissions((Guid) groupItemModel.GroupId,
                                            Guid.Empty);

                                    foreach (var n in SelectedNodes)
                                    {
                                        try
                                        {
                                            var node = await NodeObjectItemMgr.RefreshNode((Guid) n.ObjectId,
                                                new ItemManagerRequestParameters
                                                {
                                                    AcceptedCacheDuration = TimeSpan.FromSeconds(3),
                                                    CacheDuration = TimeSpan.FromSeconds(3)
                                                });
                                            var nodePermission = PermissionItemManager.ItemCollection
                                                                     .FirstOrDefault(o =>
                                                                         o.Key.Item2 == selectedGroupId &&
                                                                         o.Key.Item1 == node.ObjectId).Value ??
                                                                 new PermissionItemModel();
                                            n.Permissions = nodePermission;
                                            node.Permissions = nodePermission;
                                            //var nodeChildren =
                                            //    await NodeObjectItemMgr.RefreshNodeChildren(
                                            //        new ItemManagerRequestParameters
                                            //        {
                                            //            AcceptedCacheDuration = TimeSpan.FromSeconds(3),
                                            //            CacheDuration = TimeSpan.FromSeconds(3)
                                            //        }, n.ParentId);
                                            //foreach (var child in nodeChildren)
                                            //{
                                            //    var childPermission = PermissionItemManager.ItemCollection
                                            //                              .FirstOrDefault(o =>
                                            //                                  o.Key.Item2 == selectedGroupId &&
                                            //                                  o.Key.Item1 == child.ObjectId).Value ??
                                            //                          new PermissionItemModel();
                                            //    child.Permissions = childPermission;
                                            //}
                                        }
                                        catch (Exception e)
                                        {
                                        }
                                    }
                                }
                            }
                            finally
                            {
                                IsRefreshingFolders = false;
                            }

                            break;
                        case EGroupPermissionAction.RefreshLayouts:
                            try
                            {
                                IsRefreshingLayouts = true;
                                List<LayoutItemModel> previouslySelected = null;
                                if (SelectedLayouts?.Count > 0)
                                {
                                    previouslySelected = SelectedLayouts.ToList();
                                    //var updatePermissions = await PermissionItemManager.RequestPermissionsBatch(
                                    //    selectedGroupId, SelectedLayouts.Select(o => (Guid)o.ObjectId).ToArray());
                                }

                                await LayoutItmMgr.RefreshLayouts(itemManagerRequestParameters);
                                if (previouslySelected != null)
                                {
                                    await App.Current.Dispatcher.InvokeAsync(() =>
                                    {
                                        try
                                        {
                                            LayoutCollectionViewSource.View.Refresh();
                                            foreach (var p in previouslySelected)
                                            {
                                                try
                                                {
                                                    var matching =
                                                        View.LayoutItemSelectionListBox.Items.OfType<KeyValuePair<Guid,LayoutItemModel>>().FirstOrDefault(o => o.Value.Id == p.Id);
                                                        
                                                        //   LayoutItmMgr.ItemCollection.FirstOrDefault(o => o.Key == p.Id);
                                                        if (matching.Value != null)
                                                    {
                                                        View.LayoutItemSelectionListBox.SelectedItems.Add(matching);
                                                    }
                                                }
                                                catch (Exception e)
                                                {
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            
                                        }
                                    });
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                throw;
                            }
                            finally
                            {
                                IsRefreshingLayouts = false;
                            }

                            break;
                        case EGroupPermissionAction.SaveCameras:
                            var dialog = await DialogUtil.EmbeddedModalConfirmDialog(
                                _dialogOverlay.ModalContentPresenter,
                                new EmbeddedDialogParameters
                                {
                                    Description =
                                        $"Are you sure you wish to save\nCamera permissions for {groupItemModel.GroupName}?",
                                    Header = "Save Camera Permissions",
                                    DialogButtons = EDialogButtons.YesNo,
                                });
                            if (dialog)
                            {
                                if (SelectedCameras == null)
                                {
                                    if (SelectedServers != null)
                                    {
                                        var server = SelectedServers.FirstOrDefault();
                                        var zoneCameras =
                                            CameraItemModelManager.ItemCollection.Values.Where(o =>
                                                server != null && o.ZoneId == server.ZoneId);
                                        if (zoneCameras != null)
                                        {
                                            foreach (var n in zoneCameras)
                                            {
                                                try
                                                {
                                                    if (n != null)
                                                    {
                                                        await PermissionItemManager.UpdatePermissions(
                                                            selectedGroupId,
                                                            n.Id, n.Permissions.Value);
                                                    }
                                                }
                                                catch (Exception e)
                                                {
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    var savedCameras = SelectedCameras?.Select(o =>
                                        CameraItemModelManager?.ItemCollection?.FirstOrDefault(s => s.Key == o?.ObjectId)
                                            .Value);

                                    if (savedCameras != null)
                                    {
                                        foreach (var n in savedCameras)
                                        {
                                            try
                                            {
                                                if (n != null)
                                                {
                                                    await PermissionItemManager.UpdatePermissions(
                                                        selectedGroupId,
                                                        n.Id, n.Permissions.Value);
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                            }
                                        }
                                    }
                                }
                            }


                            break;
                        case EGroupPermissionAction.SaveFolders:
                            var savedNodes = SelectedNodes?.Select(o =>
                                NodeObjectItemMgr?.ItemCollection?.FirstOrDefault(s => s.Key == o?.ObjectId).Value);
                            var savedNodeCameras = SelectedCameraNodes?.Select(o =>
                                CameraItemModelManager?.ItemCollection?.FirstOrDefault(s => s.Key == o?.ObjectId).Value);
                            var folderDialog = await DialogUtil.EmbeddedModalConfirmDialog(
                                _dialogOverlay.ModalContentPresenter,
                                new EmbeddedDialogParameters
                                {
                                    Description =
                                        $"Are you sure you wish to save permissions for {groupItemModel.GroupName}?",
                                    Header = "Save Tree Object Permissions",
                                    DialogButtons = EDialogButtons.YesNo,
                                });
                            if (folderDialog)
                            {
                                if (savedNodeCameras != null)
                                {
                                    foreach (var c in savedNodeCameras)
                                    {
                                        try
                                        {
                                            if (c != null)
                                            {
                                                var permissionsValue = c.Permissions.Value;
                                                if (c.ObjectId != null)
                                                {
                                                    await PermissionItemManager.UpdatePermissions(
                                                        selectedGroupId,
                                                        (Guid) c.ObjectId, permissionsValue);
                                                }
                                            } 
                                        }
                                        catch (Exception e)
                                        {
                                        }
                                    }
                                }

                                if (savedNodes != null)
                                {
                                    foreach (var n in savedNodes)
                                    {
                                        try
                                        {
                                            if (n != null)
                                            {
                                                var permissionsValue = n.Permissions.Value;
                                                if (n.ObjectId != null)
                                                {
                                                    await PermissionItemManager.UpdatePermissions(
                                                        selectedGroupId,
                                                        (Guid) n.ObjectId, permissionsValue);
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                        }
                                    }
                                }
                            }

                            break;
                        case EGroupPermissionAction.SaveLayouts:
                            //var savedLayouts = SelectedLayouts.Select(o =>
                            //    LayoutItmMgr.ItemCollection.FirstOrDefault(s => s.Key == o.ObjectId).Value);
                            var layoutDialog = await DialogUtil.EmbeddedModalConfirmDialog(
                                _dialogOverlay.ModalContentPresenter,
                                new EmbeddedDialogParameters
                                {
                                    Description =
                                        $"Are you sure you wish to save\nLayout permissions for {groupItemModel.GroupName}?",
                                    Header = "Save Layout Permissions",
                                    DialogButtons = EDialogButtons.YesNo,
                                });
                            if (layoutDialog)
                            {
                                if (SelectedLayouts != null)
                                {
                                    foreach (var n in SelectedLayouts)
                                    {
                                        try
                                        {
                                            if (n != null)
                                            {
                                                await PermissionItemManager.UpdatePermissions(
                                                    selectedGroupId,
                                                    n.Id, n.Permissions.Value);
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                        }
                                    }
                                }
                            }

                            break;
                        default:
                            return;
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private async void PermissionAction(object obj)
        {
            try
            {
                if (obj is EPermissionAction action)
                {
                    switch (action)
                    {
                        case EPermissionAction.Delete:
                            break;
                        case EPermissionAction.Add:
                            break;
                        case EPermissionAction.Refresh:
                            if (GroupManagementViewModel.SelectedGroup.GroupId != null)
                            {
                                foreach (var c in SelectedCameras)
                                {
                                    var resp = await PermissionItemManager.PermissionManager.RequestPermissions(
                                        (Guid) GroupManagementViewModel.SelectedGroup.GroupId, c.Id);
                                    if (resp != null)
                                    {
                                        Trace.WriteLine($"{resp}");
                                    }
                                }

                                //var groupId = GroupManagementViewModel?.SelectedGroup?.GroupId?.ToString();
                                //var resp = await GetManagerByZoneId().PermissionService.GetPermissionsByGroup(
                                //    new GetPermissionsByGroup
                                //        {GroupId = groupId});
                            }

                            break;
                        default:
                            return;
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private async void RefreshItems()
        {
            if (SelectedServer.ZoneId != null)
            {
                await CameraItemModelManager.RefreshCameras((Guid) SelectedServer.ZoneId, SelectedServer.ServerId);
            }
        }

        private async void Init()
        {
            await Task.Run(async () =>
            {
                await ZoneItemMgr.RefreshZones();
            });
           // await ZoneCameraManagementViewModel.PopulateZones(Zones, false, SelectedServer);
            InitPermissions();
        }

        private void InitPermissions()
        {
            foreach (var r in AvailableCameraPermissionItemModel.PermissionList)
            {
                AvailableCameraPermission.Add(new AvailableCameraPermissionItemModel
                {
                    CameraPermission = r,
                    IsChecked = false
                });
                //  {RoleValue = (int) r, Role = r, IsChecked = ((ERoles) RoleValue).HasFlag(r)});
            }

            foreach (var r in AvailableLayoutPermissionItemModel.PermissionList)
            {
                AvailableLayoutPermission.Add(new AvailableLayoutPermissionItemModel
                {
                    LayoutPermission = r,
                    IsChecked = false
                });
                //  {RoleValue = (int) r, Role = r, IsChecked = ((ERoles) RoleValue).HasFlag(r)});
            }

            foreach (var r in AvailableFolderPermissionItemModel.PermissionList)
            {
                AvailableFolderPermission.Add(new AvailableFolderPermissionItemModel
                {
                    FolderPermission = r,
                    IsChecked = false
                });
                //  {RoleValue = (int) r, Role = r, IsChecked = ((ERoles) RoleValue).HasFlag(r)});
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void NodeObjectCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            try
            {
                if (SelectedNodes != null)
                {
                    e.Accepted = e.Item is KeyValuePair<Guid, NodeItemModel> model &&
                                 SelectedNodes.Any(s =>
                                     s.ObjectId == model.Key); //; .ActiveCameras.Any(o => o.Id == model.Key.Item1));
                }
                else
                {
                    e.Accepted = false;
                }
            }
            catch (Exception exception)
            {
                e.Accepted = false;
            }
        }

        private void LayoutCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            e.Accepted = true;
            //try
            //{
            //    if (e.Item is KeyValuePair<Guid, LayoutItemModel> model)
            //    {
            //        e.Accepted = SelectedLayouts.Any(o => o.Id == model.Key);
            //    }
            //    else
            //    {
            //        e.Accepted = false;
            //    }
            //}
            //catch (Exception exception)
            //{
            //    e.Accepted = false;
            //}
        }

        private void LayoutGridCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            //e.Accepted = true;
            try
            {
                if (e.Item is KeyValuePair<Guid, LayoutItemModel> model)
                {
                    e.Accepted = SelectedLayouts != null && SelectedLayouts.Any(o => o != null && o.Id == model.Key);
                }
                else
                {
                    e.Accepted = false;
                }
            }
            catch (Exception exception)
            {
                e.Accepted = false;
            }
        }

        private void PermissionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            try
            {
                if (e.Item is KeyValuePair<Tuple<Guid, Guid>, PermissionItemModel> model)
                {
                    e.Accepted = model.Key.Item2 == GroupManagementViewModel?.SelectedGroup?.GroupId;
                }
                else
                {
                    e.Accepted = false;
                }
            }
            catch (Exception exception)
            {
                e.Accepted = false;
            }
        }

        private void ZonesViewSourceOnFilter(object sender, FilterEventArgs e)
        {
        }

        private void CameraProfileCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            try
            {
                if (SelectedCameras != null)
                {
                    e.Accepted = e.Item is KeyValuePair<Guid, CameraItemModel> model &&
                                 SelectedCameras.Any(c => c.Id == model.Value.Id);
                }
                else if (SelectedServers != null)
                {
                    e.Accepted = e.Item is KeyValuePair<Guid, CameraItemModel> model &&
                                 SelectedServers.Any(s =>
                                     s.Archiver != null && s.Archiver.ActiveCameras.Any(o => o.Id == model.Value.Id));
                }
                else
                {
                    e.Accepted = false;
                }
            }
            catch (Exception exception)
            {
                e.Accepted = false;
            }
        }

        private void RecordingProfileCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            try
            {
                if (SelectedCameras != null)
                {
                    e.Accepted = e.Item is KeyValuePair<Guid, RecordingProfileItemModel> model &&
                                 SelectedCameras.Any(o => o.Id == model.Value.CameraId);
                }
                else if (SelectedServers != null)
                {
                    e.Accepted = e.Item is KeyValuePair<Guid, RecordingProfileItemModel> model &&
                                 SelectedServers.Any(o =>
                                     o.Archiver != null &&
                                     o.Archiver.ActiveCameras.Any(z => z.Id == model.Value.CameraId));
                }
                else
                {
                    e.Accepted = false;
                }
            }
            catch (Exception exception)
            {
                e.Accepted = false;
            }
        }

        private void StreamProfileCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            //StreamProfileItemManager
            try
            {
                if (SelectedCameras != null)
                {
                    e.Accepted = e.Item is KeyValuePair<Tuple<Guid, Guid>, StreamProfileItemModel> model &&
                                 SelectedCameras.Any(o => o.Id == model.Key.Item2);
                }
                else if (SelectedServers != null)
                {
                    e.Accepted = e.Item is KeyValuePair<Tuple<Guid, Guid>, StreamProfileItemModel> model &&
                                 SelectedServers.Any(s =>
                                     s.Archiver != null && s.Archiver.ActiveCameras.Any(o => o.Id == model.Key.Item2));
                }
                else
                {
                    e.Accepted = false;
                }
            }
            catch (Exception exception)
            {
                e.Accepted = false;
            }
        }

        private void PresetProfileCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            try
            {
                if (SelectedCameras != null)
                {
                    e.Accepted = e.Item is KeyValuePair<Tuple<Guid, string>, PtzPresetItemModel> model &&
                                 SelectedCameras.Any(o => o.Id == model.Key.Item1);
                }
                else if (SelectedServers != null)
                {
                    e.Accepted = e.Item is KeyValuePair<Tuple<Guid, string>, PtzPresetItemModel> model &&
                                 SelectedServers.Any(s =>
                                     s.Archiver != null && s.Archiver.ActiveCameras.Any(o => o.Id == model.Key.Item1));
                }
                else
                {
                    e.Accepted = false;
                }
            }
            catch (Exception exception)
            {
                e.Accepted = false;
            }
        }
    }

    public enum EGroupPermissionAction
    {
        RefreshSummary,

        //  RefreshRecordingProfiles,
        //  RefreshPresets,
        RefreshCameras,
        RefreshFolders,
        RefreshLayouts,
        SaveCameras,
        SaveFolders,
        SaveLayouts,
    }
}