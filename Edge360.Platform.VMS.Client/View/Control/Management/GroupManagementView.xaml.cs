using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.Annotations;

namespace Edge360.Platform.VMS.Client.View.Control.Management
{
    public partial class GroupManagementView : UserControl, IView , INotifyPropertyChanged
    {
        private GroupManagementViewModel _viewModel;
        public GroupManagementViewModel Model => DataContext as GroupManagementViewModel;

        public GroupManagementViewModel ViewModel
        {
            get => _viewModel;
            set
            {
                if (Equals(value, _viewModel)) return;
                _viewModel = value;
                OnPropertyChanged();
            }
        }

        public GroupManagementView()
        {
            InitializeComponent();
            Model.View = this;
            ViewModel = DataContext as GroupManagementViewModel;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}