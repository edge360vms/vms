﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CameraService.Adapters.Shared.Enums;
using CameraService.Adapters.Shared.Interfaces;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.View.Canvas;
using Edge360.Platform.VMS.Common.Enums.Camera;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Monitoring.CameraComponents.Axis.PTZ;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.UI.Common.Util;
using Mictlanix.DotNet.Onvif.Common;
using Mictlanix.DotNet.Onvif.Ptz;
using Unity;
using Application = System.Windows.Application;
using Cursors = System.Windows.Input.Cursors;
using H264Configuration = CameraService.Adapters.Shared.Types.H264Configuration;
using IntRectangle = CameraService.Adapters.Shared.Types.IntRectangle;
using MouseEventArgs = System.Windows.Input.MouseEventArgs;
using Profile = CameraService.Adapters.Shared.Types.Profile;
using Transport = CameraService.Adapters.Shared.Types.Transport;
using UserControl = System.Windows.Controls.UserControl;
using VideoResolution = CameraService.Adapters.Shared.Types.VideoResolution;

namespace Edge360.Platform.VMS.Client.View.Control.Management.CameraConfiguration
{
    /// <summary>
    /// Interaction logic for CameraConfigurationView.xaml
    /// </summary>
    public partial class CameraProfileConfigurationView : UserControl
    {
        public CameraProfileConfigurationView()
        {
            InitializeComponent();
            Model.View = this;
        }

        public CameraProfileConfigurationViewModel Model => DataContext as CameraProfileConfigurationViewModel;
    }

        public class CameraProfileConfigurationViewModel : INotifyPropertyChanged
    {
        public CameraProfileConfigurationView View;
        private OnvifCameraConnectionItemModel _onvifConnection = new OnvifCameraConnectionItemModel();
        
        private bool _isPtzActionActive;
        private bool _isQueryingCamera;
        //private ONVIFPtz _onvifPtzController;
        //private MpvPlayer _player;
        //private CameraProfilesModel _cameraProfiles;
        //private CameraDetailsModel _cameraDetails;
        private RelayCommand _queryCameraInformation;
        private RelayCommand _setPresetCommand;
        private RelayCommand _configurationQualityCommand;
        private RelayCommand _addConfigurationCommand;
        private RelayCommand _saveConfigurationCommand;
        private RelayCommand _deleteConfigurationCommand;
        private int _videoHeight;
        private int _videoWidth;
        private RelayCommand _cameraActionCommand;
        private SynchronizationContext _syncContext = SynchronizationContext.Current;
        private string _profiletoken;
        private Size _resolution;
        private string _newConfigurationName;
        private string _selectedConfigurationName;

        private Mictlanix.DotNet.Onvif.Common.Profile _selectedProfile;

        private string _address = "10.100.0.56";

        private string _password = "pass";

        private string _username = "root";
        //private ConfigurationItemModel _selectedConfiguration;


        public OnvifCameraConnectionItemModel OnvifConnection
        {
            get => _onvifConnection;
            set
            {
                if (Equals(value, _onvifConnection))
                {
                    return;
                }

                _onvifConnection = value;
                OnPropertyChanged();
            }
        }

        //public CameraProfilesModel CameraProfiles
        //{
        //    get => _cameraProfiles;
        //    set
        //    {
        //        if (Equals(value, _cameraProfiles))
        //        {
        //            return;
        //        }

        //        _cameraProfiles = value;
        //        OnPropertyChanged();
        //    }
        //}

        //public CameraDetailsModel CameraDetails
        //{
        //    get => _cameraDetails;
        //    set
        //    {
        //        if (value == null)
        //        {
        //            return;
        //        }

        //        _cameraDetails = value;
        //        OnPropertyChanged();
        //    }
        //}

        public bool IsPtzActionActive
        {
            get => _isPtzActionActive;
            set
            {
                if (value == _isPtzActionActive)
                {
                    return;
                }

                _isPtzActionActive = value;
                OnPropertyChanged();
            }
        }

        public bool IsQueryingCamera
        {
            get => _isQueryingCamera;
            set
            {
                if (value == _isQueryingCamera)
                {
                    return;
                }

                _isQueryingCamera = value;
                OnPropertyChanged();
                //OnPropertyChanged(nameof(ConnectCameraCommand));
                OnPropertyChanged(nameof(CameraActionCommand));
            }
        }

        public PTZClient ONVIFPtzController => OnvifConnection?.PtzClient;

        //public ONVIFPtz ONVIFPtzController
        //{
        //    get => _onvifPtzController;
        //    set
        //    {
        //        if (Equals(value, _onvifPtzController))
        //        {
        //            return;
        //        }

        //        _onvifPtzController = value;
        //        OnPropertyChanged();
        //    }
        //}

        //public MpvPlayer Player
        //{
        //    get => _player;
        //    set
        //    {
        //        if (Equals(value, _player))
        //        {
        //            return;
        //        }

        //        _player = value;
        //        OnPropertyChanged();
        //    }
        //}


        public RelayCommand ConnectCameraCommand => _queryCameraInformation ??
                                                             (_queryCameraInformation =
                                                                 new RelayCommand(CameraConnect));

        private void CameraConnect(object obj)
        {
            try
            {
                App.GlobalUnityContainer.Resolve<DirectModeManager>().IsDirectMode = true;
                View.Player.VideoObject = new CameraItemModel(){Address = Address, Username = Username, Password = Password};
                View.Player.Model.PlayLive();
                if (View.Player.VideoComponent.PtzManager is ONVIFManager onvifMgr)
                {
                    OnvifConnection = onvifMgr.OnvifCameraConnection;
                    OnvifMgr = onvifMgr;
                    SelectedProfile = OnvifConnection.Profiles.FirstOrDefault();
                    SetupProfile(SelectedProfile);
                }
            }
            catch (Exception e)
            {
                
            }
        }

        public ONVIFManager OnvifMgr { get; set; }

        public RelayCommand CameraActionCommand => _cameraActionCommand ??
                                                   (_cameraActionCommand =
                                                       new RelayCommand(p => CameraAction(p), p => !IsQueryingCamera));

        public RelayCommand ConfigurationQualityCommand => _configurationQualityCommand ??
                                                   (_configurationQualityCommand =
                                                       new RelayCommand(p => ConfigurationQuality(p), p => !IsQueryingCamera));

        private async Task CameraAction(object o)
        {
            if (o is ECameraAction action && OnvifConnection != null)
            {
                switch (action)
                {
                    case ECameraAction.Reboot:
                        if (DialogUtil.MetroConfirmDialog("Are you sure you wish to REBOOT the camera?"))
                        {
                            var response = await OnvifConnection.DeviceClient.SystemRebootAsync(); //.RebootSystemAsync();
                            Trace.WriteLine($"RebootResponse {response}");
                        }
                        break;
                    case ECameraAction.Restore:
                        if (DialogUtil.MetroConfirmDialog("Are you sure you wish to RESTORE the camera?"))
                        {
                            //var response = await Camera?.RebootSystemAsync();
                           // Trace.WriteLine($"RebootResponse {response}");
                        }
                        break;
                    case ECameraAction.UpdateFirmware:
                        if (DialogUtil.MetroConfirmDialog("Are you sure you wish to Update the camera?"))
                        {
                            OpenFileDialog ofd = new OpenFileDialog();
                            if (ofd.ShowDialog() == DialogResult.OK && !string.IsNullOrWhiteSpace(ofd.FileName))
                            {
                               // var response = await Camera?.RebootSystemAsync();
                                Trace.WriteLine($"RebootResponse {ofd.FileName}");
                            }
                          
                        }
                        break;
                    case ECameraAction.Configure:

                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private async Task ConfigurationQuality(object o)
        {
            if (o is EConfigurationQuality quality)
            {
                switch (quality)
                {
                    case EConfigurationQuality.High:
                        break;
                    case EConfigurationQuality.Medium:
                        break;
                    case EConfigurationQuality.Low:
                        break;
                }
            }
        }

        //TODO: The profile token should be chosen on connection, and probably stored in the Controller like we do with PTZ
        public string Profiletoken
        {
            get => SelectedProfile?.token;
        }

        public Mictlanix.DotNet.Onvif.Common.Profile SelectedProfile
        {
            get => _selectedProfile;
            set
            {
                if (SelectedProfile == value)
                {
                    return;
                }

                _selectedProfile = value;
               // VideoEncoderConfigurationtoken = value.token;
                OnPropertyChanged();
            }
        }

        public string VideoEncoderConfigurationtoken
        {
            get => SelectedProfile?.VideoEncoderConfiguration?.token;
            //set
            //{
                
            //    if (Equals(value, SelectedProfile?.token))
            //    {
            //        return;
            //    }

            //    try
            //    {
            //        //Task.Run(async () =>
            //        //{
            //        //    var configuration =
            //        //        await OnvifConnection.MediaClient.GetVideoEncoderConfigurationAsync(SelectedConfiguration
            //        //            .token);//.VideoEncoderConfiguration;// .GetVideoEncoderConfiguration(value);

            //        //    OnvifConnection.VideoEncoderConfiguration = configuration;

            //        //    await OnvifConnection.MediaController.Controller.AddVideoEncoderConfigurationAsync(Profiletoken, value);
            //        //    Resolution = new Size(OnvifConnection.VideoEncoderConfiguration.Resolution.Width, OnvifConnection.VideoEncoderConfiguration.Resolution.Height);

            //        //    OnPropertyChanged(nameof(VideoEncoderConfigurationtoken));
            //        //    OnPropertyChanged(nameof(BitrateLimit));
            //        //    OnPropertyChanged(nameof(Framerate));
            //        //    OnPropertyChanged(nameof(KeyframeInterval));

            //        //    await FetchResolutions();
            //        //});
                    
                    
            //    }
            //    catch(Exception ex)
            //    {
            //        Trace.WriteLine($"Resolution: {ex}");
            //    }
            //}
        }
        public Size Resolution
        {
            get => _resolution;
            set
            {
                if (value != _resolution)
                {
                     
                    if (Math.Abs(value.Width) <= 0 || Math.Abs(value.Height) <= 0)
                    {
                        _resolution = Size.Empty;
                    }

                    _resolution = new Size(value.Width, value.Height);

                    //if (CameraProfiles.AvailableResolutions != null && (CameraProfiles != null && CameraProfiles.AvailableResolutions.Contains(_resolution)))
                    //{
                    //    Task.Run((async () =>
                    //    {
                    //        try
                    //        {
                    //            if (OnvifConnection.VideoSourceConfiguration != null)
                    //            {
                    //                var sourceInfo = OnvifConnection.VideoSourceConfiguration;

                    //                sourceInfo.Bounds = new IntRectangle()
                    //                {
                    //                    Height = (int)value.Height,
                    //                    Width = (int) value.Width,
                    //                    X = 0,
                    //                    Y = 0
                    //                };

                    //                OnvifConnection.SetVideoSourceConfiguration(sourceInfo);

                    //                var encoderInfo = OnvifConnection.VideoEncoderConfiguration;
                    //                var res = encoderInfo.Resolution;
                    //                encoderInfo.H264 = new H264Configuration() { GovLength = 20, H264Profile = EH264Profile.Main };
                    //                encoderInfo.Encoding = EVideoEncoding.H264;

                    //                if ((int)value.Width != res.Width || (int)value.Height != res.Height)
                    //                {
                    //                    //Prevents this from being called if resolution changes via a change to the video encoder configuration token
                    //                    encoderInfo.Resolution = new CameraService.Adapters.Shared.Types.VideoResolution() { Height = (int)_resolution.Height, Width = (int)_resolution.Width };
                    //                    OnvifConnection.SetVideoEncoderConfiguration(encoderInfo);
                    //                }

                    //                OnPropertyChanged(nameof(Resolution));

                    //                Trace.WriteLine($"Saved Resolution to {_resolution}! Loading {OnvifConnection.StreamUrl}");

                    //                await Application.Current.Dispatcher.InvokeAsync((async () =>
                    //                {
                    //                    await UpdateStream();
                    //                }));
                    //            }
                    //        }
                    //        catch (Exception e)
                    //        {
                    //            Trace.WriteLine($"Resolution: {e}");
                    //        }
                    //    }));
                    //}

                    
                    OnPropertyChanged();
                }
               
            }
        }

        public int Framerate
        {
            get => SelectedProfile?.VideoEncoderConfiguration?.RateControl?.FrameRateLimit ?? 0;
            set
            {
                
                if (value == SelectedProfile?.VideoEncoderConfiguration?.RateControl?.FrameRateLimit)
                {
                    return;
                }

                Task.Run(async() =>
                {
                    var videoEncoder = SelectedProfile.VideoEncoderConfiguration;

                    videoEncoder.RateControl.FrameRateLimit = value;
                    //OnvifConnection.MediaClient.SetVideoEncoderConfigurationAsync(videoEncoder);//  .SetVideoEncoderConfiguration(videoEncoder);

                    //await Application.Current.Dispatcher.InvokeAsync((async () => { await UpdateStream(); }));
                });
                
            }
        }

        public int KeyframeInterval
        {
            get => SelectedProfile?.VideoEncoderConfiguration?.RateControl?.EncodingInterval ?? 0;
            set
            {
                if (value == SelectedProfile?.VideoEncoderConfiguration?.RateControl?.EncodingInterval)
                {
                    return;
                }

                Task.Run(async () =>
                {
                    var videoEncoder = SelectedProfile.VideoEncoderConfiguration;

                    videoEncoder.RateControl.EncodingInterval = value;
                   // OnvifConnection.MediaClient.SetVideoEncoderConfigurationAsync(videoEncoder);

                   // await Application.Current.Dispatcher.InvokeAsync((async () => { await UpdateStream(); }));
                });
            }
        }

        public int BitrateLimit
        {
            get => SelectedProfile?.VideoEncoderConfiguration?.RateControl?.BitrateLimit ?? 0;
            set
            {
                if (value == SelectedProfile?.VideoEncoderConfiguration?.RateControl?.BitrateLimit)
                {
                    return;
                }

                Task.Run(async () =>
                {
                    var videoEncoder = SelectedProfile.VideoEncoderConfiguration;

                    videoEncoder.RateControl.BitrateLimit = value;
                    //OnvifConnection.MediaClient.SetVideoEncoderConfigurationAsync(videoEncoder);

                    //await Application.Current.Dispatcher.InvokeAsync((async () => { await UpdateStream(); }));
                });
            }
        }

        public string NewConfigurationName
        {
            get => _newConfigurationName;
            set
            {
                if (Equals(value, _newConfigurationName))
                {
                    return;
                }

                _newConfigurationName = value;
                OnPropertyChanged();
            }
        }

        public string SelectedConfigurationName
        {
            get => _selectedConfigurationName;
            set
            {
                if (Equals(value, _selectedConfigurationName))
                {
                    return;
                }

                _selectedConfigurationName = value;
                OnPropertyChanged();
            }
        }

        public bool NewConfigurationValid => !string.IsNullOrEmpty(NewConfigurationName);
        public bool EditedConfigurationValid => !string.IsNullOrEmpty(SelectedConfigurationName);

        public RelayCommand SetPresetCommand =>
            _setPresetCommand ?? (_setPresetCommand = new RelayCommand(p => SetPreset(p)));

        public RelayCommand AddConfigurationCommand =>
            _addConfigurationCommand ?? (_addConfigurationCommand = new RelayCommand(p => AddConfiguration(), c => NewConfigurationValid));

        public RelayCommand SaveConfigurationCommand =>
            _saveConfigurationCommand ?? (_saveConfigurationCommand = new RelayCommand(p => SaveConfiguration(), c => EditedConfigurationValid));

        public RelayCommand DeleteConfigurationCommand =>
            _deleteConfigurationCommand ?? (_deleteConfigurationCommand = new RelayCommand(p => DeleteConfiguration()));


        public CameraProfileConfigurationViewModel()
        {
            //_view = view;
            //_view.PtzCircleControl.OnSpeedSet += PtzCircleControlOnOnSpeedSet;
            //_view.PtzCircleControl.OnPtzAction += PtzCircleControlOnOnPtzAction;
            //_view.ConnectionComboBox.SelectionChanged += ConnectionComboBoxOnSelectionChanged;

            //_view.PtzOverlayCanvas.SetOwner(_view);
            //_view.PlayerGrid.MouseUp += (sender, args) => _view.PlayerGrid.ReleaseMouseCapture();
            //_view.Panel.MouseDown += (sender, args) =>
            //    HandlePtzMouseDown(args);
            //_view.Panel.MouseUp += PanelOnMouseUp;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        //private void HandlePtzMouseDown(MouseEventArgs args)
        //{
        //    HandlePtzMouseDown((args.Button & MouseButtons.Left) != 0);
        //    _view.PlayerGrid.CaptureMouse();
        //}

        private async void SetPreset(object o = null)
        {
            try
            {
                if (o is EPtzPreset preset)
                {
                    if (ONVIFPtzController != null)
                    {
                        await OnvifMgr.SetPreset(preset);
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private async void AddConfiguration()
        {
            var name = NewConfigurationName;
            await OnvifConnection.MediaClient.AddVideoEncoderConfigurationAsync(Profiletoken, NewConfigurationName);
        }

        private async void SaveConfiguration()
        {
            var selectedConfig = SelectedProfile;
            var configuration = await OnvifConnection.MediaClient.GetVideoEncoderConfigurationAsync(selectedConfig.token);
            configuration.Name = SelectedConfigurationName;
            selectedConfig.Name = SelectedConfigurationName;
            await OnvifConnection.MediaClient.SetVideoEncoderConfigurationAsync(configuration, true);
        }

        private async void DeleteConfiguration()
        {
            await OnvifConnection.MediaClient.RemoveVideoEncoderConfigurationAsync(Profiletoken);
           // VideoEncoderConfigurationtoken =  // Pro.Configurations[0].token;
        }

        //private async Task UpdateStream()
        //{
        //    var cameraUrl = OnvifConnection.StreamUrl;
        //    await Task.Delay(25);
        //    PlayAuthorizedUrl(cameraUrl);
        //}

        //private async void CameraConnect(object obj = null)
        //{
        //    if (!IsQueryingCamera)
        //    {
        //        IsQueryingCamera = true;
        //        try
        //        {
        //            var controllerInitialized = await OnvifConnection.InitMediaController(OnvifConnection.Hostname, OnvifConnection.Username, OnvifConnection.Password);
        //            if (controllerInitialized)
        //            {
        //                if (OnvifConnection.MediaController != null)
        //                {
        //                    var profiles = await OnvifConnection.GetProfilesAsync();
        //                    CameraProfiles = new CameraProfilesModel();
                            
        //                    if (profiles != null)
        //                    {
        //                        var profiletokens = profiles.Select(p => p.token).ToList();
        //                        var firstProfile = profiles.FirstOrDefault();

        //                        if (firstProfile != null && !string.IsNullOrEmpty(firstProfile.token))
        //                        {
        //                            CameraProfiles.Profiletokens = new ObservableCollection<string>(profiletokens);
        //                            Profiletoken = CameraProfiles.Profiletokens.FirstOrDefault();

        //                            var streamUrl = await OnvifConnection.MediaController.GetStreamUriAsync(
        //                                Profiletoken, EStreamType.RTPUnicast,
        //                                new Transport {Protocol = ETransportProtocol.RTSP});
        //                            if (!string.IsNullOrEmpty(streamUrl))
        //                            {
        //                                PlayAuthorizedUrl(streamUrl);


        //                                Settings.Logins.RemoveAll(o => o.Hostname == OnvifConnection.Hostname);

        //                                Settings.Logins.Add(new CameraLoginItemModel
        //                                {
        //                                    Hostname = OnvifConnection.Hostname,
        //                                    Password = OnvifConnection.Password,
        //                                    Username = OnvifConnection.Username
        //                                });
        //                                OnPropertyChanged(nameof(Settings));
        //                                OnPropertyChanged(nameof(Settings.Logins));
        //                                try
        //                                {
        //                                    ONVIFPtzController = new ONVIFPtz(OnvifConnection.Hostname,
        //                                            OnvifConnection.Username,
        //                                            OnvifConnection.Password, Profiletoken)
        //                                        {PanSpeed = 50, ZoomSpeed = 50};
        //                                }
        //                                catch (Exception e)
        //                                {
        //                                    Console.WriteLine(e);
        //                                }

        //                                try
        //                                {
        //                                    await GetCameraDetails();
        //                                }
        //                                catch (Exception e)
        //                                {
        //                                    Trace.WriteLine(e);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            Trace.WriteLine(e);
        //        }

        //        IsQueryingCamera = false;
        //    }
        //}

        private void PlayAuthorizedUrl(string streamUrl)
        {
            //OnvifConnection.StreamUrl = streamUrl;
            //if (streamUrl.Contains(
            //    "rtsp://") && !streamUrl.Contains(
            //    $"rtsp://{OnvifConnection.Username}:{OnvifConnection.Password}@"))
            //{
            //    streamUrl = streamUrl.Replace("rtsp://",
            //        $"rtsp://{OnvifConnection.Username}:{OnvifConnection.Password}@");
            //}


            //Player.Load(
            //    streamUrl,true); //(@"rtsp://root:pass@10.100.0.56/axis-media/media.amp?resolution=1280x720&compression=30");
            //Player.Resume();
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        //private async void HandlePtzMouseDown(bool leftMouse)
        //{
        //    if (leftMouse)
        //    {
        //        if (_view.PtzOverlayCanvas is PtzOverlayCanvas ptzCanvas)
        //        {
        //            PtzClickCancellationtokenSource?.Cancel();
        //            PtzClickCancellationtokenSource = new CancellationtokenSource();
        //            var token = PtzClickCancellationtokenSource.token;
        //            //Task.Run(async () =>
        //            //{
        //            try
        //            {
        //                await Task.Delay(220);
        //                if (!token.IsCancellationRequested && _view.PlayerGrid.IsMouseCaptured)
        //                {
        //                    IsPtzActionActive = true;
        //                    //await ptzCanvas.WpfUiThreadAsync(async () =>
        //                    //{
        //                    ptzCanvas.StartingPoint = Mouse.GetPosition(_view.PlayerGrid);
        //                    Mouse.OverrideCursor = Cursors.None;
        //                    while (_view.PlayerGrid.IsMouseCaptured)
        //                    {
        //                        await Task.Delay(20);

        //                        //Owner.PtzOverlay.WpfUiThread(() =>
        //                        //{
        //                        var currentPoint =
        //                            _view.PlayerGrid.PointFromScreen(new System.Windows.Point(
        //                                System.Windows.Forms.Control.MousePosition.X,
        //                                System.Windows.Forms.Control.MousePosition.Y));
        //                        ptzCanvas.CurrentPoint = currentPoint;
        //                        ptzCanvas.InvalidateVisual();

        //                        var pointFromCenterX = _view.PlayerGrid.RenderSize.Width / 2 -
        //                            ptzCanvas.StartingPoint.X + currentPoint.X;
        //                        var pointFromCenterY = _view.PlayerGrid.RenderSize.Height / 2 -
        //                            ptzCanvas.StartingPoint.Y + currentPoint.Y;

        //                        //VideoComponent.PtzController?.MovePtzToPoint(
        //                        //    new Point(Math.Max(0, pointFromCenterX),
        //                        //        Math.Max(0, pointFromCenterY)),
        //                        //    new Size(_playerGrid.RenderSize.Width, _playerGrid.RenderSize.Height));
        //                        //}, DispatcherPriority.Render);
        //                    }

        //                    IsPtzActionActive = false;
        //                    Mouse.OverrideCursor = null;
        //                    if (ptzCanvas.CurrentPoint != new System.Windows.Point(-1, -1))
        //                    {
        //                        var mainSize = _view.PlayerGrid.RenderSize;
        //                        var imageSize = GetImageBounds();

        //                        if (!imageSize.IsEmpty)
        //                        {
        //                            var xOffset = (mainSize.Width -
        //                                           imageSize.Width) / 2;
        //                            var yOffset = (mainSize.Height -
        //                                           imageSize.Height) / 2;

        //                            var distance =
        //                                Math.Abs(ptzCanvas.StartingPoint.X - ptzCanvas.CurrentPoint.X) +
        //                                Math.Abs(ptzCanvas.StartingPoint.Y - ptzCanvas.CurrentPoint.Y);
        //                            {
        //                                if (ONVIFPtzController is IPtzController
        //                                    ptzController)
        //                                {
        //                                    if (distance < 15)
        //                                    {
        //                                        await ptzController.CenterMoveAsync((int) imageSize.Width,
        //                                            (int) imageSize.Height,
        //                                            ptzCanvas.StartingPoint.X - xOffset,
        //                                            ptzCanvas.StartingPoint.Y - yOffset
        //                                        );
        //                                    }
        //                                    else
        //                                    {
        //                                        var widthMagnitude = Math.Sqrt(
        //                                            Math.Pow(
        //                                                ptzCanvas.CurrentPoint.X -
        //                                                ptzCanvas.StartingPoint.X,
        //                                                2) +
        //                                            Math.Pow(
        //                                                ptzCanvas.CurrentPoint.Y -
        //                                                ptzCanvas.StartingPoint.Y,
        //                                                2));

        //                                        var zoomFromWidth =
        //                                            imageSize.Width /
        //                                            (float) widthMagnitude *
        //                                            100; //GetPreviewResolution().Width / Math.Abs(PtzStartPoint.X - currentPoint.X));
        //                                        await ptzController.AreaZoomAsync((int) imageSize.Height,
        //                                            (int) imageSize.Width, (int) ptzCanvas.DrawnRectangle.Height,
        //                                            (int) ptzCanvas.DrawnRectangle.Width,
        //                                            (int) (ptzCanvas.StartingPoint.X - xOffset),
        //                                            (int) (ptzCanvas.StartingPoint.Y - yOffset)
        //                                        );
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }

        //                    ptzCanvas.CurrentPoint = new System.Windows.Point(-1, -1);
        //                    ptzCanvas.InvalidateVisual();
        //                    ptzCanvas.StartingPoint = new System.Windows.Point(-1, -1);
        //                    //});
        //                }
        //            }
        //            catch
        //            {
        //            }

        //            //}, token);
        //        }
        //    }
        //}

        private Size GetImageBounds()
        {
            var previewRectangle = View.PlayerGrid.RenderSize;
            var sourceResolution = Resolution;
            if (sourceResolution.IsEmpty)
            {
                return Size.Empty;
            }

            var ratio = Math.Min(
                previewRectangle.Width / sourceResolution.Width,
                previewRectangle.Height / sourceResolution.Height);
            var imageBrushWidth = sourceResolution.Width * ratio;
            var imageBrushHeight = sourceResolution.Height * ratio;

            var xOffset = previewRectangle.Width - imageBrushWidth;
            var yOffset = previewRectangle.Height - imageBrushHeight;

            var newHeight =
                previewRectangle.Height - yOffset;
            var newWidth =
                previewRectangle.Width - xOffset;

            return new Size((int) newWidth, (int) newHeight);
        }

        public string Address
        {
            get => _address;
            set
            {
                if (value == _address) return;
                _address = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                if (value == _password) return;
                _password = value;
                OnPropertyChanged();
            }
        }

        public string Username
        {
            get => _username;
            set
            {
                if (value == _username) return;
                _username = value;
                OnPropertyChanged();
            }
        }

        public void ViewLoaded()
        {
            //Player = new MpvPlayer(_view.Panel.Handle) {Loop = true, Volume = 0, KeepOpen = KeepOpen.No};

            //Player.API.PropertyChange += APIOnPropertyChange;
            //Player.API.GetPropertyReply += APIOnGetPropertyReply;
        }

        private async Task SetupProfile(Mictlanix.DotNet.Onvif.Common.Profile profile)
        {
            //OnvifConnection.Name = profile.Name;
            //OnvifConnection.token = profile.token;
            //OnvifConnection.VideoEncoderConfiguration = profile.VideoEncoderConfiguration;
            //OnvifConnection.VideoSourceConfiguration = profile.VideoSourceConfiguration;
            //OnvifConnection.VideoAnalyticsConfiguration = profile.VideoAnalyticsConfiguration;
            //OnvifConnection.AudioSourceConfiguration = profile.AudioSourceConfiguration;
            //OnvifConnection.AudioEncoderConfiguration = profile.AudioEncoderConfiguration;
            
            OnPropertyChanged(nameof(VideoEncoderConfigurationtoken));
            OnPropertyChanged(nameof(Resolution));
            OnPropertyChanged(nameof(BitrateLimit));
            OnPropertyChanged(nameof(Framerate));
            OnPropertyChanged(nameof(KeyframeInterval));
            await FetchConfigurations();
            await FetchResolutions();

        }

        private async Task FetchConfigurations()
        {
            try
            {
                var configurations = await OnvifConnection.MediaClient.GetCompatibleVideoEncoderConfigurationsAsync(Profiletoken);// GetCompatibleVideoEncoderConfigurations(Profiletoken);

                if (configurations?.Configurations != null && configurations.Configurations.Length != 0)
                {
                    ////var tokens = configurations.Configurations.Select(x => x.token).ToList();
                    //var configs = configurations.Configurations.Select(x => new ConfigurationItemModel()
                    //{
                    //    token = x.token,
                    //    Name = x.Name
                    //}).ToList();

                    ////CameraProfiles.Configurationtokens = new ObservableCollection<string>(tokens);
                    //CameraProfiles.Configurations = new ObservableCollection<ConfigurationItemModel>(configs);
                    //SelectedConfiguration = OnvifConnection.Profiles.FirstOrDefault(x => x.token == OnvifConnection.token);
                    SelectedConfigurationName = SelectedProfile?.Name;
                }
            }
            catch (Exception e)
            {
                
            }
        }

        //Called whenever the profile token or configuration token is changed
        private async Task FetchResolutions()
        {
            try
            {
                var encoderConfiguration = OnvifConnection.Profiles.FirstOrDefault()?.VideoEncoderConfiguration;
                if (encoderConfiguration != null)
                {
                    var resolution = encoderConfiguration.Resolution;

                    //This uses the private setter to avoid triggering everything in the setter for Resolution.
                    //Resolution (public setter) should only be changed when manually editing the resolution, so that we can update the configuration, which is already happening for the config token
                    _resolution = new Size(resolution.Width, resolution.Height);
                    var availableResolutions = await OnvifConnection.MediaClient.GetVideoEncoderConfigurationOptionsAsync(VideoEncoderConfigurationtoken, Profiletoken);

                    switch (encoderConfiguration.Encoding)
                    {
                        case VideoEncoding.JPEG:
                            OnvifConnection.AvailableResolutions =
                                ParseResolutions(availableResolutions.JPEG.ResolutionsAvailable);
                            break;
                        case VideoEncoding.MPEG4:
                            OnvifConnection.AvailableResolutions =
                                ParseResolutions(availableResolutions.MPEG4.ResolutionsAvailable);
                            break;
                        case VideoEncoding.H264:
                            OnvifConnection.AvailableResolutions =
                                ParseResolutions(availableResolutions.H264.ResolutionsAvailable);
                            break;
                        case VideoEncoding.H265:
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                
            }
        }

        //private void ConnectionComboBoxOnSelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (View.ConnectionComboBox.SelectedItem is CameraItemModel login)
        //    {
        //        OnvifConnection.Hostname = login.Address;
        //        OnvifConnection.Username = login.Username;
        //        OnvifConnection.Password = login.Password;
        //    }
        //}

        //private void PanelOnMouseUp(object sender, MouseEventArgs e)
        //{
        //    View.PlayerGrid.ReleaseMouseCapture();
        //}

        private ObservableCollection<Size> ParseResolutions(Mictlanix.DotNet.Onvif.Common.VideoResolution[] resolutions)
        {
            var sizes = resolutions.Select(res => new Size()
            {
                Width = res.Width,
                Height = res.Height
            }).ToList();

            return new ObservableCollection<Size>(sizes);
        }
    }

    internal enum ECameraAction
    {
        Reboot,
        Restore,
        UpdateFirmware,
        Configure
    }

    internal enum EConfigurationQuality
    {
        High,
        Medium,
        Low
    }
}
