﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.DataObject;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Dialog;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Nodes;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Cameras;
using log4net;
using Newtonsoft.Json;
using ServiceStack;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.DragDrop;
using static Edge360.Platform.VMS.Communication.CommunicationManager;
using IDataObject = System.Runtime.InteropServices.ComTypes.IDataObject;

namespace Edge360.Platform.VMS.Client.View.Control.Camera
{
    public class CameraTreeViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private readonly ILog _log;
        private RelayCommand _clearFilterTextCommand;

        private ICommand _createCameraCommand;
        private ICommand _deleteNodeCommand;
        private CancellationTokenSource _filterCts;
        private Task _filterTask;
        private string _filterText = string.Empty;
        private bool _hideNonMatching;
        private bool _isEditing;
        private bool _isLoadingRootNode;
        private bool _managementToolsEnabled;
        private ICommand _newLayoutCommand;
        private ICommand _newNodeCommand;
        private ObservableCollection<NodeObject> _nodes = new ObservableCollection<NodeObject>();

        private ICommand _PreviewDropCommand;
        private RelayCommand _refreshCommand;
        private RelayCommand _renameCommand;
        private RelayCommand _renameNodeCommand;
        private NodeObject _selectedItem;
        private RelayCommand _testCommand;

        public CameraTreeView View;

        [JsonIgnore] public CameraItemModelManager CameraItemMgr { get; set; }

        public CollectionViewSource CameraViewSource { get; set; } = new CollectionViewSource
        {
            GroupDescriptions =
                {new PropertyGroupDescription("NodeItemModel"), new PropertyGroupDescription("CameraItemModel")},
            SortDescriptions =
            {
                new SortDescription {PropertyName = "Label"},
                new SortDescription {PropertyName = "Value.Label"}
            }
        };

        public ICommand ClearFilterTextCommand
        {
            get
            {
                if (_clearFilterTextCommand == null)
                {
                    _clearFilterTextCommand = new RelayCommand(
                        param => FilterText = string.Empty,
                        param => true
                    );
                }

                return _clearFilterTextCommand;
            }
        }


        public ICommand CreateCameraCommand
        {
            get
            {
                if (_createCameraCommand == null)
                {
                    _createCameraCommand = new RelayCommand(
                        param => AddCamera(),
                        param => CanAddCamera()
                    );
                }

                return _createCameraCommand;
            }
        }

        public ICommand DeleteNodeCommand
        {
            get
            {
                if (_deleteNodeCommand == null)
                {
                    _deleteNodeCommand = new RelayCommand(
                        param => DeleteNode(),
                        param => CanDeleteNode()
                    );
                }

                return _deleteNodeCommand;
            }
        }

        [JsonIgnore] public DirectModeManager DirectModeMgr { get; set; }


        public string FilterText
        {
            get => _filterText;
            set
            {
                if (value == _filterText)
                {
                    return;
                }

                _filterText = value;
                OnPropertyChanged();
                _filterCts?.Cancel();
                _filterCts = new CancellationTokenSource();
                var cts = _filterCts;
                _filterTask = Task.Run(async () =>
                {
                    await Task.Delay(800);
                    if (!cts.IsCancellationRequested)
                    {
                        await Application.Current.Dispatcher.InvokeAsync(() => { CameraViewSource?.View?.Refresh(); });
                    }
                }, cts.Token);
            }
        }

        public bool HideNonMatching
        {
            get => _hideNonMatching;
            set
            {
                if (value == _hideNonMatching)
                {
                    return;
                }

                _hideNonMatching = value;
                OnPropertyChanged();
                CameraViewSource.View.Refresh();
                Trace.WriteLine($"CameraTreeViewModel 171 InvalidateRequerySuggested");
                CommandManager.InvalidateRequerySuggested();
            }
        }

        public bool IsEditing
        {
            get => _isEditing;
            set
            {
                if (_isEditing != value)
                {
                    _isEditing = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool IsLoadingRootNode
        {
            get => _isLoadingRootNode;
            set
            {
                if (value == _isLoadingRootNode)
                {
                    return;
                }

                _isLoadingRootNode = value;
                OnPropertyChanged();
            }
        }

        public bool IsUpdatingNodes { get; set; }

        public bool ManagementToolsEnabled
        {
            get => _managementToolsEnabled;
            set
            {
                if (value == _managementToolsEnabled)
                {
                    return;
                }

                _managementToolsEnabled = value;
                OnPropertyChanged();
            }
        }

        public ICommand NewCameraCommand
        {
            get
            {
                if (_renameCommand == null)
                {
                    _renameCommand = new RelayCommand(
                        param => CreateCamera(),
                        param => CanCreateCamera()
                    );
                }

                return _renameCommand;
            }
        }

        public ICommand NewLayoutCommand
        {
            get
            {
                if (_newLayoutCommand == null)
                {
                    _newLayoutCommand = new RelayCommand(
                        param => CreateLayout(),
                        param => CanCreateLayout()
                    );
                }

                return _newLayoutCommand;
            }
        }

        public ICommand NewNodeCommand
        {
            get
            {
                if (_newNodeCommand == null)
                {
                    _newNodeCommand = new RelayCommand(
                        param => CreateNode(),
                        param => CanCreateNode()
                    );
                }

                return _newNodeCommand;
            }
        }

        public ObservableCollection<NodeObject> Nodes
        {
            get => _nodes;
            set
            {
                if (_nodes != value)
                {
                    _nodes = value;
                    OnPropertyChanged();
                }
            }
        }

        public List<NodeItemModel> NodesToExpand { get; set; } = new List<NodeItemModel>();

        public ICommand PreviewDropCommand
        {
            get => _PreviewDropCommand ??= new RelayCommand(HandlePreviewDrop);
            set
            {
                _PreviewDropCommand = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand RefreshNodeCommand
        {
            get { return _refreshCommand ??= new RelayCommand(p => Refresh(), p => CanRefresh()); }
        }

        public ICommand RenameCommand
        {
            get
            {
                return _renameCommand ??= new RelayCommand(
                    param => Rename(),
                    param => CanRename()
                );
            }
        }

        public ICommand RenameNodeCommand
        {
            get
            {
                if (_renameNodeCommand == null)
                {
                    _renameNodeCommand = new RelayCommand(
                        param => Rename(SelectedItem),
                        param => CanRenameNode()
                    );
                }

                return _renameNodeCommand;
            }
        }

        public NodeObject SelectedItem
        {
            get => _selectedItem;
            set
            {
                if (_selectedItem != value)
                {
                    _selectedItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public RelayCommand TestCommand
        {
            get { return _testCommand ??= new RelayCommand(p => Test(), p => CanTest()); }
        }

        public string ViewModelName { get; set; }

        public CameraTreeViewModel(ILog log = null, CameraItemModelManager cameraItemModelManager = null,
            DirectModeManager directModeManager = null)
        {
            _log = log;
            DirectModeMgr = directModeManager;
            CameraItemMgr = cameraItemModelManager;
            App.Current?.Dispatcher?.InvokeAsync(async () =>
            {
                BindingOperations.SetBinding(CameraViewSource,
                    CollectionViewSource.SourceProperty,
                    new Binding(nameof(Nodes)) {Source = this, Mode = BindingMode.OneWay});
                CameraViewSource.Filter += CameraViewSourceOnFilter;
            }, DispatcherPriority.Background);
            //DragDropManager.AddPreviewDropHandler(_cameraTree, OnDrop);
            //DragDropManager.AddDragEnterHandler(_cameraTree, OnDragEntered, true);

            //try
            //{
            //    HubManager.SignalRClientEvent += async (e, zone, data) =>
            //    {
            //        if (Equals(e, ESignalRClientEvent.CamerasUpdated) || Equals(e, ESignalRClientEvent.NodesUpdated))
            //        {
            //            if (zone == ConnectedZoneId)
            //            {
            //                HubManagerOnNodesUpdated(zone, data);
            //            }
            //        }
            //        else if (Equals(e, ESignalRClientEvent.NodeDeleted))
            //        {
            //            if (zone == ConnectedZoneId)
            //            {
            //                HubManagerOnNodeDeleted(zone, data);
            //            }
            //        }
            //    };
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //}

            ViewModelName = "Camera Management";
            LoadRootNode();
        }


        public event PropertyChangedEventHandler PropertyChanged;


        private async void Test()
        {
            var resp =
                await GetManagerByZoneId().AdConfigZoneService.GetAdConfig(Guid.Empty);
            _log?.Info(resp);
            // CommunicationManager.Client.SetServiceType(EServiceType.Authority);
            // CommunicationManager.Client.GetAsync<>(new GetAdConfig);
        }

        private bool CanTest()
        {
            return true;
        }

        private bool CanRefresh()
        {
            return true;
        }

        public async void Refresh(object node = null, bool setSelected = true)
        {
            _ = this.TryUiAsync(async () =>
            {
                var selected = node as NodeObject ?? Nodes.FindNodeById<NodeObject>(SelectedItem != null
                    ? SelectedItem.ObjectId
                    : Guid.Empty);
                if (node == null)
                {
                    LoadRootNode();
                }

                if (setSelected)
                {
                    SelectedItem = selected;
                }

                await GetChildren(selected);
                //var selectedParent = Nodes.FindParentNodeByChildId<NodeObject>(SelectedItem.ObjectId);
                //var selectedParentById = Nodes.FindNodeByParentId<NodeObject>(SelectedItem.ObjectId);

                Console.WriteLine($"{selected}");
            }, DispatcherPriority.Background);
        }

        private bool CanCreateNode()
        {
            return true;
        }


        private async void CreateNode()
        {
            _ = this.TryUiAsync(() =>
            {
                if (SelectedItem != null)
                {
                    var input = DialogUtil.InputDialog("Please enter a label for the new Folder.");
                    if (input.Result && !string.IsNullOrEmpty(input.Response))
                    {
                        try
                        {
                            var resp = NodeObjectItemManager.NodeManager.CreateNode(new NodeDescriptor
                            {
                                Label = input.Response,
                                ParentId = SelectedItem.ObjectId,
                                Type = "Node"
                            });
                            Console.WriteLine($"New Node {resp}");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                }
            }, DispatcherPriority.Background);
        }

        private async void CreateCamera()
        {
            if (SelectedItem != null)
            {
                var dialog = new CreateCameraDialog();
                if (dialog.ShowDialog().HasValue)
                {
                    if (dialog != null && dialog.Model.DialogItem is CreateCameraItemModel item &&
                        !string.IsNullOrEmpty(dialog.Model?.DialogItem?.Label) &&
                        !string.IsNullOrEmpty(dialog.Model?.DialogItem?.Address))
                    {
                        _ = this.TryUiAsync(async () =>
                        {
                            try
                            {
                                var nearestNode = Nodes.FindNodeById<NodeItemModel>(SelectedItem.ObjectId) ??
                                                  Nodes.FindNodeById<NodeItemModel>(SelectedItem.ParentId);

                                if (nearestNode != null)
                                {
                                    if (nearestNode.ObjectId != null)
                                    {
                                        var convertedCreate = item.ConvertTo<CreateCamera>();
                                        convertedCreate.ParentId = nearestNode.ParentId;
                                        convertedCreate.ZoneId = Guid.Empty;
                                        await GetManagerByZoneId().CameraService.CreateCamera(
                                            convertedCreate);
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }
                        }, DispatcherPriority.Background);
                    }
                }
            }
        }

        private bool CanCreateCamera()
        {
            return true;
        }

        private bool CanCreateLayout()
        {
            return SelectedItem != null;
        }

        private void CreateLayout()
        {
        }

        private void DeleteNode()
        {
            if (ManagementToolsEnabled)
            {
                _ = this.TryUiAsync(() =>
                {
                    if (SelectedItem is NodeItemModel nodeItem)
                    {
                        nodeItem.Delete();
                    }

                    if (SelectedItem is CameraItemModel cameraItem)
                    {
                        cameraItem.DeleteCamera();
                    }

                    if (SelectedItem is LayoutItemModel layoutItem)
                    {
                        if (layoutItem.ObjectId != null)
                        {
                            var parent = Nodes.FindParentNodeByChildId<NodeItemModel>(layoutItem.ObjectId.Value);

                            if (layoutItem.DeleteLayout().Result)
                            {
                                Refresh(parent, false);
                            }
                        }
                    }
                }, DispatcherPriority.Background);
            }
        }

        private bool CanDeleteNode()
        {
            return SelectedItem != null;
        }

        private bool CanRenameNode()
        {
            return SelectedItem != null;
        }

        private bool CanRename()
        {
            return true;
        }

        private void Rename(NodeObject selectedItem = null)
        {
            if (selectedItem != null)
            {
            }

            IsEditing = true;
        }

        private void HandlePreviewDrop(object inObject)
        {
            try
            {
                var ido = inObject as IDataObject;
                if (null == ido)
                {
                }
            }
            catch (Exception e)
            {
            }
        }

        private async void AddCamera()
        {
            _log?.Info($"Adding Camera to {SelectedItem?.ParentId.ToString()}");
            if (SelectedItem != null)
            {
                //if (SelectedItem.ParentId != null)
                //{
                //    var resp = await ServiceManagers[HARD_CODED_ZONE_ID].CameraService.CreateCamera(new CameraDescriptor
                //    {
                //        Address = "10.100.0.56",
                //        Description = "Stuff",

                //        Label = "The Pit",
                //        License = "",
                //        Password = "pass",
                //        Path = "",
                //        Port = "",
                //        Type = "Camera",
                //        Username = "root",
                //        ZoneId = HARD_CODED_ZONE_ID,
                //        ParentId = (Guid) SelectedItem.ParentId
                //    });
                //    if (resp != null)
                //    {
                //        _log?.Info(resp.Message);
                //    }
                //}
            }

            //_control.CreateUserNameBox.Text = "";
            //_control.TxtBoxPassword.Password = "";
            //_control.CreateUserNameBox.Focus();
            //LoadRootNode();
        }

        private bool CanAddCamera()
        {
            return true;
        }

        public async void LoadRootNode()
        {
            // this.TryUiAsync(async () =>
            // {
            try
            {
                IsLoadingRootNode = true;
                //  await _cameraTree.WpfUiThreadAsync(async () =>
                // {
                await App.Current?.Dispatcher?.InvokeAsync(async () =>
                {
                    var nodeChildren =
                        await NodeObjectItemManager.NodeManager
                            .RefreshNodeChildren(
                                new ItemManagerRequestParameters
                                {
                                    AcceptedCacheDuration = TimeSpan.FromSeconds(3),
                                    CacheDuration = TimeSpan.FromSeconds(3)
                                }, Guid.Empty);
                    if (nodeChildren != null)
                    {
                        Nodes.Clear();
                        foreach (var node in nodeChildren)
                        {
                            Nodes.Add(node);


                            await GetChildren(node);
                        }

                        var selectedItem = Nodes.FirstOrDefault(o => o.ZoneId == ConnectedZoneId);
                        if (selectedItem != null)
                        {
                            await Task.Delay(50);
                            View?.CameraTree?.ExpandItemByPath(selectedItem?.ObjectId?.ToString());
                        }
                    }
                }, DispatcherPriority.Background);

                // });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                IsLoadingRootNode = false;
            }

            // });
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public async Task<bool> GetChildren(NodeObject node, RadTreeViewItem treeItem = null)
        {
            var invokeAsync = await App.Current.Dispatcher.InvokeAsync(async () =>
            {
                if (node == null)
                {
                    return false;
                }

                try
                {
                    var childList = await NodeObjectItemManager.NodeManager
                        .RefreshNodeChildren(
                            new ItemManagerRequestParameters
                                {AcceptedCacheDuration = TimeSpan.FromSeconds(3), CacheDuration = TimeSpan.FromSeconds(3)},
                            node.ObjectId);
                    node.Children.Clear();
                    if (childList != null)
                    {
                        var nodes = await ParseNodes(node, childList, treeItem);
                        return nodes;
                    }

                    if (treeItem != null)
                    {
                        treeItem.IsLoadingOnDemand = false;
                    }
                }
                catch (Exception)
                {
                }

                return true;
            }, DispatcherPriority.Background);
            return await invokeAsync;
        }

        private async Task<bool> ParseNodes(NodeObject node, List<NodeItemModel> childList,
            RadTreeViewItem treeItem = null)
        {
            var result = await App.Current?.Dispatcher?.InvokeAsync((async () =>
            {
                if (node == null)
                {
                    return false;
                }

                try
                {
                    //   var finished = await Application.Current.Dispatcher.InvokeAsync(async () =>
                    // {
                    if (node.Children.Count > 0)
                    {
                        var lingering = node.Children.Where(o =>
                                o is NodeItemModel folder && !folder.IsExpanded || childList.All(p =>
                                    p.ObjectId != o.ObjectId)
                                || childList.Any(p =>
                                    p.ObjectId == o.ObjectId &&
                                    p.ParentId != o.ParentId))
                            .ToList();
                        foreach (var l in lingering)
                        {
                            node.Children.Remove(l);
                        }
                    }

                    foreach (var c in childList.Where(o => o.ParentId == node.ObjectId))
                    {
                        var existingNode = node.Children.FirstOrDefault(o => o.ObjectId == c.ObjectId);

                        if (c.Type != null && c.Type.StartsWith("Camera"))
                        {
                            node.Children.Remove(existingNode);
                            //continue;
                        }
                        else if (existingNode == null)
                        {
                            //var child = new NodeItemModel(c);
                            node.Children.Add(c);
                        }
                    }

                    var cameraChildren = childList.Where(o =>
                            o.ParentId == node.ObjectId && o.Type != null && o.Type.StartsWith("Camera"))
                        .ToList();
                    if (cameraChildren.Any())
                    {
                        if (DirectModeMgr.IsOfflineMode)
                        {
                            var cameraBatch = await CameraItemMgr.RequestOfflineCameras(cameraChildren.ToArray());

                            var missingCameras =
                                cameraChildren.Where(o =>
                                    cameraBatch == null || cameraBatch.All(c => c?.Id != o?.ObjectId));

                            if (treeItem != null && !treeItem.IsExpanded)
                            {
                                treeItem.IsLoadingOnDemand = true;
                                treeItem.IsExpanded = true;
                            }

                            if (cameraBatch != null)
                            {
                                var cameraModels = cameraBatch?.Where(c => c != null)
                                    ?.ToList();
                                if (cameraModels != null)
                                {
                                    var cameraItemModels = cameraModels.Where(c => c != null)?.ToList();
                                    if (cameraItemModels?.Count > 0)
                                    {
                                        _ = Task.Run(async () =>
                                        {
                                            await CameraItemHelper.LoadOnvifProfiles(cameraItemModels).ConfigureAwait(false);
                                            await CameraItemHelper.LoadPresetsBatch(cameraItemModels).ConfigureAwait(false);
                                        }).ConfigureAwait(false);
                                        node.Children.AddRange(cameraItemModels);
                                    }
                                }
                            }

                            foreach (var m in missingCameras)
                            {
                                m.Label += " (Offline)";
                                var cam = m.ConvertTo<NodeDescriptor>();
                                cam.Id = m.ObjectId;
                                var cameraItemModel = (CameraItemModel) cam;
                                cameraItemModel.Status = "Disabled";
                                node.Children.Add(cameraItemModel);
                            }
                        }
                        else
                        {
                            var cameraBatch =
                                await CameraItemMgr.RequestGlobeCameras(cameraChildren
                                    .Where(o => o.ZoneId != ConnectedZoneId).ToArray());
                            var sameZoneBatch = await CameraItemMgr.RequestCameras(cameraChildren
                                .Where(o => o.ZoneId == ConnectedZoneId).ToArray());

                            var missingCameras =
                                cameraChildren.Where(o =>
                                    cameraBatch.All(c => c?.Camera?.CameraId != o?.ObjectId) &&
                                    sameZoneBatch.All(c => c?.Camera?.Id != o?.ObjectId));

                            if (treeItem != null && !treeItem.IsExpanded)
                            {
                                treeItem.IsLoadingOnDemand = true;
                                treeItem.IsExpanded = true;
                            }

                            if (sameZoneBatch != null)
                            {
                                var cameraModels = sameZoneBatch?.Where(c => c != null)
                                    ?.Select(f => (CameraItemModel) f?.Camera)?.ToList();
                                if (cameraModels != null)
                                {
                                    var cameraItemModels = cameraModels.Where(c => c != null)?.ToList();
                                    if (cameraItemModels?.Count > 0)
                                    {
                                        _ = Task.Run(async () => { await CameraItemHelper.InitCameraProfiles(cameraItemModels); }).ConfigureAwait(false);
                                        node.Children.AddRange(cameraItemModels);
                                    }
                                }
                            }

                            if (cameraBatch != null)
                            {
                                var cameraModels = cameraBatch?.Where(c => c != null)
                                    ?.Select(f =>
                                        (CameraItemModel) (Raven.Vms.AuthorityService.ServiceModel.Api.Cameras.Camera) f
                                            ?.Camera)?.ToList();
                                if (cameraModels != null)
                                {
                                    var cameraItemModels = cameraModels.Where(c => c != null)?.ToList();
                                    if (cameraItemModels?.Count > 0)
                                    {
                                        _ = Task.Run(async () => { await CameraItemHelper.InitCameraProfiles(cameraItemModels).ConfigureAwait(false); }).ConfigureAwait(false);
                                        node.Children.AddRange(cameraItemModels);
                                    }
                                }
                            }

                            foreach (var m in missingCameras)
                            {
                                m.Label += " (Offline)";

                                var cam = m.ConvertTo<NodeDescriptor>();

                                cam.Id = m.ObjectId;
                                var cameraItemModel = (CameraItemModel) cam;
                                cameraItemModel.Status = "Disabled";
                                node.Children.Add(cameraItemModel);
                            }
                        }
                    }

                    if (treeItem != null)
                    {
                        treeItem.IsLoadingOnDemand = false;
                    }

                    // });
                    return true;
                }
                catch (Exception e)
                {
                    _log?.Info(e);
                }


                return false;
            }), DispatcherPriority.Background);
            return await result;
        }



        private async Task GetExpandedNodes(ObservableCollection<NodeObject> nodeList, bool updateChildren = false)
        {
            _ = this.TryUiAsync(async () =>
            {
                foreach (var n in nodeList)
                {
                    if (n is NodeItemModel node && node.IsExpanded)
                    {
                        if (!updateChildren)
                        {
                            //var str = $"{node.ObjectId}|";
                            if (!NodesToExpand.Contains(node))
                            {
                                NodesToExpand.Add(node);
                            }
                        }

                        await GetExpandedNodes(n.Children);
                    }
                }
            }, DispatcherPriority.Background);
        }

        public NodeObject FilterMatch(NodeObject node)
        {
            if (FilterText.PartialContain(node.Label, node.ObjectId.ToString()))
            {
                return node;
            }

            var matchingChild = node.GetAllChildren()
                .FirstOrDefault(o => FilterText.PartialContain(o.Label, o.ObjectId.ToString()));
            return matchingChild;
        }

        private async void HubManagerOnNodeDeleted(Guid guid1, string data)
        {
            _log?.Info($"Node Deleted {data}");
            try
            {
                _ = this.TryUiAsync(async () =>
                {
                    var guid = Guid.Parse(data);
                    var existingNode = Nodes.FindNodeById<NodeObject>(guid);
                    if (existingNode != null)
                    {
                        var parentNode = Nodes.FindNodeById<NodeObject>(existingNode.ParentId);
                        if (parentNode != null)
                        {
                            await GetChildren(parentNode);
                        }
                    }
                });

                //if (Nodes.FindNodeByParentId<NodeObject>(guid) is NodeObject parent)
                //{
                //    Console.WriteLine(parent);
                //    await GetChildren(parent);
                //}
                //else if (Nodes.FindParentNodeByChildId<NodeObject>(guid) is NodeObject parentOfChild)
                //{
                //    await GetChildren(parentOfChild);
                //    //var childList = await NodeService
                //    //    .GetNode(Guid.Parse(data));
                //    //if (Nodes.FindNodeById(childList.Node.ParentId) is NodeObject parent)
                //    //{
                //    //    await GetChildren(parent);
                //    //}
                //}
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            //  LoadRootNode();
        }

        private async void HubManagerOnNodesUpdated(Guid guid, string data)
        {
            _ = this.TryUiAsync(async () =>
            {
                if (!IsUpdatingNodes)
                {
                    IsUpdatingNodes = true;
                    _log?.Info($"Node Updated {data}");
                    try
                    {
                        //update all expanded nodes...

                        var nodeList = View.CameraTree?.Items.OfType<NodeObject>().ToList();
                        if (nodeList != null)
                        {
                            NodesToExpand.Clear();
                            var firstNode = nodeList.OfType<NodeItemModel>().FirstOrDefault();
                            if (firstNode != null)
                            {
                                //firstNode.IsExpanded = true;
                                var rootNode =
                                    new NodeItemModel(new NodeDescriptor {Id = firstNode.ParentId, Type = "Node"})
                                        {IsExpanded = true};
                                rootNode.Children.Add(firstNode);
                                NodesToExpand.Add(rootNode); //add rootnode
                            }

                            await GetExpandedNodes(new ObservableCollection<NodeObject>(nodeList));
                            var resp = await NodeObjectItemManager.NodeManager
                                .RefreshNodeChildren(
                                    new ItemManagerRequestParameters
                                    {
                                        AcceptedCacheDuration = TimeSpan.FromSeconds(3),
                                        CacheDuration = TimeSpan.FromSeconds(3)
                                    }, NodesToExpand.Select(o => o.ObjectId).ToArray());

                            await Application.Current.Dispatcher.InvokeAsync(() =>
                            {
                                View.CameraTree.IsLoadOnDemandEnabled = false;
                            });

                            //await GetChildren(nodeList.FirstOrDefault());
                            foreach (var existingNode in NodesToExpand.Where(o =>
                                resp != null && resp.Any(n => n.ObjectId == o.ObjectId))
                            )
                            {
                                await ParseNodes(existingNode, resp);
                            }

                            await Application.Current.Dispatcher.InvokeAsync(() =>
                            {
                                View.CameraTree.IsLoadOnDemandEnabled = true;
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }

                    IsUpdatingNodes = false;
                }
            }, DispatcherPriority.Background);
        }

        private void CameraViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            //_ = this.TryUiAsync(async () =>
            //{
            try
            {
                if (e.Item is NodeObject item)
                {
                    NodeObject finalItemParent = null;
                    var finalItem = FilterMatch(item);

                    var nodeObjects = item.GetAllChildren().ToList();
                    nodeObjects.ForEach(o =>
                    {
                        //if (HideNonMatching)
                        //{
                        ////    App.TestLog.Info($"Setting {o.Label} Filter {FilterText}");
                        //    o.FilterText = o is CameraItemModel ? FilterText : string.Empty;
                        //}
                        //else
                        {
                            o.FilterText = o.Children.OfType<CameraItemModel>().Count() > 0 ? FilterText : string.Empty;
                        }

                        if (HideNonMatching && o.Children.Contains(finalItem))
                        {
                            finalItemParent = o;
                            if (finalItemParent is NodeItemModel node)
                            {
                                node.IsExpanded = true;
                            }

                            var parentOfParent =
                                nodeObjects.FirstOrDefault(b => b.ObjectId == finalItemParent.ParentId);
                            if (parentOfParent != null)
                            {
                                var adjacent = parentOfParent.Children.Where(b => b != finalItemParent);
                                foreach (var a in adjacent)
                                {
                                    //App.TestLog.Info($"Filtering Adjacent {a.Label}");
                                    a.FilterText = FilterText;
                                    foreach (var nodeObject in a.Children)
                                    {
                                        nodeObject.FilterText = FilterText;
                                    }
                                }
                            }
                        }
                    });

                    e.Accepted = finalItem != null;
                    if (finalItem != null)
                    {
                        SelectedItem = finalItem;
                        if (HideNonMatching)
                        {
                            finalItemParent?.GetAllChildren().ToList().ForEach(o => o.FilterText = FilterText);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                
            }
            //}, DispatcherPriority.Background);
        }

        public void CameraTreeOnEdited(object sender, RadTreeViewItemEditedEventArgs e)
        {
            _log?.Info($"Edited Items {e.OldValue}");
            _ = this.TryUiAsync(async () =>
            {
                if (e.OldValue is CameraItemModel camItem)
                {
                    camItem.UpdateCameraLabel();
                }

                if (e.OldValue is NodeItemModel nodeItem)
                {
                    nodeItem.UpdateNodeLabel();
                }
            }, DispatcherPriority.Background);
        }


        public async void CameraTreeOnLoadOnDemand(object sender, RadRoutedEventArgs e)
        {
            _ = this.TryUiAsync(async () =>
            {
                if (e.OriginalSource is RadTreeViewItem treeItem && treeItem.Item is NodeObject nodeItem)
                {
                    //if (IsUpdatingNodes)
                    //{
                    //    return;
                    //}

                    //_log?.Info($"Load On Demand {nodeItem}");
                    if (nodeItem is CameraItemModel cam)
                    {
                        if (string.IsNullOrEmpty(cam.Address))
                        {
                            treeItem.IsLoadingOnDemand = false;
                            return;
                        }

                        treeItem.IsLoadingOnDemand = true;
                        var monitoring = ObjectResolver.ResolveAll<MonitoringTab>()
                            .FirstOrDefault((o) =>
                            {
                                return o.TabIsFocused();
                            });

                        var controls = monitoring?.MonitoringView?.TileGridControl?.DynamicGrid?.Children
                            .OfType<EdgeVideoControl>().Where(o => o.IsVisible).ToList();

                        var tile = controls?.FirstOrDefault(o => o.IsFullscreen) ?? controls
                            ?.OrderBy(o => o.TileRow).ThenBy(o => o.TileColumn)
                            .FirstOrDefault(o => o.VideoObject == null) ?? controls
                            ?.FirstOrDefault();
                        if (monitoring?.MonitoringView?.TileGridControl?.Model != null)
                        {
                            if (tile != null)
                            {
                                monitoring.MonitoringView.TileGridControl.Model.SelectedComponents.Clear();
                                monitoring.MonitoringView.TileGridControl.Model.SelectedComponents.Add(tile);
                                tile.VideoObject = cam;
                                tile.Model?.PlayLive();
                            }
                        }

                        treeItem.IsLoadingOnDemand = false;
                    }
                    else
                    {
                        treeItem.IsLoadingOnDemand = true;

                        var complete = await GetChildren(nodeItem, treeItem);
                        if (complete)
                        {
                        }

                        // Console.WriteLine($"Get Children Completed!!!");
                        // treeItem.IsLoadingOnDemand = false;
                    }
                }
            }, DispatcherPriority.Background);
        }


        public void OnDragCompleted(object sender, DragDropCompletedEventArgs e)
        {
            //if (e.OriginalSource is RadTreeViewItem item)
            //{
            //    if (item.DataContext is CameraItemModel camera)
            //    {
            //        camera.AddToTile();
            //    }
            //}
        }
    }
}