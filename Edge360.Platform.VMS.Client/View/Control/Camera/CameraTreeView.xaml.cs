﻿using System;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.DragDrop;

namespace Edge360.Platform.VMS.Client.View.Control.Camera
{
    /// <summary>
    ///     Interaction logic for CameraTreeView.xaml
    /// </summary>
    public partial class CameraTreeView : UserControl, IView
    {
        public static readonly DependencyProperty ManagementToolsEnabledProperty = DependencyProperty.Register(
            "ManagementToolsEnabled", typeof(bool), typeof(CameraTreeView),
            new PropertyMetadata(default(bool), PropertyChangedCallback));

        public bool ManagementToolsEnabled
        {
            get => (bool) GetValue(ManagementToolsEnabledProperty);
            set => SetValue(ManagementToolsEnabledProperty, value);
        }

        public CameraTreeViewModel Model => DataContext as CameraTreeViewModel;


        public CameraTreeView()
        {
            InitializeComponent();
            Model.View = this;
            Model.View.CameraTree = CameraTree;
            CameraTree.LoadOnDemand += CameraTreeOnLoadOnDemand;
            CameraTree.Edited += CameraTreeOnEdited;
            DragDropManager.AddDragDropCompletedHandler(CameraTree, Model.OnDragCompleted, true);
        }

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (d is CameraTreeView view)
                {
                    view.Model.ManagementToolsEnabled = view.ManagementToolsEnabled;
                }
            }
            catch (Exception exception)
            {
                
            }
        }

        private void CameraTreeOnEdited(object sender, RadTreeViewItemEditedEventArgs e)
        {
            Model?.CameraTreeOnEdited(sender, e);
        }

        private void CameraTreeOnLoadOnDemand(object sender, RadRoutedEventArgs e)
        {
            Model?.CameraTreeOnLoadOnDemand(sender, e);
        }
    }
}