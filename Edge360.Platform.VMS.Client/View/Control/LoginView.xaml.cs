﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Edge360.Platform.VMS.Client.View.Control
{
    /// <summary>
    ///     Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl, IView
    {
        public LoginViewModel Model => DataContext as LoginViewModel;

        public LoginView()
        {
            Loaded += OnLoaded;
            KeyDown += OnKeyDown;

            InitializeComponent();
            Model.View = this;
        }

        private void CheckCapsLockPressed()
        {
            try
            {
                Model.WarningMessage = Keyboard.IsKeyToggled(Key.CapsLock) ? "CapsLock is enabled." : "";
            }
            catch (Exception exception)
            {
            }
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            CheckCapsLockPressed();
        }


        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Model?.ViewOnLoaded(sender, e);
                CheckCapsLockPressed();
                Task.Factory.StartNew(async o =>
                {
                    while (!App.IsExiting)
                    {
                        await Task.Delay(5000);
                        try
                        {
                            if (Visibility == Visibility.Visible)
                            {
                                App.Current?.Dispatcher?.InvokeAsync(() =>
                                {
                                    CheckCapsLockPressed();
                                });
                            }
                        }
                        catch (Exception exception)
                        {
                            
                        }
                    }
                }, TaskCreationOptions.LongRunning);
            }
            catch (Exception exception)
            {
            }
        }
    }
}