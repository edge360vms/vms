﻿using System.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Mapping
{
    /// <summary>
    ///     Interaction logic for MappingView.xaml
    /// </summary>
    public partial class MappingView : UserControl, IView
    {
        public MappingView()
        {
            InitializeComponent();
        }
    }
}