﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Client.View.Test;
using Edge360.Platform.VMS.Common.Interfaces.Video;
using Edge360.Platform.VMS.Common.Util;

namespace Edge360.Platform.VMS.Client.View.Control.Thumbnail
{
    /// <summary>
    /// Interaction logic for ThumbnailStripView.xaml
    /// </summary>
    public partial class ThumbnailStripView : UserControl , IDisposable
    {
         public static ConcurrentDictionary<Guid, List<ThumbnailItemModel>> ThumbnailCache =
            new ConcurrentDictionary<Guid, List<ThumbnailItemModel>>();


         public EdgeVideoControl Owner
         {
             get => _owner;
             set => _owner = value;
         }

         private EdgeVideoControl _owner;
         public bool StripIsActive { get; set; }

        public ThumbnailStripView()
        {
            InitializeComponent();
            DataContext = new ThumbnailStripViewModel(new List<ThumbnailItemModel>());
            SizeChanged += OnSizeChanged;
        }

        public void Dispose()
        {
            if (GetModel() != null)
            {
                ClearThumbs();

                GetModel().Thumbnails.Clear();
                GetModel().Thumbnails = null;
                GetModel().RequestThumbnailUpdate = null;
            }

            SizeChanged -= OnSizeChanged;
            DataContext = null;
            _owner = null;
        }

        public void SetOwner(EdgeVideoControl owner)
        {
            _owner = owner;
        }

        public ThumbnailStripViewModel GetModel()
        {
            return (ThumbnailStripViewModel) DataContext;
        }

        public void SetThumbnails(List<ThumbnailItemModel> items)
        {
            //DataContext = new ThumbnailViewModel(items);
            var model = GetModel();
            if (model != null && items != null)
            {
                ClearThumbs();
                var cachedThumbnails =
                    ThumbnailCache?.FirstOrDefault(k => k.Key == _owner.VideoObject.Id).Value;

                var matchingThumbs = cachedThumbnails?.Where(t =>
                    t.DateTime > _owner.EdgeVideoTimeline.DataProvider.BeginTime &&
                    t.DateTime <= _owner.EdgeVideoTimeline.DataProvider.EndTime &&
                    items.All(o => o.DateTime != t.DateTime)).ToList();

                // if (newThumbs != null)
                {
                    //Trace.WriteLine($"{newThumbs.Count} cached thumbnails matched the time");

                    if (matchingThumbs != null)
                    {
                        var thumbsMissing = (int) (GetModel().DesiredThumbnailCount - items.Count);
                        if (thumbsMissing > 0)
                        {
                            items.AddRange(matchingThumbs.Take(thumbsMissing));
                        }
                    }

                    if (cachedThumbnails == null)
                    {
                        cachedThumbnails = new List<ThumbnailItemModel>();
                    }

                    cachedThumbnails.AddRange(items.Where(t => !cachedThumbnails.Contains(t)));
                }

                items = items.OrderBy(o => o.DateTime).ToList();
                foreach (var i in items)
                {
                    model.Thumbnails.Add(i);
                    i.PropertyChanged += OnItemPropertyChanged;
                }

                if (_owner.VideoObject != null)
                {
                    ThumbnailCache?.TryAdd(_owner.VideoObject.Id, cachedThumbnails);
                }

                if (StripIsActive)
                {
                    _ = this.TryUiAsync(async () =>
                    {
                        await Task.Delay(100);
                        foreach (var i in items)
                        {
                            i.IsOpen = false;
                            i.IsOpen = true;
                        }
                    });
                }
            }
        }

        private void ClearThumbs()
        {
            if (GetModel() != null)
            {
                foreach (var i in GetModel().Thumbnails)
                {
                    i.PropertyChanged -= OnItemPropertyChanged;
                    i.ThumbnailSelected = null;
                }

                GetModel().Thumbnails.Clear();
            }
        }

        public int GetEstimatedHeight(double width)
        {
            return (int) (129 * Math.Min(1, width / GetModel().DesiredThumbnailCount / 230));
        }

        public void CalculateThumbnailHeight()
        {
            var thumbWidth = Width / GetModel().Thumbnails.Count;
            var imageScale = thumbWidth / 230; // 230x129 thumbnail image size ironyun 
            var maxHeight = 129 * Math.Min(1, imageScale);

            Height = Math.Min(Height, Math.Min(129, maxHeight));
            MaxHeight = 129;
        }

        private void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsHovered")
            {
                if (sender is ThumbnailItemModel model)
                {
                    _owner?.EdgeVideoTimeline.DataProvider.Thumbnails.Clear();
                    if (model.IsHovered)
                    {
                        _owner?.EdgeVideoTimeline.DataProvider.Thumbnails.Add(model);
                    }
                    _owner?.EdgeVideoTimeline?.DrawThumbnails();
                        
                }
                
            }
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            CalculateThumbnailHeight();
        }

        private void UniformGrid_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                Height = e.Delta > 0 ? Height + 6 : Height - 6;

                //CheckHeight();
            }
            else
            {
                e.Handled = false;

                var model = GetModel();
                if (model != null)
                {
                    if (e.Delta > 0)
                    {
                        model.DesiredThumbnailCount++;
                    }
                    else
                    {
                        model.DesiredThumbnailCount--;
                    }

                    model.RequestThumbnailUpdate?.Invoke();
                }
            }
        }

        private void Thumbnail_MouseHover(object sender, MouseEventArgs e)
        {
            if (sender is Button b && b.DataContext is ThumbnailItemModel model)
            {
                model.IsHovered = true;
                model.IsOpen = true;
            }
        }

        private void Thumbnail_MouseHoverLeave(object sender, MouseEventArgs e)
        {
            if (sender is Button b && b.DataContext is ThumbnailItemModel model)
            {
                model.IsHovered = false;
                model.IsOpen = true;
            }
        }

    }

    public class ThumbnailStripViewModel : INotifyPropertyChanged
    {
        private uint _desiredThumbnailCount = 8;
        private bool _isHovered;

        public Action RequestThumbnailUpdate;

        public uint DesiredThumbnailCount
        {
            get => _desiredThumbnailCount;
            set
            {
                if (_desiredThumbnailCount != value)
                {
                    _desiredThumbnailCount = Math.Min(20, Math.Max(1, value));
                    OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<ThumbnailItemModel> Thumbnails { get; set; } =
            new ObservableCollection<ThumbnailItemModel>();

        public ThumbnailStripViewModel(List<ThumbnailItemModel> items)
        {
            foreach (var i in items)
            {
                //  Trace.WriteLine(i.ThumbnailTime);
                Thumbnails.Add(i);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
