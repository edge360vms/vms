﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.Extensions.CommandLine;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Monitoring;
using Edge360.Platform.VMS.Client.View.Control.Monitoring.TileGrid;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Interfaces;
using Edge360.Vms.AuthorityService.SharedModel;
using Edge360.Vms.AuthorityService.SharedModel.Enums;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using Newtonsoft.Json;
using Telerik.Windows;
using Telerik.Windows.Controls;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control.Layouts
{
    /// <summary>
    ///     Interaction logic for CameraTreeView.xaml
    /// </summary>
    public partial class LayoutTreeView : UserControl, IView
    {
        public LayoutTreeViewModel Model => DataContext as LayoutTreeViewModel;

        public LayoutTreeView()
        {
            InitializeComponent();
            Model.View = this;
        }

        private void LayoutTree_OnEdited(object sender, RadTreeViewItemEditedEventArgs e)
        {
            if (e.OldValue is LayoutItemModel layoutItem)
            {
                layoutItem.UpdateLabel();
            }
        }

        private void LayoutTree_OnItemDoubleClick(object sender, RadRoutedEventArgs e)
        {
            if (e.OriginalSource is RadTreeViewItem treeItem && treeItem.Item is LayoutItemModel layoutItem)
            {
                treeItem.IsLoadingOnDemand = true;
                var monitoring = ObjectResolver.ResolveAll<MonitoringTab>().FirstOrDefault(o => o.TabIsFocused());
                monitoring?.TryUiAsync(async () =>
                {
                    var controls = monitoring.MonitoringView?.TileGridControl?.DynamicGrid?.Children
                        ?.OfType<EdgeVideoControl>().OrderBy(o => o.TileRow).ToList();
                    var tile = controls
                        ?.FirstOrDefault(o => o.VideoObject == null) ?? controls
                        ?.FirstOrDefault();
                    if (monitoring?.MonitoringView?.TileGridControl?.Model != null)
                    {
                        if (tile != null)
                        {
                            monitoring.MonitoringView.TileGridControl.Model.SelectedComponents.Clear();
                            await monitoring.MonitoringView.TileGridControl.Model.HandleLayoutItem(layoutItem);
                        }
                    }
                });
                treeItem.IsLoadingOnDemand = false;
            }
        }
    }

    public class LayoutTreeViewModel : INotifyPropertyChanged
    {
        private RelayCommand _clearFilterTextCommand;
        private RelayCommand _createLayoutShortcutCommand;
        private RelayCommand _deleteNodeCommand;
        private string _filterText = string.Empty;
        private bool _isEditing;
        private ObservableCollection<LayoutItemModel> _layouts = new ObservableCollection<LayoutItemModel>();
        private RelayCommand _refreshNodeCommand;
        private ICommand _renameCommand;
        private RelayCommand _renameNodeCommand;
        private ILog _log;
        private LayoutItemManager _layoutItemMgr;

        public static bool LayoutBeenInitialized { get; set; }

        public ICommand ClearFilterTextCommand
        {
            get
            {
                if (_clearFilterTextCommand == null)
                {
                    _clearFilterTextCommand = new RelayCommand(
                        ClearFilterText,
                        param => true
                    );
                }

                return _clearFilterTextCommand;
            }
        }


        public ICommand CreateLayoutShortcutCommand
        {
            get
            {
                if (_createLayoutShortcutCommand == null)
                {
                    _createLayoutShortcutCommand = new RelayCommand(
                        CreateShortcut,
                        param => CanCreateShortcut()
                    );
                }

                return _createLayoutShortcutCommand;
            }
        }

        public ICommand DeleteNodeCommand
        {
            get
            {
                if (_deleteNodeCommand == null)
                {
                    _deleteNodeCommand = new RelayCommand(
                        DeleteNode,
                        param => CanDeleteLayout()
                    );
                }

                return _deleteNodeCommand;
            }
        }

        //new SortDescription{PropertyName = "GridSize"}
        public string FilterText
        {
            get => _filterText;
            set
            {
                if (_filterText != value)
                {
                    _filterText = value;
                    OnPropertyChanged();
                    LayoutCollectionViewSource.View.Refresh();
                }
            }
        }

        public bool IsEditing
        {
            get => _isEditing;
            set
            {
                if (_isEditing != value)
                {
                    _isEditing = value;
                    OnPropertyChanged();
                }
            }
        }

        public CollectionViewSource LayoutCollectionViewSource { get; set; } = new CollectionViewSource
            {SortDescriptions = {new SortDescription {PropertyName = "Label"}}};


        public ObservableCollection<LayoutItemModel> Layouts
        {
            get => _layouts;
            set
            {
                if (_layouts != value)
                {
                    _layouts = value;
                    OnPropertyChanged();
                }
            }
        }


        public ICommand RefreshNodeCommand
        {
            get
            {
                if (_refreshNodeCommand == null)
                {
                    _refreshNodeCommand = new RelayCommand(
                        RefreshNodes,
                        param => CanRefresh()
                    );
                }

                return _refreshNodeCommand;
            }
        }


        public ICommand RenameCommand
        {
            get
            {
                return _renameCommand ??= new RelayCommand(
                    param => Rename(),
                    param => CanRename()
                );
            }
        }

        public ICommand RenameNodeCommand
        {
            get
            {
                if (_renameNodeCommand == null)
                {
                    _renameNodeCommand = new RelayCommand(
                        param => Rename(),
                        param => CanRename()
                    );
                }

                return _renameNodeCommand;
            }
        }

        public LayoutTreeView View { get; set; }

        public LayoutTreeViewModel(ILog log = null, LayoutItemManager layoutItemManager = null)
        {
            _log = log;
            LayoutItemMgr = layoutItemManager;
            Dispatcher.CurrentDispatcher.InvokeAsync(async () =>
            {
                BindingOperations.SetBinding(LayoutCollectionViewSource,
                    CollectionViewSource.SourceProperty,
                    new Binding(nameof(Layouts)) {Source = this, Mode = BindingMode.OneWay});
                LayoutCollectionViewSource.Filter += LayoutCollectionViewSourceOnFilter;
            }, DispatcherPriority.Background);
            //try
            //{
            //    HubManager.SignalRClientEvent += async (e, zone, data) =>
            //    {
            //        if (Equals(e, ESignalRClientEvent.GridLayoutUpdated))
            //        {
            //            HubManagerOnGridLayoutUpdated(zone, data);
            //        }
            //    };
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //}

            LoadLayouts();
        }

        public LayoutItemManager LayoutItemMgr
        {
            get => _layoutItemMgr;
            set
            {
                if (Equals(value, _layoutItemMgr)) return;
                _layoutItemMgr = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void CreateShortcut(object obj)
        {
            if (View?.LayoutTree?.SelectedItem is LayoutItemModel item)
            {
                if (item.Id != null)
                {
                    var args = new CommandLineManager.CommandLineArgs
                    {
                        Layout = item.Id,
                        BearerToken = SettingsManager.Settings.Login.Credentials.BearerToken,
                        RefreshToken = SettingsManager.Settings.Login.Credentials.RefreshToken,
                        //Username = SettingsManager.Settings.Login.Credentials.UserName,
                        //Password = SettingsManager.Settings.Login.Credentials.PasswordLength,

                        ServerAddress = SettingsManager.Settings.Login.Credentials.Domain
                    };
                    Clipboard.SetText(
                        $"-l={args.Layout} -b={args.BearerToken} -r={args.RefreshToken} -s={args.ServerAddress}");
                }
            }
        }

        private bool CanCreateShortcut()
        {
            return View?.LayoutTree?.SelectedItem is LayoutItemModel;
        }

        private bool CanRename()
        {
            return View?.LayoutTree?.SelectedItem is LayoutItemModel layout &&
                   (layout.IsPersonal || EffectivePermissions.HasRoleAny(ERoles.Administrator | ERoles.LayoutEditor));
        }

        private void Rename()
        {
            IsEditing = true;
        }

        private void ClearFilterText(object obj)
        {
            FilterText = string.Empty;
        }

        private void RefreshNodes(object obj)
        {
            LoadLayouts();
        }

        private bool CanRefresh()
        {
            return true;
        }

        private async void DeleteNode(object obj)
        {
            if (View?.LayoutTree?.SelectedItem is LayoutItemModel layout)
            {
                var deleted = await layout.DeleteLayout();
                if (deleted)
                {
                    //LoadLayouts(); //SignalR does this
                }
            }
        }

        private bool CanDeleteLayout()
        {
            return View?.LayoutTree?.SelectedItem is LayoutItemModel layout &&
                   (layout.IsPersonal || EffectivePermissions.HasRoleAny(ERoles.Administrator | ERoles.LayoutEditor));
        }

        public async void LoadLayouts()
        {
            try
            {
                
                var remoteLayouts = await LayoutItemMgr.RefreshLayouts();// await GetManagerByZoneId().GridLayoutService.ListGridLayouts();
                HandleStartupLayout(remoteLayouts?.FirstOrDefault(o => o.Id == CommandLineManager.StartupArgs.Layout));
                await App.Current.Dispatcher.InvokeAsync(() =>
                {
                    Layouts.Clear();
                    if (remoteLayouts != null)
                    {
                        Layouts.AddRange(remoteLayouts);
                    }
                });

                //var localLayouts = LayoutManager.Layouts?.Layouts
                //    ?.Where(o => remoteLayouts.All(r => r.Id != o.Id)).ToList();


                //if (localLayouts != null && localLayouts.Any())
                //{
                //    View.WpfUiThread(() => { Layouts.AddRange(localLayouts); });
                //}
            }
            catch (Exception e)
            {
            }
        }

        public void HandleStartupLayout(IGridLayout remoteLayouts)
        {
            if (!TileGridView.StartupLayoutBeenInitialized && CommandLineManager.StartupArgs.Layout != Guid.Empty)
            {
                TileGridView.StartupLayoutBeenInitialized = true;
                var matchingLayout =
                    remoteLayouts;
                if (matchingLayout != null)
                {
                    var _ = Task.Run(async () =>
                    {
                        try
                        {
                            if (matchingLayout.LayoutData != null)
                            {
                                var deserialized =
                                    JsonConvert.DeserializeObject<LayoutItemModel>(matchingLayout.LayoutData,
                                        ClientJsonUtil.IgnoreExceptionSettings());
                                if (deserialized != null)
                                {
                                    await App.Current.Dispatcher.InvokeAsync(async () =>
                                    {
                                        if (View != null)
                                        {
                                            await View.ParentOfType<MonitoringView>()?.TileGridControl?.Model?
                                                .HandleLayoutItem(deserialized);
                                        }
                                    });
                                }
                            }
                        }
                        catch
                        {
                        }
                    }).ConfigureAwait(false);
                }
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void HubManagerOnGridLayoutUpdated(Guid guid, string s)
        {
            LoadLayouts();
        }

        private void LayoutCollectionViewSourceOnFilter(object sender, FilterEventArgs e)
        {
            e.Accepted = e.Item is LayoutItemModel item &&
                         FilterText
                             .PartialContain(item.Label, item?.GridSize.ToString(), item?.ObjectId?.ToString());
        }
    }
}