﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Edge360.Platform.VMS.Client.View.Control.SyntaxHighlightingBox
{
    public static class TextBlockHighlighter
    {
        public static readonly DependencyProperty SelectionRegexProperty =
            DependencyProperty.RegisterAttached("SelectionRegex", typeof(string), typeof(TextBlockHighlighter),
                new PropertyMetadata(SelectText));

        public static readonly DependencyProperty BackgroundProperty =
            DependencyProperty.RegisterAttached("Background", typeof(Brush), typeof(TextBlockHighlighter),
                new PropertyMetadata(Brushes.Yellow, SelectText));

        public static readonly DependencyProperty ForegroundProperty =
            DependencyProperty.RegisterAttached("Foreground", typeof(Brush), typeof(TextBlockHighlighter),
                new PropertyMetadata(Brushes.Black, SelectText));

        public static string GetSelectionRegex(DependencyObject obj)
        {
            return (string) obj.GetValue(SelectionRegexProperty);
        }

        public static void SetSelectionRegex(DependencyObject obj, string value)
        {
            obj.SetValue(SelectionRegexProperty, value);
        }

        private static void SelectText(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d == null)
            {
                return;
            }

            if (!(d is TextBlock))
            {
                throw new InvalidOperationException("Only valid for TextBlock");
            }

            var txtBlock = d as TextBlock;
            var text = txtBlock.Text;
            if (string.IsNullOrEmpty(text))
            {
                return;
            }

            var highlightText = (string) d.GetValue(SelectionRegexProperty);
            if (string.IsNullOrEmpty(highlightText))
            {
                return;
            }

            var match = Regex.Match(text, highlightText);
            if (!match.Success)
            {
                return;
            }

            var selectionColor = (Brush) d.GetValue(BackgroundProperty);
            var forecolor = (Brush) d.GetValue(ForegroundProperty);

            txtBlock.Inlines.Clear();
            txtBlock.Inlines.AddRange(new Inline[]
            {
                new Run(text.Substring(0, match.Index - 1)),
                new Run(text.Substring(match.Index, match.Length))
                {
                    Background = selectionColor,
                    Foreground = forecolor
                }
            });
            var end = match.Index + match.Length;
            if (end < text.Length)
            {
                txtBlock.Inlines.Add(new Run(text.Substring(end, text.Length - end)));
            }
        }

        public static Brush GetBackground(DependencyObject obj)
        {
            return (Brush) obj.GetValue(BackgroundProperty);
        }

        public static void SetBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(BackgroundProperty, value);
        }


        public static Brush GetForeground(DependencyObject obj)
        {
            return (Brush) obj.GetValue(ForegroundProperty);
        }

        public static void SetForeground(DependencyObject obj, Brush value)
        {
            obj.SetValue(ForegroundProperty, value);
        }
    }
}