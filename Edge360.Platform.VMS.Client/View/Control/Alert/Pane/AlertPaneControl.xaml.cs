﻿namespace Edge360.Platform.VMS.Client.View.Control.Alert.Pane
{
    /// <summary>
    ///     Interaction logic for AlertPaneControl.xaml
    /// </summary>
    public partial class AlertPaneControl : IView
    {
        public AlertPaneControl()
        {
            InitializeComponent();
            DataContext = new AlertPaneViewControl();
        }
    }
}