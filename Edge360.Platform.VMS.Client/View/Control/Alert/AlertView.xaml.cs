﻿using Edge360.Platform.VMS.Client.ViewModel.View;

namespace Edge360.Platform.VMS.Client.View.Control.Alert
{
    /// <summary>
    ///     Interaction logic for AlertView.xaml
    /// </summary>
    public partial class AlertView : IView
    {
        public AlertView()
        {
            InitializeComponent();
            DataContext = new AlertViewModel();
        }
    }
}