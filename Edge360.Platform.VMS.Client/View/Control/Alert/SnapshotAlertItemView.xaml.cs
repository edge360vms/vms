﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Edge360.Platform.VMS.Client.Annotations;
using Telerik.Windows.Controls;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Edge360.Platform.VMS.Client.View.Control.Alert
{
    /// <summary>
    ///     Interaction logic for SnapshotAlertItemView.xaml
    /// </summary>
    public partial class SnapshotAlertItemView : RadDesktopAlert
    {
        public SnapshotAlertItemViewModel Model => DataContext as SnapshotAlertItemViewModel;

        public SnapshotAlertItemView()
        {
            InitializeComponent();
        }
    }

    public class SnapshotAlertItemViewModel : INotifyPropertyChanged
    {
        private DelegateCommand _clickCommand;
        private DelegateCommand _copyImageToClipboard;
        private string _description;
        private string _filePath;
        private ImageSource _imageSource;
        private DelegateCommand _openFileCommand;
        private DelegateCommand _openFolderCommand;
        private DateTimeOffset _timestamp;

        public DelegateCommand ClickCommand => _clickCommand ??= new DelegateCommand(Clicked);

        public DelegateCommand CopyImageToClipboard => _copyImageToClipboard ??= new DelegateCommand(CopyImage);

        public string Description
        {
            get => _description;
            set
            {
                if (value == _description)
                {
                    return;
                }

                _description = value;
                OnPropertyChanged();
            }
        }

        public string FilePath
        {
            get => _filePath;
            set
            {
                if (value == _filePath)
                {
                    return;
                }

                _filePath = value;
                OnPropertyChanged();
            }
        }

        public ImageSource ImageSource
        {
            get => _imageSource;
            set
            {
                if (Equals(value, _imageSource))
                {
                    return;
                }

                _imageSource = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand OpenFileCommand => _openFileCommand ??= new DelegateCommand(OpenFile);

        public DelegateCommand OpenFolderCommand => _openFolderCommand ??= new DelegateCommand(OpenFolder);

        public DateTimeOffset Timestamp
        {
            get => _timestamp;
            set
            {
                if (value.Equals(_timestamp))
                {
                    return;
                }

                _timestamp = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void CopyImage()
        {
            try
            {
                if (ImageSource != null)
                {
                    Clipboard.SetImage(ImageSource as BitmapSource);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void OpenFile()
        {
            try
            {
                Process.Start(new ProcessStartInfo("explorer", FilePath)
                {
                    UseShellExecute = true
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void OpenFolder()
        {
            try
            {
                Process.Start(new ProcessStartInfo("explorer", new FileInfo(FilePath).Directory?.FullName)
                {
                    UseShellExecute = true
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public event Action ImageClicked;

        private void Clicked()
        {
            ImageClicked?.Invoke();
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}