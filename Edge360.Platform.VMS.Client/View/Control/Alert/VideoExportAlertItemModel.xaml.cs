﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Alert;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using RecordingService.ServiceModel.Api.VideoExportJob;

namespace Edge360.Platform.VMS.Client.View.Control.Alert
{
    /// <summary>
    ///     Interaction logic for HealthMonitoringAlert.xaml
    /// </summary>
    public partial class VideoExportAlertItemModel : IAlertItem, INotifyPropertyChanged
    {
        private RelayCommand _clickCommand;
        private string _description;
        private VideoExportJobDescriptor _descriptor;
        private string _glyph;
        private string _glyphFont;

        //public RelayCommand ClickCommand => _clickCommand ?? (_clickCommand = new RelayCommand(o => Click(o), o => CanClick()));

        //private bool CanClick()
        //{
        //    return true;
        //}

        //public event Action Clicked;

        //   public new RelayCommand Command { get; set; }
        public VideoExportJobDescriptor Descriptor
        {
            get => _descriptor;
            set
            {
                if (Equals(value, _descriptor))
                {
                    return;
                }

                _descriptor = value;
                OnPropertyChanged();
            }
        }

        public string GlyphFont
        {
            get => _glyphFont;
            set
            {
                if (value == _glyphFont)
                {
                    return;
                }

                _glyphFont = value;
                OnPropertyChanged();
            }
        }

        public VideoExportAlertItemModel()
        {
            InitializeComponent();
            _glyphFont = TryFindResource("TelerikFont") as string;
            _glyph = TryFindResource("GlyphDownload") as string;
            DataContext = this;
        }

        public string Description
        {
            get => _description;
            set
            {
                if (value == _description)
                {
                    return;
                }

                _description = value;
                OnPropertyChanged();
            }
        }

        public string Glyph
        {
            get => _glyph;
            set
            {
                if (value == _glyph)
                {
                    return;
                }

                _glyph = value;
                OnPropertyChanged();
            }
        }

        public string Title { get; set; }
        public bool IsHandled { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}