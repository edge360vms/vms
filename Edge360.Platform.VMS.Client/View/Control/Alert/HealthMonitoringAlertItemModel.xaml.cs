﻿using Edge360.Platform.VMS.Client.ItemModel.Alert;

namespace Edge360.Platform.VMS.Client.View.Control.Alert
{
    /// <summary>
    ///     Interaction logic for HealthMonitoringAlert.xaml
    /// </summary>
    public partial class HealthMonitoringAlertItemModel : IAlertItem
    {
        public HealthMonitoringAlertItemModel()
        {
            InitializeComponent();
            DataContext = this;
        }

        //public new RelayCommand Command { get; set; }
        public string Description { get; set; }
        public string Glyph { get; set; }
        public string Title { get; set; }
        public bool IsHandled { get; set; }
    }
}