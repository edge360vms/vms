﻿namespace Edge360.Platform.VMS.Client.View.Control.Alert.DataGrid
{
    /// <summary>
    ///     Interaction logic for AlertDataGridControl.xaml
    /// </summary>
    public partial class AlertDataGridControl : IView
    {
        public AlertDataGridControl()
        {
            InitializeComponent();
            DataContext = new AlertDataGridViewModel();
        }
    }
}