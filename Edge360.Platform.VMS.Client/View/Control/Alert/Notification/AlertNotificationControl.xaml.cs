﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Alert;
using Edge360.Platform.VMS.Common.Extensions.Commands;

namespace Edge360.Platform.VMS.Client.View.Control.Alert.Notification
{
    /// <summary>
    ///     Interaction logic for AlertNotificationControl.xaml
    /// </summary>
    public partial class AlertNotificationControl : UserControl
    {
        public AlertNotificationControlViewModel Model => DataContext as AlertNotificationControlViewModel;

        public AlertNotificationControl()
        {
            InitializeComponent();
            DataContext = ObjectResolver.Resolve<AlertNotificationControlViewModel>() ??
                          new AlertNotificationControlViewModel(this);
            ObjectResolver.Register<AlertNotificationControlViewModel>(DataContext, overwrite: true);
        }
    }

    public class AlertNotificationControlViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<IAlertItem> _alerts = new ObservableCollection<IAlertItem>();
        private RelayCommand _clearAlertsCommand;
        private RelayCommand _deleteAlertCommand;
        private bool _isRefreshingAlerts;
        private RelayCommand _refreshAlertsCommand;
        private AlertNotificationControl _view;

        public ObservableCollection<IAlertItem> Alerts
        {
            get => _alerts;
            set
            {
                if (Equals(value, _alerts))
                {
                    return;
                }

                _alerts = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand ClearAlertsCommand => _clearAlertsCommand ??= new RelayCommand(DeleteAlerts,
            o => !IsRefreshingAlerts);

        public RelayCommand DeleteAlertCommand => _deleteAlertCommand ??= new RelayCommand(DeleteAlert, p => CanDeleteAlert());

        public bool IsRefreshingAlerts
        {
            get => _isRefreshingAlerts;
            set
            {
                if (value == _isRefreshingAlerts)
                {
                    return;
                }

                _isRefreshingAlerts = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(RefreshAlertsCommand));
            }
        }

        public RelayCommand RefreshAlertsCommand => _refreshAlertsCommand ??= new RelayCommand(RefreshAlerts,
            o => !IsRefreshingAlerts);

        public AlertNotificationControlViewModel(AlertNotificationControl view)
        {
            _view = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void DeleteAlerts(object obj)
        {
            Alerts.Clear();
        }

        private void RefreshAlerts(object obj)
        {
            AddTestAlert();
        }

        private void AddTestAlert()
        {
            var alert = new HealthMonitoringAlertItemModel
            {
                Description = "This is a test Alert.", Title = "Health Monitoring",
                Glyph = ""
            };
            Alerts.Add(alert);
        }

        private void DeleteAlert(object obj)
        {
            if (obj is IAlertItem item)
            {
                if (Alerts.Contains(item))
                {
                    Alerts.Remove(item);
                }
            }
        }

        private bool CanDeleteAlert()
        {
            return true;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}