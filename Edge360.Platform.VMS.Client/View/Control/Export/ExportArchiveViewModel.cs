﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ViewModel;
using Edge360.Vms.Client.VideoExport.Common.Events;
using Prism.Events;
using Prism.Regions;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control.Export
{
    public class ExportArchiveViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private bool _isConnected;
        public IEventAggregator EventAggregator { get; set; }

        public bool IsConnected
        {
            get => _isConnected;
            set
            {
                if (value == _isConnected)
                {
                    return;
                }

                _isConnected = value;
                OnPropertyChanged();
            }
        }

        private IRegionManager RegionManager { get; }

        public ExportArchiveViewModel(IEventAggregator eventAggregator = null, IRegionManager regionManager = null)
        {
            EventAggregator = eventAggregator;
            RegionManager = regionManager;
            ViewLoaded();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public async void ViewLoaded()
        {
            try
            {
                await Task.Delay(1200);
                EventAggregator.GetEvent<ServiceClientActivatedEvent>()
                    .Publish(GetManagerByZoneId());
                IsConnected = true;
                //var cameras = (await GetManagerByZoneId().CameraService.ListCameras()).Cameras;
                //_eventAggregator.GetEvent<>().Publish(cameras);
                //var jobs = await GetManagerByZoneId().VideoExportJobService.ListJobs();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}