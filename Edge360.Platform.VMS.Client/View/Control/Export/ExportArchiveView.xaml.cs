﻿using System.Windows;
using System.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Export
{
    /// <summary>
    ///     Interaction logic for ExportArchiveView.xaml
    /// </summary>
    public partial class ExportArchiveView : UserControl, IView
    {
        public ExportArchiveViewModel Model => DataContext as ExportArchiveViewModel;

        public ExportArchiveView()
        {
            Loaded += OnLoaded;
            InitializeComponent();
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
        }
    }
}