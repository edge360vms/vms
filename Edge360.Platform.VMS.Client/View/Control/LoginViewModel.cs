﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.Extensions.CommandLine;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Services;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Alert;
using Edge360.Platform.VMS.Client.View.Control.Alert.Notification;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Client.View.Window;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Communication.Polly;
using Edge360.Platform.VMS.Communication.Services;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.TelemetryControl.DataObject;
using Edge360.Platform.VMS.TelemetryControl.ViewModel;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Vms.AuthorityService.AdSsoClient;
using Edge360.Vms.AuthorityService.AdSsoClient.AdSsoServiceReference;
using Edge360.Vms.AuthorityService.SharedModel.Enums;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using RecordingService.ServiceModel.Api.VideoExportJob;
using ServiceStack;
using ServiceStack.Text;
using Telerik.Windows.Controls;
using Unity;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control
{
    public class LoginViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private readonly bool _canCancelConnect = true;

        private readonly ILog _log;

        private readonly ILog _networkLog = LogManager.GetLogger("NetworkLogger");

        public readonly RadDesktopAlertManager AlertManager;
        private readonly bool TEST_WINDOW_ENABLED = false;
        private string _adDomain;
        private RelayCommand _cancelConnectCommand;
        private bool _canConnect = true;

        private RelayCommand _connectCommand;

        private UserLoginItemModel _credential =
            SettingsManager.Settings.Login.Credentials.ConvertTo<UserLoginItemModel>() ?? new UserLoginItemModel();

        private RelayCommand _openSettingsCommand;
        private bool _skipValidation;
        private string _statusMessage;
        private RadTabbedWindow _tabbedWindow;
        private bool _windowsAuth;

        // public readonly Dictionary<ETelemetryAlert, DateTime> TelemetryAlertDictionary = new Dictionary<ETelemetryAlert, DateTime>();

        public bool IsHealthUsageHigh;
        private string _warningMessage;
        public static bool IsLoggedIn { get; set; }

        public string AdDomain
        {
            get => _adDomain;
            set
            {
                if (value == _adDomain)
                {
                    return;
                }

                _adDomain = value;
                OnPropertyChanged();
            }
        }

        public CacheUpdateService CacheUpdateService { get; }

        public CameraItemModelManager CameraItemMgr { get; set; }

        public RelayCommand CancelConnectCommand => _cancelConnectCommand ??= new RelayCommand(p => CancelConnect(),
            p => CanCancelConnect());

        public bool CanChangeLoginAndConnect => CanConnect && !WindowsAuth;

        public bool CanConnect
        {
            get => _canConnect;
            set
            {
                if (_canConnect != value)
                {
                    _canConnect = value;

                    OnPropertyChanged();
                    OnPropertyChanged(nameof(CanChangeLoginAndConnect));
                    Trace.WriteLine($"LoginViewModel 122 InvalidateRequerySuggested");
                    OnPropertyChanged(nameof(ConnectCommand));
                    
                    //CommandManager.InvalidateRequerySuggested();
                }
            }
        }

        public RelayCommand ConnectCommand => _connectCommand ??= new RelayCommand(p => Connect(p),
            p => CanConnect);

        public UserLoginItemModel Credential
        {
            get => _credential;
            set
            {
                if (_credential != value)
                {
                    _credential = value;
                    SettingsManager.Settings.Login.Credentials = Credential;
                    OnPropertyChanged();
                }
            }
        }

        public DirectModeManager DirectModeMgr { get; set; }


        public int HealthAlertThresholdTicks { get; set; }

        public DateTime LastHealthAlertTime { get; set; } = DateTime.Now.AddSeconds(10);

        public LayoutItemManager LayoutItemMgr { get; set; }

        public NodeObjectItemManager NodeItemMgr { get; set; }


        public RelayCommand OpenSettingsCommand => _openSettingsCommand ??= new RelayCommand(p => OpenSettings(),
            p => CanOpenSettings());

        public bool SkipValidation
        {
            get => _skipValidation;
            set
            {
                if (value == _skipValidation)
                {
                    return;
                }

                _skipValidation = value;
                if (Credential != null)
                {
                    Credential.IgnoreValidation = value;
                }

                OnPropertyChanged();
            }
        }

        public string StatusMessage
        {
            get => _statusMessage;
            set
            {
                //if (_statusMessage != value)
                {
                    _statusMessage = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(CanChangeLoginAndConnect));
                    OnPropertyChanged(nameof(CanConnect));
                }
            }
        }

        public UserItemManager UserItemMgr { get; set; }
        public LoginView View { get; set; }

        public bool WindowsAuth
        {
            get => _windowsAuth;
            set
            {
                if (value == _windowsAuth)
                {
                    return;
                }

                _windowsAuth = value;
                if (_windowsAuth)
                {
                    try
                    {
                        PreviousUsername = Credential.UserName;
                        //PreviousPassword = Credential.Password;
                        Credential.IsWindowsLogin = true;
                        var principal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
                        Credential.WindowsId =
                            ((WindowsIdentity)principal.Identity)?.User?.Value;
                        if (principal.Identity != null)
                        {
                            Credential.UserName = principal?.Identity.Name;
                        }

                        Credential.Password = string.Empty;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    Credential.IsWindowsLogin = false;
                    if (!string.IsNullOrEmpty(PreviousUsername))
                    {
                        Credential.UserName = PreviousUsername;
                        //Credential.Password = PreviousPassword;
                        //PreviousPassword = null;
                        PreviousUsername = null;
                    }
                }

                OnPropertyChanged();
                OnPropertyChanged(nameof(CanChangeLoginAndConnect));
                OnPropertyChanged(nameof(CanConnect));
                Trace.WriteLine($"LoginViewModel 245 InvalidateRequerySuggested");
                CommandManager.InvalidateRequerySuggested();
            }
        }

        private string PreviousPassword { get; set; }

        private string PreviousUsername { get; set; }

        public string WarningMessage
        {
            get => _warningMessage;
            set
            {
                if (value == _warningMessage) return;
                _warningMessage = value;
                OnPropertyChanged();
            }
        }


        public LoginViewModel(CacheUpdateService cacheUpdateService = null, UserItemManager userManager = null,
            DirectModeManager directModeManager = null,
            LayoutItemManager layoutMgr = null, CameraItemModelManager camaModelManager = null,
            NodeObjectItemManager nodeManager = null, ILog log = null)
        {
            _log = log;
            CacheUpdateService = cacheUpdateService;
            CameraItemMgr = camaModelManager;
            LayoutItemMgr = layoutMgr;
            UserItemMgr = userManager;
            NodeItemMgr = nodeManager;
            DirectModeMgr = directModeManager;
            AdDomain = AdSsoClient.MyDomain();
            //_log?.Info($"{ADDomain} Domain !!!");
            Credential.Validation += CredentialOnValidation;


            if (Credential.RememberMe && Credential.PasswordLength > 0)
            {
                try
                {
                    Credential.BearerToken = SettingsManager.Settings.Login.Credentials.BearerToken;
                    Credential.RefreshToken = SettingsManager.Settings.Login.Credentials.RefreshToken;
                    if (!string.IsNullOrEmpty(Credential.BearerToken))
                    {
                        Credential.Password = string.Concat(Enumerable.Repeat("*", Credential.PasswordLength));
                        Credential.IsBearerLogin = true;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            ConnectionError += UserServiceOnConnectionError;
            AlertManager = new RadDesktopAlertManager();
            TelemetryControlViewModel.TelemetryAlert += TelemetryAlert;
            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;

            if (CommandLineManager.StartupArgs != null)
            {
                var args = CommandLineManager.StartupArgs;
                if (args.UseWindowsLogin)
                {
                    WindowsAuth = true;
                    // Credential.IsWindowsLogin = true;
                }

                if (args.AutoLogin)
                {
                    var starred = Credential.Password;
                    Credential = SettingsManager.Settings.Login.Credentials.ConvertTo<UserLoginItemModel>();
                    Credential.Password = starred;
                    var server = !string.IsNullOrEmpty(args.ServerAddress)
                        ? args.ServerAddress
                        : SettingsManager.Settings.Login.LoginServers.FirstOrDefault();

                    if (!string.IsNullOrEmpty(server))
                    {
                        // WispFramework.Extensions.Common.Extensions.IsNotEmpty(Credential.RefreshToken)
                        //Credential.BearerToken = args.BearerToken;
                        //Credential.RefreshToken = args.RefreshToken;
                        Credential.IsBearerLogin = true;
                        Credential.IgnoreValidation = true;
                        Credential.Domain = server;

                        Connect(args.OfflineLogin ? this : null);
                    }
                }
            }

            Credential?.Initialize();

            //this returns null...?
            //_window = ((UIElement) View)?.GetParentWindow<LoginWindow>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private async void TelemetryAlert(TelemetryAlert alert)
        {
            try
            {
                await Task.Run(async () =>
                {
                    //Check if Alert Type has occured within 5 minutes
                    if (alert != null)
                    {
                        //Check if Usage had previously been high
                        if (IsHealthUsageHigh && alert.HighestPercentage < 50)
                        {
                            HealthAlertThresholdTicks = 0;
                            LastHealthAlertTime = DateTime.Now.AddSeconds(20);
                            IsHealthUsageHigh = false;
                        }

                        if (!IsHealthUsageHigh && alert.HighestPercentage > 70 && LastHealthAlertTime < DateTime.Now)
                        {
                            HealthAlertThresholdTicks++;
                            if (HealthAlertThresholdTicks >= 25)
                            {
                                LastHealthAlertTime = DateTime.Now.AddMinutes(1);
                                IsHealthUsageHigh = true;
                                _ = View?.TryUiAsync(() =>
                                {
                                    var alertItem = new HealthMonitoringAlertItemModel
                                    {
                                        Title = "Health Monitoring",
                                        Glyph = View?.TryFindResource("FontAwesome-Tachometer-Alt-Fast") as string,
                                        Description =
                                            $"{DateTime.Now.ToShortDateString()}\nYour computer is running at very high utilization.\nYour video performance may be affected.\n\nYou may need to reduce the number of simultaneous video streams you are watching to improve performance.",
                                        ShowDuration = 5000
                                        //CanAutoClose = false,
                                    };
                                    if (alertItem != null)
                                    {
                                        AlertManager?.ShowAlert(alertItem);
                                        ObjectResolver.Resolve<AlertNotificationControlViewModel>()?.Alerts?.Add(alertItem);
                                    }
                                }, DispatcherPriority.Background);
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
                
            }
        }

        private void CancelConnect()
        {
            try
            {
                if (!CanConnect)
                {
                    CanConnect = false;

                    LoginTokenSource?.Cancel();
                    StatusMessage = string.Empty;
                }
                else
                {
                    AlertManager.CloseAllAlerts(false);
                    GetLoginWindow()?.Close();
                }
            }
            catch (Exception e)
            {
                _log?.Error(e);
            }
        }

        public LoginWindow GetLoginWindow()
        {
            return App.GlobalUnityContainer?.Resolve<LoginWindow>() ?? View?.GetParentRadWindow<LoginWindow>();
        }

        private bool CanCancelConnect()
        {
            return _canCancelConnect;
        }

        public bool Validate()
        {
            try
            {
                Credential.TryValidate(Credential.Password, nameof(Credential.Password));
            }
            catch (ValidationException e)
            {
                StatusMessage = e.Message;
                return false;
            }

            try
            {
                Credential.TryValidate(Credential.UserName, nameof(Credential.UserName));
            }
            catch (ValidationException e)
            {
                StatusMessage = e.Message;
                return false;
            }

            try
            {
                Credential.TryValidate(Credential.Domain, nameof(Credential.Domain));
            }
            catch (ValidationException e)
            {
                StatusMessage = e.Message;
                return false;
            }

            return true;
        }

        public async Task Connect(object o = null, bool showGui = true)
        {
            await Task.Run(async () =>
            {
                if (DirectModeMgr != null)
                {
                    DirectModeMgr.IsOfflineMode = o != null;
                }

                try
                {
                    var invalidOfflineConnect = await HandleOfflineConnect(showGui);
                    if (invalidOfflineConnect)
                    {
                        return;
                    }
                }
                catch (Exception e)
                {
                }

                try
                {
                    var skipValidation = Credential.IgnoreValidation;
                    if (!skipValidation)
                    {
                        if (!WindowsAuth && !Validate())
                        {
                            CanConnect = true;
                            return;
                        }
                    }

                    try
                    {
                        var portInAddress = Credential.Domain.Split(':');
                        if (portInAddress.Length > 1)
                        {
                            if (ushort.TryParse(portInAddress[1], out var apiPort))
                            {
                                Credential.ApiPort = apiPort;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                    }

                    LoginTokenSource?.Cancel();
                    LoginTokenSource = new CancellationTokenSource();

                    CustomServiceClient cl = null;
                    CanConnect = false;
                    ServiceManager.IsSignalREnabled = SettingsManager.Settings.Login.SignalREnabled;
                    if (WindowsAuth)
                    {
                        await HandleWindowsAuthConnect();
                    }
                    else
                    {
                        cl = await HandleDefaultConnection();
                    }

                    ServiceManager svcMgr = null;
                    if (cl != null)
                    {
                        cl.OnWebException -= ClOnOnWebException;
                        var baseUrl = Credential.ResolveDomain();
                        svcMgr = new ServiceManager(cl, baseUrl);
                    }

                    if (cl == null || svcMgr == null)
                    {
                        CanConnect = true;
                        //StatusMessage = "Failed to Connect";
                        return;
                    }

                    var svcMgrInit = await InitializeServiceManager(svcMgr, exception =>
                    {
                        if (exception is WebServiceException webEx)
                        {
                            StatusMessage = $"{webEx.Message} ({webEx.StatusCode})"; //" Zone Information Error";
                        }
                        else
                        {
                            StatusMessage = $"{exception.Message}"; //" Zone Information Error";
                        }
                    });

                    if (!svcMgrInit)
                    {
                        CanConnect = true;
                        return;
                    }

                    await HandleSuccessfulConnection(showGui, cl);
                }
                catch (Exception e)
                {
                    _log?.Info(e);
                }
            });
        }

        private async Task HandleSuccessfulConnection(bool showGui, CustomServiceClient cl)
        {
            ServiceManager.OnServiceClientWebException +=
                ValueOnOnServiceClientWebException;
            SettingsManager.Settings.Login.Credentials.RefreshToken = cl.RefreshToken;
            SettingsManager.Settings.Login.Credentials.BearerToken = cl.BearerToken;
            SettingsManager.Settings.Login.Credentials.PasswordLength = Credential.Password.Length;
            SettingsManager.Settings.Login.SessionId = cl.SessionId;
            SettingsManager.SaveSettings(); //TODO remove this in the future...

            if (Credential.PasswordLength > 0)
            {
                try
                {
                    Credential.BearerToken = SettingsManager.Settings.Login.Credentials.BearerToken;
                    Credential.RefreshToken = SettingsManager.Settings.Login.Credentials.RefreshToken;
                    if (!string.IsNullOrEmpty(Credential.BearerToken))
                    {
                        var credentialPassword = Credential.Password.Length;
                        Credential.PasswordLength = credentialPassword;
                        Credential.Password = string.Concat(Enumerable.Repeat("*", credentialPassword));
                        Credential.IsBearerLogin = true;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            StatusMessage = "";
            await Task.Run(async () =>
            {
                try
                {
                    // LoginWindow.InitializeAuditLoggingEvents();
                    _log?.Info("LOADING LOGIN CACHES");
                    try
                    {
                        NodeItemMgr.SaveCache(Credential).ConfigureAwait(false);
                        LayoutItemMgr.RefreshLayouts().ConfigureAwait(false);
                        CameraItemMgr.RefreshCameras().ConfigureAwait(false);
                    }
                    catch (Exception e)
                    {
                    }

                    var userRequest = await GetManagerByZoneId().UserService.GetUser();
                    CurrentUser = userRequest?.User;
                    var effectivePermissions =
                        await GetManagerByZoneId().RolesService.GetEffectiveRole();
                    if (effectivePermissions != null)
                    {
                        EffectivePermissions = (ERoles) effectivePermissions.RoleValue;
                    }
                }
                catch (Exception e)
                {
                }
            }).ConfigureAwait(false);

            IsLoggedIn = true;
            if (showGui)
            {
                WebUtil.IgnoredBaseExceptions = new List<WebExceptionStatus>
                {
                    WebExceptionStatus.ConnectFailure
                };
                ShowMainWindow();
            }

            //_tabbedWindow.Show();
        }

        private async Task<CustomServiceClient> HandleDefaultConnection()
        {
            CustomServiceClient cl;
            SettingsManager.Settings.Login.Credentials = Credential;

            cl = new CustomServiceClient($"https://{Credential.ResolveDomain()}/authority");
            cl.OnWebException += ClOnOnWebException;
            try
            {
                cl =
                    await ConnectAsync(cl, Credential,
                        retryCount: CommandLineManager.StartupArgs.LoadRecoveryLayout ? 1 : 15);

                if (CommandLineManager.StartupArgs.LoadRecoveryLayout && cl == null &&
                    !DirectModeMgr.IsOfflineMode)
                {
                    _log?.Info("CONNECT WAS NULL!");
                    DirectModeMgr.IsOfflineMode = true;
                    await Connect(this);
                }
            }
            catch (Exception e)
            {
            }

            return cl;
        }

        private async Task HandleWindowsAuthConnect()
        {
            CustomServiceClient cl = null;
            try
            {
                _log?.Info($"Using Domain Authentication... {AdDomain}");

                var adLogin =
                    await new AdSsoClient($"{Credential.DomainWithoutPort}:3500").AuthenticateViaAdSso();

                if (adLogin != null && !string.IsNullOrEmpty(adLogin.BearerToken))
                {
                    cl = new CustomServiceClient(
                        $"https://{Credential.ResolveDomain()}/authority")
                    {
                        Timeout = TimeSpan.FromSeconds(
                            SettingsManager.Settings?.Login?.RequestTimeoutDuration ??
                            3), // TimeSpan.FromMilliseconds(3000),
                        BearerToken = adLogin.BearerToken,
                        RefreshToken = adLogin.RefreshToken
                    };
                }

                if (CommandLineManager.StartupArgs.LoadRecoveryLayout && cl == null &&
                    !DirectModeMgr.IsOfflineMode)
                {
                    _log?.Info("CONNECT WAS NULL!");
                    DirectModeMgr.IsOfflineMode = true;
                    await Connect(this);
                }

                if (adLogin != null && !adLogin.ErrorMessage.IsNullOrEmpty())
                {
                    StatusMessage = adLogin.ErrorMessage;
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
                StatusMessage = $"{e?.Message}";
                cl = null;
                if (CommandLineManager.StartupArgs.LoadRecoveryLayout && cl == null &&
                    !DirectModeMgr.IsOfflineMode)
                {
                    _log?.Info("CONNECT WAS NULL!");
                    DirectModeMgr.IsOfflineMode = true;
                    await Connect(this);
                }
            }
        }

        private async Task<bool> HandleOfflineConnect(bool showGui)
        {
            if (DirectModeMgr != null && DirectModeMgr.IsOfflineMode)
            {
                if (!DirectModeMgr.IsNodeCacheLoaded)
                {
                    try
                    {
                        var cacheExists = await NodeItemMgr.LoadCacheFile(Credential);
                        if (!cacheExists)
                        {
                            StatusMessage = "Missing or Invalid Offline Cache Credential.";
                            CanConnect = true;
                            return true;
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }

                if (!DirectModeMgr.IsCameraCacheLoaded)
                {
                    try
                    {
                        var cacheExists = await CameraItemMgr.LoadCacheFile(Credential);
                    }
                    catch (Exception e)
                    {
                    }
                }

                if (!DirectModeMgr.IsLayoutCacheLoaded)
                {
                    try
                    {
                        var cacheExists = await LayoutItemMgr.LoadCacheFile(Credential);
                    }
                    catch (Exception e)
                    {
                    }
                }

                if (!DirectModeMgr.IsUserCacheLoaded)
                {
                    try
                    {
                        var cacheExists = await UserItemMgr.LoadCacheFile(Credential);
                    }
                    catch (Exception e)
                    {
                    }
                }

                IsLoggedIn = true;
                if (showGui)
                {
                    WebUtil.IgnoredBaseExceptions = new List<WebExceptionStatus>
                    {
                    };
                    ShowMainWindow();
                }

                return true;
            }

            return false;
        }

        private void ShowMainWindow()
        {
            Application.Current.Dispatcher.InvokeAsync(() =>
            {
                GetLoginWindow().Visibility = Visibility.Hidden;


                _tabbedWindow = TEST_WINDOW_ENABLED
                    ? (RadTabbedWindow) WindowUtil.ShowWindow<TestWindow>()
                    : WindowUtil.ShowWindow<Window.TabbedWindow>();

                try
                {
                    var domainExists = SettingsManager.Settings.Login.LoginServers.Contains(Credential.Domain);
                    if (!domainExists)
                    {
                        Credential.LoginServers.Insert(0, Credential.Domain);
                        SettingsManager.Settings.Login.LoginServers.Insert(0, Credential.Domain);
                    }
                    else
                    {
                        if (Credential.LoginServers.IndexOf(Credential.Domain) != 0)
                        {
                            Credential.LoginServers.Move(Credential.LoginServers.IndexOf(Credential.Domain), 0);
                            SettingsManager.Settings.Login.LoginServers.Move(SettingsManager.Settings.Login.LoginServers.IndexOf(Credential.Domain), 0);
                        }
                    }

                    SettingsManager.Settings.Login.LoginServers = new ObservableCollection<string>(SettingsManager.Settings.Login.LoginServers.Take(5));
                }
                catch (Exception)
                {
                }


                CanConnect = true;
                _tabbedWindow.Closed += (o, args) =>
                {
                    Application.Current?.Dispatcher?.InvokeAsync(() =>
                    {
                        if (GetLoginWindow() != null)
                        {
                            GetLoginWindow().Visibility = Visibility.Visible;
                            GetLoginWindow().BringIntoView();
                            GetLoginWindow().BringToFront();
                            GetLoginWindow().Show();
                        }
                    });
                };
            });
            //_tabbedWindow = new TabbedWindow();
        }

        public async Task<AuthenticateViaAdSsoResponse> AdLogin(string endpointUri, CancellationTokenSource cts)
        {
            var myBinding = new NetTcpBinding
            {
                Security =
                {
                    Mode = SecurityMode.TransportWithMessageCredential,
                    Transport = {ClientCredentialType = TcpClientCredentialType.Windows}
                }
            };

            EndpointAddress myEndpointAddress = null;
            var endpointIdentity = "vmssso.edge360.com";
            var endpoint = $"net.tcp://{endpointUri}:3500/adsso";
            if (endpointIdentity != null)
            {
                myEndpointAddress = new EndpointAddress(new Uri(endpoint),
                    new DnsEndpointIdentity(endpointIdentity));
            }
            else
            {
                myEndpointAddress = new EndpointAddress(new Uri(endpoint));
            }


            var client = new AdSsoServiceClient(myBinding, myEndpointAddress);

            if (cts?.Token != null)
            {
                cts.Token.Register(() =>
                {
                    try
                    {
                        client?.Abort();
                        client?.Close();
                    }
                    catch (Exception e)
                    {
                    }
                });
            }

            var thumb = "D77B7F0FBA19870FD1C293F77838D9036B2D2644";
            if (!string.IsNullOrWhiteSpace(thumb))
            {
                if (client?.ClientCredentials?.ServiceCertificate == null)
                {
                    throw new Exception("Client Null Exception");
                }

                client.ClientCredentials.ServiceCertificate.SslCertificateAuthentication =
                    new X509ServiceCertificateAuthentication
                    {
                        CertificateValidationMode = X509CertificateValidationMode.Custom,
                        CustomCertificateValidator = new MyX509CertificateValidator(thumb)
                    };
            }

            return await client.AuthenticateViaAdSsoAsync();
        }


        private void OpenSettings()
        {
            WindowUtil.ShowWindow<SettingsWindow>();
        }

        private bool CanOpenSettings()
        {
            return true;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void ClOnOnWebException(object o, WebServiceException exception)
        {
            Console.WriteLine($"{o} {exception}");
            StatusMessage = $"{exception.Message} ({exception.StatusCode})";
        }

        private async void HubManagerOnSignalRClientEvent(ESignalRClientEvent @event, Guid zone, string data)
        {
            try
            {
                await Task.Run(async () =>
                {
                    if (@event.Equals(ESignalRClientEvent.VideoExportJobEdited))
                    {
                        if (ConnectedZoneId != zone) return;

                        var descriptor = JsonSerializer.DeserializeFromString<VideoExportJobDescriptor>(data); //
                        if (descriptor != null)
                        {
                            if (descriptor.CreatedById == CurrentUser.UserId)
                            {
                                //Guid? guid = descriptor.RecordingProfileIds.FirstOrDefault();
                                //var cameraName = "";
                                //var profileName = "";
                                var resolution = "";
                                try
                                {
                                    //var recordingProfile = await ServiceManagers[descriptor.ZoneOwnerId]
                                    //    .RecordingProfileService.GetRecordingProfile(guid);
                                    //var cameraProfile = await ServiceManagers[descriptor.ZoneOwnerId].CameraService
                                    //    .GetCamera(recordingProfile.RecordingProfile.CameraId);
                                    //  cameraName = cameraProfile.Camera.Label;
                                    //  profileName = recordingProfile.RecordingProfile.Label;
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }

                                if (descriptor.ExportHeight != 0 && descriptor.ExportWidth != 0)
                                {
                                    resolution = $"{descriptor.ExportWidth}x{descriptor.ExportHeight}";
                                }

                                await Application.Current.Dispatcher.InvokeAsync(() =>
                                {
                                    try
                                    {
                                        if (!Application.Current.Dispatcher.CheckAccess()) return;


                                        var videoExportAlertItemModel = new VideoExportAlertItemModel
                                        {
                                            Title = "Video Export Job Complete",

                                            Descriptor = descriptor,
                                            Description =
                                                $"{descriptor.CameraName} ({descriptor.ProfileName})\n{resolution}\n{descriptor.Description}\n{descriptor.ExportDateTimeEnd - descriptor.ExportDateTimeStart} Duration"
                                        };
                                        videoExportAlertItemModel.Command = new RelayCommand(o =>
                                        {
                                            Application.Current.Dispatcher.InvokeAsync(async() =>
                                            {
                                                try
                                                {
                                                    var tabbedWindow = ObjectResolver.ResolveAll<RadTabbedWindow>()
                                                        .FirstOrDefault();

                                                    if (tabbedWindow != null)
                                                    {
                                                        var tab = await tabbedWindow.FindTab<ExportArchiveTab>() ?? await
                                                            tabbedWindow.OpenTab<ExportArchiveTab>();
                                                        if (tab != null)
                                                        {
                                                            tab.FocusTab(tab);
                                                        }
                                                    }
                                                }
                                                catch (Exception e)
                                                {
                                                }
                                            });
                                        });

                                        AlertManager.ShowAlert(videoExportAlertItemModel);
                                    }
                                    catch (Exception e)
                                    {
                                    }
                                });
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
            }
        }

        private void ValueOnOnServiceClientWebException(CustomServiceClient arg1, object arg2, WebServiceException arg3)
        {
            _networkLog.Info($"Obj:{arg2} StatusCode:{arg3.StatusCode} ErrorMessage:{arg3.ErrorMessage}");
        }

        public void ViewOnLoaded(object sender, RoutedEventArgs e)
        {
            Validate();
            if (Credential.Domain.IsEmpty())
            {
                var viewAddressComboBox = View?.AddressComboBox?.ChildrenOfType<RadComboBox>()?.FirstOrDefault();
                ;
                if (viewAddressComboBox != null)
                {
                    viewAddressComboBox.Focus();
                    FocusManager.SetFocusedElement(FocusManager.GetFocusScope(viewAddressComboBox),
                        viewAddressComboBox);
                }
            }
            else if (Credential.UserName.IsEmpty())
            {
                var dataFormDataField = View?.UsernameField?.ChildrenOfType<TextBox>()?.FirstOrDefault();
                ;
                if (dataFormDataField != null)
                {
                    dataFormDataField.Focus();
                    FocusManager.SetFocusedElement(FocusManager.GetFocusScope(dataFormDataField), dataFormDataField);
                }
            }
            else if (Credential.Password.IsEmpty())
            {
                var dataFormPasswordField = View?.PasswordField?.ChildrenOfType<TextBox>()?.FirstOrDefault();
                ;
                if (dataFormPasswordField != null)
                {
                    dataFormPasswordField.Focus();
                    FocusManager.SetFocusedElement(FocusManager.GetFocusScope(dataFormPasswordField),
                        dataFormPasswordField);
                }
            }

            //  View?.RadDataForm?.ValidateItem();
        }

        private void UserServiceOnConnectionError(string obj)
        {
            StatusMessage = obj;
        }

        private void CredentialOnValidation(object arg1, string errorMessage)
        {
            var validationError = "";
            try
            {
                //View?.RadDataForm.ValidateItem();
                if (errorMessage.IsEmpty())
                {
                    Validate();
                }

                //validationError = View?.RadDataForm?.ValidationSummary?.Errors?.FirstOrDefault()?.ErrorContent;
                Console.WriteLine(validationError);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            StatusMessage = string.IsNullOrEmpty(errorMessage)
                ? validationError
                : $"{errorMessage}";
        }
    }
}