﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.View.Control.VideoControl.Timeline.Event;
using Edge360.Platform.VMS.Common.Interfaces.Video;

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl.Timeline.DataProvider
{
    /// <summary>
    ///     Timeline data provider using the sdk
    /// </summary>
    public class EdgeTimelineDataProvider : ITimelineDataProvider, INotifyPropertyChanged
    {
        private EdgeVideoControl _dataObject;
        private bool _isTickerHidden = true;

        public EdgeVideoControl DataObject
        {
            get => _dataObject;
            set
            {
                if (Equals(value, _dataObject))
                {
                    return;
                }

                _dataObject = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(Thumbnails));
            }
        }

        public EdgeTimelineDataProvider(EdgeVideoControl dataObj)
        {
            DataObject = dataObj;

            Sequences = new List<ITimelineEvent>();
            Motions = new List<ITimelineEvent>();
            Bookmarks = new List<ITimelineEvent>();
            Thumbnails = new List<ITimelineEvent>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsTickerHidden
        {
            get => _isTickerHidden;
            set
            {
                if (value == _isTickerHidden)
                {
                    return;
                }

                _isTickerHidden = value;
                OnPropertyChanged();
            }
        }

        public event Action<DateTime, DateTime> TimelineRangeChanged;

        /// <summary>
        ///     Set the current timeline range
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        public void SetTimelineRange(DateTime begin, DateTime end)
        {
            if (BeginTime != begin || EndTime != end)
            {
                TimelineRangeChanged?.Invoke(begin, end);
            }

            BeginTime = begin;
            EndTime = end;
            QuerySequences();
            QueryBookmarks();
            QueryThumbnails();
        }

        /// <summary>
        ///     Gets the current begin time
        /// </summary>
        public DateTime BeginTime { get; private set; }


        /// <summary>
        ///     Gets the current end time
        /// </summary>
        public DateTime EndTime { get; private set; }

        /// <summary>
        ///     Gets the received bookmarks
        /// </summary>
        public List<ITimelineEvent> Bookmarks { get; set; } = new List<ITimelineEvent>();

        public Guid Camera { get; set; }

        /// <summary>
        ///     Gets the received thumbnails
        /// </summary>
        public List<ITimelineEvent> Thumbnails { get; } = new List<ITimelineEvent>();

        /// <summary>
        ///     Gets the received motions
        /// </summary>
        public List<ITimelineEvent> Motions { get; }  = new List<ITimelineEvent>();

        /// <summary>
        ///     Gets the received sequences
        /// </summary>
        public List<ITimelineEvent> Sequences { get; }  = new List<ITimelineEvent>();

        /// <summary>
        ///     Gets the camera time zone
        /// </summary>
        public TimeZoneInfo TimeZone => TimeZoneInfo.Utc;

        public TimeZoneInfo FutureTimeZone => TimeZoneInfo.Local;

        /// <summary>
        ///     Event fired when thumbnails are received
        /// </summary>
        public event EventHandler ThumbnailsReceived;

        /// <summary>
        ///     Event fired when bookmarks are received
        /// </summary>
        public event EventHandler BookmarksReceived;

        /// <summary>
        ///     Event fired when motions are received
        /// </summary>
        public event EventHandler MotionsReceived;

        /// <summary>
        ///     Event fired when sequences are received
        /// </summary>
        public event EventHandler SequencesReceived;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        /// <summary>
        ///     Build the list of sequence events from the data table received.
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        private List<ITimelineEvent> BuildSequenceEvents(DataTable dataTable)
        {
            var sequences = new List<ITimelineEvent>();
            foreach (DataRow row in dataTable.Rows)
            {
                sequences.Add(new Sequence((DateTime) row["StartTime"], (DateTime) row["EndTime"]));
            }

            return sequences;
        }


        /// <summary>
        ///     Launch motion query
        /// </summary>
        public void QueryThumbnails()
        {
        }

        /// <summary>
        ///     Launch motion query
        /// </summary>
        public void QueryBookmarks()
        {
        }


        ///// <summary>
        /////     Launch motion query
        ///// </summary>
        private void QueryMotions()
        {
        }

        ///// <summary>
        /////     Launch sequence query
        ///// </summary>
        private void QuerySequences()
        {
        }
    }
}