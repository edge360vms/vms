﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using Edge360.Platform.VMS.Common.Interfaces.Video;

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl.Timeline.Layers
{
    /// <summary>
    ///     Canvas control that displays timeline events
    /// </summary>
    public class EventsLayer : VisualsLayer
    {
        private Grid _parentGrid;

        private void InsertEvent(ITimelineEvent timelineEvent, DateTime beginTime, DateTime endTime)
        {
            // Determine the UI constraints
            var constraint = GetConstraint(timelineEvent, InternalWidth, InternalHeight, beginTime, endTime);

            // Ensure the visual's constraint would appear inside the timeline's current view
            var maxWidth = InternalWidth;
            if (constraint.Left < 0 && constraint.Right < 0 || constraint.Left > maxWidth)
            {
                // Exit!
                return;
            }

            // Determine the X canvas position
            var posX = constraint.X;

            // Extreme negative canvas position can affect the visual appearance
            if (posX < 0 && constraint.Width + posX >= 0)
            {
                constraint.Width += posX;
                constraint.X = 0;
                posX = 0;
            }

            // Retrieve a visual object representing the event
            var visual = timelineEvent.GetVisual(constraint);
            if (visual == null)
            {
                return;
            }


            var width = constraint.Width;
            posX -= width;

            // Determine the Y canvas position
            var posY = 0.0;

            // Set the canvas positions
            visual.SetValue(LeftProperty, posX);
            visual.SetValue(TopProperty, posY);

            if (visual is Polygon poly)
            {
                //  _log?.Info("Polygon found!");
                poly.Margin = new Thickness(Math.Max(0, posX - 5), -3, 0, 0);


                poly.Tag = timelineEvent;
                _parentGrid = Parent as Grid;
                if (_parentGrid != null && !_parentGrid.Children.Contains(poly))
                {
                    _parentGrid.Children.Add(poly);
                }

                return;
            }

            AddChild(visual);
        }

        public void DrawFrEvents(DateTime beginTime, DateTime endTime, List<ITimelineEvent> events)
        {
            if (events == null)
            {
                return;
            }

            if (Parent is Grid grid)
            {
                foreach (var p in grid.Children.OfType<Polygon>().ToList()
                ) //.Where(o => !events.Contains(o.Tag)).ToList())
                {
                    //  _log?.Info("Removing poly..");
                    _parentGrid.Children.Remove(p);
                }
            }

            Clear();
            foreach (var sequence in events)
            {
                InsertEvent(sequence, beginTime, endTime);
            }
        }


        public void Draw(DateTime beginTime, DateTime endTime, List<ITimelineEvent> events)
        {
            if (events == null)
            {
                return;
            }

            if (_parentGrid != null)
            {
                foreach (var p in _parentGrid.Children.OfType<Polygon>().ToList()
                ) //.Where(o => !events.Contains(o.Tag)).ToList())
                {
                    //  _log?.Info("Removing poly..");
                    _parentGrid.Children.Remove(p);
                }
            }

            Clear();
            foreach (var sequence in events)
            {
                InsertEvent(sequence, beginTime, endTime);
            }
        }
    }
}