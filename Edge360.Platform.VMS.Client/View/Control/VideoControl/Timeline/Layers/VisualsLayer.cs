﻿using System;
using System.Windows;
using System.Windows.Media;
using Edge360.Platform.VMS.Common.Interfaces.Video;

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl.Timeline.Layers
{
    /// <summary>
    ///     Canvas control that displays Visual objects
    /// </summary>
    public class VisualsLayer : System.Windows.Controls.Canvas
    {
        private readonly VisualCollection _colVisuals;

        public VisualsLayer()
        {
            _colVisuals = new VisualCollection(this);
        }

        protected double InternalHeight
        {
            get
            {
                var nHeight = ActualHeight;
                if (nHeight > 0)
                {
                    return nHeight;
                }

                return 0;
            }
        }

        protected double InternalWidth
        {
            get
            {
                var nWidth = ActualWidth;
                if (nWidth > 0)
                {
                    return nWidth;
                }

                return 0;
            }
        }

        protected override int VisualChildrenCount => _colVisuals.Count;

        public void AddChild(Visual visual)
        {
            _colVisuals.Add(visual);
        }

        public void Clear()
        {
            //   this.WpfUiThread(() =>
            //   {
            _colVisuals.Clear();
            Children.Clear();
            //   });
        }

        protected Rect GetConstraint(ITimelineEvent timelineEvent, double maxWidth, double maxHeight,
            DateTime beginTime, DateTime endTime)
        {
            var posX = GetXFromPos(timelineEvent.EventTime, beginTime, endTime, maxWidth);

            var constraint = new Rect(posX, 0, 3, maxHeight);
            var x = GetXFromPos(timelineEvent.EventTime + timelineEvent.Duration, beginTime, endTime, maxWidth);
            constraint.Width = Math.Max(0, x - constraint.Left);

            return constraint;
        }

        protected override Visual GetVisualChild(int index)
        {
            return _colVisuals[index];
        }

        protected double GetXFromPos(DateTime? dt, DateTime beginTime, DateTime endTime, double width)
        {
            if (endTime == beginTime)
            {
                return 0;
            }

            var tsPos = dt - beginTime;
            var tsTotal = endTime - beginTime;

            if (tsPos != null)
            {
                return tsPos.Value.TotalMilliseconds * width / tsTotal.TotalMilliseconds;
            }

            return default;
        }
    }
}