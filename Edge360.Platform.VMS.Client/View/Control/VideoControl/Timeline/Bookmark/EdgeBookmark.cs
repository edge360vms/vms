﻿#region

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Edge360.Platform.VMS.Common.Enums.Video;
using Edge360.Platform.VMS.Common.Interfaces.Video;
using Newtonsoft.Json;

#endregion

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl.Timeline.Bookmark
{
    [Serializable]
    public class CatapultBookmark : ITimelineEvent
    {
        private Polygon _poly;


        public Visual GetVisual(Rect constraint)
        {
            // if (_poly == null)
            {
                CreatePoly();
            }
            return _poly;
        }

        public void Initialize()
        {
            CreatePoly();
        }

        private Color GetStroke()
        {
            switch (AlertLevel)
            {
                case 1:
                    return Colors.LightGreen;
                case 2:
                    return Colors.LightCoral;
                case 0:
                default:
                    return Colors.Gold;
            }
        }

        private Color GetFill()
        {
            switch (AlertLevel)
            {
                case 1:
                    return Colors.ForestGreen;
                case 2:
                    return Colors.Red;
                case 0:
                default:
                    return Colors.Orange;
            }
        }

        private void CreatePoly()
        {
            _poly = new Polygon
            {
                Points = new PointCollection(new[]
                {
                    new Point(0, 0), new Point(0, 10), new Point(5, 5), new Point(10, 10), new Point(10, 0),
                    new Point(0, 0)
                }),
                Stroke = new SolidColorBrush(GetStroke()),
                Fill = new SolidColorBrush(GetFill()),
                StrokeThickness = 1,
                MinHeight = 10,
                MinWidth = 10,
                MaxHeight = 10,
                Tag = this
            };

            if (!string.IsNullOrEmpty(Description))
            {
                //  _log?.Info($"Setting description... {Description}");
                _poly.ToolTip = new TextBlock {Text = Description};
            }
        }

        #region Designer generated code

        public int AlertLevel { get; set; }

        //public DateTime BookmarkTime { get; set; }
        public int CameraId { get; set; }
        public string Description { get; set; }

        public ETimelineEventType EventType { get; set; }
        [JsonIgnore] public TimeSpan Duration { get; set; }
        public string Name { get; set; }
        public Guid ObjectId { get; set; }
        public TimeZoneInfo TimeZone { get; set; }

        public DateTime EventTime { get; set; }

        #endregion
    }
}