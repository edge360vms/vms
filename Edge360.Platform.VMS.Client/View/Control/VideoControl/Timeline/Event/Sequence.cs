﻿using System;
using System.Windows;
using System.Windows.Media;
using Edge360.Platform.VMS.Common.Enums.Video;
using Edge360.Platform.VMS.Common.Interfaces.Video;

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl.Timeline.Event
{
    public class Sequence : ITimelineEvent
    {
        public Sequence(DateTime beginTime, DateTime endTime)
            : this(beginTime, endTime - beginTime)
        {
        }

        public Sequence(DateTime eventTime, TimeSpan duration)
        {
            EventTime = eventTime;
            Duration = duration;
        }

        public Visual GetVisual(Rect constraint)
        {
            var visual = new DrawingVisual();
            var dc = visual.RenderOpen();
            dc.DrawRectangle(new SolidColorBrush(Color.FromArgb(75, 189, 189, 189)), null, constraint);
            dc.Close();

            return visual;
        }

        public string Description { get; set; }

        public ETimelineEventType EventType { get; set; }
        public TimeSpan Duration { get; set; }
        public string Name { get; set; }
        public Guid ObjectId { get; set; }
        public TimeZoneInfo TimeZone { get; set; }

        public DateTime EventTime { get; set; }
    }
}