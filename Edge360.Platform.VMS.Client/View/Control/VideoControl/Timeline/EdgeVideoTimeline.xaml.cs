﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Common.Enums.Video;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Interfaces.Video;
using Edge360.Platform.VMS.Common.Util;
using log4net;
using ITimelineEventModel = Edge360.Platform.VMS.Common.Interfaces.Video.ITimelineEvent;

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl.Timeline
{
    /// <summary>
    ///     Timeline user control
    /// </summary>
    public partial class EdgeVideoTimeline : IDisposable, INotifyPropertyChanged
    {
        /// <summary>
        ///     Used to throttle cursor time changed
        /// </summary>
        private const int CursorTimeChangedRefreshRate = 500;

        /// <summary>
        ///     Maximum timeline range in seconds
        /// </summary>
        private const int MaximumRange = 86400;

        /// <summary>
        ///     Minimum timeline range in seconds
        /// </summary>
        private const int MinimumRange = 30;

        /// <summary>
        ///     Timeline auto-scroll threshold
        /// </summary>
        private const double ScrollThreshold = 0.05;

        private readonly int _timerTickInterval = 100;

        private bool _cursorDragged;

        private DateTime _cursorTime = DateTime.MinValue;

        private bool _futureIsUtc = true;
        private bool _isTickerVisible;
        private RelayCommand _jumpToLiveCommand;
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private RelayCommand _recenterCommand;

        private bool _snapToCursor;

        private DispatcherTimer _timelineTimer;

        private DispatcherTimer _timerCursorTimeChanged;

        public DateTime FutureEndTime = DateTime.MinValue;

        /// <summary>
        ///     Should be set to the current camera displayed.
        /// </summary>
        public Guid Camera
        {
            get => DataProvider.Camera;
            set
            {
                if (DataProvider != null)
                {
                    DataProvider.Camera = value;
                }
            }
        }

        /// <summary>
        ///     Timeline cursor time. Should be set when a frame is rendered in playback mode.
        /// </summary>
        public DateTime CursorTime
        {
            get => _cursorTime;
            set
            {
                if (value == DateTime.MinValue)
                {
                    return;
                }

                // Cannot change cursor time externally while dragging
                if (IsDragging)
                {
                    return;
                }

                // Should always be utc
                if (value.Kind != DateTimeKind.Utc)
                {
                    _cursorTime = new DateTime(value.Ticks, DateTimeKind.Utc);
                }
                else
                {
                    _cursorTime = value;
                }

                AutoScroll();

                // Re-enable snap to cursor if cursor time is within the current time range
                if (_cursorTime >= BeginTime && _cursorTime <= EndTime)
                {
                    IsSnapToCursor = true;
                }

                UpdateCursorPosition();
            }
        }

        public bool DataBeingRequested { get; set; }

        /// <summary>
        ///     Gets the timeline data provider
        /// </summary>
        public ITimelineDataProvider DataProvider { get; private set; }

        /// <summary>
        ///     Should be set to True when the video is in rewind
        /// </summary>
        public bool IsRewind { get; set; }

        public bool IsTickerVisible
        {
            get => _isTickerVisible;
            set
            {
                if (value == _isTickerVisible)
                {
                    return;
                }

                _isTickerVisible = value;
                OnPropertyChanged();
            }
        }

        // private DispatcherTimer _updateTimer;

        public RelayCommand JumpToLiveCommand =>
            _jumpToLiveCommand ??= new RelayCommand(p => JumpToLive(p));

        public EdgeVideoControl Owner { get; set; }

        public RelayCommand RecenterCommand =>
            _recenterCommand ??= new RelayCommand(p => Recenter(p));

        private DateTime BeginTime => DataProvider == null ? DateTime.MinValue : DataProvider.BeginTime;

        private DateTime EndTime => DataProvider == null ? DateTime.MinValue : DataProvider.EndTime;

        private double InternalHeight
        {
            get
            {
                var nHeight = m_gridLayers.ActualHeight;
                if (nHeight > 0)
                {
                    return nHeight;
                }

                return 0;
            }
        }

        private double InternalWidth
        {
            get
            {
                var nWidth = m_gridLayers.ActualWidth;
                if (nWidth > 0)
                {
                    return nWidth;
                }

                return 0;
            }
        }

        private bool IsDragging { get; set; }

        private bool IsPlaying => Owner?.VideoComponent != null && Owner.VideoComponent.IsPlaying;

        private bool IsSnapToCursor
        {
            get => _snapToCursor;
            set
            {
                var oldValue = _snapToCursor;
                _snapToCursor = value;
                if (_snapToCursor && oldValue != _snapToCursor)
                {
                    //_log?.Info("Snap To Cursor!");
                    AutoScroll();
                }
            }
        }

        private long LastLayerUpdate { get; set; }

        private double MillisecondsPerPixel
        {
            get
            {
                if (InternalWidth == 0)
                {
                    return 1;
                }

                var tsTotal = EndTime - BeginTime;
                return tsTotal.TotalMilliseconds / InternalWidth;
            }
        }

        public EdgeVideoTimeline()
        {
            InitializeComponent();

            BtnPanLeft.AddHandler(PreviewMouseUpEvent, new MouseButtonEventHandler(OnButtonScrollLeft), true);
            BtnPanRight.AddHandler(PreviewMouseUpEvent, new MouseButtonEventHandler(OnButtonScrollRight), true);
            BtnZoomIn.AddHandler(PreviewMouseUpEvent, new MouseButtonEventHandler(OnButtonZoomIn), true);
            BtnZoomOut.AddHandler(PreviewMouseUpEvent, new MouseButtonEventHandler(OnButtonZoomOut),
                true);


            _timelineTimer = new DispatcherTimer(TimeSpan.FromMilliseconds(_timerTickInterval),
                DispatcherPriority.Render, TimelineTimerCallback, Dispatcher.CurrentDispatcher);
        }

        public void Dispose()
        {
            UnsubscribeProvider();
            DataProvider = null;
            DataContext = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void JumpToLive(object o)
        {
            CursorTime = DateTime.Now;
            Recenter();
            UpdateCursorPosition();
        }

        private void Recenter(object o)
        {
            Recenter();
            UpdateCursorPosition();
        }

        private bool IsPlayingLive()
        {
            return Owner?.VideoComponent != null && Owner.VideoComponent.IsLivePlayback;
        }


        private bool HandleLivePlayback(System.Windows.Controls.Control activeControl)
        {
            if (!DataBeingRequested && DateTime.Now - Owner.EdgeVideoTimeline.CursorTime < TimeSpan.FromSeconds(3))
            {
                if (IsPlayingLive() || !IsPlaying)
                {
                    return false;
                }

                if (Owner?.VideoObject != null && !string.IsNullOrEmpty(Owner.VideoObject.CameraUrl))
                {
                    // _log?.Info(Settings.AxisRtspUrl);
                    if (activeControl != null)
                    {
                        Owner.EdgeVideoTimeline.CursorTime = DateTime.Now;
                    }
                }
            }

            return true;
        }

        public void UpdateTimeline(bool forced = false)
        {
            Owner?.TryUiAsync(() =>
            {
                var activeControl = Owner;

                if (!forced && activeControl != null && IsPlaying)
                {
                    if (IsPlayingLive())
                    {
                        Owner.EdgeVideoTimeline.UpdateTimerCallback(DateTime.Now);
                    }
                    else //just add our timer time
                    {
                        if (Owner.VideoComponent.PlaybackState == EPlaybackState.Reverse)
                        {
                            Owner.EdgeVideoTimeline.UpdateTimerCallback(
                                Owner.EdgeVideoTimeline.CursorTime.Subtract(
                                    TimeSpan.FromMilliseconds(
                                        _timerTickInterval * (int) Owner.VideoComponent.PlaybackSpeed * 1)));
                        }
                        else
                        {
                            Owner.EdgeVideoTimeline.UpdateTimerCallback(
                                Owner.EdgeVideoTimeline.CursorTime.AddMilliseconds(
                                    _timerTickInterval * (int) Owner.VideoComponent.PlaybackSpeed * 1));
                        }
                    }
                }

                // timeline was changed by JumpToDate or cursor time being moved
                if (forced)
                {
                    if (!HandleForcedTimeline())
                    {
                        return;
                    }
                }

                if (!HandleLivePlayback(activeControl))
                {
                }
            });
        }

        private bool HandleForcedTimeline()
        {
            return false;
        }

        public void UpdateTimerCallback(DateTime videoDateTime)
        {
            if (videoDateTime == DateTime.MinValue)
            {
                return;
            }

            // TODO: Optimize the frequency of render updates...
            CursorTime = videoDateTime;

            if (Environment.TickCount - LastLayerUpdate < 1000)
            {
                return;
            }

            LastLayerUpdate = Environment.TickCount;

            var _ = this.TryUiAsync(() =>
            {
                DrawLayers();
                AutoScroll();
                UpdateCursorPosition();
            });
        }

        /// <summary>
        ///     Indicates that the user released the cursor.
        ///     Good moment to resume paused video.
        /// </summary>
        public event EventHandler CursorDragCompleted;

        /// <summary>
        ///     Indicates that the user started a drag.
        ///     Good moment to pause video.
        /// </summary>
        public event EventHandler CursorDragStarted;

        /// <summary>
        ///     Indicates that the user changed the cursor position.
        ///     Good moment to seek video.
        /// </summary>
        public event EventHandler CursorTimeChanged;

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            DrawLayers();
        }

        private void CenterAtTime()
        {
        }

        private void ZoomInAtTime(DateTime time)
        {
            IsSnapToCursor = false;
            BtnZoomIn.ReleaseMouseCapture();
            var origin = GetXFromPos(time) / InternalWidth;
            var tsLength = EndTime - BeginTime;
            if (tsLength.TotalSeconds <= MinimumRange)
            {
                return;
            }

            var nNewLength = (int) tsLength.TotalSeconds / 2;
            if (nNewLength < MinimumRange)
            {
                nNewLength = MinimumRange;
            }

            var tsNewLength = new TimeSpan(0, 0, nNewLength);
            var centerTime = BeginTime + TimeSpan.FromTicks((long) (tsLength.Ticks * origin));
            var offset = TimeSpan.FromTicks((long) (tsNewLength.Ticks * origin));
            SetTimelineRange(centerTime - offset, centerTime + tsNewLength - offset);
        }

        private void ZoomOutAtTime(DateTime time)
        {
            BtnZoomOut.ReleaseMouseCapture();
            var origin = 0.5;
            var tsLength = EndTime - BeginTime;
            if (tsLength.TotalSeconds >= MaximumRange)
            {
                return;
            }

            var nNewLength = (int) tsLength.TotalSeconds * 2;
            if (nNewLength > MaximumRange)
            {
                nNewLength = MaximumRange;
            }

            var tsNewLength = new TimeSpan(0, 0, nNewLength);
            var centerTime = BeginTime + TimeSpan.FromTicks((long) (tsLength.Ticks * origin));
            var offset = TimeSpan.FromTicks((long) (tsNewLength.Ticks * origin));
            SetTimelineRange(centerTime - offset, centerTime + tsNewLength - offset);
        }

        /// <summary>
        ///     Initialize the timeline with your prefered data provider.
        /// </summary>
        /// <param name="dataProvider"></param>
        public void Initialize(ITimelineDataProvider dataProvider)
        {
            DataProvider = dataProvider;
            SubscribeProvider();
        }

        /// <summary>
        ///     Set the current timeline range.
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        public void SetTimelineRange(DateTime begin, DateTime end)
        {
            if (DataProvider != null)
            {
                DataProvider.IsTickerHidden = !(CursorTime < end && CursorTime > begin);
                //  _log?.Info($"SetTimeline {begin} {end} {Environment.StackTrace}");
                DataProvider?.SetTimelineRange(begin, end);
            }

            DrawLayers();
        }

        public void AutoScroll()
        {
            // Protect against invalid values
            if (BeginTime == DateTime.MinValue || EndTime == DateTime.MinValue || CursorTime == DateTime.MinValue ||
                EndTime <= BeginTime)
            {
                return;
            }

            DataProvider.IsTickerHidden = !(CursorTime < EndTime && CursorTime > BeginTime);

            // Do not auto-scroll if snap is disabled
            if (!IsSnapToCursor)
            {
                return;
            }

            Recenter();
        }

        private void Recenter()
        {
            var bRefreshTimeline = false;
            var tsTotal = EndTime - BeginTime;

            // Calculate the threshold in seconds
            var nThreshold = (int) Math.Floor(tsTotal.TotalSeconds * ScrollThreshold);
            var nThresholdInverse = (int) Math.Ceiling(tsTotal.TotalSeconds * (1 - ScrollThreshold));

            var newBeginTime = BeginTime;
            var newEndTime = EndTime;

            if (IsRewind && (CursorTime < BeginTime + new TimeSpan(0, 0, 0, nThreshold) || CursorTime > EndTime))
            {
                newBeginTime = CursorTime - new TimeSpan(0, 0, 0, nThresholdInverse);
                newEndTime = CursorTime + new TimeSpan(0, 0, 0, nThreshold);
                bRefreshTimeline = true;
            }
            else if (!IsRewind && (CursorTime > EndTime - new TimeSpan(0, 0, 0, nThreshold) || CursorTime < BeginTime))
            {
                newBeginTime = CursorTime - new TimeSpan(0, 0, 0, nThreshold);
                newEndTime = CursorTime + new TimeSpan(0, 0, 0, nThresholdInverse);
                bRefreshTimeline = true;
            }

            if (!bRefreshTimeline)
            {
                return;
            }


            Console.WriteLine($"{IsTickerVisible}");
            // Update the range of the timeline
            SetTimelineRange(newBeginTime, newEndTime);
        }

        private void DrawFuture()
        {
            m_layerFuture.Clear();

            var visual = new DrawingVisual();
            var dc = visual.RenderOpen();


            // Determine the position of the future start
            var futurePos = (int) GetXFromPos(FutureEndTime == DateTime.MinValue
                ? DataProvider != null && DataProvider.FutureTimeZone.Equals(TimeZoneInfo.Local) ? DateTime.Now :
                DateTime.UtcNow
                : FutureEndTime);
            if (futurePos < 0)
            {
                futurePos = 0;
            }

            var futureWidth = (int) InternalWidth - futurePos;
            if (futureWidth < 0)
            {
                futureWidth = 0;
            }

            // Draw the future rectangle
            if (futureWidth > 0)
            {
                dc.DrawRectangle(new SolidColorBrush(Color.FromRgb(102, 51, 255)), null,
                    new Rect(futurePos, 0, futureWidth, InternalHeight));
            }

            dc.Close();

            m_layerFuture.AddChild(visual);
        }

        public void DrawLayers()
        {
            DrawTicks();
            DrawFuture();
            DrawSequences();
            DrawMotions();
            DrawBookmarks();
            DrawThumbnails();
            UpdateCursorPosition();
        }

        private void DrawBookmarks()
        {
            m_layerBookmarks.Draw(BeginTime, EndTime, DataProvider?.Bookmarks);
        }

        private void DrawMotions()
        {
            m_layerMotions.Draw(BeginTime, EndTime, DataProvider?.Motions);
        }

        private void DrawSequences()
        {
            m_layerSequences.Draw(BeginTime, EndTime, DataProvider?.Sequences);
        }

        private void DrawTicks()
        {
            m_layerTicks.Draw(BeginTime, EndTime, DataProvider?.TimeZone);
        }

        private DateTime GetPosFromX(double nX)
        {
            var tsTotal = EndTime - BeginTime;
            var dtPos =
                BeginTime + new TimeSpan(0, 0, 0, 0, (int) (nX * tsTotal.TotalMilliseconds / InternalWidth));
            return dtPos;
        }

        private double GetXFromPos(DateTime dt)
        {
            return GetXFromPos(dt, BeginTime, EndTime, InternalWidth);
        }

        private double GetXFromPos(DateTime dt, DateTime beginTime, DateTime endTime, double width)
        {
            if (endTime == beginTime)
            {
                return 0;
            }

            var tsPos = dt - beginTime;
            var tsTotal = endTime - beginTime;

            return tsPos.TotalMilliseconds * width / tsTotal.TotalMilliseconds;
        }

        private void SubscribeProvider()
        {
            if (DataProvider != null)
            {
                DataProvider.SequencesReceived += OnProviderSequencesReceived;
                DataProvider.MotionsReceived += OnProviderMotionsReceived;
                DataProvider.BookmarksReceived += OnProviderBookmarksReceived;
                DataProvider.ThumbnailsReceived += OnProviderThumbnailsReceived;
            }
        }

        public void DrawThumbnails()
        {
            m_layerThumbnails.Draw(BeginTime, EndTime, DataProvider?.Thumbnails);
        }

        private void UnsubscribeProvider()
        {
            if (DataProvider != null)
            {
                DataProvider.SequencesReceived -= OnProviderSequencesReceived;
                DataProvider.MotionsReceived -= OnProviderMotionsReceived;
                DataProvider.BookmarksReceived -= OnProviderBookmarksReceived;
                DataProvider.ThumbnailsReceived -= OnProviderThumbnailsReceived;
            }
        }

        public void UpdateCursorPosition()
        {
            // return;
            try
            {
                var nX = GetXFromPos(CursorTime);
                m_thumb.SetValue(System.Windows.Controls.Canvas.LeftProperty, nX - m_thumb.ActualWidth / 2);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void TimelineTimerCallback(object sender, EventArgs e)
        {
            UpdateTimeline();
        }

        private void FlatButton_MouseUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }


        private void OnClicked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void M_layerTicks_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
            if (e.Delta > 0)
            {
                var pt = e.GetPosition(m_gridLayers);
                var timeAtCursor = GetPosFromX(pt.X);
                ZoomInAtTime(timeAtCursor);
            }
            else
            {
                var pt = e.GetPosition(m_gridLayers);
                var timeAtCursor = GetPosFromX(pt.X);
                ZoomOutAtTime(timeAtCursor);
            }
        }

        public void OnButtonScrollLeft(object sender, RoutedEventArgs e)
        {
            BtnPanLeft.ReleaseMouseCapture();
            IsSnapToCursor = false;
            var range = EndTime - BeginTime;
            SetTimelineRange(BeginTime - range, BeginTime);
        }

        public void OnButtonScrollRight(object sender, RoutedEventArgs e)
        {
            BtnPanRight.ReleaseMouseCapture();
            IsSnapToCursor = false;
            var range = EndTime - BeginTime;
            SetTimelineRange(EndTime, EndTime + range);
        }

        private void OnButtonZoomIn(object sender, RoutedEventArgs e)
        {
            ZoomInAtTime(CursorTime);
        }

        private void OnButtonZoomOut(object sender, RoutedEventArgs e)
        {
            ZoomOutAtTime(CursorTime);
        }

        private void OnCanvasMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            var pt = e.GetPosition(m_gridLayers);
            if (Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                try
                {
                    var targetTime = GetPosFromX(pt.X);
                    var eventsNearCursor = new List<ITimelineEventModel>();
                    eventsNearCursor.AddRange(
                        DataProvider.Thumbnails.Where(o => o.EventTime > BeginTime && o.EventTime < EndTime));
                    eventsNearCursor.AddRange(
                        DataProvider.Bookmarks.Where(o => o.EventTime > BeginTime && o.EventTime < EndTime));

                    var closest = eventsNearCursor.OrderBy(i => Math.Abs(i.EventTime.Ticks - targetTime.Ticks))
                        .FirstOrDefault();
                    if (closest != null)
                    {
                        _cursorTime = closest.EventTime;
                    }
                }
                catch (Exception exception)
                {
                    _log?.Info(exception);
                }
            }
            else
            {
                _cursorTime = GetPosFromX(pt.X);
            }

            UpdateCursorPosition();

            CursorTimeChanged?.Invoke(this, EventArgs.Empty);
        }

        private void OnProviderMotionsReceived(object sender, EventArgs e)
        {
            DrawMotions();
        }

        private void OnProviderSequencesReceived(object sender, EventArgs e)
        {
            DrawSequences();
        }

        private void OnThumbCursorDragCompleted(object sender, DragCompletedEventArgs e)
        {
            IsDragging = false;

            _timerCursorTimeChanged.Stop();
            _cursorDragged = false;

            CursorTimeChanged?.Invoke(this, EventArgs.Empty);

            CursorDragCompleted?.Invoke(this, EventArgs.Empty);
        }

        private void OnThumbCursorDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (IsDragging)
            {
                _cursorTime = _cursorTime +
                              new TimeSpan(0, 0, 0, 0, (int) (e.HorizontalChange * MillisecondsPerPixel));
                _cursorDragged = true;
                UpdateCursorPosition();
            }
        }

        private void OnThumbCursorDragStarted(object sender, DragStartedEventArgs e)
        {
            IsDragging = true;

            CursorDragStarted?.Invoke(this, EventArgs.Empty);

            _cursorDragged = true;
            _timerCursorTimeChanged = new DispatcherTimer(new TimeSpan(0, 0, 0, 0, CursorTimeChangedRefreshRate),
                DispatcherPriority.Normal, OnTimerCursorTimeChanged, Dispatcher);
        }

        private void OnTimerCursorTimeChanged(object sender, EventArgs e)
        {
            if (_cursorDragged)
            {
                _cursorDragged = false;
                CursorTimeChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private void OnProviderThumbnailsReceived(object sender, EventArgs e)
        {
            DrawThumbnails();
        }

        private void OnProviderBookmarksReceived(object sender, EventArgs e)
        {
            DrawBookmarks();
        }
    }
}