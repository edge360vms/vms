﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Common.Enums.Video;
using RecordingService.ServiceModel.Api.TimelineEvents;
using ServiceStack;
using ITimelineEvent = Edge360.Platform.VMS.Common.Interfaces.Video.ITimelineEvent;

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl.Timeline
{
    public class TimelineEventItemModel : ITimelineEvent,  RecordingService.ServiceModel.Api.TimelineEvents.ITimelineEvent, INotifyPropertyChanged
    {
        public static implicit operator TimelineEventItemModel(TimelineEvent descriptor)
        {
            var timelineEventItemModel = descriptor.ConvertTo<TimelineEventItemModel>();
            switch (descriptor.EventType.ToLowerInvariant())
            {
                case "bookmark":
                    timelineEventItemModel.AlertLevel = 1;
                    break;
            }
            return timelineEventItemModel;
        }

        [Browsable(false)]
        public Guid CameraId
        {
            get => _cameraId;
            set
            {
                if (value.Equals(_cameraId)) return;
                _cameraId = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                if (value == _description) return;
                _description = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        int RecordingService.ServiceModel.Api.TimelineEvents.ITimelineEvent.Duration
        {
            get => (int) (Duration.Ticks / 10000);
            set => Duration = TimeSpan.FromTicks(value * 10000);
        }

        [Browsable(false)]
        string RecordingService.ServiceModel.Api.TimelineEvents.ITimelineEvent.EventType
        {
            get => EventType.ToString();
            set
            {
                //EventType = value;
            }
        }

        [Browsable(false)]
        public TimeSpan Duration
        {
            get => _duration;
            set
            {
                if (value.Equals(_duration)) return;
                _duration = value;
                OnPropertyChanged();
            }
        }

        [DisplayName("Event Time")]
        [Editable(false)]
        public DateTime EventTime
        {
            get
            {
                return TimestampUtc.ToLocalTime();
            }
            set
            {

            }
        }

        [Browsable(false)]
        public ETimelineEventType EventType
        {
            get => _eventType;
            set
            {
                if (value == _eventType) return;
                _eventType = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public Guid TimelineEventId
        {
            get => _timelineEventId;
            set
            {
                if (value.Equals(_timelineEventId)) return;
                _timelineEventId = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(ObjectId));
            }
        }

        [Browsable(false)]
        public DateTime TimestampUtc
        {
            get => _timestampUtc;
            set
            {
                if (value.Equals(_timestampUtc)) return;
                _timestampUtc = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(EventTime));
            }
        }

        [Browsable(false)]
        public Guid ObjectId
        {
            get => TimelineEventId;
            set
            {

            }
        }

        [Browsable(false)]
        public TimeZoneInfo TimeZone { get; set; }
        private Polygon _poly;
        private string _name;
        private ETimelineEventType _eventType;
        private Guid _timelineEventId;
        private DateTime _timestampUtc;
        private TimeSpan _duration;
        private string _description;
        private Guid _cameraId;


        public Visual GetVisual(Rect constraint)
        {
            // if (_poly == null)
            {
                CreatePoly();
            }
            return _poly;
        }

        public void Initialize()
        {
            CreatePoly();
        }

        private Color GetStroke()
        {
            switch (AlertLevel)
            {
                case 1:
                    return Colors.LightGreen;
                case 2:
                    return Colors.LightCoral;
                case 0:
                default:
                    return Colors.Gold;
            }
        }

        public int AlertLevel { get; set; }

        private Color GetFill()
        {
            switch (AlertLevel)
            {
                case 1:
                    return Colors.ForestGreen;
                case 2:
                    return Colors.Red;
                case 0:
                default:
                    return Colors.Orange;
            }
        }

        private void CreatePoly()
        {
            _poly = new Polygon
            {
                Points = new PointCollection(new[]
                {
                    new Point(0, 0), new Point(0, 10), new Point(5, 5), new Point(10, 10), new Point(10, 0),
                    new Point(0, 0)
                }),
                Stroke = new SolidColorBrush(GetStroke()),
                Fill = new SolidColorBrush(GetFill()),
                StrokeThickness = 1,
                MinHeight = 10,
                MinWidth = 10,
                MaxHeight = 10,
                Tag = this
            };

            if (!string.IsNullOrEmpty(Description))
            {
                //  _log?.Info($"Setting description... {Description}");
                _poly.ToolTip = new TextBlock {Text = Description};
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}