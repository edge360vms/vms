using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.ItemModel.Stream;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Monitoring.TileGrid;
using Edge360.Platform.VMS.Client.View.Control.VideoControl.Timeline;
using Edge360.Platform.VMS.Client.View.Control.VideoControl.Timeline.DataProvider;
using Edge360.Platform.VMS.Client.View.Test;
using Edge360.Platform.VMS.Common.Enums.Video;
using Edge360.Platform.VMS.Common.Enums.Video.Playback;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Interfaces.Camera;
using Edge360.Platform.VMS.Common.Interfaces.Monitoring;
using Edge360.Platform.VMS.Common.Interfaces.Video;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Monitoring.CameraComponents.Axis.PTZ;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.Monitoring.VideoComponents.MPV;
using log4net;
using Prism.Commands;
using RecordingService.ServiceModel.Api.TimelineEvents;
using Telerik.Windows.Controls;
using Unity;
using static Edge360.Platform.VMS.Communication.CommunicationManager;
using ITimelineEvent = Edge360.Platform.VMS.Common.Interfaces.Video.ITimelineEvent;

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl
{
    /// <summary>
    ///     VideoControl used with a Tile
    /// </summary>
    public partial class EdgeVideoControl : ITileComponent,
        INotifyPropertyChanged, IView
    {
        private string _archivePlaybackSessionToken;
        private StreamProfileItemModel _currentLiveProfile;
        private DirectModeManager _directModeManager;
        private bool _isAutoSelectStream = true;
        private bool _isSendingKeepAlive;
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private RelayCommand _setRecordingProfileCommand;
        private RelayCommand _setStreamProfileCommand;
        private StreamProfileItemManager _streamProfileItemManager;
        private DelegateCommand<object> _thumbnailJumpCommand;
        private int _tileColumn;
        private int _tileRow;
        private CameraItemModel _videoObject;
        private RelayCommand _videoPlaybackCommand;
        private int _tileIndex;
        public override string ToString()
        {
            return $"TileIndex {TileIndex} TileRow {TileRow} TileColumn {TileColumn}";
        }

        public StreamProfileItemModel CurrentLiveProfile
        {
            get => _currentLiveProfile;
            set
            {
                if (Equals(value, _currentLiveProfile))
                {
                    return;
                }

                _currentLiveProfile = value;
                OnPropertyChanged();
            }
        }

        public bool IsAutoSelectStream
        {
            get => _isAutoSelectStream;
            set
            {
                if (value == _isAutoSelectStream)
                {
                    return;
                }

                _isAutoSelectStream = value;
                OnPropertyChanged();
            }
        }


        public bool IsPlaying
        {
            get
            {
                var videoComponent = Model?.VideoComponent;
                return videoComponent != null && videoComponent.IsPlaying;
            }
        }

        public EdgeVideoControlViewModel Model => DataContext as EdgeVideoControlViewModel;

        public RelayCommand SetRecordingProfileCommand =>
            _setRecordingProfileCommand ??= new RelayCommand(p => SetProfile(p));

        public RelayCommand SetStreamProfileCommand =>
            _setStreamProfileCommand ??= new RelayCommand(p => SetStreamProfile(p));

        public DelegateCommand<object> ThumbnailJumpCommand =>
            _thumbnailJumpCommand ??= new DelegateCommand<object>(ThumbnailJump);

        public EdgeTimelineDataProvider TimelineDataProvider { get; private set; }

        public RelayCommand VideoControlCommand =>
            _videoPlaybackCommand ??= new RelayCommand(o => ExecuteVideoPlayback(o));

        public CameraItemModel VideoObject
        {
            get => _videoObject;
            set
            {
                if (_videoObject != value)
                {
                    _videoObject = value;

                    UpdateVideoObject();

                    OnPropertyChanged();
                }
            }
        }

        public EdgeVideoControl()
        {
            InitializeComponent();
            ThumbnailStrip.SetOwner(this);
            SizeChanged += OnSizeChanged;
            FloatingButtonControl.VideoControl = this;
            //   DataContext = new EdgeVideoControlViewModel(this);
            Model.Owner = this;

            InitTimeline();
            UpdateVideoObject();
            //try
            //{
            ////    DigitalZoomWindow.Initialize(VideoComponent as IEdgeDigitalZoomSupport);
            //}
            //catch (Exception e)
            //{
            //}
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public override ITileGrid GridModel => this.ParentOfType<TileGridView>()?.Model;


        public override event Action<object> ObjectDragOver;
        public override event Action<object> ObjectDragComplete;

        public override int TileIndex
        {
            get => _tileIndex;
            set
            {
                if (value == _tileIndex) return;
                _tileIndex = value;
                OnPropertyChanged();
            }
        }

        public override int TileColumn
        {
            get => _tileColumn;
            set
            {
                if (_tileColumn != value)
                {
                    _tileColumn = value;
                    Grid.SetColumn(this, value);
                    OnPropertyChanged();
                }
            }
        }

        public override int TileColumnSpan { get; set; }
        public override bool IsFullscreen { get; set; }

        public override int TileRow
        {
            get => _tileRow;
            set
            {
                if (_tileRow != value)
                {
                    _tileRow = value;
                    Grid.SetRow(this, value);
                    OnPropertyChanged();
                }
            }
        }

        public override int TileRowSpan { get; set; }


        public override void Dispose()
        {
            Model?.Dispose();
            VideoComponent?.Dispose();
            base.Dispose();
        }

        private void MpvOnMediaLoaded()
        {
        }

        private void MpvOnMediaUnloaded()
        {
        }

        private void MpvOnMediaPlayerError()
        {
        }

        private void MpvOnMediaPlayerEnded()
        {
        }

        private void ThumbnailJump(object obj)
        {
            if (obj is ThumbnailItemModel thumb)
            {
                JumpToDate(TimeUtil.FixServiceStackTime(thumb.DateTime.DateTime));
            }
        }

        /// <summary>
        ///     Goes to the previous bookmark when Timeline has queried data
        /// </summary>
        public void GotoPreviousBookmark()
        {
            try
            {
                if (EdgeVideoTimeline == null || EdgeVideoTimeline.DataProvider?.Bookmarks == null)
                {
                    return;
                }

                var bookmarkTime = EdgeVideoTimeline.DataProvider.Bookmarks.OrderByDescending(o => o.EventTime)
                    .Select(o => o.EventTime)
                    .FirstOrDefault(dt => dt + TimeSpan.FromSeconds(1) < EdgeVideoTimeline.CursorTime);
                if (bookmarkTime != null && bookmarkTime != DateTime.MinValue)
                {
                    //var dt = TimeZoneInfo.ConvertTimeFromUtc(DateTime.SpecifyKind(bookmarkTime, DateTimeKind.Utc),
                    //    TimeZoneInfo.Local);
                    JumpToDate(bookmarkTime, VideoComponent.PlayerState == EPlayerState.Paused);
                    //Timeline.CursorTime = bookmarkTime;
                }
            }
            catch (Exception e)
            {
            }
        }


        /// <summary>
        ///     Goes to the next bookmark when Timeline has queried data
        /// </summary>
        public void GotoNextBookmark()
        {
            try
            {
                if (EdgeVideoTimeline == null || EdgeVideoTimeline.DataProvider?.Bookmarks == null)
                {
                    return;
                }

                var bookmarkTime = EdgeVideoTimeline.DataProvider.Bookmarks.OrderBy(o => o.EventTime)
                    .Select(o => o.EventTime)
                    .FirstOrDefault(dt => dt > EdgeVideoTimeline.CursorTime);
                if (bookmarkTime != null && bookmarkTime != DateTime.MinValue)
                {
                    //var dt = TimeZoneInfo.ConvertTimeFromUtc(DateTime.SpecifyKind(bookmarkTime, DateTimeKind.Local),
                    //    TimeZoneInfo.Local);
                    JumpToDate(bookmarkTime, VideoComponent.PlayerState == EPlayerState.Paused);
                    //Timeline.CursorTime = bookmarkTime;
                }
            }
            catch (Exception e)
            {
            }
        }

        public void SetPlayback(EPlaybackState state)
        {
            if (VideoComponent.PlaybackState == EPlaybackState.None)
            {
                VideoComponent.PlaybackState = state;
            }

            if (VideoComponent.IsLivePlayback)
            {
                VideoComponent.PlaybackState = EPlaybackState.Archive;
                if (state == EPlaybackState.Reverse)
                {
                    JumpToDate(EdgeVideoTimeline.CursorTime.Subtract(TimeSpan.FromSeconds(10)));
                }
            }

            if (state != VideoComponent.PlaybackState && VideoComponent.PlaybackSpeed == EPlaybackSpeed.Speed1x)
            {
                // reverse button is used to slow down forward X

                VideoComponent.PlaybackState = state;
                VideoComponent.PlaybackSpeed = EPlaybackSpeed.Speed1x;
            }
            else
            {
                AdjustPlaybackSpeed(state == VideoComponent.PlaybackState);
            }

            _log?.Info(
                $"PlaybackSpeed {VideoComponent.PlaybackSpeed} PlaybackState {VideoComponent.PlaybackState}");
        }

        public void AdjustPlaybackSpeed(bool increase)
        {
            switch (VideoComponent.PlaybackSpeed)
            {
                case EPlaybackSpeed.Speed1x:
                    VideoComponent.PlaybackSpeed = increase ? EPlaybackSpeed.Speed2x : EPlaybackSpeed.Speed1x;
                    break;
                case EPlaybackSpeed.Speed2x:
                    VideoComponent.PlaybackSpeed = increase ? EPlaybackSpeed.Speed4x : EPlaybackSpeed.Speed1x;
                    break;
                case EPlaybackSpeed.Speed4x:
                    VideoComponent.PlaybackSpeed = increase ? EPlaybackSpeed.Speed10x : EPlaybackSpeed.Speed2x;
                    break;
                case EPlaybackSpeed.Speed10x:
                    VideoComponent.PlaybackSpeed = increase ? EPlaybackSpeed.Speed10x : EPlaybackSpeed.Speed4x;
                    break;
            }
        }

        public void UpdateVideoObject()
        {
            this?.TryUiAsync(() =>
            {
                var isDirectMode = Model?.DirectModeManager?.IsDirectMode ?? false;
                var newVis = _videoObject == null
                    ? Visibility.Hidden
                    : Visibility.Visible;

                if (FloatingButtonControl.Visibility != newVis)
                {
                    FloatingButtonControl.Visibility = newVis;
                }

                if (_videoObject != null)
                {
                    if (ContentControl?.Content is MPVVideoComponent component)
                    {
                        component?.VideoView?.Stop();
                        try
                        {
                            ThumbnailStrip.GetModel().Thumbnails.Clear();
                        }
                        catch (Exception e)
                        {
                        }

                        try
                        {
                            if (Model != null)
                            {
                                ClearPlaybackMembers(component);
                                Model.IsFailedPlayback = false;
                            }

                            CurrentLiveProfile = null;
                            component.PtzManager = isDirectMode
                                ? (IPtzManager) new ONVIFManager(VideoObject.Address, VideoObject.Username,
                                    VideoObject.Password)
                                : new RemoteONVIFPtz(VideoObject.Id);
                            Task.Run(LoadLiveProfiles);
                            LoadTimelineEvents();
                        }
                        catch (Exception e)
                        {
                        }

                        //component.PtzManager = new AxisPtz(ContentControl,
                        //    new AxisCameraConfigManager
                        //    {
                        //        Address = _videoObject.Address,
                        //        Label = _videoObject.Label,
                        //        Model = "Axis",
                        //        Id = _videoObject.Id
                        //    });
                    }
                }
            });
        }

        public void ClearPlaybackMembers(MPVVideoComponent component)
        {
            if (component.VideoView != null)
            {
                component.VideoView.VideoBitrate = 0;
                component.VideoView.TimePosition = 0;
            }
            Model.LiveFallbackFailureTime = DateTime.MinValue;
            Model.ClearPlaybackTokens();
            
            Model.IsDirectPlayback = false;
            Model.IsRetryingUnloaded = false;
            Model.IsUnableToFindStream = false;
            Model.IsRetryingServerAfterUnload = false;
        }

        private async void LoadLiveProfiles()
        {
            try
            {
                _streamProfileItemManager ??= StreamProfileItemManager.Manager;
                await _streamProfileItemManager.RefreshProfiles(VideoObject);
            }
            catch (Exception e)
            {
            }
        }

        public override void JumpToDate(DateTime? date, bool pause = false)
        {
            try
            {
                if (date != null)
                {
                    //EdgeVideoTimeline.UpdateTimerCallback(date.Value);
                    if (date.Value <= DateTime.Now)
                    {
                        if (VideoComponent != null)
                        {
                            VideoComponent.IsLivePlayback = false;
                        }

                        EdgeVideoTimeline.CursorTime = date.Value;

                        EdgeVideoTimeline.UpdateTimeline(true);
                        Model?.RequestRecording(EdgeVideoTimeline.CursorTime);
                    }

                    // EdgeVideoTimeline.UpdateTimeline();
                }

                base.JumpToDate(date, pause);
            }
            catch (Exception e)
            {
            }
        }


        private void ExecuteVideoPlayback(object o)
        {
            Console.WriteLine($"{TimelineDataProvider.IsTickerHidden}");
            if (GridModel is TileGridViewModel tileGridViewModel)
            {
                if (tileGridViewModel.SelectedComponents.Contains(this))
                {
                    tileGridViewModel.SelectedComponents.Remove(this);
                }

                tileGridViewModel.SelectedComponents.Add(this);

                if (o is EVideoPlayerCommand command)
                {
                    switch (command)
                    {
                        case EVideoPlayerCommand.ConfigureStream:
                            FloatingButtonControl.Model.IsConfigMode = !FloatingButtonControl.Model.IsConfigMode;
                            break;
                    }
                }

                GridModel?.VideoControlCommand?.Execute(o);
            }
        }

        public async void SetStreamProfile(object o)
        {
            try
            {
                if (o is BaseAdapterCameraProfileItemModel cameraProfile)
                {
                    if (VideoObject != null)
                    {
                        _directModeManager ??= App.GlobalUnityContainer.Resolve<DirectModeManager>();
                        if (_directModeManager.IsDirectMode)
                        {
                            if (VideoObject.CurrentOnvifProfile != cameraProfile)
                            {
                                VideoObject.LiveStreamUrl = cameraProfile.StreamUri;
                                VideoObject.CameraUrl = cameraProfile.StreamUri;
                                VideoObject.CurrentOnvifProfile = cameraProfile;
                                IsAutoSelectStream = false;
                                Model.PlayLive();
                            }
                        }
                    }
                }
                else if (o is StreamProfileItemModel stream)
                {
                    if (VideoObject != null)
                    {
                        //  if (resp != null)
                        {
                            //      VideoObject.LiveStreamUrl = resp.RtspUrl;
                            CurrentLiveProfile = stream;
                            IsAutoSelectStream = false;
                            //VideoObject.CurrentOnvifProfile = cameraProfile;
                            Model.PlayLive();
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private void SetProfile(object o)
        {
            if (o is RecordingProfileItemModel item)
            {
                Model.SelectedRecordingProfile = item;
            }
        }

        private void InitTimeline()
        {
            if (EdgeVideoTimeline.DataProvider == null)
            {
                if (EdgeVideoTimeline.DataProvider == null)
                {
                    EdgeVideoTimeline.Owner = this;
                    TimelineDataProvider = new EdgeTimelineDataProvider(this);
                    EdgeVideoTimeline.Initialize(TimelineDataProvider);

                    if (EdgeVideoTimeline.DataProvider != null)
                    {
                        EdgeVideoTimeline.DataProvider.TimelineRangeChanged += DataProviderOnTimelineRangeChanged;
                        EdgeVideoTimeline.CursorTimeChanged += EdgeVideoTimelineOnCursorTimeChanged;
                    }

                    EdgeVideoTimeline.SetTimelineRange(DateTime.UtcNow.Subtract(TimeSpan.FromHours(8)),
                        DateTime.UtcNow.Add(TimeSpan.FromMinutes(5)));
                    EdgeVideoTimeline.CursorTime = DateTime.Now;
                    EdgeVideoTimeline.UpdateTimeline(true);
                    EdgeVideoTimeline.DrawLayers();
                }
            }
        }

        private void LoadTimelineEvents()
        {
            Task.Run(async () =>
            {
                try
                {
                    if (VideoObject != null &&
                        ServiceManagers.ContainsKey(VideoObject.ZoneId))
                    {
                        var serviceManager = GetManagerByZoneId(VideoObject.ZoneId);
                        if (serviceManager == null)
                        {
                            return;
                        }

                        var events = await serviceManager?.TimelineEventService.SearchTimelineEvents(
                            new SearchTimelineEvents {CameraId = VideoObject.Id});
                        if (events?.TimelineEvents != null)
                        {
                            EdgeVideoTimeline.DataProvider.Bookmarks =
                                new List<ITimelineEvent>(
                                    events.TimelineEvents.Select(t => (TimelineEventItemModel) t));
                        }
                    }
                }
                catch (Exception e)
                {
                }
            });
        }


        public void SetSelected(bool selected)
        {
            Application.Current.Dispatcher.InvokeAsync(() =>
            {
                ControlBorder.BorderBrush = selected
                    ? Brushes.CornflowerBlue
                    : new SolidColorBrush((Color) ColorConverter.ConvertFromString("#CC3A3A3A"));
            });
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            Model.OnSizeChanged(sender, e);
        }

        private void EdgeVideoTimelineOnCursorTimeChanged(object sender, EventArgs e)
        {
            if (EdgeVideoTimeline != null)
            {
                EdgeVideoTimeline.UpdateTimeline(true);
                Model.RequestRecording(EdgeVideoTimeline.CursorTime);
            }
        }

        private void DataProviderOnTimelineRangeChanged(DateTime begin, DateTime end)
        {
            Model.HandleThumbnailRequest();
        }

        private void UIElement_OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            VideoComponent.IsShowingVideoStats = false;
            e.Handled = true;
        }


        private void ThumbnailView_OnMouseEnter(object sender, MouseEventArgs e)
        {
            foreach (var c in ThumbnailStrip.ItemsControl.Items.OfType<ThumbnailItemModel>())
            {
                c.IsOpen = true;
            }
        }

        private void ThumbnailView_OnMouseLeave(object sender, MouseEventArgs e)
        {
            foreach (var c in ThumbnailStrip.ItemsControl.Items.OfType<ThumbnailItemModel>())
            {
                c.IsOpen = false;
            }
        }

        private void ThumbnailStrip_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //if (e.Delta > 0)
            //{
            //    ThumbnailStrip.GetModel().DesiredThumbnailCount++;
            //}
            //else
            //{
            //    ThumbnailStrip.GetModel().DesiredThumbnailCount--;
            //}
            //Trace.WriteLine($"MOUSE WHEEL");
            try
            {
                ThumbnailStrip.StripIsActive = false;
                Model.HandleThumbnailRequest();
                ThumbnailStrip.StripIsActive = true;
            }
            catch (Exception exception)
            {
            }
        }
    }
}