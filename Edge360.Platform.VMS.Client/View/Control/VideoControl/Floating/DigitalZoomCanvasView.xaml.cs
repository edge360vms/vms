﻿using System.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl.Floating
{
    /// <summary>
    ///     Interaction logic for VideoThumbnailControl.xaml
    /// </summary>
    public partial class DigitalZoomCanvasView : UserControl, IView
    {
        public DigitalZoomCanvasViewModel Model => DataContext as DigitalZoomCanvasViewModel;

        public DigitalZoomCanvasView()
        {
            InitializeComponent();
            DataContext = new DigitalZoomCanvasViewModel(this);
        }
    }
}