﻿using System.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl.Floating
{
    /// <summary>
    ///     Interaction logic for VideoZoomControl.xaml
    /// </summary>
    public partial class VideoZoomControl : UserControl, IView
    {
        public VideoZoomControl()
        {
            InitializeComponent();
        }
    }
}