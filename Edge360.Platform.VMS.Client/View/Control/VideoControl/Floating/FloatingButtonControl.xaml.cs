﻿using System.Windows;
using System.Windows.Controls;
using Edge360.Platform.VMS.Common.Interfaces.Monitoring;

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl.Floating
{
    /// <summary>
    ///     Interaction logic for VideoButtonControl.xaml
    /// </summary>
    public partial class FloatingButtonControl : UserControl, IView
    {
        public static readonly DependencyProperty VideoControlProperty = DependencyProperty.Register(
            "VideoControl", typeof(ITileComponent), typeof(FloatingButtonControl),
            new PropertyMetadata(default(ITileComponent)));
        //public static readonly DependencyProperty VideoWindowProperty = DependencyProperty.Register(
        //    "VideoWindow", typeof(VMS.Monitoring.VideoComponents.VideoControl), typeof(FloatingButtonControl),
        //    new PropertyMetadata(default(VMS.Monitoring.VideoComponents.VideoControl)));

        //public FloatingButtonControlViewModel Model => DataContext as FloatingButtonControlViewModel;

        //public VMS.Monitoring.VideoComponents.VideoControl VideoWindow
        //{
        //    get => (VMS.Monitoring.VideoComponents.VideoControl) GetValue(VideoWindowProperty);
        //    set => SetValue(VideoWindowProperty, value);
        //}

        public FloatingButtonControlViewModel Model => DataContext as FloatingButtonControlViewModel;

        public ITileComponent VideoControl
        {
            get => (ITileComponent) GetValue(VideoControlProperty);
            set => SetValue(VideoControlProperty, value);
        }


        public FloatingButtonControl()
        {
            InitializeComponent();
            DataContext = new FloatingButtonControlViewModel(this);
        }
    }
}