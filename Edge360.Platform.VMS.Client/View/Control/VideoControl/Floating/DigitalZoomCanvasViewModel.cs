﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.Annotations;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl.Floating
{
    public class DigitalZoomCanvasViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private readonly DigitalZoomCanvasView _view;
        private int _desiredThumbnailCount;

        public int DesiredThumbnailCount
        {
            get => _desiredThumbnailCount;
            set
            {
                if (_desiredThumbnailCount != value)
                {
                    _desiredThumbnailCount = value;
                    OnPropertyChanged();
                }
            }
        }

        public DigitalZoomCanvasViewModel(DigitalZoomCanvasView view)
        {
            _view = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Zoom(bool zoomIn, Point getPosition)
        {
            var videoContent = _view.GetVisualParent<System.Windows.Window>();
            if (videoContent != null && videoContent.DataContext is EdgeVideoControlViewModel videoControlViewModel)
            {
                //if (videoControlViewModel?.VideoComponent?.PtzController is PtzControllerManager manager)
                //{
                //    manager.Zoom(zoomIn, getPosition);
                //    //vlc.VideoView.MediaPlayer.EnableHardwareDecoding = true;

                //    //vlc.VideoView.MediaPlayer.CropGeometry = "120x120x+10+10";
                //}

                try
                {
                    if (Keyboard.IsKeyDown(Key.LeftCtrl))
                    {
                        videoControlViewModel.VideoComponent?.PtzManager?.Zoom(zoomIn, getPosition);
                    }
                    else
                    {
                        videoControlViewModel.VideoComponent?.DigitalZoom(zoomIn, getPosition);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}