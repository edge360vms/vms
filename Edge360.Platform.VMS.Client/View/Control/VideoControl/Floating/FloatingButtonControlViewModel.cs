﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl.Floating
{
    public class FloatingButtonControlViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private bool _isConfigMode;
        private FloatingButtonControl _view;

        public bool IsConfigMode
        {
            get => _isConfigMode;
            set
            {
                if (value == _isConfigMode)
                {
                    return;
                }

                _isConfigMode = value;
                OnPropertyChanged();
            }
        }


        public FloatingButtonControlViewModel(FloatingButtonControl view)
        {
            _view = view;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}