﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.ItemModel.Video.Settings;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Canvas;
using Edge360.Platform.VMS.Client.View.Test;
using Edge360.Platform.VMS.Common.Enums.Settings;
using Edge360.Platform.VMS.Common.Enums.Video;
using Edge360.Platform.VMS.Common.Interfaces.Camera;
using Edge360.Platform.VMS.Common.Interfaces.Video;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Monitoring.CameraComponents.Axis.PTZ;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.Monitoring.VideoComponents.MPV;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.StreamingProfile;
using Edge360.Raven.Vms.RecordingServiceAgent.ServiceModel.Api.Thumbs;
using log4net;
using RecordingService.ServiceModel.Api.Playback;
using WispFramework.Extensions.Common;
using static Edge360.Platform.VMS.Communication.CommunicationManager;
using Point = System.Windows.Point;

namespace Edge360.Platform.VMS.Client.View.Control.VideoControl
{
    public class EdgeVideoControlViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private readonly EControlOverlaySettings _defaultOverlaySettings =
            EControlOverlaySettings.Timestamp | EControlOverlaySettings.Timeline | EControlOverlaySettings.CameraName |
            EControlOverlaySettings.PlaybackSpeed | EControlOverlaySettings.PtzClickArea;

        private readonly ILog _log;
        private readonly StreamProfileItemManager _streamStreamProfileMgr;


        private string _archivePlaybackSessionToken;
        private CancellationTokenSource _bitrateCheckCts;
        private Task<object> _bitrateTaskCheck;

        private EControlOverlaySettings _controlOverlaySettings;
        private CancellationTokenSource _directLiveCheckCts;
        private DirectModeManager _directModeManager;
        private CancellationTokenSource _fallbackLiveToDirectCts;
        private bool _isDirectPlayback;
        private bool _isFailedPlayback;
        private bool _isRequestingThumbnail;
        private bool _isSendingKeepAlive;
        private bool _isUnableToFindStream;
        private OnvifProfileItemManager _onvifProfileItemMgr;

        private ObservableCollection<OverlayFlagItemModel> _overlayOptionsCollection;
        private EdgeVideoControl _owner;
        private Task _playLiveTask;
        private EControlOverlaySettings _previousOverlaySettings;
        private RecordingProfileItemModel _selectedRecordingProfile;
        private ThumbnailWindow _thumbWindow;
        private int bitrateMatchCount;

        internal DateTime LiveFallbackFailureTime;
        private DateTime playbackTimePosFailureTime;

        private double previousBitrate;
        private ListThumbnailsResponse thumbnailsResponse;

        public string ActiveArchivePlaybackRtspUrl { get; set; }

        public string ArchivePlaybackSessionToken
        {
            get => _archivePlaybackSessionToken;
            set
            {
                if (value == _archivePlaybackSessionToken)
                {
                    return;
                }

                _archivePlaybackSessionToken = value;
                OnPropertyChanged();
            }
        }


        public EControlOverlaySettings ControlOverlaySettings
        {
            get => _controlOverlaySettings;
            set
            {
                if (_controlOverlaySettings != value)
                {
                    _controlOverlaySettings = value;
                    UpdateFlagSettings();
                    OnPropertyChanged();
                }
            }
        }

        public DirectModeManager DirectModeManager
        {
            get => _directModeManager;
            private set
            {
                if (Equals(value, _directModeManager))
                {
                    return;
                }

                _directModeManager = value;
                OnPropertyChanged();
            }
        }

        public MPVVideoComponent GetMpvVideoComponent => VideoComponent as MPVVideoComponent;

        public bool IsDirectPlayback
        {
            get => _isDirectPlayback;
            set
            {
                if (value == _isDirectPlayback)
                {
                    return;
                }

                _isDirectPlayback = value;
                OnPropertyChanged();
            }
        }

        public bool IsFailedPlayback
        {
            get => _isFailedPlayback;
            set
            {
                if (value == _isFailedPlayback)
                {
                    return;
                }

                _isFailedPlayback = value;
                OnPropertyChanged();
            }
        }

        public bool IsMediaLoaded { get; set; }

        public bool IsMouseDown { get; private set; }

        public bool IsPtzActionActive { get; set; }

        public bool IsReconnecting { get; set; }

        public bool IsRequestingRecording { get; set; }

        public bool IsRequestingThumbnail
        {
            get => _isRequestingThumbnail;
            set
            {
                if (value == _isRequestingThumbnail)
                {
                    return;
                }

                _isRequestingThumbnail = value;
                OnPropertyChanged();
            }
        }

        public bool IsRetryingServerAfterUnload { get; set; }

        public bool IsRetryingUnloaded { get; set; }

        public bool IsSendingKeepAlive
        {
            get => _isSendingKeepAlive;
            set
            {
                if (value == _isSendingKeepAlive)
                {
                    return;
                }


                _isSendingKeepAlive = value;
                UpdateKeepingAlive();

                OnPropertyChanged();
            }
        }

        public bool IsUnableToFindStream
        {
            get => _isUnableToFindStream;
            set
            {
                if (value == _isUnableToFindStream)
                {
                    return;
                }

                _isUnableToFindStream = value;
                OnPropertyChanged();
            }
        }

        public OnvifProfileItemManager OnvifProfileItemMgr
        {
            get => _onvifProfileItemMgr;
            set
            {
                if (Equals(value, _onvifProfileItemMgr))
                {
                    return;
                }

                _onvifProfileItemMgr = value;
                OnPropertyChanged();
            }
        }


        public ObservableCollection<OverlayFlagItemModel> OverlayOptionsCollection
        {
            get => _overlayOptionsCollection;
            set
            {
                if (_overlayOptionsCollection != value)
                {
                    _overlayOptionsCollection = value;
                    OnPropertyChanged();
                }
            }
        }

        public EdgeVideoControl Owner
        {
            get => _owner;
            set
            {
                if (_owner != value)
                {
                    _owner = value;
                    InitializeOwner(value);
                    OnPropertyChanged();
                    if (VideoComponent is MPVVideoComponent mpv)
                    {
                        mpv.MediaPlayerEnded += MpvOnMediaPlayerEnded;
                        mpv.MediaPlayerError += MpvOnMediaPlayerError;
                        mpv.MediaLoaded += MpvOnMediaLoaded;
                        mpv.MediaUnloaded += MpvOnMediaUnloaded;
                        mpv.MediaEndedSeeking += MpvOnMediaEndedSeeking;
                        mpv.MediaStartedBuffering += MpvOnMediaStartedBuffering;
                        mpv.MediaEndedBuffering += MpvOnMediaEndedBuffering;
                        mpv.MediaStartedSeeking += MpvOnMediaStartedSeeking;
                        mpv.MediaResumed += MpvOnMediaResumed;
                        mpv.MediaPaused += MpvOnMediaPaused;
                    }
                }
            }
        }

        public CancellationTokenSource PtzClickCancellationTokenSource { get; set; }

        public RecordingProfileItemModel SelectedRecordingProfile
        {
            get => _selectedRecordingProfile;
            set
            {
                if (Equals(value, _selectedRecordingProfile))
                {
                    return;
                }

                try
                {
                    _selectedRecordingProfile = value;
                    if (_selectedRecordingProfile != null)
                    {
                        RequestRecording(_owner.EdgeVideoTimeline.CursorTime);
                        HandleThumbnailRequest();
                        Application.Current.Dispatcher.InvokeAsync(() =>
                        {
                            try
                            {
                                if (!_owner.VideoComponent.IsPlaying)
                                {
                                    _owner?.Model?.Open(_owner?.VideoComponent?.LastOpenedUrl);
                                    _owner?.Model?.Play();
                                }
                            }
                            catch (Exception e)
                            {
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                }

                OnPropertyChanged();
            }
        }

        public IVideoComponent VideoComponent => Owner?.VideoComponent;

        private CancellationTokenSource archivePlaybackCancellationTokenSource { get; set; }

        public EdgeVideoControlViewModel(ILog log = null, DirectModeManager directModeManager = null,
            StreamProfileItemManager streamProfileItemManager = null,
            OnvifProfileItemManager onvifProfileItemManager = null)
        {
            _log = log;
            OnvifProfileItemMgr = onvifProfileItemManager;
            _streamStreamProfileMgr = streamProfileItemManager;
            DirectModeManager = directModeManager;
            if (directModeManager != null)
            {
                directModeManager.DirectModeChanged += OnDirectModeChanged;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        public async void HandleThumbnailRequest()
        {
            if (IsRequestingThumbnail ||
                Owner.EdgeVideoTimeline.DataProvider == null)
            {
                return;
            }

            //await Task.Delay(100);
            var _ = this.TryUiAsync(async () =>
            {
                try
                {
                    IsRequestingThumbnail = true;
                    var begin = DateTime.SpecifyKind(Owner.EdgeVideoTimeline.DataProvider.BeginTime, DateTimeKind.Local)
                        .ToUniversalTime();
                    var end = DateTime.SpecifyKind(Owner.EdgeVideoTimeline.DataProvider.EndTime, DateTimeKind.Local)
                        .ToUniversalTime();

                    var newList = new List<ThumbnailItemModel>();
                    //
                    if (end > DateTime.UtcNow)
                    {
                        end = DateTime.UtcNow;
                    }

                    var totalTicks = (end - begin).Ticks;
                    var thumbnailCount = Owner?.ThumbnailStrip?.GetModel()?.DesiredThumbnailCount ?? 0;
                    if (thumbnailCount == 0)
                    {
                        return;
                    }

                    if (SelectedRecordingProfile != null)
                    {
                        thumbnailsResponse = await
                            ThumbnailItemManager.Manager.RequestThumbnails(SelectedRecordingProfile.Id,
                                begin);
                        var orderedThumbs = thumbnailsResponse?.Thumbnails?.OrderBy(o => o.ThumbnailDateTime).ToList();

                        if (orderedThumbs != null)
                        {
                            var timelineGapTimes = new TimeSpan(totalTicks / thumbnailCount);
                            for (var i = 0; i < thumbnailCount; i++)
                            {
                                begin = begin + timelineGapTimes;
                                var thumb = orderedThumbs.FirstOrDefault(o =>
                                    o.ThumbnailDateTime.DateTime > begin && o.ThumbnailDateTime.DateTime < end &&
                                    newList.All(n => n.DateTime != o.ThumbnailDateTime));
                                if (thumb != null && thumb.ImageBytes != null && thumb.ImageBytes.Length > 0)
                                {
                                    if (thumb != null)
                                    {
                                        using (var ms = new MemoryStream(thumb.ImageBytes))
                                        {
                                            var bmp = Image.FromStream(ms) as Bitmap;
                                            var thumbItem = new ThumbnailItemModel
                                            {
                                                IsOpen = false,
                                                Thumbnail = ImageUtil.BitmapToSource(bmp),
                                                DateTime = thumb.ThumbnailDateTime
                                            };
                                            newList.Add(thumbItem);
                                        }
                                    }
                                }
                            }

                            await Task.Delay(200);
                            orderedThumbs = null;
                            Owner?.ThumbnailStrip?.SetThumbnails(newList);
                        }
                    }
                }
                catch (Exception e)
                {
                }
                finally
                {
                    IsRequestingThumbnail = false;
                }
            }, DispatcherPriority.Background);
        }

        private void InitializeOwner(EdgeVideoControl owner)
        {
            if (owner != null)
            {
                // Owner = owner;
                Owner.TextOverlay.VideoControl = owner;
                owner.PtzOverlay.SetOwner(owner);
                owner.MainGrid.MouseDown += (args, e) => { DoMouseDown(e); };
                owner.MainGrid.MouseUp += (args, e) => { DoMouseUp(e); };
                Owner.VideoComponent = Owner.ContentControl.Content as IVideoComponent;
                VideoComponent.PlaybackSpeedChanged += VideoComponentOnPlaybackSpeedChanged;
                VideoComponent.PlaybackStateChanged += VideoComponentOnPlaybackStateChanged;
                VideoComponent.MediaPlayerError += VideoComponentOnMediaPlayerError;
                VideoComponent.MediaPlayerEnded += VideoComponentOnMediaPlayerEnded;
                var edgeVideoControl = Owner;
                var mpv = VideoComponent as MPVVideoComponent;
                _bitrateCheckCts = new CancellationTokenSource();
                _directLiveCheckCts = new CancellationTokenSource();

                try
                {
                    Task.Factory.StartNew(async o =>
                    {
                        try
                        {
                            while (!_directLiveCheckCts.IsCancellationRequested)
                            {
                                if (edgeVideoControl != null)
                                {
                                    var reconnectFromDirectInterval =
                                        SettingsManager.Settings?.Camera?.ReconnectFromDirectModeInterval ?? 20;
                                    await Task.Delay(TimeSpan.FromSeconds(reconnectFromDirectInterval));

                                    if (mpv != null && DateTime.UtcNow >
                                        playbackTimePosFailureTime + TimeSpan.FromMinutes(1))
                                    {
                                        if (IsMediaLoaded && mpv.IsLivePlayback && IsDirectPlayback &&
                                            !DirectModeManager.IsDirectMode)
                                        {
                                            PlayLive(false);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }, TaskCreationOptions.LongRunning, _bitrateCheckCts.Token);
                }
                catch (Exception e)
                {
                }

                try
                {
                    Task.Factory.StartNew(async o =>
                    {
                        try
                        {
                            while (!_bitrateCheckCts.IsCancellationRequested)
                            {
                                if (edgeVideoControl != null)
                                {
                                    await Task.Delay(2000);

                                    if (mpv != null)
                                    {
                                        if (IsMediaLoaded && mpv.IsLivePlayback && mpv.IsPlaying &&
                                            mpv.PlayerState != EPlayerState.Stopped)
                                        {
                                            if (mpv.VideoView != null && mpv.VideoView.TimePosition == previousBitrate)
                                            {
                                                bitrateMatchCount++;
                                            }
                                            else
                                            {
                                                bitrateMatchCount = 0;
                                            }

                                            var cameraMaxBitrateMatchingFallbackValue = SettingsManager.Settings?.Camera
                                                ?.MaxBitrateMatchingFallbackValue ?? 5;
                                            if (bitrateMatchCount > cameraMaxBitrateMatchingFallbackValue)
                                            {
                                                Trace.WriteLine("Bitrate hasn't changed in 10+ seconds...");
                                                playbackTimePosFailureTime = DateTime.UtcNow;
                                                IsFailedPlayback = true;
                                                PlayLive();
                                                bitrateMatchCount = 0;
                                            }
                                        }

                                        if (mpv.VideoView != null)
                                        {
                                            previousBitrate = mpv.VideoView.TimePosition;
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }, TaskCreationOptions.LongRunning, _bitrateCheckCts.Token);
                }
                catch (Exception e)
                {
                }
            }

            InitOverlayFlagSettings();
        }

        private void UpdateKeepingAlive()
        {
            if (_isSendingKeepAlive)
            {
                archivePlaybackCancellationTokenSource?.Cancel();
                archivePlaybackCancellationTokenSource = new CancellationTokenSource();
                var _ = Task.Run(async () =>
                {
                    try
                    {
                        while (IsSendingKeepAlive &&
                               !archivePlaybackCancellationTokenSource.Token.IsCancellationRequested)
                        {
                            await Application.Current.Dispatcher.InvokeAsync(() =>
                            {
                                //stop keepalive if we have ended archive playback
                                if (VideoComponent.PlayerState == EPlayerState.Stopped ||
                                    VideoComponent.IsLivePlayback)
                                {
                                    IsSendingKeepAlive = false;
                                }
                            });
                            if (archivePlaybackCancellationTokenSource.IsCancellationRequested)
                            {
                                throw new OperationCanceledException(archivePlaybackCancellationTokenSource.Token);
                            }

                            //update time and send keepalive every 3 seconds
                            await Task.Delay(3000);
                            var keepAlive = false;
                            try
                            {
                                if (_owner?.VideoObject != null &&
                                    ServiceManagers.ContainsKey(_owner.VideoObject.ZoneId))
                                {
                                    //request keepalive from zone with camera
                                    var serviceManager = GetManagerByZoneId(_owner.VideoObject.ZoneId);
                                    keepAlive = _owner.VideoObject != null &&
                                                await serviceManager.PlaybackService
                                                    .KeepAlivePlayback(new KeepAlivePlayback
                                                        {SessionToken = ArchivePlaybackSessionToken});
                                    if (_owner.VideoObject != null)
                                    {
                                        var sw = new Stopwatch();
                                        sw.Start();
                                        var resp = await serviceManager.PlaybackService
                                            .GetPlaybackStatus(new GetPlaybackStatus
                                                {SessionToken = ArchivePlaybackSessionToken});
                                        sw.Stop();

                                        if (resp != null && resp.Success &&
                                            resp.FrameTime.LocalDateTime > DateTime.MinValue)
                                        {
                                            await Application.Current.Dispatcher.InvokeAsync(() =>
                                            {
                                                //update timebar with time sent from RecordingServiceAgent
                                                _owner.EdgeVideoTimeline.CursorTime =
                                                    resp.FrameTime.LocalDateTime +
                                                    TimeSpan.FromMilliseconds(sw.ElapsedMilliseconds);
                                            });
                                        }
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                            _log?.Info($"KeepAlive sent {keepAlive}");
                            IsSendingKeepAlive = keepAlive;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }, archivePlaybackCancellationTokenSource.Token);
            }
            else
            {
                ArchivePlaybackSessionToken = string.Empty;
                ActiveArchivePlaybackRtspUrl = string.Empty;
                archivePlaybackCancellationTokenSource?.Cancel();
            }
        }

        private void InitOverlayFlagSettings()
        {
            OverlayOptionsCollection = new ObservableCollection<OverlayFlagItemModel>
            {
                new OverlayFlagItemModel {Setting = EControlOverlaySettings.DigitalZoom},
                new OverlayFlagItemModel {Setting = EControlOverlaySettings.Timeline},
                new OverlayFlagItemModel {Setting = EControlOverlaySettings.PlaybackSpeed},
                new OverlayFlagItemModel {Setting = EControlOverlaySettings.CameraName},
                new OverlayFlagItemModel {Setting = EControlOverlaySettings.Framerate},
                new OverlayFlagItemModel {Setting = EControlOverlaySettings.Timestamp},
                new OverlayFlagItemModel {Setting = EControlOverlaySettings.Bitrate},
                new OverlayFlagItemModel {Setting = EControlOverlaySettings.All}
            };
            foreach (var o in OverlayOptionsCollection)
            {
                if (o.Setting != EControlOverlaySettings.All)
                {
                    o.IsChecked = ControlOverlaySettings.HasFlag(o.Setting);
                }

                o.FlagChanged += OverlaySettingOnFlagChanged;
            }
        }

        private void UpdateFlagSettings()
        {
            foreach (var o in OverlayOptionsCollection)
            {
                if (o.Setting != EControlOverlaySettings.All)
                {
                    o.ChangeSilently = true;
                    o.IsChecked = ControlOverlaySettings.HasFlag(o.Setting);
                    o.ChangeSilently = false;
                }
            }
        }

        public void DoMouseUp(MouseButtonEventArgs e)
        {
            PtzClickCancellationTokenSource?.Cancel();
            if (IsPtzActionActive)
            {
                // disable select/deselection when ptz dragging
                e.Handled = true;
                Owner.ClickableAreaButton.ReleaseMouseCapture();
            }

            IsMouseDown = false;
            IsPtzActionActive = false;
        }

        public void DoMouseDown(MouseButtonEventArgs e)
        {
            try
            {
                if (Owner.FloatingButtonControl.Model.IsConfigMode)
                {
                    Owner.FloatingButtonControl.Model.IsConfigMode = false;
                    return;
                }
            }
            catch (Exception exception)
            {
            }

            Owner.ClickableAreaButton.CaptureMouse();
            IsMouseDown = true;

            HandlePtzMouseDown(e);
        }

        private async void HandlePtzMouseDown(MouseButtonEventArgs e)
        {
            var videoComponent = VideoComponent;
            if (e.LeftButton == MouseButtonState.Pressed && videoComponent.IsLivePlayback && videoComponent.IsPlaying)
            {
                if (Owner.PtzOverlay is PtzOverlayCanvas ptzCanvas)
                {
                    PtzClickCancellationTokenSource?.Cancel();
                    PtzClickCancellationTokenSource = new CancellationTokenSource();
                    var token = PtzClickCancellationTokenSource.Token;
                    //Task.Run(async () =>
                    //{
                    try
                    {
                        await Task.Delay(220);
                        if (!token.IsCancellationRequested && IsMouseDown)
                        {
                            IsPtzActionActive = true;
                            //await ptzCanvas.WpfUiThreadAsync(async () =>
                            //{
                            ptzCanvas.StartingPoint = e.GetPosition(Owner);
                            Mouse.OverrideCursor = Cursors.None;
                            var renderCount = 0;
                            var isBoxPtz = ptzCanvas.Model.ControlType != EPtzControlType.Arrow;
                            while (IsMouseDown)
                            {
                                await Task.Delay(35);

                                //Owner.PtzOverlay.WpfUiThread(() =>
                                //{
                                var currentPoint =
                                    Owner.PointFromScreen(new Point(
                                        System.Windows.Forms.Control.MousePosition.X,
                                        System.Windows.Forms.Control.MousePosition.Y));
                                ptzCanvas.CurrentPoint = currentPoint;
                                ptzCanvas.InvalidateVisual();

                                //ptzCanvas.Model.ArrowLine.
                                if (!isBoxPtz)
                                {
                                    renderCount++;
                                    if (renderCount % 20 == 2)
                                    {
                                        var mainSize = Owner.MainGrid.RenderSize;
                                        var pointFromCenterX = Owner.MainGrid.RenderSize.Width / 2 -
                                            ptzCanvas.StartingPoint.X + currentPoint.X;
                                        var pointFromCenterY = Owner.MainGrid.RenderSize.Height / 2 -
                                            ptzCanvas.StartingPoint.Y + currentPoint.Y;
                                        var imageSize = videoComponent.GetImageBounds();
                                        var xOffset = (mainSize.Width -
                                                       imageSize.Width) / 2;
                                        var yOffset = (mainSize.Height -
                                                       imageSize.Height) / 2;

                                        var _ = Task.Run(() =>
                                        {
                                            try
                                            {
                                                var vector = ONVIFManager.GetCenterMoveVector(
                                                    (int) imageSize.Width, (int) imageSize.Height,
                                                    (int) (pointFromCenterX - xOffset),
                                                    (int) (pointFromCenterY - yOffset), 0);
                                                videoComponent?.PtzManager?.PanCamera(new Point(
                                                    Math.Max(-1, Math.Min(1, vector.PanTilt.x)) * 100,
                                                    Math.Max(-1, Math.Min(1, vector.PanTilt.y)) * 100));
                                            }
                                            catch (Exception exception)
                                            {
                                            }
                                        });
                                        //  Trace.WriteLine(ptzCanvas.Model.ArrowLine.ArrowAngle);(new Point(
                                        //(int) (ptzCanvas.StartingPoint.X - xOffset),
                                        //(int) (ptzCanvas.StartingPoint.Y - yOffset))


                                        // Trace.WriteLine($"{vector.PanTilt.x}x{vector.PanTilt.y}");
                                    }
                                }


                                //VideoComponent?.PtzManager?.MovePtzToPoint(
                                //    new Point(Math.Max(0, pointFromCenterX),
                                //        Math.Max(0, pointFromCenterY)),

                                //VideoComponent.PtzController?.MovePtzToPoint(
                                //    new Point(Math.Max(0, pointFromCenterX),
                                //        Math.Max(0, pointFromCenterY)),
                                //    new Size(_playerGrid.RenderSize.Width, _playerGrid.RenderSize.Height));
                                //}, DispatcherPriority.Render);
                            }

                            IsPtzActionActive = false;
                            Mouse.OverrideCursor = null;

                            if (ptzCanvas.CurrentPoint != new Point(-1, -1))
                            {
                                if (isBoxPtz)
                                {
                                    var mainSize = Owner.MainGrid.RenderSize;
                                    var imageSize = videoComponent.GetImageBounds();

                                    if (!imageSize.IsEmpty)
                                    {
                                        var xOffset = (mainSize.Width -
                                                       imageSize.Width) / 2;
                                        var yOffset = (mainSize.Height -
                                                       imageSize.Height) / 2;

                                        var distance =
                                            Math.Abs(ptzCanvas.StartingPoint.X - ptzCanvas.CurrentPoint.X) +
                                            Math.Abs(ptzCanvas.StartingPoint.Y - ptzCanvas.CurrentPoint.Y);
                                        {
                                            if (videoComponent.PtzManager is IPtzManager
                                                ptzController)
                                            {
                                                if (distance < 15)
                                                {
                                                    ptzController.PtzCenter(new Point(
                                                            (int) (ptzCanvas.StartingPoint.X - xOffset),
                                                            (int) (ptzCanvas.StartingPoint.Y - yOffset)),
                                                        (int) imageSize.Width, (int) imageSize.Height
                                                    );
                                                }
                                                else
                                                {
                                                    var widthMagnitude = Math.Sqrt(
                                                        Math.Pow(
                                                            ptzCanvas.CurrentPoint.X -
                                                            ptzCanvas.StartingPoint.X,
                                                            2) +
                                                        Math.Pow(
                                                            ptzCanvas.CurrentPoint.Y -
                                                            ptzCanvas.StartingPoint.Y,
                                                            2));

                                                    var zoomFromWidth =
                                                        imageSize.Width /
                                                        (float) widthMagnitude *
                                                        100; //GetPreviewResolution().Width / Math.Abs(PtzStartPoint.X - currentPoint.X));
                                                    ptzController.AreaZoom(new Point(
                                                            (int) (ptzCanvas.StartingPoint.X - xOffset),
                                                            (int) (ptzCanvas.StartingPoint.Y - yOffset))
                                                        ,
                                                        (int) zoomFromWidth, (int) imageSize.Height,
                                                        (int) imageSize.Width,
                                                        (int) ptzCanvas.DrawnRectangle.Height,
                                                        (int) ptzCanvas.DrawnRectangle.Width);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    videoComponent?.PtzManager?.PanCamera(new Point(0, 0));
                                }
                            }

                            ptzCanvas.CurrentPoint = new Point(-1, -1);
                            ptzCanvas.InvalidateVisual();
                            ptzCanvas.StartingPoint = new Point(-1, -1);
                            //});
                        }
                    }
                    catch
                    {
                    }

                    //}, token);
                }
            }
        }

        public void Dispose()
        {
            IsSendingKeepAlive = false;
            VideoComponent?.Dispose();
            _bitrateCheckCts?.Cancel();
            _directLiveCheckCts?.Cancel();
        }

        public async void Play()
        {
            Owner?.TryUiAsync(async () =>
                {
                    if (Owner.VideoObject != null)
                    {
                        if (!VideoComponent.IsPlaying)
                        {
                            HideSplashShowControls();
                            VideoComponent.Open(VideoComponent.LastOpenedUrl);
                            if (!VideoComponent.IsLivePlayback && ArchivePlaybackSessionToken.IsNotEmpty())
                            {
                                if (IsSendingKeepAlive)
                                {
                                    var universalTime = DateTime.SpecifyKind(_owner.EdgeVideoTimeline.CursorTime,
                                            DateTimeKind.Local)
                                        .ToUniversalTime();
                                    var resp = await GetManagerByZoneId(_owner.VideoObject.ZoneId).PlaybackService
                                        .ExecutePlayback(
                                            new ExecutePlayback
                                            {
                                                Action = EPlaybackActions.Play,
                                                ProfileId = "same",
                                                SessionToken = ArchivePlaybackSessionToken,
                                                Timestamp = universalTime
                                            });
                                    if (resp != null && resp.Success)
                                    {
                                    }
                                }
                            }
                        }
                        else
                        {
                            //VideoComponent.PlaybackState = EPlaybackState.Forward;
                            VideoComponent.PlaybackSpeed = EPlaybackSpeed.Speed1x;
                        }

                        VideoComponent?.Play();


                        await HandleRestartingArchive();
                    }
                }
            );
        }

        private void HideSplashShowControls()
        {
            Owner.LoadingSplash.Visibility = Visibility.Hidden;
            if (_previousOverlaySettings == EControlOverlaySettings.None)
            {
                _previousOverlaySettings = _defaultOverlaySettings;
            }

            ControlOverlaySettings = _previousOverlaySettings;
        }

        private async Task HandleRestartingArchive()
        {
            try
            {
                if (!VideoComponent.IsLivePlayback && ArchivePlaybackSessionToken.IsNotEmpty())
                {
                    if (IsSendingKeepAlive)
                    {
                        var playbackAction = EPlaybackActions.Forward1X;
                        var universalTime = DateTime
                            .SpecifyKind(_owner.EdgeVideoTimeline.CursorTime, DateTimeKind.Local)
                            .ToUniversalTime();
                        var isReverse = VideoComponent.PlaybackState == EPlaybackState.Reverse;
                        switch (VideoComponent.PlaybackSpeed)
                        {
                            case EPlaybackSpeed.Speed1x:
                                playbackAction = isReverse
                                    ? EPlaybackActions.Reverse1X
                                    : EPlaybackActions.Forward1X;
                                break;
                            case EPlaybackSpeed.Speed2x:
                                playbackAction = isReverse
                                    ? EPlaybackActions.Reverse2X
                                    : EPlaybackActions.Forward2X;
                                break;
                            case EPlaybackSpeed.Speed4x:
                                playbackAction = isReverse
                                    ? EPlaybackActions.Reverse4X
                                    : EPlaybackActions.Forward4X;
                                break;
                            case EPlaybackSpeed.Speed10x:
                                playbackAction = isReverse
                                    ? EPlaybackActions.Reverse10X
                                    : EPlaybackActions.Forward10X;
                                break;
                        }

                        var resp = await GetManagerByZoneId(_owner.VideoObject.ZoneId).PlaybackService.ExecutePlayback(
                                new ExecutePlayback
                                {
                                    Action = playbackAction,
                                    ProfileId = "same",
                                    SessionToken = ArchivePlaybackSessionToken,
                                    Timestamp = universalTime
                                }
                            )
                            ;
                        if (resp != null && resp.Success)
                        {
                            _log?.Info($"HandleRestartingArchive {playbackAction} {universalTime} {resp.Success}");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        public async void Pause()
        {
            VideoComponent?.Pause();
            try
            {
                if (!VideoComponent.IsLivePlayback && ArchivePlaybackSessionToken.IsNotEmpty())
                {
                    if (IsSendingKeepAlive)
                    {
                        var universalTime = DateTime
                            .SpecifyKind(_owner.EdgeVideoTimeline.CursorTime, DateTimeKind.Local)
                            .ToUniversalTime();
                        var resp = await GetManagerByZoneId(_owner.VideoObject.ZoneId).PlaybackService.ExecutePlayback(
                            new ExecutePlayback
                            {
                                Action = EPlaybackActions.Pause,
                                ProfileId = "same",
                                SessionToken = ArchivePlaybackSessionToken,
                                Timestamp = universalTime
                            });
                        if (resp != null && resp.Success)
                        {
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async void Stop()
        {
            Owner?.TryUiAsync(async () =>
            {
                IsFailedPlayback = false;
                VideoComponent?.Stop();
                if (VideoComponent != null)
                {
                    VideoComponent.PtzManager = null;
                    try
                    {
                        if (!VideoComponent.IsLivePlayback && ArchivePlaybackSessionToken.IsNotEmpty())
                        {
                            if (IsSendingKeepAlive)
                            {
                                var universalTime = DateTime
                                    .SpecifyKind(_owner.EdgeVideoTimeline.CursorTime, DateTimeKind.Local)
                                    .ToUniversalTime();
                                if (_owner.VideoObject != null)
                                {
                                    var resp = await GetManagerByZoneId(_owner.VideoObject.ZoneId).PlaybackService
                                        .ExecutePlayback(
                                            new ExecutePlayback
                                            {
                                                Action = EPlaybackActions.Stop,
                                                ProfileId = "same",
                                                SessionToken = ArchivePlaybackSessionToken,
                                                Timestamp = universalTime
                                            });
                                    if (resp != null && resp.Success)
                                    {
                                        IsSendingKeepAlive = false;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }


                Owner.VideoObject = null;
                Owner.CurrentLiveProfile = null;
                SelectedRecordingProfile = null;
                
                if (Owner.LoadingSplash.Visibility != Visibility.Visible)
                {
                    Owner.LoadingSplash.Visibility = Visibility.Visible;
                }

                _previousOverlaySettings = ControlOverlaySettings;
                ControlOverlaySettings = EControlOverlaySettings.None;
                ClearPlaybackTokens();
            });
        }


        public void Open(string url)
        {
            Application.Current?.Dispatcher?.InvokeAsync(() =>
            {
                try
                {
                    VideoComponent.LastOpenedUrl = url;
                    VideoComponent?.Open(url);
                }
                catch (Exception e)
                {
                    Trace.WriteLine($"Error Opening Stream... {e.GetType()} : {e.Message}");
                }
            });
        }

        public void NextEvent(ITimelineEvent type)
        {
            VideoComponent?.NextEvent(type);
        }

        public void PreviousEvent(ITimelineEvent type)
        {
            VideoComponent?.PreviousEvent(type);
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void PlayLive(bool fallbackToDirect = true)
        {
            _log?.Info("Play Live ...");
            Application.Current?.Dispatcher?.InvokeAsync(async () =>
            {
                if (Owner != null)
                {
                    IsUnableToFindStream = false;
                    try
                    {
                        var cameraModel = Owner.VideoObject;

                        if (cameraModel != null)
                        {
                            if (Owner.VideoComponent != null)
                            {
                                Owner.VideoComponent.IsLivePlayback = true;
                            }

                            if (VideoComponent.PlayerState != EPlayerState.Stopped &&
                                (DirectModeManager.IsDirectMode || IsFailedPlayback))
                            {
                                _ = GetAdditionalCameraInformation(cameraModel).ConfigureAwait(false);
                                HideSplashShowControls();
                                VideoComponent.IsStartingPlayback = true;
                                if (cameraModel.CurrentOnvifProfile == null)
                                {
                                    try
                                    {
                                        await OnvifProfileItemMgr.RefreshProfiles(
                                            new ItemManagerRequestParameters {RequestDirect = true}, cameraModel);
                                        var baseAdapterCameraProfileItemModel =
                                            OnvifProfileItemMgr.GetNearestOnvifProfileForResolution(cameraModel.Id,
                                                Owner.ActualWidth);
                                        if (baseAdapterCameraProfileItemModel == null)
                                        {
                                            if (!VideoComponent.IsPlaying)
                                            {
                                                Owner.Model.IsUnableToFindStream = true;
                                            }

                                            VideoComponent.IsStartingPlayback = false;
                                            IsDirectPlayback = true;
                                            IsFailedPlayback = false;
                                            return;
                                        }

                                        cameraModel.CurrentOnvifProfile = baseAdapterCameraProfileItemModel;
                                        cameraModel.LiveStreamUrl = baseAdapterCameraProfileItemModel.StreamUri;
                                    }
                                    catch (Exception e)
                                    {
                                    }
                                }

                                var url = cameraModel?.CameraUrl;
                                try
                                {
                                    if  (cameraModel.CameraUrl != null && cameraModel.CameraUrl.Contains(
                                        "rtsp://") && !cameraModel.CameraUrl.Contains(
                                        $"rtsp://{cameraModel.Username}:{cameraModel.Password}@"))
                                    {
                                        url = url.Replace("rtsp://",
                                            $"rtsp://{cameraModel.Username}:{cameraModel.Password}@");
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }

                               
                                Owner.Model?.Open(url);
                                Owner.Model?.Play();
                                VideoComponent.IsStartingPlayback = false;
                                IsDirectPlayback = true;
                                IsFailedPlayback = false;
                            }
                            else
                            {
                                try
                                {
                                    var sw = new Stopwatch();
                                    sw.Start();
                                    HideSplashShowControls();
                                    VideoComponent.IsStartingPlayback = fallbackToDirect || !IsDirectPlayback;
                                    var ownerCurrentLiveProfile =
                                        Owner.CurrentLiveProfile ??
                                        _streamStreamProfileMgr.GetNearestStreamProfileForResolution(
                                            cameraModel, Owner.ActualWidth);

                                    if (ownerCurrentLiveProfile == null)
                                    {
                                        await _streamStreamProfileMgr.RefreshProfiles(cameraModel,
                                            new ItemManagerRequestParameters
                                                {AcceptedCacheDuration = TimeSpan.FromSeconds(5)});
                                        ownerCurrentLiveProfile =
                                            Owner.CurrentLiveProfile ??
                                            _streamStreamProfileMgr.GetNearestStreamProfileForResolution(
                                                cameraModel, Owner.ActualWidth);

                                        if (ownerCurrentLiveProfile == null)
                                        {
                                            //if (fallbackToDirect)
                                            {
                                                if (DateTime.UtcNow - TimeSpan.FromSeconds(30) >
                                                    LiveFallbackFailureTime)
                                                {
                                                    IsFailedPlayback = true;
                                                    PlayLive();
                                                    LiveFallbackFailureTime = DateTime.UtcNow;
                                                }
                                            }
                                        }
                                    }

                                    if (ownerCurrentLiveProfile != null)
                                    {
                                        Owner.CurrentLiveProfile = ownerCurrentLiveProfile;
                                        try
                                        {
                                            var videoObjectZoneId = cameraModel.ZoneId;
                                            var sw1 = new Stopwatch();
                                            sw1.Start();
                                            _ = GetAdditionalCameraInformation(cameraModel).ConfigureAwait(false);
                                            
                                            var resp = await GetManagerByZoneId(videoObjectZoneId)
                                                .StreamProfileService.GetStreamingProfileSession(
                                                    new GetStreamingProfileSession
                                                    {
                                                        IsStatic = false,
                                                        StreamingProfileId = ownerCurrentLiveProfile.Id,
                                                        ZoneId = ownerCurrentLiveProfile.ZoneId
                                                    });

                                            Trace.WriteLine(
                                                $"Live Stream RTSP Url Request took {sw1.ElapsedMilliseconds}ms");

                                            if (resp != null && resp.ResponseStatus.ErrorCode == null)
                                            {
                                                sw.Stop();
                                                Trace.WriteLine(
                                                    $"GOT RTSP FROM API {resp.RtspUrl} {sw.ElapsedMilliseconds}ms elapsed");
                                                var rtspUrl = resp.RtspUrl;

                                               

                                                Owner.Model?.Open(rtspUrl);
                                                Owner.Model?.Play();

                                                if (VideoComponent is MPVVideoComponent mpv)
                                                {
                                                    Owner.ClearPlaybackMembers(mpv);

                                                    void MediaLoaded()
                                                    {
                                                        mpv.MediaLoaded -= MediaLoaded;
                                                        _fallbackLiveToDirectCts = new CancellationTokenSource();
                                                        var cts = _fallbackLiveToDirectCts;
                                                        Task.Run(async () =>
                                                        {
                                                            try
                                                            {
                                                                var cameraDirectPlaybackFailureTimeout =
                                                                    SettingsManager.Settings?.Camera
                                                                        ?.DirectPlaybackFailureTimeout;
                                                                var failureTimeout =
                                                                    TimeSpan.FromSeconds(
                                                                        cameraDirectPlaybackFailureTimeout ?? 3);
                                                                await Task.Delay(
                                                                    failureTimeout);
                                                                if (!cts.IsCancellationRequested)
                                                                {
                                                                    var cameraIsDirectOnFailedPlaybackEnabled =
                                                                        SettingsManager.Settings?.Camera
                                                                            ?.IsDirectOnFailedPlaybackEnabled ??
                                                                        false;
                                                                    if (cameraIsDirectOnFailedPlaybackEnabled &&
                                                                        IsMediaLoaded &&
                                                                        mpv.PlayerState == EPlayerState.Playing &&
                                                                        mpv.IsLivePlayback &&
                                                                        mpv.VideoView.TimePosition == 0 &&
                                                                        mpv.VideoView.VideoBitrate == 0)
                                                                    {
                                                                        IsFailedPlayback = true;
                                                                        PlayLive();
                                                                    }
                                                                }
                                                            }
                                                            catch (Exception e)
                                                            {
                                                            }
                                                        }, cts.Token);
                                                    }

                                                    mpv.MediaLoaded += MediaLoaded;
                                                }
                                            }
                                            else if (fallbackToDirect)
                                            {
                                                if (resp != null && resp.ResponseStatus.ErrorCode != null)
                                                {
                                                    IsRetryingServerAfterUnload = true;
                                                    var _ = Task.Run(async () =>
                                                    {
                                                        await Application.Current.Dispatcher.InvokeAsync(async () =>
                                                        {
                                                            await Task.Delay(10000);
                                                            if (IsRetryingServerAfterUnload &&
                                                                VideoComponent.IsLivePlayback)
                                                            {
                                                                PlayLive();
                                                            }
                                                        });
                                                    });
                                                }

                                                sw.Stop();
                                                Trace.WriteLine(
                                                    $" RTSP PLAYBACK API RESPONSE WAS NULL (FAILING OVER TO DIRECT) {sw.ElapsedMilliseconds}ms elapsed");
                                                IsFailedPlayback = true;
                                                PlayLive();
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            sw.Stop();
                                            Trace.WriteLine(
                                                $"EXCEPTION FROM RTSP PLAYBACK (FAILING OVER TO DIRECT) {e.GetType()} {e.Message} {sw.ElapsedMilliseconds}ms elapsed");
                                            IsFailedPlayback = true;
                                            PlayLive();
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    _log?.Info($"Starting LivePlayback failed {e.Message}");
                                }
                                finally
                                {
                                    VideoComponent.IsStartingPlayback = false;
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        _log?.Info(e);
                    }
                }
            });
        }

        public static async Task GetAdditionalCameraInformation(params CameraItemModel[] cameras)
        {
            try
            {
                var obj = cameras.ToList();

                using var cts = new CancellationTokenSource(30000);

                await Task.Run(async () =>
                {
                    try
                    {
                        var cameraItemModel = cameras.FirstOrDefault();
                        await StreamProfileItemManager.Manager.RefreshGlobalProfiles(cameraItemModel.Id,
                            cameraItemModel.ZoneId).ConfigureAwait(false);

                        await RecordingProfileItemManager.Manager.RefreshGlobalProfiles(cameraItemModel.Id,
                            cameraItemModel.ZoneId).ConfigureAwait(false);
                      //  await ServerItemManager.GetManagerByZoneIdConnectIfFail(Owner.VideoObject.ZoneId);
                        await CameraItemHelper.LoadRecordingProfilesBatch(obj).ConfigureAwait(false);
                    }
                    catch (Exception e)
                    {
                    }

                    try
                    {
                        await CameraItemHelper.LoadCameraStatus(obj).ConfigureAwait(false);
                    }
                    catch (Exception e)
                    {
                    }

                    try
                    {
                        await CameraItemHelper.LoadOnvifProfiles(obj).ConfigureAwait(false);
                    }
                    catch (Exception e)
                    {
                    }

                    //await GetONVIFProfiles(cameras);
                    try
                    {
                        await CameraItemHelper.LoadPresetsBatch(obj).ConfigureAwait(false);
                    }
                    catch (Exception e)
                    {
                    }
                }, cts.Token).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
                //_log?.Info(e);
            }
        }

        public async Task<bool> RequestRecording(DateTime cursorTime)
        {
            if (!IsRequestingRecording)
            {
                IsRequestingRecording = true;
                var response = await Task.Run(async () =>
                {
                    try
                    {
                        if (cursorTime >= DateTime.Now - TimeSpan.FromSeconds(2))
                        {
                            PlayLive();
                            return true;
                        }

                        try
                        {
                            if (_owner.VideoObject != null)
                            {
                                if (SelectedRecordingProfile != null &&
                                    SelectedRecordingProfile.CameraId != _owner.VideoObject.Id)
                                {
                                    SelectedRecordingProfile = null;
                                }

                                var recordingProfile =
                                    SelectedRecordingProfile ?? _owner.VideoObject.RecordingProfileItemMgr
                                        .ItemCollection.Values.Where(o => o.CameraId == _owner.VideoObject.Id)
                                        ?.Where(o => o.IsEnabled).OrderByDescending(o => o.Height)
                                        .FirstOrDefault();
                                if (recordingProfile != null)
                                {
                                    SelectedRecordingProfile = recordingProfile;
                                    if (ServiceManagers.ContainsKey(_owner.VideoObject.ZoneId))
                                    {
                                        await Application.Current.Dispatcher.InvokeAsync(async () =>
                                        {
                                            if (GetMpvVideoComponent.IsLivePlayback)
                                            {
                                                GetMpvVideoComponent.VideoView.Stop();
                                            }

                                            VideoComponent.IsStartingPlayback = true;
                                            VideoComponent.IsLivePlayback = false;
                                            var universalTime = DateTime.SpecifyKind(cursorTime, DateTimeKind.Local)
                                                .ToUniversalTime();

                                            var seekSuccess = false;
                                            var managerByZoneId = GetManagerByZoneId(_owner.VideoObject.ZoneId);
                                            if (ArchivePlaybackSessionToken.IsNotEmpty())
                                            {
                                                var requestSeek = await managerByZoneId
                                                    .PlaybackService
                                                    .ExecutePlayback(new ExecutePlayback
                                                    {
                                                        Action = EPlaybackActions.Seek,
                                                        ProfileId = recordingProfile.Id.ToString(),
                                                        Timestamp = universalTime,
                                                        SessionToken = ArchivePlaybackSessionToken
                                                    });
                                                if (requestSeek != null)
                                                {
                                                    if (VideoComponent.LastOpenedUrl != ActiveArchivePlaybackRtspUrl)
                                                    {
                                                        Open(ActiveArchivePlaybackRtspUrl);
                                                        Play();
                                                        VideoComponent.Play();
                                                    }


                                                    seekSuccess = requestSeek.Success;
                                                }

                                                VideoComponent.IsStartingPlayback = false;
                                            }

                                            if (!seekSuccess)
                                            {
                                                // ArchivePlaybackSessionToken = string.Empty;
                                                IsSendingKeepAlive = false;
                                                var requestStream = await managerByZoneId
                                                    .PlaybackService
                                                    .CreatePlayback(new CreatePlayback
                                                    {
                                                        RecordingProfileId = recordingProfile.Id,
                                                        Timestamp = universalTime,
                                                        Action = _owner.IsPlaying
                                                            ? EPlaybackActions.Play
                                                            : EPlaybackActions.Pause
                                                    });
                                                if (requestStream != null && requestStream.RtspUrl.IsNotEmpty())
                                                {
                                                    _log?.Info(
                                                        $"RequestRTSPArchive Returned: {requestStream.RtspUrl}");
                                                    ArchivePlaybackSessionToken = requestStream.SessionToken;
                                                    ActiveArchivePlaybackRtspUrl = requestStream.RtspUrl;
                                                    Open(requestStream.RtspUrl);
                                                    Clipboard.SetText(requestStream.RtspUrl);
                                                    Play();
                                                    IsSendingKeepAlive = true;
                                                    VideoComponent.IsStartingPlayback = false;
                                                }
                                                else
                                                {
                                                    _log?.Info("RequestRTSPArchive Errored or Timedout");
                                                    ArchivePlaybackSessionToken = string.Empty;
                                                    IsSendingKeepAlive = false;
                                                    VideoComponent.IsStartingPlayback = false;
                                                }
                                            }
                                        });
                                        return IsSendingKeepAlive;
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            _log?.Info(e);
                            return false;
                        }


                        //cursorTime = cursorTime.ToLocalTime();

                        //local file playback...
                        //foreach (var profile in VideoObject.RecordingProfiles)
                        //{
                        //    cursorTime = DateTime.SpecifyKind(cursorTime, DateTimeKind.Local);
                        //    var path = $"D:\\Video\\{profile.Id}\\{cursorTime.ToUniversalTime().ToString("yyyyMMdd")}";
                        //    if (Directory.Exists(path))
                        //    {
                        //        var closestLocalFile = Directory.GetFiles(path)
                        //            .FirstOrDefault(x =>
                        //                new FileInfo(x).CreationTime <= cursorTime &&
                        //                new FileInfo(x).CreationTime.AddMinutes(1) >= cursorTime);
                        //        if (closestLocalFile != null)
                        //        {
                        //            var createTime = new FileInfo(closestLocalFile).CreationTime;
                        //            //Console.WriteLine($"{closestLocalFile}");
                        //            Model?.Open(closestLocalFile);
                        //            Model?.Play();
                        //            var position = cursorTime.Subtract(createTime);
                        //            VideoComponent?.JumpToPosition(position);
                        //            return;
                        //        }
                        //    }
                        //}
                        return false;
                    }
                    catch (Exception e)
                    {
                        _log?.Info(e);
                        return false;
                    }
                });

                IsRequestingRecording = false;
                return response;
            }

            return false;
        }

        private async Task ReconnectArchive(int retryCount = 15)
        {
            var resp = false;
            var sw = new Stopwatch();
            IsSendingKeepAlive = false;
            sw.Start();
            for (var i = retryCount; i > 0 && !resp && !VideoComponent.IsLivePlayback; i--)
            {
                if (!resp)
                {
                    await Task.Delay(2000);

                    resp = await RequestRecording(Owner.EdgeVideoTimeline.CursorTime);
                    //_log?.Info($"ReconnectArchive {resp}");
                }
            }

            sw.Stop();
            _log?.Info($"ReconnectArchive took {sw.ElapsedMilliseconds}ms to return {resp}");
            IsReconnecting = false;
        }

        public void ClearPlaybackTokens()
        {
            _fallbackLiveToDirectCts?.Cancel();
            _bitrateCheckCts?.Cancel();
            _directLiveCheckCts?.Cancel();
        }

        private void MpvOnMediaPaused()
        {
            Trace.WriteLine("MpvOnMediaPaused");
        }

        private void MpvOnMediaResumed()
        {
            Trace.WriteLine("MpvOnMediaResumed");
        }

        private void MpvOnMediaStartedSeeking()
        {
            //   Trace.WriteLine("MpvOnMediaStartedSeeking");
        }

        private void MpvOnMediaEndedBuffering()
        {
            //Trace.WriteLine("MpvOnMediaEndedBuffering");
        }

        private void MpvOnMediaStartedBuffering()
        {
            //  Trace.WriteLine("MpvOnMediaStartedBuffering");
        }

        private void MpvOnMediaEndedSeeking()
        {
            //Trace.WriteLine("MpvOnMediaEndedSeeking");
        }

        private void VideoComponentOnMediaPlayerError()
        {
            //Trace.WriteLine("!!!!!VideoComponentOnMediaPlayerError");
        }

        private void MpvOnMediaUnloaded()
        {
            IsMediaLoaded = false;
            //Trace.WriteLine("MpvOnMediaUnloaded");
        }

        private void MpvOnMediaLoaded()
        {
            IsMediaLoaded = true;
            try
            {
                if (VideoComponent.PlayerState == EPlayerState.Stopped)
                {
                    Stop();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            // Trace.WriteLine("MpvOnMediaLoaded");
        }

        private void MpvOnMediaPlayerError()
        {
            //Trace.WriteLine("MpvOnMediaPlayerError");
        }

        private void MpvOnMediaPlayerEnded()
        {
            //Trace.WriteLine("MpvOnMediaPlayerEnded");
            if (!IsRetryingUnloaded)
            {
                IsRetryingUnloaded = true;

                Action mpvOnMediaLoaded(MPVVideoComponent mpv, TaskCompletionSource<bool> tcs)
                {
                    return () =>
                    {
                        try
                        {
                            mpv.IsStartingPlayback = false;
                            tcs.TrySetResult(false);
                            IsRetryingUnloaded = false;
                        }
                        catch (Exception e)
                        {
                        }
                        finally
                        {
                            mpv.MediaLoaded -= mpvOnMediaLoaded(mpv, tcs);
                        }
                    };
                }

                var tcs = new TaskCompletionSource<bool>();
                
                Application.Current.Dispatcher?.InvokeAsync(() =>
                {
                    if (VideoComponent.PlayerState != EPlayerState.Stopped && VideoComponent is MPVVideoComponent mpv)
                    {
                        Task.Run(async () =>
                        {
                            mpv.IsStartingPlayback = true;
                            if (mpv.IsLivePlayback)
                            {
                                PlayLive();
                            }

                            mpv.MediaLoaded += mpvOnMediaLoaded(mpv, tcs);

                            await Task.Delay(15000);
                            tcs.TrySetCanceled();
                            IsRetryingUnloaded = false;
                        });
                    }
                });
            }
        }

        private void OnDirectModeChanged(bool directMode)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                //   var wasPlaying = VideoComponent.IsPlaying;
                Owner?.UpdateVideoObject();
                if (directMode)
                {
                    //IsDirectPlayback = true;
                    using var cts = new CancellationTokenSource(15000);
                    try
                    {
                        Application.Current.Dispatcher.InvokeAsync(async () =>
                        {
                            try
                            {
                                var onvifCameraConnectionItemModel =
                                    (VideoComponent.PtzManager as ONVIFManager)?.OnvifCameraConnection;
                                while (onvifCameraConnectionItemModel?.DefaultStreamUri == null)
                                {
                                    await Task.Delay(200);
                                }

                                var profileForOnvif =
                                    OnvifProfileItemMgr.GetNearestOnvifProfileForResolution(Owner.VideoObject.Id,
                                        Owner.ActualWidth);
                                Owner.VideoObject.CurrentOnvifProfile = profileForOnvif;
                                Owner.VideoObject.LiveStreamUrl = profileForOnvif.StreamUri;
                                Open(profileForOnvif?.StreamUri);
                                Trace.WriteLine($"Opening Direct Url {profileForOnvif?.StreamUri}");
                                PlayLive();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                throw;
                            }
                        }, DispatcherPriority.Background, cts.Token);
                    }
                    catch (Exception e)
                    {
                    }
                }
                else
                {
                    if (VideoComponent.IsLivePlayback)
                    {
                        PlayLive();
                    }
                }
            });
        }


        private async void VideoComponentOnMediaPlayerEnded()
        {
            //Trace.WriteLine("!!!!!VideoComponentOnMediaPlayerEnded");
            await Application.Current.Dispatcher.InvokeAsync(async () =>
            {
                if (IsSendingKeepAlive && VideoComponent.LastOpenedUrl.IsNotEmpty() && !IsReconnecting)
                {
                    IsReconnecting = true;
                    await ReconnectArchive();
                }
            });
        }


        private async void VideoComponentOnPlaybackStateChanged(EPlaybackState state)
        {
            switch (state)
            {
                case EPlaybackState.None:
                    break;
                case EPlaybackState.Live:
                    break;
                case EPlaybackState.Archive:
                    break;
                case EPlaybackState.Forward:
                    await HandleRestartingArchive();
                    break;
                case EPlaybackState.Reverse:
                    await HandleRestartingArchive();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        private async void VideoComponentOnPlaybackSpeedChanged(EPlaybackSpeed speed)
        {
            switch (speed)
            {
                case EPlaybackSpeed.Speed1x:
                case EPlaybackSpeed.Speed2x:
                case EPlaybackSpeed.Speed4x:
                case EPlaybackSpeed.Speed8x:
                case EPlaybackSpeed.Speed10x:
                case EPlaybackSpeed.Speed20x:
                case EPlaybackSpeed.Speed40x:
                case EPlaybackSpeed.Speed80x:
                case EPlaybackSpeed.Speed200x:
                    await HandleRestartingArchive();
                    break;
            }
        }

        private void OverlaySettingOnFlagChanged(EControlOverlaySettings setting, bool state)
        {
            if (setting == EControlOverlaySettings.All)
            {
                foreach (var flag in OverlayOptionsCollection.Where(o =>
                    o.Setting != setting))
                {
                    flag.IsChecked = state;
                }
            }

            ControlOverlaySettings ^= setting;
        }

        public void OnSizeChanged(object sender, SizeChangedEventArgs sizeChangedEventArgs)
        {
            try
            {
                if (Owner != null && Owner.IsVisible && Owner.IsAutoSelectStream && Owner.VideoObject != null)
                {
                    if (DirectModeManager.IsDirectMode)
                    {
                        var profile =
                            OnvifProfileItemMgr.GetNearestOnvifProfileForResolution(Owner.VideoObject.Id,
                                Owner.ActualWidth);
                        if (profile != null && profile != Owner.VideoObject.CurrentOnvifProfile)
                        {
                            Owner.VideoObject.CurrentOnvifProfile = profile;
                            Owner.VideoObject.LiveStreamUrl = profile.StreamUri;
                            Owner.VideoObject.CameraUrl = profile.StreamUri;
                            Owner.VideoComponent.LastOpenedUrl = string.Empty;
                            PlayLive();
                        }
                    }
                    else
                    {
                        if (!VideoComponent.IsLivePlayback)
                        {
                            var profile =
                                RecordingProfileItemManager.Manager.GetNearestRecordingProfileForResolution(
                                    Owner.VideoObject,
                                    Owner.ActualWidth);
                            if (profile != null && profile != SelectedRecordingProfile)
                            {
                                SelectedRecordingProfile = profile;
                            }
                        }
                        else
                        {
                            var profile =
                                _streamStreamProfileMgr.GetNearestStreamProfileForResolution(Owner.VideoObject,
                                    Owner.ActualWidth);
                            if (profile != null && profile != Owner.CurrentLiveProfile)
                            {
                                Owner.CurrentLiveProfile = profile;
                                Owner.VideoComponent.LastOpenedUrl = string.Empty;
                                PlayLive();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _log?.Error($"{e.GetType()} {e.Message}");
            }
        }
    }
}