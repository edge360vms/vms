﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using CefSharp;
using CefSharp.MinimalExample.Wpf.Behaviours;
using Edge360.Platform.VMS.Client.ItemModel.Camera;

namespace Edge360.Platform.VMS.Client.View.Control.Web
{
    /// <summary>
    ///     Interaction logic for WebView.xaml
    /// </summary>
    public partial class WebCameraView : UserControl, IView, IDisposable
    {
        public WebCameraView()
        {
            InitializeComponent();
            BrowserRequestHandler = new AuthorizationHandler("","");
            Browser.RequestHandler = BrowserRequestHandler;
            if (Model != null)
            {
                Model.CameraChanged += ModelOnCameraChanged;
                IsVisibleChanged += OnIsVisibleChanged;
            }

            // DataContext = new WebViewModel(this);
        }

        public bool HasNextVisibleRefresh { get; set; }

        private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (IsVisible && HasNextVisibleRefresh)
            {
                HasNextVisibleRefresh = false;
                RefreshPage(this);
            }
        }

        public AuthorizationHandler BrowserRequestHandler { get; set; }

        private void ModelOnCameraChanged(CameraItemModel obj)
        {
            if (obj != null)
            {
                BrowserRequestHandler.UserName = obj.Username;
                BrowserRequestHandler.Password = obj.Password;
            }
        }

        public static readonly DependencyProperty CameraProperty = DependencyProperty.Register(
            "Camera", typeof(CameraItemModel), typeof(WebCameraView), new PropertyMetadata(default(CameraItemModel), PropertyChangedCallback));

        public WebCameraViewModel Model => DataContext as WebCameraViewModel;

        private static async void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is WebCameraView view)
            {
                //view.Browser.Address = "";
                if (view.IsVisible)
                {
                    await RefreshPage(view);
                }
                else
                {
                    try
                    {
                        view.Browser.GetMainFrame().LoadHtml("<html><body><b>Loading</b></body></html>");
                    }
                    catch (Exception exception)
                    {
                    }
                    view.HasNextVisibleRefresh = true;
                }
                view.Model.SelectedCamera = view.Camera;
            }
        }

        private static async Task RefreshPage(WebCameraView view)
        {
            if (view.Browser.IsBrowserInitialized)
            {
                view.Browser.GetMainFrame().LoadHtml("<html><body><b>Loading</b></body></html>");
                await Task.Delay(1000);
                view.Browser.GetMainFrame().LoadUrl($"http://{view?.Camera?.Address}/");
            }
            else
            {
                view.Browser.Address = $"http://{view?.Camera?.Address}/";
            }
        }

        public CameraItemModel Camera
        {
            get { return (CameraItemModel) GetValue(CameraProperty); }
            set { SetValue(CameraProperty, value); }
        }

        //private void ChromiumWebBrowser_OnFrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        //{
        //    Dispatcher?.BeginInvoke((Action) (() =>
        //    {
        //        //AddressBox.Text = e.Url;
        //        //_log?.Info($"CanGoBack {Browser.CanGoBack}");
        //        //BtnBack.IsEnabled = Browser.CanGoBack;
        //        //BtnForward.IsEnabled = Browser.CanGoForward;
        //        //// BtnRefresh.IsEnabled = !string.IsNullOrWhiteSpace(AddressBox.Text); //BtnNavigate.IsEnabled  =
        //    }));
        //}
        public void Dispose()
        {
            Browser?.Dispose();
        }
    }
}