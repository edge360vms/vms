﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Camera;

namespace Edge360.Platform.VMS.Client.View.Control.Web
{
    public class WebCameraViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private CameraItemModel _selectedCamera;

        public event Action<CameraItemModel> CameraChanged; 

        public CameraItemModel SelectedCamera
        {
            get => _selectedCamera;
            set
            {
                if (Equals(value, _selectedCamera)) return;
                _selectedCamera = value;
                CameraChanged?.Invoke(value);
                OnPropertyChanged();
            }
        }
        //private ICommand _navigateAddressCommand;
        //private ICommand _selectAllAddressCommand;
        //private WebView _view;
        //public ICommand BackCommand => View?.Browser?.BackCommand;
        //public bool? CanGoBack => View?.Browser?.CanGoBack;
        //public bool? CanGoForward => View?.Browser?.CanGoForward;

        //public ICommand ForwardCommand => View?.Browser?.ForwardCommand;

        //public ICommand NavigateAddressCommand
        //{
        //    get
        //    {
        //        if (_navigateAddressCommand == null)
        //        {
        //            _navigateAddressCommand = new RelayCommand(p => NavigateAddress(), p => CanNavigate());
        //        }

        //        return _navigateAddressCommand;
        //    }
        //}

        //public ICommand ReloadCommand => View?.Browser?.ReloadCommand;

        //public ICommand SelectAllAddressCommand
        //{
        //    get
        //    {
        //        if (_selectAllAddressCommand == null)
        //        {
        //            _selectAllAddressCommand = new RelayCommand(p => SelectAllAddress(), p => CanSelectAllAddress());
        //        }

        //        return _selectAllAddressCommand;
        //    }
        //}

        //public WebView View
        //{
        //    get => _view;
        //    set
        //    {
        //        if (_view != value)
        //        {
        //            _view = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}


        //public WebViewModel(WebView webView)
        //{
        //    View = webView;
        //}

        //public event PropertyChangedEventHandler PropertyChanged;

        //private void SelectAllAddress()
        //{
        //    View?.AddressBox?.Focus();
        //    View?.AddressBox?.SelectAll();
        //}

        //private bool CanSelectAllAddress()
        //{
        //    return true;
        //}

        //private void NavigateAddress()
        //{
        //    if (!string.IsNullOrWhiteSpace(View.AddressBox.Text))
        //    {
        //        View.Browser.Address = View.AddressBox.Text;
        //    }
        //}

        //private bool CanNavigate()
        //{
        //    return true;
        //}

        //[NotifyPropertyChangedInvocator]
        //protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}