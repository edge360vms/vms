﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.View.Control.Dialog.FilterDialog.View;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using WispFramework.Extensions.Linq;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog.ObjectResolverDialog.ViewModel
{
    public class ObjectResolverViewModel<T> : INotifyPropertyChanged
    {
        private readonly ConcurrentDictionary<int, Task> ActiveTasks = new ConcurrentDictionary<int, Task>();
        private List<T> _fullList = new List<T>();
        private string _searchQuery;

        public ICommand CancelFilterOperation => new RelayCommand(o => { CancelCurrentOperation(); });

        public ICommand ClearFilterCommand => new RelayCommand(o =>
        {
            CancelCurrentOperation();
            PropertyList.Clear();
        });

        public List<T> FullList
        {
            get => _fullList;
            set
            {
                _fullList = value;

                UpdateItemsCollection();
                OnPropertyChanged();
            }
        }

        public ObservableCollection<T> ItemsCollection { get; set; } = new ObservableCollection<T>();

        public string SearchQuery
        {
            get => _searchQuery;
            set
            {
                _searchQuery = value;

                UpdateItemsCollection();
                OnPropertyChanged();
            }
        }

        public ICommand ShowFilterDialogCommand =>
            new RelayCommand(o =>
            {
                var fd = new FilterDialogView();
                if (fd.ShowDialog<T>(PropertyList) != true)
                {
                    return;
                }

                PropertyList = fd.FilterItems;
                UpdateItemsCollection();
            });

        public bool TasksActive => ActiveTasks.Any();

        private CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        private string ParsedSearchQuery => SearchQuery.Replace("_", "");

        private List<PropertyInfo> PropertyList { get; set; } = new List<PropertyInfo>();

        public event PropertyChangedEventHandler PropertyChanged;

        public void ApplyFilter()
        {
            CancelCurrentOperation();

            ItemsCollection.Clear();

            // chunk our list into groups of 100 and evaluate within parallel tasks
            var chunkedList = FullList.ChunkBy(100).ToArray();

            try
            {
                for (var i = 0; i < chunkedList.Count(); i++)
                {
                    var chunk = chunkedList[i];
                    var index = i;
                    var t = Task.Run(() =>
                    {
                        var filteredItems = chunk.Where(MatchesFilter);
                        SafeAddItemsToList(filteredItems);
                        ActiveTasks.TryRemove(index, out _);
                        OnPropertyChanged(nameof(TasksActive));
                    }, CancellationTokenSource.Token);
                    ActiveTasks.TryAdd(i, t);
                    OnPropertyChanged(nameof(TasksActive));
                }
            }
            catch (OperationCanceledException)
            {
            }
        }

        public bool MatchesFilter(T o)
        {
            var sq = ParsedSearchQuery;
            if (PropertyList.Count == 0)
            {
                var t = o.ToString();
                return t.Contains(sq);
            }

            return PropertyList.Select(prop => prop.GetValue(o)).Any(t => t != null && t.ToString().Contains(sq));
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void CancelCurrentOperation()
        {
            try
            {
                CancellationTokenSource.Cancel(true);
                CancellationTokenSource = new CancellationTokenSource();
                ActiveTasks.Clear();
                OnPropertyChanged(nameof(TasksActive));
            }
            catch (OperationCanceledException)
            {
            }
        }

        private void SafeAddItemsToList(IEnumerable<T> items)
        {
            Application.Current.Dispatcher.InvokeAsync(() =>
            {
                foreach (var v in items)
                {
                    ItemsCollection.Add(v);
                }
            });
        }

        private void UpdateItemsCollection()
        {
            if (string.IsNullOrEmpty(SearchQuery))
            {
                ItemsCollection.Clear();
                foreach (var item in FullList)
                {
                    ItemsCollection.Add(item);
                }
            }
            else
            {
                ApplyFilter();
            }
        }
    }
}