﻿using System.Windows;
using Edge360.Platform.VMS.Client.View.Control.Dialog.ObjectResolverDialog.Attributes;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog.ObjectResolverDialog.ObjectViews
{
    /// <summary>
    ///     Interaction logic for VolumeView.xaml
    /// </summary>
    [DescriptorDrawer]
    public partial class VolumeItemModelDrawer : ResourceDictionary
    {
        public VolumeItemModelDrawer()
        {
            InitializeComponent();
        }
    }
}