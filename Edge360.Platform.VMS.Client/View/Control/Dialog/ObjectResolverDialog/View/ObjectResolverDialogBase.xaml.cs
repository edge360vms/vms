﻿using System.Windows;
using Edge360.Platform.VMS.Client.Automation.Resources.Extensions;
using Edge360.Platform.VMS.Client.View.Control.Dialog.ObjectResolverDialog.Attributes;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog.ObjectResolverDialog.View
{
    /// <summary>
    ///     Interaction logic for Window1.xaml
    /// </summary>
    public partial class ObjectResolverDialogBase
    {
        public ObjectResolverDialogBase()
        {
            InitializeComponent();
            this.LoadResourceDictionaryByAttribute<DescriptorDrawerAttribute>();
        }

        private void Ok_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}