﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Client.Caches.Interfaces;
using Edge360.Platform.VMS.Client.View.Control.Dialog.ObjectResolverDialog.ViewModel;
using ServiceStack;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog.ObjectResolverDialog.View
{
    public class ObjectResolverDialog<TCacheType, TConvertDataType> : ObjectResolverDialogBase
        where TCacheType : IClientObjectCache
    {
        private readonly ObjectResolverViewModel<TConvertDataType> _dataContext;
        private TCacheType _cache;

        public ObjectResolverDialog()
        {
            _dataContext = new ObjectResolverViewModel<TConvertDataType>();
            DataContext = _dataContext;
            var instance = typeof(TCacheType).GetProperty("Instance",
                BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy);

            if (instance != null)
            {
                _cache = (TCacheType) instance.GetValue(null);
            }
        }

        public IEnumerable<TDataType> GetSelected<TDataType>()
        {
            return ItemListBox.SelectedItems.OfType<TConvertDataType>().Select(t => t.ConvertTo<TDataType>());
        }

        public IEnumerable<TConvertDataType> GetSelected()
        {
            return ItemListBox.SelectedItems.OfType<TConvertDataType>();
        }

        public new bool? ShowDialog()
        {
            var result = Task.Run(async () => await _cache.GetAll()).Result
                .Select(t => t.ConvertTo<TConvertDataType>());

            _dataContext.FullList = result.ToList();

            return base.ShowDialog();
        }
    }
}