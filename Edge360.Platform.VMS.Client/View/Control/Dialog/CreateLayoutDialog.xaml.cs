﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ViewModel;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.CameraService.ServiceModel.Enums;
using Telerik.Windows.Controls.Data.DataForm;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog
{
    /// <summary>
    ///     Interaction logic for CreateLayoutDialog.xaml
    /// </summary>
    public partial class CreateLayoutDialog : IView
    {
        public CreateLayoutDialogViewModel Model => DataContext as CreateLayoutDialogViewModel;

        public CreateLayoutDialog()
        {
            InitializeComponent();
            DataContext = new CreateLayoutDialogViewModel(this);
            ObjectResolver.Register<CreateLayoutDialogViewModel>(DataContext);
        }

        private void OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction != EditAction.Cancel)
            {
                DialogResult = true;
                DialogItem = Model?.DialogItem;
            }

            Close();
        }
    }

    public class CreateLayoutItemModel : INotifyPropertyChanged
    {
        private EGridLayoutShareType _shareType = EGridLayoutShareType.Personal;
        public string Description { get; set; }

        [Required]
        [RegularExpression(DialogUtil.DefaultRegex, ErrorMessage = DialogUtil.DefaultErrorMessage)]
        public string Label { get; set; }

        public EGridLayoutShareType ShareType
        {
            get => _shareType;
            set
            {
                if (_shareType != value)
                {
                    _shareType = value;
                    OnPropertyChanged();
                }
            }
        }

        public List<EGridLayoutShareType> ShareTypes { get; set; } = new List<EGridLayoutShareType>
            {EGridLayoutShareType.Personal, EGridLayoutShareType.Shared};

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class CreateLayoutDialogViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private CreateLayoutItemModel _dialogItem = new CreateLayoutItemModel();

        public CreateLayoutItemModel DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public CreateLayoutDialog View { get; set; }

        public CreateLayoutDialogViewModel(CreateLayoutDialog view)
        {
            View = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}