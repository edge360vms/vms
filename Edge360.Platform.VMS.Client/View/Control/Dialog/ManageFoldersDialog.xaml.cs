﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.View.Control.Management;
using Edge360.Platform.VMS.Client.ViewModel;
using Telerik.Windows.Controls.Data.DataForm;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog
{
    /// <summary>
    ///     Interaction logic for ManageFoldersDialog.xaml
    /// </summary>
    public partial class ManageFoldersDialog : IView
    {
        public ManageFoldersDialogViewModel Model => DataContext as ManageFoldersDialogViewModel;


        public ManageFoldersDialog()
        {
            InitializeComponent();
            DataContext = new ManageFoldersDialogViewModel(this);
            ObjectResolver.Register<ManageFoldersDialogViewModel>(DataContext);
        }


        private void OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction != EditAction.Cancel)
            {
                DialogResult = true;
                DialogItem = Model?.DialogItem;
            }

            Close();
        }

        private void ConfirmClicked(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }

    public class ManageFoldersItemModel : CameraItemModel, INotifyPropertyChanged
    {
        private ServerConfigItemModel _archiver;
        private NodeItemModel _selectedParent;

        public override ZoneCameraManagementViewModel GetZoneCameraViewModel =>
            ObjectResolver.ResolveAll<ZoneCameraManagementViewModel>()?.FirstOrDefault();

        //[Required]
        public override NodeItemModel Parent
        {
            get => base.Parent;
            set
            {
                // this.TryValidate(value);
                base.Parent = value;
                SelectedParent = value;
            }
        }

        public ServerConfigItemModel Archiver
        {
            get => _archiver;
            set
            {
                if (Equals(value, _archiver))
                {
                    return;
                }

                _archiver = value;
                OnPropertyChanged();
            }
        }

        public NodeItemModel SelectedParent
        {
            get => _selectedParent;
            set
            {
                if (Equals(value, _selectedParent))
                {
                    return;
                }

                _selectedParent = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ManageFoldersDialogViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private ManageFoldersItemModel _dialogItem = new ManageFoldersItemModel();

        public ManageFoldersItemModel DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public ManageFoldersDialog View { get; set; }

        public ManageFoldersDialogViewModel(ManageFoldersDialog view)
        {
            View = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}