﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.Management;
using Edge360.Platform.VMS.Client.ViewModel;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Vms.AuthorityService.SharedModel.Enums;
using Prism.Commands;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Data.DataForm;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog
{
    /// <summary>
    ///     Interaction logic for CreateGroupRoleDialog.xaml
    /// </summary>
    public partial class CreateGroupRoleDialog : IView
    {
        public CreateGroupRoleDialogViewModel Model => DataContext as CreateGroupRoleDialogViewModel;


        public CreateGroupRoleDialog()
        {
            InitializeComponent();
            DataContext = new CreateGroupRoleDialogViewModel(this);
            ObjectResolver.Register<CreateGroupRoleDialogViewModel>(DataContext);
        }

        private void OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction != EditAction.Cancel)
            {
                DialogResult = true;
                DialogItem = Model?.DialogItem;
            }

            Close();
        }
    }

    public class CreateGroupRoleItemModel : RoleItemModel, INotifyPropertyChanged
    {
        private ObservableCollection<AvailableRoleItemModel> _availableRoles =
            new ObservableCollection<AvailableRoleItemModel>();

        private ObservableCollection<CreateZoneItemModel> _availableZones =
            new ObservableCollection<CreateZoneItemModel>();

        private ObservableCollection<RoleItemModel> _originalRoles;


        private RelayCommand _refreshAvailableZones;

        private ServerItemModel _selectedServer;
        private DelegateCommand<object> _deleteRoleCommand;

        public ObservableCollection<AvailableRoleItemModel> AvailableRoles
        {
            get => _availableRoles;
            set
            {
                if (Equals(value, _availableRoles))
                {
                    return;
                }

                _availableRoles = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<CreateZoneItemModel> AvailableZones
        {
            get => _availableZones;
            set
            {
                if (Equals(value, _availableZones))
                {
                    return;
                }

                _availableZones = value;
                OnPropertyChanged();
            }
        }


        public bool IsRefreshingZones { get; set; }

        public ObservableCollection<RoleItemModel> OriginalRoles
        {
            get => _originalRoles;
            set
            {
                if (Equals(value, _originalRoles))
                {
                    return;
                }

                _originalRoles = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand RefreshAvailableZones => _refreshAvailableZones ??= new RelayCommand(RefreshZones,
            o => !IsRefreshingZones);


        public ServerItemModel SelectedServer
        {
            get => _selectedServer;
            set
            {
                if (Equals(value, _selectedServer))
                {
                    return;
                }

                _selectedServer = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand<object> DeleteRoleCommand => _deleteRoleCommand ??= new DelegateCommand<object>(o =>
            DeleteRole(o));

        private async Task DeleteRole(object obj)
        {
            try
            {
                if (obj is ListBox listBox)
                {
                    var zoneItemModel = listBox.SelectedItems.OfType<AvailableRoleItemModel>().FirstOrDefault();
                    //    foreach (var zoneItemModel in items)
                    {
                        if (zoneItemModel != null)
                        {
                            zoneItemModel.ResolvedServer ??= ServerItemManager.Manager.ItemCollection.Values.FirstOrDefault(
                                o =>
                                    o.ZoneId == zoneItemModel.ZoneId);

                            if (zoneItemModel.ResolvedServer == null)
                            {
                                await Task.Run(async () =>
                                {
                                    await ServerItemManager.Manager.RefreshServers(zoneItemModel.ZoneId ?? Guid.Empty);
                                    zoneItemModel.ResolvedServer ??=
                                        ServerItemManager.Manager.ItemCollection.Values.FirstOrDefault(o =>
                                            o.ZoneId == zoneItemModel.ZoneId);
                                });     
                                if (zoneItemModel.ResolvedServer == null)
                                {
                                    return;
                                }
                            }

                            var dialog = await DialogUtil.EmbeddedModalConfirmDialog(
                                listBox.ParentOfType<ManagementView>()?.DialogOverlayGrid?.ModalContentPresenter,
                                new EmbeddedDialogParameters
                                {
                                    DialogButtons = EDialogButtons.YesNo,
                                    Description =
                                        $"Are you sure you wish to delete this role?\nZone: {zoneItemModel.ResolvedServer.Label}\nPermissions: {zoneItemModel.AvailableRolesPreview}"
                                });
                            if (dialog)
                            {
                                try
                                {
                                    var delete = await CommunicationManager.GetManagerByZoneId().RolesService
                                        .RemoveRole(zoneItemModel.GroupId, zoneItemModel.ZoneId);
                                    if (delete.Success)
                                    {
                                        //  RefreshRolesCommand.Execute(null);
                                    }
                                }
                                catch (Exception e)
                                {
                                }
                            }
                        }
                    }
           
                }

            }
            catch (Exception e)
            {
                
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public async void Init()
        {
            await Task.Run(async () =>
            {
                await ZoneCameraManagementViewModel.LoadZones(AvailableZones);
            });
            
            var firstServiceClient = CommunicationManager.ServiceManagers?.FirstOrDefault().Value;
            if (firstServiceClient != null)
            {
                var zone = new CreateZoneItemModel
                {
                  //  Address = firstServiceClient.BaseUrl.Replace(":5001", ""),
                    Label = "Global",
                  //  ConnectionStatus = EConnectionStatus.Connected, ZoneId = Guid.Empty
                   ZoneId = Guid.Empty,

                };
                //zone.Archiver = new ArchiveItemModel
                //{
                //    Label = zone.Label,
                //    ZoneId = Guid.Empty,
                //    ServerDescriptor = new ServerDescriptor {Label = zone.Label, ZoneId = zone.ZoneId}
                //};
                //zone.Archivers.Add(zone.Archiver); 
                AvailableZones.Insert(0, zone);
                foreach (var z in AvailableZones)
                {
                    if (ZoneId != null)
                    {
                        z.IsChecked = z.ZoneId == ZoneId;
                    }
                }

                foreach (var r in RolesList)
                {
                    AvailableRoles.Add(new AvailableRoleItemModel
                        {RoleValue = (int) r, Role = r, IsChecked = ((ERoles) RoleValue).HasFlag(r)});
                }
            }

            SubscribeCheckEvent();
        }

        private void SubscribeCheckEvent()
        {
            foreach (var zone in AvailableZones)
            {
                zone.Checked += isChecked =>
                {
                    if (isChecked)
                    {
                        var matchingRole = OriginalRoles.FirstOrDefault(o => o.ZoneId == zone.ZoneId);
                        foreach (var r in AvailableRoles)
                        {
                            //Any time a new row is checked we see if we don't have the role
                            if (!((ERoles) RoleValue).HasFlag(r.Role))
                            {
                                //Now we check if this one does
                                if (matchingRole != null && ((ERoles) matchingRole.RoleValue).HasFlag(r.Role))
                                {
                                    RoleValue += r.RoleValue;
                                    r.IsChecked = true;
                                }
                            }
                        }
                    }
                };
            }
        }

        private async void RefreshZones(object obj)
        {
            IsRefreshingZones = true;
            try
            {
                await Task.Run(async () =>
                {
                    await ZoneCameraManagementViewModel.LoadZones(new ObservableCollection<ZoneItemModel>(AvailableZones));
                }); 
            }
            catch (Exception)
            {
            }

            IsRefreshingZones = false;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    

    public class AvailableRoleItemModel : RoleItemModel, INotifyPropertyChanged
    {
        private bool _isChecked;
        private ERoles _role;

        public bool IsChecked
        {
            get => _isChecked;
            set
            {
                if (value == _isChecked)
                {
                    return;
                }

                _isChecked = value;
                OnPropertyChanged();
            }
        }

        public ERoles Role
        {
            get => _role;
            set
            {
                if (value == _role)
                {
                    return;
                }

                _role = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class CreateGroupRoleDialogViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private ServerConfigItemModel _archiver;

        private CreateGroupRoleItemModel _dialogItem = new CreateGroupRoleItemModel();
        private string _dialogMessage = "Please enter the details for your Role.";
        private ObservableCollection<RoleItemModel> _roles;

        public ServerConfigItemModel Archiver
        {
            get => _archiver;
            set
            {
                if (_archiver != value)
                {
                    _archiver = value;
                    OnPropertyChanged();
                }
            }
        }


        public CreateGroupRoleItemModel DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public string DialogMessage
        {
            get => _dialogMessage;
            set
            {
                if (value == _dialogMessage)
                {
                    return;
                }

                _dialogMessage = value;
                OnPropertyChanged();
            }
        }

        public CreateGroupRoleDialog View { get; set; }

        public CreateGroupRoleDialogViewModel(CreateGroupRoleDialog view)
        {
            View = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}