﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ViewModel;
using Edge360.Platform.VMS.UI.Common.Util;
using Telerik.Windows.Controls.Data;
using Telerik.Windows.Controls.Data.DataForm;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog
{
    /// <summary>
    ///     Interaction logic for CreateCameraDialog.xaml
    /// </summary>
    public partial class ChangeAdminUserPasswordDialog : IView
    {
        public ChangeAdminUserPasswordDialogViewModel Model => DataContext as ChangeAdminUserPasswordDialogViewModel;

        public ChangeAdminUserPasswordDialog()
        {
            InitializeComponent();
            DataContext = new ChangeAdminUserPasswordDialogViewModel(this);
            ObjectResolver.Register<ChangeAdminUserPasswordDialogViewModel>(DataContext);
        }

        private async void OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction != EditAction.Cancel)
            {
                var item = Model?.DialogItem;
                if (item != null)
                {
                    RadDataForm.ValidationSummary.Errors.Clear();
                    try
                    {
                        var resp = await GetManagerByZoneId().UserService
                            .ChangeAdminUserPassword(item.NewPassword, item.UserId);
                    }
                    catch (Exception exception)
                    {
                        Model.CheckPasswordErrorInfo = new ErrorInfo
                        {
                            SourceFieldDisplayName = "New Password",
                            ErrorContent = exception.Message
                        };
                        RadDataForm.ValidationSummary.Errors.Add(Model.CheckPasswordErrorInfo);
                        return;
                    }
                }

                DialogResult = true;
                DialogItem = Model?.DialogItem;
            }

            Close();
        }
    }


    public class ChangeAdminUserPasswordDialogItemModel : INotifyPropertyChanged
    {
        private string _newPassword;


        private string _repeatPassword;
        private Guid _userId;

        //[MinLength(5, ErrorMessage = "Passwords must be 5 characters or more.")]
        //[Required]
        //[DisplayName("Current Password")]
        //public string CurrentPassword
        //{
        //    get => _currentPassword;
        //    set
        //    {
        //        if (_currentPassword != value)
        //        {
        //            this.TryValidate(value);
        //            _currentPassword = value;
        //            try
        //            {
        //                if (ViewModel?.CheckPasswordErrorInfo != null &&
        //                    ViewModel.View.RadDataForm.ValidationSummary.Errors.Contains(ViewModel
        //                        .CheckPasswordErrorInfo))
        //                {
        //                    ViewModel.View.RadDataForm.ValidationSummary.Errors.Remove(ViewModel
        //                        .CheckPasswordErrorInfo);
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //            }

        //            OnPropertyChanged();
        //        }
        //    }
        //}

        [MinLength(5, ErrorMessage = "Passwords must be 5 characters or more.")]
        [Required]
        [DisplayName("New Password")]
        public string NewPassword
        {
            get => _newPassword;
            set
            {
                if (_newPassword != value)
                {
                    this.TryValidate(value);
                    _newPassword = value;
                    OnPropertyChanged();
                }
            }
        }

        [MinLength(5, ErrorMessage = "Passwords must be 5 characters or more.")]
        [Required]
        [DisplayName("Repeat Password")]
        public string RepeatPassword
        {
            get => _repeatPassword;
            set
            {
                if (_repeatPassword != value)
                {
                    this.TryValidate(value);
                    if (!string.IsNullOrEmpty(NewPassword))
                    {
                        if (NewPassword != value)
                        {
                            throw new ValidationException("Repeat Password does not match!");
                        }
                    }

                    _repeatPassword = value;
                    OnPropertyChanged();
                }
            }
        }

        public Guid UserId
        {
            get => _userId;
            set
            {
                if (value.Equals(_userId))
                {
                    return;
                }

                _userId = value;
                OnPropertyChanged();
            }
        }


        [Browsable(false)] public string UserType { get; set; }
        public ChangeAdminUserPasswordDialogViewModel ViewModel { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ChangeAdminUserPasswordDialogViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private ErrorInfo _checkPasswordErrorInfo;
        private ChangeAdminUserPasswordDialogItemModel _dialogItem = new ChangeAdminUserPasswordDialogItemModel();

        public ErrorInfo CheckPasswordErrorInfo
        {
            get => _checkPasswordErrorInfo;
            set
            {
                if (Equals(value, _checkPasswordErrorInfo))
                {
                    return;
                }

                _checkPasswordErrorInfo = value;
                OnPropertyChanged();
            }
        }

        public ChangeAdminUserPasswordDialogItemModel DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public ChangeAdminUserPasswordDialog View { get; set; }

        public ChangeAdminUserPasswordDialogViewModel(ChangeAdminUserPasswordDialog view)
        {
            View = view;
            DialogItem.ViewModel = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}