﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Camera.Discovery;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.View.Control.Management;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication.Discovery;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.CameraAdapter;
using Newtonsoft.Json;
using Prism.Commands;
using ServiceStack;
using Telerik.Windows.Controls.Data.DataForm;
using Unity;
using WispFramework.Patterns.Generators;
using WispFramework.Utility;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog
{
    /// <summary>
    ///     Interaction logic for CreateCameraDialog.xaml
    /// </summary>
    public partial class CreateCameraDialog : IView
    {
        public static Dynamic<Task<string>> GetValueThrottled { get; set; }
        public CreateCameraDialogViewModel Model => DataContext as CreateCameraDialogViewModel;

        public CreateCameraDialog()
        {
            InitializeComponent();
            DataContext = new CreateCameraDialogViewModel(this);
            ObjectResolver.Register<CreateCameraDialogViewModel>(DataContext);
        }

        public static readonly DependencyProperty ZoneCameraManagementViewModelProperty = DependencyProperty.Register(
            "ZoneCameraManagementViewModel", typeof(ZoneCameraManagementViewModel), typeof(CreateCameraDialog), new PropertyMetadata(default(ZoneCameraManagementViewModel), PropertyChangedCallback));

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is CreateCameraDialog view)
            {
                view.Model.ZoneCameraManagementViewModel = view.ZoneCameraManagementViewModel;
            }
        }

        public ZoneCameraManagementViewModel ZoneCameraManagementViewModel
        {
            get { return (ZoneCameraManagementViewModel) GetValue(ZoneCameraManagementViewModelProperty); }
            set { SetValue(ZoneCameraManagementViewModelProperty, value); }
        }

        private void OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction != EditAction.Cancel)
            {
                if (Model.CreatedCameras.Count > 0)
                {
                    if (DialogUtil.ConfirmDialog(
                        $"Are you sure you wish to create '{Model.CreatedCameras.Count}' cameras?", true))
                    {
                        DialogResult = true;
                        DialogItem = Model?.DialogItem;
                    }
                }
                else
                {
                    DialogResult = true;
                    DialogItem = Model?.DialogItem;
                }
            }

            Close();
        }
    }

    public class CreateCameraItemModel : CameraItemModel, INotifyPropertyChanged
    {
        private EConnectionStatus _connectionStatus = EConnectionStatus.None;
        private DirectModeManager _directModeManager;

        [Required]
        public override string Type
        {
            get => _type1;
            set
            {
                if (value == _type1 || value == null) return;
                _type1 = value;
                OnPropertyChanged();
            }
        }

        private DelegateCommand _getCameraUrl;
        private string _ipAddress;

        private bool _isGettingCameraUrl;

        private string _label1;
        private string _liveStreamUrl2;
        private string _password1;
        private string _username1;
        private string _type1 = "CameraFixed";

        [Required]
        public override string Address
        {
            get => _ipAddress;
            set
            {
                if (value == _ipAddress)
                {
                    return;
                }

                this.TryValidate(value);
                ConnectionStatus = EConnectionStatus.None;
                _ipAddress = value;
                OnPropertyChanged();
            }
        }

        public override string Description { get; set; }


        public override ZoneCameraManagementViewModel GetZoneCameraViewModel =>
            ObjectResolver.ResolveAll<ZoneCameraManagementViewModel>()?.FirstOrDefault();

        [Required]
        [RegularExpression(DialogUtil.DefaultRegex, ErrorMessage = DialogUtil.DefaultErrorMessage)]
        public override string Label
        {
            get => _label1;
            set
            {
                if (value == _label1)
                {
                    return;
                }

                this.TryValidate(value);
                _label1 = value;
                OnPropertyChanged();
            }
        }

        //   [Required(AllowEmptyStrings = false)]
        public override string LiveStreamUrl
        {
            get => _liveStreamUrl2;
            set
            {
                if (value == _liveStreamUrl2)
                {
                    return;
                }

                this.TryValidate(value);
                _liveStreamUrl2 = value;
                OnPropertyChanged();
            }
        }

        [Required]
        public override NodeItemModel Parent
        {
            get => base.Parent;
            set
            {
                this.TryValidate(value);
                base.Parent = value;
            }
        }

        public override string Password
        {
            get => _password1;
            set
            {
                if (value == _password1)
                {
                    return;
                }

                ConnectionStatus = EConnectionStatus.None;
                _password1 = value;
                OnPropertyChanged();
            }
        }

        public override string Username
        {
            get => _username1;
            set
            {
                if (value == _username1)
                {
                    return;
                }

                ConnectionStatus = EConnectionStatus.None;
                _username1 = value;
                OnPropertyChanged();
            }
        }

        public EConnectionStatus ConnectionStatus
        {
            get => _connectionStatus;
            set
            {
                if (value == _connectionStatus)
                {
                    return;
                }

                _connectionStatus = value;
                OnPropertyChanged();
            }
        }


        public DelegateCommand GetCameraUrlCommand =>
            _getCameraUrl ??= new DelegateCommand(() =>  GetCameraUrl(null), () => !IsGettingCameraUrl).ObservesProperty(() => IsGettingCameraUrl);


        public bool IsGettingCameraUrl
        {
            get => _isGettingCameraUrl;
            set
            {
                if (value == _isGettingCameraUrl)
                {
                    return;
                }

                _isGettingCameraUrl = value;
                OnPropertyChanged();
                //OnPropertyChanged(nameof(GetCameraUrlCommand));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;


        private async void GetCameraUrl(object obj)
        {
            try
            {
                if (!IsGettingCameraUrl)
                {
                    var _ = Task.Run(async () =>
                    {
                        IsGettingCameraUrl = true;
                        ConnectionStatus = EConnectionStatus.None;
                        try
                        {
                            var resp = await GetManagerByZoneId(ZoneId).CameraAdapterService
                                .GetCameraInfoByConnectionString(Address, Username, Password);
                            if (resp != null && !string.IsNullOrEmpty(resp.SerializedCameraInfo))
                            {
                                var cameraInfo =
                                    JsonConvert.DeserializeObject<CameraInformation>(resp.SerializedCameraInfo,
                                        JsonUtil.IgnoreExceptionSettings());
                                if (cameraInfo != null)
                                {
                                    foreach (var p in cameraInfo.Profile)
                                    {
                                        if (!string.IsNullOrEmpty(p.StreamUri))
                                        {
                                            LiveStreamUrl = p.StreamUri;
                                            ConnectionStatus = EConnectionStatus.Connected;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            ConnectionStatus = EConnectionStatus.Unauthorized;
                        }
                        finally
                        {
                            IsGettingCameraUrl = false;
                        }
                    });
             
                }
            }
            catch (Exception e)
            {
                
            }
        }


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class CreateCameraDialogViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private DelegateCommand _addCameraBatchCommand;
        private ServerConfigItemModel _archiver;

        private ObservableCollection<CreateCameraItemModel> _createdCameras =
            new ObservableCollection<CreateCameraItemModel>();

        private CreateCameraItemModel _dialogItem = new CreateCameraItemModel(){Type = "CameraFixed"};
        private RelayCommand _discoverCamerasCommand;

        private ObservableCollection<CameraDiscoveryItemModel> _discoveredCameras =
            new ObservableCollection<CameraDiscoveryItemModel>();

        private string _discoverNetwork;
        private ushort _discoverPort = 3702;
        private DelegateCommand _inspectCameraBatchCommand;

        private bool _isDiscoveringCameras;
        private bool _isRemoteDiscovery;
        private DelegateCommand _removeCameraBatchCommand;
        private CreateCameraItemModel _selectedCamera;
        private CameraDiscoveryItemModel _selectedDiscoveredCamera;
        private DelegateCommand _openInWeb;
        private DelegateCommand _exportDiscoveredCameras;
        private ZoneCameraManagementViewModel _zoneCameraManagementViewModel;

        public DelegateCommand AddCameraBatchCommand => _addCameraBatchCommand ??= new DelegateCommand(AddCameraBatch);

        public ServerConfigItemModel Archiver
        {
            get => _archiver;
            set
            {
                if (_archiver != value)
                {
                    _archiver = value;
                    OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<CreateCameraItemModel> CreatedCameras
        {
            get => _createdCameras;
            set
            {
                if (Equals(value, _createdCameras))
                {
                    return;
                }

                _createdCameras = value;
                OnPropertyChanged();
            }
        }

        public CreateCameraItemModel DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public RelayCommand DiscoverCamerasCommand => _discoverCamerasCommand ??=
            new RelayCommand(o => DiscoverCamerasFromRange(), CanDiscoverCameras);


        public ObservableCollection<CameraDiscoveryItemModel> DiscoveredCameras
        {
            get => _discoveredCameras;
            set
            {
                if (Equals(value, _discoveredCameras))
                {
                    return;
                }

                _discoveredCameras = value;
                OnPropertyChanged();
            }
        }

        public string DiscoverNetwork
        {
            get => _discoverNetwork;
            set
            {
                if (value == _discoverNetwork)
                {
                    return;
                }

                _discoverNetwork = value;
                OnPropertyChanged();
            }
        }

        public ushort DiscoverPort
        {
            get => _discoverPort;
            set
            {
                if (value == _discoverPort)
                {
                    return;
                }

                _discoverPort = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand InspectCameraCommand =>
            _inspectCameraBatchCommand ??= new DelegateCommand(InspectCamera);

        public bool IsDiscoveringCameras
        {
            get => _isDiscoveringCameras;
            set
            {
                if (value == _isDiscoveringCameras)
                {
                    return;
                }

                _isDiscoveringCameras = value;
                OnPropertyChanged();
            }
        }

        public bool IsRemoteDiscovery
        {
            get => _isRemoteDiscovery;
            set
            {
                if (value == _isRemoteDiscovery)
                {
                    return;
                }

                _isRemoteDiscovery = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand RemoveCameraBatchCommand =>
            _removeCameraBatchCommand ??= new DelegateCommand(RemoveCameraBatch);

        //public CreateCameraItemModel SelectedCamera
        //{
        //    get => _selectedCamera;
        //    set
        //    {
        //        if (Equals(value, _selectedCamera))
        //        {
        //            return;
        //        }

        //        _selectedCamera = value;
        //        if (value != null)
        //        {
        //            DialogItem = value;
        //        }

        //        OnPropertyChanged();
        //    }
        //}

        public CameraDiscoveryItemModel SelectedDiscoveredCamera
        {
            get => _selectedDiscoveredCamera;
            set
            {
                if (Equals(value, _selectedDiscoveredCamera))
                {
                    return;
                }

                _selectedDiscoveredCamera = value;
                if (value != null)
                {
                    DialogItem.Address = value.Address;
                }

                OnPropertyChanged();
            }
        }

        public CreateCameraDialog View { get; set; }

        public ZoneCameraManagementViewModel ZoneCameraManagementViewModel
        {
            get => _zoneCameraManagementViewModel;
            set => _zoneCameraManagementViewModel = value;
            //get
            //{
            //    var zoneCameraManagementViewModel = ObjectResolver.Resolve<ZoneCameraManagementViewModel>();// App.GlobalUnityContainer.Resolve<ZoneCameraManagementViewModel>();
            //    return zoneCameraManagementViewModel;
            //}
        }

        public DelegateCommand OpenInWebCommand => _openInWeb ??= new DelegateCommand(OpenInWeb, () => SelectedDiscoveredCamera != null).ObservesProperty((() => SelectedDiscoveredCamera));

        public DelegateCommand ExportDiscoveredCameras => _exportDiscoveredCameras ??= new DelegateCommand( ExportCameras, () => DiscoveredCameras.Count > 0).ObservesProperty(() => DiscoveredCameras);

        private void ExportCameras()
        {
            try
            {
                var output = "(Name),(Model),(Address),\n";
                foreach (var d in DiscoveredCameras)
                {
                    output += $"{d.Name},{d.Model},{d.Address},\n";
                }
                Clipboard.SetText(output);
            }
            catch (Exception e)
            {
                
            }
        }

        private void OpenInWeb()
        {
            try
            {
                Process.Start(new ProcessStartInfo($"http://{SelectedDiscoveredCamera.Address}"));
            }
            catch (Exception e)
            {
                
            }
        }



        public CreateCameraDialogViewModel(CreateCameraDialog view)
        {
            View = view;
            if (string.IsNullOrEmpty(DiscoverNetwork))
            {
                var output = new List<IPAddress>();
                foreach (var item in NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (item.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
                        item.OperationalStatus == OperationalStatus.Up &&
                        !item.Description.StartsWith("Hyper-V Virtual "))
                    {
                        foreach (var ipAddressInformation in item.GetIPProperties().UnicastAddresses)
                        {
                            if (ipAddressInformation.Address.AddressFamily == AddressFamily.InterNetwork)
                            {
                                output.Add(ipAddressInformation.Address);
                            }
                        }
                    }
                }

                DiscoverNetwork = output?.FirstOrDefault()?.ToString();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public const string HardwarePattern = "(?<Hardware>hardware\\/(\\S+|\\s))";
        public const string LocationPattern = "(?<Location>location\\/(\\S+|\\s))";
        public const string NamePattern = "(?<Name>name\\/(\\S+|\\s))";
        private const string AddressPattern = "[http|https]://(.*)/onvif/device_service";

        private async void DiscoverCamerasFromRange()
        {
            if (!IsDiscoveringCameras)
            {
                try
                {
                    IsDiscoveringCameras = true;
                    if (!IsRemoteDiscovery)
                    {
                        await Task.Run(async () =>
                        {
                            var cameras =
                                await CameraDiscovery.DiscoverRangeOfCamerasAsync(DiscoverNetwork, DiscoverPort);
                            if (cameras != null)
                            {
                                var outputCameras = new List<CameraDiscoveryItemModel>();
                                await Task.WhenAll(cameras.Select(async s =>
                                {
                                    try
                                    {
                                        var address1 = CameraDiscovery.GetAddress(s);
                                        var address = address1.Split(' ').OrderByDescending(o => DiscoverNetwork)
                                                          .FirstOrDefault() ??
                                                      address1;
                                        var scopes = CameraDiscovery.GetScopes(s);
                                       
                                        var hardwareScope = Regex.Match(scopes,
                                            HardwarePattern);
                                        var locationPattern = Regex.Match(scopes,
                                            LocationPattern);
                                        var namePattern = Regex.Match(scopes,
                                            NamePattern);
                                        var addressMatch = Regex.Match(address,
                                            AddressPattern);
                                        var camera = new CameraDiscoveryItemModel
                                        {
                                            Model = hardwareScope?.Result("$1")?.Replace("%20", " ") ?? "",
                                            Name = namePattern?.Result("$1")?.Replace("%20", " ") ?? "",
                                            Address = addressMatch?.Result("$1")?.Replace("%20", " ") ?? "",
                                            Location = locationPattern?.Result("$1") ?? "",
                                        };
                                        if (!outputCameras.Any(c =>
                                            CameraDiscoveryItemModel.AddressNameModelComparer.Equals(camera, c)))
                                        {
                                            outputCameras.Add(camera);
                                        }

                                        Trace.WriteLine(camera);
                                    }
                                    catch (Exception e)
                                    {
                                    }
                                }));

                                await Application.Current.Dispatcher.InvokeAsync(() =>
                                {
                                    DiscoveredCameras.Clear();
                                    DiscoveredCameras.AddRange(outputCameras.OrderBy(o => o.Address));
                                });
                            }
                        });
                    }
                    else
                    {
                        var listDiscoveredCameras = await GetManagerByZoneId(DialogItem.ZoneId).CameraAdapterService
                            .ListDiscoveredCameras(
                                new ListDiscoveredCameras
                                {
                                    DiscoveryHostname = DiscoverNetwork
                                });
                        if (listDiscoveredCameras != null)
                        {
                            DiscoveredCameras.Clear();
                            var cams = listDiscoveredCameras.DiscoveredCameras.Select(o =>
                                o.ConvertTo<CameraDiscoveryItemModel>());
                            DiscoveredCameras.AddRange(cams);
                        }
                    }
                }
                catch (Exception e)
                {
                }
                finally
                {
                    IsDiscoveringCameras = false;
                }
            }
            OnPropertyChanged(nameof(DiscoveredCameras));
        }

        private bool CanDiscoverCameras(object obj)
        {
            return true;
        }

        private void AddCameraBatch()
        {
            if (DialogItem != null)
            {
                if (View.RadDataForm.ValidateItem()) // DialogItem.Address && DialogItem.Label )
                {
                    CreatedCameras.Add(new CreateCameraItemModel
                    {
                        ZoneId = DialogItem.ZoneId, Parent = DialogItem.Parent, ParentId = DialogItem.Parent.ObjectId,
                        Address = DialogItem.Address, Type = DialogItem.Type,
                        Label = DialogItem.Label, Username = DialogItem.Username, Password = DialogItem.Password,
                        ConnectionStatus = DialogItem.ConnectionStatus
                    });
                }
            }
        }

        private void RemoveCameraBatch()
        {
            try
            {
                var selected = View.CameraBatchListBox.SelectedItems.OfType<CreateCameraItemModel>().ToList();
                foreach (var c in selected)
                {
                    CreatedCameras.Remove(c);
                }
            }
            catch (Exception e)
            {
            }
        }

        private void InspectCamera()
        {
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}