﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.UI.Common.Util;
using Telerik.Windows.Controls.Data.DataForm;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog
{
    /// <summary>
    ///     Interaction logic for CreateCameraDialog.xaml
    /// </summary>
    public partial class CreateAppSettingDialog : IView
    {
        public CreateAppSettingDialogViewModel Model => DataContext as CreateAppSettingDialogViewModel;

        public CreateAppSettingDialog()
        {
            InitializeComponent();
            DataContext = new CreateAppSettingDialogViewModel(this);
            ObjectResolver.Register<CreateAppSettingDialogViewModel>(DataContext);
        }

        private void OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction != EditAction.Cancel)
            {
                DialogResult = true;
                DialogItem = Model?.DialogItem;
            }

            Close();
        }
    }


    public class CreateAppSettingDialogItemModel : INotifyPropertyChanged
    {
        [Required]
        [RegularExpression(DialogUtil.DefaultRegex, ErrorMessage = DialogUtil.DefaultErrorMessage)]
        public string Name { get; set; }

        [Required]
        [RegularExpression(DialogUtil.DefaultRegex, ErrorMessage = DialogUtil.DefaultErrorMessage)]
        public string Value { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }


    public class CreateAppSettingDialogViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private CreateAppSettingDialogItemModel _dialogItem = new CreateAppSettingDialogItemModel();

        public CreateAppSettingDialogItemModel DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public CreateAppSettingDialog View { get; set; }

        public CreateAppSettingDialogViewModel(CreateAppSettingDialog view)
        {
            View = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}