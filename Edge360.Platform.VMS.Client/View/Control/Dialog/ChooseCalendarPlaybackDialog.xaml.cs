﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ViewModel;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Telerik.Windows.Controls.Data.DataForm;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog
{
    /// <summary>
    ///     Interaction logic for ChooseCalendarPlaybackDialog.xaml
    /// </summary>
    public partial class ChooseCalendarPlaybackDialog : IView
    {
        public ChooseCalendarPlaybackDialogViewModel Model => DataContext as ChooseCalendarPlaybackDialogViewModel;

        public ChooseCalendarPlaybackDialog()
        {
            InitializeComponent();
            DataContext = new ChooseCalendarPlaybackDialogViewModel(this);
            ObjectResolver.Register<ChooseCalendarPlaybackDialogViewModel>(DataContext);
        }

        public void SetStartDate(DateTime date)
        {
            if (Model?.DialogItem != null)
            {
                Model.DialogItem.SelectedDate = date;
            }
        }

        private void OnEditEnded(object sender, EditEndedEventArgs e)
        {
        }
    }


    public class ChooseCalendarPlaybackDialogItemModel : INotifyPropertyChanged
    {
        private bool _pausePlayback;
        private DateTime _selectedDate;

        public bool PausePlayback
        {
            get => _pausePlayback;
            set
            {
                if (_pausePlayback != value)
                {
                    _pausePlayback = value;
                    OnPropertyChanged();
                }
            }
        }

        public DateTime SelectedDate
        {
            get => _selectedDate;
            set
            {
                if (_selectedDate != value)
                {
                    _selectedDate = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ChooseCalendarPlaybackDialogViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private RelayCommand _acceptDialogCommand;
        private RelayCommand _cancelDialogCommand;
        private ChooseCalendarPlaybackDialogItemModel _dialogItem = new ChooseCalendarPlaybackDialogItemModel();

        public RelayCommand AcceptDialogCommand => _acceptDialogCommand ??= new RelayCommand(AcceptDialog, CanAcceptDialog);

        public RelayCommand CancelDialogCommand => _cancelDialogCommand ??= new RelayCommand(CancelDialog, CanCancelDialog);

        public ChooseCalendarPlaybackDialogItemModel DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public ChooseCalendarPlaybackDialog View { get; set; }

        public ChooseCalendarPlaybackDialogViewModel(ChooseCalendarPlaybackDialog view)
        {
            View = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void CancelDialog(object obj)
        {
            View.Close();
        }

        private bool CanCancelDialog(object obj)
        {
            return true;
        }

        private void AcceptDialog(object obj)
        {
            View.DialogResult = true;
            View.DialogItem = DialogItem;
            View.Close();
        }

        private bool CanAcceptDialog(object obj)
        {
            return true;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}