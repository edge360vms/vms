﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ViewModel;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Groups;
using Telerik.Windows.Controls.Data.DataForm;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog
{
    /// <summary>
    ///     Interaction logic for CreateCameraDialog.xaml
    /// </summary>
    public partial class CreateGroupDialog : IView
    {
        public CreateGroupDialogViewModel Model => DataContext as CreateGroupDialogViewModel;

        public CreateGroupDialog()
        {
            InitializeComponent();
            DataContext = new CreateGroupDialogViewModel(this);
            ObjectResolver.Register<CreateGroupDialogViewModel>(DataContext);
        }

        private void OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction != EditAction.Cancel)
            {
                DialogResult = true;
                DialogItem = Model?.DialogItem;
            }

            Close();
        }
    }

    public class CreateGroupDialogItemModel : Group, INotifyPropertyChanged
    {
        private string _groupName;
        [Browsable(false)] [CanBeNull] public new Guid? GroupId { get; set; }

        [Required]
        [RegularExpression(@"^[] !@#$%^&*()_=+[{};:,.<>?'/`|~\w-]{3,64}$",
            ErrorMessage = "Group names must be 3-64 characters in length.")]
        public new string GroupName
        {
            get => _groupName;
            set
            {
                if (_groupName != value)
                {
                    this.TryValidate(value);
                    _groupName = value;
                    OnPropertyChanged();
                }
            }
        }

        [Browsable(false)] [CanBeNull] public new List<GroupMemberDetails> MemberDetails { get; set; }

        [Browsable(false)] [CanBeNull] public new List<Guid?> Members { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class CreateGroupDialogViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private CreateGroupDialogItemModel _dialogItem = new CreateGroupDialogItemModel();

        public CreateGroupDialogItemModel DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public CreateGroupDialog View { get; set; }

        public CreateGroupDialogViewModel(CreateGroupDialog view)
        {
            View = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}