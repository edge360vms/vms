﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ViewModel;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.UI.Common.Util;
using Telerik.Windows.Controls.Data.DataForm;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog
{
    /// <summary>
    ///     Interaction logic for CreateCameraDialog.xaml
    /// </summary>
    public partial class CreateAdConfigDialog : IView
    {
        public CreateAdConfigDialogViewModel Model => DataContext as CreateAdConfigDialogViewModel;

        public CreateAdConfigDialog()
        {
            InitializeComponent();
            DataContext = new CreateAdConfigDialogViewModel(this);
            ObjectResolver.Register<CreateAdConfigDialogViewModel>(DataContext);
        }

        private void OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction != EditAction.Cancel)
            {
                DialogResult = true;
                DialogItem = Model?.DialogItem;
            }

            Close();
        }
    }

    public class CreateAdConfigDialogItemModel : ADConfigItemModel
    {
        private EConnectionStatus _connectionStatus;
        private string _domains = ""; //"dev.edge360.com.local";
        private bool _isTesting;
        private string _password = "";//"YVKD3mPAVbXvgw1nJTLI";
        private RelayCommand _testConnectionCommand;
        private string _urls = "";//"10.200.1.107:389";
        private string _username = "";//"Administrator@dev.edge360.com.local";

        public EConnectionStatus ConnectionStatus
        {
            get => _connectionStatus;
            set
            {
                if (_connectionStatus != value)
                {
                    _connectionStatus = value;
                    OnPropertyChanged();
                }
            }
        }

        [DisplayName("Domain Names")]
        [Display(Order = 4)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter a domain name.")]
        public string Domains
        {
            get => _domains;
            set
            {
                if (_domains != value)
                {
                    this.TryValidate(value);
                    _domains = value;
                    ConnectionStatus = EConnectionStatus.None;
                    OnPropertyChanged();
                }
            }
        }

        public bool IsTesting
        {
            get => _isTesting;
            set
            {
                if (_isTesting != value)
                {
                    _isTesting = value;

                    OnPropertyChanged();
                    Trace.WriteLine($"CreateAd 97 InvalidateRequerySuggested");
                    OnPropertyChanged(nameof(TestConnectionCommand));
                    //CommandManager.InvalidateRequerySuggested();
                }
            }
        }


        [PasswordPropertyText(true)]
        [Display(Order = 3)]
        [MinLength(5, ErrorMessage = "Password must be 5 or more characters.")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter a password.")]
        public new string Password
        {
            get => _password;
            set
            {
                if (_password != value)
                {
                    this.TryValidate(value);
                    _password = value;
                    ConnectionStatus = EConnectionStatus.None;
                    OnPropertyChanged();
                }
            }
        }

        public RelayCommand TestConnectionCommand => _testConnectionCommand ??= new RelayCommand(p => TestConnection(),
            o => CanTestConnection());

        [Display(Order = 1)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter a URL.")]
        public new string Urls
        {
            get => _urls;
            set
            {
                if (_urls != value)
                {
                    this.TryValidate(value);
                    _urls = value;
                    ConnectionStatus = EConnectionStatus.None;
                    OnPropertyChanged();
                }
            }
        }

        [Display(Order = 2)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter a username.")]
        public new string Username
        {
            get => _username;
            set
            {
                if (_username != value)
                {
                    this.TryValidate(value);
                    _username = value;
                    ConnectionStatus = EConnectionStatus.None;
                    OnPropertyChanged();
                }
            }
        }

        private async void TestConnection()
        {
            IsTesting = true;
            _ = Task.Run(async () =>
            {
                try
                {
                    var result = await
                        GetManagerByZoneId().AdConfigZoneService.TestConfig(Username, Password, Urls);
                    if (result != null && result.Success)
                    {
                        ConnectionStatus = EConnectionStatus.Connected;
                    }
                    else
                    {
                        ConnectionStatus = EConnectionStatus.Timeout;
                    }
                }
                catch (Exception e)
                {
                    _log?.Error(e);
                }
                finally
                {
                    IsTesting = false;
                }
            });
        }

        private bool CanTestConnection()
        {
            if (string.IsNullOrEmpty(Domains) || string.IsNullOrWhiteSpace(Password) ||
                string.IsNullOrWhiteSpace(Urls) || string.IsNullOrWhiteSpace(Username))
            {
                return false;
            }

            if (IsTesting)
            {
                return false;
            }

            return true;
        }
    }

    public class CreateAdConfigDialogViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private CreateAdConfigDialogItemModel _dialogItem = new CreateAdConfigDialogItemModel();

        public CreateAdConfigDialogItemModel DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public CreateAdConfigDialog View { get; set; }

        public CreateAdConfigDialogViewModel(CreateAdConfigDialog view)
        {
            View = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}