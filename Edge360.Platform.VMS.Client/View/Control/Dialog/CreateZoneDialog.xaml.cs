﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.ViewModel;
using Edge360.Platform.VMS.UI.Common.Util;
using Telerik.Windows.Controls.Data.DataForm;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog
{
    /// <summary>
    ///     Interaction logic for CreateZoneDialog.xaml
    /// </summary>
    public partial class CreateZoneDialog : IView
    {
        public CreateZoneDialogViewModel Model => DataContext as CreateZoneDialogViewModel;


        public CreateZoneDialog()
        {
            InitializeComponent();
            DataContext = new CreateZoneDialogViewModel(this);
            ObjectResolver.Register<CreateZoneDialogViewModel>(DataContext);
        }

        private void OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction != EditAction.Cancel)
            {
                DialogResult = true;
                DialogItem = Model?.DialogItem;
            }

            Close();
        }
    }

    public class CreateZoneItemModel : ZoneItemModel, INotifyPropertyChanged
    {
        private bool _isChecked;

       

        public bool IsChecked
        {
            get => _isChecked;
            set
            {
                if (value == _isChecked)
                {
                    return;
                }

                Checked?.Invoke(value);
                _isChecked = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public event Action<bool> Checked;


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class CreateZoneDialogViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private ServerConfigItemModel _archiver;

        private CreateZoneItemModel _dialogItem = new CreateZoneItemModel();

        public ServerConfigItemModel Archiver
        {
            get => _archiver;
            set
            {
                if (_archiver != value)
                {
                    _archiver = value;
                    OnPropertyChanged();
                }
            }
        }

        public CreateZoneItemModel DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public CreateZoneDialog View { get; set; }

        public CreateZoneDialogViewModel(CreateZoneDialog view)
        {
            View = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}