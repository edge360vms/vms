﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reflection;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog.FilterDialog.ViewModel
{
    public class FilterDialogViewModel
    {
        public ObservableCollection<PropertyInfo> Properties { get; set; } = new ObservableCollection<PropertyInfo>();

        public void SetType<T>()
        {
            var t = typeof(T);
            foreach (var prop in t.GetProperties(BindingFlags.FlattenHierarchy | BindingFlags.Instance |
                                                 BindingFlags.Public))
            {
                var browsable = prop.GetCustomAttribute<BrowsableAttribute>();
                if (browsable != null && browsable.Browsable == false)
                {
                    continue;
                }

                if (!prop.PropertyType.IsPrimitive && prop.PropertyType != typeof(Guid) &&
                    prop.PropertyType != typeof(string))
                {
                    continue;
                }

                Properties.Add(prop);
            }
        }
    }
}