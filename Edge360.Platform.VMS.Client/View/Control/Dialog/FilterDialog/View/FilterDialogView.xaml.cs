﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using Edge360.Platform.VMS.Client.View.Control.Dialog.FilterDialog.ViewModel;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog.FilterDialog.View
{
    /// <summary>
    ///     Interaction logic for FilterDialogView.xaml
    /// </summary>
    public partial class FilterDialogView : RadWindow
    {
        private readonly FilterDialogViewModel _viewModel;

        public List<PropertyInfo> FilterItems => FilterList.SelectedItems.OfType<PropertyInfo>().ToList();

        public FilterDialogView()
        {
            _viewModel = new FilterDialogViewModel();
            DataContext = _viewModel;
            InitializeComponent();
        }

        public bool? ShowDialog<T>(IEnumerable<PropertyInfo> selected)
        {
            _viewModel.SetType<T>();

            if (selected != null)
            {
                foreach (var sel in selected)
                {
                    FilterList.SelectedItems.Add(sel);
                }
            }

            return ShowDialog();
        }

        private void Ok_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}