﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ViewModel;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.UI.Common.Util;
using Telerik.Windows.Controls.Data.DataForm;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog
{
    /// <summary>
    ///     Interaction logic for CreateCameraDialog.xaml
    /// </summary>
    public partial class CreateAdDomainDialog : IView
    {
        public CreateAdDomainDialogViewModel Model => DataContext as CreateAdDomainDialogViewModel;

        public CreateAdDomainDialog()
        {
            InitializeComponent();
            DataContext = new CreateAdDomainDialogViewModel(this);
            ObjectResolver.Register<CreateAdDomainDialogViewModel>(DataContext);
        }

        private void OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction != EditAction.Cancel)
            {
                DialogResult = true;
                DialogItem = Model?.DialogItem;
            }

            Close();
        }
    }

    public class CreateAdDomainDialogItemModel : ADDomainItemModel, INotifyPropertyChanged
    {
        private string _domain;
        [Browsable(false)] public new object ActiveDirectoryId { get; set; }
        [Browsable(false)] public new RelayCommand DeleteDomainCommand { get; set; }

        [Required]
        [RegularExpression(DialogUtil.DefaultRegex, ErrorMessage = DialogUtil.DefaultErrorMessage)]
        public new string Domain
        {
            get => _domain;
            set
            {
                if (_domain != value)
                {
                    this.TryValidate(value);
                    _domain = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class CreateAdDomainDialogViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private CreateAdDomainDialogItemModel _dialogItem = new CreateAdDomainDialogItemModel();

        public CreateAdDomainDialogItemModel DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public CreateAdDomainDialog View { get; set; }

        public CreateAdDomainDialogViewModel(CreateAdDomainDialog view)
        {
            View = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}