﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.View.Window;
using Edge360.Platform.VMS.Client.ViewModel;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Users;
using Newtonsoft.Json;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Data.DataForm;
using DelegateCommand = Prism.Commands.DelegateCommand;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog
{
    /// <summary>
    ///     Interaction logic for CreateCameraDialog.xaml
    /// </summary>
    public partial class CreateUserDialog : IView
    {
        public CreateUserDialogViewModel Model => DataContext as CreateUserDialogViewModel;

        public CreateUserDialog()
        {
            InitializeComponent();
            DataContext = new CreateUserDialogViewModel(this);
            ObjectResolver.Register<CreateUserDialogViewModel>(DataContext);
        }

        private void OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction != EditAction.Cancel)
            {
                DialogResult = true;
                DialogItem = Model?.DialogItem;
            }

            Close();
        }
    }


    public class CreateUserDialogItemModel : UserDetails, INotifyPropertyChanged
    {
        private string _displayName;
        private string _password;
        private RelayCommand _userActionCommand;
        private string _username;
        private DelegateCommand _clearCacheCommand;

        [Required]
        [RegularExpression(DialogUtil.DefaultRegex, ErrorMessage = DialogUtil.DefaultErrorMessage)]
        public new string DisplayName
        {
            get => _displayName;
            set
            {
                if (_displayName != value)
                {
                    this.TryValidate(value);
                    _displayName = value;
                    OnPropertyChanged();
                }
            }
        }

        [Browsable(false)] public new bool Enabled { get; set; }

        [MinLength(5)]
        [Required]
        [PasswordPropertyText(true)]
        public string Password
        {
            get => _password;
            set
            {
                if (_password != value)
                {
                    this.TryValidate(value);
                    _password = value;
                    OnPropertyChanged();
                }
            }
        }


        [JsonIgnore]
        public RelayCommand UserActionCommand => _userActionCommand ??= new RelayCommand(UserAction, CanExecuteUserAction);

        [Browsable(false)] public new Guid? UserId { get; set; }

        [Required]
        [RegularExpression(DialogUtil.DefaultRegex, ErrorMessage = DialogUtil.DefaultErrorMessage)]
        public new string Username
        {
            get => _username;
            set
            {
                if (_username != value)
                {
                    this.TryValidate(value);
                    _username = value;
                    OnPropertyChanged();
                }
            }
        }

        [Browsable(false)] public new string UserType { get; set; }

        public DelegateCommand ClearCacheCommand => _clearCacheCommand ??= new DelegateCommand(ClearCache);

        private void ClearCache()
        {
            TabbedWindowViewModel.ClearApiCache();
        }

        public event PropertyChangedEventHandler PropertyChanged;


        private void UserAction(object obj)
        {
            if (obj is EUserActionCommand command)
            {
                switch (command)
                {
                    case EUserActionCommand.Logout:
                        ObjectResolver.Resolve<Window.TabbedWindow>()?.Model?.LogOut(true);
                        break;
                    case EUserActionCommand.ClearSessions:
                        ((UserItemModel) CommunicationManager.CurrentUser)?.ClearSessionsCommand.Execute(null);
                        break;
                    case EUserActionCommand.ChangePassword:
                        ((UserItemModel) CommunicationManager.CurrentUser)?.ChangePasswordUserCommand.Execute(null);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private bool CanExecuteUserAction(object obj)
        {
            if (obj is EUserActionCommand command)
            {
                switch (command)
                {
                    case EUserActionCommand.Logout:
                        break;
                    case EUserActionCommand.ClearSessions:
                        break;
                    case EUserActionCommand.ChangePassword:
                        break;
                    //default:
                    //    throw new ArgumentOutOfRangeException();
                }
            }

            return true;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }


    public class CreateUserDialogViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private CreateUserDialogItemModel _dialogItem = new CreateUserDialogItemModel();

        public CreateUserDialogItemModel DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public CreateUserDialog View { get; set; }

        public CreateUserDialogViewModel(CreateUserDialog view)
        {
            View = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}