﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Communication.Polly;
using Edge360.Platform.VMS.Communication.Services;
using Edge360.Platform.VMS.UI.Common.View.Dialog;
using log4net;
using ServiceStack;
using WispFramework.Extensions.Common;

namespace Edge360.Platform.VMS.Client.View.Control.Dialog.Login
{
    /// <summary>
    ///     Interaction logic for LoginBox.xaml
    /// </summary>
    public partial class LoginDialog : DialogBase
    {
        private readonly ILog _log;
        private string _errorMessage;
        private RelayCommand _loginCommand;

        public string ErrorMessage
        {
            get => _errorMessage;
            set
            {
                if (value == _errorMessage)
                {
                    return;
                }

                _errorMessage = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand LoginCommand =>
            _loginCommand ?? (_loginCommand = new RelayCommand(o => Login(o), CanExecute));

        public LoginDialogViewModel Model => DataContext as LoginDialogViewModel;

        public LoginDialog()
        {
            InitializeComponent();
        }

        private async void Login(object obj = null)
        {
            ErrorMessage = string.Empty;
            //if (viewModel.IsLoggedIn)
            //{
            //    // Assume we are asking to logout
            //    viewModel.Logout();
            //    Close();
            //    return;
            //}
            UpdateUiEnabled(false);
            ParsePort();
            try
            {
                var auth = await Authenticate(exception =>
                {
                    if (exception is TimeoutException timeoutException)
                    {
                        ErrorMessage = "Connection timed Out.";
                        _log.Warn(ErrorMessage, exception);
                    }
                    else if (exception is AuthenticationException)
                    {
                        ErrorMessage = exception.Message;
                    }
                    else if (exception is WebServiceException webServiceException)
                    {
                        ErrorMessage =
                            $"Error: {webServiceException.ErrorMessage} ({webServiceException.ErrorCode})";
                    }
                    else
                    {
                        ErrorMessage =
                            $"Error: {exception.Message}";
                    }
                });
                if (auth)
                {
                    DialogResult = true;
                    Close();
                }
                else
                {
                    UpdateUiEnabled(true);
                }
            }
            catch (Exception e)
            {
                _log.Warn("Login attempt failed.", e);
            }
        }

        private void ParsePort()
        {
            var splitServerAddress = Model.LoginItemModel.Domain.Split(':');
            if (splitServerAddress.Length > 1)
            {
                if (ushort.TryParse(splitServerAddress[1], out var newPort))
                {
                    Model.LoginItemModel.ApiPort = newPort;
                    Model.LoginItemModel.Domain = splitServerAddress[0];
                }
            }
        }

        public async Task<bool> Authenticate(Action<Exception> connectionException = null)
        {
            var login = Model.LoginItemModel;
            if (login == null || login.UserName == null || login.Password == null)
            {
                return false;
            }

            login.ApiPort = 3501;
            ParsePort();

            try
            {
                CommunicationManager.StartCommunicationManager(); // disables certificates and enables JS UTC requests
                var client = new CustomServiceClient($"https://{login.ResolveDomain()}/authority")
                {
                    Timeout = TimeSpan.FromSeconds(5)
                };

                var authResponse = await client.PostAsync(new Authenticate
                {
                    provider = "credentials",
                    UserName = login.UserName,
                    Password = login.Password
                });

                client.BearerToken = authResponse.BearerToken;
                client.RefreshToken = authResponse.RefreshToken;
                client.SessionId = authResponse.SessionId;
                Model.BearerToken = authResponse.BearerToken;

                // var baseUrl = BaseUri.Replace("https:", "").Replace("/", "");
                var svcMgr = new ServiceManager(client, login.ResolveDomain());

                var resp = await CommunicationManager.InitializeServiceManager(svcMgr);
                if (!resp && client.BearerToken.IsNotEmpty())
                {
                    connectionException?.Invoke(new AuthenticationException("Zone Information Error"));
                }

                Model.ServiceManager = svcMgr;
                return resp;
            }
            catch (Exception e)
            {
                connectionException?.Invoke(e);

                //    Server = null;
                return false;
            }
        }


        private void UpdateUiEnabled(bool isEnabled)
        {
            LoginButton.IsEnabled = isEnabled;
            PasswordBox.IsEnabled = isEnabled;
            UserTextBox.IsEnabled = isEnabled;
        }

        private bool CanExecute(object obj)
        {
            return true;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            //ServerAddress = BootstrapConfig?.ServerAddress;
            //if (ServerAddress.IsNotWhitespace())
            //    FocusManager.SetFocusedElement(FocusManager.GetFocusScope(UserTextBox), UserTextBox);
        }

        private void OnClosed(object sender, WindowClosedEventArgs e)
        {
            //if (ServerAddress.IsNotWhitespace())
            //{
            //    Settings.Default.LastServerAddress = ServerAddress;
            //    Settings.Default.LastUsername = Username;
            //    Settings.Default.Save();
            //}
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }

    public class LoginDialogViewModel : INotifyPropertyChanged
    {
        private string _errorMessage;
        private ILog _log;
        private UserLoginItemModel _loginItemModel = new UserLoginItemModel();
        private ServiceManager _serviceManager;

        public string BearerToken { get; set; }

        public string ErrorMessage
        {
            get => _errorMessage;
            set
            {
                if (value == _errorMessage)
                {
                    return;
                }

                _errorMessage = value;
                OnPropertyChanged();
            }
        }

        public UserLoginItemModel LoginItemModel
        {
            get => _loginItemModel;
            set
            {
                if (Equals(value, _loginItemModel))
                {
                    return;
                }

                _loginItemModel = value;
                OnPropertyChanged();
            }
        }

        public ServiceManager ServiceManager
        {
            get => _serviceManager;
            set
            {
                if (Equals(value, _serviceManager))
                {
                    return;
                }

                _serviceManager = value;
                OnPropertyChanged();
            }
        }

        public LoginDialogViewModel(ILog log = null)
        {
            _log = log;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}