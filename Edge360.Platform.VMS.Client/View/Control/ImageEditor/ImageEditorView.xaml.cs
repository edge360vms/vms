﻿using System.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.ImageEditor
{
    /// <summary>
    ///     Interaction logic for ImageEditorView.xaml
    /// </summary>
    public partial class ImageEditorView : UserControl, IView
    {
        public ImageEditorView()
        {
            InitializeComponent();
        }
    }
}