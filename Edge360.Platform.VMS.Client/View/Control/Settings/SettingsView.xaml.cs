﻿using System.Linq;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Settings
{
    /// <summary>
    ///     Interaction logic for SettingsView.xaml
    /// </summary>
    public partial class SettingsView : UserControl, IView
    {
        public SettingsView()
        {
            InitializeComponent();
            Model.View = this;
            foreach (var i in RadTabControl.Items.OfType<RadTabItem>())
            {
                //   var form = i.ChildrenOfType<RadDataForm>().FirstOrDefault();
                if (i.Content is RadDataForm form)
                {
                     SettingsViewModel.HideBorder(form);
                }
            }
        }

        public SettingsViewModel Model => DataContext as SettingsViewModel;
    }
}