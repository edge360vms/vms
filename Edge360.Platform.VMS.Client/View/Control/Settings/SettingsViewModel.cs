﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Settings;
using log4net;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.Settings
{
    public class SettingsViewModel : ViewModelBase
    {
        private ICommand _cancelSettingsCommand;
        private RelayCommand _clearServerListCommand;
        private ICommand _okSettingsCommand;
        private ICommand _saveSettingsCommand;
        private ILog _log;

        public AdvancedSettings Advanced => SettingsManager.Settings.Advanced;

        public AnalyticSettings Analytics => SettingsManager.Settings.Analytics;

        public CameraSettings Camera => SettingsManager.Settings.Camera;

        public ICommand CancelSettingsCommand
        {
            get
            {
                return _cancelSettingsCommand ??= new RelayCommand(p => CancelSettings(), p => CanCancel());
            }
        }

        public RelayCommand ClearServerListCommand => _clearServerListCommand ??= new RelayCommand(o => ClearAddressList(),
            o => CanClearServerList());

        public string ClientVersion
        {
            get
            {
                var assembly = Assembly.GetExecutingAssembly();
                var fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
                var version = fileVersionInfo.FileVersion;
                return version;
            }
        }

        public ExportSettings Exporting => SettingsManager.Settings.Exporting;

        public GeneralSettings General => SettingsManager.Settings.General;

        //   public RecoverySettings Recovery => SettingsManager.Settings.Recovery;

        public LoginSettings Connection => SettingsManager.Settings.Login;
        public AppSettings Settings => SettingsManager.Settings;

        public MiscellaneousSettings Miscellaneous => SettingsManager.Settings.Miscellaneous;

        public MonitorSettings Monitoring => SettingsManager.Settings.Monitoring;

        public ICommand OkSettingsCommand => _okSettingsCommand ??= new RelayCommand(p =>
        {
            SaveSettings();
            CancelSettings();
        }, p => CanSave());


        public ICommand SaveSettingsCommand
        {
            get
            {
                return _saveSettingsCommand ??= new RelayCommand(p => SaveSettings(), p => CanSave());
            }
        }

        public SettingsView View { get; set; }

        public SettingsViewModel(ILog log = null)
        {
            _log = log;
           

            _log?.Info(Miscellaneous);
        }

        private void ClearAddressList()
        {
            try
            {
                //  ObjectResolver.ResolveAll<LoginViewModel>()?.ForEach(e => e?);
                var credentials = ObjectResolver.Resolve<LoginViewModel>()?.Credential;
                if (credentials != null)
                {
                    //credentials.Domain = string.Empty;
                    credentials.LoginServers?.Clear();
                }

                SettingsManager.Settings?.Login?.LoginServers?.Clear();
            }
            catch (Exception)
            {
            }
        }

        private bool CanClearServerList()
        {
            return true;
        }


        /// <summary>
        ///     Dirty way to hide view once it is found...
        /// </summary>
        public static void HideBorder(FrameworkElement element)
        {
            var tcs = new TaskCompletionSource<object>();

            void OnLayoutUpdate(object s, EventArgs e)
            {
                foreach (var c in element.ChildrenOfType<Border>())
                {
                    if (c.Name == "FooterPanel_Background")
                    {
                        c.Visibility = Visibility.Hidden;
                        element.LayoutUpdated -= OnLayoutUpdate;
                        tcs.SetResult(null);
                        break;
                    }
                }
                //var grid = element.FindChild<Grid>("", true);
                //if (grid != null)
                //{
                //    if (grid.FindName("FooterPanel_Background") is Border brdr)
                //    {

                //    }
                //}
            }

            element.LayoutUpdated += OnLayoutUpdate;
        }

        private bool CanCancel()
        {
            return true;
        }

        private void CancelSettings()
        {
            View?.GetParentRadWindow<RadWindow>()?.Close();
        }

        private bool CanSave()
        {
            return true;
        }

        private void SaveSettings()
        {
            SettingsManager.SaveSettings();
        }
    }
}