﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.View.Control.Dialog;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using ServiceStack;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Control.TabbedWindow
{
    /// <summary>
    ///     Interaction logic for TabbedWindowStripControl.xaml
    /// </summary>
    public partial class TabbedWindowStripControl : UserControl
    {
        public TabbedWindowStripControl()
        {
            InitializeComponent();
            Model.View = this;
        }

        public TabbedWindowStripControlViewModel Model => DataContext as TabbedWindowStripControlViewModel;
    }

    public class TabbedWindowStripControlViewModel : INotifyPropertyChanged
    {
        private bool _isMainTabbedWindow;
        private DirectModeManager _modeManager;
        private DelegateCommand _logoutCommand;

        public bool IsMainTabbedWindow => View?.GetVisualParent<Window.TabbedWindow>() != null;

        public CreateUserDialogItemModel LoginInformation =>
            CommunicationManager.CurrentUser?.ConvertTo<CreateUserDialogItemModel>();

        public TabbedWindowStripControl View { get; set; }

        public TabbedWindowStripControlViewModel(DirectModeManager  modeManager = null)
        {
            ModeManager = modeManager;
        }

        public DirectModeManager ModeManager
        {
            get => _modeManager;
            set
            {
                if (Equals(value, _modeManager)) return;
                _modeManager = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand LogoutCommand => _logoutCommand ??= new DelegateCommand(Logout);

        private void Logout(object obj)
        {
            try
            {
                var tabbedWindow = ObjectResolver.Resolve<Window.TabbedWindow>();
                tabbedWindow.SkipLogOutPrompt = true;
                tabbedWindow?.Model?.LogOut(true);
                ModeManager.IsOfflineMode = false;
            }
            catch (Exception e)
            {
                
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}