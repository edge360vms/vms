﻿using System.Windows;
using System.Windows.Controls;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;

namespace Edge360.Platform.VMS.Client.View.Canvas
{
    internal class TextOverlayCanvas : ContentControl, IView
    {
        public static readonly DependencyProperty VideoControlProperty = DependencyProperty.Register(
            "VideoControl", typeof(EdgeVideoControl), typeof(TextOverlayCanvas),
            new PropertyMetadata(default(EdgeVideoControl)));

        public EdgeVideoControl VideoControl
        {
            get => (EdgeVideoControl) GetValue(VideoControlProperty);
            set => SetValue(VideoControlProperty, value);
        }

        public TextOverlayCanvas()
        {
            DataContext = new TextOverlayViewModel(this);
        }
    }
}