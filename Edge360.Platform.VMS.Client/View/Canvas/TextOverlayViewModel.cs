﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Common.Util;

namespace Edge360.Platform.VMS.Client.View.Canvas
{
    internal class TextOverlayViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private string _overlayArchiveTimeString;
        private DateTime _overlayTime;
        private string _overlayTimeString;
        private DispatcherTimer _updateTick;
        private TextOverlayCanvas _view;

        public string OverlayArchiveTimeString
        {
            get => _overlayArchiveTimeString;
            set
            {
                if (_overlayArchiveTimeString != value)
                {
                    _overlayArchiveTimeString = value;
                    OnPropertyChanged();
                }
            }
        }

        public DateTime OverlayTime
        {
            get => _overlayTime;
            set
            {
                if (_overlayTime != value)
                {
                    _overlayTime = value;
                    OnPropertyChanged();
                }
            }
        }

        public string OverlayTimeString
        {
            get => _overlayTimeString;
            set
            {
                if (_overlayTimeString != value)
                {
                    _overlayTimeString = value;
                    OnPropertyChanged();
                }
            }
        }

        public DispatcherTimer UpdateTick
        {
            get => _updateTick;
            set
            {
                if (_updateTick != value)
                {
                    _updateTick = value;
                    OnPropertyChanged();
                }
            }
        }

        public TextOverlayCanvas View
        {
            get => _view;
            set
            {
                if (_view != value)
                {
                    _view = value;
                    OnPropertyChanged();
                }
            }
        }

        public TextOverlayViewModel(TextOverlayCanvas textOverlayCanvas)
        {
            View = textOverlayCanvas;

            UpdateTick = new DispatcherTimer(TimeSpan.FromMilliseconds(500), DispatcherPriority.Render,
                UpdateTextOverlayTick, Dispatcher.CurrentDispatcher);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        private void UpdateTextOverlayTick(object sender, EventArgs e)
        {
            if (View?.VideoControl?.VideoComponent != null)
            {
                if (View.VideoControl.EdgeVideoTimeline != null)
                {
                    OverlayTime = View.VideoControl.EdgeVideoTimeline.CursorTime;
                    OverlayTimeString =
                        $"{OverlayTime.ToString("MM/dd/yyyy HH:mm:ss")} ({TimeUtil.GetTimezoneAbbreviation(TimeZoneInfo.Local)})";
                    OverlayArchiveTimeString = DateTime.Now.Subtract(OverlayTime).PeriodOfTimeOutput();
                }
            }
        }
    }
}