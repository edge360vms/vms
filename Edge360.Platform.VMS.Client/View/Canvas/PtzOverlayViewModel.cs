﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.View.Canvas.Geometry.Edge360.Video.Monitor.Catapult.Core.Timeline.Geometry;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Common.Enums.Settings;

namespace Edge360.Platform.VMS.Client.View.Canvas
{
    internal class PtzOverlayViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private readonly PtzOverlayCanvas _canvas;
        private readonly EdgeVideoControl _control;
        private EPtzControlType _controlType = EPtzControlType.Arrow;
        private ArrowLine _arrowLine;

        public ArrowLine ArrowLine
        {
            get => _arrowLine;
            set => _arrowLine = value;
        }

        public EPtzControlType ControlType
        {
            get => _controlType;
            set
            {
                if (value == _controlType) return;
                _controlType = value;
                OnPropertyChanged();
            }
        }

        public PtzOverlayViewModel(EdgeVideoControl control, PtzOverlayCanvas canvas)
        {
            _control = control;
            _canvas = canvas;
            _control.MainGrid.MouseDown += MainGridOnMouseDown;
            ArrowLine = new ArrowLine
            {
                StrokeThickness = 5,
                X1 = 0,
                X2 = 0,
                Y1 = 0,
                Y2 = 0,
                ArrowEnds = ArrowEnds.End,
                Stroke = new SolidColorBrush(Color.FromArgb(200, 145, 200, 255))
            };
        }

        private void MainGridOnMouseDown(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            //if (Settings.AllowPtz == false)
            //{
            //    return;
            //}

            if (!_control.IsPlaying)
            {
                _canvas.Children.Clear();
                _canvas.Visibility = Visibility.Hidden;
                return;
            }
            //if (!_useTestArrow)
            //{
            //    if (!AllowArrowDrag || !IsPlayingLive())
            //    {
            //        _canvas.Children.Clear();
            //        _canvas.Visibility = Visibility.Hidden;
            //        return;
            //    }
            //}

            if (mouseButtonEventArgs.ChangedButton != MouseButton.Left)
            {
                return;
            }

            if (_canvas != null)
            {
                _canvas.Children.Clear();
                _canvas.InvalidateVisual();
                _canvas.Visibility = Visibility.Visible;



                //_canvas.PtzStartPoint =
                //    PlayerView.MainGrid.PointFromScreen(new Point(MousePosition.X,
                //        MousePosition.Y));
                //Mouse.OverrideCursor = Cursors.None;

                //Task.Run(async () =>
                //{
                //    MouseWasHeld = false;
                //    PlayerView.ArrowOverlayCanvas.StartingPoint = PtzStartPoint;
                //    _canvas.StartingPoint = PtzStartPoint;

                //    await Task.Delay(150);
                //    while (ArrowPtzMouseDown)
                //    {
                //        MouseWasHeld = true;
                //        await Task.Delay(20);
                //        PlayerView.ArrowOverlayCanvas.WpfUiThread(() =>
                //        {
                //            var currentPoint =
                //                PlayerView.MainGrid.PointFromScreen(new Point(MousePosition.X, MousePosition.Y));
                //            LastKnownPoint = currentPoint;
                //            PlayerView.ArrowOverlayCanvas.CurrentPoint = currentPoint;
                //            PlayerView.ArrowOverlayCanvas.InvalidateVisual();

                //            _canvas.CurrentPoint = currentPoint;
                //            _canvas.InvalidateVisual();


                //            //VideoSource?.PtzManager?.MovePtzToPoint(
                //            //    new Point(Math.Max(0, pointFromCenterX),
                //            //        Math.Max(0, pointFromCenterY)),
                //            //    new Size(VLCHost.clickableGrid.RenderSize.Width, VLCHost.clickableGrid.RenderSize.Height));
                //            //http://10.100.0.56/axis-cgi/com/ptz.cgi?zoom=-20
                //            //http://10.100.0.56/axis-cgi/com/ptz.cgi?rpan=5
                //        }, DispatcherPriority.Render);
                //    }


                //    _canvas.WpfUiThread(() =>
                //    {
                //        if (MouseWasHeld && _canvas.StartingPoint.X != -1)
                //        {
                //            if (!Settings.SourceResolution.IsEmpty)
                //            {
                //                //var pointFromCenterX = GetPreviewRectangleSize().Width / 2 -
                //                //                       PtzStartPoint.X + LastKnownPoint.X;
                //                //var pointFromCenterY = GetPreviewRectangleSize().Height / 2 -
                //                //                       PtzStartPoint.Y + LastKnownPoint.Y;

                //                //var xScale = Settings.SourceResolution.Width /
                //                //             PlayerView?.MainGrid?.RenderSize.Width;
                //                //var yScale = Settings.SourceResolution.Height /
                //                //             PlayerView?.MainGrid?.RenderSize.Height;

                //                //var zoom = Settings.SourceResolution.Width / PlayerView?.MainGrid?.RenderSize.Width;


                //                //if (xScale != null)
                //                //{
                //                //    if (yScale != null)
                //                //    {
                //                var mainSize = PlayerView.MainGrid.RenderSize;
                //                var imageSize = GetImageBounds();

                //                if (!imageSize.IsEmpty)
                //                {
                //                    var xOffset = (mainSize.Width -
                //                                   imageSize.Width) / 2;
                //                    var yOffset = (mainSize.Height -
                //                                   imageSize.Height) / 2;

                //                    var distance = Math.Abs(PtzStartPoint.X - LastKnownPoint.X) +
                //                                   Math.Abs(PtzStartPoint.Y - LastKnownPoint.Y);
                //                    {
                //                        if (distance < 15)
                //                        {
                //                            RequestPtzMove((int) (PtzStartPoint.X - xOffset),
                //                                (int) (PtzStartPoint.Y - yOffset)
                //                            );
                //                        }
                //                        else
                //                        {
                //                            var widthMagnitude = Math.Sqrt(
                //                                Math.Pow(LastKnownPoint.X - PtzStartPoint.X, 2) +
                //                                Math.Pow(LastKnownPoint.Y - PtzStartPoint.Y, 2));

                //                            var zoomFromWidth =
                //                                GetImageBounds().Width /
                //                                (float) widthMagnitude *
                //                                100; //GetPreviewResolution().Width / Math.Abs(PtzStartPoint.X - currentPoint.X));
                //                            RequestPtzZoomMove((int) (PtzStartPoint.X - xOffset),
                //                                (int) (PtzStartPoint.Y - yOffset),
                //                                (int) zoomFromWidth);
                //                        }
                //                    }
                //                }
                //            }
                //        }

                //        Mouse.OverrideCursor = null;
                //        PlayerView.ArrowOverlayCanvas.InvalidateVisual();
                //        PlayerView.IronYunCanvas.Visibility = Visibility.Visible;
                //        PlayerView.ArrowOverlayCanvas.StartingPoint = new Point(-1, -1);
                //        _canvas.CurrentPoint = new Point(-1, -1);
                //        _canvas.InvalidateVisual();
                //        _canvas.StartingPoint = new Point(-1, -1);
                //    });
                //});
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}