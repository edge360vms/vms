﻿using System;
using System.Windows;
using System.Windows.Media;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Common.Enums.Settings;
using Size = System.Drawing.Size;

namespace Edge360.Platform.VMS.Client.View.Canvas
{
    internal class PtzOverlayCanvas : System.Windows.Controls.Canvas, IDisposable, IView
    {
        // private readonly ArrowLine arrowLine;
        private readonly SolidColorBrush _brush;
        private EdgeVideoControl _owner;
        public Rect DrawnRectangle;
        public Point CurrentPoint { get; set; } = new Point(-1, -1);

        public Size ImageResolution { get; set; }
        public Point StartingPoint { get; set; } = new Point(-1, -1);

        public PtzOverlayViewModel Model => DataContext as PtzOverlayViewModel;

        public PtzOverlayCanvas()
        {
            _brush = new SolidColorBrush(Color.FromArgb(244, 225, 50, 55));
            IsHitTestVisible = true;
        }

        public void Dispose()
        {
        }

        public void SetOwner(EdgeVideoControl owner)
        {
            _owner = owner;
            DataContext = new PtzOverlayViewModel(owner, this);
        }

        protected override void OnRender(DrawingContext dc)
        {
            if (_owner != null)
            {
                if (Model.ControlType == EPtzControlType.Arrow && _owner?.Model != null && _owner.Model.IsMouseDown)
                {
                    if (StartingPoint.X != -1)
                    {
                        Model.ArrowLine.X1 = StartingPoint.X;
                        Model.ArrowLine.X2 = CurrentPoint.X;
                        Model.ArrowLine.Y1 = StartingPoint.Y;
                        Model.ArrowLine.Y2 = CurrentPoint.Y;
                        dc.DrawGeometry(Brushes.Transparent, new Pen(Model.ArrowLine.Stroke, 5), Model.ArrowLine.Geometry);
                    }
                }
                else
                if (StartingPoint.X != -1 && _owner?.Model != null && _owner.Model.IsMouseDown)
                {
                    var magnitude = Math.Sqrt(Math.Pow(CurrentPoint.X - StartingPoint.X, 2) +
                                              Math.Pow(CurrentPoint.Y - StartingPoint.Y, 2));

                    var width = magnitude;
                    var height = width / 16 * 9;

                    var topLeft = new Point(StartingPoint.X - width / 2, StartingPoint.Y - height / 2);

                    DrawnRectangle = new Rect(topLeft.X, topLeft.Y, width, height);
                    dc.DrawRectangle(Brushes.Transparent, new Pen(_brush, 2),
                        DrawnRectangle);
                }
            }

            //  base.OnRender(dc);
        }
    }
}