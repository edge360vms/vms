﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Edge360.Platform.VMS.Client.View.Canvas.Geometry.Edge360.Video.Monitor.Catapult.Core.Timeline.Geometry;

/// <summary>
///     Indicates which end of the line has an arrow.
/// </summary>
[Flags]
public enum ArrowEnds
{
    None = 0,
    Start = 1,
    End = 2,
    Both = 3
}


/// <summary>
///     Provides a base class for ArrowLine and ArrowPolyline.
///     This class is abstract.
/// </summary>
public abstract class ArrowLineBase : Shape
{
    /// <summary>
    ///     Identifies the ArrowAngle dependency property.
    /// </summary>
    public static readonly DependencyProperty ArrowAngleProperty =
        DependencyProperty.Register("ArrowAngle",
            typeof(double), typeof(ArrowLineBase),
            new FrameworkPropertyMetadata(45.0,
                FrameworkPropertyMetadataOptions.AffectsMeasure));

    /// <summary>
    ///     Identifies the ArrowLength dependency property.
    /// </summary>
    public static readonly DependencyProperty ArrowLengthProperty =
        DependencyProperty.Register("ArrowLength",
            typeof(double), typeof(ArrowLineBase),
            new FrameworkPropertyMetadata(12.0,
                FrameworkPropertyMetadataOptions.AffectsMeasure));

    /// <summary>
    ///     Identifies the ArrowEnds dependency property.
    /// </summary>
    public static readonly DependencyProperty ArrowEndsProperty =
        DependencyProperty.Register("ArrowEnds",
            typeof(ArrowEnds), typeof(ArrowLineBase),
            new FrameworkPropertyMetadata(ArrowEnds.End,
                FrameworkPropertyMetadataOptions.AffectsMeasure));

    /// <summary>
    ///     Identifies the IsArrowClosed dependency property.
    /// </summary>
    public static readonly DependencyProperty IsArrowClosedProperty =
        DependencyProperty.Register("IsArrowClosed",
            typeof(bool), typeof(ArrowLineBase),
            new FrameworkPropertyMetadata(false,
                FrameworkPropertyMetadataOptions.AffectsMeasure));

    private readonly PathFigure _pathfigHead1;
    private readonly PathFigure _pathfigHead2;
    private readonly PolyLineSegment _polysegHead1;
    private readonly PolyLineSegment _polysegHead2;
    protected PathFigure PathfigLine;
    protected PathGeometry Pathgeo;
    protected PolyLineSegment PolysegLine;

    /// <summary>
    ///     Gets or sets the angle between the two sides of the arrowhead.
    /// </summary>
    public double ArrowAngle
    {
        set => SetValue(ArrowAngleProperty, value);
        get => (double) GetValue(ArrowAngleProperty);
    }

    /// <summary>
    ///     Gets or sets the property that determines which ends of the
    ///     line have arrows.
    /// </summary>
    public ArrowEnds ArrowEnds
    {
        set => SetValue(ArrowEndsProperty, value);
        get => (ArrowEnds) GetValue(ArrowEndsProperty);
    }

    /// <summary>
    ///     Gets or sets the length of the two sides of the arrowhead.
    /// </summary>
    public double ArrowLength
    {
        set => SetValue(ArrowLengthProperty, value);
        get => (double) GetValue(ArrowLengthProperty);
    }

    /// <summary>
    ///     Gets or sets the property that determines if the arrow head
    ///     is closed to resemble a triangle.
    /// </summary>
    public bool IsArrowClosed
    {
        set => SetValue(IsArrowClosedProperty, value);
        get => (bool) GetValue(IsArrowClosedProperty);
    }

    /// <summary>
    ///     Gets a value that represents the Geometry of the ArrowLine.
    /// </summary>
    protected override Geometry DefiningGeometry
    {
        get
        {
            var count = PolysegLine.Points.Count;

            if (count > 0)
            {
                // Draw the arrow at the start of the line.
                if ((ArrowEnds & ArrowEnds.Start) == ArrowEnds.Start)
                {
                    var pt1 = PathfigLine.StartPoint;
                    var pt2 = PolysegLine.Points[0];
                    Pathgeo.Figures.Add(CalculateArrow(_pathfigHead1, pt2, pt1));
                }

                // Draw the arrow at the end of the line.
                if ((ArrowEnds & ArrowEnds.End) == ArrowEnds.End)
                {
                    var pt1 = count == 1 ? PathfigLine.StartPoint : PolysegLine.Points[count - 2];
                    var pt2 = PolysegLine.Points[count - 1];
                    Pathgeo.Figures.Add(CalculateArrow(_pathfigHead2, pt1, pt2));
                }
            }

            return Pathgeo;
        }
    }

    /// <summary>
    ///     Initializes a new instance of ArrowLineBase.
    /// </summary>
    public ArrowLineBase()
    {
        Pathgeo = new PathGeometry();

        PathfigLine = new PathFigure();
        PolysegLine = new PolyLineSegment();
        PathfigLine.Segments.Add(PolysegLine);

        _pathfigHead1 = new PathFigure();
        _polysegHead1 = new PolyLineSegment();
        _pathfigHead1.Segments.Add(_polysegHead1);

        _pathfigHead2 = new PathFigure();
        _polysegHead2 = new PolyLineSegment();
        _pathfigHead2.Segments.Add(_polysegHead2);
    }

    private PathFigure CalculateArrow(PathFigure pathfig, Point pt1, Point pt2)
    {
        var matx = new Matrix();
        var vect = pt1 - pt2;
        vect.Normalize();
        vect *= ArrowLength;

        if (pathfig.Segments[0] is PolyLineSegment polyseg)
        {
            polyseg.Points.Clear();
            matx.Rotate(ArrowAngle / 2);
            pathfig.StartPoint = pt2 + vect * matx;
            polyseg.Points.Add(pt2);

            matx.Rotate(-ArrowAngle);
            polyseg.Points.Add(pt2 + vect * matx);
        }

        pathfig.IsClosed = IsArrowClosed;

        return pathfig;
    }
}

namespace Edge360.Platform.VMS.Client.View.Canvas.Geometry
{
    #region

    #endregion

    public class ArrowOverlayCanvas : System.Windows.Controls.Canvas
    {
        private readonly ArrowLine _arrowLine;

        public Point CurrentPoint { get; set; }
        public Point StartingPoint { get; set; }

        public ArrowOverlayCanvas()
        {
            IsHitTestVisible = true;
            _arrowLine = new ArrowLine
            {
                StrokeThickness = 5,
                X1 = StartingPoint.X,
                X2 = CurrentPoint.X,
                Y1 = StartingPoint.Y,
                Y2 = CurrentPoint.Y,
                ArrowEnds = ArrowEnds.End,
                Stroke = new SolidColorBrush(Color.FromArgb(200, 145, 200, 255))
            };
        }

        protected override void OnRender(DrawingContext dc)
        {
            if (StartingPoint.X != -1)
            {
                _arrowLine.X1 = StartingPoint.X;
                _arrowLine.X2 = CurrentPoint.X;
                _arrowLine.Y1 = StartingPoint.Y;
                _arrowLine.Y2 = CurrentPoint.Y;
                dc.DrawGeometry(Brushes.Transparent, new Pen(_arrowLine.Stroke, 5), _arrowLine.Geometry);
            }

            //base.OnRender(dc);
        }
    }

    namespace Edge360.Video.Monitor.Catapult.Core.Timeline.Geometry
    {
        /// <summary>
        ///     Draws a straight line between two points with
        ///     optional arrows on the ends.
        /// </summary>
        public class ArrowLine : ArrowLineBase
        {
            /// <summary>
            ///     Identifies the X1 dependency property.
            /// </summary>
            public static readonly DependencyProperty X1Property =
                DependencyProperty.Register("X1",
                    typeof(double), typeof(ArrowLine),
                    new FrameworkPropertyMetadata(0.0,
                        FrameworkPropertyMetadataOptions.AffectsMeasure));

            /// <summary>
            ///     Identifies the Y1 dependency property.
            /// </summary>
            public static readonly DependencyProperty Y1Property =
                DependencyProperty.Register("Y1",
                    typeof(double), typeof(ArrowLine),
                    new FrameworkPropertyMetadata(0.0,
                        FrameworkPropertyMetadataOptions.AffectsMeasure));

            /// <summary>
            ///     Identifies the X2 dependency property.
            /// </summary>
            public static readonly DependencyProperty X2Property =
                DependencyProperty.Register("X2",
                    typeof(double), typeof(ArrowLine),
                    new FrameworkPropertyMetadata(0.0,
                        FrameworkPropertyMetadataOptions.AffectsMeasure));

            /// <summary>
            ///     Identifies the Y2 dependency property.
            /// </summary>
            public static readonly DependencyProperty Y2Property =
                DependencyProperty.Register("Y2",
                    typeof(double), typeof(ArrowLine),
                    new FrameworkPropertyMetadata(0.0,
                        FrameworkPropertyMetadataOptions.AffectsMeasure));

            public System.Windows.Media.Geometry Geometry => DefiningGeometry;

            /// <summary>
            ///     Gets or sets the x-coordinate of the ArrowLine start point.
            /// </summary>
            public double X1
            {
                set => SetValue(X1Property, value);
                get => (double) GetValue(X1Property);
            }

            /// <summary>
            ///     Gets or sets the x-coordinate of the ArrowLine end point.
            /// </summary>
            public double X2
            {
                set => SetValue(X2Property, value);
                get => (double) GetValue(X2Property);
            }

            /// <summary>
            ///     Gets or sets the y-coordinate of the ArrowLine start point.
            /// </summary>
            public double Y1
            {
                set => SetValue(Y1Property, value);
                get => (double) GetValue(Y1Property);
            }

            /// <summary>
            ///     Gets or sets the y-coordinate of the ArrowLine end point.
            /// </summary>
            public double Y2
            {
                set => SetValue(Y2Property, value);
                get => (double) GetValue(Y2Property);
            }

            /// <summary>
            ///     Gets a value that represents the Geometry of the ArrowLine.
            /// </summary>
            protected override System.Windows.Media.Geometry DefiningGeometry
            {
                get
                {
                    // Clear out the PathGeometry.
                    Pathgeo.Figures.Clear();

                    // Define a single PathFigure with the points.
                    PathfigLine.StartPoint = new Point(X1, Y1);
                    PolysegLine.Points.Clear();
                    PolysegLine.Points.Add(new Point(X2, Y2));
                    Pathgeo.Figures.Add(PathfigLine);

                    // Call the base property to add arrows on the ends.
                    return base.DefiningGeometry;
                }
            }
        }
    }
}