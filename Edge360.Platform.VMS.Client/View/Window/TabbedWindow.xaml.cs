﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;
using Edge360.Platform.VMS.Client.Extensions.CommandLine;
using Edge360.Platform.VMS.Client.ItemModel.Collection;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.Client.View.Control.TabbedWindow;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Common.Taskbar;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.UI.Common.Util;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Navigation;
using Telerik.Windows.Controls.TabbedWindow;
using static Edge360.Platform.VMS.Client.Extensions.ObjectResolver;

namespace Edge360.Platform.VMS.Client.View.Window
{
    /// <summary>
    ///     Interaction logic for TabbedWindow.xaml
    /// </summary>
    public partial class TabbedWindow : RadTabbedWindow
    {
        public static bool IsForceClosing { get; set; }

        public TabbedWindowViewModel Model => DataContext as TabbedWindowViewModel;
        public bool SkipLogOutPrompt { get; set; }

        public TabbedWindow()
        {
            InitializeComponent();
            //DataContext = new TabbedWindowViewModel(this);
            //Register<TabbedWindowViewModel>(DataContext);
            Model.TabWindow = this;
            Closed += OnClosed;
            AddingNewTab += OnAddingNewTab;
            PreviewTabClosed += OnPreviewTabClosed;
            TabClosed += OnTabClosed;
            ItemsChanged += OnItemsChanged;
            PreviewClosed += OnPreviewClosed;
            //HideBorder(this);
            AddTelemetry(this);
            if (CommandLineManager.StartupArgs.Layout != Guid.Empty ||
                SettingsManager.Settings.General.OpenMonitoringWindowOnStartup)
            {
                Model?.TabWindow?.OpenTab<MonitoringTab>();
            }
            
            if (CommandLineManager.StartupArgs.QuickExportProfiles != null)
            {
                Model?.TabWindow?.OpenTab<ExportArchiveTab>();
            }


            TabbedWindowCreating += OnTabbedWindowCreating;
            
          
        }

        /// <summary>
        ///     Dirty way to load Telemetry Module
        /// </summary>
        public static async void AddTelemetry(FrameworkElement element)
        {
            // return;
            await Task.Run(async () =>
            {
                var tcs = new TaskCompletionSource<object>();

                async void OnLayoutUpdate(object s, EventArgs e)
                {
                    if (element != null)
                    {
                        try
                        {
                            foreach (var c in element.ChildrenOfType<StackPanel>())
                            {
                                if (c.Name == "HeaderButtons")
                                {
                                    element.LayoutUpdated -= OnLayoutUpdate;
                                    tcs.SetResult(null);
                                    var _ = c.TryUiAsync(() =>
                                    {
                                        //new TelemetryControl.View.TelemetryControl
                                        //{MeterFontSize = 20, VerticalAlignment = VerticalAlignment.Center}

                                        try
                                        {
                                            c.Children.Insert(0, new TabbedWindowStripControl()
                                            );
                                        }
                                        catch (Exception exception)
                                        {
                                        }
                                    }, DispatcherPriority.Background).ConfigureAwait(false);

                                    break;
                                }
                            }
                        }
                        catch (Exception exception)
                        {
                        }
                    }
                }

                element.LayoutUpdated += OnLayoutUpdate;
            });
        }

        /// <summary>
        ///     Dirty way to hide control once it is found...
        /// </summary>
        public static void HideBorder(FrameworkElement element)
        {
            var tcs = new TaskCompletionSource<object>();

            void OnLayoutUpdate(object s, EventArgs e)
            {
                foreach (var c in element.ChildrenOfType<Border>())
                {
                    if (c.Name == "RightSeparator")
                    {
                        c.Margin = new Thickness(0);
                        c.BorderBrush = Brushes.Transparent;

                        element.LayoutUpdated -= OnLayoutUpdate;
                        tcs.SetResult(null);
                        break;
                    }
                }
            }

            element.LayoutUpdated += OnLayoutUpdate;
        }

        public TabbedWindowViewModel GetModel()
        {
            return (TabbedWindowViewModel) DataContext;
        }

        private void OnItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            try
            {
                //added tab!
                if (e?.NewItems != null && e.NewItems.Count > 0)
                {
                    var tabItem = e.NewItems.OfType<RadTabItem>().FirstOrDefault();
                    var parentWindow = tabItem?.GetParentWindow<System.Windows.Window>();
                    if (parentWindow != null && !parentWindow.IsActive)
                    {
                        parentWindow.GlobalActivate();
                    }
                }

                //Console.WriteLine($"Items changed {} vs {e?.OldItems?.Count}");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        private void OnPreviewTabClosed(object sender, PreviewTabChangedEventArgs e)
        {
            try
            {
                if (e.TabItem is EdgeTabItem t)
                {
                    if (!t.CheckClose())
                    {
                        var eCancel = !t.CloseConfirm();
                        e.Cancel = eCancel;
                        if (!eCancel)
                        {
                            t.Dispose();
                        }
                    }
                    else
                    {
                        t.Dispose();
                    }
                }
            }
            catch (Exception exception)
            {
                
            }
        }

        public void OnTabbedWindowCreating(object sender, TabbedWindowCreatingEventArgs e)
        {
            if (e.NewWindow != null)
            {
                
                Register<RadTabbedWindow>(e.NewWindow);
                //  _log?.Info("Registering new window...");
                RadWindowInteropHelper.SetAllowTransparency(e.NewWindow, false);
                e.NewWindow.IsContentPreserved = IsContentPreserved;
                e.NewWindow.Header = Header;
                e.NewWindow.Width /= 1.4f;
                e.NewWindow.ItemsPanel = ItemsPanel;
                e.NewWindow.Height /= 1.4f;
                e.NewWindow.DataContext = new TabbedWindowViewModel(Model.DirectModeManager)
                {

                    TabWindow = e.NewWindow
                };

                AddTelemetry(e.NewWindow);
                
                e.NewWindow.MinHeight = MinHeight;
                e.NewWindow.MinWidth = MinWidth;
                e.NewWindow.AddButtonStyle = AddButtonStyle;
                e.NewWindow.HeaderTemplate = HeaderTemplate;
                e.NewWindow.Resources = Resources;
                // e.NewWindow.InputBindings.AddRange(InputBindings);
                e.NewWindow.HeaderTemplateSelector = HeaderTemplateSelector;
                e.NewWindow.TabbedWindowCreating += OnTabbedWindowCreating;
                e.NewWindow.Closed += OnClosed;
                e.NewWindow.PreviewClosed += NewWindowOnPreviewClosed;
                e.NewWindow.TabClosed += OnTabClosed;
                e.NewWindow.PreviewTabClosed += OnPreviewTabClosed;
                e.NewWindow.AddingNewTab += OnAddingNewTab;
                e.NewWindow.ItemsChanged += OnItemsChanged;
                e.NewWindow.Loaded += (o, args) =>
                {
                    try
                    {
                        TaskbarUtil.SetLaunchTarget(
                            new WindowInteropHelper(e.NewWindow.GetParentWindow<System.Windows.Window>()).Handle);
                        e.NewWindow.GetParentWindow<System.Windows.Window>().GlobalActivate();
                    }
                    catch (Exception exception)
                    {
                    }
                };
            }
        }

        private void NewWindowOnPreviewClosed(object sender, WindowPreviewClosedEventArgs e)
        {
            if (sender is RadTabbedWindow window)
            {
                if (!IsForceClosing)
                {
                    foreach (var t in window.Items.OfType<EdgeTabItem>())
                    {
                        if (!t.CheckClose())
                        {
                            t.CloseConfirm();
                            Console.WriteLine($"{t.Header} stopped the Closing...");
                            e.Cancel = true;
                            return;
                        }
                    }
                }
            }
        }

        private void OnPreviewClosed(object sender, WindowPreviewClosedEventArgs e)
        {
            if (IsForceClosing)
            {
                //
                if (SkipLogOutPrompt)
                {
                    SkipLogOutPrompt = false;
                    GetModel()?.LogOut();
                }
                else
                {
                    var closing = DialogUtil.ConfirmDialog("Are you sure you wish to logout?", true);
                    e.Cancel = !closing;
                    if (closing)
                    {
                        GetModel().LogOut();
                    }
                    else
                    {
                        IsForceClosing = false;
                    }
                }
            }
            else
            {
                GetModel().ExitApplication();
                e.Cancel = true;
            }

            //var closing = DialogUtil.ConfirmDialog("Are you sure you wish to logout?", true);
            //e.Cancel = !closing;
            //if (closing)
            //{
            //    //GetModel().LogOut();

            //    //   CloseWindow<RadTabbedWindow>(closeAll: true);
            //}
        }

        private void OnTabClosed(object sender, TabChangedEventArgs e)
        {
            if (sender is RadTabbedWindow window)
            {
                if (!(window is TabbedWindow) && window.Items.Count == 0)
                {
                    try
                    {
                        //window.Close();
                    }
                    catch (Exception exception)
                    {
                    }
                }
            }

            if (e.TabItem is EdgeTabItem t)
            {
                t.Dispose();
                //var tabType = t.GetType().FullName;
                RemoveEntry(t);
                if (t.DataContext != null)
                {
                    if (t.DataContext is IDisposable ctx)
                    {
                        ctx.Dispose();
                    }
                    RemoveEntry(t.DataContext);
                }
            }
        }

        private void OnAddingNewTab(object sender, AddingNewTabEventArgs e)
        {
            e.Cancel = true;
        }

        private void OnClosed(object sender, WindowClosedEventArgs windowClosedEventArgs)
        {
            if (sender is RadTabbedWindow window)
            {
                var _ = this.TryUiAsync(() =>
                {
                    foreach (var t in window.Items.OfType<RadTabItem>())
                    {
                        if (t is EdgeTabItem edgeTab)
                        {
                            edgeTab.Dispose();
                        }
                        RemoveEntry(t);
                        if (t.DataContext != null)
                        {
                            RemoveEntry(t.DataContext);
                        }
                    }

                    foreach (var v in window.Items.OfType<IDisposable>())
                    {
                        v.Dispose();
                    }

                    RemoveEntry(window);
                    if (window.DataContext != null)
                    {
                        RemoveEntry(window.DataContext);
                    }

                    window.Items.Clear();
                }).ConfigureAwait(false);
            }
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }
    }
}