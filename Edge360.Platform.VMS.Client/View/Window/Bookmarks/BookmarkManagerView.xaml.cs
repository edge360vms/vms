﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Client.ItemModel.Recording;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.View.Control.Management;
using Edge360.Platform.VMS.Client.View.Control.VideoControl;
using Edge360.Platform.VMS.Client.View.Control.VideoControl.Timeline;
using Edge360.Platform.VMS.Common.Enums.Video;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Cameras;
using Edge360.Vms.Shared.MessageModel.Enums;
using Prism.Commands;
using RecordingService.ServiceModel.Api.TimelineEvents;
using ServiceStack;
using log4net;
using ServiceStack.Text;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Data.DataForm;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Client.View.Window.Bookmarks
{
    /// <summary>
    ///     Interaction logic for BookmarkManagerView.xaml
    /// </summary>
    public partial class BookmarkManagerView : RadWindow
    {
        public BookmarkManagerViewModel Model => DataContext as BookmarkManagerViewModel;

        public BookmarkManagerView()
        {
            InitializeComponent();
        }

        private async void RadDataForm_OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction == EditAction.Commit)
            {
                try
                {
                    await Model.UpdateTimelineItem();
                }
                catch (Exception exception)
                {
                }
            }
        }


        private void DeleteSelectedBookmarks(object sender, RoutedEventArgs e)
        {
            try
            {
                var timelineEventItemModels = GridView.SelectedItems.OfType<TimelineEventItemModel>().ToList();
                var singleDelete = timelineEventItemModels.Count() <= 1 ? timelineEventItemModels.FirstOrDefault() : null; 
                var dialog = DialogUtil.ConfirmDialog( singleDelete != null ? 
                    $"Are you sure you wish to delete the bookmark?\n'{singleDelete.Description}'" : $"Are you sure you wish to delete ({timelineEventItemModels.Count}) bookmarks?", true);
                if (dialog)
                {
                    foreach (var r in timelineEventItemModels)
                    {
                        Model.DeleteBookmark(r, false);
                    }
                }

            }
            catch (Exception exception)
            {
                
            }
        }

        private void AddBookmark(object sender, RoutedEventArgs e)
        {
            if (Model.SelectedCamera != null)
            {
                Model.AddBookmark();
            }
        }

        private async void CameraTreeSelection_OnLoadOnDemand(object sender, RadRoutedEventArgs e)
        {
            try
            {
                if (e.OriginalSource is RadTreeViewItem treeItem)
                {
                    try
                    {
                        var serverItemModel = ServerItemManager.Manager.ItemCollection.GetType();
                      //  var type = Model.SelectedZone.ServerCollectionViewSource.CollectionViewType;

                      dynamic treeItemItem = treeItem.Item;
                      if (treeItemItem.GetType() == serverItemModel)
                      {

                      }

                        treeItem.IsLoadingOnDemand = true;
                        if (treeItemItem.Value is ServerItemModel serverItem)
                        {
                            var listCameras = await CameraItemModelManager.Manager.RefreshCameras((Guid) serverItem.ZoneId);
                            serverItem.Archiver ??= new ServerConfigItemModel();
                            serverItem.Archiver.ActiveCameras = new ObservableCollection<CameraItemModel>(
                                listCameras?.Select(o => (CameraItemModel) o) ?? new List<CameraItemModel>());
                        }
                        else if (treeItemItem is ZoneItemModel zoneItem)
                        {
                        }
                        else if (treeItemItem is CameraItemModel cameraItem)
                        {
                        }
                    }
                    catch (Exception exception)
                    {

                    }
                    finally
                    {
                        await Task.Delay(100);
                        treeItem.IsLoadingOnDemand = false;
                    }
                }
            }
            catch (Exception exception)
            {
                
            }
        }

        private async void RefreshCameraTreeClicked(object sender, RoutedEventArgs e)
        {
            if (Model.SpawnControl != null)
            {
                //await ZoneCameraManagementViewModel.LoadZones(Model.Zones, true);
                //foreach (var z in Model.Zones)
                //{
                //    await ServerItemManager.ServerManager.RefreshServers(z.ZoneId);
                //    // await ZoneCameraManagementViewModel.LoadServers(z.Servers, Model.SpawnControl.VideoObject.ZoneId);
                //}
                
            }
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }

    public class BookmarkManagerViewModel : INotifyPropertyChanged
    {
        private DelegateCommand<object> _bookmarkManagerActionCommand;

        private ObservableCollection<TimelineEventItemModel> _bookmarks =
            new ObservableCollection<TimelineEventItemModel>();

        private DelegateCommand<TimelineEventItemModel> _deleteTimelineCommand;
        private ILog _log;

        private ObservableCollection<CameraItemModel> _resolvedCameras = new ObservableCollection<CameraItemModel>();
        private TimelineEventItemModel _selectedBookmark;
        private CameraItemModel _selectedCamera;
        private ZoneItemModel _selectedZone;
        private EdgeVideoControl _spawnControl;
        private ObservableCollection<ZoneItemModel> _zones = new ObservableCollection<ZoneItemModel>();
        private object _selectedTreeItem;
        private ServerItemModel _selectedServer;
        private DelegateCommand<TimelineEventItemModel> _jumpToBookmarkCommand;

        public DelegateCommand<object> BookmarkManagerActionCommand =>
            _bookmarkManagerActionCommand ??= new DelegateCommand<object>(o => ExecuteBookmarkAction(o));

        public ObservableCollection<TimelineEventItemModel> Bookmarks
        {
            get => _bookmarks;
            set
            {
                if (Equals(value, _bookmarks))
                {
                    return;
                }

                _bookmarks = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<CameraItemModel> Cameras
        {
            get => _resolvedCameras;
            set
            {
                if (Equals(value, _resolvedCameras))
                {
                    return;
                }

                _resolvedCameras = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand<TimelineEventItemModel> DeleteTimelineCommand => _deleteTimelineCommand ??=
            new DelegateCommand<TimelineEventItemModel>(model => DeleteBookmark(model, true));

        public TimelineEventItemModel SelectedBookmark
        {
            get => _selectedBookmark;
            set
            {
                if (Equals(value, _selectedBookmark))
                {
                    return;
                }

                _selectedBookmark = value;
                OnPropertyChanged();
            }
        }

        public CameraItemModel SelectedCamera
        {
            get => _selectedCamera;
            set
            {
                if (Equals(value, _selectedCamera))
                {
                    return;
                }

                _selectedCamera = value;
                if (value != null)
                {
                    LoadEventsForCamera(value);
                }

                OnPropertyChanged();
            }
        }

        public ZoneItemModel SelectedZone
        {
            get => _selectedZone;
            set
            {
                if (Equals(value, _selectedZone))
                {
                    return;
                }

                _selectedZone = value;
                OnPropertyChanged();
            }
        }

        public EdgeVideoControl SpawnControl
        {
            get => _spawnControl;
            set
            {
                if (Equals(value, _spawnControl))
                {
                    return;
                }

                _spawnControl = value;
                OnPropertyChanged();
            }
        }

        //public ObservableCollection<ZoneItemModel> Zones
        //{
        //    get => _zones;
        //    set
        //    {
        //        if (Equals(value, _zones))
        //        {
        //            return;
        //        }

        //        _zones = value;
        //        OnPropertyChanged();
        //    }
        //}

        public object SelectedTreeItem
        {
            get => _selectedTreeItem;
            set
            {
                if (Equals(value, _selectedTreeItem)) return;
                if (value is CameraItemModel cam)
                {
                    SelectedCamera = cam;
                }
                else if (value is ServerItemModel server)
                {
                    SelectedServer = server;
                }
                else if (value is ZoneItemModel zone)
                {
                    SelectedZone = zone;
                }
                _selectedTreeItem = value;
                OnPropertyChanged();
            }
        }

        public ServerItemModel SelectedServer
        {
            get => _selectedServer;
            set
            {
                if (Equals(value, _selectedServer)) return;
                _selectedServer = value;
                OnPropertyChanged();
            }
        }

        public BookmarkManagerViewModel(ILog log = null, ZoneItemManager zoneItemManager = null)
        {
            _log = log;
            ZoneItemMgr = zoneItemManager;
            HubManager.SignalRClientEvent += HubManagerOnSignalRClientEvent;
        }

        public ZoneItemManager ZoneItemMgr { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public async Task UpdateTimelineItem()
        {
            var timelineEventItemModel = SelectedBookmark;
            if (timelineEventItemModel != null)
            {
                var updateTimelineEvent = timelineEventItemModel.ConvertTo<UpdateTimelineEvent>();
                updateTimelineEvent.TimestampUtc = updateTimelineEvent.TimestampUtc.FixServiceStackTime();

                var updateResponse = await GetManagerByZoneId(SelectedCamera.ZoneId).TimelineEventService
                    .UpdateTimelineEvent(updateTimelineEvent);
            }
        }

        private async void LoadEventsForCamera(CameraItemModel camera)
        {
            try
            {
                var previousSelectedBookmark = SelectedBookmark;
                if (camera != null)
                {
                    var timelineEvents = await GetManagerByZoneId(camera.ZoneId).TimelineEventService
                        .SearchTimelineEvents(new SearchTimelineEvents {CameraId = camera.Id});
                    Bookmarks.Clear();
                    Bookmarks.AddRange(timelineEvents.TimelineEvents.Select(o => (TimelineEventItemModel) o) ?? new List<TimelineEventItemModel>());
                }

                if (previousSelectedBookmark != null)
                {
                    SelectedBookmark =
                        Bookmarks.FirstOrDefault(o => o.TimelineEventId == previousSelectedBookmark.TimelineEventId);
                }
            }
            catch (Exception e)
            {
            }
        }

        public DelegateCommand<TimelineEventItemModel> JumpToBookmarkCommand => _jumpToBookmarkCommand ??= new DelegateCommand<TimelineEventItemModel>(JumpToBookmark);

        private async void JumpToBookmark(TimelineEventItemModel obj)
        {
            try
            {
                if (SpawnControl != null)
                {
                    if (SpawnControl.VideoObject != SelectedCamera)
                    {
                        SpawnControl.VideoObject = SelectedCamera;
                        SpawnControl.Model.Play();
                        await Task.Delay(100);
                    }
                
                    SpawnControl.JumpToDate(obj.EventTime, SpawnControl.VideoComponent.PlayerState == EPlayerState.Paused);
                }
            }
            catch (Exception e)
            {
                
            }
        }

        private void ExecuteBookmarkAction(object obj)
        {
            if (obj is EBookmarkManagerAction action)
            {
                switch (action)
                {
                    case EBookmarkManagerAction.Refresh:
                        LoadEventsForCamera(SelectedCamera);
                        break;
                    case EBookmarkManagerAction.DeleteBookmark:
                        var dialog = DialogUtil.ConfirmDialog(
                            $"Are you sure you wish to delete the bookmark?\n'{SelectedBookmark.Description}'", true);
                        if (dialog)
                        {
                            DeleteBookmark(SelectedBookmark);
                        }
                        
                        break;
                    case EBookmarkManagerAction.AddBookmark:
                        break;
                    case EBookmarkManagerAction.GotoBookmark:
                        break;
                    default:
                        return;
                }
            }
        }

        public async void DeleteBookmark(TimelineEventItemModel bookmark, bool prompt = false)
        {
            try
            {
                if (!prompt || DialogUtil.ConfirmDialog(
                    $"Are you sure you wish to delete the bookmark?\n'{bookmark.Description}'", true))
                {
                    var camera = SelectedCamera.Id == bookmark.CameraId
                        ? SelectedCamera
                        : Cameras.FirstOrDefault(o => o.Id == bookmark.CameraId);
                    if (camera != null)
                    {
                        var timelineEvents = await GetManagerByZoneId(camera.ZoneId).TimelineEventService
                            .DeleteTimelineEvent(bookmark.ConvertTo<DeleteTimelineEvent>());
                        if (timelineEvents != null && timelineEvents.Success)
                        {
                            Bookmarks.Remove(bookmark);
                        }
                    }
                }

                //Bookmarks.Clear();
                //Bookmarks.AddRange(timelineEvents.TimelineEvents.Select(o => (TimelineEventItemModel)o));
            }
            catch (Exception e)
            {
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public async void AddBookmark()
        {
            var camera = SelectedCamera;
            if (camera != null)
            {
                try
                {
                    //TODO: This won't work globally
                    var dateTime = SpawnControl.EdgeVideoTimeline.CursorTime;
                    var dialog =
                        DialogUtil.InputDialog(
                            $"Please enter a description of this new Bookmark.\n{camera.Label} at {dateTime:G}");
                    if (dialog.Result)
                    {
                        var resp = await GetManagerByZoneId(camera.ZoneId).TimelineEventService.CreateTimelineEvent(
                            new CreateTimelineEvent
                            {
                                CameraId = camera.Id,
                                Description = dialog.Response,
                                Duration = 1,
                                EventType = "Bookmark",
                                Name = $"{camera.Label} Bookmark",
                                TimestampUtc = dateTime
                            });
                        if (resp != null)
                        {
                            //Application.Current?.Dispatcher?.InvokeAsync(() =>
                            //{
                            //    //var diag = DialogUtil.ConfirmDialog(
                            //    //    $"{resp.TimelineEventId} {resp.ResponseStatus}");
                            //});
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }
        }

        private async void HubManagerOnSignalRClientEvent(ESignalRClientEvent clientEvent, Guid ids, string data)
        {
            if (clientEvent.Equals(ESignalRClientEvent.TimelineEventCreated))
            {
                try
                {
                    var (cameraId, timelineId) = JsonSerializer.DeserializeFromString<Tuple<Guid, Guid>>(data);
                    if (SelectedCamera.Id == cameraId)
                    {
                        var timelineEvent = await GetManagerByZoneId().TimelineEventService
                            .GetTimelineEvent(new GetTimelineEvent {TimelineEventId = timelineId});
                        if (timelineEvent != null)
                        {
                            Bookmarks.Add(timelineEvent.TimelineEvent);
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }
            else if (clientEvent.Equals(ESignalRClientEvent.TimelineEventDeleted))
            {
                try
                {
                    var (cameraId, timelineId) = JsonSerializer.DeserializeFromString<Tuple<Guid, Guid>>(data);
                    if (SelectedCamera.Id == cameraId)
                    {
                        var timelineEvent = Bookmarks.FirstOrDefault(o => o.TimelineEventId == timelineId);
                        if (timelineEvent != null)
                        {
                            Bookmarks.Remove(timelineEvent);
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }
        }
    }

    internal enum EBookmarkManagerAction
    {
        Refresh,
        GotoBookmark,
        DeleteBookmark,
        AddBookmark,
    }
}