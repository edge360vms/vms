﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.Annotations;
using Edge360.Platform.VMS.Client.Extensions;
using Edge360.Platform.VMS.Client.Extensions.CommandLine;
using Edge360.Platform.VMS.Client.ItemModel.Authority;
using Edge360.Platform.VMS.Client.ItemModel.Collection;
using Edge360.Platform.VMS.Client.Manager;
using Edge360.Platform.VMS.Client.View.Control;
using Edge360.Platform.VMS.Client.View.Tab;
using Edge360.Platform.VMS.Client.View.Window.Management;
using Edge360.Platform.VMS.Client.ViewModel.View;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.Monitoring.Managers.DirectMode;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Vms.AuthorityService.SharedModel;
using Edge360.Vms.AuthorityService.SharedModel.Enums;
using log4net;
using ServiceStack;
using Telerik.Windows.Controls;
using Unity;
using static Edge360.Platform.VMS.Client.Util.WindowUtil;
using RadContextMenu = Telerik.WinControls.UI.RadContextMenu;

namespace Edge360.Platform.VMS.Client.View.Window
{
    public class TabbedWindowViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private ICommand _changePasswordCommand;

        private RelayCommand _closeTabActionCommand;
        private ICommand _exitApplicationCommand;
        private RelayCommand _goNextTabCommand;
        private RelayCommand _goPreviousTabCommand;
        private RelayCommand _gotoTabCommand;
        private LoginView _loginView;
        private ICommand _logOutCommand;
        private ICommand _openAlertsCommand;
        private ICommand _openAnalyticsCommand;
        private ICommand _openCameraManagementCommand;
        private Prism.Commands.DelegateCommand _openExportCommand;
        private ICommand _openImageEditorCommand;
        private ICommand _openLPRCommand;
        private Prism.Commands.DelegateCommand _openManagementCommand;
        private ICommand _openMappingCommand;
        private RelayCommand _openMonitoringCommand;
        private RelayCommand _openSettingsCommand;
        private ICommand _openUserManagementCommand;
        private ICommand _openWebCommand;

        private RadTabbedWindow _tabWindow;

        private ICommand _tabIconCommand;
        private RadContextMenu _tabIconContextMenu;

        private UserLoginItemModel _userLogin =
            SettingsManager.Settings?.Login?.Credentials?.ConvertTo<UserLoginItemModel>();

        private DirectModeManager _directModeManager;
        private ILog _log;
        private bool _autoHide = false;

        public ICommand ChangePasswordCommand
        {
            get
            {
                return _changePasswordCommand ??= new RelayCommand(p => ChangePassword(), p => CanChangePassword());
            }
        }

        public RelayCommand CloseTabActionCommand =>
            _closeTabActionCommand ??= new RelayCommand(CloseTab, CanCloseTab);

        public ICommand ExitApplicationCommand
        {
            get
            {
                return _exitApplicationCommand ??= new RelayCommand(p => ExitApplication(), p => CanExit());
            }
        }

        public RelayCommand GoNextTabCommand
        {
            get
            {
                return _goNextTabCommand ??= new RelayCommand(p => GoNextTab(), p => CanGoNextTab());
            }
        }

        public RelayCommand GoPreviousTabCommand
        {
            get
            {
                return _goPreviousTabCommand ??= new RelayCommand(p => GoPreviousTab(), p => CanGoPreviousTab());
            }
        }

        public RelayCommand GotoTabCommand
        {
            get
            {
                return _gotoTabCommand ??= new RelayCommand(GotoTab, p => CanGotoTab());
            }
        }

        public ICommand LogOutCommand
        {
            get { return _logOutCommand ??= new RelayCommand(p => LogOut(true), p => CanLogOut()); }
        }

        public ICommand OpenAlertsCommand
        {
            get
            {
                return _openAlertsCommand ??= new RelayCommand(p => OpenAlerts(), p => CanOpenAlerts());
            }
        }

        public ICommand OpenAnalyticsCommand
        {
            get
            {
                return _openAnalyticsCommand ??= new RelayCommand(p => OpenAnalytics(), p => CanOpenAnalytics());
            }
        }

        public ICommand OpenCameraManagementCommand
        {
            get
            {
                return _openCameraManagementCommand ??= new RelayCommand(p => OpenCameraManagement(), p => CanOpenCameraManagement());
            }
        }


        public Prism.Commands.DelegateCommand OpenExportCommand
        {
            get
            {
                return _openExportCommand ??= new Prism.Commands.DelegateCommand( OpenExport, CanOpenExport).ObservesProperty(() => DirectModeManager.IsDirectMode);
            }
        }

        public ICommand OpenImageEditorCommand
        {
            get
            {
                return _openImageEditorCommand ??= new RelayCommand(p => OpenImageEditor(), p => CanOpenImageEditor());
            }
        }


        public ICommand OpenLPRCommand
        {
            get
            {
                return _openLPRCommand ??= new RelayCommand(p => OpenLPR(), p => CanOpenLPR());
            }
        }


        public Prism.Commands.DelegateCommand OpenManagementCommand
        {
            get
            {
                return _openManagementCommand ??= new Prism.Commands.DelegateCommand(OpenManagement, CanOpenManagement).ObservesProperty(() => DirectModeManager.IsDirectMode);
            }
        }


        public ICommand OpenMappingCommand
        {
            get
            {
                return _openMappingCommand ??= new RelayCommand(p => OpenMapping(), p => CanOpenMapping());
            }
        }

        public ICommand OpenMonitoringCommand
        {
            get
            {
                return _openMonitoringCommand ??= new RelayCommand(OpenMonitoring, p => CanOpenMonitoring());
            }
        }

        public ICommand OpenSettingsCommand
        {
            get
            {
                return _openSettingsCommand ??= new RelayCommand(p => OpenSettings(), p => CanOpenSettings());
            }
        }

        public ICommand OpenUserManagementCommand
        {
            get
            {
                return _openUserManagementCommand ??= new RelayCommand(p => OpenUserManagement(), p => CanOpenUserManagement());
            }
        }


        public ICommand OpenWebCommand
        {
            get
            {
                return _openWebCommand ??= new RelayCommand(p => OpenWeb(), p => CanOpenWeb());
            }
        }


        public StatusBarInformation StatusBarInformation { get; set; } = new StatusBarInformation();

        public RadTabbedWindow TabWindow
        {
            get => _tabWindow;
            set
            {
                if (Equals(value, _tabWindow))
                {
                    return;
                }

                _tabWindow = value;
                OnPropertyChanged();
            }
        }

        public ICommand TabIconCommand
        {
            get
            {
                return _tabIconCommand ??= new RelayCommand(p => ClickTabIcon(), p => CanExectueTabIcon());
            }
        }

        public RadContextMenu TabIconContextMenu
        {
            get => _tabIconContextMenu;
            set
            {
                if (_tabIconContextMenu != value)
                {
                    _tabIconContextMenu = value;
                    OnPropertyChanged();
                }
            }
        }

        public UserLoginItemModel UserLogin
        {
            get => _userLogin;
            set
            {
                if (_userLogin != value)
                {
                    _userLogin = value;
                    OnPropertyChanged();
                }
            }
        }

        public TabbedWindowViewModel(DirectModeManager directModeManager = null, ILog log = null)
        {
            _log = log;
            DirectModeManager = directModeManager;
            
            TabIconContextMenu = new RadContextMenu();
        }

        public DirectModeManager DirectModeManager
        {
            get => _directModeManager;
            set
            {
                if (Equals(value, _directModeManager)) return;
                _directModeManager = value;
                OnPropertyChanged();
            }
        }

        public bool AutoHide
        {
            get => _autoHide;
            set
            {
                if (value == _autoHide) return;
                _autoHide = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void CloseTab(object obj)
        {
            if (obj is ECloseTabAction command)
            {
                if (TabWindow != null)
                {
                    var selectedIndex = TabWindow.SelectedIndex;
                    var tabIndex = 0;
                    switch (command)
                    {
                        case ECloseTabAction.CloseTab:
                            if (TabWindow?.SelectedItem is EdgeTabItem tab)
                            {
                                TryCloseTab(tab);
                            }

                            break;
                        case ECloseTabAction.CloseTabsRight:
                            foreach (var rightTabs in TabWindow.Items.OfType<EdgeTabItem>().ToList())
                            {
                                if (tabIndex > selectedIndex)
                                {
                                    if (!TryCloseTab(rightTabs))
                                    {
                                        TabWindow.SelectedItem = rightTabs;
                                        break;
                                    }
                                }

                                tabIndex++;
                            }

                            break;
                        case ECloseTabAction.CloseAllOtherTabs:
                            foreach (var otherTabs in TabWindow.Items.OfType<EdgeTabItem>().ToList())
                            {
                                if (selectedIndex != tabIndex)
                                {
                                    if (!TryCloseTab(otherTabs))
                                    {
                                        TabWindow.SelectedItem = otherTabs;
                                        break;
                                    }
                                }

                                tabIndex++;
                            }

                            break;
                    }
                }
            }
        }

        private bool TryCloseTab(EdgeTabItem edgeTab)
        {
            if (edgeTab.CheckClose() || edgeTab.CloseConfirm())
            {
                TabWindow.Items?.Remove(edgeTab);
                edgeTab.Dispose();
                return true;
            }

            return false;
        }

        private bool CanCloseTab(object obj)
        {
            if (obj is ECloseTabAction action)
            {
                switch (action)
                {
                    case ECloseTabAction.CloseTab:
                        return true;
                    case ECloseTabAction.CloseTabsRight:
                        return TabWindow?.Items?.Count - 1 > TabWindow?.SelectedIndex;
                    case ECloseTabAction.CloseAllOtherTabs:
                        return TabWindow.Items.Count > 1;
                }
            }

            return true;
        }

        private async void ChangePassword()
        {
            await UserLogin.ChangePassword();
        }

        private bool CanChangePassword()
        {
            return true;
        }

        private void GotoTab(object o)
        {
            if (int.TryParse((string) o, out var index))
            {
                if (TabWindow.Items.Count >= index)
                {
                    TabWindow.SelectedIndex = index - 1;
                }
            }
        }

        private bool CanGotoTab()
        {
            return true;
        }

        private bool CanOpenManagement()
        {
            return !DirectModeManager.IsDirectMode && CommunicationManager.EffectivePermissions.HasRoleAny(
                ERoles.Administrator | ERoles.ReadOnlyAdministrator |
                ERoles.CameraAdministrator);
        }

        private void OpenManagement()
        {
            TabWindow.OpenTab<ManagementTab>();
        }

        private void OpenLPR()
        {
            TabWindow.OpenTab<LPRTab>();
        }

        private bool CanOpenLPR()
        {
            return true;
        }

        private bool CanGoNextTab()
        {
            return true;
        }

        private void GoNextTab()
        {
            TabWindow.SelectedIndex = Math.Min(TabWindow.Items.Count - 1, TabWindow.SelectedIndex + 1);
        }

        private void GoPreviousTab()
        {
            TabWindow.SelectedIndex = Math.Max(0, TabWindow.SelectedIndex - 1);
        }

        private bool CanGoPreviousTab()
        {
            return true;
        }

        private bool CanOpenCameraManagement()
        {
            return true;
        }

        private void OpenCameraManagement()
        {
           // ShowWindow<CameraManagementWindow>();
        }

        private void OpenExport()
        {
            TabWindow.OpenTab<ExportArchiveTab>();
        }

        private bool CanOpenExport()
        {
            return !DirectModeManager.IsDirectMode;
        }

        private void OpenImageEditor()
        {
            TabWindow.OpenTab<ImageEditorTab>();
        }

        private bool CanOpenImageEditor()
        {
            return true;
        }

        private void OpenAlerts()
        {
            TabWindow.OpenTab<AlertsTab>();
        }

        private bool CanOpenAlerts()
        {
            return true;
        }

        private void OpenWeb()
        {
            TabWindow.OpenTab<WebTab>();
        }

        private bool CanOpenWeb()
        {
            return true;
        }

        private void OpenUserManagement()
        {
            ShowWindow<UserManagementWindow>();
        }

        private bool CanOpenUserManagement()
        {
            return true;
        }

        private void OpenMonitoring(object o)
        {
            TabWindow?.OpenTab<MonitoringTab>();
        }

        private bool CanOpenMonitoring()
        {
            return true;
        }

        private bool CanOpenSettings()
        {
            return true;
        }

        private void OpenSettings()
        {
            ShowWindow<SettingsWindow>();
        }

        public void LogOut(bool closeTabbedWindow = false)
        {
            try
            {
                if (closeTabbedWindow)
                {
                    var mainWindow = ObjectResolver.Resolve<TabbedWindow>();
                    if (mainWindow != null)
                    {
                        TabbedWindow.IsForceClosing = true;
                        CloseWindow<TabbedWindow>();
                        LoginViewModel.IsLoggedIn = false;
                    }
                }
                else
                {
                    foreach (var c in CommunicationManager.ServiceManagers.Values)
                    {
                        c.Dispose();
                    }

                    ClearApiCache();

                    try
                    {
                        CommandLineManager.StartupArgs.LoadRecoveryLayout = false;

                        CommunicationManager.ServiceManagers.Clear();
                        CommunicationManager.CurrentUser = default;
                        CommunicationManager.EffectivePermissions = default;
                        CommunicationManager.ConnectedZoneId = Guid.Empty;
                        CommunicationManager.ConnectedServerId = Guid.Empty;

                    }
                    catch (Exception e)
                    {
                        
                    }
                    //var objects = ObjectResolver.ResolveAll<BaseItemManager>();
                    //foreach (var o in objects)
                    //{

                    //}
                    //foreach (var hub in CommunicationManager.ZoneSignalRHubConnections.Values)
                    //{
                    //    hub.DisconnectAsync();
                    //}

                    //  CloseWindow<CameraManagementWindow>();
                    CloseWindow<UserManagementWindow>();
                    CloseWindow<UserAuditWindow>();

                    CloseWindow<RadTabbedWindow>(closeAll: true);
                    //ShowWindow<LoginWindow>();
                    TabbedWindow.IsForceClosing = false;
                    App.LoginWindow.LoginView.Model.SkipValidation = false;
                    App.LoginWindow?.Show();
                    App.LoginWindow?.GetParentWindow<System.Windows.Window>()?.GlobalActivate();
                }
            }
            catch (Exception e)
            {
                
            }
        }

        public static void ClearApiCache()
        {
            try
            {
                var itemMgrs = App.GlobalUnityContainer.ResolveAll<IBaseManager>().ToList();
                foreach (var i in itemMgrs.OfType<IDisposable>())
                {
                    try
                    {
                        i.Dispose();
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private bool CanLogOut()
        {
            return true;
        }

        private bool CanOpenMapping()
        {
            return true;
        }

        private void OpenMapping()
        {
            TabWindow.OpenTab<MappingTab>();
        }

        private bool CanOpenAnalytics()
        {
            return true;
        }

        private void OpenAnalytics()
        {
            TabWindow.OpenTab<AnalyticsTab>();
        }

        private bool CanExit()
        {
            return true;
        }

        public async void ExitApplication()
        {
            foreach (var t in ObjectResolver.ResolveMany<RadTabbedWindow>())
            {
                foreach (var i in t.Items.OfType<EdgeTabItem>())
                {
                    if (!i.CheckClose())
                    {
                        if (!i.CloseConfirm())
                        {
                            return;
                        }
                    }
                }
            }

            if (DialogUtil.MetroConfirmDialog("Do you wish to exit the application?", true, "Exit Application"))
            {
                var mainWindow = ObjectResolver.Resolve<TabbedWindow>();
                if (mainWindow != null)
                {
                    Client.View.Window.TabbedWindow.IsForceClosing = true;
                    mainWindow.SkipLogOutPrompt = true;
                    CloseWindow<TabbedWindow>();
                }

                await App.ExitApp();
                Environment.Exit(0);
                //Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
                //Application.Current.Shutdown();
            }
        }

        private bool CanExectueTabIcon()
        {
            return true;
        }

        private void ClickTabIcon()
        {

        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    internal enum ECloseTabAction
    {
        CloseTab,
        CloseTabsRight,
        CloseAllOtherTabs
    }
}