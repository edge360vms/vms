﻿using System.Windows;
using Edge360.Platform.VMS.Client.Extensions.CommandLine;
using Edge360.Platform.VMS.Client.Util;
using Edge360.Platform.VMS.UI.Common.Util;
using Prism.Events;
using Prism.Regions;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Client.View.Window
{
    /// <summary>
    ///     Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : IView
    {
        private static IEventAggregator _eventAggregator;
        private static IRegionManager _regionManager;

        public static bool HasRequestedLogs { get; set; }
        public LoginWindowViewModel Model => DataContext as LoginWindowViewModel;

        public LoginWindow(IRegionManager regionManager,
            IEventAggregator eventAggregator)
        {
            //  SetCurrentProcessExplicitAppUserModelID(AppID);
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            InitializeComponent();
            Model.View = this;
            //this.SetTaskbarVisibility(true, Name);
            IsVisibleChanged += OnIsVisibleChanged;
            
           // DataContext = new LoginWindowViewModel(this);
            PreviewClosed += OnPreviewClosed;

            
            //    Loaded += OnLoaded;
        }

        public static async void InitializeAuditLoggingEvents()
        {
            //if (_eventAggregator != null)
            //{
            //    try
            //    {
            //        if (!HasRequestedLogs)
            //        {
            //            HasRequestedLogs = true;
            //            var req = await ServiceManagers[HARD_CODED_ZONE_ID].ServiceClients[EClientType.Management]
            //                .Request(new SearchAuditLog {Page = 1});
            //            if (req != null)
            //            {
            //                //   Console.WriteLine(req.AuditLogs);
            //            }

            //            _eventAggregator.GetEvent<AuditLogColumnsSelected>().Publish(new[]
            //            {
            //                "ServerId",
            //                "ZoneId",
            //                "Success",
            //                "Action",
            //                "Message"
            //            });
            //            _eventAggregator.GetEvent<ServiceClientActivatedEvent>()
            //                .Publish(ServiceManagers[HARD_CODED_ZONE_ID].ServiceClients[EClientType.Management]);
            //        }
            //    }
            //    catch (Exception e)
            //    {
            //    }
            //}
        }

        private async void OnPreviewClosed(object sender, WindowPreviewClosedEventArgs e)
        {
            WindowUtil.CloseWindow<SettingsWindow>();
            await App.ExitApp();
        }


        private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.SetTaskbarVisibility(Visibility == Visibility.Visible, Tag.ToString());
        }
    }
}