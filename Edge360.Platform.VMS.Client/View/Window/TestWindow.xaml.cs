﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Edge360.Platform.VMS.Client.ItemModel.Camera;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.Communication;
using Edge360.Platform.VMS.UI.Common.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.RecordingProfiles;
using Telerik.WinControls.UI;
using Telerik.Windows.Controls.MaterialControls;
using RadDateTimePickerSpinEdit = Telerik.WinControls.UI.RadDateTimePickerSpinEdit;

namespace Edge360.Platform.VMS.Client.View.Window
{
    /// <summary>
    ///     Interaction logic for TestWindow.xaml
    /// </summary>
    public partial class TestWindow
    {
        public TestWindow()
        {
            InitializeComponent();
            DataContext = new TestViewModel(this);

            EdgeVideoControl.VideoObject = new CameraItemModel(){Username = "root", Password = "pass", CameraUrl = @"rtsp://10.100.0.56/axis-media/media.amp?resolution=1280x720&compression=30"};
            EdgeVideoControl.Model.Open(@"rtsp://root:pass@10.100.0.56/axis-media/media.amp?resolution=1280x720&compression=30");
            EdgeVideoControl.Model.Play();
      //      EdgeVideoControl.DigitalZoomWindow.MouseWheel += DigitalZoomWindowOnMouseWheel;
            
        }

        private void DigitalZoomWindowOnMouseWheel(object sender, MouseWheelEventArgs e)
        {
       //     EdgeVideoControl.DigitalZoomWindow.ZoomFactor += e.Delta;
        }

        private async void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {

            //var cameras = await CommunicationManager.GetManagerByZoneId().CameraService.ListCameras();

            try
            {

                var recordingProfiles = await CommunicationManager.GetManagerByZoneId().GlobalRecordingService
                    .ListRecordingProfiles(new ListRecordingProfiles() { CameraId = Guid.Empty});
                foreach (var r in recordingProfiles.RecordingProfiles)
                {
                    Trace.WriteLine($"{r.ZoneId} {r.CameraId} {r.Width} {r.Height} {r.CameraName} {r.Remote}");
                }

            }
            catch (Exception exception)
            {
                
            }
            //await Task.WhenAll(cameras.Cameras.Select(async o =>
            //{
            //   var recordingProfiles = await CommunicationManager.GetManagerByZoneId().GlobalRecordingService
            //        .ListRecordingProfiles(new ListRecordingProfiles() { CameraId = o.Id});
            //   foreach (var r in recordingProfiles.RecordingProfiles)
            //   {
            //        Trace.WriteLine($"{r.ZoneId} {r.CameraId} {r.Width} {r.Height} {r.CameraName} {r.Remote}");
            //   }
            //}));

            //foreach (var c in cameras)
            //{
            //    CommunicationManager.GetManagerByZoneId().GlobalRecordingService
            //        .ListRecordingProfiles(new ListRecordingProfiles() { });
            //}

        }
    }
//    public float ZoomFactor { get; set; } = 0;
//private void DigitalZoomRect_OnMouseWheel(object sender, MouseWheelEventArgs e)
//{
//ZoomFactor += e.Delta;
//}


    public enum ETestValue
    {
        DeleteLocally,
        DeleteRemotely
    }

    public class TestViewModel
    {
        private readonly TestWindow _view;
        private RelayCommand _testCommand;

        public RelayCommand TestCommand => _testCommand ??= new RelayCommand(o => Test(o));

        public TestViewModel(TestWindow view)
        {
            _view = view;
        }

        private void Test(object o)
        {
            if (o is ETestValue val)
            {
                switch (val)
                {
                    case ETestValue.DeleteLocally:
                        var dialog = DialogUtil.EmbeddedModalConfirmDialog(
                            _view.DialogOverlay.ModalContentPresenter,
                            new EmbeddedDialogParameters
                            {
                                DialogButtons = EDialogButtons.YesNo,
                                Description = "Are you sure you wish to delete this Recording Profile?\n"
                            });
                        break;
                    case ETestValue.DeleteRemotely:
                        var dialog2 = DialogUtil.EmbeddedModalConfirmDialog(
                            _view.DialogOverlay.ModalContentPresenter, new EmbeddedDialogParameters
                            {
                                DialogButtons = EDialogButtons.YesNo,
                                Description = "Are you sure you wish to delete this Recording Profile?\n"
                            });
                        break;
                }
            }
        }

        private bool CanTest()
        {
            return true;
        }
    }
}