﻿using System;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Common.Interfaces.Communication.IPC;

namespace Edge360.Platform.VMS.Updater
{
    public static class UpdateServiceIpcCommunicationManager
    {
        private static CancellationTokenSource _cancellationTokenSource = null;

        public static void Start()
        {
            if (_cancellationTokenSource == null)
            {
                _cancellationTokenSource = new CancellationTokenSource();

                InitIpcCommunication(_cancellationTokenSource.Token);
            }
        }

        public static void Stop()
        {
            if (_cancellationTokenSource != null)
            {
                
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource = null;
            }
        }


        public static void InitIpcCommunication(CancellationToken token)
        {
            var tcs = new TaskCompletionSource<bool>();

            Task.Run(async () =>
            {
                try
                {
                    using (var namedPipe = new ServiceHost(new UpdateServiceIpcCommunication()))
                    {
                        namedPipe.AddServiceEndpoint(typeof(IUpdateServiceCommunication),
                            new NetNamedPipeBinding(), "net.pipe://localhost/edge360_vms_update_service");
                        namedPipe.Open();
                        tcs.SetResult(true);
                        while (!token.IsCancellationRequested)
                        {
                            await Task.Delay(333, token);
                        }
                    }
                }
                catch (Exception e)
                {
                    // Trace.WriteLine(e);
                    tcs.SetResult(false);
                    //  await Task.Delay(5000);
                    //  InitIpcCommunication();
                }
            }, token);
        }

    }
}
