﻿using System;
using System.Reflection;
using System.ServiceProcess;
using Edge360.Platform.VMS.Updater.Properties;
using Edge360.Platform.VMS.Updater.Shared;
using Edge360.Platform.VMS.Updater.Shared.Static;
using Edge360.Platform.VMS.Updater.Static;
using log4net;
using log4net.Appender;
using log4net.Core;

namespace Edge360.Platform.VMS.Updater
{
    internal static class Program
    {
        private static readonly ILog Log = LogManager.GetLogger
            (MethodBase.GetCurrentMethod().DeclaringType);


        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        private static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            // Prevent Errors Later if null Args Passed
            if (args == null)
            {
                args = new string[0];
            }

            CommandLineHelper.Args = args;


            // Setup Logging Options
            if (!CommandLineHelper.HasArgument("--nolog"))
            {
                if (!LoggingManager.Configure())
                {
                    LoggingManager.Configure(Resources.Log_config);
                }
            }

            if (CommandLineHelper.HasArgument("--log"))
            {
                LoggingManager.SetLogFilePath(CommandLineHelper.FindArgumentValue("--log"));
            }

            if (CommandLineHelper.HasArgument("--verbose"))
            {
                LoggingManager.ChangeLogLevel(Level.Debug);
            }

            if (CommandLineHelper.HasArgument("--quiet"))
            {
                LoggingManager.ChangeLogLevel<ConsoleAppender>(Level.Off);
            }

            if (CommandLineHelper.HasArgument("--uninstall"))
            {
                // USED BY CLIENT INSTALLER TO UNINSTALL
                // --UNINSTALL
                try
                {
                    if (UpdateInstallManager.UninstallAll())
                    {
                        Environment.Exit(0);
                    }
                    Log.Error("Problem During Uninstall");
                    Environment.Exit(-1);
                    return; // Exit
                }
                catch (Exception e)
                {
                    Log.Error("Error During Uninstall",e);
                    Environment.Exit(-1);
                    return; // Exit
                }
            }

            if (CommandLineHelper.HasArgument("--install"))
            {
                // USED BY CLIENT INSTALLER TO PERFORM THE INITIAL CLIENT INSTALLATION
                // --INSTALL <PATH TO DIRECTOR.XML OR .ZIP> [--FORCE] [--TEST] [--NOCLEANUP] [--WAIT] [--STARTSERVICE]
                try
                {
                    UpdateManagerStandaloneUpdate.HandleStandaloneInstall();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error Handling Install Request");
                    Console.WriteLine(e.Message);
                    Log.Error("Error Handling Install Request", e);
                    Environment.Exit(-1);
                }

                Environment.Exit(0);
                return; // Exit
            }

            if (CommandLineHelper.HasArgument("--installtest"))
            {
                // USED BY TEST THE INSTALLER
                Console.WriteLine($"Arguments - {string.Join(" ", args)}");
                Console.ReadLine();
                Environment.Exit(0);
                return; // Exit
            }

            if (CommandLineHelper.HasArgument("--stopservice"))
            {
                Log.Info($"Stopping Service - {ServiceHelper.UpdateServiceName}");
                if (ServiceHelper.StopService(ServiceHelper.UpdateServiceName, ServiceHelper.DefaultTimeout))
                {
                    Environment.Exit(0);
                }
                else
                {
                    Log.Error("Unable To Stop Service");
                    Environment.Exit(-1);
                }
                return; // Exit
            }

            if (CommandLineHelper.HasArgument("--startservice"))
            {
                Log.Info($"Starting Service - {ServiceHelper.UpdateServiceName}");
                if (ServiceHelper.StartService(ServiceHelper.UpdateServiceName, ServiceHelper.DefaultTimeout))
                {
                    Environment.Exit(0);
                }
                else
                {
                    Log.Error("Unable To Start Service");
                    Environment.Exit(-1);
                }
                return; // Exit
            }

            if (CommandLineHelper.HasArgument("--restartservice"))
            {
                Log.Info($"Restarting Service - {ServiceHelper.UpdateServiceName}");
                if (ServiceHelper.RestartService(ServiceHelper.UpdateServiceName, ServiceHelper.DefaultTimeout))
                {
                    Environment.Exit(0);
                }
                else
                {
                    Log.Error("Unable To Restart Service");
                    Environment.Exit(-1);
                }
                return; // Exit
            }

            var startService = CommandLineHelper.HasArgument("--service");
            var startConsole = Environment.UserInteractive;
            if (CommandLineHelper.HasArgument("--console"))
            {
                startConsole = true;
                startService = true;
            }

            if (startService)
            {
                if (startConsole)
                {
                    // Run Service In Console Mode 
                    Console.WriteLine("EDGE360 VMS CLIENT UPDATE SERVICE");
                    Console.WriteLine("");
                    Console.WriteLine("Press ENTER to exit...");
                    Console.WriteLine("");

                    var svc = new UpdateService();
                    svc.DoStart();

                    Console.ReadLine();

                    svc.DoStop();
                }
                else
                {
                    var servicesToRun = new ServiceBase[]
                    {
                        new UpdateService()
                    };
                    ServiceBase.Run(servicesToRun);
                }
            }
            else
            {
                // Start Interactive App
                Console.WriteLine("Starting Interactive App (Coming Soon...)");
            }
        }


        /// <summary>
        ///     Log Unhandled Exceptions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e?.ExceptionObject == null)
            {
                var ex = e.ExceptionObject as Exception;
                Log.Error("Unhandled Exception", ex);
            }
        }
    }
}