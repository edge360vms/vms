﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using Edge360.Platform.VMS.Common.Interfaces.Communication.IPC;


namespace Edge360.Platform.VMS.Updater
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant,
        InstanceContextMode = InstanceContextMode.Single)]
    public class UpdateServiceIpcCommunication : IUpdateServiceCommunication
    {
        public void RegisterServers(string[] serverList)
        {

        }

        public bool UpdateNow(string[] serverList)
        {
            return false;
        }

        public bool UpdateAvailable(string[] serverList)
        {
            return false;
        }
    }
}
