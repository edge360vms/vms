﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Updater.Shared;

namespace Edge360.Platform.VMS.Updater
{
    partial class UpdateService : ServiceBase
    {
        public UpdateService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            DoStart();
        }

        protected override void OnStop()
        {
            DoStop();
        }

        public void DoStart()
        {
            // Start Update Manager
            UpdateManager.Start();

            // Start IPC
            UpdateServiceIpcCommunicationManager.Start();
        }

        public void DoStop()
        {
            // Stop Update Manager
            UpdateManager.Stop();

            // Stop IPC
            UpdateServiceIpcCommunicationManager.Stop();
        }

    }
}
