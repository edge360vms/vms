﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Core;
using log4net.Filter;
using log4net.Repository.Hierarchy;

namespace Edge360.Platform.VMS.Updater.Static
{
    public static class LoggingManager
    {
        public static string ApplicationStartMessage = "";

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static bool Configure(bool watch=true)
        {
            try
            {
                var logConfigFile = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Log.config");
                return Configure(new FileInfo(logConfigFile), watch);
            }
            catch (Exception e)
            {
                Log.Error("Error with Log4Net Configure", e);
                return false;
            }
        }

        public static bool Configure(FileInfo logConfigFile, bool watch=true)
        {
            //[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Log.config", Watch = true)]
            try
            {
                if (logConfigFile == null || !logConfigFile.Exists)
                {
                    return false;
                }

                if (watch)
                {
                    XmlConfigurator.ConfigureAndWatch(logConfigFile);
                }
                else
                {
                    XmlConfigurator.Configure(logConfigFile);
                }
                return true;
            }
            catch (Exception e)
            {
                Log.Error("Error with Log4Net Configure", e);
                return false;
            }
        }

        public static bool Configure(string logConfigXml)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(logConfigXml))
                {
                    return false;
                }

                byte[] logConfigXmlArray = Encoding.ASCII.GetBytes(logConfigXml);

                XmlConfigurator.Configure(new MemoryStream(logConfigXmlArray));
                return true;
            }
            catch (Exception e)
            {
                Log.Error("Error with Log4Net Configure", e);
                return false;
            }
        }

        public static void SetLogFilePath(string newPath, string filename="Log.txt")
        {
            try
            {
                if (string.IsNullOrWhiteSpace(newPath) || string.IsNullOrWhiteSpace(filename))
                {
                    throw new InvalidOperationException("Path or Filename was Null");
                }

                var logFilePath = newPath;

                if (newPath != null)
                {
                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }

                    logFilePath = Path.Combine(newPath, filename);
                }

                foreach (var appender in LogManager.GetRepository().GetAppenders())
                {
                    var appenderFile = appender as RollingFileAppender;
                    if (appenderFile != null)
                    {
                        appenderFile.File = logFilePath;
                        appenderFile.ActivateOptions();
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("Error Setting Up Log File", e);
            }
        }

        public static void ChangeLogLevel(Level level)
        {
            ((log4net.Repository.Hierarchy.Hierarchy) LogManager.GetRepository()).Root.Level = level;
            ((log4net.Repository.Hierarchy.Hierarchy) LogManager.GetRepository()).RaiseConfigurationChanged(
                EventArgs.Empty);
        }

        public static void ChangeLogLevel<T>(Level level, bool acceptOnMatch=true)
        {
            foreach (var appender in LogManager.GetRepository().GetAppenders())
            {
                if (appender is T)
                {
                    ((AppenderSkeleton) appender).Threshold = level;
                }
            }

            ((log4net.Repository.Hierarchy.Hierarchy) LogManager.GetRepository()).RaiseConfigurationChanged(
                EventArgs.Empty);
        }

        public static void RemoveAppenders<T>()
        {
            List<T> removeList = new List<T>();

            foreach (var appender in LogManager.GetRepository().GetAppenders())
            {
                if (appender is T)
                {
                    removeList.Add((T)appender);
                }
            }

            foreach (var remove in removeList)
            {
                if (remove is IAppender)
                {
                    ((log4net.Repository.Hierarchy.Logger) Log.Logger).RemoveAppender(((IAppender)remove).Name);
                }
            }
            ((log4net.Repository.Hierarchy.Hierarchy) LogManager.GetRepository()).RaiseConfigurationChanged(
                EventArgs.Empty);
        }

        private static string _logFileBackup = null;

        public static void BackupLogFile(string logPath, bool onlyLatestSession=true)
        {
            try
            {
                var logFilePath = Path.Combine(logPath, "Log.txt");
                Log.Debug($"Backing Up Log File Contents - {logFilePath}");

                if (File.Exists(logFilePath))
                {
                    ((Hierarchy) LogManager.GetRepository()).Flush(60 * 1000);
                    _logFileBackup = File.ReadAllText(logFilePath);
                    if (onlyLatestSession && !string.IsNullOrWhiteSpace(_logFileBackup))
                    {
                        var indexStart = _logFileBackup.LastIndexOf(ApplicationStartMessage,
                            StringComparison.CurrentCultureIgnoreCase);
                        if (indexStart >= 0)
                        {
                            _logFileBackup = _logFileBackup.Substring(indexStart);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("Error Backing Up Log File", e);
            }
        }

        public static void RestoreLogFile(string logPath)
        {
            try
            {
                if (_logFileBackup == null)
                {
                    return;
                }

                var logFilePath = Path.Combine(logPath, "Log.txt");
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }
                File.WriteAllText(logFilePath, _logFileBackup);
                Log.Debug($"Restored Log File Contents - {logFilePath}");
            }
            catch (Exception e)
            {
                Log.Error("Error Restoring Log File", e);
            }
        }
    }
}
