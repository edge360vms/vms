﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Users;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class UserService : BaseCommunicationService
    {
        public UserService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<UpdateUserResponse> UpdateUser(string displayName, bool isEnabled, Guid? userId,
            string userName, string type = "user", string password = null)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new UpdateUser
                    {
                        DisplayName = displayName,
                        Enabled = isEnabled,
                        Password = password,
                        UserId = userId,
                        Username = userName,
                        UserType = type
                    });
                    return resp;
                }

                return default;
            });
        }

        public async Task<RevokeUserTokensResponse> RevokeToken(Guid userId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new RevokeUserTokens
                    {
                        UserId = userId
                    });
                    return resp;
                }

                return default;
            });
        }

        /// <summary>
        ///     Deletes a User based on Descriptor
        /// </summary>
        /// <param name="desc"></param>
        /// <returns></returns>
        public async Task<bool> DeleteUser(UserDetails desc)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new DeleteUserById
                    {
                        UserId = desc.UserId
                    });
                    return resp != null && resp.Success;
                }

                return false;
            });
        }

        /// <summary>
        ///     Returns all Users from the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<List<UserDetails>> GetUsers()
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new ListUsers());
                    return resp?.Users;
                }

                return null;
            });
        }


        public async Task<GetUserResponse> GetUser(string username = null, Guid? userId = null)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new GetUser
                    {
                        UserId = userId,
                        Username = username
                    });
                    return resp;
                }

                return null;
            });
        }

        public async Task<SetUserPasswordResponse> ChangeAdminUserPassword(string newPassword, Guid userId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var user = new SetUserPassword
                    {
                        NewPassword = newPassword,
                        UserId = userId
                    };

                    var resp = await customServiceClient.Request(user);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return null;
            });
        }


        public async Task<ChangeUserPasswordResponse> ChangeUserPassword(string newPassword, string oldPassword)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var user = new ChangeUserPassword
                    {
                        NewPassword = newPassword,
                        OldPassword = oldPassword
                    };

                    var resp = await customServiceClient.Request(user);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return null;
            });
        }

        /// <summary>
        ///     Creates a User on the VMS Server
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns />
        public async Task<Guid?> CreateUser(string username, string password, string displayName = "")
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var user = new CreateUser
                    {
                        Username = username,
                        Password = password,
                        DisplayName = displayName
                    };

                    var resp = await customServiceClient.Request(user);
                    return resp?.UserId;
                }

                return null;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Authority;
        }
    }
}