﻿using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using RecordingService.ServiceModel.Api.Playback;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class PlaybackService : BaseCommunicationService
    {
        public PlaybackService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<PlaybackRequestDescriptor> CreatePlayback(CreatePlayback createPlayback)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(createPlayback);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }

        public async Task<ExecutePlaybackResponse> ExecutePlayback(ExecutePlayback executePlayback)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(executePlayback);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }

        public async Task<bool> KeepAlivePlayback(KeepAlivePlayback keepAlivePlayback)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(keepAlivePlayback);
                    if (resp)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }

        public async Task<GetPlaybackStatusResponse> GetPlaybackStatus(GetPlaybackStatus getPlaybackStatus)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(getPlaybackStatus);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Recording;
        }
    }
}