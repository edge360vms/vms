﻿using System;
using System.Linq;
using System.Reflection;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Polly;
using log4net;

namespace Edge360.Platform.VMS.Communication.Services
{
    public abstract class BaseCommunicationService
    {
        public const bool VERBOSITY_CLIENT_SELECT = true; //true;
        public ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public ServiceManager ServiceManager;

        /// <summary>
        ///     Client used to communicate with the VMS Server
        /// </summary>
        public virtual CustomServiceClient Client
        {
            get
            {
                var customServiceClient = ServiceManager.ServiceClients[GetClientType()].GetClient();
                if (VERBOSITY_CLIENT_SELECT)
                {
                    _log?.Info($"Client {customServiceClient.BaseUri} being used...");
                }

                return customServiceClient;
            }
        }

        public BaseCommunicationService(ServiceManager serviceManager)
        {
            ServiceManager = serviceManager;
        }

        protected BaseCommunicationService()
        {
        }

        public abstract EClientType GetClientType();

        public CustomServiceClient ServerSpecificClient(Guid serverId)
        {
            var serverSpecificClient = ServiceManager
                .ServiceClients[GetClientType()].AvailableClients.InternalCache
                .FirstOrDefault(o => { return o.Value.ServerId == serverId; }).Value;
            return serverSpecificClient;
        }
    }
}