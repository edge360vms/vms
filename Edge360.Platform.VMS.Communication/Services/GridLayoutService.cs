﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.GridLayouts;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Interfaces;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class GridLayoutService : BaseCommunicationService
    {
        public GridLayoutService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<CreateGridLayoutResponse> CreateGridLayout(string label, string description = "",
            string layoutData = "", int layoutType = 0, int shareType = 0)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new CreateGridLayout
                    {
                        Description = description,
                        Label = label,
                        LayoutData = layoutData,
                        LayoutType = layoutType,
                        ShareType = shareType
                    });
                    return resp;
                }

                return default;
            });
        }

        public async Task<UpdateGridLayoutResponse> UpdateGridLayout(string label, Guid? Id,
            string description = "",
            string layoutData = "", int layoutType = 0, int shareType = 0)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new UpdateGridLayout
                    {
                        Id = Id,
                        Description = description,
                        Label = label,
                        LayoutData = layoutData,
                        LayoutType = layoutType,
                        ShareType = shareType
                    });
                    return resp;
                }

                return default;
            });
        }

        public async Task<DeleteGridLayoutResponse> DeleteGridLayout(Guid? Id)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new DeleteGridLayout
                    {
                        Id = Id
                    });
                    return resp;
                }

                return default;
            });
        }

        public async Task<IGridLayout> GetGridLayout(Guid? Id)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new GetGridLayout
                    {
                        Id = Id
                    });
                    return resp?.GridLayout;
                }

                return default;
            });
        }

        public async Task<List<GridLayoutDescriptor>> ListGridLayouts()
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new ListGridLayouts());
                    return resp?.GridLayouts;
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Cameras;
        }
    }
}