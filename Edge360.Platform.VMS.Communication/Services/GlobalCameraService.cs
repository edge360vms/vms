﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Cameras;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class GlobalCameraService : BaseCommunicationService
    {
        public GlobalCameraService(ServiceManager serviceManager) : base(serviceManager)
        {
            ServiceManager = serviceManager;
        }

        public async Task<List<GetCameraResponse>> GetCameraBatch(params Guid[] cameraId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    if (cameraId != null)
                    {
                        try
                        {
                            var getCameras = cameraId.Select(id => new GetCamera {CameraId = id}).ToArray();
                            var resp = await customServiceClient.Request(
                                getCameras);
                            return resp;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }

                return null;
            });
        }

        /// <summary>
        ///     Returns Cameras by ID from the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<GetCameraResponse> GetCamera(Guid? cameraId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    if (cameraId != null)
                    {
                        var resp = await customServiceClient.Request(new GetCamera {CameraId = (Guid) cameraId});
                        return resp;
                    }
                }

                return null;
            });
        }

        /// <summary>
        ///     Returns all Cameras from the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<ListCamerasResponse> ListCameras()
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new ListCameras());
                    return resp;
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Authority;
        }
    }
}