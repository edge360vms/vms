﻿using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Zones;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class GlobalZoneService : BaseCommunicationService
    {
        public GlobalZoneService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<SaveZoneResponse> SaveZone(SaveZone request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }


        public async Task<GetZoneResponse> GetZone(GetZone request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }

        public async Task<UpdateZoneParentNodeResponse> UpdateZoneParentNode(UpdateZoneParentNode request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }

        public async Task<UpdateZoneNameResponse> UpdateZoneName(UpdateZoneName request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }

        public async Task<DeleteZoneResponse> DeleteZone(DeleteZone request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }

        public async Task<ListZonesResponse> ListZones()
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new ListZones());
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Authority;
        }
    }
}