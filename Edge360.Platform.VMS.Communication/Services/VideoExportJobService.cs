﻿using System;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using RecordingService.ServiceModel.Api.VideoExportJob;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class VideoExportJobService : BaseCommunicationService
    {
        public VideoExportJobService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<VideoExportJobDescriptor[]> ListJobs()
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new ListVideoExportJobs());
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }

        public async Task<bool> UpdateExportJob(VideoExportJobDescriptor updateItem)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new UpdateVideoExportJob
                    {
                        UpdateItem = updateItem
                    });
                    return resp;
                }

                return default;
            });
        }

        public async Task<bool> DeleteExportJob(Guid id)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new DeleteVideoExportJob
                    {
                        Id = id
                    });
                    return resp;
                }

                return default;
            });
        }

        public async Task<VideoExportJobDescriptor> GetExportJob(Guid id)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new GetVideoExportJob
                    {
                        Id = id
                    });
                    return resp;
                }

                return default;
            });
        }

        public async Task<Guid> CreateExportJob(VideoExportJobDescriptor createItem)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new CreateVideoExportJob
                    {
                        CreateItem = createItem
                    });
                    return resp;
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Recording;
        }
    }
}