﻿using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.StreamProfiles;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class GlobalStreamProfileService : BaseCommunicationService
    {
        public GlobalStreamProfileService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<ListStreamProfilesResponse> ListStreamingProfiles(ListStreamProfiles request)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<GetStreamProfileResponse> GetStreamingProfile(GetStreamProfile request)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Authority;
        }
    }
}