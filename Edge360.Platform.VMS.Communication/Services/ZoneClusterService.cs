﻿using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.Cluster;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class ZoneClusterService : BaseCommunicationService
    {
        public ZoneClusterService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<UpdateReplicationResponse> UpdateReplication(UpdateReplication request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<UpdateClusterInfoResponse> UpdateClusterInfo(UpdateClusterInfo request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<GetZoneClusterCertificateResponse> GetZoneClusterCertificate(
            GetZoneClusterCertificate request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<GetClusterInfoResponse> GetZoneClusterStatus(GetClusterInfo request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<JoinZoneClusterResponse> JoinZoneCluster(JoinZoneCluster request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }


        public async Task<ListZoneCertificatesResponse> ListZoneCertificates(ListZoneCertificates request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Coordinator;
        }
    }
}