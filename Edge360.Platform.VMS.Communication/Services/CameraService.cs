﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.CameraInventory;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.Cameras;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.CameraStatus;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class CameraService : BaseCommunicationService
    {
        public CameraService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        /// <summary>
        ///     Deletes a Camera based on Descriptor
        /// </summary>
        /// <param name="desc"></param>
        /// <returns></returns>
        public async Task<DeleteCameraResponse> DeleteCamera(CameraDescriptor desc)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new DeleteCamera
                    {
                        Id = desc.Id
                    });
                    return resp;
                }

                return null;
            });
        }

        /// <summary>
        ///     Deletes a Camera based on Descriptor
        /// </summary>
        /// <param name="camera"></param>
        /// <returns></returns>
        public async Task<UpdateCameraResponse> UpdateCamera(UpdateCamera camera)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(camera);
                    return resp;
                }

                return null;
            });
        }

        public async Task<GetCameraStatusResponse> GetCameraStatus(GetCameraStatus camera)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(camera);
                    return resp;
                }

                return null;
            });
        }

        public async Task<UpdateCameraStatusResponse> UpdateCameraStatus(UpdateCameraStatus camera)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(camera);
                    return resp;
                }

                return null;
            });
        }

        public async Task<GetCameraInventoryResponse> GetCameraInventory(GetCameraInventory camera)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(camera);
                    return resp;
                }

                return null;
            });
        }

        public async Task<List<GetCameraStatusResponse>> GetCameraStatusBatch(params Guid[] id)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    if (id != null)
                    {
                        try
                        {
                            var cameraBatch = id.Select(i => new GetCameraStatus {CameraId = i}).ToArray();
                            var resp = await cl.Request(cameraBatch);
                            return resp;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }

                return null;
            });
        }

        public async Task<List<GetCameraResponse>> GetCameraBatch(params Guid[] id)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    if (id != null)
                    {
                        try
                        {
                            var cameraBatch = id.Select(i => new GetCamera {Id = i}).ToArray();
                            var resp = await cl.Request(cameraBatch);
                            return resp;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }

                return default;
            });
        }

        /// <summary>
        ///     Returns Cameras by ID from the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<GetCameraResponse> GetCamera(Guid? id)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    if (id != null)
                    {
                        var resp = await customServiceClient.Request(new GetCamera {Id = (Guid) id});
                        return resp;
                    }
                }

                return default;
            });
        }

        /// <summary>
        ///     Returns all Cameras from the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<ListCamerasResponse> ListCameras(Guid? serverId = null)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = serverId != null && serverId != Guid.Empty
                    ? ServiceManager.ServiceClients[GetClientType()].GetClient(serverId)
                    : Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new ListCameras(), useSameClient: true);
                    return resp;
                }

                return default;
            });
        }

        /// <summary>
        ///     Creates a Camera on the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<CreateCameraResponse> CreateCamera(CreateCamera camera)
        {
            var customServiceClient = Client;
            if (customServiceClient != null)
            {
                var resp = await customServiceClient.Request(camera);
                return resp;
            }

            return null;
        }

        public override EClientType GetClientType()
        {
            return EClientType.Cameras;
        }
    }
}