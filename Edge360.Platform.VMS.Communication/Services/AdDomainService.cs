﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.AdDomain;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class AdDomainService : BaseCommunicationService
    {
        public AdDomainService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<AdDomain> GetDomainByName(string domainName)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new GetAdDomain {Domain = domainName});
                    return resp?.AdDomain;
                }

                return null;
            });
        }

        public async Task<List<AdDomain>> ListDomains()
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new ListAdDomains()
                    );
                    return resp?.AdDomains;
                }

                return null;
            });
        }

        public async Task<bool> CreateDomain(string domainName, Guid activeDirectoryId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new CreateAdDomain
                        {Domain = domainName, AdConfigId = activeDirectoryId});
                    return resp != null && resp.Success;
                }

                return false;
            });
        }

        public async Task<bool> DeleteDomainByName(string domainName)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new DeleteAdDomain {Domain = domainName});
                    return resp != null && resp.Success;
                }

                return false;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Authority;
        }
    }
}