﻿using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.AppSetting;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class AppSettingService : BaseCommunicationService
    {
        public AppSettingService(ServiceManager serviceManager) : base(serviceManager)
        {
            ServiceManager = serviceManager;
        }

        public async Task<GetAppSettingsResponse> GetAppSettings(GetAppSettings request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    if (request != null)
                    {
                        var resp = await customServiceClient.Request(request);
                        return resp;
                    }
                }

                return null;
            });
        }

        public async Task<GetAppSettingResponse> GetAppSetting(GetAppSetting request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    if (request != null)
                    {
                        var resp = await customServiceClient.Request(request);
                        return resp;
                    }
                }

                return null;
            });
        }


        public async Task<SaveAppSettingResponse> SaveAppSetting(SaveAppSetting request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    if (request != null)
                    {
                        var resp = await customServiceClient.Request(request);
                        return resp;
                    }
                }

                return null;
            });
        }

        public async Task<DeleteAppSettingResponse> DeleteAppSetting(DeleteAppSetting request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    if (request != null)
                    {
                        var resp = await customServiceClient.Request(request);
                        return resp;
                    }
                }

                return null;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Coordinator;
        }
    }
}