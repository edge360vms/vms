﻿using Edge360.Platform.VMS.Communication.Enums;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class VideoExportService : BaseCommunicationService
    {
        public VideoExportService(ServiceManager serviceManager)
        {
            ServiceManager = serviceManager;
        }

        public override EClientType GetClientType()
        {
            return EClientType.RecordingAgent;
        }

        //public async Task RemoteJobComplete(RemoteJobComplete request)
        //{
        //    var customServiceClient = Client;
        //    if (customServiceClient != null)
        //    {
        //        await customServiceClient.Request(request);
        //    }
        //}

        //public async Task RemoteJobStatus(RemoteJobStatus request)
        //{
        //    var customServiceClient = Client;
        //    if (customServiceClient != null)
        //    {
        //        await customServiceClient.Request(request);
        //    }
        //}

        //public async Task RemoteJobCancel(RemoteJobCancel request)
        //{
        //    var customServiceClient = Client;
        //    if (customServiceClient != null)
        //    {
        //        await customServiceClient.Request(request);
        //    }
        //}

        //public async Task Md5JobFile(Md5JobFile request)
        //{
        //    var customServiceClient = Client;
        //    if (customServiceClient != null)
        //    {
        //        await customServiceClient.Request(request);
        //    }
        //}

        //public async Task JobFile(JobFile request)
        //{
        //    var customServiceClient = Client;
        //    if (customServiceClient != null)
        //    {
        //        await customServiceClient.Request(request);
        //    }
        //}
    }
}