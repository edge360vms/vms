﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Polly;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.RecordingServiceAgent.ServiceModel.Api.Thumbs;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class ThumbService : BaseCommunicationService
    {
        public override CustomServiceClient Client
        {
            get
            {
                var svc = ServiceManager;
                // var defaultClient = base.Client;
                //TODO: Use the Regular URL
                var cl = new CustomServiceClient($"http://{svc.ServerList.FirstOrDefault()?.HostName}:23547");

                return cl;
            }
        }

        public ThumbService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<ListThumbnailsResponse> ListThumbnails(ListThumbnails request)
        {
            return await Task.Run(async () =>
            {
                try
                {
                    var resp = await Client.Request(request);
                    return resp;
                }
                catch (Exception e)
                {
                    return default;
                }
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.RecordingAgent;
        }
    }
}