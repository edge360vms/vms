﻿using System;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using RecordingService.ServiceModel.Api.RecordingProfile;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class RecordingProfileService : BaseCommunicationService
    {
        public RecordingProfileService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        /// <summary>
        ///     Deletes a Recording based on Descriptor
        /// </summary>
        /// <param name="desc"></param>
        /// <returns></returns>
        public async Task DeleteRecordingProfile(IRecordingProfileDescriptor desc)
        {
            await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    await customServiceClient.Request(new DeleteRecordingProfile {Id = desc.Id});
                }
            });
        }

        /// <summary>
        ///     Returns all Recordings from the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<GetRecordingProfileResponse> GetRecordingProfile(Guid? id)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null && id != null)
                {
                    var resp = await customServiceClient.Request(new GetRecordingProfile {Id = id.Value});
                    return resp;
                }

                return null;
            });
        }

        /// <summary>
        ///     Returns all Recordings from the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<GetRecordingProfilesByCameraIdResponse> GetRecordingProfileByCameraId(Guid? id)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null && id != null)
                {
                    var resp = await customServiceClient.Request(new GetRecordingProfilesByCameraId {Id = id.Value});
                    return resp;
                }

                return null;
            });
        }


        /// <summary>
        ///     Returns all Recordings from the VMS Server
        /// </summary>
        /// <param name="cameraId"></param>
        /// <returns></returns>
        public async Task<ListRecordingProfilesResponse> GetRecordingProfiles()
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new ListRecordingProfiles());
                    return resp;
                }

                return null;
            });
        }

        /// <summary>
        ///     Creates a Recording on the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<CreateRecordingProfileResponse> CreateRecordingProfile(CreateRecordingProfile desc)
        {
            return await Task.Run(async () =>
            {
                try
                {
                    var customServiceClient = Client;
                    if (customServiceClient != null)
                    {
                        var resp = await customServiceClient.Request(desc);
                        return resp;
                    }
                }
                catch (Exception e)
                {
                    _log?.Info(e);
                }

                return default;
            });
        }

        /// <summary>
        ///     Creates a Recording on the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<UpdateRecordingProfileResponse> UpdateRecordingProfile(UpdateRecordingProfile desc)
        {
            return await Task.Run(async () =>
            {
                try
                {
                    var customServiceClient = Client;
                    if (customServiceClient != null)
                    {
                        var resp = await customServiceClient.Request(desc, useSameClient: true);
                        return resp;
                    }
                }
                catch (Exception e)
                {
                    _log?.Info(e);
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Recording;
        }
    }
}