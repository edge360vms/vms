﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Groups;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class GroupsService : BaseCommunicationService
    {
        public GroupsService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<Group> GetGroupById(Guid groupId, bool includeMembers = false,
            bool includeMemberDetails = false)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new GetGroup
                    {
                        GroupId = groupId, IncludeMemberDetails = includeMemberDetails, IncludeMembers = includeMembers
                    });
                    return resp?.Group;
                }

                return null;
            });
        }

        public async Task<List<Group>> ListGroups(bool includeMembers = false, bool includeMemberDetails = false)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new ListGroups
                        {IncludeMemberDetails = includeMemberDetails, IncludeMembers = includeMembers});
                    return resp?.Groups;
                }

                return null;
            });
        }


        public async Task<UpdateGroupResponse> UpdateGroup(Group group, List<IGroupMember> members)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new UpdateGroup
                    {
                        GroupId = group.GroupId,
                        GroupName = group.GroupName,
                        Members = members
                    });
                    return resp;
                }

                return null;
            });
        }

        public async Task<bool?> DeleteGroupById(Guid? groupId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new DeleteGroup {GroupId = groupId});
                    return resp?.Success;
                }

                return false;
            });
        }

        public async Task<Guid?> CreateGroup(string groupName, List<IGroupMember> members)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new CreateGroup
                        {GroupName = groupName, Members = members});
                    return resp?.GroupId;
                }

                return Guid.Empty;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Authority;
        }
    }
}