﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Managers;
using Edge360.Platform.VMS.Communication.Polly;
using Edge360.Platform.VMS.Communication.SignalR;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Servers;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.Stack;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Enums;
using log4net;
using ServiceStack;
using WispFramework.Extensions.Enums;
using GetServer = Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.Server.GetServer;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class ServiceManager : IDisposable
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public readonly Dictionary<EClientType, ClientManager> ServiceClients =
            new Dictionary<EClientType, ClientManager>();

        public AdConfigZoneService AdConfigZoneService;
        public AdDomainService AdDomainService;
        public AppSettingService AppSettingService;
        public AuditLogService AuditLogService;
        public CameraAdapterService CameraAdapterService;
        public CameraService CameraService;
        public CustomServiceClient Client;
        public GlobalCameraService GlobalCameraService;
        public GlobalRecordingService GlobalRecordingService;
        public GlobalServerService GlobalServerService;
        public GlobalStreamProfileService GlobalStreamProfileService;
        public GlobalZoneService GlobalZoneService;
        public GridLayoutService GridLayoutService;
        public GroupsService GroupsService;
        public List<HubManager> HubManagers = new List<HubManager>();
        public NodeService NodeService;
        public PermissionService PermissionService;
        public PlaybackService PlaybackService;
        public RecordingProfileService RecordingProfileService;
        public RolesService RolesService;

        public List<IServer> ServerList = new List<IServer>();
        public ServerService ServerService;

        public StreamProfileService StreamProfileService;
        public ThumbService ThumbService;

        public TimelineEventService TimelineEventService;

        public UserService UserService;
        public VideoExportJobService VideoExportJobService;
        public VideoExportJobStatusService VideoExportJobStatusService;
        public VideoExportService VideoExportService;
        public VolumeService VolumeService;
        public WatermarkService WatermarkService;
        public ZoneClusterService ZoneClusterService;

        public static bool IsSignalREnabled { get; set; } = true;
        public string BaseUrl { get; set; }

        public Guid ZoneId { get; set; }

        public ServiceManager(CustomServiceClient client, string baseUrl)
        {
            if (client == null)
            {
                return;
            }

            var sw = new Stopwatch();
            sw.Start();

            var strings = baseUrl.Split(':');
            ushort.TryParse(strings[1], out var apiPort);
            InitializeClients(client, new Server {HostName = strings[0], PortApi = apiPort});
            Trace.WriteLine($"ServiceManager ctor took {sw.ElapsedMilliseconds}ms");
        }

        public async void Dispose()
        {
            try
            {
                await Task.Run(() =>
                {
                    foreach (var h in HubManagers.ToList())
                    {
                        try
                        {
                            h?.Dispose();
                        }
                        catch (Exception e)
                        {
                        }
                    }


                    Client?.Dispose();
                    foreach (var client in ServiceClients.Values.ToList())
                    {
                        try
                        {
                            client.Dispose();
                        }
                        catch (Exception e)
                        {
                        }

                        foreach (var c in client.AvailableClients.Clients)
                        {
                            try
                            {
                                c.Dispose();
                            }
                            catch (Exception e)
                            {
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
                _log?.Info($"Disposing ServiceManager {e.GetType()} {e.Message}");
            }
            finally
            {
                //ServiceManager with the same ZoneId might have replaced this one
                var existing = CommunicationManager.ServiceManagers.FirstOrDefault(o => o.Value == this);
                if (existing.Value != null)
                {
                    CommunicationManager.ServiceManagers.TryRemove(ZoneId, out _);
                }
            }
        }

        private void InitializeClients(CustomServiceClient client, IServer server)
        {
            Client = client;
            BaseUrl = $"{server.HostName}:{server.PortApi}";
            if (IsSignalREnabled)
            {
                ConnectSignalR(BaseUrl);
            }

            try
            {
                GenerateClientForServer(client, server);
            }
            catch (Exception e)
            {
            }

            AppSettingService = new AppSettingService(this);
            GlobalStreamProfileService = new GlobalStreamProfileService(this);
            StreamProfileService = new StreamProfileService(this);
            PermissionService = new PermissionService(this);
            ZoneClusterService = new ZoneClusterService(this);
            WatermarkService = new WatermarkService(this);
            PlaybackService = new PlaybackService(this);
            VideoExportJobStatusService = new VideoExportJobStatusService(this);
            GlobalCameraService = new GlobalCameraService(this);
            GlobalRecordingService = new GlobalRecordingService(this);
            UserService = new UserService(this);
            GlobalZoneService = new GlobalZoneService(this);
            GlobalServerService = new GlobalServerService(this);
            ServerService = new ServerService(this);
            VolumeService = new VolumeService(this);
            VideoExportJobService = new VideoExportJobService(this);
            AuditLogService = new AuditLogService(this);
            GridLayoutService = new GridLayoutService(this);
            AdDomainService = new AdDomainService(this);
            CameraAdapterService = new CameraAdapterService(this);
            CameraService = new CameraService(this);
            GroupsService = new GroupsService(this);
            RecordingProfileService = new RecordingProfileService(this);
            NodeService = new NodeService(this);
            RolesService = new RolesService(this);
            ThumbService = new ThumbService(this);
            VideoExportService = new VideoExportService(this);
            TimelineEventService = new TimelineEventService(this);
            AdConfigZoneService = new AdConfigZoneService(this);
        }

        private void ConnectSignalR(string baseUrl)
        {
            try
            {
                var _ = Task.Run(async () =>
                {
                    var hubManager = new HubManager {ServiceManager = this};
                    HubManagers.Add(hubManager);
                    try
                    {
                        hubManager?.ConnectAsync($"https://{baseUrl}/eventhub");
                    }
                    catch (Exception e)
                    {
                    }
                }).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        /// <summary>
        ///     A client manager should exist for every service in a cluster,
        ///     then we will add a client for every server in that cluster for that particular service
        /// </summary>
        /// <param name="rootClient"></param>
        /// <param name="server"></param>
        private void GenerateClientForServer(CustomServiceClient rootClient, IServer server)
        {
            for (EClientType i = 0; i < EClientType.MAX; i++)
            {
                if (!ServiceClients.TryGetValue(i, out var clientManager))
                {
                    clientManager = new ClientManager(i, i.GetValue(), rootClient);
                    ServiceClients[i] = clientManager;
                }

                clientManager.AddClient(server);
            }
        }

        public CustomServiceClient GetClient(EClientType clientType)
        {
            return ServiceClients.TryGetValue(clientType, out var client) ? client.GetClient() : null;
        }

        public static async Task<bool> SetConnectedZoneAndServer(ServiceManager svcMgr,
            Action<Exception> outException = default)
        {
            try
            {
                if (svcMgr.ServerService != null)
                {
                    return await Task.Run(async () =>
                    {
                        var serverInfo = await svcMgr.ServerService.GetServer(new GetServer());
                        if (!CommunicationManager.ServiceManagers.Any()) // firstConnection
                        {
                            if (serverInfo != null)
                            {
                                CommunicationManager.ConnectedZoneId = serverInfo.ZoneId;
                                CommunicationManager.ConnectedServerId = serverInfo.Id;
                            }
                        }

                        if (serverInfo != null)
                        {
                            svcMgr.ZoneId = serverInfo.ZoneId;
                            svcMgr.Client.ServerId = serverInfo.Id;
                            foreach (var c in svcMgr.ServiceClients.Values)
                            {
                                foreach (var a in c.AvailableClients.InternalCache.Values)
                                {
                                    a.ServerId = serverInfo.Id;
                                }
                            }

                            _ = Task.Run(async () => { await svcMgr.PopulateServerList(serverInfo.Id); }).ConfigureAwait(false);
                            return true;
                        }

                        return false;
                    });
                }
            }
            catch (Exception e)
            {
                outException?.Invoke(e);
            }

            return false;
        }

        private async Task PopulateServerList(Guid seedNodeId)
        {
            if (ZoneId != Guid.Empty)
            {
                try
                {
                    await Task.Run(async () =>
                    {
                        var listServersResponse = await GlobalServerService.ListServers(new ListServers {ZoneId = ZoneId});
                        var svList =
                            listServersResponse
                                ?.Servers;
                        if (svList != null)
                        {
                            ServerList = svList;
                        }

                        foreach (var server in ServerList)
                        {
                            if (server.ServerId == seedNodeId)
                            {
                                continue;
                            }

                            try
                            {
                                var cl = new CustomServiceClient($"https://{server.HostName}:{server.PortApi}/coordinator");
                                cl.BearerToken = Client.BearerToken;
                                cl.RefreshToken = Client.RefreshToken;
                                var resp = await cl.Request(new GetCoordinatorInfo(), 1).ConfigureAwait(false);
                                if (resp != null && resp.State == ECoordinatorState.Operational)
                                {
                                    _log?.Info($"Initializing {server.HostName}:{server.PortApi}");
                                    InitializeClients(Client, server);
                                }
                            }
                            catch (Exception e)
                            {
                                _log?.Info(
                                    $"Couldn't connect to server {server.HostName}:{server.PortApi}\n{e.GetType()} {e.Message}");
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                }
            }
        }

        internal static void HandleWebException(CustomServiceClient cl, object obj, WebServiceException exception)
        {
            OnServiceClientWebException?.Invoke(cl, obj, exception);
        }

        public static event Action<CustomServiceClient, object, WebServiceException> OnServiceClientWebException;
    }
}