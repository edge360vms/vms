﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.CameraAdapter;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class CameraReportService : BaseCommunicationService
    {
        public CameraReportService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        //public async Task<object> GetCameraInventoryReport(GetCameraInventoryReport request)
        //{
        //    var customServiceClient =  Client;
        //    if (customServiceClient != null)
        //    {
        //        var resp = await customServiceClient.Request(request);
        //        return resp;
        //    }

        //    return default;
        //}

        public override EClientType GetClientType()
        {
            return EClientType.Cameras;
        }
    }


    public class CameraAdapterService : BaseCommunicationService
    {
        public CameraAdapterService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<SetHomePtzPresetResponse> SetHomePtzPreset(SetHomePtzPreset request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<GetCameraInfoResponse> GetCameraInfo(Guid cameraId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var request = await customServiceClient.Request(new GetCameraInfo
                    {
                        CameraId = cameraId
                    });
                    return request;
                }

                return default;
            });
        }

        public async Task<GetCameraInfoResponse> GetCameraInfoByConnectionString(string hostname, string username,
            string password)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var request = await customServiceClient.Request(new GetCameraInfo
                    {
                        Username = username,
                        Hostname = hostname,
                        Password = password
                    });
                    return request;
                }

                return default;
            });
        }

        public async Task<AddPtzPresetResponse> AddPtzPreset(Guid cameraId, string label)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var request = await customServiceClient.Request(new AddPtzPreset
                    {
                        CameraId = cameraId, PresetLabel = label
                    });
                    return request;
                }

                return default;
            });
        }

        public async Task<RemovePtzPresetResponse> RemovePtzPreset(Guid cameraId, string presetToken)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var request = await customServiceClient.Request(new RemovePtzPreset
                    {
                        CameraId = cameraId, PresetToken = presetToken
                    });
                    return request;
                }

                return default;
            });
        }

        public async Task<UpdatePtzPresetResponse> UpdatePtzPreset(Guid cameraId, string presetToken,
            string presetLabel)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var request = await customServiceClient.Request(new UpdatePtzPreset
                    {
                        CameraId = cameraId,
                        PresetToken = presetToken,
                        PresetLabel = presetLabel
                    });
                    return request;
                }

                return default;
            });
        }

        public async Task<ListDiscoveredCamerasResponse> ListDiscoveredCameras(ListDiscoveredCameras request,
            Guid serverId = default)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = serverId != Guid.Empty ? ServerSpecificClient(serverId) : Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<GetCameraDetailsResponse> GetCameraDetails(GetCameraDetails request, Guid serverId = default)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = serverId != Guid.Empty ? ServerSpecificClient(serverId) : Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<GetCameraDeviceActionResponse> GetCameraDeviceAction(GetCameraDeviceAction request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<ListPtzPresetsResponse> ListPtzPresets(Guid cameraId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var request = await customServiceClient.Request(new ListPtzPresets
                    {
                        CameraId = cameraId,
                    });
                    return request;
                }

                return default;
            });
        }

        public async Task<GoToPtzPresetResponse> GoToPtzPreset(Guid cameraId, string presetToken = null,
            int? presetIndex = null, float panSpeed = 50, float tiltSpeed = 50, float zoomSpeed = 50)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var request = await customServiceClient.Request(new GoToPtzPreset
                    {
                        CameraId = cameraId,
                        PresetIndex = presetIndex,
                        PresetToken = presetToken,
                        PanSpeed = panSpeed,
                        TiltSpeed = tiltSpeed,
                        ZoomSpeed = zoomSpeed,
                    });
                    return request;
                }

                return default;
            });
        }

        public async Task<GoToPtzHomePresetResponse> GoToPtzHomePreset(Guid cameraId, float panSpeed = 50,
            float tiltSpeed = 50, float zoomSpeed = 50)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var request = await customServiceClient.Request(new GoToPtzHomePreset
                    {
                        CameraId = cameraId,
                        PanSpeed = panSpeed,
                        TiltSpeed = tiltSpeed,
                        ZoomSpeed = zoomSpeed,
                    });
                    return request;
                }

                return default;
            });
        }

        public async Task<GetSnapshotResponse> GetSnapshot(Guid cameraId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var request = await customServiceClient.Request(new GetSnapshot
                    {
                        CameraId = cameraId,
                    });
                    return request;
                }

                return default;
            });
        }

        public async Task<object> PtzAction(int actionType, Guid? cameraId = null,
            Point? coordinates = null, Size? imageSize = null, int? panSpeed = null, Size? rectSize = null,
            TimeSpan? timeout = null, int? value = null,
            bool? zoomIn = null, int? zoomSpeed = null)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var request = new GetPtzAction
                    {
                        ActionType = actionType,
                        CameraId = cameraId,
                        CoordinateX = coordinates?.X,
                        CoordinateY = coordinates?.Y,
                        ImageHeight = imageSize?.Height,
                        ImageWidth = imageSize?.Width,
                        PanSpeed = panSpeed,
                        RectHeight = rectSize?.Height,
                        RectWidth = rectSize?.Width,
                        Timeout = timeout,
                        Value = value,
                        ZoomIn = zoomIn,
                        ZoomSpeed = zoomSpeed
                    };
                    // Console.WriteLine($"GetPtzAction {(EPtzAction)request.ActionType} {request}");
                    var resp = await customServiceClient.Request(request);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Cameras;
        }
    }
}