﻿using System;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using RecordingService.ServiceModel.Api.Watermark;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class WatermarkService : BaseCommunicationService
    {
        public WatermarkService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<Guid> CreateWatermark(CreateWatermark request)
        {
            var customServiceClient = Client;
            if (customServiceClient != null)
            {
                var resp = await customServiceClient.Request(request);
                return resp;
            }

            return default;
        }

        public async Task<bool> UpdateWatermark(UpdateWatermark request)
        {
            var customServiceClient = Client;
            if (customServiceClient != null)
            {
                var resp = await customServiceClient.Request(request);
                return resp;
            }

            return default;
        }

        public async Task<bool> DeleteWatermark(DeleteWatermark request)
        {
            var customServiceClient = Client;
            if (customServiceClient != null)
            {
                var resp = await customServiceClient.Request(request);
                return resp;
            }

            return default;
        }

        public async Task<WatermarkDescriptor[]> ListWatermarks(ListWatermarks request)
        {
            var customServiceClient = Client;
            if (customServiceClient != null)
            {
                var resp = await customServiceClient.Request(request);
                return resp;
            }

            return default;
        }

        public async Task<WatermarkDescriptor> GetWatermark(GetWatermark request)
        {
            var customServiceClient = Client;
            if (customServiceClient != null)
            {
                var resp = await customServiceClient.Request(request);
                return resp;
            }

            return default;
        }

        public override EClientType GetClientType()
        {
            return EClientType.Recording;
        }
    }
}