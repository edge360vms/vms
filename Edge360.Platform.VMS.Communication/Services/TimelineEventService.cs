﻿using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using RecordingService.ServiceModel.Api.TimelineEvents;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class TimelineEventService : BaseCommunicationService
    {
        public TimelineEventService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<CreateTimelineEventResponse> CreateTimelineEvent(CreateTimelineEvent timelineEvent)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(timelineEvent);
                    return resp;
                }

                return default;
            });
        }

        public async Task<DeleteTimelineEventResponse> DeleteTimelineEvent(DeleteTimelineEvent request)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<SearchTimelineEventsResponse> SearchTimelineEvents(SearchTimelineEvents request)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<UpdateTimelineEventResponse> UpdateTimelineEvent(UpdateTimelineEvent request)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(request);
                    return resp;
                }

                return default;
            });
        }

        //public async Task<CreateTimelineEventResponse> CreateTimelineEvent(Guid? cameraId, string description, int duration,
        //    string eventType, string name, DateTime? TimestampUtc)
        //{

        //    //if (Client != null)
        //    //{
        //    //    var resp = await Client.Request(new CreateTimelineEvent
        //    //    {
        //    //        UserId = userId,
        //    //        ZoneId = zoneId
        //    //    });
        //    //    return resp;
        //    //}

        //    //return null;
        //}

        public async Task<GetTimelineEventResponse> GetTimelineEvent(GetTimelineEvent timelineEvent)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(timelineEvent);
                    return resp;
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Recording;
        }
    }
}