﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Polly;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Roles;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class RolesService : BaseCommunicationService
    {
        public RolesService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<GetRolesByGroupResponse> GetRolesByGroup(Guid? groupId, Guid? zoneId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new GetRolesByGroup
                    {
                        GroupId = groupId,
                        ZoneId = zoneId
                    });
                    return resp;
                }

                return null;
            });
        }

        public async Task<GetRolesByUserResponse> GetRolesByUser(Guid? userId, Guid? zoneId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new GetRolesByUser
                    {
                        UserId = userId,
                        ZoneId = zoneId
                    });
                    return resp;
                }

                return null;
            });
        }

        public async Task<RemoveRoleResponse> RemoveRole(Guid? groupId, Guid? zoneId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new RemoveRole
                    {
                        GroupId = groupId,
                        ZoneId = zoneId
                    });
                    return resp;
                }

                return null;
            });
        }

        public async Task<AssignRolesResponse> AssignRoles(List<Role> roles)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new AssignRoles
                    {
                        Roles = roles
                    });
                    return resp;
                }

                return null;
            });
        }

        public async Task<List<AssignRoleResponse>> AssignRoleBatch(Guid? groupId,
            Dictionary<Guid, int> zoneRoleDictionary, CustomServiceClient cl = null)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var assignRoles = zoneRoleDictionary.Select(c => new AssignRole
                        {GroupId = groupId, RoleValue = c.Value, ZoneId = c.Key}).ToArray();
                    var resp = await customServiceClient.Request(
                        assignRoles);
                    return resp;
                }

                return null;
            });
        }

        public async Task<AssignRoleResponse> AssignRole(Guid? groupId, int roleValue, Guid? zoneId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new AssignRole
                    {
                        GroupId = groupId,
                        RoleValue = roleValue,
                        ZoneId = zoneId
                    });
                    return resp;
                }

                return null;
            });
        }

        public async Task<GetEffectiveRoleResponse> GetEffectiveRole(Guid? userId = null, Guid? zoneId = null)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new GetEffectiveRole
                    {
                        UserId = userId,
                        ZoneId = zoneId
                    });
                    return resp;
                }

                return null;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Authority;
        }
    }
}