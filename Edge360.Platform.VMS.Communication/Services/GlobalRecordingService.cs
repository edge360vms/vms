﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.RecordingProfiles;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class GlobalRecordingService : BaseCommunicationService
    {
        public GlobalRecordingService(ServiceManager serviceManager) : base(serviceManager)
        {
            ServiceManager = serviceManager;
        }

        /// <summary>
        ///     Returns all Global Recording Profiles from the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<ListRecordingProfilesResponse> ListRecordingProfiles(ListRecordingProfiles request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<List<GetRecordingProfileResponse>> GetRecordingProfileBatchByRecordingProfileId(
            params Guid[] cameraId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    if (cameraId != null)
                    {
                        try
                        {
                            var getCameras = cameraId.Select(id => new GetRecordingProfile {RecordingProfileId = id})
                                .ToArray();
                            var resp = await customServiceClient.Request(
                                getCameras);
                            return resp;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }

                return null;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Authority;
        }
    }
}