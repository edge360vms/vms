﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.Server;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class ServerService : BaseCommunicationService
    {
        public ServerService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<DeleteServerResponse> DeleteServer(DeleteServer request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task UpdateServer(UpdateServer request)
        {
            await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    await customServiceClient.Request(request);
                }
            });
        }

        public async Task<ServerDescriptor[]> GetServers(GetServers request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }


        public async Task<ServerDescriptor> GetServer(GetServer request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Coordinator;
        }
    }
}