﻿using System;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using RecordingService.ServiceModel.Api.VideoExportJobStatus;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class VideoExportJobStatusService : BaseCommunicationService
    {
        public VideoExportJobStatusService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<VideoExportJobStatusDescriptor[]> ListVideoExportJobStatuses(
            ListVideoExportJobStatuses request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<VideoExportJobStatusDescriptor> GetVideoExportJobStatus(GetVideoExportJobStatus request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }


        public async Task<bool> DeleteVideoExportJobStatus(DeleteVideoExportJobStatus request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<bool> UpdateVideoExportJobStatus(UpdateVideoExportJobStatus request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<Guid> CreateVideoExportJobStatus(CreateVideoExportJobStatus request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Recording;
        }
    }
}