﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Nodes;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class NodeService : BaseCommunicationService
    {
        public NodeService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        /// <summary>
        ///     Returns Configuration information from the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<ShowConfigResponse> GetConfig()
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new ShowConfig());
                    return resp;
                }

                return null;
            });
        }

        public async Task<GetNodeResponse> GetNode(Guid id)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    try
                    {
                        var resp = await customServiceClient.Request(new GetNodeById {NodeId = id});
                        return resp;
                    }
                    catch (Exception)
                    {
                    }
                }

                return null;
            });
        }

        public async Task<GetNodeTreeResponse> GetNodeTree(GetNodeTree request)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<GetNodesResponse> GetNodeChildren(params Guid?[] id)
        {
            return await Task.Run(async () =>
            {
                if (id.Length == 0)
                {
                    return null;
                }

                return await Task.Run(async () =>
                {
                    var customServiceClient = Client;
                    if (customServiceClient != null)
                    {
                        if (id.Length == 1)
                        {
                            var resp = await customServiceClient.Request(new GetChildrenByParentId
                            {
                                ParentId = (Guid) id.FirstOrDefault()
                            });
                            return resp;
                        }
                        else
                        {
                            var parents = id.Select(i => (Guid) i).ToList();

                            var resp = await customServiceClient.Request(new GetChildrenByParentIds
                                {ParentIds = parents.ToArray()}
                            );
                            return resp;
                        }
                    }

                    return null;
                });


                return null;
            });
        }

        public async Task<UpdateNodeResponse> UpdateNode(NodeDescriptor desc)
        {
            return await Task.Run(async () =>
            {
                try
                {
                    return await Client.Request(new UpdateNode
                    {
                        Id = desc.Id,
                        Label = desc.Label,
                        ParentId = desc.ParentId,
                        Type = desc.Type
                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                return default;
            });
        }

        public async Task UpdateNodeLabel(NodeDescriptor desc)
        {
            await Task.Run(async () =>
            {
                try
                {
                    if (desc.Id != null)
                    {
                        await Client.PutAsync(new UpdateNodeLabel
                        {
                            Id = (Guid) desc.Id,
                            Label = desc.Label
                        });
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            });
        }

        public async void UpdateNodeType(NodeDescriptor desc)
        {
            await Task.Run(async () =>
            {
                if (desc.Id != null)
                {
                    await Client.PutAsync(new UpdateNodeType
                    {
                        Id = (Guid) desc.Id,
                        Type = desc.Type
                    });
                }
            });
        }

        public async void UpdateNodeParentId(NodeDescriptor desc)
        {
            await Task.Run(async () =>
            {
                await Client.PutAsync(new UpdateNode
                {
                    Id = desc.Id,
                    Label = desc.Label,
                    ParentId = desc.ParentId,
                    Type = desc.Type
                });
            });
        }

        public async Task<CreateNodeResponse> CreateNode(NodeDescriptor desc)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new CreateNode
                    {
                        Id = Guid.Empty,
                        Label = desc.Label,
                        ParentId = desc.ParentId,
                        Type = desc.Type
                    });
                    return resp;
                }

                return null;
            });
        }


        public async Task DeleteNode(NodeDescriptor desc)
        {
            await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    if (desc.Id != null)
                    {
                        await customServiceClient.Request(new DeleteNode
                        {
                            Id = (Guid) desc.Id
                        });
                    }
                }
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Authority;
        }
    }
}