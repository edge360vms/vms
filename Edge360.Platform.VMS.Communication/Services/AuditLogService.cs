﻿using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AdminService.ServiceModel.Api.AuditLog;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class AuditLogService : BaseCommunicationService
    {
        public AuditLogService(ServiceManager serviceManager) : base(serviceManager)
        {
        }
        //public async Task<CreateAuditLogResponse> CreateAuditLog(CreateAuditLog createAuditLog)
        //{
        //    if (Client != null)
        //    {
        //        var resp = await Client.Request(createAuditLog);
        //        if (resp != null)
        //        {
        //            return resp;
        //        }
        //    }

        //    return default;
        //}

        public async Task<GetAuditLogResponse> GetAuditLog(GetAuditLog getAuditLog)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(getAuditLog);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }

        public async Task<SearchAuditLogResponse> SearchAuditLog(SearchAuditLog searchAuditLog)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(searchAuditLog);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Management;
        }
    }
}