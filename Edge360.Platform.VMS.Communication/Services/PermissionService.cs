﻿using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Permissions;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class PermissionService : BaseCommunicationService
    {
        public PermissionService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<DeletePermissionsResponse> DeletePermissions(DeletePermissions request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return null;
            });
        }

        public async Task<GetPermissionsByGroupResponse> GetPermissionsByGroup(GetPermissionsByGroup request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return null;
            });
        }

        public async Task<SetPermissionResponse> SetPermission(SetPermission request)
        {
            var customServiceClient = Client;
            if (customServiceClient != null)
            {
                var resp = await customServiceClient.Request(request);
                return resp;
            }

            return null;
        }

        public override EClientType GetClientType()
        {
            return EClientType.Authority;
        }
    }
}