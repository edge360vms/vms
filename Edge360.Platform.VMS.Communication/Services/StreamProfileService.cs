﻿using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.CameraService.ServiceModel.Api.StreamingProfile;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class StreamProfileService : BaseCommunicationService
    {
        public StreamProfileService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        public async Task<CreateStreamingProfileResponse> CreateStreamingProfile(CreateStreamingProfile request)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<UpdateStreamingProfileResponse> UpdateStreamingProfile(UpdateStreamingProfile request)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<DeleteStreamingProfileResponse> DeleteStreamingProfile(DeleteStreamingProfile request)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<ListStreamingProfilesResponse> ListStreamingProfiles(ListStreamingProfiles request)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<GetStreamingProfileResponse> GetStreamingProfile(GetStreamingProfile request)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<GetStreamingProfileSessionResponse> GetStreamingProfileSession(
            GetStreamingProfileSession request)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(request);
                    return resp;
                }

                return default;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Cameras;
        }
    }
}