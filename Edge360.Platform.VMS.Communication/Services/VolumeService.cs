﻿using System;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.CoordinatorService.ServiceModel.Api.Volumes;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class VolumeService : BaseCommunicationService
    {
        public VolumeService(ServiceManager serviceManager) : base(serviceManager)
        {
        }

        /// <summary>
        ///     Returns Volumes from the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<Guid> CreateVolume(CreateVolume volume)
        {
            return await Task.Run(async () =>
            {
                var cl = volume.ServerId != Guid.Empty ? ServerSpecificClient(volume.ServerId) : Client;
                if (cl != null)
                {
                    var resp = await cl.Request(volume, useSameClient: true);
                    return resp;
                }

                return Guid.Empty;
            });
        }

        //public async Task<object> Test(EndpointsAdd request)
        //{
        //    var cl = Client;
        //    if (cl != null)
        //    {
        //        await cl.Request(request);
        //    }

        //    return true;
        //}

        // 


        /// <summary>
        ///     Returns Volumes from the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<GetVolumesResponse> GetVolumes(Guid id)
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(new GetVolumes {Id = id});
                    return resp;
                }

                return null;
            });
        }


        /// <summary>
        ///     Returns Volumes from the VMS Server
        /// </summary>
        /// <returns></returns>
        public async Task<GetVolumesResponse> GetVolumes()
        {
            return await Task.Run(async () =>
            {
                var cl = Client;
                if (cl != null)
                {
                    var resp = await cl.Request(new GetVolumes());

                    return resp;
                }

                return null;
            });
        }

        public async Task<Guid> UpdateVolume(UpdateVolume desc)
        {
            return await Task.Run(async () =>
            {
                var cl = desc.ServerId != Guid.Empty ? ServerSpecificClient(desc.ServerId) : Client;
                if (desc.Id == Guid.Empty)
                {
                    return new Guid();
                }

                if (cl == null)
                {
                    return Guid.Empty;
                }

                var volumeId = await cl.Request(desc, useSameClient: true);
                return volumeId;
            });
        }

        public async Task<GetVolumeInfoResponse> GetVolumeInfo(GetVolumeInfo request, Guid serverId)
        {
            return await Task.Run(async () =>
            {
                var cl = serverId != Guid.Empty ? ServerSpecificClient(serverId) : Client;
                if (cl != null)
                {
                    var resp = await cl.Request(request);
                    if (resp != null)
                    {
                        return resp;
                    }
                }

                return default;
            });
        }


        public async Task DeleteVolume(Guid id = new Guid(), Guid serverId = default)
        {
            await Task.Run(async () =>
            {
                var cl = id != Guid.Empty ? ServerSpecificClient(serverId) : Client;
                if (cl != null)
                {
                    await cl.Request(new DeleteVolume {Id = id});
                }
            });

            // return null;
        }

        public override EClientType GetClientType()
        {
            return EClientType.Coordinator;
        }
    }
}