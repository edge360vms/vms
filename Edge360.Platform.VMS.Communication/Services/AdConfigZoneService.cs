﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Util;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.AdConfigZone;

namespace Edge360.Platform.VMS.Communication.Services
{
    public class AdConfigZoneService : BaseCommunicationService
    {
        public AdConfigZoneService(ServiceManager serviceManager) : base(serviceManager)
        {
        }
        //CommunicationManager.ServiceClients[CommunicationManager.HARD_CODED_ZONE_ID]
        //?.FirstOrDefault(o => o.Key == EClientType.Authority).Value;

        public async Task<TestAdConfigZoneResponse> TestConfigZone(Guid? zoneId, Guid? activeDirectoryId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new TestAdConfigZone
                    {
                        ActiveDirectoryId = activeDirectoryId,
                        ZoneId = zoneId
                    });
                    return resp;
                }

                return null;
            });
        }

        public async Task<TestAdConfigResponse> TestConfig(
            string username, string password, string addresses)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new TestAdConfig
                    {
                        Addresses = addresses,
                        Password = password,
                        Username = username
                    });
                    return resp;
                }

                return null;
            });
        }

        public async Task<AdConfigZone> GetConfig(Guid? zoneId, Guid? activeDirectoryId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new GetAdConfigZone
                    {
                        ActiveDirectoryId = activeDirectoryId,
                        ZoneId = zoneId
                    });
                    return resp?.AdConfigZone;
                }

                return null;
            });
        }

        public async Task<QueryAdGroupsResponse> QueryAdGroups(QueryAdGroupsV2 request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(request);
                    return resp;
                }

                return null;
            });
        }


        public async Task<QueryAdGroupsResponse> QueryAdGroups(Guid? activeDirectoryId, string filter)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new QueryAdGroups
                    {
                        ActiveDirectoryId = activeDirectoryId,
                        GroupNameMatch = filter
                    });
                    return resp;
                }

                return null;
            });
        }

        public async Task<List<AdConfigZone>> ListConfigs()
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new ListAdConfigZones()
                    );
                    return resp?.AdConfigZones;
                }

                return null;
            });
        }

        public async Task<UpdateAdConfigZoneResponse> UpdateConfig(UpdateAdConfigZone request)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(
                        request);
                    return resp;
                }

                return default;
            });
        }

        public async Task<Guid?> CreateConfig(Guid? zoneId,
            string username, string password, string address)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new CreateAdConfigZone
                    {
                        Addresses = address,
                        Password = password,
                        Username = username,
                        ZoneId = zoneId
                    });
                    return resp?.ActiveDirectoryId;
                }

                return Guid.Empty;
            });
        }

        public async Task<bool> DeleteConfigById(Guid? zoneId, Guid? activeDirectoryId)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.Request(new DeleteAdConfigZone
                    {
                        ActiveDirectoryId = activeDirectoryId,
                        ZoneId = zoneId
                    });
                    return resp != null && resp.Success;
                }

                return false;
            });
        }

        public async Task<Guid?> GetAdConfig(Guid id)
        {
            return await Task.Run(async () =>
            {
                var customServiceClient = Client;
                if (customServiceClient != null)
                {
                    var resp = await customServiceClient.GetAsync(new GetAdConfigZone());
                    return resp?.AdConfigZone?.ZoneId;
                    //  var resp = await Client.GetAsync<>(new GetAdConfigs());
                }

                return Guid.Empty;
            });
        }

        public override EClientType GetClientType()
        {
            return EClientType.Authority;
        }
    }
}