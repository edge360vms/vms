﻿using WispFramework.Attributes;

namespace Edge360.Platform.VMS.Communication.Enums
{
    public enum EClientType
    {
        [Value("authority")] Authority,
        [Value("cameras")] Cameras,
        [Value("admin")] Management,
        [Value("recording")] Recording,
        [Value("recordingagent")] RecordingAgent,
        [Value("coordinator")] Coordinator,
        [Value("telemetry")] Telemetry,
        MAX
    }
}