﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Communication.Polly;
using log4net;
using Polly;
using ServiceStack;

namespace Edge360.Platform.VMS.Communication.Util
{
    public static class WebUtil
    {
        public enum ERouteVerb
        {
            DELETE,
            POST,
            PUT,
            GET,
            PATCH,
        }

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static bool DisplayVerboseRequests = true; //true;
        //public static async Task GetRetryAsync(this CustomServiceClientcl, IReturnVoid request)
        //{
        //    try
        //    {
        //        var polly = await Policy.Handle<Exception>(
        //                e =>
        //                {
        //                    if (!HandleError(e))
        //                    {
        //                        return false;
        //                    }

        //                    return !e.IsUnauthorized();
        //                }).WaitAndRetryForeverAsync(count =>
        //            {
        //                _log?.Info($"Retrying Get {count} {request}");
        //                return TimeSpan.FromSeconds(1);
        //            })
        //            .ExecuteAsync(async
        //                () =>
        //            {
        //                await cl.GetAsync(request);
        //                return true;
        //            });
        //    }
        //    catch (Exception e)
        //    {
        //        _log?.Info(e);
        //    }
        //}

        //public static async Task<T> GetRetryAsync<T>(this CustomServiceClientcl, IReturn<T> request)
        //{
        //    try
        //    {
        //        var polly = await Policy.Handle<Exception>(
        //                e =>
        //                {
        //                    if (!HandleError(e))
        //                    {
        //                        return false;
        //                    }

        //                    return !e.IsUnauthorized();
        //                }).WaitAndRetryForeverAsync(count =>
        //            {
        //                _log?.Info($"Retrying Get {count} {request}");
        //                return TimeSpan.FromSeconds(1);
        //            })
        //            .ExecuteAsync(async
        //                () =>
        //            {
        //                var resp = await cl.GetAsync(request);
        //                return resp;
        //            });
        //        return polly;
        //    }
        //    catch (Exception e)
        //    {
        //        _log?.Info(e);
        //    }

        //    return default;
        //}

        //public static async Task<T> DeleteRetryAsync<T>(this CustomServiceClientcl, IReturn<T> request)
        //{
        //    try
        //    {
        //        var polly = await Policy.Handle<Exception>(
        //                e =>
        //                {
        //                    if (!HandleError(e))
        //                    {
        //                        return false;
        //                    }

        //                    return !e.IsUnauthorized();
        //                }).WaitAndRetryForeverAsync(count =>
        //            {
        //                _log?.Info($"Retrying Get {count} {request}");
        //                return TimeSpan.FromSeconds(1);
        //            })
        //            .ExecuteAsync(async
        //                () =>
        //            {
        //                var resp = await cl.DeleteAsync(request);
        //                return resp;
        //            });
        //        return polly;
        //    }
        //    catch (Exception e)
        //    {
        //        _log?.Info(e);
        //    }

        //    return default;
        //}

        //public static async Task DeleteRetryAsync(this CustomServiceClientcl, IReturnVoid request)
        //{
        //    try
        //    {
        //        await Policy.Handle<Exception>(
        //                e =>
        //                {
        //                    if (!HandleError(e))
        //                    {
        //                        return false;
        //                    }

        //                    return !e.IsUnauthorized();
        //                }).WaitAndRetryForeverAsync(count =>
        //            {
        //                _log?.Info($"Retrying Get {count} {request}");
        //                return TimeSpan.FromSeconds(1);
        //            })
        //            .ExecuteAsync(async
        //                () =>
        //            {
        //                await cl.DeleteAsync(request); 
        //            }); 
        //    }
        //    catch (Exception e)
        //    {
        //        _log?.Info(e);
        //    } 
        //}

        //public static async Task<T> PutRetryAsync<T>(this CustomServiceClientcl, IReturn<T> request)
        //{
        //    try
        //    {
        //        var polly = await Policy.Handle<Exception>(
        //                e =>
        //                {
        //                    if (!HandleError(e))
        //                    {
        //                        return false;
        //                    }

        //                    return !e.IsUnauthorized();
        //                }).WaitAndRetryForeverAsync(count =>
        //            {
        //                _log?.Info($"Retrying Post {count} {request} {cl.BaseUri}");
        //                return TimeSpan.FromSeconds(1);
        //            })
        //            .ExecuteAsync(async
        //                () =>
        //            {
        //                var resp = await cl.PutAsync(request);
        //                return resp;
        //            });
        //        return polly;
        //    }
        //    catch (Exception e)
        //    {
        //        _log?.Info(e);
        //    }

        //    return default;
        //}

        //public static async Task PutRetryAsync(this CustomServiceClientcl, IReturnVoid request)
        //{
        //    try
        //    {
        //        await Policy.Handle<Exception>(
        //                e =>
        //                {
        //                    if (!HandleError(e))
        //                    {
        //                        return false;
        //                    }

        //                    return !e.IsUnauthorized();
        //                }).WaitAndRetryForeverAsync(count =>
        //            {
        //                _log?.Info($"Retrying Post {count} {request} {cl.BaseUri}");
        //                return TimeSpan.FromSeconds(1);
        //            })
        //            .ExecuteAsync(async
        //                () =>
        //            {
        //                await cl.PutAsync(request); 
        //            }); 
        //    }
        //    catch (Exception e)
        //    {
        //        _log?.Info(e);
        //    }
        //}

        //public static async Task<T> PostRetryAsync<T>(this CustomServiceClientcl, IReturn<T> request)
        //{
        //    try
        //    {
        //        var polly = await Policy.Handle<Exception>(
        //                e =>
        //                {
        //                    if (!HandleError(e))
        //                    {
        //                        return false;
        //                    }

        //                    return !e.IsUnauthorized();
        //                }).WaitAndRetryForeverAsync(count =>
        //            {
        //                _log?.Info($"Retrying Post {count} {request} {cl.BaseUri}");
        //                return TimeSpan.FromSeconds(1);
        //            })
        //            .ExecuteAsync(async
        //                () =>
        //            {
        //                var resp = await cl.PostAsync(request);
        //                return resp;
        //            });
        //        return polly;
        //    }
        //    catch (Exception e)
        //    {
        //        _log?.Info(e);
        //    }

        //    return default;
        //}

        public static int DefaultRetryCount = 15;

        public static List<WebExceptionStatus> IgnoredBaseExceptions { get; set; } = new List<WebExceptionStatus>();

        public static async Task<List<T>> Request<T>(this CustomServiceClient cl, IReturn<T>[] request,
            int? retryCount = null, bool useSameClient = false, Action<Exception> ex = default,
            CancellationTokenSource cancellationToken = null)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    var policyContext = new Context();
                    var currentClient = cl;
                    if (currentClient != null)
                    {
                        if (cancellationToken != null)
                        {
                            policyContext = new Context("RetryContext") {{"CancellationTokenSource", cancellationToken}};
                        }

                        var polly = await Policy.Handle<Exception>(
                                e =>
                                {
                                    if (!RetryOnError(e))
                                    {
                                        return false;
                                    }

                                    if (e is WebServiceException exception)
                                    {
                                        cl?.HandleWebException(request, exception);
                                    }

                                    return !e.IsUnauthorized();
                                }).WaitAndRetryAsync(retryCount ?? DefaultRetryCount, count =>
                            {
                                currentClient.Failures.Value++;
                                if (!useSameClient)
                                {
                                    currentClient = currentClient?.ClientManager?.GetClient() ?? currentClient;
                                }

                                _log?.Info($"RetryingBatch {count} {request} {currentClient.BaseUri}");
                                return TimeSpan.FromSeconds(1);
                            })
                            .ExecuteAsync(async
                                (ctx, ct) =>
                            {
                                if (DisplayVerboseRequests)
                                {
                                    _log?.Info($"{request}");
                                }

                                var resp = await cl.SendAllAsync<T>(
                                    request,
                                    new CancellationToken());
                                return resp;
                            }, policyContext, cancellationToken?.Token ?? default);
                        return polly;
                    }

                    return default;
                });
            }
            catch (WebServiceException webException)
            {
                cl?.HandleWebException(request, webException);
                ex?.Invoke(webException);
                if (CommunicationManager.EnableExceptionThrowing)
                {
                    throw;
                }
            }
            catch (Exception e)
            {
                _log?.Info($"{request.GetType()} {e.GetType()} {e.Message}");
                ex?.Invoke(e);
                if (CommunicationManager.EnableExceptionThrowing)
                {
                    throw;
                }
            }

            return default;
        }

        public static async Task<T> Request<T>(this CustomServiceClient cl, IReturn<T> request, int? retryCount = null,
            bool useSameClient = false, Action<Exception> ex = default,
            CancellationTokenSource cancellationToken = default)
        {
            var currentClient = cl;
            try
            {
                return await Task.Run(async () =>
                {
                    var policyContext = new Context();
                    var route = request.GetType().GetCustomAttributes<RouteAttribute>()
                        .Where(o => !string.IsNullOrEmpty(o.Verbs)).FirstOrDefault();
                    if (route == null || !Enum.TryParse(route.Verbs, true, out ERouteVerb routeType))
                    {
                        return default;
                    }

                    if (cancellationToken != null)
                    {
                        policyContext = new Context("RetryContext") {{"CancellationTokenSource", cancellationToken}};
                    }

                    var polly = await Policy.Handle<Exception>(
                            e =>
                            {
                                if (!RetryOnError(e))
                                {
                                    return false;
                                }

                                if (e is WebServiceException exception)
                                {
                                    currentClient?.HandleWebException(request, exception);
                                }

                                return !e.IsUnauthorized();
                            }).WaitAndRetryAsync(retryCount ?? DefaultRetryCount, count =>
                        {
                            currentClient.Failures.Value++;

                            cancellationToken?.Token.ThrowIfCancellationRequested();

                            if (!useSameClient)
                            {
                                currentClient = currentClient?.ClientManager?.GetClient() ?? currentClient;
                            }

                            _log?.Info($"Retrying {routeType} {count} {request}  {currentClient.BaseUri}");
                            return TimeSpan.FromSeconds(count);
                        })
                        .ExecuteAsync(async
                            (ct, ctx) =>
                        {
                            ctx.ThrowIfCancellationRequested();
                            if (DisplayVerboseRequests)
                            {
                                _log?.Info($"{request}");
                            }

                            switch (routeType)
                            {
                                case ERouteVerb.POST:
                                    return await currentClient.PostAsync(request);
                                case ERouteVerb.PUT:
                                    return await currentClient.PutAsync(request);
                                case ERouteVerb.DELETE:
                                    return await currentClient.DeleteAsync(request);
                                case ERouteVerb.PATCH:
                                    return await currentClient.PatchAsync(request);
                                case ERouteVerb.GET:
                                default:
                                    return await currentClient.GetAsync(request);
                            }
                        }, policyContext, cancellationToken?.Token ?? default);
                    return polly;
                });
            }
            catch (WebServiceException webException)
            {
                currentClient?.HandleWebException(request, webException);
                ex?.Invoke(webException);
                if (CommunicationManager.EnableExceptionThrowing)
                {
                    throw;
                }
            }
            catch (Exception e)
            {
                _log?.Info($"{request.GetType()} {e.GetType()} {e.Message}");
                ex?.Invoke(e);
                if (CommunicationManager.EnableExceptionThrowing)
                {
                    throw;
                }

                //throw;
            }

            return default;
        }

        public static async Task Request(this CustomServiceClient cl, IReturnVoid request, bool useSameClient = false,
            Action<Exception> ex = default, CancellationTokenSource cancellationToken = default)
        {
            try
            {
                await Task.Run(async () =>
                {
                    var currentClient = cl;
                    var policyContext = new Context();
                    if (cl != null)
                    {
                        var route = request.GetType().GetCustomAttributes<RouteAttribute>().FirstOrDefault();
                        if (route == null || !Enum.TryParse(route.Verbs, true, out ERouteVerb routeType))
                        {
                            return;
                        }

                        if (cancellationToken != null)
                        {
                            policyContext = new Context("RetryContext") {{"CancellationTokenSource", cancellationToken}};
                        }

                        await Policy.Handle<Exception>(
                                e =>
                                {
                                    if (!RetryOnError(e))
                                    {
                                        return false;
                                    }

                                    if (e is WebServiceException exception)
                                    {
                                        cl?.HandleWebException(request, exception);
                                    }

                                    return !e.IsUnauthorized();
                                }).WaitAndRetryForeverAsync(count =>
                            {
                                currentClient.Failures.Value++;
                                if (!useSameClient)
                                {
                                    currentClient = currentClient?.ClientManager?.GetClient() ?? currentClient;
                                }

                                _log?.Info($"Retrying {routeType} {count} {request} {currentClient.BaseUri}");

                                return TimeSpan.FromSeconds(1);
                            })
                            .ExecuteAsync(async
                                (ct, ctx) =>
                            {
                                ctx.ThrowIfCancellationRequested();
                                switch (routeType)
                                {
                                    case ERouteVerb.POST:
                                        await cl.PostAsync(request);
                                        break;
                                    case ERouteVerb.PUT:
                                        await cl.PutAsync(request);
                                        break;
                                    case ERouteVerb.DELETE:
                                        await cl.DeleteAsync(request);
                                        break;
                                    default:
                                        await cl.GetAsync(request);
                                        break;
                                }
                            }, policyContext, cancellationToken?.Token ?? default);
                    }
                });
            }
            catch (WebServiceException webException)
            {
                cl?.HandleWebException(request, webException);
                ex?.Invoke(webException);
                if (CommunicationManager.EnableExceptionThrowing)
                {
                    throw;
                }
            }
            catch (Exception e)
            {
                _log?.Info(
                    $"{request.GetType()} {e.Message}");
                ex?.Invoke(e);
                if (CommunicationManager.EnableExceptionThrowing)
                {
                    throw;
                }
            }
        }

        private static bool RetryOnError(Exception e)
        {
            if (e is WebException exception)
            {
                Console.WriteLine($"{e.Message}");
                var statusCode = exception.Status;
                if (IgnoredBaseExceptions.Any(o => o == statusCode))
                {
                    Trace.WriteLine($"Matching Exception Failed! {statusCode}");

                    return false;
                }

                switch (statusCode)
                {
                    case WebExceptionStatus.Success:
                        break;
                    case WebExceptionStatus.NameResolutionFailure:
                        break;
                    case WebExceptionStatus.ConnectFailure:
                        return true;
                    case WebExceptionStatus.ReceiveFailure:
                        break;
                    case WebExceptionStatus.SendFailure:
                        break;
                    case WebExceptionStatus.PipelineFailure:
                        break;
                    case WebExceptionStatus.RequestCanceled:
                        return false;
                    case WebExceptionStatus.ProtocolError:
                        break;
                    case WebExceptionStatus.ConnectionClosed:
                        break;
                    case WebExceptionStatus.TrustFailure:
                        break;
                    case WebExceptionStatus.SecureChannelFailure:
                        break;
                    case WebExceptionStatus.ServerProtocolViolation:
                        break;
                    case WebExceptionStatus.KeepAliveFailure:
                        break;
                    case WebExceptionStatus.Pending:
                        break;
                    case WebExceptionStatus.Timeout:
                        break;
                    case WebExceptionStatus.ProxyNameResolutionFailure:
                        break;
                    case WebExceptionStatus.UnknownError:
                        break;
                    case WebExceptionStatus.MessageLengthLimitExceeded:
                        break;
                    case WebExceptionStatus.CacheEntryNotFound:
                        break;
                    case WebExceptionStatus.RequestProhibitedByCachePolicy:
                        break;
                    case WebExceptionStatus.RequestProhibitedByProxy:
                        break;
                }
            }

            if (e is WebServiceException serviceException)
            {
                _log?.Info(
                    $"{serviceException.ErrorMessage} {e.Message}, StatusCode: {serviceException.StatusCode}");
                var statusCode = (HttpStatusCode) serviceException.StatusCode;
                if (HandleError(statusCode, out var handleError))
                {
                    return handleError;
                }
            }

            return true;
        }

        private static bool HandleError(HttpStatusCode statusCode, out bool handleError)
        {
            handleError = false;
            switch (statusCode)
            {
                case HttpStatusCode.Continue:
                    break;
                case HttpStatusCode.SwitchingProtocols:
                    break;
                case HttpStatusCode.OK:
                    break;
                case HttpStatusCode.Created:
                    break;
                case HttpStatusCode.Accepted:
                    break;
                case HttpStatusCode.NonAuthoritativeInformation:
                    break;
                case HttpStatusCode.NoContent:
                    break;
                case HttpStatusCode.ResetContent:
                    break;
                case HttpStatusCode.PartialContent:
                    break;
                case HttpStatusCode.MultipleChoices:
                    break;
                case HttpStatusCode.MovedPermanently:
                    break;
                case HttpStatusCode.Found:
                    break;
                case HttpStatusCode.SeeOther:
                    break;
                case HttpStatusCode.NotModified:
                    break;
                case HttpStatusCode.UseProxy:
                    break;
                case HttpStatusCode.Unused:
                    break;
                case HttpStatusCode.TemporaryRedirect:
                    break;
                case HttpStatusCode.BadRequest:
                {
                    handleError = false;
                    return true;
                }
                case HttpStatusCode.Unauthorized:
                    //CommunicationService.ConnectAsync().Wait();
                    break;
                case HttpStatusCode.PaymentRequired:
                case HttpStatusCode.Forbidden:
                case HttpStatusCode.MethodNotAllowed:
                case HttpStatusCode.NotAcceptable:
                case HttpStatusCode.ProxyAuthenticationRequired:
                case HttpStatusCode.NotFound: //API Missing Possible Version Mismatch?
                {
                    handleError = false;
                    return true;
                }

                case HttpStatusCode.RequestTimeout:
                {
                    handleError = true;
                    return true;
                }
                case HttpStatusCode.Conflict:
                case HttpStatusCode.Gone:
                case HttpStatusCode.LengthRequired:
                case HttpStatusCode.PreconditionFailed:
                case HttpStatusCode.RequestEntityTooLarge:
                case HttpStatusCode.RequestUriTooLong:
                case HttpStatusCode.UnsupportedMediaType:
                {
                    handleError = false;
                    return true;
                }
                case HttpStatusCode.RequestedRangeNotSatisfiable:
                    break;
                case HttpStatusCode.ExpectationFailed:
                    break;
                case HttpStatusCode.UpgradeRequired:
                    break;
                case HttpStatusCode.InternalServerError:
                case HttpStatusCode.NotImplemented:
                case HttpStatusCode.BadGateway:
                case HttpStatusCode.ServiceUnavailable:
                case HttpStatusCode.GatewayTimeout:
                case HttpStatusCode.HttpVersionNotSupported:
                {
                    handleError = false;
                    return true;
                }
                default:
                {
                    handleError = true;
                    return true;
                }
            }

            return false;
        }

        //public static async Task<string> GetRequest(string url)
        //{
        //    try
        //    {
        //        var client = new HttpClient();

        //        var reply = client.GetAsync(url);

        //        var connectionReply = await reply.Result.Content.ReadAsStringAsync();

        //        return connectionReply;
        //    }
        //    catch (Exception)
        //    {
        //        return "";
        //    }
        //}

        //public static async Task<T> GetRequest<T>(string url)
        //{
        //    try
        //    {
        //        var client = new HttpClient();

        //        var reply = client.GetAsync(url);

        //        var connectionReply = await reply.Result.Content.ReadAsAsync<T>();

        //        return connectionReply;
        //    }
        //    catch (Exception)
        //    {
        //        return default;
        //    }
        //}

        //public static async Task<T> PostRequest<T>(string url, object content)
        //{
        //    try
        //    {
        //        var client = new HttpClient();

        //        var reply = client.PostAsJsonAsync(url,
        //            content);

        //        var connectionReply = await reply.Result.Content.ReadAsAsync<T>();

        //        return connectionReply;
        //    }
        //    catch (Exception)
        //    {
        //        return default;
        //    }
        //}
    }
}