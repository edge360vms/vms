﻿using ServiceStack;

namespace Edge360.Platform.VMS.Communication.Util
{
    public static class ObjectUtil
    {
        public static string CollapseWhitespaces(this string str)
        {
            return str.CollapseWhitespace();
        }

        public static T Convert<T>(this object obj)
        {
            return obj.ConvertTo<T>();
        }
    }
}