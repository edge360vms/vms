﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Edge360.Platform.VMS.Communication.Discovery
{
    //public partial class CameraDiscoveryResponse
    //{
    //    [JsonProperty("SOAP-ENV:Envelope")] public SoapEnvEnvelope SoapEnvEnvelope { get; set; }
    //}

    //public class SoapEnvEnvelope
    //{
    //    [JsonProperty("SOAP-ENV:Body")] public SoapEnvBody SoapEnvBody { get; set; }

    //    [JsonProperty("SOAP-ENV:Header")] public SoapEnvHeader SoapEnvHeader { get; set; }

    //    [JsonProperty("-xmlns:d")] public Uri XmlnsD { get; set; }

    //    [JsonProperty("-xmlns:dn")] public Uri XmlnsDn { get; set; }

    //    [JsonProperty("-xmlns:SOAP-ENC")] public Uri XmlnsSoapEnc { get; set; }

    //    [JsonProperty("-xmlns:SOAP-ENV")] public Uri XmlnsSoapEnv { get; set; }

    //    [JsonProperty("-xmlns:tds")] public Uri XmlnsTds { get; set; }

    //    [JsonProperty("-xmlns:wsa")] public Uri XmlnsWsa { get; set; }
    //}

    //public class SoapEnvBody
    //{
    //    [JsonProperty("d:ProbeMatches")] public DProbeMatches DProbeMatches { get; set; }
    //}

    //public class DProbeMatches
    //{
    //    [JsonProperty("d:ProbeMatch")] public DProbeMatch DProbeMatch { get; set; }
    //}

    //public class DProbeMatch
    //{
    //    [JsonProperty("d:MetadataVersion")]
    //    [JsonConverter(typeof(PurpleParseStringConverter))]
    //    public long DMetadataVersion { get; set; }

    //    [JsonProperty("d:Scopes")] public string DScopes { get; set; }

    //    [JsonProperty("d:Types")] public string DTypes { get; set; }

    //    [JsonProperty("d:XAddrs")] public Uri DXAddrs { get; set; }

    //    [JsonProperty("wsa:EndpointReference")]
    //    public WsaEndpointReference WsaEndpointReference { get; set; }
    //}

    //public class WsaEndpointReference
    //{
    //    [JsonProperty("wsa:Address")] public string WsaAddress { get; set; }
    //}

    //public class SoapEnvHeader
    //{
    //    [JsonProperty("d:AppSequence")] public DAppSequence DAppSequence { get; set; }

    //    [JsonProperty("wsa:Action")] public Wsa WsaAction { get; set; }

    //    [JsonProperty("wsa:MessageID")] public string WsaMessageId { get; set; }

    //    [JsonProperty("wsa:RelatesTo")] public string WsaRelatesTo { get; set; }

    //    [JsonProperty("wsa:To")] public Wsa WsaTo { get; set; }
    //}

    //public class DAppSequence
    //{
    //    [JsonProperty("-InstanceId")]
    //    [JsonConverter(typeof(PurpleParseStringConverter))]
    //    public long InstanceId { get; set; }

    //    [JsonProperty("-MessageNumber")]
    //    [JsonConverter(typeof(PurpleParseStringConverter))]
    //    public long MessageNumber { get; set; }

    //    [JsonProperty("-SOAP-ENV:mustUnderstand")]
    //    [JsonConverter(typeof(FluffyParseStringConverter))]
    //    public bool SoapEnvMustUnderstand { get; set; }
    //}

    //public class Wsa
    //{
    //    [JsonProperty("-SOAP-ENV:mustUnderstand")]
    //    [JsonConverter(typeof(FluffyParseStringConverter))]
    //    public bool SoapEnvMustUnderstand { get; set; }

    //    [JsonProperty("#text")] public Uri Text { get; set; }
    //}

    //public partial class CameraDiscoveryResponse
    //{
    //    public static CameraDiscoveryResponse FromJson(string json)
    //    {
    //        return JsonConvert.DeserializeObject<CameraDiscoveryResponse>(json, Converter.Settings);
    //    }
    //}

    //public static class Serialize
    //{
    //    public static string ToJson(this CameraDiscoveryResponse self)
    //    {
    //        return JsonConvert.SerializeObject(self, Converter.Settings);
    //    }
    //}

    //internal static class Converter
    //{
    //    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    //    {
    //        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
    //        DateParseHandling = DateParseHandling.None,
    //        Converters =
    //        {
    //            new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
    //        },
    //    };
    //}

    //internal class PurpleParseStringConverter : JsonConverter
    //{
    //    public static readonly PurpleParseStringConverter Singleton = new PurpleParseStringConverter();

    //    public override bool CanConvert(Type t)
    //    {
    //        return t == typeof(long) || t == typeof(long?);
    //    }

    //    public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
    //    {
    //        if (reader.TokenType == JsonToken.Null)
    //        {
    //            return null;
    //        }

    //        var value = serializer.Deserialize<string>(reader);
    //        long l;
    //        if (long.TryParse(value, out l))
    //        {
    //            return l;
    //        }

    //        throw new Exception("Cannot unmarshal type long");
    //    }

    //    public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
    //    {
    //        if (untypedValue == null)
    //        {
    //            serializer.Serialize(writer, null);
    //            return;
    //        }

    //        var value = (long) untypedValue;
    //        serializer.Serialize(writer, value.ToString());
    //    }
    //}

    //internal class FluffyParseStringConverter : JsonConverter
    //{
    //    public static readonly FluffyParseStringConverter Singleton = new FluffyParseStringConverter();

    //    public override bool CanConvert(Type t)
    //    {
    //        return t == typeof(bool) || t == typeof(bool?);
    //    }

    //    public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
    //    {
    //        if (reader.TokenType == JsonToken.Null)
    //        {
    //            return null;
    //        }

    //        var value = serializer.Deserialize<string>(reader);
    //        bool b;
    //        if (bool.TryParse(value, out b))
    //        {
    //            return b;
    //        }

    //        throw new Exception("Cannot unmarshal type bool");
    //    }

    //    public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
    //    {
    //        if (untypedValue == null)
    //        {
    //            serializer.Serialize(writer, null);
    //            return;
    //        }

    //        var value = (bool) untypedValue;
    //        var boolString = value ? "true" : "false";
    //        serializer.Serialize(writer, boolString);
    //    }
    //}

    public static class CameraDiscovery
    {
        public static string IpString = "192.168.1.255";
        public static double TimeoutInSeconds { get; set; } = 10;

        public static async Task<List<string>> DiscoverRangeOfCamerasAsync(string address = "",
            ushort discoverPort = 3702)
        {
            var ipParsed = address.Split('.');
            var addresses = new List<string>();
            if (ipParsed.Length == 4)
            {
                for (var i = 1; i < 256; i++)
                {
                    addresses.Add($"{ipParsed[0]}.{ipParsed[1]}.{ipParsed[2]}.{i}");
                }
            }

            var result = new List<string>();
            await Task.WhenAll(addresses.Select(async ip =>
            {
                var cameras = await DiscoverCamera(ip, discoverPort);
                result.AddRange(cameras);
            }));

            return result;
        }

        public static async Task<List<string>> DiscoverCamera(string address, ushort port = 3702)
        {
            var result = new List<string>();

            using (var client = new UdpClient())
            {
                if (!string.IsNullOrEmpty(IpString))
                {
                    var ipEndpoint = new IPEndPoint(IPAddress.Parse(address), port);
                    // client.EnableBroadcast = false;
                    try
                    {
                        var soapMessage = GetBytes(CreateSoapRequest());
                        var timeout = DateTime.Now.AddSeconds(TimeoutInSeconds);
                        await client.SendAsync(soapMessage, soapMessage.Length, ipEndpoint);

                        while (timeout > DateTime.Now)
                        {
                            if (client.Available > 0)
                            {
                                var receiveResult = await client.ReceiveAsync();
                                var text = GetText(receiveResult.Buffer);
                                result.Add(text);
                            }
                            else
                            {
                                await Task.Delay(45);
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception.Message);
                    }
                }
            }

            return result;
        }

        //public static async Task<List<string>> GetSoapResponsesFromCamerasAsync()
        //{
        //    var result = new List<string>();

        //    using (var client = new UdpClient())
        //    {
        //        if (!string.IsNullOrEmpty(IpString))
        //        {
        //            var ipEndpoint = new IPEndPoint(IPAddress.Parse(IpString), 3702);
        //            client.EnableBroadcast = true;
        //            try
        //            {
        //                var soapMessage = GetBytes(CreateSoapRequest());
        //                var timeout = DateTime.Now.AddSeconds(TimeoutInSeconds);
        //                await client.SendAsync(soapMessage, soapMessage.Length, ipEndpoint);

        //                while (timeout > DateTime.Now)
        //                {
        //                    if (client.Available > 0)
        //                    {
        //                        var receiveResult = await client.ReceiveAsync();
        //                        var text = GetText(receiveResult.Buffer);
        //                        result.Add(text);
        //                    }
        //                    else
        //                    {
        //                        await Task.Delay(10);
        //                    }
        //                }
        //            }
        //            catch (Exception exception)
        //            {
        //                Console.WriteLine(exception.Message);
        //            }
        //        }
        //    }

        //    return result;
        //}

        private static string CreateSoapRequest()
        {
            var messageId = Guid.NewGuid();
            const string soap = @"
            <?xml version=""1.0"" encoding=""UTF-8""?>
            <e:Envelope xmlns:e=""http://www.w3.org/2003/05/soap-envelope""
            xmlns:w=""http://schemas.xmlsoap.org/ws/2004/08/addressing""
            xmlns:d=""http://schemas.xmlsoap.org/ws/2005/04/discovery""
            xmlns:dn=""http://www.onvif.org/ver10/device/wsdl"">
            <e:Header>
            <w:MessageID>uuid:{0}</w:MessageID>
            <w:To e:mustUnderstand=""true"">urn:schemas-xmlsoap-org:ws:2005:04:discovery</w:To>
            <w:Action a:mustUnderstand=""true"">http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe</w:Action>
            </e:Header>
            <e:Body>
            <d:Probe>
            <d:Types>dn:Device</d:Types>
            </d:Probe>
            </e:Body>
            </e:Envelope>
            ";

            var result = string.Format(soap, messageId);
            return result;
        }

        private static byte[] GetBytes(string text)
        {
            return Encoding.ASCII.GetBytes(text);
        }

        private static string GetText(byte[] bytes)
        {
            return Encoding.ASCII.GetString(bytes, 0, bytes.Length);
        }

        public static string GetAddress(string soapMessage)
        {
            var xmlNamespaceManager = new XmlNamespaceManager(new NameTable());
            xmlNamespaceManager.AddNamespace("g", "http://schemas.xmlsoap.org/ws/2005/04/discovery");

            var element = XElement.Parse(soapMessage).XPathSelectElement("//g:XAddrs[1]", xmlNamespaceManager);
            return element?.Value ?? string.Empty;
        }

        public static string GetScopes(string soapMessage)
        {
            var xmlNamespaceManager = new XmlNamespaceManager(new NameTable());
            xmlNamespaceManager.AddNamespace("g", "http://schemas.xmlsoap.org/ws/2005/04/discovery");

            var element = XElement.Parse(soapMessage).XPathSelectElement("//g:Scopes[1]", xmlNamespaceManager);
            return element?.Value ?? string.Empty;
        }
    }
}