﻿using System;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Common.Interfaces.Communication;
using Edge360.Platform.VMS.Communication.Services;
using Edge360.Vms.Shared.MessageModel.Enums;
using log4net;
using Microsoft.AspNetCore.SignalR.Client;
using Polly;

namespace Edge360.Platform.VMS.Communication.SignalR
{
    public class HubManager : IHub
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private PolicyResult _retryConnection;

        public CancellationTokenSource HubTokenSource = new CancellationTokenSource();

        public ServiceManager ServiceManager;

        public bool ClosingConnection { get; set; }

        private HubConnection SignalRHub { get; set; }

        public void Dispose()
        {
            ClosingConnection = true;

            HubTokenSource?.Cancel();

            SignalRHub?.DisposeAsync();
        }

        public Task ConnectAsync(string hubUrl)
        {
            _log?.Info($"Connecting to SignalR at {hubUrl}");

            StartEventHub(hubUrl);

            return Task.CompletedTask;
        }

        public void DisconnectAsync()
        {
            ClosingConnection = true;

            SignalRHub?.StopAsync(HubTokenSource.Token);

            HubTokenSource?.Cancel();
        }

        public void SubscribeAsync(string eventName, Action action)
        {
            SignalRHub.On(eventName, action);
        }

        private void StartEventHub(string hubUrl)
        {
            //In WPF clients, you might have to increase the maximum number of concurrent connections from its default value of 2. The recommended value is 10.
            // https://docs.microsoft.com/en-us/aspnet/signalr/overview/guide-to-the-api/hubs-api-guide-net-client#wpfsl
            ServicePointManager.DefaultConnectionLimit = 10;
            //$"https://{Settings.Login.ServerAddress}:{Settings.Login.ServerPort}/eventhub"
            SignalRHub = new HubConnectionBuilder().WithUrl(hubUrl
                , options =>
                {
                    options.CloseTimeout = TimeSpan.FromSeconds(3);
                    options.UseDefaultCredentials = true; //false;

                    options.AccessTokenProvider = () => Task.FromResult(ServiceManager?.Client.BearerToken);
                    //options.Credentials = new NetworkCredential(Settings.Login.Credentials.UserName,
                    //    Settings.Login.Credentials.Password);
                    //options.AccessTokenProvider = () => Task.FromResult(Settings.Login.BearerToken);
                }).Build();
            SignalRHub.Closed += SignalRHubOnClosed;
            SignalRHub.Reconnecting += SignalRHubOnReconnecting;
            SignalRHub.Reconnected += SignalRHubOnReconnected;
            SubscribeEvents();
            ConnectWithRetry();
        }

        private async void ConnectWithRetry()
        {
            HubTokenSource?.Cancel();
            HubTokenSource = new CancellationTokenSource();

            var retryPolicy = Policy
                .Handle<Exception>()
                .WaitAndRetryForeverAsync(i => TimeSpan.FromSeconds(5)
                    , (exception, timeSpan) =>
                    {
                        //_log?.Info($"Could not connect to SignalR {exception.GetType()} {exception.Message}");
                    });

            var policyContext = new Context("RetryContext") {{"CancellationTokenSource", HubTokenSource}};

            _retryConnection = await retryPolicy.ExecuteAndCaptureAsync(async (ctx, ct) =>
            {
                if (SignalRHub != null && !ct.IsCancellationRequested)
                {
                    await SignalRHub.StartAsync(ct);
                }
            }, policyContext, HubTokenSource.Token);
        }

        public static event Action<ESignalRClientEvent, Guid, string> SignalRClientEvent;

        private void SubscribeEvents()
        {
            foreach (var clientEvent in Enumeration.GetAll<ESignalRClientEvent>())
            {
                SignalRHub.On<string>(clientEvent.Name,
                    data =>
                    {
                        _log?.Info($"{clientEvent.Name} Received");
                        SignalRClientEvent?.Invoke(clientEvent, ServiceManager.ZoneId, data);
                    });
            }
        }

        private Task SignalRHubOnClosed(Exception arg)
        {
            if (!ClosingConnection)
            {
                ConnectWithRetry();
            }

            return null;
        }

        private Task SignalRHubOnReconnected(string arg)
        {
            _log?.Info($"SignalR Reconnected {arg}");
            return null;
        }

        private Task SignalRHubOnReconnecting(Exception arg)
        {
            _log?.Info($"SignalR Reconnecting {arg}");
            return null;
        }
    }
}