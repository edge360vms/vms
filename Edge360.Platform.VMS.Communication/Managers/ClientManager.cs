﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.Communication.Enums;
using Edge360.Platform.VMS.Communication.Polly;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Servers;
using ServiceStack;
using WispFramework.EventArguments;
using WispFramework.Extensions.Common;

namespace Edge360.Platform.VMS.Communication.Managers
{
    public class ClientManager : IDisposable
    {
        public const int MAX_ACCEPTABLE_FAILURES = 2;
        private readonly CustomServiceClientList _failedClients = new CustomServiceClientList();

        private readonly CancellationTokenSource _updateCancellationToken = new CancellationTokenSource();

        public readonly CustomServiceClientList AvailableClients = new CustomServiceClientList();

        public static bool UseRandomLoadBalancing => SettingsManager.Settings?.Login?.RandomLoadBalance ?? true;
        public CustomServiceClient RootClient { get; set; }
        public string Service { get; set; }

        public EClientType Type { get; set; }

        /// <summary>
        ///     A ClientManager for managing clients for a specific service
        /// </summary>
        /// <param name="type"></param>
        /// <param name="service"></param>
        /// <param name="rootClient"></param>
        public ClientManager(EClientType type, string service, CustomServiceClient rootClient)
        {
            Type = type;
            Service = service;
            RootClient = rootClient;

            //RefreshTokens();

            Task.Factory.StartNew(Update, _updateCancellationToken.Token, TaskCreationOptions.LongRunning,
                TaskScheduler.Default);
        }

        public void Dispose()
        {
            _updateCancellationToken?.Cancel(true);
            AvailableClients.Dispose();
            _failedClients.Dispose();
        }

        private async Task Update()
        {
            try
            {
                await UpdateInternal();
            }
            catch (OperationCanceledException e)
            {
                //Console.WriteLine(e);
            }
            catch (Exception e)
            {
            }
        }

        /// <summary>
        ///     Every 5 seconds, remove a failure from all clients
        /// </summary>
        /// <returns></returns>
        private async Task UpdateInternal()
        {
            var union = _failedClients.Clients.Union(AvailableClients.Clients);
            ThrowIfCancellationRequested();

            // decrement our client failures every 5 seconds
            foreach (var client in union)
            {
                DecrementFailure(client);
                ThrowIfCancellationRequested();
            }

            await Task.Delay(TimeSpan.FromSeconds(5));
            ThrowIfCancellationRequested();
        }

        private static void DecrementFailure(CustomServiceClient client)
        {
            if (client.Failures.Value > 0)
            {
                client.Failures.Value--;
            }
        }

        /// <summary>
        ///     Throw cancelled exception if cancellation requested
        /// </summary>
        private void ThrowIfCancellationRequested()
        {
            _updateCancellationToken.Token.ThrowIfCancellationRequested();
        }

        /// <summary>
        ///     Creates a new CustomServiceClient for this service pointing to the baseUri
        /// </summary>
        /// <param name="server"></param>
        public void AddClient(IServer server)
        {
            if (AvailableClients.Any(server.HostName))
            {
                return;
            }

            var customServiceClient = new CustomServiceClient(
                $"https://{server.HostName}:{server.PortApi}/{Service}")
            {
                ServerId = server.ServerId,
                BearerToken = RootClient.BearerToken,
               // RefreshToken = RootClient.RefreshToken,
                SessionId = RootClient.SessionId,
                ClientManager = this,
                Timeout = TimeSpan.FromSeconds(SettingsManager.Settings?.Login?.RequestTimeoutDuration ??
                                               3) //TimeSpan.FromMilliseconds(5000)
            };
            AvailableClients.Add(customServiceClient);
            InitializeClientEvents(customServiceClient);
        }

        private void InitializeClientEvents(CustomServiceClient customServiceClient)
        {
            void OnFailuresOnValueChanged(object sender, ValueChangedEventArgs<int> args)
            {
                UpdateServiceClientAvailability(args.NewValue, customServiceClient);
            }

            void OnCustomServiceClientOnDisposing()
            {
                customServiceClient.Failures.ValueChanged -= OnFailuresOnValueChanged;
                customServiceClient.Disposing -= OnCustomServiceClientOnDisposing;
            }

            customServiceClient.Failures.ValueChanged += OnFailuresOnValueChanged;
            customServiceClient.Disposing += OnCustomServiceClientOnDisposing;
            customServiceClient.OnAuthenticationRequired += async() =>
            {
                await RefreshTokens();
            };
        }


        /// <summary>
        ///     Move client between buckets based on current availability
        /// </summary>
        /// <param name="failureCount"></param>
        /// <param name="customServiceClient"></param>
        private void UpdateServiceClientAvailability(int failureCount, CustomServiceClient customServiceClient)
        {
            // if we haven't already moved it, move it to its respective bucket
            // ignore cases where only 1 client is available
            if (failureCount > MAX_ACCEPTABLE_FAILURES && AvailableClients.Clients.Count() > 1)
            {
                MoveToFailed(customServiceClient);
            }
            else if (failureCount <= 0)
            {
                MoveToAvailable(customServiceClient);
            }
        }

        private void MoveToFailed(CustomServiceClient customServiceClient)
        {
            if (AvailableClients.Contains(customServiceClient))
            {
                AvailableClients.Transfer(customServiceClient, _failedClients);
            }
        }

        private void MoveToAvailable(CustomServiceClient customServiceClient)
        {
            if (_failedClients.Contains(customServiceClient))
            {
                _failedClients.Transfer(customServiceClient, AvailableClients);
            }
        }


        public async Task RefreshTokens()
        {
            await Task.Run(async () =>
            {
                try
                {
                    Trace.WriteLine("RefreshToken");
                    if (!string.IsNullOrEmpty(RootClient?.RefreshToken))
                    {
                        var token = RootClient?.Get(new GetAccessToken {RefreshToken = RootClient.RefreshToken})
                            ?.AccessToken;

                        if (!string.IsNullOrEmpty(token))
                        {
                            foreach (var cl in AvailableClients.Clients)
                            {
                                cl.BearerToken = token;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                }
            });     
        }

        public CustomServiceClient GetClient(Guid? serverId = null)
        {
            if (serverId != null)
            {
                return AvailableClients.Clients.FirstOrDefault(o => o.ServerId == serverId);
            }

            return UseRandomLoadBalancing
                ? AvailableClients.Clients.ToList().RandomElement()
                : AvailableClients.Clients.FirstOrDefault();
        }
    }
}