﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Edge360.Platform.VMS.Communication.Polly;

namespace Edge360.Platform.VMS.Communication.Managers
{
    /// <summary>
    ///     Thread-safe container for CustomServiceClients
    /// </summary>
    public class CustomServiceClientList : IDisposable
    {
        public readonly ConcurrentDictionary<string, CustomServiceClient> InternalCache =
            new ConcurrentDictionary<string, CustomServiceClient>();

        public IEnumerable<CustomServiceClient> Clients => InternalCache.Values;

        public void Dispose()
        {
            foreach (var client in InternalCache.Values)
            {
                client.Dispose();
            }
        }

        public bool Transfer(CustomServiceClient client, CustomServiceClientList targetList)
        {
            return Remove(client) && targetList.Add(client);
        }

        public bool Any(CustomServiceClient client)
        {
            return InternalCache.TryGetValue(client.BaseUri, out var result);
        }

        public bool Contains(CustomServiceClient client)
        {
            return Any(client);
        }

        public bool Contains(string baseUri)
        {
            return Any(baseUri);
        }

        public bool Any(string baseUri)
        {
            return InternalCache.TryGetValue(baseUri, out var result);
        }

        public bool Add(CustomServiceClient client)
        {
            if (Any(client))
            {
                return false;
            }

            InternalCache[client.BaseUri] = client;
            return true;
        }

        public bool Remove(CustomServiceClient client)
        {
            return InternalCache.TryRemove(client.BaseUri, out _);
        }
    }
}