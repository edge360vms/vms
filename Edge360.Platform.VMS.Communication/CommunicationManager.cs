using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Common.Interfaces.Authority;
using Edge360.Platform.VMS.Common.Settings;
using Edge360.Platform.VMS.Communication.Polly;
using Edge360.Platform.VMS.Communication.Services;
using Edge360.Raven.Vms.AuthorityService.ServiceModel.Api.Users;
using Edge360.Vms.AuthorityService.SharedModel.Enums;
using log4net;
using Polly;
using ServiceStack;
using ServiceStack.Text;
using WispFramework.Extensions.Common;

namespace Edge360.Platform.VMS.Communication
{
    public class CommunicationManager
    {
        public static ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static ConcurrentDictionary<Guid, ServiceManager> ServiceManagers =
            new ConcurrentDictionary<Guid, ServiceManager>();

        public static CancellationTokenSource LoginTokenSource;

        public static Guid ConnectedServerId = Guid.Parse("45859ee8-fbac-4bb3-a48e-55189a4d1db8");

        //    public static string HARD_CODED_ROOT_NODE_ID = "8d0abccb-49e7-43ae-826e-ef55ce4ce498";
        public static Guid ConnectedZoneId = Guid.Parse("e388fe89-ff0f-4e43-9c95-e8c9e79e4de8");
        public static UserDetails CurrentUser { get; set; }
        public static ERoles EffectivePermissions { get; set; }
        public static bool EnableExceptionThrowing { get; set; }


        public static void StartCommunicationManager()
        {
            JsConfig.AlwaysUseUtc = true;
            ServicePointManager.Expect100Continue = false;
            // Disables SSL Error Message and Certification Validation requirements.
            ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;
        }

        public static async Task<bool> InitializeServiceManager(ServiceManager svcMgr,
            Action<Exception> outException = default)
        {
            return await Task.Run(async () =>
            {
                if (svcMgr != null)
                {
                    var infoResponse = await ServiceManager.SetConnectedZoneAndServer(svcMgr, outException);
                    if (infoResponse)
                    {
                        InitializeClients(svcMgr);
                        return true;
                    }
                }

                return false;
            });
        }

        internal static void InitializeClients(ServiceManager svcMgr)
        {
            var client = svcMgr.Client;
            if (client != null && svcMgr.ZoneId != Guid.Empty)
            {
                if (ServiceManagers.TryGetValue(svcMgr.ZoneId, out var oldServices))
                {
                    ServiceManagers[svcMgr.ZoneId] = svcMgr;
                    Trace.WriteLine($"Replacing Service Manager {oldServices.ZoneId}...");
                    oldServices.Dispose();
                }
                else
                {
                    ServiceManagers.TryAdd(svcMgr.ZoneId, svcMgr);
                }
            }
        }

        /// <summary>
        ///     Connects and Authenticates the Client to the VMS Server
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public static async Task<CustomServiceClient> ConnectAsync(CustomServiceClient client = null,
            UserLogin login = null, Guid? zoneId = null, int retryCount = 15)
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (login != null)
                    {
                        Trace.WriteLine(login);
                        var cl =
                            new CustomServiceClient(
                                $"https://{login.ResolveDomain()}/authority")
                            {
                                Timeout = TimeSpan.FromSeconds(SettingsManager.Settings?.Login?.RequestTimeoutDuration ??
                                                               3) //TimeSpan.FromMilliseconds(3000)
                            };
                        LoginTokenSource.Token.Register(() =>
                        {
                            Task.Run(() =>
                            {
                                _log?.Info("Disposing Login Client!");
                                cl?.Dispose();
                            });
                        });

                        var waitAndRetryAsync = Policy.Handle<Exception>(e =>
                            {
                                if (e is WebServiceException serviceException)
                                {
                                    if (serviceException.StatusCode == (int) HttpStatusCode.InternalServerError)
                                    {
                                        Console.WriteLine($"Got Internal Error back... {e.Message}");
                                        return false;
                                    }

                                    client?.HandleWebException(client, serviceException);
                                }

                                Console.WriteLine(e.Message);
                                return !e.IsUnauthorized();
                            })
                            .WaitAndRetryAsync(retryCount, count =>
                            {
                                if (!LoginTokenSource.IsCancellationRequested)
                                {
                                    ConnectionError?.Invoke($"Retrying connection... {count}");
                                    if (count == retryCount)
                                    {
                                        ConnectionError?.Invoke("Failed to connect.");
                                        LoginTokenSource.Cancel();
                                    }
                                }

                                _log?.Info($"Retrying connection... {count}");
                                return TimeSpan.FromSeconds(1);
                            });
                        var policyContext = new Context("RetryContext") {{"CancellationTokenSource", LoginTokenSource}};
                        var resp = await waitAndRetryAsync
                            .ExecuteAndCaptureAsync(
                                async (ctx, t) =>
                                {
                                    if (login.IsBearerLogin && login.RefreshToken.IsNotEmpty())
                                    {
                                        cl.RefreshToken = login.RefreshToken;
                                        cl.BearerToken = login.BearerToken;
                                        Trace.WriteLine($"{cl.RefreshToken} {cl.BearerToken}");
                                        //throw an error if we can't connect

                                        var token = cl?.Get(new GetAccessToken {RefreshToken = cl.RefreshToken})
                                            ?.AccessToken;
                                        if (token != null && token.IsNotEmpty())
                                        {
                                            client.RefreshToken = cl.RefreshToken;
                                            client.BearerToken = token;
                                        }
                                    }
                                    else
                                    {
                                        var auth = await cl.PostAsync(new Authenticate
                                        {
                                            provider = "credentials",
                                            UserName = login.UserName,
                                            Password = login.Password,

                                            // RememberMe = true
                                        });
                                        //if (auth == null)
                                        //{
                                        //    client = null;
                                        //}

                                        if (auth != null)
                                        {
                                            client.RefreshToken = auth.RefreshToken;
                                            client.BearerToken = auth.BearerToken;
                                            client.SessionId = auth.SessionId;
                                        }
                                    }
                                }, policyContext, LoginTokenSource.Token);
                    }

                    if (LoginTokenSource.IsCancellationRequested)
                    {
                        return null;
                    }

                    return client;
                });
            }
            catch (WebServiceException exception)
            {
                client?.HandleWebException(client, exception);
            }

            catch (Exception e)
            {
                _log?.Info(e);
            }

            return null;
        }

        public static event Action<string> ConnectionError;

        public static ServiceManager GetManagerByZoneId(Guid zoneId = default)
        {
            try
            {
                if (zoneId == default)
                {
                    zoneId = ConnectedZoneId;
                }

                var svc = ServiceManagers.ContainsKey(zoneId) ? ServiceManagers[zoneId] : default;
                return svc;
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public static ServiceManager GetManagerByServerId(Guid serverId = default)
        {
            try
            {
                if (serverId == default)
                {
                    serverId = ConnectedServerId;
                }

                var svc = ServiceManagers.Values.FirstOrDefault(s => s.ServiceClients.Values.Any(c =>
                    c.AvailableClients.InternalCache.Any(j => j.Value.ServerId == serverId)));
                return svc;
            }
            catch (Exception e)
            {
                return default;
            }
        }
    }
}