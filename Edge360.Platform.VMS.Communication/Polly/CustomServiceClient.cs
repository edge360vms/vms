﻿using Edge360.Platform.VMS.Common.Enums.Communication;
using Edge360.Platform.VMS.Communication.Managers;
using Edge360.Platform.VMS.Communication.Services;
using log4net;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Reflection;
using WispFramework.Patterns.Observables;

namespace Edge360.Platform.VMS.Communication.Polly
{
    /// <summary>
    ///     Custom Implementation of ServiceStack JsonHttpClient
    /// </summary>
    public class CustomServiceClient : JsonServiceClient, IDisposable
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public List<string> LoadEndpoints = new List<string>();

        public ClientManager ClientManager { get; set; }

        public Sub<int> Failures { get; set; } = new Sub<int>(0);
        public Guid ServerId { get; set; }

        public EServiceType ServiceType { get; private set; }


        public CustomServiceClient(ClientManager clientManager, string url) : base(url)
        {
            OnWebException += OnOnWebException;
            ClientManager = clientManager;
        }

        public CustomServiceClient(string url) : base(url)
        {
            OnWebException += OnOnWebException;
            //SetDefaultClient(url);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public event Action<object, WebServiceException> OnWebException;

        internal void HandleWebException(object o, WebServiceException e)
        {
            ServiceManager.HandleWebException(this, o, e);
            OnWebException?.Invoke(o, e);
        }

        public event Action Disposing;

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Disposing?.Invoke();
            }
        }
        //public void SetServiceType(EServiceType type)
        //{
        //    if (type != ServiceType)
        //    {
        //        ServiceType = type;
        //    }
        //}

        //public void GetServiceType()
        //{
        //    switch (ServiceType)
        //    {
        //        case EServiceType.None:
        //            AsyncOneWayBaseUri = $"https://{Settings.Login.Credentials.ResolveDomain()}/json/oneway/";
        //            BaseUri = $"https://{Settings.Login.Credentials.ResolveDomain()}/";
        //            SyncReplyBaseUri = $"https://{Settings.Login.Credentials.ResolveDomain()}/json/reply/";
        //            break;
        //        case EServiceType.Authority:
        //            BaseUri = $"https://{Settings.Login.Credentials.ResolveDomain()}/authority";
        //            SyncReplyBaseUri = $"https://{Settings.Login.Credentials.ResolveDomain()}/authority/json/reply/";
        //            AsyncOneWayBaseUri = $"https://{Settings.Login.Credentials.ResolveDomain()}/authority";
        //            break;
        //        case EServiceType.Cameras:
        //            BaseUri = $"https://{Settings.Login.Credentials.ResolveDomain()}/cameras";
        //            SyncReplyBaseUri = $"https://{Settings.Login.Credentials.ResolveDomain()}/cameras/json/reply/";
        //            AsyncOneWayBaseUri = $"https://{Settings.Login.Credentials.ResolveDomain()}/cameras/json/oneway/";
        //            break;

        //        case EServiceType.Recordings:
        //            BaseUri = $"https://{Settings.Login.Credentials.ResolveDomain()}/recording";
        //            SyncReplyBaseUri = $"https://{Settings.Login.Credentials.ResolveDomain()}/recording/json/reply/";
        //            AsyncOneWayBaseUri = $"https://{Settings.Login.Credentials.ResolveDomain()}/recording/json/oneway/";
        //            break;
        //    }
        //}

        ///// <summary>
        /////     Uses similar HttpClient to the Default (GetHttpClient)
        ///// </summary>
        ///// <param name="url"></param>
        //private void SetDefaultClient(string url)
        //{
        //    var handler = HttpMessageHandler ?? new HttpClientHandler
        //    {
        //        UseCookies = true,
        //        CookieContainer = CookieContainer,
        //        UseDefaultCredentials = Credentials == null,
        //        Credentials = Credentials,
        //        AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
        //    };
        //    var client = new HttpClient(handler) {BaseAddress = new Uri(url)};

        //    if (client != null)
        //    {
        //        HttpClient = client;
        //    }
        //}

        //private void SetPollyClientTest2(string url)
        //{
        //    var collectionWithPolly = new HostBuilder().ConfigureServices((hostContext, services) =>
        //    {
        //        services.AddHttpClient("Local");
        //    });

        //    var host = collectionWithPolly.Build();

        //    var httpClientFactory =
        //        host?.Services.GetService<IHttpClientFactory>();
        //    if (httpClientFactory != null)
        //    {
        //        HttpClient = httpClientFactory.CreateClient("Local");
        //    }
        //}

        //private void SetPollyClientTest(string url)
        //{
        //    var collectionWithPolly = new ServiceCollection().AddHttpClient("MyClient",
        //            c =>
        //            {
        //                c.BaseAddress = new Uri(url);
        //                c.DefaultRequestHeaders.Add("Accept", "application/json");
        //                c.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", BearerToken);
        //            })
        //        .AddTransientHttpErrorPolicy(b => b.WaitAndRetryForeverAsync(i =>
        //        {
        //            _log?.Info($"Retrying... {i}");
        //            return TimeSpan.FromSeconds(2);
        //        }));

        //    var serviceProvider = collectionWithPolly?.Services?.AddHttpClient()?.BuildServiceProvider();
        //    var httpClientFactory =
        //        serviceProvider?.GetService<IHttpClientFactory>();
        //    var client = httpClientFactory?.CreateClient("Local");
        //    if (client != null)
        //    {
        //        HttpClient = client;
        //    }
        //}

        private void OnOnWebException(object o, WebServiceException webException)
        {
            _log?.Info($"Web Exception Occurred with {(o is CustomServiceClient cl ? $"LoginClient {cl.ServerId} {cl.RefreshToken} {cl.BearerToken}" : o)}\n{webException}");
        }
    }
}