﻿namespace Edge360.Platform.VMS.Communication.Polly
{
    public static class PollyTests
    {
        //private static async void PollyCreateFactoryClientRequestTest()
        //{
        //    var serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
        //    var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();
        //    var client = httpClientFactory.CreateClient();
        //    var response = await client.SendAsync(new HttpRequestMessage(HttpMethod.Get, "http://www.google.com"));
        //    var content = await response.Content.ReadAsStringAsync();
        //    _log?.Info(content);
        //}

        /// <summary>
        ///     Demonstrates a retry policy when Server is Unavailable
        /// </summary>
        internal static async void PollyRetryRequestTest()
        {
            //try
            //{
            //    //Create a new ServiceCollection as a base to Build a Default ServiceProvider with a Polly HttpClient
            //    var pollyCollection = new ServiceCollection().AddHttpClient("Local",
            //            c =>
            //            {
            //                c.BaseAddress =
            //                    new Uri($"http://{Settings.Login.ServerAddress}:{Settings.Login.ServerPort}/");
            //                c.DefaultRequestHeaders.Add("Accept", "application/json");
            //            })
            //        //Uses Polly to create a Retry Forever Policy
            //        .AddTransientHttpErrorPolicy(b => b.WaitAndRetryForeverAsync(i =>
            //        {
            //            _log?.Info($"Retrying... {i}");
            //            return TimeSpan.FromSeconds(1);
            //        })).Services.BuildServiceProvider();
            //    ;

            //    var httpClientFactory = pollyCollection.GetService<IHttpClientFactory>();

            //    //Reference the Polly HttpClient from the HttpClientFactory
            //    var client = httpClientFactory.CreateClient("Local");
            //    var response =
            //        await client.SendAsync(new HttpRequestMessage(HttpMethod.Get,
            //            $"https://{Settings.Login.ServerAddress}:{Settings.Login.ServerPort}/api/values"));
            //    var content = await response.Content.ReadAsStringAsync();
            //    _log?.Info($"PollyRetryRequestTest receieved {content}");
            //}
            //catch (Exception e)
            //{
            //    _log?.Info(e);
            //}
        }
    }
}