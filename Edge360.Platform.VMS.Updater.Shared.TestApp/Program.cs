﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Edge360.Platform.VMS.Updater.Shared.TestApp
{
    internal class Program
    {
        private static VmsClientUpdateSystem updateSystem;

        private static Dictionary<UpdateLocation, UpdateInformation[]> updateList =
            new Dictionary<UpdateLocation, UpdateInformation[]>();

        /// <summary>
        ///     Test Application For VMSClientUpdateSystem
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            var updateSourceFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestUpdates");
            if (!Directory.Exists(updateSourceFolder) || !File.Exists(Path.Combine(updateSourceFolder, "Director.Xml")))
            {
                Console.WriteLine("Unable To Locate Update Source Folder");
                Environment.Exit(-1);
            }

            Console.WriteLine("Loading Update System...\n");
            updateSystem = new VmsClientUpdateSystem(
                new UpdateLocation(updateSourceFolder), "*AppLifeTargetApp.exe");

            ListInstalled();

            var hasUpdate = updateSystem.HasUpdate();
            var showAllUpdates = false;
            if (!hasUpdate)
            {
                Console.WriteLine("No New Updates Are Available\n");
                if (ReadValue("List ALL Published Updates? (Y/N)", "N") == "Y")
                {
                    hasUpdate = true;
                    showAllUpdates = true;
                }
            }

            if (hasUpdate)
            {
                var updateAvailable = updateSystem.CheckUpdate();

                if (updateAvailable == null)
                {
                    Console.WriteLine("\n\nLatest Version Installed");
                }

                ShowUpdateList(updateAvailable?.Item2.Version, showAllUpdates);

                Console.WriteLine("\n\nPress Any Key To Exit");
                Console.Read();
                Environment.Exit(0);
            }

            Console.WriteLine("\nNo Updates Available");
            Console.WriteLine("\n\nPress Any Key To Exit");
            Console.ReadKey();
        }

        private static void ShowUpdateList(Version bestVersion, bool showAllUpdates)
        {
            updateList = updateSystem.ListAvailableUpdates(allUpdatesAvailable: showAllUpdates);
            Console.WriteLine();
            ListUpdates(updateList, bestVersion);

            if (updateSystem.UpdateActive)
            {
                Console.WriteLine("\n\n*** UPDATE IN PROGRESS ***");
            }
            else
            {
                var defaultValue = "Y";
                if (bestVersion == null)
                {
                    defaultValue = "N";
                }

                var input = ReadValue("\nInstall Update? (Y/N/A/#)", defaultValue);
                switch (input)
                {
                    case "A":
                        ShowUpdateList(bestVersion, true);
                        return;
                    case "N":
                        Console.WriteLine("\n\nDo Not Install Updates");
                        break;
                    case "Y":
                        if (bestVersion == null)
                        {
                            if (!ApplySelectedUpdate("1"))
                            {
                                Console.WriteLine($"Unable To Find Requested Update ({input})!");
                            }
                        }
                        else
                        {
                            Console.WriteLine("\n\nInstalling Updates...");
                            if (updateSystem.ApplyUpdate())
                            {
                                // Wait For Finish
                                Console.WriteLine("Updates Installed!\n");
                            }
                            else
                            {
                                Console.WriteLine("Problem Installing Updates!\n");
                            }
                        }

                        ListInstalled();
                        break;
                    default:
                        if (!ApplySelectedUpdate(input))
                        {
                            Console.WriteLine($"Unable To Find Requested Update ({input})!");
                        }
                        else
                        {
                            ListInstalled();
                        }

                        break;
                }
            }
        }

        private static bool ApplySelectedUpdate(string targetNum)
        {
            if (updateList == null || updateList.Count <= 0)
            {
                return false;
            }

            var found = false;
            if (int.TryParse(targetNum, out var index))
            {
                if (index > 0)
                {
                    var i = 1;
                    foreach (var update in updateList)
                    {
                        foreach (var updateInformation in update.Value)
                        {
                            if (index == i++)
                            {
                                found = true;
                                Console.WriteLine("Installing Updates...\n\n");
                                if (updateSystem.ApplyUpdate(update.Key, updateInformation.Version,
                                    forceInstall: true))
                                {
                                    // Wait For Finish
                                    Console.WriteLine("Updates Installed!\n\n");
                                }
                                else
                                {
                                    Console.WriteLine("Problem Installing Updates!\n");
                                }
                            }
                        }
                    }
                }
            }

            return found;
        }

        private static void ListInstalled()
        {
            var installedVersions = updateSystem.InstalledVersions();
            if (installedVersions == null || installedVersions.Count <= 0)
            {
                Console.WriteLine("Not Installed");
            }
            else
            {
                Console.WriteLine("Installed Versions\n------------------\n");
                foreach (var installed in installedVersions)
                {
                    string shortPath;
                    try
                    {
                        shortPath = Path.Combine(Directory.GetParent(installed.Value).Name,
                            Path.GetFileName(installed.Value));
                    }
                    catch (Exception)
                    {
                        shortPath = "";
                    }

                    Console.WriteLine(installed.Key + $"{installed.Key} - {shortPath}");
                }
            }

            Console.WriteLine();
        }

        private static Dictionary<int, UpdateInformation> ListUpdates(
            Dictionary<UpdateLocation, UpdateInformation[]> updates, Version bestUpdateVersion = null)
        {
            if (bestUpdateVersion == null)
            {
                bestUpdateVersion = new Version(0, 0, 0);
            }


            Console.WriteLine("Available Updates\n-----------------\n");
            if (updates == null || updates.Count <= 0)
            {
                Console.WriteLine("No Updates Available\n");
                return null;
            }

            var result = new Dictionary<int, UpdateInformation>();
            var index = 1;
            foreach (var update in updates)
            {
                Console.WriteLine(update.Key.Location);
                foreach (var updateInformation in update.Value)
                {
                    Console.WriteLine(
                        $"  [{index}{(bestUpdateVersion == updateInformation.Version ? "*" : "")}]  {updateInformation.Version} - {updateInformation.PostedDate}");
                    result.Add(index++, updateInformation);
                }
            }

            return result;
        }

        private static string ReadValue(string prompt, string defaultValue)
        {
            if (prompt != null)
            {
                Console.Write(prompt);
                if (defaultValue != null)
                {
                    Console.Write($" [{defaultValue}]");
                }

                Console.Write(" ");
            }

            var input = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(input))
            {
                input = defaultValue;
            }

            return input?.Trim().ToUpper();
        }
    }
}