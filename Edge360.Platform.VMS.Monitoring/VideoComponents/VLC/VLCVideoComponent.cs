﻿//using System;
//using System.ComponentModel;
//using System.Diagnostics;
//using System.Runtime.CompilerServices;
//using System.Windows;
//using System.Windows.Controls;
//using System.Windows.Media;
//using Edge360.Platform.VMS.Common.Enums.Video;
//using Edge360.Platform.VMS.Common.Interfaces.Camera;
//using Edge360.Platform.VMS.Common.Interfaces.Monitoring;
//using Edge360.Platform.VMS.Common.Interfaces.Video;
//using Edge360.Platform.VMS.Common.Properties;
//using Edge360.Platform.VMS.Common.Util;
//using LibVLCSharp.Shared;
//using MediaPlayer = LibVLCSharp.Shared.MediaPlayer;

//namespace Edge360.Platform.VMS.Monitoring.VideoComponents.VLC
//{
//    public class VLCVideoComponent : UserControl, IVideoComponent, IAirspaceContent, INotifyPropertyChanged
//    {
//        private readonly Grid _gridContent;
//        private readonly UIElement _owner;
//        private bool _isLivePlayback;
//        private bool _isPlaying;
//        public LibVLC _libvlc;
//        public VLCVideoView VideoView;

//        public VLCVideoComponent(Grid gridContent)
//        {
//            _gridContent = gridContent;
//            Init();


//            //if (_videoView != null)
//            //{
//            //    _videoView.MediaPlayer = new MediaPlayer(_libvlc);
//            //    ContentControl = _videoView;
//            //}

//            Loaded += OnLoaded;
//        }

//        public void Redraw()
//        {
//            VideoView?.VideoContent?.Redraw();
//        }

//        public event PropertyChangedEventHandler PropertyChanged;

//        public void Dispose()
//        {
//        }

//        public bool IsPlaying
//        {
//            get => _isPlaying;
//            set
//            {
//                if (_isPlaying != value)
//                {
//                    _isPlaying = value;
//                    OnPropertyChanged();
//                }
//            }
//        }

//        public double ZoomFactor { get; set; }

//        public void DigitalZoom(bool zoomIn, Point point)
//        {
//        }

//        public Size GetImageBounds()
//        {
//            return default;
//        }

//        public IPtzController PtzController { get; set; }

//        public bool IsLivePlayback
//        {
//            get => _isLivePlayback;
//            set
//            {
//                if (_isLivePlayback != value)
//                {
//                    _isLivePlayback = value;
//                    OnPropertyChanged();
//                }
//            }
//        }

//        public EPlaybackSpeed PlaybackSpeed { get; set; }
//        public EPlaybackState PlaybackState { get; set; }
//        public EPlayerState PlayerState { get; set; }
//        public event Action<EPlayerState> PlayerStateChanged;
//        public event Action<EPlaybackSpeed> PlaybackSpeedChanged;
//        public event Action<EPlaybackState> PlaybackStateChanged;
//        public event Action<ImageSource> FrameRendered;

//        public void Play()
//        {
//            VideoView.MediaPlayer?.Play();
//            IsLivePlayback = true;
//            IsPlaying = true;
//            PlayerState = EPlayerState.Playing;
//        }

//        public void Pause()
//        {
//            VideoView.MediaPlayer?.Pause();
//            IsPlaying = false;
//            PlayerState = EPlayerState.Paused;
//        }

//        public void Stop()
//        {
//            VideoView.MediaPlayer?.Stop();
//            IsPlaying = false;
//            IsLivePlayback = false;
//            PlayerState = EPlayerState.Stopped;
//        }

//        public void JumpToDate(DateTime? date, bool pause = false)
//        {
//            if (VideoView.MediaPlayer != null)
//            {
//                VideoView.MediaPlayer.Time = 0;
//            }
//        }

//        public void Open(string url)
//        {
//            if (VideoView.MediaPlayer != null)
//            {
//                var media = new Media(_libvlc, url, FromType.FromLocation);
//                //media.AddOption(new MediaConfiguration(){EnableHardwareDecoding = true, FileCaching = 60000, NetworkCaching = 10});
//                media.AddOption("--video-filter=\"magnify\"");
//                VideoView.MediaPlayer.Media = media;
//                PlayerState = EPlayerState.Opening;
//            }
//        }

//        public void NextEvent(ITimelineEvent type)
//        {
//        }

//        public void PreviousEvent(ITimelineEvent type)
//        {
//        }

//        public void Zoom(bool zoomIn, Point point)
//        {
//        }

//        private async void Init()
//        {
//            await this.WpfUiThreadAsync(async () =>
//            {
//                _libvlc = new LibVLC("--no-audio");
//                if (_gridContent.Parent is Button parent)
//                {
//                    // parent.Content = null;
//                    //parent.Content = null;
//                    if (parent.Parent is Grid grid)
//                    {
//                        grid.Children.Remove(parent);
//                        var mp = new MediaPlayer(_libvlc) {NetworkCaching = 10};

//                        VideoView = new VLCVideoView
//                        {
//                            MediaPlayer = mp,
//                            Content = parent
//                        };
//                        Content = VideoView;
//                    }
//                }
//            });
//        }

//        public void GetHwnd()
//        {
//            _log?.Info($"{VideoView.MediaPlayer.Hwnd}");
//        }

//        public async void Initialize()
//        {
//            //var hwnd = (HwndSource) PresentationSource.FromVisual(_owner);
//            //if (hwnd != null)
//            //{
//            //    _videoView.MediaPlayer.Hwnd = hwnd.Handle;
//            //}
//        }

//        [NotifyPropertyChangedInvocator]
//        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
//        {
//            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
//        }

//        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
//        {
//        }
//    }
//}

