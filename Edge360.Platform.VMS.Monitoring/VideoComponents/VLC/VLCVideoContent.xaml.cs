﻿//using System;
//using System.ComponentModel;
//using System.Diagnostics;
//using System.Windows;
//using System.Windows.Input;
//using Edge360.Platform.VMS.Common.Interfaces.Monitoring;
//using LibVLCSharp.Shared;

//namespace Edge360.Platform.VMS.Monitoring.VideoComponents.VLC
//{
//    public partial class VLCVideoContent : Window, IAirspaceContent
//    {
//        private readonly FrameworkElement _bckgnd;
//        private readonly Point _zeroPoint = new Point(0, 0);
//        private UIElement _content;
//        private Window _wndhost;

//        internal new UIElement Content
//        {
//            get => _content;
//            set
//            {
//                _content = value;
//                PART_Content.Children.Clear();
//                if (_content != null)
//                {
//                    PART_Content.Children.Add(_content);
//                }
//            }
//        }

//        public VLCVideoContent(FrameworkElement background)
//        {
//            InitializeComponent();

//            DataContext = background.DataContext;

//            _bckgnd = background;
//            _bckgnd.DataContextChanged += Background_DataContextChanged;
//            _bckgnd.Loaded += Background_Loaded;
//            _bckgnd.Unloaded += Background_Unloaded;
//        }

//        public void Redraw()
//        {
//            var locationFromScreen = _bckgnd.PointToScreen(_zeroPoint);
//            var source = PresentationSource.FromVisual(_wndhost);
//            var targetPoints = source.CompositionTarget.TransformFromDevice.Transform(locationFromScreen);
//            Left = targetPoints.X;
//            Top = targetPoints.Y;
//        }

//        public void Zoom(bool zoomIn, Point point)
//        {
//        }

//        protected override void OnKeyDown(KeyEventArgs e)
//        {
//            if (e.Key == Key.System && e.SystemKey == Key.F4)
//            {
//                _wndhost?.Focus();
//            }
//        }

//        private void Background_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
//        {
//            DataContext = e.NewValue;
//        }

//        private void Background_Unloaded(object sender, RoutedEventArgs e)
//        {
//            _bckgnd.SizeChanged -= Wndhost_SizeChanged;

//            if (_wndhost != null)
//            {
//                _wndhost.Closing -= Wndhost_Closing;
//                _wndhost.LocationChanged -= Wndhost_LocationChanged;
//            }

//            Hide();
//        }

//        private void Background_Loaded(object sender, RoutedEventArgs e)
//        {
//            _wndhost = GetWindow(_bckgnd);
//            Trace.Assert(_wndhost != null);
//            if (_wndhost == null)
//            {
//                return;
//            }

//            if (Owner != _wndhost)
//            {
//                Owner = _wndhost;
//            }

//            _wndhost.Closing += Wndhost_Closing;
//            _bckgnd.SizeChanged += Wndhost_SizeChanged;
//            _bckgnd.IsVisibleChanged += BckgndOnIsVisibleChanged;
//            _wndhost.LocationChanged += Wndhost_LocationChanged;

//            try
//            {
//                var locationFromScreen = _bckgnd.PointToScreen(_zeroPoint);
//                var source = PresentationSource.FromVisual(_wndhost);
//                var targetPoints = source.CompositionTarget.TransformFromDevice.Transform(locationFromScreen);
//                Left = targetPoints.X;
//                Top = targetPoints.Y;
//                var size = new Point(_bckgnd.ActualWidth, _bckgnd.ActualHeight);
//                Height = size.Y;
//                Width = size.X;
//                Show();
//                _wndhost.Focus();
//            }
//            catch
//            {
//                Hide();
//                throw new VLCException("Unable to create WPF Window in VideoView.");
//            }
//        }

//        private void BckgndOnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
//        {
//            var wasVisible = (bool) e.OldValue;
//            _content.Visibility = wasVisible ? Visibility.Hidden : Visibility.Visible;
//        }

//        private void Wndhost_LocationChanged(object sender, EventArgs e)
//        {
//            Redraw();
//        }

//        private void Wndhost_SizeChanged(object sender, SizeChangedEventArgs e)
//        {
//            var locationFromScreen = _bckgnd.PointToScreen(_zeroPoint);
//            var source = PresentationSource.FromVisual(_wndhost);
//            var targetPoints = source.CompositionTarget.TransformFromDevice.Transform(locationFromScreen);
//            Left = targetPoints.X;
//            Top = targetPoints.Y;
//            var size = new Point(_bckgnd.ActualWidth, _bckgnd.ActualHeight);
//            Height = size.Y;
//            Width = size.X;
//        }

//        private void Wndhost_Closing(object sender, CancelEventArgs e)
//        {
//            Close();
//        }
//    }
//}

