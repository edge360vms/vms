using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using Edge360.Platform.VMS.Common.Annotations;
using Edge360.Platform.VMS.Common.Settings;
using log4net;
using Mpv.NET.API;
using Mpv.NET.Player;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.Monitoring.VideoComponents.MPV
{
    /// <summary>
    ///     WPF VideoView with databinding for use with LibVLCSharp
    /// </summary>
    [TemplatePart(Name = PART_PlayerHost, Type = typeof(WindowsFormsHost))]
    [TemplatePart(Name = "PART_ContentPresenter", Type = typeof(ContentPresenter))]
    [TemplatePart(Name = "PART_MainGrid", Type = typeof(Grid))]
    //   [TemplatePart(Name = PART_PlayerView, Type = typeof(Panel))]
    public class MPVVideoView : ContentControl, IDisposable, INotifyPropertyChanged
    {
        ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string PART_PlayerHost = "PART_PlayerHost";

        /// <summary>
        ///     MediaPlayer WPF databinding property
        /// </summary>
        public static readonly DependencyProperty MediaPlayerProperty = DependencyProperty.Register(nameof(MediaPlayer),
            typeof(MpvPlayer),
            typeof(MPVVideoView),
            new PropertyMetadata(null, OnMediaPlayerChanged));


        public int VideoHeight
        {
            get => _videoHeight;
            set
            {
                if (value == _videoHeight) return;
                _videoHeight = value;
                OnPropertyChanged();
            }
        }

        public int VideoWidth
        {
            get => _videoWidth;
            set
            {
                if (value == _videoWidth) return;
                _videoWidth = value;
                OnPropertyChanged();
            }
        }

        public bool IsDisposed;

        public bool IsDisposing;
        private int _videoBitrate;
        private int _videoWidth;
        private int _videoHeight;
        private int _fps;
        private string _videoCodec;
        private double _timePosition;

        public ContentPresenter ContentPresenter =>
            Template.FindName("PART_ContentPresenter", this) as ContentPresenter;

        public IntPtr Hwnd { get; set; }

        public Grid MainGrid => Template.FindName("PART_MainGrid", this) as Grid;

        /// <summary>
        ///     MediaPlayer property for this VideoView
        /// </summary>
        public MpvPlayer MediaPlayer
        {
            get => GetValue(MediaPlayerProperty) as MpvPlayer;
            set => SetValue(MediaPlayerProperty, value);
        }

        public bool Stop()
        {
            try
            {
                if (MediaPlayer != null && (!IsDisposing && !IsDisposed && MediaPlayer.IsMediaLoaded))
                {
                    MediaPlayer.Stop();
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public Size Resolution
        {
            get
            {
                if (_videoHeight == 0 || _videoWidth == 0)
                {
                    return Size.Empty;
                }


                return new Size(_videoWidth, _videoHeight);
            }
        }

        public MPVVideoContent VideoContent { get; set; }

        public UIElement ViewContent { get; set; }

        public WindowsFormsHost WindowsFormsHost => Template.FindName(PART_PlayerHost, this) as WindowsFormsHost;

        private bool IsDesignMode => (bool) DesignerProperties.IsInDesignModeProperty
            .GetMetadata(typeof(DependencyObject)).DefaultValue;

        private bool IsUpdatingContent { get; set; }

        /// <summary>
        ///     WPF VideoView constructor
        /// </summary>
        public MPVVideoView()
        {
            DefaultStyleKey = typeof(MPVVideoView);
        }

        /// <summary>
        ///     Unhook mediaplayer and dispose foreground window
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        private static void OnMediaPlayerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //if (e.OldValue is MpvPlayer oldMediaPlayer)
            //{
            ////    oldMediaPlayer.API.SetPropertyLong("wid", IntPtr.Zero.ToInt64());
            //}

            //if (e.NewValue is MpvPlayer newMediaPlayer)
            //{
            //    newMediaPlayer.API.SetPropertyLong("wid", ((MPVVideoView) d).Hwnd.ToInt64());
            //}
        }


        /// <summary>
        ///     VideoContent management and MediaPlayer setup.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if (!IsDesignMode)
            {
                var windowsFormsHost = WindowsFormsHost;
                WindowsFormsHost.EnableWindowsFormsInterop();
                var videoOwner = this.GetVisualParent<VideoControl>();

                WindowsFormsHost.IsHitTestVisible = false;
                if (windowsFormsHost != null)
                {
                    VideoContent = new MPVVideoContent(windowsFormsHost, videoOwner)
                    {
                        Content = ViewContent
                    };
                }

                Hwnd = WindowsFormsHost.Child
                    .Handle; //(Template.FindName("PART_PlayerView", this) as Panel)?.Handle ?? IntPtr.Zero;

                var mpvPlayer = new MpvPlayer(Hwnd) {Volume = 0, KeepOpen = KeepOpen.No, AutoPlay = true};
                mpvPlayer.API.SetPropertyString("profile", "low-latency");
                if (!SettingsManager.Settings.Miscellaneous.DisableHardwareAcceleration)
                {
                    mpvPlayer.API.SetPropertyString("hwdec", "auto-copy");
                }

                mpvPlayer.API.SetPropertyDouble("fps", 600d);

                mpvPlayer.API.ObserveProperty("perf-info", MpvFormat.String, 0);
                mpvPlayer.API.ObserveProperty("video-bitrate", MpvFormat.String, 0);
                mpvPlayer.API.ObserveProperty("width", MpvFormat.String, 0);
                mpvPlayer.API.ObserveProperty("height", MpvFormat.String, 0);
                mpvPlayer.API.ObserveProperty("display-fps", MpvFormat.String, 0);
                mpvPlayer.API.ObserveProperty("video-codec", MpvFormat.String, 0);
                mpvPlayer.API.ObserveProperty("time-pos", MpvFormat.String, 0);
                mpvPlayer.API.PropertyChange += APIOnPropertyChange;
                mpvPlayer.API.CommandReply += APIOnCommandReply;
                MediaPlayer = mpvPlayer;
            }
        }

        //public object GetSnapshot()
        //{

        //    MediaPlayer.API.CommandAsync(32, "screenshot");
        //    return null;
        //}

        private void APIOnCommandReply(object sender, MpvCommandReplyEventArgs e)
        {
            if (e.ReplyUserData == 32)
            {
                
            }
        }

        /// <summary>
        ///     Override to update the foreground window content
        /// </summary>
        /// <param name="oldContent">old content</param>
        /// <param name="newContent">new content</param>
        protected override void OnContentChanged(object oldContent, object newContent)
        {
            base.OnContentChanged(oldContent, newContent);

            if (IsDesignMode || IsUpdatingContent)
            {
                return;
            }

            IsUpdatingContent = true;
            try
            {
                Content = null;
            }
            finally
            {
                IsUpdatingContent = false;
            }

            ViewContent = newContent as UIElement;
            if (VideoContent != null)
            {
                VideoContent.Content = ViewContent;
            }
        }

        /// <summary>
        ///     Unhook mediaplayer and dispose foreground window
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual async void Dispose(bool disposing)
        {
            var sw = new Stopwatch();
            sw.Start();
            if (!IsDisposed && !IsDisposing)
            {
                if (disposing)
                {
                    IsDisposing = true;

                    if (MediaPlayer != null && VideoContent != null)
                    {
                        try
                        {
                            // MediaPlayer.API.SetPropertyLong("wid", IntPtr.Zero.ToInt64());

                          
                            if (MediaPlayer.API.Handle != IntPtr.Zero)
                            {
                                MediaPlayer.API.EventLoop.Stop();
                                MediaPlayer.API.Command("quit");
                                // MediaPlayer.API.Functions.DetachDestroy(MediaPlayer.API.Handle);
                                MediaPlayer.API.Functions.TerminateDestroy(MediaPlayer.API.Handle);

                                GC.SuppressFinalize(MediaPlayer.API);
                                //await Task.Delay(5000);
                                //GC.ReRegisterForFinalize(mediaPlayerApi);
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    WindowsFormsHost?.Dispose();
                    VideoContent?.Close();
                }

                ViewContent = null;
                VideoContent = null;
                IsDisposed = true;
                IsDisposing = false;
            }

            sw.Stop();

            //Console.WriteLine($"VideoView took {sw.ElapsedMilliseconds}ms to dispose.");
        }

       
        private void APIOnPropertyChange(object sender, MpvPropertyChangeEventArgs e)
        {
            try
            {
                if (e.EventProperty.Format == MpvFormat.String)
                {
                    switch (e.EventProperty.Name)
                    {
                        case "video-bitrate":
                            int.TryParse(e.EventProperty.DataString, out var bitRate);
                            VideoBitrate = bitRate;
                            //_log?.Info($"{e.EventProperty.Name} {e.EventProperty.DataString}" );
                            break;
                        case "perf-info":
                            //_log?.Info($"{e.EventProperty.Name} {e.EventProperty.DataString}" );
                            break;
                        case "display-fps":
                            float.TryParse(e.EventProperty.DataString, out var fps);
                            Fps = (int)fps;
                            break;
                        case "video-codec":
                            VideoCodec = e.EventProperty.DataString;
                            break;
                        case "width":
                            int.TryParse(e.EventProperty.DataString, out var videoWidth);
                            VideoWidth = videoWidth;
                            break;
                        case "metadata":
                            _log?.Info($"{e.EventProperty.Name} {e.EventProperty.DataString}" );
                            break;
                        case "height":
                            int.TryParse(e.EventProperty.DataString, out var videoHeight);
                            VideoHeight = videoHeight;
                            break;
                        case "time-pos":
                            double.TryParse(e.EventProperty.DataString, out var timePos);
                            //Trace.WriteLine(e.EventProperty.DataString);
                            TimePosition = timePos;
                            break;
                    }
                }
            }
            catch (Exception exception)
            {
                
            }
        }

        public double TimePosition
        {
            get => _timePosition;
            set
            {
                if (value == _timePosition) return;
                _timePosition = value;
                OnPropertyChanged();
            }
        }

        public string VideoCodec
        {
            get => _videoCodec;
            set
            {
                if (value == _videoCodec) return;
                _videoCodec = value;
                OnPropertyChanged();
            }
        }

        public int Fps
        {
            get => _fps;
            set
            {
                if (value == _fps) return;
                _fps = value;
                OnPropertyChanged();
            }
        }

        public int VideoBitrate
        {
            get => _videoBitrate;
            set
            {
                if (value == _videoBitrate) return;
                _videoBitrate = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}