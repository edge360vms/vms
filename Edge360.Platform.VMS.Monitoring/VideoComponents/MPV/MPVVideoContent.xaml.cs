﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Interop;
using Edge360.Platform.VMS.Common.Annotations;

namespace Edge360.Platform.VMS.Monitoring.VideoComponents.MPV
{
    public partial class MPVVideoContent : INotifyPropertyChanged
    {
        public readonly WindowsFormsHost _bckgnd;
        private readonly Point _zeroPoint = new Point(0, 0);
        private UIElement _content;
        public Window WindowHost;
        private VideoControl _videoControl;

        internal new UIElement Content
        {
            get => _content;
            set
            {
                _content = value;
                PART_Content.Children.Clear();
                if (_content != null)
                {
                    PART_Content.Children.Add(_content);
                }
            }
        }

        public VideoControl VideoControl
        {
            get => _videoControl;
            set
            {
                if (Equals(value, _videoControl)) return;
                _videoControl = value;
                OnPropertyChanged();
            }
        }

        public MPVVideoContent(WindowsFormsHost background, VideoControl videoOwner)
        {
            if (videoOwner != null)
            {
                VideoControl = videoOwner;
            }

            Loaded += OnLoaded;
            Initialized += OnInitialized;
            InitializeComponent();
            
            DataContext = background.DataContext;

            _bckgnd = background;

            _bckgnd.DataContextChanged += Background_DataContextChanged;
            _bckgnd.Loaded += Background_Loaded;
            _bckgnd.Unloaded += Background_Unloaded;
        }

        private void OnInitialized(object sender, EventArgs e)
        {
            var helper2 = new WindowInteropHelper(this);
            SetWindowLong(helper2.Handle, GWL_EXSTYLE,
                GetWindowLong(helper2.Handle, GWL_EXSTYLE) | WS_EX_NOACTIVATE);
        }

        [DllImport("user32.dll")]
        public static extern IntPtr SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll")]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        private const int GWL_EXSTYLE = -20;
        private const int WS_EX_NOACTIVATE = 0x08000000;
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            //var window2 = FindParentWindow(VideoView.WindowsFormsHost.Child);
            //var window3 = VideoView.WindowsFormsHost.GetVisualParent<Window>();
            var helper2 = new WindowInteropHelper(this);
            SetWindowLong(helper2.Handle, GWL_EXSTYLE,
                GetWindowLong(helper2.Handle, GWL_EXSTYLE) | WS_EX_NOACTIVATE);
        }

        public void Redraw()
        {
            if (!IsRedrawing)
            {
                IsRedrawing = true;
                try
                {
                    if (_bckgnd.IsVisible)
                    {
                        var locationFromScreen = _bckgnd.PointToScreen(_zeroPoint);
                        var source = PresentationSource.FromVisual(WindowHost);
                        if (source?.CompositionTarget != null)
                        {
                            var targetPoints =
                                source.CompositionTarget.TransformFromDevice.Transform(locationFromScreen);
                            Left = targetPoints.X;
                            Top = targetPoints.Y;
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    IsRedrawing = false;
                }
            }
          
        }

        public bool IsRedrawing { get; set; }

        public void UpdateLocation()
        {
            if (!IsUpdatingLocation)
            {
                IsUpdatingLocation = true;
                try
                {
                    if (_bckgnd == null || !_bckgnd.IsVisible)
                    {
                        return;
                    }

                    if (_bckgnd.Parent != null)
                    {
                        var locationFromScreen = _bckgnd.PointToScreen(_zeroPoint);
                        var source = PresentationSource.FromVisual(WindowHost);
                        if (source?.CompositionTarget != null)
                        {
                            var targetPoints =
                                source.CompositionTarget.TransformFromDevice.Transform(locationFromScreen);
                            Left = targetPoints.X;
                            Top = targetPoints.Y;
                        }

                        var size = new Point(_bckgnd.ActualWidth, _bckgnd.ActualHeight);
                        Height = size.Y;
                        Width = size.X;
                    }
                }
                catch (Exception exception)
                {
                }
                finally
                {
                    IsUpdatingLocation = false;
                }
            }
          
        }

        public bool IsUpdatingLocation { get; set; }


        private void Background_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            DataContext = e.NewValue;
        }

        private void Background_Unloaded(object sender, RoutedEventArgs e)
        {
            _bckgnd.SizeChanged -= Wndhost_SizeChanged;

            if (WindowHost != null)
            {
                WindowHost.Closing -= WindowHostClosing;
                WindowHost.LocationChanged -= WindowHostLocationChanged;
            }

            Hide();
        }

        private void Background_Loaded(object sender, RoutedEventArgs e)
        {
            WindowHost = GetWindow(_bckgnd);
            //Trace.Assert(WindowHost != null);
            if (WindowHost == null)
            {
                return;
            }

            if (Owner != WindowHost)
            {
                Owner = WindowHost;
            }
            WindowHost.KeyDown += windowKeyDown;
            void windowKeyDown(object s, KeyEventArgs en)
            {
                try
                {
                    if ( en.Key == Key.System && en.SystemKey == Key.F4)
                    {
                        en.Handled = true;
                    }
                }
                catch (Exception exception)
                {
                    
                }
            }
            WindowHost.Closing += WindowHostClosing;
            _bckgnd.SizeChanged += Wndhost_SizeChanged;
            _bckgnd.IsVisibleChanged += BckgndOnIsVisibleChanged;
            WindowHost.LocationChanged += WindowHostLocationChanged;

            try
            {
                var locationFromScreen = _bckgnd.PointToScreen(_zeroPoint);
                var source = PresentationSource.FromVisual(WindowHost);
                if (source?.CompositionTarget != null)
                {
                    var targetPoints = source.CompositionTarget.TransformFromDevice.Transform(locationFromScreen);
                    Left = targetPoints.X;
                    Top = targetPoints.Y;
                }

                var size = new Point(_bckgnd.ActualWidth, _bckgnd.ActualHeight);
                Height = size.Y;
                Width = size.X;
                if (VideoControl.Visibility == Visibility.Visible)
                {
                    if (Visibility != Visibility.Visible)
                    {
                        Visibility = Visibility.Visible;
                    }
                }
                //WindowHost.Focus();
            }
            catch
            {
                //Hide();
                //throw new Exception("Unable to create WPF Window in VideoView.");
            }
        }

        private void BckgndOnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
           //     Trace.WriteLine($"My Visibility {Visibility} {_bckgnd.Visibility}");
                var wasVisible = (bool) e.OldValue;
                var newVis =  wasVisible ? Visibility.Hidden : Visibility.Visible;
                if (Visibility != newVis)
                {
                //    Trace.WriteLine($"Visibility Didn't Match! Setting {newVis}");
                    Visibility = newVis;
                }
            }
            catch (Exception exception)
            {
                
            }
        }

        private void WindowHostLocationChanged(object sender, EventArgs e)
        {
            Redraw();
        }

        private void Wndhost_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateLocation();
        }

        private void WindowHostClosing(object sender, CancelEventArgs e)
        {
            Close();
        }

        public event PropertyChangedEventHandler PropertyChanged;



        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}