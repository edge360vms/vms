using System;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using Edge360.Platform.VMS.Common.Enums.Video;
using Edge360.Platform.VMS.Common.Interfaces.Camera;
using Edge360.Platform.VMS.Common.Interfaces.Monitoring;
using Edge360.Platform.VMS.Common.Interfaces.Video;
using Edge360.Platform.VMS.Common.Properties;
using Edge360.Platform.Vms.TileGrid.Common.Interfaces.Adapters.VideoComponents;
using Telerik.Windows.Controls;
using Control = System.Windows.Forms.Control;
using Point = System.Windows.Point;
using Size = System.Windows.Size;

namespace Edge360.Platform.VMS.Monitoring.VideoComponents.MPV
{
    public class MPVVideoComponent : UserControl, IEdgeDigitalZoomSupport, IVideoComponent, IAirspaceContent,
        INotifyPropertyChanged
    {
        private bool _isLivePlayback;
        private bool _isPlaying;
        private bool _isShowingVideoStats;
        private bool _isStartingPlayback;
        private string _lastOpenedurl;
        private EPlaybackSpeed _playbackSpeed = EPlaybackSpeed.Speed1x;
        private EPlaybackState _playbackState;
        private EPlayerState _playerState;
        private IPtzManager _ptzManager;
        private MPVVideoView _videoView;

        private double _zoomFactorLast;

        public DateTime TimeSinceOpeningStream { get; set; }


        public VideoControl VideoControl { get; private set; }

        public MPVVideoView VideoView
        {
            get => _videoView;
            set
            {
                if (_videoView != value)
                {
                    _videoView = value;
                    OnPropertyChanged();
                }
            }
        }

        public Size ZoomLocation { get; set; }

        public MPVVideoComponent()
        {
            //VideoView = new MPVVideoView() {Content = Content};
            //Content = VideoView;
            //UIElement owner
            // Owner = owner;
            //_gridContent = gridContent;
            // Init();

            Loaded += OnLoaded;
        }

        public void Redraw()
        {
            VideoView?.VideoContent?.Redraw();
        }

        public void UpdateLocation()
        {
            VideoView?.VideoContent?.UpdateLocation();
        }

        public void SetZoomFactor(double zoomFactor)
        {
            _zoomFactorLast = zoomFactor;
            VideoView.MediaPlayer.API.SetPropertyDouble("video-zoom", zoomFactor);
        }

        public void SetPan(double x, double y)
        {
            //var amtX = x / (_zoomFactorLast + 1) * 1.3d;
            //var amtY = y / (_zoomFactorLast + 1) * 1.3d;
            VideoView.MediaPlayer.API.SetPropertyDouble("video-pan-x", x);
            VideoView.MediaPlayer.API.SetPropertyDouble("video-pan-y", y);
        }

        public Task<Bitmap> GetThumbnailAsync()
        {
            return null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public double ZoomFactor { get; set; }

        public string LastOpenedAddress
        {
            get
            {
                var lastOpenedAddress = LastOpenedUrl?.Replace("rtsp://", "")?.Split('/')[0];
                try
                {
                    var strings = lastOpenedAddress?.Split('@');
                    var authAddress = strings != null && strings.Length > 1 ? strings?[1] : lastOpenedAddress;
                    return authAddress;
                }
                catch (Exception e)
                {
                    
                }
                return lastOpenedAddress ?? "";
            }
        }

        public string LastOpenedUrl
        {
            get => _lastOpenedurl;
            set
            {
                if (_lastOpenedurl != value)
                {
                    _lastOpenedurl = value;
                    OnPropertyChanged();
                    OnPropertyChanged(nameof(LastOpenedAddress));
                }
            }
        }

        public bool IsShowingVideoStats
        {
            get => _isShowingVideoStats;
            set
            {
                if (_isShowingVideoStats != value)
                {
                    _isShowingVideoStats = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool IsStartingPlayback
        {
            get => _isStartingPlayback;
            set
            {
                if (_isStartingPlayback != value)
                {
                    _isStartingPlayback = value;
                    OnPropertyChanged();
                }
            }
        }

        public void Dispose()
        {
            VideoView?.Dispose();
        }

        public bool IsPlaying
        {
            get => _isPlaying;
            set
            {
                if (_isPlaying != value)
                {
                    _isPlaying = value;
                    OnPropertyChanged();
                }
            }
        }


        public void DigitalZoom(bool zoomIn, Point point)
        {
            ZoomFactor = Math.Max(0, zoomIn ? ZoomFactor + .16 : ZoomFactor - .16);
            var res = VideoView.Resolution;
            VideoView.MediaPlayer.API.SetPropertyDouble("video-zoom", ZoomFactor);
            if (res != Size.Empty)
            {
                var bounds = GetImageBounds();

                ZoomLocation = new Size(point.X / res.Width, point.Y / res.Height);
                var panX = bounds.Width / point.X;
                var panY = bounds.Height / point.Y;

                var newPanX = (bounds.Width / 2 - point.X) / bounds.Width * ZoomFactor;
                var newPanY = (bounds.Height / 2 - point.Y) / bounds.Height * ZoomFactor;
                // Console.WriteLine($"{panX}x{panY} - {ZoomLocation} - {bounds} - {point}");
                // Console.WriteLine($"{newPanX}x{newPanY}");
                try
                {
                    VideoView.MediaPlayer.API.SetPropertyDouble("video-pan-x",
                        Math.Max(-0.2 * ZoomFactor, Math.Min(0.2 * ZoomFactor, newPanX)));
                    VideoView.MediaPlayer.API.SetPropertyDouble("video-pan-y",
                        Math.Max(-0.2 * ZoomFactor, Math.Min(0.2 * ZoomFactor, newPanY)));
                }
                catch (Exception e)
                {
                }
            }
        }

        public event Action MediaPlayerEnded;
        public event Action MediaPlayerError;

        public bool IsLivePlayback
        {
            get => _isLivePlayback;
            set
            {
                if (_isLivePlayback != value)
                {
                    _isLivePlayback = value;
                    OnPropertyChanged();
                }
            }
        }

        public virtual EPlaybackSpeed PlaybackSpeed
        {
            get => _playbackSpeed;
            set
            {
                if (_playbackSpeed != value)
                {
                    _playbackSpeed = value;
                    PlaybackSpeedChanged?.Invoke(value);
                    OnPropertyChanged();
                }
            }
        }

        public virtual EPlaybackState PlaybackState
        {
            get => _playbackState;
            set
            {
                if (_playbackState != value)
                {
                    _playbackState = value;
                    PlaybackStateChanged?.Invoke(value);
                    OnPropertyChanged();
                }
            }
        }

        public EPlayerState PlayerState
        {
            get => _playerState;
            set
            {
                if (_playerState != value)
                {
                    _playerState = value;
                    PlayerStateChanged?.Invoke(value);
                    OnPropertyChanged();
                }
            }
        }

        public event Action<EPlayerState> PlayerStateChanged;
        public event Action<EPlaybackSpeed> PlaybackSpeedChanged;
        public event Action<EPlaybackState> PlaybackStateChanged;
        public event Action<ImageSource> FrameRendered;

        public async Task Restart()
        {
        }

        public void Play()
        {
            //_log?.Info($"Play... {LastOpenedUrl}");
            if (VideoView != null && !VideoView.IsDisposed && !VideoView.IsDisposing)
            {
                VideoView?.MediaPlayer?.Resume();
            }

            IsPlaying = true;
            PlayerState = EPlayerState.Playing;
        }

        public void Pause()
        {
            if (VideoView != null && !VideoView.IsDisposed && !VideoView.IsDisposing)
            {
                VideoView?.MediaPlayer?.Pause();
            }

            IsPlaying = false;
            PlayerState = EPlayerState.Paused;
        }

        public void Stop()
        {
            if (VideoView != null && !VideoView.IsDisposed && !VideoView.IsDisposing)
            {
                VideoView?.MediaPlayer?.Stop();
            }

            IsPlaying = false;
            IsLivePlayback = false;
            PlayerState = EPlayerState.Stopped;
        }

        public void JumpToPosition(TimeSpan time)
        {
            if (VideoView?.MediaPlayer != null)
            {
                //try
                //{
                //    for (int i = 0; i < 20; i++)
                //    {
                //        await Task.Delay(1000);
                //        try
                //        {
                //            Console.WriteLine($"{VideoView.MediaPlayer.Duration} {VideoView.MediaPlayer.Position}");
                //        }
                //        catch (Exception e)
                //        {
                //            Console.WriteLine(e);
                //        }

                //    }

                //    if (VideoView.MediaPlayer.Duration >= time)
                //    {
                //        await VideoView.MediaPlayer.SeekAsync(time);
                //    }

                //    //VideoView.MediaPlayer.Position = time;
                //}
                //catch (Exception e)
                //{
                //}
            }
        }

        public void Open(string url)
        {
            try
            {
                if (VideoView != null && !VideoView.IsDisposed && !VideoView.IsDisposing)
                {
                    if (VideoView?.MediaPlayer != null)
                    {
                        //if (LastOpenedUrl == url && TimeSinceOpeningStream > DateTime.UtcNow)
                        //{
                        //    return;
                        //}
                        // _log?.Info($"Loading... {url}");
                        TimeSinceOpeningStream = DateTime.UtcNow + TimeSpan.FromSeconds(1);
                        LastOpenedUrl = url;
                        if (!string.IsNullOrEmpty(url))
                        {
                            VideoView.MediaPlayer.Load(url, true);
                        }

                        PlayerState = EPlayerState.Opening;
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        public void NextEvent(ITimelineEvent type)
        {
        }

        public void PreviousEvent(ITimelineEvent type)
        {
        }


        public Size GetImageBounds()
        {
            if (VideoView != null)
            {
                var previewRectangle = VideoView.RenderSize;
                var sourceResolution = VideoView.Resolution;
                if (sourceResolution.IsEmpty)
                {
                    return Size.Empty;
                }

                var ratio = Math.Min(
                    previewRectangle.Width / sourceResolution.Width,
                    previewRectangle.Height / sourceResolution.Height);
                var imageBrushWidth = sourceResolution.Width * ratio;
                var imageBrushHeight = sourceResolution.Height * ratio;

                var xOffset = previewRectangle.Width - imageBrushWidth;
                var yOffset = previewRectangle.Height - imageBrushHeight;

                var newHeight =
                    previewRectangle.Height - yOffset;
                var newWidth =
                    previewRectangle.Width - xOffset;

                return new Size((int) newWidth, (int) newHeight);
            }

            return Size.Empty;
        }

        public IPtzManager PtzManager
        {
            get => _ptzManager;
            set
            {
                if (_ptzManager != value)
                {
                    _ptzManager = value;
                    OnPropertyChanged();
                }
            }
        }

        public static Window FindParentWindow(Control control)
        {
            var host = FindWpfHost(control);
            return Window.GetWindow(host);
        } //FindParentWindow

        private static WindowsFormsHost FindWpfHost(Control control)
        {
            if (control.Parent != null)
            {
                return FindWpfHost(control.Parent);
            }

            var typeName = "System.Windows.Forms.Integration.WinFormsAdapter";
            if (control.GetType().FullName == typeName)
            {
                var adapterAssembly = control.GetType().Assembly;
                var winFormsAdapterType = adapterAssembly.GetType(typeName);
                return (WindowsFormsHost) winFormsAdapterType.InvokeMember("_host",
                    BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.Instance, null, control,
                    new string[0], null);
            }

            throw new Exception(
                "The top parent of a control within a host should be a System.Windows.Forms.Integration.WinFormsAdapter, but that is not what was found.  Someone should propably check this out.");
        } //FindWPFHost

        public void SeekToStart()
        {
            if (VideoView.MediaPlayer != null && VideoView.MediaPlayer.IsMediaLoaded &&
                VideoView.MediaPlayer.Duration > TimeSpan.MinValue)
            {
                //   _log?.Info($"{VideoView.MediaPlayer.Duration}");
                VideoView.MediaPlayer.SeekAsync(VideoView.MediaPlayer.Duration);
            }
        }

        //private async void Init()
        //{
        //    await this.WpfUiThreadAsync(async () =>
        //    {
        //        if (_gridContent.Parent is Button parent)
        //        {
        //            if (parent.Parent is Grid grid)
        //            {
        //                grid.Children.Remove(parent);

        //                VideoView = new MPVVideoView
        //                {
        //                    //  MediaPlayer = mp,
        //                    Content = parent
        //                };

        //                Content = VideoView;
        //            }
        //        }
        //    });
        //}

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event Action MediaEndedBuffering;
        public event Action MediaEndedSeeking;
        public event Action MediaStartedBuffering;
        public event Action MediaStartedSeeking;
        public event Action MediaUnloaded;
        public event Action MediaLoaded;
        public event Action MediaPaused;
        public event Action MediaResumed;

        private void MediaPlayerOnMediaError(object sender, EventArgs e)
        {
            MediaPlayerError?.Invoke();
        }

        private void MediaPlayerOnMediaFinished(object sender, EventArgs e)
        {
            MediaPlayerEnded?.Invoke();
        }


        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            var videoControl = this.GetVisualParent<VideoControl>();
            VideoControl = videoControl;
            videoControl.VideoComponent = this;
            VideoView = Content as MPVVideoView;
            if (VideoView != null)
            {
                VideoView.Loaded += (o, a) =>
                {
                    if (VideoView != null)
                    {
                        if (VideoView.MediaPlayer != null)
                        {
                            VideoView.MediaPlayer.MediaPaused += (s, args) =>
                            {
                                MediaPaused?.Invoke();
                                IsPlaying = false;
                                PlayerState = EPlayerState.Paused;
                            };
                            VideoView.MediaPlayer.MediaResumed += (s, args) =>
                            {
                                MediaResumed?.Invoke();
                                IsPlaying = true;
                                PlayerState = EPlayerState.Playing;
                            };
                            VideoView.MediaPlayer.MediaFinished += MediaPlayerOnMediaFinished;
                            VideoView.MediaPlayer.MediaError += MediaPlayerOnMediaError;
                            VideoView.MediaPlayer.MediaUnloaded += MediaPlayerOnMediaUnloaded;
                            VideoView.MediaPlayer.MediaLoaded += MediaPlayerOnMediaLoaded;
                            VideoView.MediaPlayer.MediaEndedSeeking += MediaPlayerOnMediaEndedSeeking;
                            VideoView.MediaPlayer.MediaEndedBuffering += (s, args) =>
                            {
                                MediaEndedBuffering?.Invoke();
                            };
                            VideoView.MediaPlayer.MediaStartedSeeking += (s, args) =>
                            {
                                MediaStartedSeeking?.Invoke();
                            };
                            VideoView.MediaPlayer.MediaStartedBuffering += (s, args) =>
                            {
                                MediaStartedBuffering?.Invoke();
                            };
                        }
                    }
                };
            }
        }

        private void MediaPlayerOnMediaEndedSeeking(object sender, EventArgs e)
        {
            MediaEndedSeeking?.Invoke();
        }

        private void MediaPlayerOnMediaLoaded(object sender, EventArgs e)
        {
            MediaLoaded?.Invoke();
        }

        private void MediaPlayerOnMediaUnloaded(object sender, EventArgs e)
        {
            MediaUnloaded?.Invoke();
        }
    }
}