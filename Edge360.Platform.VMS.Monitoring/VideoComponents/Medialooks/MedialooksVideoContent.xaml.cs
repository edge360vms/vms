﻿//using System;
//using System.Diagnostics;
//using System.Runtime.InteropServices;
//using System.Threading;
//using System.Threading.Tasks;
//using System.Windows;
//using System.Windows.Interop;
//using System.Windows.Media;
//using Edge360.Platform.VMS.Common.Enums.Video;
//using Edge360.Platform.VMS.Common.Interfaces.Camera;
//using Edge360.Platform.VMS.Common.Interfaces.Video;
//using Edge360.Platform.VMS.Common.Util;
//using MFORMATSLib;

//namespace Edge360.Platform.VMS.Monitoring.VideoComponents.Medialooks
//{
//    public partial class MedialooksVideoContent : IVideoComponent
//    {
//        private readonly CancellationTokenSource _cancelSource;

//        private IntPtr _mSavedPointer;
//        private M_TIME _mTime;
//        private EPlaybackSpeed _playbackSpeed;
//        private EPlaybackState _playbackState;
//        private EPlayerState _playerState;

//        public MFPreviewClass ObjPreview { get; set; } = new MFPreviewClass();
//        public MFReaderClass ObjReader { get; set; } = new MFReaderClass();

//        public MedialooksVideoContent()
//        {
//            InitializeComponent();
//            SourceBrush.ImageSource = new D3DImage();
//            IsLivePlayback = true;
//            InitReader();
//            InitPreview();

//            ObjPreview.OnEventSafe += PreviewOnEvent;
//            if (GetD3DSource() != null)
//            {
//                GetD3DSource().IsFrontBufferAvailableChanged += PreviewSourceOnIsFrontBufferAvailableChanged;
//            }

//            _cancelSource?.Cancel();
//            _cancelSource = new CancellationTokenSource();

//            Task.Factory.StartNew(async () =>
//                {
//                    await Task.Delay(100);
//                    try
//                    {
//                        PlayerThreadWork(_cancelSource.Token);
//                    }
//                    catch //(Exception e)
//                    {
//                        //  _log?.Info(e);
//                    }
//                },
//                _cancelSource.Token);
//        }

//        public double ZoomFactor { get; set; }

//        public void DigitalZoom(bool zoomIn, Point point)
//        {
//        }

//        public Size GetImageBounds()
//        {
//            return default;
//        }

//        public IPtzController PtzController { get; set; }

//        public bool IsLivePlayback { get; set; }

//        public bool IsPlaying { get; set; }

//        public EPlaybackState PlaybackState
//        {
//            get => _playbackState;
//            set
//            {
//                if (_playbackState != value)
//                {
//                    PlaybackStateChanged?.Invoke(value);
//                    _playbackState = value;
//                }
//            }
//        }

//        public EPlaybackSpeed PlaybackSpeed
//        {
//            get => _playbackSpeed;
//            set
//            {
//                if (_playbackSpeed != value)
//                {
//                    PlaybackSpeedChanged?.Invoke(value);
//                    _playbackSpeed = value;
//                }
//            }
//        }

//        public EPlayerState PlayerState
//        {
//            get => _playerState;
//            set
//            {
//                if (_playerState != value)
//                {
//                    PlayerStateChanged?.Invoke(value);
//                    _playerState = value;
//                }
//            }
//        }

//        public async void Dispose()
//        {
//            await Task.Run(async () =>
//            {
//                try
//                {
//                    _cancelSource?.Cancel();
//                    _cancelSource?.Dispose();

//                    ObjPreview.MFClose();

//                    ObjReader.ReaderClose();

//                    //ObjReader.ReaderAbort();
//                }
//                catch (Exception e)
//                {
//                }
//            });
//        }

//        public event Action<ImageSource> FrameRendered;

//        public void Play()
//        {
//            IsPlaying = true;
//            PlaybackState = EPlaybackState.Live;
//        }

//        public void Pause()
//        {
//            IsPlaying = false;
//            PlayerState = EPlayerState.Paused;
//        }

//        public void Stop()
//        {
//            IsPlaying = false;
//            PlayerState = EPlayerState.Stopped;
//            SourceBrush.ImageSource = new D3DImage();
//            Task.Run(async () =>
//            {
//                try
//                {
//                    //ObjPreview?.MFClose();
//                    ObjReader?.ReaderClose();
//                    await this.WpfUiThreadAsync(() => { SourceBrush.ImageSource = new D3DImage(); });
//                }
//                catch (Exception e)
//                {
//                    Console.WriteLine(e);
//                }
//            });

//            //Dispose();
//        }

//        public void JumpToDate(DateTime? date, bool pause = false)
//        {
//        }

//        public void Open(string url)
//        {
//            Task.Run(async () => { ObjReader?.ReaderOpen(url, ""); });

//            PlayerState = EPlayerState.Opening;
//        }

//        public void NextEvent(ITimelineEvent type)
//        {
//        }

//        public void PreviousEvent(ITimelineEvent type)
//        {
//        }

//        public event Action<EPlayerState> PlayerStateChanged;

//        public event Action<EPlaybackSpeed> PlaybackSpeedChanged;

//        public event Action<EPlaybackState> PlaybackStateChanged;

//        public void JumpToDate(DateTime? date)
//        {
//        }


//        public D3DImage GetD3DSource()
//        {
//            return (D3DImage) SourceBrush.ImageSource;
//        }

//        private void ProcessFrame()
//        {
//            if (IsPlaying)
//            {
//                if (ObjReader != null)
//                {
//                    try
//                    {
//                        ObjReader.SourceFrameGetByNumber(-1, -1, out var pFrame,
//                            "direct=true"); // "direct=true"); // Get next frame from file
//                        ((IMFReceiver) ObjPreview)?.ReceiverFramePut(pFrame, -1, ""); //Send frame to the preview
//                        // pFrame.MFTimeGet(out _mTime);
//                        ReleaseComObject(pFrame);
//                        if (PlayerState != EPlayerState.Playing)
//                        {
//                            PlayerState = EPlayerState.Playing;
//                        }
//                    }
//                    catch
//                    {
//                        Thread.Sleep(15);
//                    }
//                }

//                Thread.Sleep(1);
//            }
//            else
//            {
//                Thread.Sleep(100);
//            }
//        }

//        private void InitReader()
//        {
//            ObjReader.PropsSet("scaling_quality", "2");
//            if (IsLivePlayback)
//            {
//                ObjReader.PropsSet("network.low_delay", "true");
//                ObjReader.PropsSet("network.buffer_max", "10.0");
//                ObjReader.PropsSet("network.buffer_min", "0.3");
//                ObjReader.PropsSet("forward.cache", "4");
//                ObjReader.PropsSet("network.open_async", "true");
//                ObjReader.PropsSet("external_process", "false");
//            }
//            else
//            {
//                ObjReader.PropsSet("network.low_delay", "false");
//                ObjReader.PropsSet("network.buffer_max", "10.0");
//                ObjReader.PropsSet("network.buffer_min", "0.5");
//                ObjReader.PropsSet("forward.cache", "4");
//                ObjReader.PropsSet("network.open_async", "true");
//                ObjReader.PropsSet("external_process", "true");
//            }

//            ObjReader.PropsSet("network.show_attempts", "false");
//            ObjReader.PropsSet("experimental.optimize_cpu", "true");
//            //ObjReader.PropsSet("mxf.force_ffmpeg", "true");
//            if (RenderOptions.ProcessRenderMode != RenderMode.SoftwareOnly)
//            {
//                ObjReader.PropsSet("decoder.nvidia", "true");
//            }
//        }

//        private void InitPreview()
//        {
//            ObjPreview.PropsSet("wpf_preview", "true");
//            ObjPreview.PropsSet("wpf_preview.downscale", "1");
//            ObjPreview.PropsSet("wpf_preview.update", "0");
//            ObjPreview.PropsSet("audio.enabled", "false");
//            ObjPreview.PreviewEnable("", 0, 1);
//        }

//        private void PlayerThreadWork(CancellationToken token)
//        {
//            while (token != null && !token.IsCancellationRequested)
//            {
//                try
//                {
//                    ProcessFrame();
//                }
//                catch
//                {
//                    Thread.Sleep(1);
//                }
//            }
//        }


//        private void ReleaseComObject(object obj)
//        {
//            if (obj != null)
//            {
//                Marshal.ReleaseComObject(obj);
//            }
//        }

//        private void PreviewSourceOnIsFrontBufferAvailableChanged(object sender, DependencyPropertyChangedEventArgs e)
//        {
//            if (IsPlaying)
//            {
//                if ((bool) e.NewValue)
//                {
//                    try
//                    {
//                        GetD3DSource().Lock();
//                        GetD3DSource().SetBackBuffer(D3DResourceType.IDirect3DSurface9, IntPtr.Zero);
//                        GetD3DSource().SetBackBuffer(D3DResourceType.IDirect3DSurface9, _mSavedPointer);
//                        GetD3DSource().AddDirtyRect(new Int32Rect(0, 0, GetD3DSource().PixelWidth,
//                            GetD3DSource().PixelHeight));
//                        GetD3DSource().Unlock();
//                    }
//                    catch (Exception ex)
//                    {
//                        _log?.Info(ex);
//                    }
//                }
//            }
//        }

//        private void PreviewOnEvent(string bsChannelId, string bsEventName, string bsEventParam,
//            object pEventObject)
//        {
//            try
//            {
//                if (_cancelSource.IsCancellationRequested || ObjReader == null)
//                {
//                    var pEventObjectPtr = Marshal.GetIUnknownForObject(pEventObject);
//                    if (pEventObjectPtr != IntPtr.Zero)
//                    {
//                        Marshal.Release(pEventObjectPtr);
//                    }

//                    return;
//                }

//                if (SourceBrush.ImageSource != null)
//                {
//                    if (bsEventName == "wpf_nextframe")
//                    {
//                        GetD3DSource().Lock();
//                        var pEventObjectPtr = Marshal.GetIUnknownForObject(pEventObject);
//                        if (pEventObjectPtr != _mSavedPointer)
//                        {
//                            //if (_mSavedPointer != IntPtr.Zero)
//                            //{
//                            //    Marshal.Release(_mSavedPointer);
//                            //}

//                            _mSavedPointer = pEventObjectPtr;

//                            //  previewSource.SetBackBuffer(D3DResourceType.IDirect3DSurface9, IntPtr.Zero);
//                            try
//                            {
//                                if (_mSavedPointer != IntPtr.Zero)
//                                {
//                                    // FrameImage.SetBackBuffer(D3DResourceType.IDirect3DSurface9, IntPtr.Zero);
//                                    GetD3DSource().SetBackBuffer(D3DResourceType.IDirect3DSurface9, _mSavedPointer);
//                                }
//                            }
//                            catch
//                            {
//                            }

//                            //PlayerView.PreviewImage.ImageSource = previewSource;
//                        }

//                        if (pEventObjectPtr != IntPtr.Zero)
//                        {
//                            Marshal.Release(pEventObjectPtr);
//                        }


//                        ReleaseComObject(pEventObject);
//                        try
//                        {
//                            if (GetD3DSource().IsFrontBufferAvailable)
//                            {
//                                GetD3DSource()?.AddDirtyRect(new Int32Rect(0, 0, GetD3DSource().PixelWidth,
//                                    GetD3DSource().PixelHeight));
//                            }
//                        }
//                        catch (Exception e)
//                        {
//                            _log?.Info(e);
//                            GetD3DSource()?.SetBackBuffer(D3DResourceType.IDirect3DSurface9, _mSavedPointer);
//                        }

//                        GetD3DSource()?.Unlock();
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                _log?.Info(e);
//            }
//        }
//    }
//}

