﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using Edge360.Platform.VMS.Common.Interfaces.Monitoring;
using Edge360.Platform.VMS.Common.Interfaces.Video;
using Edge360.Platform.VMS.UI.Common.Annotations;

namespace Edge360.Platform.VMS.Monitoring.VideoComponents
{
    public class VideoControl : UserControl, ITileComponent, INotifyPropertyChanged
    {
        //public static readonly DependencyProperty VideoComponentProperty = DependencyProperty.Register(
        //    "VideoComponent", typeof(IVideoComponent), typeof(VideoControl), new PropertyMetadata(default(IVideoComponent)));

        private int _tileIndex;

        public IVideoComponent VideoComponent
        {
            get;
            set;
        }

        public virtual void Dispose()
        {
        }



        public virtual ITileGrid GridModel { get; }
        public virtual int TileColumn { get; set; }
        public virtual int TileColumnSpan { get; set; }
        public virtual bool IsFullscreen { get; set; }
        public virtual int TileRow { get; set; }
        public virtual int TileRowSpan { get; set; }

        public virtual int TileIndex
        {
            get;
            set;
        }


        public virtual event Action<object> ObjectDragOver;
        public virtual event Action<object> ObjectDragComplete;

        public virtual void JumpToDate(DateTime? date, bool pause = false)
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}