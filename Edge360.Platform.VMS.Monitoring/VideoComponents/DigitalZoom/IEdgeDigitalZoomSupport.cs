﻿using System.Drawing;
using System.Threading.Tasks;

namespace Edge360.Platform.Vms.TileGrid.Common.Interfaces.Adapters.VideoComponents
{
    public interface IEdgeDigitalZoomSupport
    {
        void SetZoomFactor(double zoomFactor);
        void SetPan(double x, double y);

        Task<Bitmap> GetThumbnailAsync();
    }
}
