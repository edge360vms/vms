﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Edge360.Platform.VMS.Monitoring.Properties;
using Edge360.Platform.Vms.TileGrid.Common.Interfaces.Adapters.VideoComponents;
using Point = System.Windows.Point;

namespace Edge360.Platform.Vms.TileGrid.Common.UI.Controls.Tile.View.Overlay.Components.DigitalZoom
{
    /// <summary>
    /// Interaction logic for DigitalZoomWindow.xaml
    /// </summary>
    public partial class DigitalZoomWindow : UserControl, INotifyPropertyChanged
    {
        private double _viewWidth;
        private double _viewHeight;
        private double _zoomFactor;
        private double _viewLeft;
        private double _viewTop;

        public DigitalZoomWindow()
        {
            InitializeComponent(); 
            DataContext = this;

            ViewWidth = Canvas.Width;
            ViewHeight = Canvas.Height;

            //ThumbnailResolver = new Dynamic<Task<ImageBrush>>(async () =>
            //{
            //    if (Owner != null)
            //    {
            //        using var thumb = await Owner.GetThumbnailAsync().ConfigureAwait(true);
            //        if (thumb != null)
            //        {
            //            return new ImageBrush(thumb.ImageSourceFromBitmap());
            //        }
            //    }

            //    return null;
            //});
             
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(2);
            timer.Tick += timer_Tick;
            timer.Start(); 
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            GetThumbnailRequest();
        }
 
        public Task<ImageBrush> ThumbnailResolver { get; set; }
         

        private async void GetThumbnailRequest()
        {
            if (Math.Abs(_zoomFactor) > double.Epsilon)
            {
                return;
            }

            //var results = await ThumbnailResolver.Value.ConfigureAwait(true);
            //if (results != null )
            //{ 
            //    Window.Stretch = Stretch.Fill;
                
            //    Window.Fill = results; 
            //}
        }

        public double CanvasWidth => Canvas.Width;
        public double CanvasHeight => Canvas.Height;

        public double ViewWidth
        {
            get => _viewWidth;
            set
            { 
                value = Clamp(value, 0, CanvasWidth);
                if (value.Equals(_viewWidth)) return;
                _viewWidth = value;
                OnPropertyChanged(); 
            }
        }

        public double ViewHeight
        {
            get => _viewHeight;
            set
            { 
                value = Clamp(value, 0, CanvasHeight);
                if (value.Equals(_viewHeight)) return;
                _viewHeight = value;
                OnPropertyChanged(); 
            }
        }

        private double Clamp(double value, double min, double max)
        {
            var result = Math.Max(value, min);
            result = Math.Min(result, max);
            return result;
        }

        public double ZoomFactor
        {
            get => _zoomFactor;
            set
            {
                value = Clamp(value, 0, 4);
 
                if (value.Equals(_zoomFactor)) return;
                _zoomFactor = value;

                OnPropertyChanged();
                
                // 1 = *2
                // 2 = *4

                var offset = value + 1;
                
                ViewWidth = CanvasWidth / offset;
                ViewHeight = CanvasHeight / offset;

                GetThumbnailRequest();
            }
        }

        public double ViewLeft
        {
            get => _viewLeft;
            set
            {
                value = Clamp(value, 0, CanvasWidth / 2d);
                if (value.Equals(_viewLeft)) return;
                _viewLeft = value;
                OnPropertyChanged();

                Canvas.SetLeft(ViewBox, value);
            }
        }

        public double ViewTop
        {
            get => _viewTop;
            set
            {
                value = Clamp(value, 0, CanvasHeight / 2d);

                if (value.Equals(_viewTop)) return;
                _viewTop = value;
                OnPropertyChanged();

                Canvas.SetTop(ViewBox, value);
            }
        }

        private void ViewBox_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ZoomFactor += e.Delta * 0.001;
            UpdateViewPosition(e); 
        }

        private void UpdateViewPosition(MouseEventArgs e)
        {
            var pos = e.GetPosition(Canvas);

            if (ViewWidth >= CanvasWidth && ViewHeight >= CanvasHeight)
            {
                pos.X = Canvas.Width / 2d;
                pos.Y = Canvas.Height / 2d;
            }

            ViewLeft = pos.X - ViewWidth / 2d;
            ViewTop = pos.Y - ViewHeight / 2d;

            SendUpdatedView();
        }

        private IEdgeDigitalZoomSupport Owner { get; set; }

        public void Initialize(IEdgeDigitalZoomSupport owner)
        {
            Owner = owner;
        }

        public Point GetCanvasCenter()
        {
            return new Point(CanvasWidth / 2d, CanvasHeight / 2d);
        }

        public Point GetViewCenter()
        {
            return new Point(ViewLeft + ViewWidth / 2d, ViewTop + ViewHeight / 2d);
        }

        public Point GetViewCenterNormalized()
        { 
            return new Point((ViewLeft + ViewWidth / 2d) / CanvasWidth, (ViewTop + ViewHeight / 2d) / CanvasHeight);
        }

        public Point GetPanNormalized()
        {
            var viewCenterNormalized = GetViewCenterNormalized();
            return new Point(0.5 - viewCenterNormalized.X, 0.5 - viewCenterNormalized.Y);
        }

        public void SendUpdatedView()
        {
            if (Owner != null)
            { 
                var normalized = GetPanNormalized();
                Owner.SetZoomFactor(ZoomFactor);
                Owner.SetPan(normalized.X, normalized.Y);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Canvas_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            { 
                Trace.WriteLine(e);
                UpdateViewPosition(e);
            }
        }
    }
}
