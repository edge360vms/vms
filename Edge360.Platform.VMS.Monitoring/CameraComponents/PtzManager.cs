﻿using System.Threading.Tasks;
using System.Windows;
using Edge360.Platform.VMS.Common.Enums.Camera;
using Edge360.Platform.VMS.Common.Interfaces.Camera;

namespace Edge360.Platform.VMS.Monitoring.CameraComponents
{
    public class PtzManager : IPtzManager
    {
        public CameraConfigManager CameraConfig { get; set; }

        public virtual int ZoomSpeed { get; set; } = 100;
        public virtual int PanSpeed { get; set; } = 100;

        public virtual void ZoomRelease()
        {
        }

        public virtual void PanRelease()
        {
        }

        public Task StopPtz()
        {
            return null;
        }

        public virtual async Task<bool> PanCamera(EPtzCommand command)
        {
            return false;
        }

        public virtual async Task<bool> ZoomCamera(EPtzCommand command)
        {
            return false;
        }

        public virtual async Task<bool> SetPreset(EPtzPreset preset)
        {
            return false;
        }

        public Task Zoom(bool zoomIn, Point point, float zoomAmount)
        {
            return null;
        }

        public virtual Task Zoom(bool zoomIn, Point point)
        {
            return null;
        }

        public virtual void ContinuousZoom(bool zoomIn, Point point)
        {
        }

        public virtual Task MaxZoomAsync(bool zoomIn)
        {
            return null;
        }

        public void AreaZoom(Point point, int widthZoom, int imageHeight, int imageWidth, int rectHeight, int rectWidth)
        {
        }

        public void PtzCenter(Point point, int imageWidth, int imageHeight)
        {
        }

        public Task<bool> PanCamera(Point point)
        {
            return default;
        }
    }
}