﻿using System;
using System.Windows;
using Edge360.Platform.VMS.Common.Interfaces.Camera;

namespace Edge360.Platform.VMS.Monitoring.CameraComponents
{
    public class CameraConfigManager : ICameraConfiguration
    {
        public string Address { get; set; }
        public string Label { get; set; }
        public string Model { get; set; }
        public Guid? Id { get; set; }

        public Size GetResolution()
        {
            return default;
        }
    }
}