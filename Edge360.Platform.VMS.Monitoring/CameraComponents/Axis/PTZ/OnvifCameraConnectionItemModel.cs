﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Edge360.Platform.VMS.Common.Util;
using Edge360.Platform.VMS.UI.Common.Annotations;
using Mictlanix.DotNet.Onvif;
using Mictlanix.DotNet.Onvif.Common;
using Mictlanix.DotNet.Onvif.Device;
using Mictlanix.DotNet.Onvif.Media;
using Mictlanix.DotNet.Onvif.Ptz;

namespace Edge360.Platform.VMS.Monitoring.CameraComponents.Axis.PTZ
{
    //public class OnvifGetCompatibleConfigurationsResponse : GetCompatibleConfigurationsResponse, ICompatibleResponses, INotifyPropertyChanged
    //{
    //    public PTZConfiguration[] PtzConfiguration
    //    {
    //        get => PTZConfiguration;
    //        set => PTZConfiguration = value;
    //    }

    //    public event PropertyChangedEventHandler PropertyChanged;

    //    [NotifyPropertyChangedInvocator]
    //    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    //    {
    //        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    //    }
    //}

    //public interface ICompatibleResponses
    //{
    //}

    public interface ICameraConfiguration
    {
        string RtspUrl { get; set; }
        GetCompatibleConfigurationsResponse CompatibleConfigurations { get; set; }
        PTZConfigurationOptions ConfigurationOptions { get; set; }
        string Hostname { get; set; }
        string Password { get; set; }
        string Token { get; set; }
        string Username { get; set; }
        string DefaultStreamUri { get; set; }
        bool IsGettingStreamUri { get; set; }
        List<Profile> Profiles { get; set; }
        void InitializeConnections();
        Task GetToken();
        Task GetConfigurations();
    }

    public class OnvifCameraConnectionItemModel : INotifyPropertyChanged, ICameraConfiguration
    {
        private GetCompatibleConfigurationsResponse _compatibleConfigurations;
        private PTZConfigurationOptions _configurationOptions;
        private DeviceClient _deviceController;
        private string _hostname;
        private bool _isSettingDeviceClient;
        private bool _isSettingMediaClient;
        private bool _isSettingPtzClient;

        private MediaClient _mediaProfileController;
        private string _password;
        private PTZClient _ptzClient;
        private string _onvifRtspUrl;
        private List<Profile> _profiles;
        private bool _isGettingStreamUri;
        private string _defaultStreamUri;
        private GetPresetsResponse _cameraPresets;
        private bool _isInitializing;

        public static IEqualityComparer<OnvifCameraConnectionItemModel> HostnameUsernamePasswordComparer { get; } =
            new HostnameUsernamePasswordEqualityComparer();

        public string RtspUrl
        {
            get => _onvifRtspUrl;
            set
            {
                if (value == _onvifRtspUrl) return;
                _onvifRtspUrl = value;
                OnPropertyChanged();
            }
        }

        public GetCompatibleConfigurationsResponse CompatibleConfigurations
        {
            get => _compatibleConfigurations;
            set
            {
                if (Equals(value, _compatibleConfigurations))
                {
                    return;
                }

                _compatibleConfigurations = value;
                OnPropertyChanged();
            }
        }

        public PTZConfigurationOptions ConfigurationOptions
        {
            get => _configurationOptions;
            set
            {
                if (Equals(value, _configurationOptions))
                {
                    return;
                }

                _configurationOptions = value;
                OnPropertyChanged();
            }
        }

        public DeviceClient DeviceClient
        {
            get
            {
                if (_deviceController == null)
                {
                    Task.Run(async () =>
                    {
                        var deviceClientAsync = await CreateDeviceClientAsync();
                        if (deviceClientAsync != null)
                        {
                            DeviceClient = deviceClientAsync;
                        }
                    });
                }

                return _deviceController;
            }
            set => _deviceController = value;
        }

        public string Hostname
        {
            get => _hostname;
            set
            {
                if (value == _hostname)
                {
                    return;
                }

                _hostname = value;
                OnPropertyChanged();
            }
        }

        public bool IsSettingDeviceClient
        {
            get => _isSettingDeviceClient;
            set
            {
                if (value == _isSettingDeviceClient)
                {
                    return;
                }

                _isSettingDeviceClient = value;
                OnPropertyChanged();
            }
        }

        public bool IsSettingMediaClient
        {
            get => _isSettingMediaClient;
            set
            {
                if (value == _isSettingMediaClient)
                {
                    return;
                }

                _isSettingMediaClient = value;
                OnPropertyChanged();
            }
        }

        public bool IsSettingPtzClient
        {
            get => _isSettingPtzClient;
            set
            {
                if (value == _isSettingPtzClient)
                {
                    return;
                }

                _isSettingPtzClient = value;
                OnPropertyChanged();
            }
        }

        

        public MediaClient MediaClient
        {
            get
            {
                if (_mediaProfileController == null)
                {
                    Task.Run(async () =>
                    {
                        var mediaClientAsync = await CreateMediaClientAsync();
                        if (mediaClientAsync != null)
                        {
                            MediaClient = mediaClientAsync;
                        }
                    });
                }

                return _mediaProfileController;
            }
            set => _mediaProfileController = value;
        }

        public string Password
        {
            get => _password;
            set
            {
                if (value == _password)
                {
                    return;
                }

                _password = value;
                OnPropertyChanged();
            }
        }

        public bool NoPtz { get; set; }

        public PTZClient PtzClient
        {
            get
            {
                if (_ptzClient == null && !NoPtz)
                {
                    Task.Run(async () =>
                    {
                        var ptzClientAsync = await CreatePtzClientAsync();
                        if (ptzClientAsync != null)
                        {
                            PtzClient = ptzClientAsync;
                        }
                        else
                        {
                            NoPtz = true;
                        }
                    });
                }

                return _ptzClient;
            }

            set => _ptzClient = value;
        }

        public string Token { get; set; }
        public string Username { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        public DateTimeOffset TimeSinceCreateMediaClient { get; set; }

        private async Task<MediaClient> CreateMediaClientAsync()
        {
            try
            {
                return await Task.Run(async () =>
                {
                    if (!IsSettingMediaClient && TimeSinceCreateMediaClient < DateTimeOffset.UtcNow - TimeSpan.FromSeconds(30))
                    {
                        IsSettingMediaClient = true;
                        TimeSinceCreateMediaClient = DateTimeOffset.UtcNow;

                        using (var cts = new CancellationTokenSource(10000))
                        {

                            var client = await Task.Run(async () =>
                            {
                                try
                                {
                                    // Trace.WriteLine($"CreateMediaClientAsync {Hostname} {Username} {Password}");
                                    var client =
                                        await OnvifClientFactory.CreateMediaClientAsync(Hostname, Username, Password);
                                    return
                                        client;
                                }
                                catch (Exception e)
                                {
                                    return default;
                                }
                                finally
                                {

                                    IsSettingMediaClient = false;
                                }
                            });

                            return client;
                        }
                    }

                    return default;
                });
            }
            catch (Exception e)
            {
                return default;
            }
        }

        private async Task<DeviceClient> CreateDeviceClientAsync()
        {
            try
            {
                using (var cts = new CancellationTokenSource(12000))
                {

                    return await Task.Run(async () =>
                    {
                        if (!IsSettingDeviceClient && TimeSinceCreateDeviceClientAsync <
                            DateTimeOffset.UtcNow - TimeSpan.FromSeconds(30))
                        {
                            IsSettingDeviceClient = true;
                            TimeSinceCreateDeviceClientAsync = DateTimeOffset.UtcNow;
                            var client = await Task.Run(async () =>
                            {
                                try
                                {
                                    var client =
                                        await OnvifClientFactory.CreateDeviceClientAsync(Hostname, Username, Password);
                                    return client;
                                }
                                catch (Exception e)
                                {
                                    return default;
                                }
                                finally
                                {
                                    IsSettingDeviceClient = false;
                                }
                            }, cts.Token);
                            return client;
                        }

                        return default;
                    });
                }
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public DateTimeOffset TimeSinceCreateDeviceClientAsync { get; set; }

        private async Task<PTZClient> CreatePtzClientAsync()
        {
            try
            {
                using (var cts = new CancellationTokenSource(12000))
                {

                    return await Task.Run(async () =>
                    {
                        if (!IsSettingPtzClient && TimeSinceCreatePtzClientAsync <
                            DateTimeOffset.UtcNow - TimeSpan.FromSeconds(30))
                        {
                            IsSettingPtzClient = true;
                            TimeSinceCreatePtzClientAsync = DateTimeOffset.UtcNow;
                            var client = await Task.Run(async () =>
                            {
                                try
                                {
                                    var cl =
                                        await OnvifClientFactory.CreatePTZClientAsync(Hostname, Username, Password);
                                    return cl;
                                }
                                catch (FaultException fault)
                                {
                                    if (fault?.Message == "The service is not supported")
                                    {
                                        NoPtz = true;
                                    }

                                    return default;
                                }
                                catch (Exception e)
                                {
                                    return default;
                                }
                                finally
                                {
                                    IsSettingPtzClient = false;
                                }
                            },cts.Token);
                            return client;
                        }

                        return default;
                    });
                }
            }
            catch (Exception e)
            {
                return default;
            }
        }

        public DateTimeOffset TimeSinceCreatePtzClientAsync { get; set; }

        public async void InitializeConnections()
        {
            if (!IsInitializing)
            {
                IsInitializing = true;
                try
                {
                    var ctgs = new CancellationTokenSource();
                    ctgs.CancelAfter(TimeSpan.FromSeconds(15));
                    
                    await Task.Run(async () =>
                    {
                        while (MediaClient == null && !ctgs.IsCancellationRequested)
                        {
                            await Task.Delay(200);
                        }
                        ctgs.Token.ThrowIfCancellationRequested();
                        await GetToken();
                    //    DefaultStreamUri = await GetStreamUri(Token);
                        while (PtzClient == null && !NoPtz  && !ctgs.IsCancellationRequested)
                        {
                            await Task.Delay(200);
                        }
                        ctgs.Token.ThrowIfCancellationRequested();
                        if (PtzClient != null  && !ctgs.IsCancellationRequested)
                        {
                            await GetConfigurations();
                        }
                        ctgs.Token.ThrowIfCancellationRequested();
                        while (DeviceClient == null  && !ctgs.IsCancellationRequested)
                        {
                            await Task.Delay(200);
                        }
                        ctgs.Token.ThrowIfCancellationRequested();
                    }, ctgs.Token);
                }
                catch (Exception e)
                {
                }
                finally
                {

                    IsInitializing = false;
                }
            }
              
        }

        public bool IsInitializing
        {
            get => _isInitializing;
            set
            {
                if (value == _isInitializing) return;
                _isInitializing = value;
                OnPropertyChanged();
            }
        }

        public string DefaultStreamUri
        {
            get
            {
                if (_defaultStreamUri == null)
                {
                    Task.Run((async () =>
                    {
                        if (!string.IsNullOrEmpty(Token))
                        {
                            IsInitializing = true;
                            var defaultUri = await GetStreamUri(Token);
                            if (defaultUri != null)
                            {
                                DefaultStreamUri = defaultUri;
                            }

                            IsInitializing = false;
                        }
                    }));
                }
                return _defaultStreamUri;
            }
            set
            {
                if (value == _defaultStreamUri) return;
                _defaultStreamUri = value;
                OnPropertyChanged();
            }
        }

        public async Task GetToken()
        {
            try
            {
                await Task.Run(async () =>
                {
                    var profilesAsync = await MediaClient.GetProfilesAsync();
                    if (profilesAsync != null)
                    {
                        Profiles = profilesAsync?.Profiles.OrderBy(o => o.VideoEncoderConfiguration.H264 != null).ToList();
                        foreach (var p in Profiles)
                        {
                            try
                            {
                                var streamUri = await GetStreamUri(null, p);
                                if (!string.IsNullOrEmpty(streamUri))
                                {
                                    Token = p.token;
                                    DefaultStreamUri = streamUri;
                                    break;
                                }
                            }
                            catch (Exception e)
                            {
                            
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
            }
        }

        public async Task<string> GetStreamUri(string token = null, Profile profile = null)
        {
            if (!IsGettingStreamUri)
            {
                IsGettingStreamUri = true;
                try
                {
                    return await Task.Run(async () =>
                    {
                        if (profile != null)
                        {
                            try
                            {
                                var uri =
                                    await MediaClient.GetStreamUriAsync(
                                        new StreamSetup
                                        {
                                            Stream = StreamType.RTPUnicast,
                                            Transport = new Transport
                                                {Protocol = TransportProtocol.RTSP}
                                        }, profile.token);
                                if (uri != null)
                                {
                                    if (uri.Uri != null)
                                    {
                                        return uri.Uri;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                Trace.WriteLine($"{e.GetType()} {e.Message}");
                                try
                                {
                                    var uri =
                                        await MediaClient.GetStreamUriAsync(
                                            new StreamSetup
                                            {
                                                Stream = StreamType.RTPUnicast,
                                                Transport = new Transport
                                                    {Protocol = TransportProtocol.TCP}
                                            }, profile.token);
                                    if (uri != null)
                                    {
                                        if (uri.Uri != null)
                                        {
                                            return uri.Uri;
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {
                                    Trace.WriteLine($"{exception.GetType()} {exception.Message}");
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(token))
                        {
                            try
                            {
                                var uri =
                                    await MediaClient.GetStreamUriAsync(
                                        new StreamSetup
                                        {
                                            Stream = StreamType.RTPUnicast,
                                            Transport = new Transport
                                                {Protocol = TransportProtocol.RTSP}
                                        }, token);
                                if (uri != null)
                                {
                                    if (uri.Uri != null)
                                    {
                                        return uri.Uri;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                Trace.WriteLine($"{e.GetType()} {e.Message}");
                                try
                                {
                                    var uri =
                                        await MediaClient.GetStreamUriAsync(
                                            new StreamSetup
                                            {
                                                Stream = StreamType.RTPUnicast,
                                                Transport = new Transport
                                                    {Protocol = TransportProtocol.TCP}
                                            }, token);
                                    if (uri != null)
                                    {
                                        if (uri.Uri != null)
                                        {
                                            return uri.Uri;
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {
                                    Trace.WriteLine($"{exception.GetType()} {exception.Message}");
                                    return default;
                                }
                            }
                        }
                        return default;
                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                finally
                {
                    IsGettingStreamUri = false;
                }
            }

            return default;
        }

        public bool IsGettingStreamUri
        {
            get
            {
                return _isGettingStreamUri;
            }
            set
            {
                if (value == _isGettingStreamUri) return;
                _isGettingStreamUri = value;
                OnPropertyChanged();
            }
        }

        public List<Profile> Profiles
        {
            get => _profiles;
            set
            {
                if (Equals(value, _profiles)) return;
                _profiles = value;
                OnPropertyChanged();
            }
        }

        public GetPresetsResponse CameraPresets
        {
            get => _cameraPresets;
            set
            {
                if (Equals(value, _cameraPresets)) return;
                _cameraPresets = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Size> AvailableResolutions { get; set; } = new ObservableCollection<Size>();

        public async Task GetConfigurations()
        {
            try
            {
                await Task.Run(async () =>
                {
                    if (PtzClient != null)
                    {
                        var getCompatibleConfigurationsResponse =
                            (await PtzClient?.GetCompatibleConfigurationsAsync(Token));
                        if (getCompatibleConfigurationsResponse != null)
                        {
                            CompatibleConfigurations = getCompatibleConfigurationsResponse;
                        }


                        var getConfigurationsResponse = await PtzClient?.GetConfigurationsAsync();
                        if (getConfigurationsResponse != null)
                        {
                            var configurationToken = getConfigurationsResponse?.PTZConfiguration
                                ?.FirstOrDefault();
                            if (!string.IsNullOrWhiteSpace(configurationToken?.token))
                            {
                                try
                                {
                                    ConfigurationOptions =
                                        await PtzClient?.GetConfigurationOptionsAsync(configurationToken.token);
                                    //if (_configOptions.Spaces.RelativePanTiltTranslationSpace.Length == 1)
                                    //{

                                    //    var addSpace = await Controller.SetConfigurationAsync(configurationToken,)
                                    //}
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private sealed class
            HostnameUsernamePasswordEqualityComparer : IEqualityComparer<OnvifCameraConnectionItemModel>
        {
            public bool Equals(OnvifCameraConnectionItemModel x, OnvifCameraConnectionItemModel y)
            {
                if (ReferenceEquals(x, y))
                {
                    return true;
                }

                if (ReferenceEquals(x, null))
                {
                    return false;
                }

                if (ReferenceEquals(y, null))
                {
                    return false;
                }

                if (x.GetType() != y.GetType())
                {
                    return false;
                }

                return x.Hostname == y.Hostname && x.Username == y.Username && x.Password == y.Password;
            }

            public int GetHashCode(OnvifCameraConnectionItemModel obj)
            {
                unchecked
                {
                    var hashCode = obj.Hostname != null ? obj.Hostname.GetHashCode() : 0;
                    hashCode = (hashCode * 397) ^ (obj.Username != null ? obj.Username.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (obj.Password != null ? obj.Password.GetHashCode() : 0);
                    return hashCode;
                }
            }
        }
    }

    public interface ICameraConnection
    {
    }
}