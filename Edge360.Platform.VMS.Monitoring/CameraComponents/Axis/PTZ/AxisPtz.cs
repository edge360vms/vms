﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Edge360.Platform.VMS.Common.Enums.Camera;
using Edge360.Platform.VMS.Common.Interfaces.Video;
using Edge360.Platform.VMS.Monitoring.CameraComponents.Axis.Config;
using Edge360.Platform.VMS.Monitoring.VideoComponents;
using log4net;

namespace Edge360.Platform.VMS.Monitoring.CameraComponents.Axis.PTZ
{
    public class AxisPtz : PtzManager
    {
        ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override int PanSpeed { get; set; } = 100;
        public override int ZoomSpeed { get; set; } = 100;
        public string BaseUrl => $"http://{CameraConfig.Address}";

        public VideoContentControl ContentControl { get; set; }

        public bool IsRequestingPtz { get; set; }

        public AxisPtz(VideoContentControl control, AxisCameraConfigManager config)
        {
            ContentControl = control;

            CameraConfig = config;
        }


        public override void PanRelease()
        {
            RequestContinuousPan(0, 0);
            base.PanRelease();
        }

        public override void ZoomRelease()
        {
            if (!Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                ContinuousZoom(false, 0);
            }

            base.ZoomRelease();
        }

        public override async Task<bool> PanCamera(EPtzCommand command)
        {
            switch (command)
            {
                case EPtzCommand.PanUp:
                    RequestContinuousPan(0, PanSpeed);
                    break;
                case EPtzCommand.PanUpLeft:
                    RequestContinuousPan(-PanSpeed, PanSpeed);
                    break;
                case EPtzCommand.PanUpRight:
                    RequestContinuousPan(PanSpeed, PanSpeed);
                    break;
                case EPtzCommand.PanDown:
                    RequestContinuousPan(0, -PanSpeed);
                    break;
                case EPtzCommand.PanDownLeft:
                    RequestContinuousPan(-PanSpeed, -PanSpeed);
                    break;
                case EPtzCommand.PanDownRight:
                    RequestContinuousPan(PanSpeed, -PanSpeed);
                    break;
                case EPtzCommand.PanLeft:
                    RequestContinuousPan(-PanSpeed, 0);
                    break;
                case EPtzCommand.PanRight:
                    RequestContinuousPan(PanSpeed, 0);
                    break;
            }

            return false;
        }

        public override async Task<bool> ZoomCamera(EPtzCommand command)
        {
            switch (command)
            {
                case EPtzCommand.ZoomIn:
                    ContinuousZoom(true);
                    break;
                case EPtzCommand.ZoomOut:
                    ContinuousZoom(false);
                    break;
            }

            return false;
        }

        public override async Task Zoom(bool zoomIn, Point point)
        {
            try
            {
                IsRequestingPtz = true;
                var uri = new UriBuilder(BaseUrl)
                {
                    Path = "axis-cgi/com/ptz.cgi",
                    Query = $"rzoom={(zoomIn ? 200 : -200)}"
                };
                var request = (HttpWebRequest) WebRequest.Create(uri.Uri);
                request.Credentials = new NetworkCredential("root", "pass");

                using (var response = await request.GetResponseAsync())
                {
                    IsRequestingPtz = false;
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
            finally
            {
                IsRequestingPtz = false;
            }
        }

        public override async Task<bool> SetPreset(EPtzPreset preset)
        {
            try
            {
                IsRequestingPtz = true;
                var builtUri = new UriBuilder(BaseUrl)
                {
                    Path = "axis-cgi/com/ptz.cgi",

                    Query = (int) preset <= 0
                        ? "move=home&speed=100"
                        : $"gotoserverpresetno={(int) preset}&speed=100"
                };
                var request = (HttpWebRequest) WebRequest.Create(builtUri.Uri);
                request.Credentials = new NetworkCredential("root", "pass");

                using (var response = await request.GetResponseAsync())
                {
                    IsRequestingPtz = false;
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
                //   _log?.Info(e);
            }
            finally
            {
                IsRequestingPtz = false;
            }

            return false;
        }

        public override Task MaxZoomAsync(bool zoomIn)
        {
            MaxZoom(zoomIn, new Point());
            base.MaxZoomAsync(zoomIn);
            return null;
        }

        private async void MaxZoom(bool zoomIn, Point point)
        {
            try
            {
                IsRequestingPtz = true;
                var uri = new UriBuilder(BaseUrl)
                {
                    Path = "axis-cgi/com/ptz.cgi",
                    Query = $"zoom={(zoomIn ? 9999 : 1)}&speed=100"
                };
                var request = (HttpWebRequest) WebRequest.Create(uri.Uri);
                request.Credentials = new NetworkCredential("root", "pass");

                using (var response = await request.GetResponseAsync())
                {
                    IsRequestingPtz = false;
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
            finally
            {
                IsRequestingPtz = false;
            }
        }

        public void ContinuousZoom(bool zoomIn, int? speed = null)
        {
            if (speed == null)
            {
                speed = ZoomSpeed;
            }

            try
            {
                IsRequestingPtz = true;
                var resolution = ((IVideoComponent) ContentControl.Content).GetImageBounds();
                var uri = new UriBuilder(BaseUrl)
                {
                    Path = "axis-cgi/com/ptz.cgi",
                    Query =
                        $"continuouszoommove={(zoomIn ? speed : -speed)}&imagewidth={resolution.Width}&imageheight={resolution.Height}" //,{z}"//$"areazoom={x},{y},{z}" //&imagerotation=0&resolution=1280x720"
                };
                var request = (HttpWebRequest) WebRequest.Create(uri.Uri);
                request.KeepAlive = false;
                request.Timeout = 1000;
                request.Credentials = new NetworkCredential("root", "pass");

                using (var response = (HttpWebResponse) request.GetResponse())
                {
                    IsRequestingPtz = false;
                    response.Close();
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                IsRequestingPtz = false;
            }
        }

        public bool RequestContinuousPan(int x, int y)
        {
            try
            {
                //http://10.100.0.56/axis-cgi/com/ptz.cgi?camera=1&continuouspantiltmove=0,0&imagerotation=180&timestamp=1562862458786
                IsRequestingPtz = true;
                var resolution = ((IVideoComponent) ContentControl.Content).GetImageBounds();
                var uri = new UriBuilder(BaseUrl)
                {
                    Path = "axis-cgi/com/ptz.cgi",
                    Query =
                        $"continuouspantiltmove={x},{y}&imagewidth={resolution.Width}&imageheight={resolution.Height}" //,{z}"//$"areazoom={x},{y},{z}" //&imagerotation=0&resolution=1280x720"
                };
                var request = (HttpWebRequest) WebRequest.Create(uri.Uri);
                request.Credentials = new NetworkCredential("root", "pass");
                request.KeepAlive = false;
                request.Timeout = 1000;
                using (var response = (HttpWebResponse) request.GetResponse())
                {
                    IsRequestingPtz = false;
                    response.Close();
                }

                return true;
            }
            catch (Exception)
            {
            }
            finally
            {
                IsRequestingPtz = false;
            }

            return false;
        }

        public async void RequestPtzMove(int x, int y)
        {
            //http://10.100.0.56/axis-cgi/com/ptz.cgi?areazoom=100,0,0

            try
            {
                IsRequestingPtz = true;
                var resolution = ((IVideoComponent) ContentControl.Content).GetImageBounds();
                var uri = new UriBuilder(BaseUrl)
                {
                    Path = "axis-cgi/com/ptz.cgi",
                    //  Query = $"continuouspantiltmove={x},{y} //&imagerotation=0"
                    Query =
                        $"center={x},{y}&imagewidth={resolution.Width}&imageheight={resolution.Height}&speed=100" //,{z}"//$"areazoom={x},{y},{z}" //&imagerotation=0&resolution=1280x720"
                };
                //_log?.Info($"{uri.Query}");
                var request = (HttpWebRequest) WebRequest.Create(uri.Uri);
                request.Credentials = new NetworkCredential("root", "pass");

                using (var response = await request.GetResponseAsync())
                {
                    if (response.GetResponseStream() != null)
                    {
                        IsRequestingPtz = false;
                        response.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
            finally
            {
                IsRequestingPtz = false;
            }
        }

        public async void RequestPtzZoomMove(int x, int y, int z)
        {
            //http://10.100.0.56/axis-cgi/com/ptz.cgi?areazoom=100,0,0

            try
            {
                IsRequestingPtz = true;
                var resolution = ((IVideoComponent) ContentControl.Content).GetImageBounds();
                var uri = new UriBuilder(BaseUrl)
                {
                    Path = "axis-cgi/com/ptz.cgi",
                    Query =
                        $"areazoom={x},{y},{z}&imagewidth={resolution.Width}&imageheight={resolution.Height}&speed=100" //&imagerotation=0&resolution=1280x720"
                };
                _log?.Info($"{uri.Query}");
                var request = (HttpWebRequest) WebRequest.Create(uri.Uri);
                request.Credentials = new NetworkCredential("root", "pass");

                using (var response = await request.GetResponseAsync())
                {
                    IsRequestingPtz = false;
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
            finally
            {
                IsRequestingPtz = false;
            }
        }
    }
}