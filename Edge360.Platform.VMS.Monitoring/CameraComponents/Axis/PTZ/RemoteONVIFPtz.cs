﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using CameraService.Adapters.Shared.Enums;
using Edge360.Platform.VMS.Common.Enums.Camera;
using Edge360.Platform.VMS.Common.Interfaces.Camera;
using static Edge360.Platform.VMS.Communication.CommunicationManager;

namespace Edge360.Platform.VMS.Monitoring.CameraComponents.Axis.PTZ
{
    public class RemoteONVIFPtz : IPtzManager
    {
        public Guid CameraId { get; set; }
        private Point DefaultCoordinate => new Point(0, 0);
        private TimeSpan DefaultTimeout => TimeSpan.FromSeconds(3);

        public RemoteONVIFPtz(Guid cameraId)
        {
            CameraId = cameraId;
        }

        public int PanSpeed { get; set; }
        public int ZoomSpeed { get; set; }

        public async void ZoomRelease()
        {
            try
            {
                await GetManagerByZoneId().CameraAdapterService
                    .PtzAction((int) EPtzAction.ReleaseZoom, CameraId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async void PanRelease()
        {
            try
            {
                await GetManagerByZoneId().CameraAdapterService
                    .PtzAction((int) EPtzAction.ReleasePan, CameraId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task StopPtz()
        {
            try
            {
                await GetManagerByZoneId().CameraAdapterService
                    .PtzAction((int) EPtzAction.StopPtz, CameraId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task<bool> PanCamera(EPtzCommand command)
        {
            try
            {
                var coordinates = DefaultCoordinate;

                switch (command)
                {
                    case EPtzCommand.PanUp:
                        coordinates = new Point(0, PanSpeed);
                        break;
                    case EPtzCommand.PanUpLeft:
                        coordinates = new Point(-PanSpeed, PanSpeed);
                        break;
                    case EPtzCommand.PanUpRight:
                        coordinates = new Point(PanSpeed, PanSpeed);
                        break;
                    case EPtzCommand.PanDown:
                        coordinates = new Point(0, -PanSpeed);
                        break;
                    case EPtzCommand.PanDownLeft:
                        coordinates = new Point(-PanSpeed, -PanSpeed);
                        break;
                    case EPtzCommand.PanDownRight:
                        coordinates = new Point(PanSpeed, -PanSpeed);
                        break;
                    case EPtzCommand.PanLeft:
                        coordinates = new Point(-PanSpeed, 0);
                        break;
                    case EPtzCommand.PanRight:
                        coordinates = new Point(PanSpeed, 0);
                        break;
                }

                await GetManagerByZoneId().CameraAdapterService.PtzAction((int) EPtzAction.ContinuousPan,
                    CameraId, coordinates, panSpeed: PanSpeed);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return false;
        }

        public async Task<bool> ZoomCamera(EPtzCommand command)
        {
            try
            {
                var coordinates = new Point(0, 0);
                await GetManagerByZoneId().CameraAdapterService.PtzAction(
                    (int) EPtzAction.ContinuousZoom, CameraId, coordinates,
                    panSpeed: PanSpeed, zoomSpeed: ZoomSpeed, zoomIn: command == EPtzCommand.ZoomIn);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return false;
        }

        public async Task<bool> SetPreset(EPtzPreset preset)
        {
            try
            {
                var val = await GetManagerByZoneId().CameraAdapterService
                    .PtzAction((int) EPtzAction.GoPreset, CameraId, value: (int) preset);
                return val != null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return false;
        }

        public async Task Zoom(bool zoomIn, System.Windows.Point point, float zoomAmount)
        {
            try
            {
                //_log?.Info($"Zooming {zoomIn} {zoomAmount}");
                var action = (int) EPtzAction.RelativeZoom;
                if (zoomAmount == 0)
                {
                    action = (int) EPtzAction.ReleaseZoom;
                }
                var coordinates = new Point((int) point.X, (int) point.Y);
               
                //await GetManagerByZoneId().CameraAdapterService.PtzAction(
                //    action, CameraId, coordinates, value: 1,
                //    panSpeed: PanSpeed, zoomSpeed: ZoomSpeed, zoomIn: zoomIn);

                await GetManagerByZoneId().CameraAdapterService.PtzAction(
                    action, CameraId, coordinates, value: 1,
                    panSpeed: (int)(zoomAmount * 100), zoomSpeed: (int)(zoomAmount * 100), zoomIn: zoomIn);


                //await RelativeMoveAsync(
                //    new PtzVector
                //        {PanTilt = new Vector2D {X = 0, Y = 0}, Zoom = new Vector1D {Y = zoomIn ? zoomAmount : -zoomAmount}},
                //    new PtzSpeed
                //    {
                //        PanTilt = new Vector2D {X = 0, Y = 0},
                //        Zoom = new Vector1D {Y = 1f}
                //    });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task Zoom(bool zoomIn, System.Windows.Point point)
        {
            try
            {
                var coordinates = new Point((int) point.X, (int) point.Y);
                await GetManagerByZoneId().CameraAdapterService.PtzAction(
                    (int) EPtzAction.RelativeZoom, CameraId, coordinates, value: 1,
                    panSpeed: PanSpeed, zoomSpeed: ZoomSpeed, zoomIn: zoomIn);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async void ContinuousZoom(bool zoomIn, System.Windows.Point point)
        {
            try
            {
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task MaxZoomAsync(bool zoomIn)
        {
            try
            {
                var coordinates = new Point(0, 0);
                await GetManagerByZoneId().CameraAdapterService.PtzAction(
                    (int) EPtzAction.MaxZoom, CameraId, zoomIn: zoomIn);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async void AreaZoom(System.Windows.Point point, int widthZoom, int imageHeight, int imageWidth,
            int rectHeight, int rectWidth)
        {
            try
            {
                var val = await GetManagerByZoneId().CameraAdapterService.PtzAction(
                    (int) EPtzAction.AreaZoom, CameraId, imageSize: new Size(imageWidth, imageHeight),
                    rectSize: new Size(rectWidth, rectHeight), coordinates: new Point((int) point.X, (int) point.Y));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async void PtzCenter(System.Windows.Point point, int imageWidth, int imageHeight)
        {
            try
            {
                var val = await GetManagerByZoneId().CameraAdapterService.PtzAction(
                    (int) EPtzAction.CenterArea, CameraId, imageSize: new Size(imageWidth, imageHeight),
                    coordinates: new Point((int) point.X, (int) point.Y));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task<bool> PanCamera(System.Windows.Point point)
        {
            try
            {
                var continuousPan = (int) EPtzAction.ContinuousPan;
                if (point.X == 0 && point.Y == 0)
                {
                    continuousPan = (int) EPtzAction.ReleasePan;
                }
                await GetManagerByZoneId().CameraAdapterService.PtzAction(continuousPan,
                    CameraId, new Point((int)point.X, y: (int)point.Y), panSpeed: PanSpeed);
            }
            catch (Exception e)
            {
                
            }
            return default;
        }
    }
}