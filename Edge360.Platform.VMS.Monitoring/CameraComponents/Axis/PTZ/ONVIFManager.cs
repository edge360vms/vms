﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using CameraService.Adapters.ONVIF.Helpers;
using Edge360.Platform.VMS.Common.Enums.Camera;
using Edge360.Platform.VMS.Common.Interfaces.Camera;
using Edge360.Platform.VMS.UI.Common.Annotations;
using log4net;
using Mictlanix.DotNet.Onvif.Common;

namespace Edge360.Platform.VMS.Monitoring.CameraComponents.Axis.PTZ
{
    public class ONVIFManager : IPtzManager, INotifyPropertyChanged
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private OnvifCameraConnectionItemModel _onvifCameraConnection;
        private TimeSpan _ptzTimeout;

        public static ConcurrentDictionary<Guid, OnvifCameraConnectionItemModel> OnvifConnections { get; set; } =
            new ConcurrentDictionary<Guid, OnvifCameraConnectionItemModel>();

        public PTZSpeed MaxSpeed =>
            new PTZSpeed {PanTilt = new Vector2D {x = 1f, y = 1f}, Zoom = new Vector1D {x = 1f}};


        public OnvifCameraConnectionItemModel OnvifCameraConnection
        {
            get => _onvifCameraConnection;
            set
            {
                if (Equals(value, _onvifCameraConnection))
                {
                    return;
                }

                _onvifCameraConnection = value;
                OnPropertyChanged();
            }
        }

        public TimeSpan PtzTimeout
        {
            get => _ptzTimeout;
            set
            {
                if (value.Equals(_ptzTimeout))
                {
                    return;
                }

                _ptzTimeout = value;
                OnPropertyChanged();
            }
        }

        public string PTZTimeoutXML
        {
            get
            {
                string timeoutXml = null;
                var timeoutSeconds = PtzTimeout.TotalSeconds;

                if (timeoutSeconds > 0)
                {
                    timeoutXml = $"PT{PtzTimeout.TotalSeconds.ToString(CultureInfo.InvariantCulture)}S";
                }

                return timeoutXml;
            }
        }

        public ONVIFManager(string host, string username = "", string password = "")
        {
            try
            {
                Task.Run(() =>
                {
                    ServicePointManager.Expect100Continue = false;
                    var model = new OnvifCameraConnectionItemModel
                        {Hostname = host, Password = password, Username = username};
                    var existing = OnvifConnections.Values.FirstOrDefault(o =>
                        OnvifCameraConnectionItemModel.HostnameUsernamePasswordComparer.Equals(o, model
                        ));
                    if (existing == null)
                    {
                        OnvifConnections.TryAdd(Guid.NewGuid(), model);
                        model.InitializeConnections();
                    }

                    OnvifCameraConnection = existing ?? model;
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public int PanSpeed { get; set; }
        public int ZoomSpeed { get; set; }

        public async void ZoomRelease()
        {
            try
            {
                await OnvifCameraConnection.PtzClient.StopAsync(OnvifCameraConnection.Token, false, true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async void PanRelease()
        {
            try
            {
                await OnvifCameraConnection.PtzClient.StopAsync(OnvifCameraConnection.Token, true, false);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task StopPtz()
        {
            try
            {
                await OnvifCameraConnection.PtzClient.StopAsync(OnvifCameraConnection.Token, true, true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task<bool> PanCamera(EPtzCommand command)
        {
            try
            {
                if (OnvifCameraConnection.PtzClient == null)
                {
                    return false;
                }

                switch (command)
                {
                    case EPtzCommand.PanUp:
                        await OnvifCameraConnection.PtzClient.ContinuousMoveAsync(OnvifCameraConnection.Token,
                            new PTZSpeed
                            {
                                PanTilt = new Vector2D {x = 0, y = (float) PanSpeed / 100},
                                Zoom = new Vector1D()
                            }, PTZTimeoutXML);
                        break;
                    case EPtzCommand.PanUpLeft:
                        await OnvifCameraConnection.PtzClient.ContinuousMoveAsync(OnvifCameraConnection.Token,
                            new PTZSpeed
                            {
                                PanTilt = new Vector2D {x = -((float) PanSpeed / 100), y = (float) PanSpeed / 100},
                                Zoom = new Vector1D()
                            }, PTZTimeoutXML);
                        break;
                    case EPtzCommand.PanUpRight:
                        await OnvifCameraConnection.PtzClient.ContinuousMoveAsync(OnvifCameraConnection.Token,
                            new PTZSpeed
                            {
                                PanTilt = new Vector2D {x = (float) PanSpeed / 100, y = (float) PanSpeed / 100},
                                Zoom = new Vector1D()
                            }, PTZTimeoutXML);
                        break;
                    case EPtzCommand.PanDown:
                        await OnvifCameraConnection.PtzClient.ContinuousMoveAsync(OnvifCameraConnection.Token,
                            new PTZSpeed
                            {
                                PanTilt = new Vector2D {x = 0, y = -((float) PanSpeed / 100)},
                                Zoom = new Vector1D()
                            }, PTZTimeoutXML);
                        break;
                    case EPtzCommand.PanDownLeft:
                        await OnvifCameraConnection.PtzClient.ContinuousMoveAsync(OnvifCameraConnection.Token,
                            new PTZSpeed
                            {
                                PanTilt = new Vector2D {x = -((float) PanSpeed / 100), y = -((float) PanSpeed / 100)},
                                Zoom = new Vector1D()
                            }, PTZTimeoutXML);
                        break;
                    case EPtzCommand.PanDownRight:
                        await OnvifCameraConnection.PtzClient.ContinuousMoveAsync(OnvifCameraConnection.Token,
                            new PTZSpeed
                            {
                                PanTilt = new Vector2D {x = (float) PanSpeed / 100, y = -((float) PanSpeed / 100)},
                                Zoom = new Vector1D()
                            }, PTZTimeoutXML);
                        break;
                    case EPtzCommand.PanLeft:
                        await OnvifCameraConnection.PtzClient.ContinuousMoveAsync(OnvifCameraConnection.Token,
                            new PTZSpeed
                            {
                                PanTilt = new Vector2D {x = -((float) PanSpeed / 100), y = 0},
                                Zoom = new Vector1D()
                            }, PTZTimeoutXML);
                        break;
                    case EPtzCommand.PanRight:
                        await OnvifCameraConnection.PtzClient.ContinuousMoveAsync(OnvifCameraConnection.Token,
                            new PTZSpeed
                            {
                                PanTilt = new Vector2D {x = (float) PanSpeed / 100, y = 0},
                                Zoom = new Vector1D()
                            }, PTZTimeoutXML);
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                OnvifCameraConnection.PtzClient = null;
            }


            return true;
        }

        public async Task<bool> ZoomCamera(EPtzCommand command)
        {
            try
            {
                if (OnvifCameraConnection.PtzClient == null)
                {
                    return false;
                }

                switch (command)
                {
                    case EPtzCommand.ZoomIn:
                        await OnvifCameraConnection.PtzClient.ContinuousMoveAsync(OnvifCameraConnection.Token,
                            new PTZSpeed
                            {
                                PanTilt = new Vector2D(),
                                Zoom = new Vector1D {x = (float) ZoomSpeed / 100}
                            }, PTZTimeoutXML);
                        break;
                    case EPtzCommand.ZoomOut:
                        await OnvifCameraConnection.PtzClient.ContinuousMoveAsync(OnvifCameraConnection.Token,
                            new PTZSpeed
                            {
                                PanTilt = new Vector2D(),
                                Zoom = new Vector1D {x = -((float) ZoomSpeed / 100)}
                            }, PTZTimeoutXML);
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


            return true;
        }

        public async Task<bool> SetPreset(EPtzPreset preset)
        {
            if (OnvifCameraConnection.PtzClient == null)
            {
                return false;
            }

            switch (preset)
            {
                case EPtzPreset.Home:
                    await OnvifCameraConnection.PtzClient.GotoHomePositionAsync(OnvifCameraConnection.Token, MaxSpeed);
                    break;
                case EPtzPreset.Preset1:
                case EPtzPreset.Preset2:
                case EPtzPreset.Preset3:
                case EPtzPreset.Preset4:
                case EPtzPreset.Preset5:
                case EPtzPreset.Preset6:
                case EPtzPreset.Preset7:
                case EPtzPreset.Preset8:
                case EPtzPreset.Preset9:
                case EPtzPreset.Preset10:
                    OnvifCameraConnection.CameraPresets ??=
                        await OnvifCameraConnection.PtzClient.GetPresetsAsync(OnvifCameraConnection.Token);
                    await OnvifCameraConnection.PtzClient.GotoPresetAsync(OnvifCameraConnection.Token,
                        OnvifCameraConnection.CameraPresets.Preset[(int) preset - 1].token, MaxSpeed);
                    break;
            }

            return true;
        }

        public async Task Zoom(bool zoomIn, Point point, float zoomAmount = .016f)
        {
            try
            {
                _log?.Info($"Zooming {zoomIn} {zoomAmount}");
                await OnvifCameraConnection.PtzClient.RelativeMoveAsync(OnvifCameraConnection.Token,
                    new PTZVector
                    {
                        PanTilt = new Vector2D {x = 0, y = 0},
                        Zoom = new Vector1D {x = zoomIn ? zoomAmount : -zoomAmount}
                    },
                    new PTZSpeed
                    {
                        PanTilt = new Vector2D {x = 0, y = 0},
                        Zoom = new Vector1D {x = 1f}
                    });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async void ContinuousZoom(bool zoomIn, Point point)
        {
            try
            {
                await OnvifCameraConnection.PtzClient.ContinuousMoveAsync(OnvifCameraConnection.Token,
                    new PTZSpeed
                    {
                        PanTilt = new Vector2D {x = (float) point.X, y = (float) point.Y},
                        Zoom = new Vector1D {x = 1f}
                    },
                    PTZTimeoutXML);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task MaxZoomAsync(bool zoomIn)
        {
            try
            {
                await OnvifCameraConnection.PtzClient.RelativeMoveAsync(OnvifCameraConnection.Token,
                    new PTZVector {PanTilt = new Vector2D(), Zoom = new Vector1D {x = zoomIn ? 1 : -1f}},
                    new PTZSpeed
                    {
                        PanTilt = new Vector2D {x = 0, y = 0},
                        Zoom = new Vector1D {x = 1f}
                    });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async void AreaZoom(Point point, int widthZoom, int imageHeight, int imageWidth,
            int rectHeight,
            int rectWidth)
        {
            try
            {


                await AreaZoomAsync(imageHeight, imageWidth, rectHeight, rectWidth, point.X,
                    point.Y); //, rectWidth / 2, rectHeight / 2);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async void PtzCenter(Point point, int imageWidth, int imageHeight)
        {
            try
            {
                await CenterMoveAsync(imageWidth, imageHeight, point.X, point.Y);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static PTZVector GetCenterMoveVector(
            double imageWidth,
            double imageHeight,
            double x,
            double y,
            double zoom,
            string space = null)
        {
            double num1 = imageWidth / 2.0;
            double num2 = imageHeight / 2.0;
            double num3 = x - num1;
            double num4 = num2 - y;
            double num5 = num1;
            double num6 = num3 / num5;
            double num7 = num4 / num2;
            return new PTZVector()
            {
                PanTilt = new Vector2D()
                {
                    x = (float) num6,
                    y = (float) num7,
                    space = space
                },
                Zoom = new Vector1D() { x = (float) zoom }
            };
        }
    

        public async Task<bool> PanCamera(Point point)
        {
            try
            {
                _log?.Info($"Panning {point}");
                await OnvifCameraConnection.PtzClient.ContinuousMoveAsync(OnvifCameraConnection.Token, new PTZSpeed
                {
                    PanTilt = new Vector2D {x = (float) point.X / 100, y = (float) point.Y / 100},
                    Zoom = new Vector1D()
                }, PTZTimeoutXML);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        public static PTZSpeed CreateSpeed(int panSpeed, int zoomSpeed)
        {
            return new PTZSpeed
            {
                PanTilt = new Vector2D
                {
                    x = (float) panSpeed / 100,
                    y = (float) panSpeed / 100
                },
                Zoom = new Vector1D
                {
                    x = (float) zoomSpeed / 100
                }
            };
        }

        public async Task RefreshPresets()
        {
            try
            {
                var cancellationToken = new CancellationTokenSource();
                cancellationToken.CancelAfter(TimeSpan.FromSeconds(5));
                await Task.Run(async () =>
                {
                    try
                    {
                        while (OnvifCameraConnection == null || OnvifCameraConnection.PtzClient == null)
                        {
                            await Task.Delay(100);
                        }

                        OnvifCameraConnection.CameraPresets =
                            await OnvifCameraConnection.PtzClient.GetPresetsAsync(OnvifCameraConnection.Token);
                    }
                    catch (Exception e)
                    {
                        
                    }
                }, cancellationToken.Token);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task CenterMoveAsync(double imageWidth, double imageHeight, double x, double y)
        {
            if (OnvifCameraConnection.ConfigurationOptions == null)
            {
                await OnvifCameraConnection.GetConfigurations();
            }
            var space = OnvifCameraConnection.ConfigurationOptions?.Spaces?.RelativePanTiltTranslationSpace[2]?.URI;
            var vector = PtzUtility.GetCenterMoveVector(imageWidth, imageHeight, x, y, 0, space);

            var speed = new PTZSpeed
            {
                PanTilt = new Vector2D
                {
                    x = 1f,
                    y = 1f
                },
                Zoom = new Vector1D
                {
                    x = 0
                }
            };

            await OnvifCameraConnection.PtzClient.RelativeMoveAsync(OnvifCameraConnection.Token, vector, speed);
        }

        public async Task AreaZoomAsync(double imageHeight, double imageWidth, double rectHeight, double rectWidth,
            double rectCenterX, double rectCenterY)
        {
            try
            {
                if (OnvifCameraConnection.ConfigurationOptions == null)
                {
                    await OnvifCameraConnection.GetConfigurations();
                }
                //await CenterMoveAsync(imageWidth, imageHeight, rectCenterX, rectCenterY);

                var imagePerim = 2 * imageWidth + 2 * imageHeight;
                var rectPerim = 2 * rectWidth + 2 * rectHeight;

                var perimeterDifference = imagePerim - rectPerim;
                var perimeterRatio = perimeterDifference / imagePerim;

                const double offset = 0.575;
                var adjustedPerimeterRatio = perimeterRatio - offset;

                var zoomLimit = OnvifCameraConnection.CompatibleConfigurations.PTZConfiguration[0].ZoomLimits.Range
                    .XRange
                    .Max;
                var space = OnvifCameraConnection.ConfigurationOptions?.Spaces?.RelativePanTiltTranslationSpace[2]?.URI;

                var zoom = (float) (adjustedPerimeterRatio * zoomLimit);

                var vector =
                    PtzUtility.GetCenterMoveVector(imageWidth, imageHeight, rectCenterX, rectCenterY, zoom, space);

                var speed = new PTZSpeed
                {
                    PanTilt = new Vector2D
                    {
                        x = 1f,
                        y = 1f
                    },
                    Zoom = new Vector1D
                    {
                        x = 1f
                    }
                };

                await OnvifCameraConnection.PtzClient.RelativeMoveAsync(OnvifCameraConnection.Token, vector, speed);
            }
            catch (Exception e)
            {
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}