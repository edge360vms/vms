﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Common.Annotations;

namespace Edge360.Platform.VMS.Monitoring.Managers.DirectMode
{
    public class DirectModeManager : INotifyPropertyChanged
    {
        //public DirectModeManager()
        //{
        //    if (Instance == null)
        //    {
        //        Instance = this;
        //    }
        //}
        //public static DirectModeManager Instance { get; set; }

        public bool IsOfflineMode
        {
            get => _isOfflineMode;
            set
            {
                if (value == _isOfflineMode) return;
                _isOfflineMode = value;
                IsDirectMode = _isOfflineMode;

                OnPropertyChanged();
            }
        }
        public bool IsUserCacheLoaded
        {
            get => _isUserCacheLoaded;
            set
            {
                if (value == _isUserCacheLoaded) return;
                _isUserCacheLoaded = value;
                OnPropertyChanged();
            }
        }

        public bool IsCameraCacheLoaded
        {
            get => _isCameraCacheLoaded;
            set
            {
                if (value == _isCameraCacheLoaded) return;
                _isCameraCacheLoaded = value;
                OnPropertyChanged();
            }
        }

        public bool IsLayoutRecoveryCacheLoaded
        {
            get => _isLayoutRecoveryCacheLoaded;
            set
            {
                if (value == _isLayoutRecoveryCacheLoaded) return;
                _isLayoutRecoveryCacheLoaded = value;
                OnPropertyChanged();
            }
        }

        public bool IsLayoutCacheLoaded
        {
            get => _isLayoutCacheLoaded;
            set
            {
                if (value == _isLayoutCacheLoaded) return;
                _isLayoutCacheLoaded = value;
                OnPropertyChanged();
            }
        }

        public bool IsNodeCacheLoaded
        {
            get => _isNodeCacheLoaded;
            set
            {
                if (value == _isNodeCacheLoaded) return;
                _isNodeCacheLoaded = value;
                OnPropertyChanged();
            }
        }

        private bool _isDirectMode;
        private bool _isOfflineMode;
        private bool _isNodeCacheLoaded;
        private bool _isUserCacheLoaded;
        private bool _isCameraCacheLoaded;
        private bool _isLayoutCacheLoaded;
        private bool _isLayoutRecoveryCacheLoaded;
        private bool _isDirectOnFailedPlaybackEnabled;
        public event Action<bool> DirectModeChanged;
        public bool IsDirectMode
        {
            get => _isDirectMode;
            set
            {
                if (value == _isDirectMode)
                {
                    return;
                }

                _isDirectMode = value;
               
                OnPropertyChanged();
                DirectModeChanged?.Invoke(value);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}