﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using log4net;

namespace Edge360.Platform.VMS.Common.Util
{
    public static class ThreadUtil
    {
        static ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //public static async Task<bool> WpfUiThreadAsync(this FrameworkElement e, Action a,
        //    DispatcherPriority priority = DispatcherPriority.Normal)
        //{
        //    if (e == null || a == null || e.Dispatcher == null)
        //    {
        //        return false;
        //    }

        //    try
        //    {
        //        await e?.Dispatcher?.BeginInvoke(a, priority);
        //    }
        //    catch (Exception exception)
        //    {
        //        _log?.Info(exception);
        //    }

        //    return true;
        //}

        private static void WpfUiThread(this FrameworkElement e, Action a,
            DispatcherPriority priority = DispatcherPriority.Normal)
        {
            if (e == null || a == null || e.Dispatcher == null)
            {
                return;
            }

            try
            {
                e.Dispatcher.BeginInvoke(a, priority);
            }
            catch (Exception exception)
            {
                _log?.Info(exception);
            }
        }
    }
}