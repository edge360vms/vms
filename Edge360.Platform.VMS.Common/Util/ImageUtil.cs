﻿#region

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using PixelFormat = System.Drawing.Imaging.PixelFormat;
using Point = System.Drawing.Point;

#endregion

namespace Edge360.Platform.VMS.Common.Util
{
    public static class ImageUtil
    {
        public enum TernaryRasterOperations : uint
        {
            /// <summary>dest = source</summary>
            Srccopy = 0x00CC0020,

            /// <summary>dest = source OR dest</summary>
            Srcpaint = 0x00EE0086,

            /// <summary>dest = source AND dest</summary>
            Srcand = 0x008800C6,

            /// <summary>dest = source XOR dest</summary>
            Srcinvert = 0x00660046,

            /// <summary>dest = source AND (NOT dest)</summary>
            Srcerase = 0x00440328,

            /// <summary>dest = (NOT source)</summary>
            Notsrccopy = 0x00330008,

            /// <summary>dest = (NOT src) AND (NOT dest)</summary>
            Notsrcerase = 0x001100A6,

            /// <summary>dest = (source AND pattern)</summary>
            Mergecopy = 0x00C000CA,

            /// <summary>dest = (NOT source) OR dest</summary>
            Mergepaint = 0x00BB0226,

            /// <summary>dest = pattern</summary>
            Patcopy = 0x00F00021,

            /// <summary>dest = DPSnoo</summary>
            Patpaint = 0x00FB0A09,

            /// <summary>dest = pattern XOR dest</summary>
            Patinvert = 0x005A0049,

            /// <summary>dest = (NOT dest)</summary>
            Dstinvert = 0x00550009,

            /// <summary>dest = BLACK</summary>
            Blackness = 0x00000042,

            /// <summary>dest = WHITE</summary>
            Whiteness = 0x00FF0062,

            /// <summary>
            ///     Capture window as seen on screen.  This includes layered windows
            ///     such as WPF windows with AllowsTransparency="true"
            /// </summary>
            Captureblt = 0x40000000
        }

        private const int WmSetredraw = 0x000B;

        public static Bitmap Base64StringToBitmap(this string
            base64String)
        {
            Bitmap bmpReturn = null;


            var byteBuffer = Convert.FromBase64String(base64String);
            var memoryStream = new MemoryStream(byteBuffer);


            memoryStream.Position = 0;


            bmpReturn = (Bitmap) Image.FromStream(memoryStream);


            memoryStream.Close();


            return bmpReturn;
        }

        public static Bitmap Screenshot(this Control c)
        {
            //using (var bmp = new Bitmap(c.Width, c.Height))
            //{
            //    using (var g = Graphics.FromImage(bmp))
            //    {
            //        g.CopyFromScreen(c.PointToScreen(c.Location), new Point(0, 0), c.Size);
            //        return bmp;
            //    }
            //}


            try
            {
                var controlBitMap = new Bitmap(c.Width, c.Height);
                var g = Graphics.FromImage(controlBitMap);
                g.CopyFromScreen(c.PointToScreen(c.Location), new Point(0, 0), c.Size);
                return controlBitMap;
            }
            catch (Exception)
            {
            }

            return default;
        }


        public static Bitmap BitmapFromSource(this BitmapSource bitmapsource)
        {
            if (bitmapsource == null)
            {
                return null;
            }

            Bitmap bitmap;
            using (var outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new Bitmap(outStream);
            }

            return bitmap;
        }


        public static Bitmap GetBitmapFromSource(this BitmapSource source)
        {
            if (source == null)
            {
                return null;
            }

            var bmp = new Bitmap(
                source.PixelWidth,
                source.PixelHeight,
                PixelFormat.Format32bppPArgb);
            var data = bmp.LockBits(
                new Rectangle(Point.Empty, bmp.Size),
                ImageLockMode.WriteOnly,
                PixelFormat.Format32bppPArgb);
            source.CopyPixels(
                Int32Rect.Empty,
                data.Scan0,
                data.Height * data.Stride,
                data.Stride);
            bmp.UnlockBits(data);
            return bmp;
        }

        public static BitmapSource BitmapToSource(Bitmap bitmap)
        {
            if (bitmap != null && !bitmap.Size.IsEmpty)
            {
                var bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                    ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

                var bitmapSource = BitmapSource.Create(
                    bitmapData.Width, bitmapData.Height,
                    bitmap.HorizontalResolution, bitmap.VerticalResolution,
                    PixelFormats.Bgra32, null,
                    bitmapData.Scan0, bitmapData.Stride * bitmapData.Height, bitmapData.Stride);

                bitmap.UnlockBits(bitmapData);
                return bitmapSource;
            }

            return null;
        }


        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetDC(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern int ReleaseDC(IntPtr hWnd, IntPtr hDc);


        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool BitBlt(IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc,
            int nXSrc, int nYSrc, TernaryRasterOperations dwRop);


        public static RenderTargetBitmap SnapshotRenderTargetBitmap(this UIElement c, int dpiX = 96, int dpiY = 96)
        {
            try
            {
                if (c == null)
                {
                    return null;
                }

                var bounds = VisualTreeHelper.GetDescendantBounds(c);
                var rtb = new RenderTargetBitmap((int) (bounds.Width * dpiX / 96.0),
                    (int) (bounds.Height * dpiY / 96.0),
                    dpiX,
                    dpiY,
                    PixelFormats.Pbgra32);
                var dv = new DrawingVisual();
                using (var ctx = dv.RenderOpen())
                {
                    var vb = new VisualBrush(c);
                    ctx.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), bounds.Size));
                }

                rtb.Render(dv);
                return rtb;
            }
            catch (Exception)
            {
                return default;
            }
        }

        public static Bitmap Snapshot(this Control c)
        {
            if (c == null || c.IsDisposed)
            {
                return null;
            }

            int width = 0, height = 0;
            var hwnd = IntPtr.Zero;
            var dc = IntPtr.Zero;
            c.Invoke(new MethodInvoker(() =>
            {
                width = c.ClientSize.Width;
                height = c.ClientSize.Height;
                hwnd = c.Handle;
                dc = GetDC(hwnd);
            }));
            var bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            if (dc != IntPtr.Zero)
            {
                try
                {
                    using (var g = Graphics.FromImage(bmp))
                    {
                        var bdc = g.GetHdc();
                        try
                        {
                            BitBlt(bdc, 0, 0, width, height, dc, 0, 0, TernaryRasterOperations.Captureblt);
                        }
                        finally
                        {
                            g.ReleaseHdc(bdc);
                        }
                    }
                }
                finally
                {
                    ReleaseDC(hwnd, dc);
                }
            }

            return bmp;
        }


        public static BitmapSource SnapshotRenderTargetBitmapPng(this UIElement c, int dpiX = 96, int dpiY = 96)
        {
            try
            {
                if (c == null)
                {
                    return null;
                }

                var renderTargetBitmap =
                    new RenderTargetBitmap((int) c.RenderSize.Width, (int) c.RenderSize.Height, 96, 96,
                        PixelFormats.Pbgra32);
                renderTargetBitmap.Render(c);
                var pngImage = new PngBitmapEncoder();
                pngImage.Frames.Add(BitmapFrame.Create(renderTargetBitmap));

                return pngImage.Frames[0].Clone();
            }
            catch (Exception)
            {
                return default;
            }
        }

        public static Bitmap Snapshot(this UIElement c, IntPtr windowHandle)
        {
            if (c == null)
            {
                return null;
            }

            int width = 0, height = 0;
            var hwnd = IntPtr.Zero;
            var dc = IntPtr.Zero;
            //  c.WpfUiThread((() =>
            //   {
            width = (int) c.RenderSize.Width;
            height = (int) c.RenderSize.Height;
            hwnd = windowHandle;
            dc = GetDC(hwnd);
            //   }));
            var bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            if (dc != IntPtr.Zero)
            {
                try
                {
                    using (var g = Graphics.FromImage(bmp))
                    {
                        var bdc = g.GetHdc();
                        try
                        {
                            BitBlt(bdc, 0, 0, width, height, dc, 0, 0, TernaryRasterOperations.Srccopy);
                        }
                        finally
                        {
                            g.ReleaseHdc(bdc);
                        }
                    }
                }
                finally
                {
                    ReleaseDC(hwnd, dc);
                }
            }

            return bmp;
        }

        //public static void UiThread(this Control c, Action a)
        //{
        //    if (a == null || c == null || c.IsDisposed)
        //    {
        //        return;
        //    }

        //    try
        //    {
        //        if (c.InvokeRequired)
        //        {
        //            c.Invoke(a);
        //        }
        //        else
        //        {
        //            a();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        _log?.Info(e);
        //    }
        //}
    }
}