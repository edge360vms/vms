﻿using System.Reflection;
using System.Windows.Input;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Edge360.Platform.VMS.Common.Util
{
    public static class JsonUtil
    {
        public static JsonSerializerSettings IgnoreExceptionSettings()
        {
            return new JsonSerializerSettings
            {
                ContractResolver = ShouldSerializeContractResolver.Instance,
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore,
                Error = (sender, args) => args.ErrorContext.Handled = true
            };
        }


    }

    public class ShouldSerializeContractResolver : DefaultContractResolver
    {
        public static readonly ShouldSerializeContractResolver Instance =
            new ShouldSerializeContractResolver();

        protected override JsonProperty CreateProperty(MemberInfo member,
            MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);

            if (property.PropertyType?.BaseType == typeof (ICommand) || property.PropertyType?.BaseType == typeof(ILog))
            {
                property.ShouldSerialize = instance => { return false; };
            }

            return property;
        }
    }

}