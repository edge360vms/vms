﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace Edge360.Platform.VMS.Common.Util
{
    public static class FileUtil
    {
        [DllImport("Shlwapi.dll", CharSet = CharSet.Auto)]
        public static extern int StrFormatByteSize(
            long fileSize,
            [MarshalAs(UnmanagedType.LPTStr)] StringBuilder buffer,
            int bufferSize);

        // Return a file size created by the StrFormatByteSize API function.
        public static string ToFileSizeApi(this long file_size)
        {
            var sb = new StringBuilder(20);
            StrFormatByteSize(file_size, sb, 20);
            return sb.ToString();
        }

        public static bool IsFileLocked(this FileInfo fileInfo)
        {
            try
            {
                using (var stream = fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }
    }

    public static class TimeUtil
    {
        public static DateTime? FixServiceStackTime(this DateTime? time)
        {
            if (time != null)
            {
                return DateTime.SpecifyKind(time.Value.ToLocalTime(), DateTimeKind.Utc);
            }

            return default;
        }

        public static string PeriodOfTimeOutput(this TimeSpan tspan, int level = 0)
        {
            var str = "ago";
            if (level >= 2)
            {
                return str;
            }

            if (tspan.Days > 1)
            {
                str = $"{tspan.Days}d ago";
            }
            else if (tspan.Days == 1)
            {
                str =
                    $"1d {PeriodOfTimeOutput(new TimeSpan(tspan.Hours, tspan.Minutes, tspan.Seconds), level + 1)}";
            }
            else if (tspan.Hours >= 1)
            {
                str =
                    $"{tspan.Hours}{(tspan.Hours > 1 ? "h" : "h")} {PeriodOfTimeOutput(new TimeSpan(0, tspan.Minutes, tspan.Seconds), level + 1)}";
            }
            else if (tspan.Minutes >= 1)
            {
                str =
                    $"{tspan.Minutes}{(tspan.Minutes > 1 ? "m" : "m")} {PeriodOfTimeOutput(new TimeSpan(0, 0, tspan.Seconds), level + 1)}";
            }
            else if (tspan.Seconds >= 1)
            {
                str = $"{tspan.Seconds}{(tspan.Seconds > 1 ? "s" : "s")} ago";
            }

            return str;
        }

        public static string GetTimezoneAbbreviation(TimeZoneInfo timeZoneName)
        {
            var timeZone = timeZoneName.IsDaylightSavingTime(DateTime.Now)
                ? timeZoneName.DaylightName
                : timeZoneName.StandardName;
            var output = string.Empty;

            var timeZoneWords = timeZone.Split(' ');
            foreach (var timeZoneWord in timeZoneWords)
            {
                if (timeZoneWord[0] != '(')
                {
                    output += timeZoneWord[0];
                }
                else
                {
                    output += timeZoneWord;
                }
            }

            return output;
        }
    }
}