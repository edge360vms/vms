﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Common.Properties;

namespace Edge360.Platform.VMS.Common.Settings
{
    [Serializable]
    public class GeneralSettings : INotifyPropertyChanged
    {
        private bool _openMonitoringWindowOnStartup = true;

        [DisplayName("Open Monitoring On Startup")]
        public bool OpenMonitoringWindowOnStartup
        {
            get => _openMonitoringWindowOnStartup;
            set
            {
                if (_openMonitoringWindowOnStartup != value)
                {
                    _openMonitoringWindowOnStartup = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}