﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Edge360.Platform.VMS.Common.Settings
{
    [Serializable]
    public class CameraSettings
    {
        private int? _directPlaybackFailureTimeout = 15;
        private bool? _isDirectOnFailedPlaybackEnabled = true;

        [DisplayName("Direct Mode on LivePlayback Failure (Seconds)")]
        public bool? IsDirectOnFailedPlaybackEnabled
        {
            get => _isDirectOnFailedPlaybackEnabled;
            set => _isDirectOnFailedPlaybackEnabled = value;
        }

        [DisplayName("LivePlayback Failure Timeout")]
        public int? DirectPlaybackFailureTimeout
        {
            get => _directPlaybackFailureTimeout;
            set => _directPlaybackFailureTimeout = value;
        }

        [DisplayName("Detected Bitrate Failure Reconnect Count")]
        public int MaxBitrateMatchingFallbackValue { get; set; } = 5;

        [DisplayName("Reconnect from DirectMode Interval (Online Mode Required)")]
        public int ReconnectFromDirectModeInterval { get; set; } = 20;
    }
}