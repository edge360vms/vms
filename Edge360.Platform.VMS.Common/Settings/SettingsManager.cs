﻿using System;
using System.Diagnostics;
using System.IO;
using log4net;
using Newtonsoft.Json;

namespace Edge360.Platform.VMS.Common.Settings
{
    public static class SettingsManager
    {
        static ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static readonly string AppDataPath =
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

        public static readonly string VMSAppDataPath =
            Path.Combine(AppDataPath, @"E360\VMS\");

        public static AppSettings Settings { get; set; }

        public static void LoadSettings()
        {
            try
            {
                var cachePath = Path.Combine(VMSAppDataPath, "cache");
                if (!Directory.Exists(cachePath))
                {
                    Directory.CreateDirectory(cachePath);
                }
                Settings = Load<AppSettings>("settings.json") ?? new AppSettings();
            }
            catch (Exception e)
            {
                
            }
        }

        private static T Load<T>(string fileName)
        {
            try
            {
                if (CheckDirectoryExists())
                {
                    var file = File.ReadAllText(Path.Combine(VMSAppDataPath, fileName));
                    if (string.IsNullOrEmpty(file))
                    {
                        return default;
                    }

                    var bookmarks = JsonConvert.DeserializeObject<T>(file);
                    return bookmarks;
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }

            return default;
        }

        private static bool CheckDirectoryExists()
        {
            try
            {
                var appDirectory = Directory.Exists(VMSAppDataPath);
                if (!appDirectory)
                {
                    Directory.CreateDirectory(VMSAppDataPath);
                }

                return true;
            }
            catch (Exception e)
            {
                _log?.Info(e);
                return false;
            }
        }

        public static void ExitingApplication()
        {

            if (!Settings.Login.Credentials.RememberMe)
            {
                try
                {
                    Settings.Login.Credentials.RefreshToken = string.Empty;
                    Settings.Login.Credentials.BearerToken = string.Empty;
                    Settings.Login.Credentials.PasswordLength = 0;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            else
            {
                try
                {
                  
                }
                catch (Exception e)
                {
                    
                }
            }

            SaveSettings();
        }

        public static void SaveSettings()
        {
            try
            {
                Save("settings.json", JsonConvert.SerializeObject(Settings, Formatting.Indented));
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        private static void Save(string fileName, string output)
        {
            try
            {
                CheckDirectoryExists();

                File.WriteAllText(Path.Combine(VMSAppDataPath, fileName), output);
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }
    }
}