﻿using System;
using System.ComponentModel;
using Edge360.Platform.VMS.Common.Annotations;

namespace Edge360.Platform.VMS.Common.Settings
{
    [Serializable]
    public class AppSettings
    {
        public AdvancedSettings Advanced { get; set; } = new AdvancedSettings();
        public AnalyticSettings Analytics { get; set; } = new AnalyticSettings();
        public CameraSettings Camera { get; set; } = new CameraSettings();
        public ExportSettings Exporting { get; set; } = new ExportSettings();
        public GeneralSettings General { get; set; } = new GeneralSettings();
        public LoginSettings Login { get; set; } = new LoginSettings();
        public RecoverySettings Recovery { get; set; } = new RecoverySettings();
        public MiscellaneousSettings Miscellaneous { get; set; } = new MiscellaneousSettings();
        public MonitorSettings Monitoring { get; set; } = new MonitorSettings();
    }

    public class RecoverySettings
    {
        private int? _maximumLayoutCacheAge = 10;
        private bool? _isRecoveryDisabled = true;

        [DisplayName("Maximum Layout Cache Age (Minutes)")]
        [CanBeNull]
        public int? MaximumLayoutCacheAge
        {
            get => _maximumLayoutCacheAge;
            set => _maximumLayoutCacheAge = value;
        }

        [DisplayName("Disable Layout Recovery")]
        [CanBeNull]
        public bool? IsRecoveryDisabled
        {
            get => _isRecoveryDisabled;
            set => _isRecoveryDisabled = value;
        }
    }
}