﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Common.Properties;

namespace Edge360.Platform.VMS.Common.Settings
{
    [Serializable]
    public class MiscellaneousSettings : INotifyPropertyChanged
    {
        private bool _minimizeToTray;
        private bool _startWithWindows;

        [DisplayName("Allow Multiple Application Instances")]
        public bool IsMultiInstanceEnabled { get; set; }

        [DisplayName("Enable Audit Tool Viewer")]
        public bool UseAuditTool { get; set; } = true;

        [Browsable(false)] public string UseControlBorder { get; set; }

        [DisplayName("Enable Health Monitoring Meter")]
        public bool UseHealthMonitoring { get; set; } = true;

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return
                $"{nameof(UseControlBorder)}: {UseControlBorder}, {nameof(MinimizeToTray)}: {MinimizeToTray}, {nameof(StartWithWindows)}: {StartWithWindows}";
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region DisabledArrangement

        [DisplayName("Disable Hardware Acceleration")]
        public bool DisableHardwareAcceleration { get; set; }

        [Browsable(false)]
        public bool MinimizeToTray
        {
            get => _minimizeToTray;
            set
            {
                if (_minimizeToTray != value)
                {
                    _minimizeToTray = value;
                    OnPropertyChanged();
                }
            }
        }

        [DisplayName("Disable Animations")] public bool DisableAnimations { get; set; }

        [Browsable(false)]
        public bool StartWithWindows
        {
            get => _startWithWindows;
            set
            {
                if (_startWithWindows != value)
                {
                    _startWithWindows = value;
                    OnPropertyChanged();
                }
            }
        }

        #endregion
    }
}