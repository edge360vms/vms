﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Common.Properties;
using Newtonsoft.Json;

namespace Edge360.Platform.VMS.Common.Settings
{
    [Serializable]
    public class AnalyticSettings : INotifyPropertyChanged
    {
        [JsonIgnore] private int _lprRefreshInterval;

        private string _lprServerAddress;
        private int _lprServerPort;

        [DisplayName("Refresh Interval")] public int FR_RefreshInterval { get; set; }

        [DisplayName("Server Address")] public string FR_ServerAddress { get; set; }

        [DisplayName("Server Port")] public int FR_ServerPort { get; set; }

        [DisplayName("Refresh Interval")]
        public int LPR_RefreshInterval
        {
            get => _lprRefreshInterval;
            set
            {
                if (_lprRefreshInterval != value)
                {
                    _lprRefreshInterval = value;
                    OnPropertyChanged();
                }
            }
        }

        [DisplayName("Server Address")]
        public string LPR_ServerAddress
        {
            get => _lprServerAddress;
            set
            {
                if (_lprServerAddress != value)
                {
                    _lprServerAddress = value;
                    OnPropertyChanged();
                }
            }
        }

        [DisplayName("Server Port")]
        public int LPR_ServerPort
        {
            get => _lprServerPort;
            set
            {
                if (_lprServerPort != value)
                {
                    _lprServerPort = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}