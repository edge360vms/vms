﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Common.Enums.Settings;
using Edge360.Platform.VMS.Common.Properties;

namespace Edge360.Platform.VMS.Common.Settings
{
    [Serializable]
    public class MonitorSettings : INotifyPropertyChanged
    {
        private EPtzControlType _ptzControlType;
        private EVideoComponentType _videoPlayerType;

        [DisplayName("Overlay Ptz Type")]
        public EPtzControlType PtzControlType
        {
            get => _ptzControlType;
            set
            {
                if (_ptzControlType != value)
                {
                    _ptzControlType = value;
                    OnPropertyChanged();
                }
            }
        }

        [Browsable(false)]
        [DisplayName("Video Player Renderer")]
        public EVideoComponentType VideoPlayerType
        {
            get => _videoPlayerType;
            set
            {
                if (_videoPlayerType != value)
                {
                    _videoPlayerType = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}