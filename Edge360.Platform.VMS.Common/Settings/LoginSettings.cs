﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Common.Annotations;
using Edge360.Platform.VMS.Common.Interfaces.Authority;

namespace Edge360.Platform.VMS.Common.Settings
{
    [Serializable]
    public class LoginSettings : INotifyPropertyChanged
    {
        private ObservableCollection<string> _loginServers = new ObservableCollection<string>();
        private int _requestTimeoutDuration = 3;
        private string _sessionId;
        private bool _randomLoadBalance = true;
        private bool _signalREnabled = true;
        public UserLogin Credentials { get; set; } = new UserLogin();


        [Required(AllowEmptyStrings = false, ErrorMessage = "Server Address is required.")]
        public ObservableCollection<string> LoginServers
        {
            get => _loginServers;
            set
            {
                if (_loginServers != value)
                {
                    _loginServers = value;
                    OnPropertyChanged();
                }
            }
        }


        [MinLength(3)]
        [MaxLength(30000)]
        [DisplayName("Request Timeout Duration (Seconds)")]
        public int RequestTimeoutDuration
        {
            get => _requestTimeoutDuration;
            set
            {
                if (value == _requestTimeoutDuration)
                {
                    return;
                }

                var _timeout = Math.Min(30000, Math.Max(3, value));
                _requestTimeoutDuration = _timeout;
                OnPropertyChanged();
            }
        }

        public int RetryAttempts { get; set; }
        //public string ServerAddress { get; set; }
        //public int ServerPort { get; set; }

        public string SessionId
        {
            get => _sessionId;
            set => _sessionId = value;
        }

        public bool RandomLoadBalance
        {
            get => _randomLoadBalance;
            set
            {
                if (value == _randomLoadBalance) return;
                _randomLoadBalance = value;
                OnPropertyChanged();
            }
        }

        public bool SignalREnabled
        {
            get => _signalREnabled;
            set
            {
                if (value == _signalREnabled) return;
                _signalREnabled = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
           
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}