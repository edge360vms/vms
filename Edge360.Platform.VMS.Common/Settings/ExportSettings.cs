﻿using System;
using System.Drawing;
using System.IO;
using Newtonsoft.Json;

namespace Edge360.Platform.VMS.Common.Settings
{
    [Serializable]
    public class ExportSettings
    {
        [JsonIgnore]
        public static string DefaultExportPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "E360", "VMS", "exports");

        public string OutputDirectory { get; set; } =
            $"{DefaultExportPath}";

        [JsonIgnore] public Bitmap Watermark { get; set; }

        public byte[] WatermarkBytes { get; set; }
    }
}