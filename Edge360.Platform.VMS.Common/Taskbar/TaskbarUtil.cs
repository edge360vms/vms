﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Edge360.Platform.VMS.Common.Taskbar
{
    internal struct PropertyKey
    {
        private Guid formatId;
        private int propertyId;

        internal PropertyKey(Guid guid, int propertyId)
        {
            formatId = guid;
            this.propertyId = propertyId;
        }
    }

    [StructLayout(LayoutKind.Explicit)]
    internal struct PropVariant
    {
        [FieldOffset(0)] internal ushort vt;
        [FieldOffset(8)] internal IntPtr pv;
        [FieldOffset(8)] internal ulong padding;

        [DllImport("Ole32.dll", PreserveSig = false)]
        internal static extern void PropVariantClear(ref PropVariant pvar);

        internal PropVariant(string value)
        {
            vt = (ushort) VarEnum.VT_LPWSTR;
            padding = 0;
            pv = Marshal.StringToCoTaskMemUni(value);
        }

        internal void Clear()
        {
            PropVariantClear(ref this);
        }
    }

    [ComImport]
    [Guid("886D8EEB-8CF2-4446-8D02-CDBA1DBDCF99")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    internal interface IPropertyStore
    {
        int GetCount([Out] out uint propertyCount);

        void GetAt([In] uint propertyIndex, [Out] [MarshalAs(UnmanagedType.Struct)]
            out PropertyKey key);

        void GetValue([In] [MarshalAs(UnmanagedType.Struct)] ref PropertyKey key,
            [Out] [MarshalAs(UnmanagedType.Struct)]
            out PropVariant pv);

        void SetValue([In] [MarshalAs(UnmanagedType.Struct)] ref PropertyKey key,
            [In] [MarshalAs(UnmanagedType.Struct)] ref PropVariant pv);

        void Commit();
    }

    public static class TaskbarUtil
    {
        internal static Guid AppGuid { get; set; }
        //System.AppUserModel
        internal static Guid FormatId => new Guid("9F4C2855-9F79-4B39-A8D0-E1D42DE1D5F3");
        internal static PropertyKey AppId => new PropertyKey(FormatId, 5);
        internal static PropertyKey DisplayName => new PropertyKey(FormatId, 4);
        internal static PropertyKey RelaunchCommand => new PropertyKey(FormatId, 2);

        [DllImport("shell32.dll")]
        private static extern int SHGetPropertyStoreForWindow(
            IntPtr hwnd,
            ref Guid iid /*IID_IPropertyStore*/,
            [Out] [MarshalAs(UnmanagedType.Interface)]
            out IPropertyStore propertyStore);

        private static void ClearValue(IPropertyStore store, PropertyKey key)
        {
            try
            {
                var prop = new PropVariant();
                prop.vt = (ushort) VarEnum.VT_EMPTY;
                if (store != null)
                {
                    store.SetValue(ref key, ref prop);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }
        }

        private static void SetValue(IPropertyStore store, PropertyKey key, string value)
        {
            try
            {
                var prop = new PropVariant(value);
                store.SetValue(ref key, ref prop);
                prop.Clear();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        internal static IPropertyStore Store(IntPtr handle)
        {
            IPropertyStore store;
            var store_guid = new Guid("886D8EEB-8CF2-4446-8D02-CDBA1DBDCF99");
            var rc = SHGetPropertyStoreForWindow(handle, ref store_guid, out store);
            if (rc != 0)
            {
                //_log?.Info($"Error Storing Value...");
                //throw Marshal.GetExceptionForHR(rc);
            }

            return store;
        }

        public static TaskbarInfo TaskbarInfo { get; set; }

        public static void SetLaunchTarget(IntPtr handle)
        {
            try
            {
                if (TaskbarInfo == null)
                {
                    return;
                }
                AppGuid = TaskbarInfo.AppGuid;
                var store = Store(handle);
                SetValue(store, AppId, AppGuid.ToString());

                SetValue(store, RelaunchCommand, TaskbarInfo.TargetFile);
                SetValue(store, DisplayName, TaskbarInfo.AppDisplayName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            //form.FormClosed += delegate { Cleanup(handle); };
        }

        public static void Cleanup(IntPtr handle)
        {
            try
            {
                var store = Store(handle);
                ClearValue(store, AppId);
                ClearValue(store, RelaunchCommand);
                ClearValue(store, DisplayName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }

    public class TaskbarInfo
    {
        public Guid FormatId => new Guid("9F4C2855-9F79-4B39-A8D0-E1D42DE1D5F3");
        public string TargetFile { get; set; }
        public Guid AppGuid { get; set; }
        public string AppDisplayName { get; set; }
        
    }
}