﻿namespace Edge360.Platform.VMS.Common.Enums.Video
{
    /// <summary>
    ///     The Playback speed of the VideoControl
    /// </summary>
    public enum EPlaybackSpeed
    {
        Speed1x = 1,
        Speed2x = 2,
        Speed4x = 4,
        Speed8x = 8,
        Speed10x = 10,
        Speed20x = 20,
        Speed40x = 40,
        Speed80x = 80,
        Speed200x = 200
    }
}