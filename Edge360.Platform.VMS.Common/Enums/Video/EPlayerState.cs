﻿namespace Edge360.Platform.VMS.Common.Enums.Video
{
    /// <summary>
    ///     The PlayerState of the VideoControl
    /// </summary>
    public enum EPlayerState
    {
        None,
        Stopped,
        Paused,
        Playing,
        Rewinding,
        Opening
    }
}