﻿namespace Edge360.Platform.VMS.Common.Enums.Video
{
    /// <summary>
    ///     Event Types inside of a Timeline
    /// </summary>
    public enum ETimelineEventType
    {
        Bookmark,
        LPR_Event,
        Analytic_Event,
        Motion_Event,
        Generic_Event
    }
}