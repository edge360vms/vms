﻿namespace Edge360.Platform.VMS.Common.Enums.Video
{
    /// <summary>
    ///     The Playback State of the VideoControl
    /// </summary>
    public enum EPlaybackState
    {
        None = 1 << 1,
        Live = 1 << 2,
        Archive = 1 << 3,
        Forward = 1 << 4,
        Reverse = 1 << 5
    }
}