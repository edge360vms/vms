﻿using System;

namespace Edge360.Platform.VMS.Common.Enums.Video
{
    [Flags]
    public enum EControlOverlaySettings : uint
    {
        None = 0,
        Ptz = 1 << 1,
        Timestamp = 1 << 2,
        Framerate = 1 << 3,
        Bitrate = 1 << 4,
        Timeline = 1 << 5,
        CameraName = 1 << 6,
        PlaybackSpeed = 1 << 7,
        DigitalZoom = 1 << 8,
        PtzClickArea = 1 << 9,
        All = uint.MaxValue
    }
}