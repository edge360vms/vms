﻿namespace Edge360.Platform.VMS.Common.Enums.Notification
{
    /// <summary>
    ///     Types of Notifications
    /// </summary>
    public enum ENotificationType
    {
        LPR,
        Analytic,
        Motion,
        Hardware_Failure,
        VideoManagement
    }
}