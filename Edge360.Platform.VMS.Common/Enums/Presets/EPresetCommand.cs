﻿namespace Edge360.Platform.VMS.Common.Enums.Presets
{
    public enum EPresetCommand
    {
        Home,
        Preset1,
        Preset2,
        Preset3,
        Preset4,
        Preset5,
        Preset6,
        Preset7
    }
}