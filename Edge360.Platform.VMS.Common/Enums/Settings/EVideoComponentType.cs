﻿using System;
using System.ComponentModel;

namespace Edge360.Platform.VMS.Common.Enums.Settings
{
    [Serializable]
    public enum EVideoComponentType
    {
        [Description("Medialooks Renderer")] Medialooks,
        [Description("VLC Renderer")] VLC,
        [Description("MPV Renderer")] MPV
    }
}