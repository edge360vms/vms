﻿using System;

namespace Edge360.Platform.VMS.Common.Enums.Settings
{
    [Serializable]
    public enum EPtzControlType
    {
        Box,
        Arrow
    }
}