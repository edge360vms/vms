﻿namespace Edge360.Platform.VMS.Common.Enums.Bookmarks
{
    public enum EBookmarkCommand
    {
        None,
        PreviousBookmark,
        BookmarkManager,
        NextBookmark,
        AddBookmark
    }
}