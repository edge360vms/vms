﻿namespace Edge360.Platform.VMS.Common.Enums.Management
{
    public enum EZoneActionConfigCommand
    {
        CreateZone,
        EditZone,
        DisconnectZone,
        RefreshZone,
        RefreshCameras,
        RefreshServers,
        RefreshAllZones,
        PreviewPopoutPlayer
    }

    public enum EArchiverConfigCommand
    {
        OpenNotifications,
        OpenStatistics,
        OpenAdvancedSettings,
        DeleteDatabase,
        CreateDatabase,
        DatabaseInfo,
        BackupRestore,
        CreateVolume,
        CreateCamera,
        CreateZone,
        ManageFolders,
        DeleteVolume,
        DeleteZone,
        RefreshVolumes,
        RefreshCameras,
        RefreshParents,
        EditVolume,
        EditZone
    }
}