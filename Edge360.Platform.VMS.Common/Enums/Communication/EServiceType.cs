﻿namespace Edge360.Platform.VMS.Common.Enums.Communication
{
    public enum EServiceType
    {
        None,
        Authority,
        Cameras,
        Recordings
    }
}