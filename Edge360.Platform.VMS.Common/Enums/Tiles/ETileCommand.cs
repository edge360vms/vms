﻿namespace Edge360.Platform.VMS.Common.Enums.Tiles
{
    public enum ETileCommand
    {
        Fullscreen,
        Layout1x1,
        Layout2x2,
        Layout3x3,
        Layout4x4,
        SearchAnalytics,
        ManageBookmarks,
        ToggleThumbnails,
        ToggleTimeline,
        CancelPtz
    }
}