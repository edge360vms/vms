﻿namespace Edge360.Platform.VMS.Common.Enums.Camera
{
    public enum EPtzCommand
    {
        PanUp,
        PanUpLeft,
        PanUpRight,
        PanDown,
        PanDownLeft,
        PanDownRight,
        PanLeft,
        PanRight,
        ZoomIn,
        ZoomOut
    }
}