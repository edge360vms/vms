﻿namespace Edge360.Platform.VMS.Common.Enums.Camera
{
    public enum EPtzPreset
    {
        Home,
        Preset1,
        Preset2,
        Preset3,
        Preset4,
        Preset5,
        Preset6,
        Preset7,
        Preset8,
        Preset9,
        Preset10
    }
}