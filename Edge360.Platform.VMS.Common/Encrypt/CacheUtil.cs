﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace Edge360.Platform.VMS.Common.Encrypt
{
    public static class CacheUtil
    {
        public static void EncryptInMemoryData(byte[] Buffer, MemoryProtectionScope Scope)
        {
            if (Buffer == null)
            {
                throw new ArgumentNullException("Buffer");
            }

            if (Buffer.Length <= 0)
            {
                throw new ArgumentException("Buffer");
            }

            // Encrypt the data in memory. The result is stored in the same array as the original data.
            ProtectedMemory.Protect(Buffer, Scope);
        }

        public static void DecryptInMemoryData(byte[] Buffer, MemoryProtectionScope Scope)
        {
            if (Buffer == null)
            {
                throw new ArgumentNullException("Buffer");
            }

            if (Buffer.Length <= 0)
            {
                throw new ArgumentException("Buffer");
            }

            // Decrypt the data in memory. The result is stored in the same array as the original data.
            ProtectedMemory.Unprotect(Buffer, Scope);
        }

        public static byte[] CreateRandomEntropy()
        {
            // Create a byte array to hold the random value.
            var entropy = new byte[16];

            // Create a new instance of the RNGCryptoServiceProvider.
            // Fill the array with a random value.
            new RNGCryptoServiceProvider().GetBytes(entropy);

            // Return the array.
            return entropy;
        }

        public static int EncryptDataToStream(byte[] Buffer, byte[] Entropy, DataProtectionScope Scope, Stream S)
        {
            if (Buffer == null)
            {
                throw new ArgumentNullException("Buffer");
            }

            if (Buffer.Length <= 0)
            {
                throw new ArgumentException("Buffer");
            }

            if (Entropy == null)
            {
                throw new ArgumentNullException("Entropy");
            }

            if (Entropy.Length <= 0)
            {
                throw new ArgumentException("Entropy");
            }

            if (S == null)
            {
                throw new ArgumentNullException("S");
            }

            var length = 0;

            // Encrypt the data and store the result in a new byte array. The original data remains unchanged.
            var encryptedData = ProtectedData.Protect(Buffer, Entropy, Scope);

            // Write the encrypted data to a stream.
            if (S.CanWrite && encryptedData != null)
            {
                S.Write(encryptedData, 0, encryptedData.Length);

                length = encryptedData.Length;
            }

            // Return the length that was written to the stream.
            return length;
        }

        public static byte[] DecryptDataFromStream(byte[] Entropy, DataProtectionScope Scope, Stream S, int Length)
        {
            if (S == null)
            {
                throw new ArgumentNullException("S");
            }

            if (Length <= 0)
            {
                throw new ArgumentException("Length");
            }

            if (Entropy == null)
            {
                throw new ArgumentNullException("Entropy");
            }

            if (Entropy.Length <= 0)
            {
                throw new ArgumentException("Entropy");
            }

            var inBuffer = new byte[Length];
            byte[] outBuffer;

            // Read the encrypted data from a stream.
            if (S.CanRead)
            {
                S.Read(inBuffer, 0, Length);

                outBuffer = ProtectedData.Unprotect(inBuffer, Entropy, Scope);
            }
            else
            {
                throw new IOException("Could not read the stream.");
            }

            // Return the length that was written to the stream.
            return outBuffer;
        }
    }
}