﻿namespace Edge360.Platform.VMS.Common.Interfaces.Authority
{
    public interface IUserLogin
    {
        string Domain { get; set; }
        int PasswordLength { get; set; }
        bool RememberMe { get; set; }
        string UserName { get; set; }
    }
}