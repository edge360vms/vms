﻿namespace Edge360.Platform.VMS.Common.Interfaces.Authority
{
    public class UserLogin : IUserLogin
    {
        public override string ToString()
        {
            return $"{nameof(DomainWithoutPort)}: {DomainWithoutPort}, {nameof(IsWindowsLogin)}: {IsWindowsLogin}, {nameof(Domain)}: {Domain}, {nameof(WindowsId)}: {WindowsId}, {nameof(BearerToken)}: {BearerToken}, {nameof(RefreshToken)}: {RefreshToken}, {nameof(IsBearerLogin)}: {IsBearerLogin}, {nameof(PasswordLength)}: {PasswordLength}, {nameof(Password)}: {Password}, {nameof(UserName)}: {UserName}, {nameof(RememberMe)}: {RememberMe}, {nameof(ApiPort)}: {ApiPort}";
        }

        public string GetEntropy()
        {
            return (IsWindowsLogin ? $"{WindowsId}" : $"{UserName}▼{(IsBearerLogin ? RefreshToken : Password)}▼{DomainWithoutPort}") + "◄EdgeVMSCache";
        }
        public string DomainWithoutPort => Domain?.Replace($":{ApiPort}", "");

        public virtual bool IsWindowsLogin { get; set; }
        public virtual string Domain { get; set; }
        public virtual string WindowsId { get; set; }
        public virtual string BearerToken { get; set; }
        public virtual string RefreshToken { get; set; }
        public virtual bool IsBearerLogin { get; set; }
        public virtual int PasswordLength { get; set; }
        public virtual string Password { get; set; }
        public virtual string UserName { get; set; }
        public virtual bool RememberMe { get; set; }
        public virtual ushort ApiPort { get; set; } = 3501;
        public string ResolveDomain()
        {
            if (!string.IsNullOrEmpty(Domain))
            {
                if (!Domain.Contains(":"))
                {
                    return $"{Domain}:{ApiPort}";
                }
            }

            return Domain;
        }
    }
}