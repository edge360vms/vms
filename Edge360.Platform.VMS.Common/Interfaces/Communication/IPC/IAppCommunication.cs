﻿using System.ServiceModel;

namespace Edge360.Platform.VMS.Common.Interfaces.Communication.IPC
{
    [ServiceContract(SessionMode = SessionMode.Allowed)]
    public interface IAppCommunication
    {
        [OperationContract(IsOneWay = true)]
        void StartupArgs(string[] args);

        [OperationContract(IsOneWay = true)]
        void WatchdogPing(long timestamp);
    }
}