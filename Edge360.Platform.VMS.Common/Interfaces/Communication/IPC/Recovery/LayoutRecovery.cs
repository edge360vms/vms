﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Edge360.Platform.VMS.Common.Interfaces.Communication.IPC.Recovery
{
    public class RecoveryMonitoringWindowItemModel
    {
        public Size Bounds { get; set; }
        public int DisplayIndex { get; set; }
        public Point GridSize { get; set; }
        public List<RecoveryTileItemModel> RecoveryTiles { get; set; }
    }

    public class RecoveryTileItemModel
    {
        public Guid CameraId { get; set; }
        public Point GridLocation { get; set; }
    }

    public class LayoutRecoveryCache
    {
        public List<RecoveryMonitoringWindowItemModel> RecoveryCacheList { get; set; } =
            new List<RecoveryMonitoringWindowItemModel>();
    }
}