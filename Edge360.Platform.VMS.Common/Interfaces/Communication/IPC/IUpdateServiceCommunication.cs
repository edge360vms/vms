﻿using System.ServiceModel;
using System.Windows.Forms.VisualStyles;

namespace Edge360.Platform.VMS.Common.Interfaces.Communication.IPC
{
    [ServiceContract(SessionMode = SessionMode.Allowed)]
    public interface IUpdateServiceCommunication
    {
        [OperationContract(IsOneWay = true)]
        void RegisterServers(string[] serverList);

        [OperationContract(IsOneWay = false)]
        bool UpdateNow(string[] serverList);

        [OperationContract(IsOneWay = false)]
        bool UpdateAvailable(string[] serverList);
    }
}
