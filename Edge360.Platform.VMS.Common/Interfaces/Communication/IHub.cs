﻿using System;
using System.Threading.Tasks;

namespace Edge360.Platform.VMS.Common.Interfaces.Communication
{
    public interface IHub : IDisposable
    {
        Task ConnectAsync(string hubUrl);
        void DisconnectAsync();
        void SubscribeAsync(string eventName, Action action);
    }
}