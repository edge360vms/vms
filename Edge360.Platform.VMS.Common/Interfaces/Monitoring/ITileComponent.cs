﻿using System;
using Edge360.Platform.VMS.Common.Interfaces.Video;

namespace Edge360.Platform.VMS.Common.Interfaces.Monitoring
{
    /// <summary>
    ///     Tile to be used with a TileGrid
    /// </summary>
    public interface ITileComponent : IDisposable
    {
        ITileGrid GridModel { get; }

        /// <summary>
        ///     Column Location of Tile
        /// </summary>
        int TileColumn { get; set; }

        /// <summary>
        ///     Number of Columns the Tile should span
        /// </summary>
        int TileColumnSpan { get; set; }

        /// <summary>
        ///     Toggles Tile to a fullscreen state
        /// </summary>
        bool IsFullscreen { get; set; }

        /// <summary>
        ///     Row Location of Tile
        /// </summary>
        int TileRow { get; set; }

        /// <summary>
        ///     Number of Rows the Tile should span
        /// </summary>
        int TileRowSpan { get; set; }

        int TileIndex { get; set; }

        event Action<object> ObjectDragOver;
        event Action<object> ObjectDragComplete;
    }
}