﻿namespace Edge360.Platform.VMS.Common.Interfaces.Monitoring
{
    public interface IAirspaceContent
    {
        void Redraw();
        void UpdateLocation();
    }
}