﻿using System.Threading.Tasks;
using System.Windows;
using Edge360.Platform.VMS.Common.Enums.Camera;

namespace Edge360.Platform.VMS.Common.Interfaces.Camera
{
    public interface IPtzManager
    {
        int PanSpeed { get; set; }
        int ZoomSpeed { get; set; }
        void ZoomRelease();
        void PanRelease();
        Task StopPtz();
        Task<bool> PanCamera(EPtzCommand command);
        Task<bool> ZoomCamera(EPtzCommand command);
        Task<bool> SetPreset(EPtzPreset preset);
        Task Zoom(bool zoomIn, Point point, float zoomAmount = .016f);
        void ContinuousZoom(bool zoomIn, Point point);
        Task MaxZoomAsync(bool zoomIn);
        void AreaZoom(Point point, int widthZoom, int imageHeight, int imageWidth, int rectHeight, int rectWidth);
        void PtzCenter(Point point, int imageWidth, int imageHeight);
        Task<bool> PanCamera(Point point);
    }
}