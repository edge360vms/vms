﻿using System;
using System.Windows;

namespace Edge360.Platform.VMS.Common.Interfaces.Camera
{
    public interface ICameraConfiguration
    {
        string Address { get; set; }
        Guid? Id { get; set; }
        string Label { get; set; }
        string Model { get; set; }
        Size GetResolution();
    }
}