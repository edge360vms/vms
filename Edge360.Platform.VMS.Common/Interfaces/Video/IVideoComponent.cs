﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Edge360.Platform.VMS.Common.Enums.Video;
using Edge360.Platform.VMS.Common.Interfaces.Camera;

namespace Edge360.Platform.VMS.Common.Interfaces.Video
{
    /// <summary>
    ///     A VideoComponent that Hosts a Video Player
    /// </summary>
    public interface IVideoComponent : IDisposable, INotifyPropertyChanged
    {
        bool IsLivePlayback { get; set; }

        /// <summary>
        ///     True when Video is Playing
        /// </summary>
        bool IsPlaying { get; set; }

        /// <summary>
        ///     Displays the current Playback speed (1x 2x 3x etc)
        /// </summary>
        EPlaybackSpeed PlaybackSpeed { get; set; }

        /// <summary>
        ///     Determines if Playback is Live, Archived, or None.
        /// </summary>
        EPlaybackState PlaybackState { get; set; }

        /// <summary>
        ///     Displays Playerstate (Stopped, Playing, Rewinding, Paused, and None)
        /// </summary>
        EPlayerState PlayerState { get; set; }

        IPtzManager PtzManager { get; set; }

        double ZoomFactor { get; set; }
        string LastOpenedAddress { get; }
        string LastOpenedUrl { get; set; }
        bool IsShowingVideoStats { get; set; }
        bool IsStartingPlayback { get; set; }

        Task Restart();
        Size GetImageBounds();

        void DigitalZoom(bool zoomIn, Point point);

        event Action MediaPlayerEnded; 
        event Action MediaPlayerError;

        event Action<EPlayerState> PlayerStateChanged;

        event Action<EPlaybackSpeed> PlaybackSpeedChanged;

        event Action<EPlaybackState> PlaybackStateChanged;

        /// <summary>
        ///     Event Action that fires each time a frame is rendered
        /// </summary>
        event Action<ImageSource> FrameRendered;

        /// <summary>
        ///     Sets the Play State
        /// </summary>
        void Play();

        /// <summary>
        ///     Sets the Pause State
        /// </summary>
        void Pause();

        /// <summary>
        ///     Stops the Player
        /// </summary>
        void Stop();

        void JumpToPosition(TimeSpan time);

        /// <summary>
        ///     Opens a Video from specified Url
        /// </summary>
        /// <param name="url"></param>
        void Open(string url);

        /// <summary>
        ///     Goes to the next event found in the Timeline
        /// </summary>
        /// <param name="type"></param>
        void NextEvent(ITimelineEvent type);

        /// <summary>
        ///     Goes to the previous event found in the Timeline
        /// </summary>
        /// <param name="type"></param>
        void PreviousEvent(ITimelineEvent type);
    }
}