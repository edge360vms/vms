﻿using Edge360.Platform.VMS.Common.Extensions.Commands;

namespace Edge360.Platform.VMS.Common.Interfaces.Video
{
    public interface ITileGrid
    {
        RelayCommand VideoControlCommand { get; }
    }
}