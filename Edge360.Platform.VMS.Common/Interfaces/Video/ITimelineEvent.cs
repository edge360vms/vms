﻿using System;
using System.Windows;
using System.Windows.Media;
using Edge360.Platform.VMS.Common.Enums.Video;

namespace Edge360.Platform.VMS.Common.Interfaces.Video
{
    /// <summary>
    ///     Events used on a Timeline
    /// </summary>
    public interface ITimelineEvent
    {
        /// <summary>
        ///     Description of Event
        /// </summary>
        string Description { get; set; }

        TimeSpan Duration { get; set; }

        /// <summary>
        ///     Time of Event
        /// </summary>
        DateTime EventTime { get; set; }

        /// <summary>
        ///     Type of Event
        /// </summary>
        ETimelineEventType EventType { get; set; }

        /// <summary>
        ///     Name of Event
        /// </summary>
        string Name { get; set; }

        /// <summary>
        ///     ObjectId referencing Event
        /// </summary>
        Guid ObjectId { get; set; }

        /// <summary>
        ///     TimeZone Information regarding Event (EST / PST)
        /// </summary>
        TimeZoneInfo TimeZone { get; set; }

        Visual GetVisual(Rect constraint);
    }
}