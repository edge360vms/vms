﻿using System;
using System.Collections.Generic;

namespace Edge360.Platform.VMS.Common.Interfaces.Video
{
    /// <summary>
    ///     Represents a provider of events to be displayed in the timeline
    /// </summary>
    public interface ITimelineDataProvider
    {
        /// <summary>
        ///     Gets the current begin time
        /// </summary>
        DateTime BeginTime { get; }

        /// <summary>
        ///     Gets the received bookmarks
        /// </summary>
        List<ITimelineEvent> Bookmarks { get; set; }

        /// <summary>
        ///     Gets Sets the current camera
        /// </summary>
        Guid Camera { get; set; }

        /// <summary>
        ///     Gets the current end time
        /// </summary>
        DateTime EndTime { get; }

        /// <summary>
        ///     Sets the timezone of the future sequences
        /// </summary>
        TimeZoneInfo FutureTimeZone { get; }

        bool IsTickerHidden { get; set; }

        /// <summary>
        ///     Gets the received motions
        /// </summary>
        List<ITimelineEvent> Motions { get; }

        /// <summary>
        ///     Gets the received sequences
        /// </summary>
        List<ITimelineEvent> Sequences { get; }

        List<ITimelineEvent> Thumbnails { get; }

        /// <summary>
        ///     Gets the camera time zone
        /// </summary>
        TimeZoneInfo TimeZone { get; }

        /// <summary>
        ///     Event fired when sequences are received
        /// </summary>
        event EventHandler SequencesReceived;

        /// <summary>
        ///     Event fired when motions are received
        /// </summary>
        event EventHandler MotionsReceived;

        /// <summary>
        ///     Event fired when bookmarks are received
        /// </summary>
        event EventHandler BookmarksReceived;

        /// <summary>
        ///     Event fired when thumbnails are received
        /// </summary>
        event EventHandler ThumbnailsReceived;

        event Action<DateTime, DateTime> TimelineRangeChanged;

        /// <summary>
        ///     Set the current timeline range
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        void SetTimelineRange(DateTime begin, DateTime end);
    }
}