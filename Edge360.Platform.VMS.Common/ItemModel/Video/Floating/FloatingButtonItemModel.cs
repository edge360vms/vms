﻿using System.Collections;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using Edge360.Platform.VMS.Common.Annotations;
using Edge360.Platform.VMS.Common.Enums.Video.Playback;
using Edge360.Platform.VMS.Common.Extensions.Commands;

namespace Edge360.Platform.VMS.Common.ItemModel.Video.Floating
{
    public class FloatingButtonItemModel : INotifyPropertyChanged
    {
        private ArrayList _buttonContextMenu = new ArrayList();
        private string _buttonFontFamily = "Font Awesome 5 Pro";
        private float _buttonFontSize = 12;
        private SolidColorBrush _buttonForeground = Brushes.White;
        private string _buttonToolTip;
        private string _glyph = "FontAwesomePlay";
        private bool _isDirectModeOnly;
        private bool _isDisabledDirectMode;
        private string _isFullscreenGlyph;
        private bool _isVisible = true;

        private RelayCommand _playbackCommand;

        public ArrayList ButtonContextMenu
        {
            get => _buttonContextMenu;
            set
            {
                if (Equals(value, _buttonContextMenu))
                {
                    return;
                }

                _buttonContextMenu = value;
                OnPropertyChanged();
            }
        }

        public string ButtonFontFamily
        {
            get => _buttonFontFamily;
            set
            {
                if (value == _buttonFontFamily)
                {
                    return;
                }

                _buttonFontFamily = value;
                OnPropertyChanged();
            }
        }

        public float ButtonFontSize
        {
            get => _buttonFontSize;
            set
            {
                if (value.Equals(_buttonFontSize))
                {
                    return;
                }

                _buttonFontSize = value;
                OnPropertyChanged();
            }
        }

        public SolidColorBrush ButtonForeground
        {
            get => _buttonForeground;
            set
            {
                if (Equals(value, _buttonForeground))
                {
                    return;
                }

                _buttonForeground = value;
                OnPropertyChanged();
            }
        }

        public bool ButtonIsVisible
        {
            get => _isVisible && !IsDirectModeOnly && !IsDisabledDirectMode;
            set
            {
                if (_isVisible != value)
                {
                    _isVisible = value;
                    OnPropertyChanged();
                }
            }
        }

        public string ButtonToolTip
        {
            get => _buttonToolTip;
            set
            {
                if (value == _buttonToolTip)
                {
                    return;
                }

                _buttonToolTip = value;
                OnPropertyChanged();
            }
        }

        public string Glyph
        {
            get => _glyph;
            set
            {
                if (value == _glyph)
                {
                    return;
                }

                _glyph = value;
                OnPropertyChanged();
            }
        }

        public bool IsDirectModeOnly
        {
            get => _isDirectModeOnly;
            set
            {
                if (value == _isDirectModeOnly)
                {
                    return;
                }

                _isDirectModeOnly = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(ButtonIsVisible));
            }
        }

        public bool IsDisabledDirectMode
        {
            get => _isDisabledDirectMode;
            set
            {
                if (value == _isDisabledDirectMode)
                {
                    return;
                }

                _isDisabledDirectMode = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(ButtonIsVisible));
            }
        }

        public string IsFullscreenGlyph
        {
            get => _isFullscreenGlyph;
            set
            {
                if (value == _isFullscreenGlyph)
                {
                    return;
                }

                _isFullscreenGlyph = value;
                OnPropertyChanged();
            }
        }

        public EVideoPlayerCommand VideoPlayerCommandType { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}