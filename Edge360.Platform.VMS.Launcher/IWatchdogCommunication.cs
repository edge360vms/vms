using System.ServiceModel;
// ReSharper disable CheckNamespace

namespace Edge360.Platform.VMS.Common.Interfaces.Communication.IPC
{
    [ServiceContract(SessionMode = SessionMode.Allowed)]
    public interface IWatchdogCommunication
    {
        [OperationContract(IsOneWay = true)]
        void OpenConnection(int pid, string filePath);

        [OperationContract(IsOneWay = true)]
        void OpenWatchConnection(int pid, string filePath, string[] arguments);

        [OperationContract(IsOneWay = true)]
        void SendHeartbeat(int pid, long timestamp);

        [OperationContract(IsOneWay = true)]
        void CloseConnection(int pid);
    }
}