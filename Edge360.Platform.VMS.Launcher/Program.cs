using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using Edge360.Platform.VMS.Common.Interfaces.Communication.IPC;
using Edge360.Platform.VMS.Launcher.View;
using Application = System.Windows.Application;
using MessageBox = System.Windows.Forms.MessageBox;

namespace Edge360.Platform.VMS.Launcher
{
    internal class Program
    {
        private static Mutex _instanceMutex;
        private static Task _processCheckLoop;
        private static MenuItem recoeryMenuItem;

        public static FileInfo NewestClient { get; set; }

        [STAThread]
        private static void Main(string[] args)
        {

            ResolveNewestClient();
            if (NewestClient == null)
            {
                MessageBox.Show(
                    "Edge360 VMS Client application was not found.\r\nPlease reinstall or repair the installation.",
                    "Edge360 VMS - Unable To Start", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomainOnAssemblyResolve;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;

            _instanceMutex = new Mutex(true, "Edge360VMS-Client-Watchdog", out var isNewInstance);
            var watchdog = new WatchdogService();

            CommandLineManager.Start(args);
            var startupArgs = CommandLineManager.StartupArgs;
            if (startupArgs != null && startupArgs.IsWatcher)
            {
                HandleWatcherStartup(args, startupArgs, isNewInstance, watchdog);
            }

            if (!isNewInstance)
            {
                if (startupArgs != null && startupArgs.IsWatcher)
                {
                    Console.WriteLine($"Watch Argument means I will not launch the client!");
                }
                else
                {
                    watchdog.SendClientIpcCommunication(args,
                        exception => { watchdog.StartClient(args: string.Join(" ", args)); });
                }
                Environment.Exit(0);
                return;
            }

            if (startupArgs != null && startupArgs.IsWatcher)
            {
                System.Windows.Forms.Application.EnableVisualStyles();
                System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
                watchdog.Start();
                ShowTaskbarIcon();
            }
            else
            {
                watchdog.Start().Wait();
            }
        }

        private static void HandleWatcherStartup(string[] args, CommandLineArgs startupArgs, bool isNewInstance,
            WatchdogService watchdog)
        {
            try
            {
                if (!string.IsNullOrEmpty(startupArgs.TargetExecutableName))
                {
                    Console.WriteLine($"Target Executable to be watched... {startupArgs.TargetExecutableName}");
                    var process = Process.GetProcessesByName($"{startupArgs.TargetExecutableName}")
                        ?.FirstOrDefault(o => o.Id != 0)?.Id ?? 0;
                    if (process != 0)
                    {
                        Console.WriteLine($"Found Process Id {process}");
                        if (!isNewInstance)
                        {
                            watchdog.RegisterProcessIdWithWatchdog(process, "", args);
                        }
                        else
                        {
                            Console.WriteLine($"Attempting to Register Process with arguments {string.Join(",", args)}");
                            watchdog.AddProcessWithArgumentsToDictionary(process, "", args);
                        }
                    }
                    else 
                    {
                        if (isNewInstance)
                        {
                            watchdog.AddTargetFileToSearch(startupArgs.TargetExecutableName, args);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }


        private static void ShowTaskbarIcon()
        {
            try
            {
                var exitMenuItem = new MenuItem("E&xit Watchdog Application");
                recoeryMenuItem = new MenuItem("Launch Recovery", (sender, args) =>
                {
                    try
                    {
                        var watchdogService = WatchdogService.Instance;
                        if (watchdogService.ClientDictionary.Any())
                        {
                            foreach (var pid in watchdogService.ClientDictionary.Keys)
                            {
                                watchdogService.TryStartAndRemove(pid);
                            }
                        }
                        else
                        {
                            watchdogService.TryRunClient();
                        }
                    }
                    catch (Exception e)
                    {
                        
                    }
                });
                var contextMenu = new ContextMenu(new MenuItem[]{recoeryMenuItem,exitMenuItem});
                contextMenu.Popup += RecoeryMenuItemOnPopup;
                var m_notifyIcon = new NotifyIcon(new Container())
                {
                    Text = "Edge360 VMS-Client Watchdog",
                    Icon = Icon.ExtractAssociatedIcon(
                        System.Windows.Forms.Application.ExecutablePath),
                    Visible = true,
                    ContextMenu = contextMenu,
                };
                exitMenuItem.Click += (sender, args) =>
                {
                    Environment.Exit(0);

                };

                System.Windows.Forms.Application.Run();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private static void RecoeryMenuItemOnPopup(object sender, EventArgs e)
        {
            if (recoeryMenuItem != null)
            {
                recoeryMenuItem.Text = $"Launch Recovery ({CommandLineManager.StartupArgs?.TargetExecutableName}) ({WatchdogService.Instance?.ClientDictionary?.Count ?? 0})";
            }
        }

        public static void ResolveNewestClient()
        {
            try
            {
                var baseDirectory = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
                var clients = baseDirectory.GetFiles("*VMS.Client.exe", SearchOption.AllDirectories);
                var latestVersion = new Version();
                foreach (var c in clients)
                {
                    try
                    {
                        var version = new Version(FileVersionInfo.GetVersionInfo(c.FullName).FileVersion);
                        if (version > latestVersion)
                        {
                            latestVersion = version;
                            NewestClient = c;
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }

                //Select Version if Shift held
                if (Keyboard.IsKeyDown(Key.LeftShift))
                {
                    ShowVersionWindow(clients, latestVersion);
                }

                //_log?.Info($"Newest Client Found {NewestClient.FullName} {latestVersion}");
            }
            catch (Exception e)
            {
            }
        }

        private static void ShowVersionWindow(FileInfo[] clients, Version latestVersion)
        {
            try
            {
                //  Application.Current.Dispatcher.Invoke((() =>
                //  {
                var versionSelect = new VersionSelectView();
                var fileVersionInfos = clients.Select(o => FileVersionInfo.GetVersionInfo(o.FullName))
                    .OrderByDescending(v => v.FileVersion);
                versionSelect.Model.Versions = new ObservableCollection<FileVersionInfo>(fileVersionInfos);
                versionSelect.Model.SelectedVersion =
                    versionSelect.Model.Versions.FirstOrDefault(o => o.FileVersion == latestVersion.ToString());
                //versionSelect.Show();
                versionSelect.ShowInTaskbar = true;
                versionSelect.ShowActivated = true;

                var showDialog = versionSelect.ShowDialog();
                if (showDialog.HasValue && showDialog.Value)
                {
                    try
                    {
                        NewestClient = new FileInfo(versionSelect.Model.SelectedVersion.FileName);
                    }
                    catch (Exception e)
                    {
                    }
                }

                //  }));
            }
            catch (Exception e)
            {
            }
        }

        private static void CurrentDomainOnUnhandledException(object sender,
            UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
        }

        private static Assembly CurrentDomainOnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (NewestClient?.Directory != null)
            {
                return SearchForAssembly(args, Path.Combine(NewestClient?.Directory.FullName, "lib")) ?? SearchForAssembly(args, Path.Combine(NewestClient?.Directory.FullName));
            }

            return default;
        }

        private static Assembly SearchForAssembly(ResolveEventArgs args, string libraryLocation)
        {
            Assembly assembly = null;
            var assemblyName = new AssemblyName(args.Name);

            var assemblyPath = Path.Combine(libraryLocation, assemblyName.Name + ".dll");
            try
            {
                if (!string.IsNullOrEmpty(assemblyPath))
                {
                    if (File.Exists(assemblyPath))
                    {
                        assembly = Assembly.LoadFrom(assemblyPath);
                    }
                }
            }
            catch (Exception)
            {
            }

            return assembly;
        }
    }
}