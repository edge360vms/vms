﻿using System.ComponentModel;
using System.Configuration.Install;

namespace Edge360.Platform.VMS.Launcher
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}