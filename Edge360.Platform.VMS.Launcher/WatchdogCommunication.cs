using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using Edge360.Platform.VMS.Common.Interfaces.Communication.IPC;

namespace Edge360.Platform.VMS.Common.Interfaces.Communication.IPC
{
}

namespace Edge360.Platform.VMS.Launcher
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant,
        InstanceContextMode = InstanceContextMode.Single)]
    public class WatchdogCommunication : IWatchdogCommunication
    {
        public void OpenConnection(int pid, string filePath)
        {
            ConnectionOpened?.Invoke(pid, filePath);
        }

        public void OpenWatchConnection(int pid, string filePath, string[] arguments)
        {
            ConnectionWatchOpened?.Invoke(pid, filePath, arguments);
        }

        public void SendHeartbeat(int pid, long timestamp)
        {
            HeartbeatReceived?.Invoke(pid, timestamp);
        }

        public void CloseConnection(int pid)
        {
            ConnectionClosed?.Invoke(pid);
        }

        public static event Action<int, long> HeartbeatReceived;
        public static event Action<int> ConnectionClosed;
        public static event Action<int, string> ConnectionOpened;
        public static event Action<int, string, string[]> ConnectionWatchOpened;
    }
}