﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Windows;
using Edge360.Platform.VMS.Common.Interfaces.Communication.IPC;
using NDesk.Options;
using Application = System.Windows.Forms.Application;

namespace Edge360.Platform.VMS.Common.Interfaces.Communication.IPC
{
    public class CommandLineArgs
    {
        public string QuickExportProfiles { get; set; }
        public bool AllowMultiInstance { get; set; }
        public bool AutoLogin { get; set; }
        public string BearerToken { get; set; }
        public bool CacheCreate { get; set; }
        public bool HideConsole { get; set; }
        public bool IsWatcher { get; set; }
        public Guid Layout { get; set; }
        public bool LoadCache { get; set; }
        public bool LoadRecoveryLayout { get; set; }
        public bool OfflineLogin { get; set; }
        public string Password { get; set; }
        public int Preset { get; set; }
        public string RefreshToken { get; set; }
        public string ServerAddress { get; set; }
        public bool ShowHelp { get; set; }
        public string TargetExecutableName { get; set; }
        public string Username { get; set; }
        public bool UseWindowsLogin { get; set; }
    }

    public static class CommandLineManager
    {
        public static CommandLineArgs StartupArgs { get; set; }

        public static void Start(string[] args)
        {
            try
            {
                CheckInvalidArgs(args);
            }
            catch (Exception e)
            {
            }
        }

        public static bool CheckInvalidArgs(string[] args)
        {
            StartupArgs = new CommandLineArgs();

            var p = new OptionSet
            {
                {
                    "export=", "<RecordingProfileId|> Launches QuickExport of RecordingProfiles",
                    v => StartupArgs.QuickExportProfiles = v
                },
                {
                    "wl", "Enables WindowsLogin",
                    v => StartupArgs.UseWindowsLogin = v != null
                },
                {
                    "offline", "Enables OfflineMode",
                    v => StartupArgs.OfflineLogin = v != null
                },
                {
                    "cache", "Creates a cache",
                    v => StartupArgs.CacheCreate = v != null
                },
                {
                    "recovery", "Loads a recovered save",
                    v => StartupArgs.LoadRecoveryLayout = v != null
                },
                {
                    "h|help", "Shows help",
                    v => StartupArgs.ShowHelp = v != null
                },
                {
                    "target=|t=", "the {TARGET EXE} to login with.",
                    v => StartupArgs.TargetExecutableName = v
                },
                {
                    "w|watch", "Start the Application as a watcher",
                    v => StartupArgs.IsWatcher = v != null
                },
                {
                    "u=", "the {USERNAME} to login with.",
                    v => StartupArgs.Username = v
                },
                {
                    "al", "Autologin if RefreshToken is Valid",
                    v => StartupArgs.AutoLogin = v != null
                },
                {
                    "b=", "the {BEARERTOKEN} to login with.",
                    v => StartupArgs.BearerToken = v
                },
                {
                    "r=", "the {REFRESHTOKEN} to login with.",
                    v => StartupArgs.RefreshToken = v
                },
                {
                    "p=",
                    "The {PASSWORD} to login with.",
                    v => StartupArgs.Password = v
                },
                {
                    "s=", "The {SERVER ADDRESS} to login with",
                    v => StartupArgs.ServerAddress = v
                },
                {
                    "preset=", "The {PRESET} to display",
                    (int v) => StartupArgs.Preset = v
                },
                {
                    "l|layout=", "The {PRESET} to display",
                    v => StartupArgs.Layout = Guid.Parse(v)
                },
                {
                    "silent", "Hides the console",
                    v => StartupArgs.HideConsole = v != null
                },
                {
                    "m|multi", "Allows for Multiple Instances",
                    v => StartupArgs.AllowMultiInstance = v != null
                }
            };

            try
            {
                p.Parse(args);
            }
            catch (OptionException optionException)
            {
                Console.WriteLine(optionException.Message);
                //_log?.Info(optionException.Message);
                return true;
            }

            if (StartupArgs.ShowHelp)
            {
                p.WriteOptionDescriptions(Console.Out);

                return false;
            }

            if (string.IsNullOrEmpty(StartupArgs.Username) ||
                string.IsNullOrEmpty(StartupArgs.ServerAddress) ||
                string.IsNullOrEmpty(StartupArgs.Password))
            {
                p.WriteOptionDescriptions(Console.Out);
                return true;
            }

            return false;
        }
    }
}


namespace Edge360.Platform.VMS.Launcher
{
    internal partial class WatchdogService : ServiceBase
    {
        public ConcurrentDictionary<int, WatchdogClient> ClientDictionary =
            new ConcurrentDictionary<int, WatchdogClient>();

        private Task _processCheckLoop;

        public static WatchdogService Instance { get; set; }

        public static bool IsConnectionOpen { get; set; }

        public WatchdogService()
        {
            Instance = this;

            InitializeComponent();
        }

        public void SendClientIpcCommunication(string[] commandLineArgs, Action<Exception> onException)
        {
            try
            {
                var pipeFactory =
                    new ChannelFactory<IAppCommunication>(
                        new NetNamedPipeBinding(),
                        new EndpointAddress("net.pipe://localhost/edge360_vms_client"));

                var service = pipeFactory.CreateChannel();

                service.StartupArgs(commandLineArgs);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                onException?.Invoke(e);
            }
        }

        //protected override void OnStart(string[] args)
        //{
        //    Instance?.Start();
        //    base.OnStart(args);
        //}

        //protected override void OnStop()
        //{
        //    base.OnStop();
        //}

        private void PingClient(Action<Exception> onException = null)
        {
            try
            {
                var pipeFactory =
                    new ChannelFactory<IAppCommunication>(
                        new NetNamedPipeBinding(),
                        new EndpointAddress("net.pipe://localhost/edge360_vms_client"));

                var service = pipeFactory.CreateChannel();

                service.WatchdogPing(DateTimeOffset.UtcNow.UtcTicks);
            }
            catch (Exception e)
            {
                onException?.Invoke(e);
                // Console.WriteLine(e);
            }
        }

        public async void RegisterProcessIdWithWatchdog(int pid, string fileToOpen = "", string[] arguments = default)
        {
            try
            {
                Console.WriteLine(
                    $"Attempting to Register Process with running Watchdog arguments PID:{pid} fileToOpen:{fileToOpen} args:{string.Join(",", arguments ?? new[] {""})}");
                var pipeFactory =
                    new ChannelFactory<IWatchdogCommunication>(
                        new NetNamedPipeBinding(),
                        new EndpointAddress("net.pipe://localhost/edge360_vms_watchdog"));

                var service = pipeFactory.CreateChannel();
                Console.WriteLine("Sending to Service...");
                service.OpenWatchConnection(pid, fileToOpen, arguments);
            }
            catch (Exception e)
            {
                await Task.Delay(1000);
                Console.WriteLine(e);
            }
        }

        public static Task<bool> InitIpcCommunication()
        {
            if (IsConnectionOpen)
            {
                return Task.FromResult(true);
            }

            var tcs = new TaskCompletionSource<bool>();

            Task.Run(async () =>
            {
                try
                {
                    using (var _namedPipe = new ServiceHost(new WatchdogCommunication()))
                    {
                        _namedPipe.AddServiceEndpoint(typeof(IWatchdogCommunication),
                            new NetNamedPipeBinding(), "net.pipe://localhost/edge360_vms_watchdog");
                        _namedPipe.Open();
                        IsConnectionOpen = true;
                        tcs.SetResult(true);
                        while (true)
                        {
                            await Task.Delay(65);
                        }
                    }
                }
                catch (Exception e)
                {
                    // _log?.Info(e);
                    tcs.SetResult(false);
                    //  await Task.Delay(5000);
                    //  InitIpcCommunication();
                }
            });
            return tcs.Task;
        }

        public async Task Start()
        {
            var initIpcCommunication = await InitIpcCommunication();
            Console.WriteLine($"IPC Started {initIpcCommunication}");

            var commandLineArgs = CommandLineManager.StartupArgs;
            if (commandLineArgs == null || !commandLineArgs.IsWatcher)
            {
                TryRunClient();
            }

            WatchdogCommunication.HeartbeatReceived += WatchdogCommunicationOnHeartbeatReceived;
            WatchdogCommunication.ConnectionClosed += WatchdogCommunicationOnConnectionClosed;
            WatchdogCommunication.ConnectionOpened += WatchdogCommunicationOnConnectionOpened;
            WatchdogCommunication.ConnectionWatchOpened += WatchdogCommunicationOnConnectionWatchOpened;
            while (true)
            {
                var expired = (DateTimeOffset.UtcNow - TimeSpan.FromSeconds(6)).UtcTicks;

                foreach (var c in ClientDictionary)
                {
                    if (c.Value.IsProcessExitOnly)
                    {
                        //ignore null clients (IPSC)
                        continue;
                    }

                    PingClient(exception =>
                    {
                        StartClient(c.Value);
                        ClientDictionary.TryRemove(c.Key, out _);
                    });
                    if (c.Value.LastPinged < expired)
                    {
                        StartClient(c.Value);
                        ClientDictionary.TryRemove(c.Key, out _);
                    }
                }

                await Task.Delay(2000);
            }
        }

        public async void TryRunClient()
        {
            var processId = StartClient();
        }

        public int StartClient(WatchdogClient client = null, string args = null)
        {
            try
            {
                if (client == null)
                {
                    try
                    {
                        Program.ResolveNewestClient();
                    }
                    catch (Exception e)
                    {
                        
                    }
                }
                var cl = client ?? new WatchdogClient
                {
                    FilePath  = Program.NewestClient
                        .FullName, // Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Edge360.Platform.VMS.Client.exe"),
                    LastPinged = 0,
                    ProcessId = 0
                };
                Console.WriteLine($"Starting {cl.FilePath} {cl.ProcessId}!!!");
                var file = new FileInfo(cl.FilePath);

                if (file.Directory != null)
                {
                    //ApplicationLoader.PROCESS_INFORMATION procInfo;
                    //ApplicationLoader.StartProcessAndBypassUAC(client.FilePath, out procInfo);

                    var process = Process.Start(new ProcessStartInfo
                    {
                        FileName = cl.FilePath,
                        Arguments = $"{args}",
                        UseShellExecute = true,
                        //LoadUserProfile = true,
                        CreateNoWindow = false,
                        WindowStyle = ProcessWindowStyle.Normal,
                        WorkingDirectory = file.Directory.FullName
                    });

                    if (process != null)
                    {
                        process.Exited += ProcessOnExited;
                    }

                    return process.Id;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return -1;
        }

        public void AddTargetFileToSearch(string targetExe, string [] args)
        {
            _processCheckLoop = Task.Factory.StartNew(async b =>
                {
                    while (true)
                    {
                        try
                        {
                            await Task.Delay(5000);
                            var loopLookForProcess =
                                Process.GetProcessesByName(targetExe)?.FirstOrDefault(o => o.Id != 0)?.Id ?? 0;
                            if (loopLookForProcess != 0)
                            {
                                Console.WriteLine($"Found Process Id {loopLookForProcess}");
                                Console.WriteLine(
                                    $"Attempting to Register Process with arguments {string.Join(",", args)}");
                                AddProcessWithArgumentsToDictionary(loopLookForProcess, "", args);
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }
                }, TaskCreationOptions.LongRunning);
            }

        public void AddProcessWithArgumentsToDictionary(int pid, string filePath, string[] arguments)
        {
            try
            {
                //Console.WriteLine($"Adding {filePath} {pid}");
                var process = Process.GetProcessById(pid);

                if (process != null && !ClientDictionary.TryGetValue(process.Id, out _)
                ) // don't create multiple wait for exit
                {
                    Task.Run(() =>
                    {
                        //IPSC?
                        var sw = new Stopwatch();
                        sw.Start();
                        Console.WriteLine($"Waiting on Process Handle {process.Id}");
                        process.WaitForExit();
                        sw.Stop();
                        var swElapsedMilliseconds = sw.ElapsedMilliseconds;
                        //Console.WriteLine($"Process exited! Checking if in our Dictionary...");
                        var processId = process.Id;
                        TryStartAndRemove(processId, swElapsedMilliseconds);
                    });
                }

                ClientDictionary.AddOrUpdate(pid, new WatchdogClient
                {
                    FilePath = filePath,
                    LastPinged = DateTimeOffset.UtcNow.UtcTicks,
                    ProcessId = pid,
                    Arguments = arguments,
                    IsProcessExitOnly = true,
                }, (i, client) => { return client; });
                Console.WriteLine(
                    $"AddProcessWithArgumentsToDictionary Pid:{pid} path:{filePath} args:{string.Join(",", arguments)}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.GetType()} {e.Message}");
            }
        }

        public void TryStartAndRemove(int processId, long swElapsedMilliseconds = 3001)
        {
            if (ClientDictionary.TryGetValue(processId, out var client))
            {
                //  Console.WriteLine($"Found Process!");

                if (swElapsedMilliseconds > 3000)
                {
                    StartClient(null, string.Join(" ", client.Arguments));
                }

                ClientDictionary.TryRemove(processId, out _);
            }
        }

        private void WatchdogCommunicationOnConnectionWatchOpened(int pid, string filePath, string[] arguments)
        {
            AddProcessWithArgumentsToDictionary(pid, filePath, arguments);
        }

        private void WatchdogCommunicationOnConnectionOpened(int pid, string filePath)
        {
            try
            {
                //Console.WriteLine($"Adding {filePath} {pid}");
                Task.Run(() =>
                {
                    var process = Process.GetProcessById(pid);
                    if (process != null)
                    {
                        var sw = new Stopwatch();
                        sw.Start();
                        process.WaitForExit();
                        sw.Stop();

                        //Console.WriteLine($"Process exited! Checking if in our Dictionary...");
                        if (ClientDictionary.TryGetValue(process.Id, out var client))
                        {
                            //  Console.WriteLine($"Found Process!");
                            if (sw.ElapsedMilliseconds > 3000)
                            {
                                StartClient(client, "-al");
                            }

                            ClientDictionary.TryRemove(client.ProcessId, out _);
                        }
                    }
                });

                ClientDictionary.TryAdd(pid, new WatchdogClient
                {
                    FilePath = filePath,
                    LastPinged = DateTimeOffset.UtcNow.UtcTicks,
                    ProcessId = pid
                });
            }
            catch (Exception e)
            {
            }
        }

        private void ProcessOnExited(object sender, EventArgs e)
        {
            Console.WriteLine("Watchdog's Launched NativeClient exited!");

        }

        private void WatchdogCommunicationOnConnectionClosed(int pid)
        {
            Console.WriteLine($"Client Closed from {pid}");
            ClientDictionary.TryRemove(pid, out _);

            if (!ClientDictionary.Any())
            {
                if (!CommandLineManager.StartupArgs.IsWatcher)
                {

                    Environment.Exit(0);
                }
            }

        }

        private void WatchdogCommunicationOnHeartbeatReceived(int pid, long timestamp)
        {
            try
            {
                ClientDictionary.TryGetValue(pid, out var existingClient);
                if (existingClient != null)
                {
                    existingClient.LastPinged = timestamp;
                }

                // Console.WriteLine($"Heartbeat from {pid} {timestamp}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}