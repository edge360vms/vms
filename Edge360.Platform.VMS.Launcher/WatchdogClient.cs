namespace Edge360.Platform.VMS.Launcher
{
    public class WatchdogClient
    {
        public string FilePath { get; set; }
        public long LastPinged { get; set; }
        public int ProcessId { get; set; }
        public string[] Arguments { get; set; }
        public bool IsProcessExitOnly { get; set; }
    }
}