﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using Edge360.Platform.VMS.Launcher.Annotations;

namespace Edge360.Platform.VMS.Launcher.View
{
    /// <summary>
    ///     Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class VersionSelectView : Window
    {
        public VersionSelectViewModel Model => DataContext as VersionSelectViewModel;

        public VersionSelectView()
        {
            InitializeComponent();
            DataContext = new VersionSelectViewModel(this);
            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {

            Activate();
            BringIntoView();
            Focus();
        }
    }

    public class VersionSelectViewModel : INotifyPropertyChanged
    {
        private RelayCommand _confirmCommand;

        private FileVersionInfo _selectedVersion;
        private ObservableCollection<FileVersionInfo> _versions;
        private readonly VersionSelectView _view;

        public RelayCommand ConfirmCommand =>
            _confirmCommand ?? (_confirmCommand = new RelayCommand(o => Confirm(o), o => CanConfirm()));

        public FileVersionInfo SelectedVersion

        {
            get => _selectedVersion;
            set
            {
                if (Equals(value, _selectedVersion))
                {
                    return;
                }

                _selectedVersion = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<FileVersionInfo> Versions
        {
            get => _versions;
            set
            {
                if (Equals(value, _versions))
                {
                    return;
                }

                _versions = value;
                OnPropertyChanged();
            }
        }

        public VersionSelectViewModel(VersionSelectView view)
        {
            _view = view;
        }

        private void Confirm(object o)
        {
            _view.DialogResult = true;
        }

        private bool CanConfirm()
        {
            return SelectedVersion != null;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}