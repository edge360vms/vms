﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.UI.Common.Annotations;
using MahApps.Metro.Controls;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.UI.Common.View.Dialog
{
    /// <summary>
    ///     Interaction logic for MetroConfirm.xaml
    /// </summary>
    public partial class MetroConfirm : MetroWindow, INotifyPropertyChanged
    {
        private string _dialogTitle;
        private string _description;
        private DelegateCommand _affirmDialogCommand;
        private DelegateCommand _negativeDialogCommand;
        private bool? _result;

        public MetroConfirm()
        {
            InitializeComponent();
            DataContext = this;
        }

        public string DialogTitle
        {
            get => _dialogTitle;
            set
            {
                if (value == _dialogTitle) return;
                _dialogTitle = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                if (value == _description) return;
                _description = value;
                OnPropertyChanged();
            }
        }

        public bool? Result
        {
            get => _result;
            set
            {
                if (value == _result) return;
                _result = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand NegativeDialogCommand => _negativeDialogCommand ??= new DelegateCommand((o) => DialogExecute(null));

        public DelegateCommand AffirmDialogCommand => _affirmDialogCommand ??= new DelegateCommand((o) => DialogExecute(this));

        private void DialogExecute(object obj)
        {
            Result = obj != null;
            Close();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}