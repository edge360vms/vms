﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Edge360.Platform.VMS.Common.Properties;
using Edge360.Platform.VMS.UI.Common.Util;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.UI.Common.View.Dialog
{
    public class DialogBase : UserControl, INotifyPropertyChanged, IDisposable
    {
        public static readonly DependencyProperty GlyphFontProperty = DependencyProperty.Register(
            "GlyphFont", typeof(string), typeof(DialogBase),
            new PropertyMetadata(Application.Current.TryFindResource("TelerikFont")));

        public static readonly DependencyProperty GlyphProperty = DependencyProperty.Register(
            "Glyph", typeof(string), typeof(DialogBase),
            new PropertyMetadata(Application.Current.TryFindResource("GlyphGear")));

        private object _dialogItem;
        private bool _dialogResult;
        private bool _hideCloseButton;
        private bool _isModalMode;
        private ContentPresenter _modalContentPresenter;
        private string _modalWindowHeader;
        private RadWindow ModalWindow;

        public object DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool DialogResult
        {
            get => _dialogResult;
            set
            {
                if (_dialogResult != value)
                {
                    _dialogResult = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Glyph
        {
            get => (string) GetValue(GlyphProperty);
            set => SetValue(GlyphProperty, value);
        }

        public string GlyphFont
        {
            get => (string) GetValue(GlyphFontProperty);
            set => SetValue(GlyphFontProperty, value);
        }

        public string Header { get; set; }

        public bool HideCloseButton
        {
            get => _hideCloseButton;
            set
            {
                if (_hideCloseButton != value)
                {
                    _hideCloseButton = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool HideMaximizeButton { get; set; }

        public bool HideMinimizeButton { get; set; }

        public bool IsModalMode
        {
            get => _isModalMode;
            set
            {
                if (value == _isModalMode)
                {
                    return;
                }

                _isModalMode = value;
                OnPropertyChanged();
            }
        }

        public ContentPresenter ModalContentPresenter
        {
            get => _modalContentPresenter;
            set
            {
                if (Equals(value, _modalContentPresenter))
                {
                    return;
                }

                _modalContentPresenter = value;
                OnPropertyChanged();
            }
        }

        public EDialogAction DialogAction { get; set; }


        public bool Result { get; set; }

        // private object _hostControl { get; set; }
        private object _originalContent { get; set; }

        //static DialogBase()
        //{

        //}

        public DialogBase()
        {
            //WindowStartupLocation = WindowStartupLocation.CenterOwner;
            Closed += OnClosed;
            Loaded += OnLoaded;
            //HeaderTemplate = new DataTemplate
            //{
            //    Resources = null,
            //    Template = null,
            //    VisualTree = null,
            //    DataType = null
            //};
        }

        public void Dispose()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            //  dialogRadDataFormStyle.TargetType = typeof(StackPanel);
            try
            {
                var stackPanelStyle = Application.Current.FindResource("DialogStackPanelStyle") as Style;
                //   stackPanelStyle.TargetType = typeof(StackPanel);
                var dialogRadDataFormStyle = Application.Current.FindResource("DialogRadDataFormStyle") as Style;
                Resources.Add(typeof(StackPanel), stackPanelStyle);
                Resources.Add(typeof(RadDataForm), dialogRadDataFormStyle);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        public void Close()
        {
            Visibility = Visibility.Hidden;
            Closed?.Invoke(this, new WindowClosedEventArgs {DialogResult = DialogResult});
            if (ModalWindow != null)
            {
                ModalWindow.Close();
            }
        }

        public event EventHandler<WindowClosedEventArgs> Closed;


        public bool? ShowDialog()
        {
            ModalWindow = new RadWindow
            {
                ResizeMode = ResizeMode.NoResize,
                WindowStartupLocation = WindowStartupLocation.CenterScreen, Header = Header, Content = this,
                HideMaximizeButton = HideMaximizeButton, HideMinimizeButton = HideMinimizeButton
            };
            ModalWindow.Loaded += (sender, args) => { ModalWindow.SetTaskbarVisibility(true, Header, false); };

            var result = ModalWindow.ShowDialog();

            return result != null && result.Value && Result;
        }

        public void SetModalView(ContentPresenter modalContentPresenter = null, bool dialogView = true)
        {
            if (modalContentPresenter != null)
            {
                ModalContentPresenter = modalContentPresenter;
            }

            if (ModalContentPresenter != null)
            {
                if (ModalContentPresenter != null)
                {
                    IsModalMode = dialogView;
                }

                if (dialogView)
                {
                    _originalContent = ModalContentPresenter.Content;
                    //_hostControl = control;

                    ModalContentPresenter.Content = this;
                    try
                    {
                    }
                    catch (Exception e)
                    {
                    }
                }
                else
                {
                    ModalContentPresenter.Content = _originalContent;
                    ModalContentPresenter.DataContext = null;
                }
            }
        }


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                var dataFormDataField = this.ChildrenOfType<TextBox>().FirstOrDefault();
                //var text = dataFormDataField?.ChildrenOfType<DataFormDataField>().FirstOrDefault()?
                //    .ChildrenOfType<TextBox>().FirstOrDefault();
                if (dataFormDataField != null)
                {
                    dataFormDataField?.SelectAll();
                    dataFormDataField?.Focus();
                    FocusManager.SetFocusedElement(FocusManager.GetFocusScope(dataFormDataField), dataFormDataField);
                }
            }
            catch (Exception exception)
            {
            }
        }

        private void OnClosed(object sender, WindowClosedEventArgs e)
        {
            
            if (e.DialogResult != null)
            {
                Result = e.DialogResult.Value;
            }

            if (!Result)
            {
                DialogItem = null;
            }

            if (ModalContentPresenter != null)
            {
                SetModalView(ModalContentPresenter, false);
            }
        }
    }
}