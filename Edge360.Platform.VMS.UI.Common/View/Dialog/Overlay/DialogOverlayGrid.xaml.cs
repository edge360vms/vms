﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.UI.Common.Annotations;

namespace Edge360.Platform.VMS.UI.Common.View.Dialog.Overlay
{
    /// <summary>
    ///     Interaction logic for DialogOverlayGrid.xaml
    /// </summary>
    public partial class DialogOverlayGrid : Grid
    {
        private Point modalDragButtonStartPoint;

        public DialogOverlayGrid()
        {
            InitializeComponent();
            DataContext = new DialogOverlayGridViewModel(this);
        }

        private async void ModalDragButtonPreviewMouseMove(object sender, MouseEventArgs e)
        {
            var currentPoint = e.GetPosition(this);
            if (e.LeftButton == MouseButtonState.Pressed &&
                ModalDragButton.IsMouseCaptured &&
                (Math.Abs(currentPoint.X - modalDragButtonStartPoint.X) >
                 SystemParameters.MinimumHorizontalDragDistance ||
                 Math.Abs(currentPoint.Y - modalDragButtonStartPoint.Y) >
                 SystemParameters.MinimumVerticalDragDistance))
            {
                if (ModalWindow.HorizontalAlignment == HorizontalAlignment.Center)
                {
                    ModalWindow.HorizontalAlignment = HorizontalAlignment.Left;
                    ModalWindow.VerticalAlignment = VerticalAlignment.Top;
                }

                var left = Math.Min(ActualWidth - ModalWindow.ActualWidth,
                    Math.Max(0, currentPoint.X - modalDragButtonStartPoint.X));
                var top = Math.Min(ActualHeight - ModalWindow.ActualHeight,
                    Math.Max(0, currentPoint.Y - modalDragButtonStartPoint.Y));
                ModalWindow.Margin = new Thickness(
                    left,
                    top, left * -1,
                    top * -1);
            }
        }

        private void ModalDragButtonPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            modalDragButtonStartPoint = e.GetPosition(ModalDragButton);
        }
    }

    public class DialogOverlayGridViewModel : INotifyPropertyChanged
    {
        private readonly DialogOverlayGrid _view;
        private RelayCommand _closeModalWindowCommand;
        private DialogBase _dialog = new DialogBase();

        public RelayCommand CloseModalWindowCommand =>
            _closeModalWindowCommand ?? (_closeModalWindowCommand = new RelayCommand(CloseModal));

        public DialogBase Dialog
        {
            get => _dialog;
            set
            {
                if (Equals(value, _dialog))
                {
                    return;
                }

                _dialog = value;
                OnPropertyChanged();
            }
        }

        public DialogOverlayGridViewModel(DialogOverlayGrid view)
        {
            _view = view;
            if (_view?.ModalContentPresenter != null)
            {
                _view.ModalContentPresenter.DataContextChanged += ModalContentPresenterOnDataContextChanged;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void CloseModal(object obj)
        {
            if (_view?.ModalContentPresenter?.Content is DialogBase dialog)
            {
                dialog.Close();
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ModalContentPresenterOnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Dialog = _view?.ModalContentPresenter?.Content as DialogBase ?? new DialogBase();
        }
    }
}