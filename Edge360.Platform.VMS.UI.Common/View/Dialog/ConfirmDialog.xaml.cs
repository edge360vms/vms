﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using Edge360.Platform.VMS.Common.Extensions.Commands;
using Edge360.Platform.VMS.UI.Common.Annotations;
using Edge360.Platform.VMS.UI.Common.Util;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Data.DataForm;

//using Edge360.Platform.VMS.Client.Annotations;
//using Edge360.Platform.VMS.Client.Extensions;
//using Edge360.Platform.VMS.Client.ViewModel;

namespace Edge360.Platform.VMS.UI.Common.View.Dialog
{
    /// <summary>
    ///     Interaction logic for CreateCameraDialog.xaml
    /// </summary>
    public partial class ConfirmDialog
    {
        public ConfirmDialogViewModel Model => DataContext as ConfirmDialogViewModel;


        public ConfirmDialog()
        {
            InitializeComponent();
            DataContext = new ConfirmDialogViewModel(this);
            //  ObjectResolver.Register<ConfirmDialogViewModel>(DataContext);
        }

        public void OnEditEnded(object sender, EditEndedEventArgs e)
        {
            if (e.EditAction != EditAction.Cancel)
            {
                DialogResult = true;
                DialogItem = Model?.DialogItem;
            }

            Close();
        }
    }

    public class ConfirmDialogItemModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ConfirmDialogViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private RelayCommand _cancelCommand;
        private RelayCommand _confirmCommand;
        private string _confirmDescription;
        private EDialogButtons _dialogButtonType;
        private ConfirmDialogItemModel _dialogItem = new ConfirmDialogItemModel();
        private DelegateCommand _discardCommand;
        private string _glyph = Application.Current.TryFindResource("GlyphQuestion") as string;
        private string _glyphFont = Application.Current.TryFindResource("TelerikFont") as string;
        private bool _isYesNo;
        public string AcceptText => IsYesNo ? "_Yes" : "_OK";
        public RelayCommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand(Cancel, CanConfirm));
        public string CancelText
        {
            get
            {
                if (IsSaveDontSaveCancel || IsYesNoCancel)
                {
                    return "_Cancel";
                }
                return IsYesNo ? "_No" : "_Cancel";
            }
        }

        public RelayCommand ConfirmCommand =>
            _confirmCommand ?? (_confirmCommand = new RelayCommand(Confirm, CanConfirm));

        public string ConfirmDescription
        {
            get => _confirmDescription;
            set
            {
                if (value == _confirmDescription)
                {
                    return;
                }

                _confirmDescription = value;
                OnPropertyChanged();
            }
        }

        public EDialogAction DialogAction { get; set; }

        public EDialogButtons DialogButtonType
        {
            get => _dialogButtonType;
            set
            {
                if (value == _dialogButtonType)
                {
                    return;
                }

                _dialogButtonType = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsYesNoCancel));
                OnPropertyChanged(nameof(IsSaveDontSaveCancel));
            }
        }

        public ConfirmDialogItemModel DialogItem
        {
            get => _dialogItem;
            set
            {
                if (_dialogItem != value)
                {
                    _dialogItem = value;
                    OnPropertyChanged();
                }
            }
        }

        public DelegateCommand DiscardCommand => _discardCommand ??= new DelegateCommand(Discard);

        public string DiscardText => IsSaveDontSaveCancel ? "_Don't Save" : "_No";

        public string Glyph
        {
            get => _glyph;
            set
            {
                if (value == _glyph)
                {
                    return;
                }

                _glyph = value;
                OnPropertyChanged();
            }
        }

        public string GlyphFont
        {
            get => _glyphFont;
            set
            {
                if (value == _glyphFont)
                {
                    return;
                }

                _glyphFont = value;
                OnPropertyChanged();
            }
        }

        public bool IsYesNo
        {
            get => _isYesNo;
            set
            {
                if (value == _isYesNo)
                {
                    return;
                }

                _isYesNo = value;
                OnPropertyChanged();
            }
        }
        public bool IsSaveDontSaveCancel => DialogButtonType == EDialogButtons.SaveDontSaveCancel;

        public bool IsYesNoCancel => DialogButtonType == EDialogButtons.YesNoCancel;

        public ConfirmDialog View { get; set; }

        public ConfirmDialogViewModel(ConfirmDialog view)
        {
            View = view;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Discard(object obj)
        {
            DialogAction = EDialogAction.Negative;
            View?.OnEditEnded(this, new EditEndedEventArgs(EditAction.Cancel));
        }

        private void Cancel(object obj)
        {
            DialogAction = EDialogAction.Cancel;
            View?.OnEditEnded(this, new EditEndedEventArgs(EditAction.Cancel));
        }

        private void Confirm(object obj)
        {
            DialogAction = EDialogAction.Affirmative;
            View?.OnEditEnded(this, new EditEndedEventArgs(EditAction.Commit));
        }

        private bool CanConfirm(object obj)
        {
            return true;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum EDialogAction
    {
        Affirmative,
        Negative,
        Cancel
    }
}