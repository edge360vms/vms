﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;

namespace Edge360.Platform.VMS.UI.Common.View
{
    /// <summary>
    ///     Interaction logic for DraggablePopup.xaml
    /// </summary>
    [ContentProperty("Child")]
    [DefaultEvent("Opened")]
    [DefaultProperty("Child")]
    [Localizability(LocalizationCategory.None)]
    public partial class DraggablePopup : Popup
    {
        public Thumb Thumb { get; } = new Thumb
        {
            Width = 0,
            Height = 0
        };

        /// <summary>
        ///     The original child added via Xaml
        /// </summary>
        public UIElement TrueChild { get; private set; }

        public DraggablePopup()
        {
            //InitializeComponent();
            MouseDown += (sender, e) => { Thumb.RaiseEvent(e); };

            Thumb.DragDelta += (sender, e) =>
            {
                HorizontalOffset += e.HorizontalChange;
                VerticalOffset += e.VerticalChange;
            };
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            TrueChild = Child;

            var surrogateChild = new StackPanel();

            RemoveLogicalChild(TrueChild);

            surrogateChild.Children.Add(Thumb);
            surrogateChild.Children.Add(TrueChild);

            AddLogicalChild(surrogateChild);
            Child = surrogateChild;
        }
    }
}