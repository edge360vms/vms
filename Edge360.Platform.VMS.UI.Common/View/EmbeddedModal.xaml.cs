﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Edge360.Platform.VMS.UI.Common.Util;

namespace Edge360.Platform.VMS.UI.Common.View
{
    /// <summary>
    ///     Interaction logic for DraggablePopup.xaml
    /// </summary>
    //[ContentProperty("Child")]
    //[DefaultEvent("Opened")]
    //[DefaultProperty("Child")]
    //[Localizability(LocalizationCategory.None)]
    public partial class EmbeddedModal : Grid
    {
        public bool IsLocationChanging { get; set; }
        public bool IsSizeChanging { get; set; }

        public double LastKnownHeight { get; set; }

        public double LastKnownWidth { get; set; }

        public double LastKnownX { get; set; }

        public double LastKnownY { get; set; }

        public Thumb Thumb { get; } = new Thumb
        {
            Width = 0,
            Height = 0
        };

        /// <summary>
        ///     The original child added via Xaml
        /// </summary>
        //public EmbeddedModalWindow TrueChild { get; private set; }
        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            //Loaded += OnLoaded;
            //TrueChild = new EmbeddedModalWindow
            //{
            //    Background = Black,
            //    Opacity = .5
            //};
            //TrueChild.Loaded += (s, eve) =>
            //{
            //    if (TrueChild.ContentPresenter != null)
            //    {
            //        TrueChild.ContentPresenter.Content = new Grid {Background = Blue, Height = 200, Width = 200};
            //    }

            //    if (TrueChild.DraggableBar != null)
            //    {
            //        TrueChild.DraggableBar.DragDelta += (sender, args) =>
            //        {
            //            if (TrueChild.ParentStackPanel != null)
            //            {
            //                var horVal = TrueChild.ParentStackPanel.Margin.Left + args.HorizontalChange;
            //                var vertVal = TrueChild.ParentStackPanel.Margin.Top + args.VerticalChange;

            //                vertVal = Math.Max(0,
            //                    Math.Min((float) (TrueChild.ActualHeight - (TrueChild.MainBorder.ActualHeight + 2)),
            //                        vertVal));
            //                horVal = Math.Max(0,
            //                    Math.Min((float) (TrueChild.ActualWidth - (TrueChild.MainBorder.ActualWidth + 2)),
            //                        horVal));
            //                LastKnownX = horVal;
            //                LastKnownY = vertVal;
            //                LastKnownWidth = TrueChild.MainBorder.ActualWidth;
            //                LastKnownHeight = TrueChild.MainBorder.ActualHeight;
            //                TrueChild.ParentStackPanel.Margin =
            //                    new Thickness(horVal, vertVal, horVal * -1, vertVal * -1);
            //                Console.WriteLine($"{horVal}x{vertVal}");
            //            }
            //        };
            //    }
            //};
            //Children.Add(TrueChild);

            //TrueChild = Children.OfType<UIElement>().FirstOrDefault() as Grid;

            //var surrogateChild = new StackPanel();
            //RemoveLogicalChild(TrueChild);

            ////Children.Clear();
            //// RemoveLogicalChild(TrueChild);

            //surrogateChild.Children.Add(Thumb);
            //surrogateChild.Children.Add(TrueChild);
            ////foreach (var c in TrueChild)
            ////{
            ////    surrogateChild.Children.Add(c);
            ////}


            //AddLogicalChild(surrogateChild);
            //  Child = surrogateChild;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            var window = this.GetParentWindow<Window>();
            if (window != null)
            {
                window.LocationChanged += WindowOnLocationChanged;
                window.SizeChanged += WindowOnSizeChanged;
            }
        }


        private void WindowOnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!IsSizeChanging)
            {
                IsSizeChanging = true;
                try
                {
                    var horVal = LastKnownX *= e.NewSize.Width / e.PreviousSize.Width;
                    var vertVal = LastKnownY *= e.NewSize.Height / e.PreviousSize.Height;

                    //vertVal = Math.Max(0,
                    //    Math.Min((float) (TrueChild.ActualHeight - (TrueChild.MainBorder.ActualHeight + 2)),
                    //        vertVal));
                    //horVal = Math.Max(0,
                    //    Math.Min((float) (TrueChild.ActualWidth - (TrueChild.MainBorder.ActualWidth + 2)),
                    //        horVal));
                    //LastKnownX = horVal;
                    //LastKnownY = vertVal;
                    //TrueChild.ParentStackPanel.Margin =
                    //    new Thickness(LastKnownX, LastKnownY, LastKnownX * -1, LastKnownY * -1);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                }

                IsSizeChanging = false;
            }
        }

        private void WindowOnLocationChanged(object sender, EventArgs e)
        {
            if (!IsLocationChanging)
            {
                IsLocationChanging = true;
                IsLocationChanging = false;
            }
        }
    }
}