﻿using System.IO;
using System.Security.Cryptography;
using System.Text;
using Edge360.Platform.VMS.Common.Interfaces.Authority;
using Edge360.Platform.VMS.Common.Settings;
using static Edge360.Platform.VMS.Common.Encrypt.CacheUtil;
using static Edge360.Platform.VMS.Common.Util.JsonUtil;
using static Newtonsoft.Json.JsonConvert;

namespace Edge360.Platform.VMS.UI.Common.Interfaces.Ipc
{
    public class FileStorage<T> where T : new()
    {
        public string FileName { get; set; } = typeof(T)?.Name;
        public string FolderPath { get; set; } = Path.Combine(SettingsManager.VMSAppDataPath, "cache");

        public string CustomEntropy { get; set; }

        public virtual byte[] GetEntropy
        {
            get
            {
                if (!string.IsNullOrEmpty(CustomEntropy))
                {
                    return Encoding.ASCII.GetBytes(CustomEntropy);
                }

                if (Login != null)
                {
                    return Encoding.ASCII.GetBytes(Login.GetEntropy());
                }

                return default;
            }
        }

        public bool HasStorageLoaded { get; set; }
        public UserLogin Login { get; set; }
        public bool RawJson { get; set; }

        public T Storage { get; set; }

        public FileStorage()
        {
            Storage = new T();
        }

        public bool Save()
        {
            if (!Directory.Exists(FolderPath))
            {
                Directory.CreateDirectory(FolderPath);
            }

            var rawData =
                SerializeObject(Storage,
                    IgnoreExceptionSettings());
            if (RawJson)
            {
                var rawPath = Path.Combine(FolderPath,
                    $"{FileName}-{Login.DomainWithoutPort}.json");
                File.WriteAllText(rawPath, rawData);
            }
            else if (Login != null)
            {
                var filePath = Path.Combine(FolderPath,
                    $"{FileName}-{Login.DomainWithoutPort}.dat");
                var fStream = new FileStream(filePath, FileMode.OpenOrCreate);
                EncryptDataToStream(Encoding.ASCII.GetBytes(rawData), GetEntropy
                    , DataProtectionScope.CurrentUser,
                    fStream);
                fStream.Close();
            }

            return false;
        }

        public T Load()
        {
            if (RawJson)
            {
                var path = Path.Combine($"{FolderPath}",
                    $"{FileName}-{Login.DomainWithoutPort}.json");
                var data = File.ReadAllText(path);
                var layoutRecoveryCache =
                    DeserializeObject<T>(data);
                Storage = layoutRecoveryCache;
                HasStorageLoaded = true;
                return Storage;
            }
            else if (Login != null)
            {
                var path = Path.Combine($"{FolderPath}",
                    $"{FileName}-{Login.DomainWithoutPort}.dat");
                var data = File.ReadAllBytes(path);
                var fStream = new FileStream(path, FileMode.Open);
                var decryptData = DecryptDataFromStream(GetEntropy,
                    DataProtectionScope.CurrentUser, fStream, data.Length);
                fStream.Close();

                var layoutRecoveryCache =
                    DeserializeObject<T>(
                        Encoding.ASCII.GetString(decryptData));
                Storage = layoutRecoveryCache;
                HasStorageLoaded = true;
                return Storage;
            }

            return default;
        }
    }
}