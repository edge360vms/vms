﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Windows.Media;
using Edge360.Platform.VMS.Common.Taskbar;
using Edge360.Platform.VMS.Common.Util;
using log4net;
using Telerik.Windows.Controls;
using Application = System.Windows.Application;

namespace Edge360.Platform.VMS.UI.Common.Util
{
    public static class CommonWindowUtil
    {
        static ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const int GWL_EX_STYLE = -20;
        private const int WS_EX_APPWINDOW = 0x00040000;
        private const int WS_EX_TOOLWINDOW = 0x00000080;

        private const uint SWP_NOSIZE = 0x0001;
        private const uint SWP_NOMOVE = 0x0002;
        private const uint SWP_SHOWWINDOW = 0x0040;

        public static T GetParentWindow<T>(this DependencyObject child) where T : Window
        {
            if (child == null)
            {
                return default;
            }

            var parentObject = VisualTreeHelper.GetParent(child);

            if (parentObject == null)
            {
                return default;
            }

            if (parentObject is T parent)
            {
                return parent;
            }

            return GetParentWindow<T>(parentObject);
        }

        public static void SetTaskbarVisibility(this FrameworkElement control, bool visible, string title,
            bool showControl = true)
        {
            try
            {
                try
                {
                    if (showControl)
                    {
                        Application.Current?.Dispatcher?.InvokeAsync(() =>
                        {
                            try
                            {
                                if (control.GetParentWindow<Window>() is Window win)
                                {
                                    win.Show();
                                    try
                                    {
                                        TaskbarUtil.SetLaunchTarget(new WindowInteropHelper(win).Handle);
                                    }
                                    catch (Exception e)
                                    {
                                        _log?.Info(e);
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                            }
                        });
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                if (control != null)
                {
                    var window = control.ParentOfType<Window>();
                    if (window != null)
                    {
                        // hide the window from Alt+Tab
                        if (!visible)
                        {
                            try
                            {
                                var helper = new WindowInteropHelper(window).Handle;
                                SetWindowLong(helper, GWL_EX_STYLE,
                                    (GetWindowLong(helper, GWL_EX_STYLE) | WS_EX_TOOLWINDOW) & ~WS_EX_APPWINDOW);
                            }
                            catch (Exception)
                            {
                            }
                        }


                        window.ShowInTaskbar = visible;
                        window.Title = title;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            //var uri = new Uri("pack://application:,,,/CLOS_UI;component/Icon.ico");
            //window.Icon = BitmapFrame.Create(uri);
        }

        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        //public static IEnumerable<T> FindVisualChildren<T>(this DependencyObject dependencyObject) where T : DependencyObject
        //{
        //    if (dependencyObject == null)
        //        yield break;

        //    if (dependencyObject is T)
        //        yield return (T)dependencyObject;

        //    for (int i = 0; i < VisualTreeHelper.GetChildrenCount(dependencyObject); i++)
        //    {
        //        DependencyObject child = VisualTreeHelper.GetChild(dependencyObject, i);
        //        foreach (T childOfChild in FindVisualChildren<T>(__, child))
        //        {
        //            yield return childOfChild;
        //        }
        //    }
        //}

        /// <summary>
        ///     Finds a Child of a given item in the visual tree.
        /// </summary>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="childName">x:Name or Name of child. </param>
        /// <returns>
        ///     The first parent item that matches the submitted type parameter.
        ///     If not matching item can be found,
        ///     a null parent is being returned.
        /// </returns>
        //public static T FindChild<T>(this DependencyObject parent, string childName, bool partialMatch = false)
        //    where T : DependencyObject
        //{
        //    // Confirm parent and childName are valid. 
        //    if (parent == null)
        //    {
        //        return null;
        //    }

        //    T foundChild = null;

        //    var childrenCount = VisualTreeHelper.GetChildrenCount(parent);
        //    for (var i = 0; i < childrenCount; i++)
        //    {
        //        var child = VisualTreeHelper.GetChild(parent, i);
        //        // If the child is not of the request child type child
        //        if (!(child is T childType))
        //        {
        //            // recursively drill down the tree
        //            foundChild = FindChild<T>(__, child, childName);

        //            //// If the child is found, break so we do not overwrite the found child. 
        //            //if (foundChild != null)
        //            //{
        //            //    break;
        //            //}
        //        }
        //        else if (!string.IsNullOrEmpty(childName))
        //        {
        //            // If the child's name is set for search
        //            if (child is FrameworkElement frameworkElement && frameworkElement.Name == childName)
        //                //( ||
        //                // partialMatch && frameworkElement.Name.ToLower().StartsWith(childName)))
        //            {
        //                // if the child's name is of the request name
        //                foundChild = (T) child;
        //                break;
        //            }
        //        }
        //        else
        //        {
        //            // child element found.
        //            foundChild = (T) child;
        //            break;
        //        }
        //    }

        //    return foundChild;
        //}
        public static void UiThreadAsync(this Control c, Action a)
        {
            if (a == null || c == null || c.IsDisposed)
            {
                return;
            }

            try
            {
                if (c.InvokeRequired)
                {
                    c.BeginInvoke(a);
                }
                else
                {
                    a();
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        public static void UiThread(this Control c, Action a)
        {
            if (a == null || c == null || c.IsDisposed)
            {
                return;
            }

            try
            {
                if (c.InvokeRequired)
                {
                    c.Invoke(a);
                }
                else
                {
                    a();
                }
            }
            catch (Exception e)
            {
                _log?.Info(e);
            }
        }

        /// <summary>
        ///     Activate a window from anywhere by attaching to the foreground window
        /// </summary>
        public static void GlobalActivate(this Window w)
        {
            try
            {
                //Get the process ID for this window's thread
                var interopHelper = new WindowInteropHelper(w);
                var thisWindowThreadId = GetWindowThreadProcessId(interopHelper.Handle, IntPtr.Zero);

                //Get the process ID for the foreground window's thread
                var currentForegroundWindow = GetForegroundWindow();
                var currentForegroundWindowThreadId = GetWindowThreadProcessId(currentForegroundWindow, IntPtr.Zero);

                //Attach this window's thread to the current window's thread
                AttachThreadInput(currentForegroundWindowThreadId, thisWindowThreadId, true);

                //Set the window position
                SetWindowPos(interopHelper.Handle, new IntPtr(0), 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);

                //Detach this window's thread from the current window's thread
                AttachThreadInput(currentForegroundWindowThreadId, thisWindowThreadId, false);

                //Show and activate the window
                if (w.WindowState == WindowState.Minimized)
                {
                    w.WindowState = WindowState.Normal;
                }

                w.Show();
                w.Activate();
            }
            catch (Exception e)
            {
            }
        }

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        private static extern uint GetWindowThreadProcessId(IntPtr hWnd, IntPtr ProcessId);

        [DllImport("user32.dll")]
        private static extern bool AttachThreadInput(uint idAttach, uint idAttachTo, bool fAttach);

        [DllImport("user32.dll")]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy,
            uint uFlags);
    }
}