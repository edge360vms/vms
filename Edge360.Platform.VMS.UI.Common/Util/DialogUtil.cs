﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using Edge360.Platform.VMS.Common.Taskbar;
using Edge360.Platform.VMS.UI.Common.View.Dialog;
using Telerik.Windows.Controls;

namespace Edge360.Platform.VMS.UI.Common.Util
{
    public class DialogResponse
    {
        public string Response { get; set; }
        public bool Result { get; set; }
    }

    public static class DialogUtil
    {
        public const string DefaultRegex = @"^[] !@#$%^&*()_=+[{};:,.<>?'/`|~\w-]{3,512}$";
        public const string DefaultErrorMessage = "This field requires 3-512 characters and does not allow unicode.";

        public static bool MetroConfirmDialog(string caption, bool yesNoDialog = false, string title = "Confirm")
        {
            bool? result = false;

            //var dialogParams = new DialogParameters
            //{
                
            //    Content = caption, OkButtonContent = yesNoDialog ? "Yes" : "Ok",
            //    CancelButtonContent = yesNoDialog ? "No" : "Cancel"
            //};
    

            var metro = new MetroConfirm(){Description = caption, DialogTitle = title};

            metro.Loaded += (sender, args) =>
            {
                if (sender is RadWindow window)
                {
                    window.IsVisibleChanged += (o, eventArgs) =>
                    {
                        window.SetTaskbarVisibility(true, caption ?? "", false);
                        try
                        {
                            TaskbarUtil.SetLaunchTarget(
                                new WindowInteropHelper(window.GetParentWindow<Window>()).Handle);
                        }
                        catch (Exception e)
                        {
                        }

                        // window.SetTaskbarVisibility(true, caption ?? "", false);
                    };
                }
            };
            metro.Closed += (sender, args) => result = metro.Result ?? false;
            metro.ShowDialog();
            return result ?? false;
        }

        public static bool ConfirmDialog(string caption, bool yesNoDialog = false)
        {
            bool? result = false;

            var dialogParams = new DialogParameters
            {
                
                Content = caption, OkButtonContent = yesNoDialog ? "Yes" : "Ok",
                CancelButtonContent = yesNoDialog ? "No" : "Cancel"
            };
            dialogParams.Opened += (sender, args) =>
            {
                if (sender is RadWindow window)
                {
                    window.IsVisibleChanged += (o, eventArgs) =>
                    {
                        window.SetTaskbarVisibility(true, caption ?? "", false);
                        try
                        {
                            TaskbarUtil.SetLaunchTarget(
                                new WindowInteropHelper(window.GetParentWindow<Window>()).Handle);
                        }
                        catch (Exception e)
                        {
                        }

                        // window.SetTaskbarVisibility(true, caption ?? "", false);
                    };
                }
            };
            dialogParams.Closed += (sender, args) => result = args.DialogResult;

            RadWindow.Confirm(dialogParams);
            return result != null && result.Value;
        }

        public static void TryValidate(this object obj, object value, [CallerMemberName] string memberName = null)
        {
            try
            {
                if (obj != null)
                {
                    var validationContext = new ValidationContext(obj) {MemberName = memberName};
                    Validator.ValidateProperty(value, validationContext);
                }
            }
            catch (ValidationException e)
            {
                throw new ValidationException(e.Message);
            }
        }

        public static DialogResponse InputDialog(object caption, bool yesNoDialog = false)
        {
            bool? result = false;
            var output = "";

            var dialogParams = new DialogParameters
            {
                Content = caption, OkButtonContent = yesNoDialog ? "Yes" : "Ok",
                CancelButtonContent = yesNoDialog ? "No" : "Cancel", Header = ""
            };
            dialogParams.Opened += (sender, args) =>
            {
                if (sender is RadWindow window)
                {
                    window.IsVisibleChanged += (o, eventArgs) =>
                    {
                        window.SetTaskbarVisibility(true, caption as string ?? "", false);
                        try
                        {
                            TaskbarUtil.SetLaunchTarget(
                                new WindowInteropHelper(window.GetParentWindow<Window>()).Handle);
                        }
                        catch (Exception e)
                        {
                        }
                    };
                }
            };
            dialogParams.Closed += (sender, args) =>
            {
                output = args.PromptResult;
                result = args.DialogResult;
            };

            RadWindow.Prompt(dialogParams);

            if (result != null && result.Value)
            {
                return new DialogResponse
                {
                    Response = output,
                    Result = result.Value
                };
            }

            return new DialogResponse();
        }

        public static async Task<ConfirmDialog> OverlayDialog(ContentPresenter contentPresenter,
            EmbeddedDialogParameters parameters = null)
        {
            if (parameters == null)
            {
                parameters = new EmbeddedDialogParameters();
            }

            var tcs = new TaskCompletionSource<ConfirmDialog>();

            if (contentPresenter != null)
            {
                var confirm = new ConfirmDialog();
                confirm.Model.Glyph = parameters.Glyph;
                confirm.Model.GlyphFont = parameters.GlyphFont;
                confirm.Header = parameters.Header;
                confirm.Model.IsYesNo = parameters.DialogButtons == EDialogButtons.YesNo;
                confirm.Model.DialogButtonType = parameters.DialogButtons;
                confirm.Model.ConfirmDescription = parameters.Description;
                confirm.SetModalView(contentPresenter);
                confirm.Closed += async (sender, args) => { tcs.SetResult(confirm); };
            }

            var response = await tcs.Task;

            return response;
        }

        public static async Task<bool> EmbeddedModalConfirmDialog(ContentPresenter contentPresenter,
            EmbeddedDialogParameters parameters = null)
        {
            if (parameters == null)
            {
                parameters = new EmbeddedDialogParameters();
            }

            var tcs = new TaskCompletionSource<bool>();

            if (contentPresenter != null)
            {
                var confirm = new ConfirmDialog();
                confirm.Model.Glyph = parameters.Glyph;
                confirm.Model.GlyphFont = parameters.GlyphFont;
                confirm.Header = parameters.Header;
                confirm.Model.IsYesNo = parameters.DialogButtons == EDialogButtons.YesNo;
                confirm.Model.DialogButtonType = parameters.DialogButtons;
                confirm.Model.ConfirmDescription = parameters.Description;
                confirm.SetModalView(contentPresenter);
                confirm.Closed += async (sender, args) => { tcs.SetResult(confirm.Result); };
            }

            var response = await tcs.Task;

            return response;
        }
    }

    public enum EDialogButtons
    {
        OkCancel,
        YesNo,
        YesNoCancel,
        SaveDontSaveCancel,
    }

    public class EmbeddedDialogParameters
    {
        public string Description { get; set; } = "";
        public EDialogButtons DialogButtons { get; set; } = EDialogButtons.OkCancel;
        public string Glyph { get; } = Application.Current.TryFindResource("GlyphQuestion") as string;
        public string GlyphFont { get; set; } = Application.Current.TryFindResource("TelerikFont") as string;
        public string Header { get; set; } = "Are you sure?";
    }
}