﻿using System;
using System.Windows.Media;

namespace Edge360.Platform.VMS.UI.Common.Util
{
    public static class FontUtil
    {
        public static FontFamily GetFontAwesomeFont()
        {
            try
            {
                var fontAwesomeFont =
                    new FontFamily(
                        new Uri("pack://application:,,,/Edge360.Platform.VMS.UI.Common;component/Resources/Fonts/"),
                        "./#Font Awesome 5 Pro");
                return fontAwesomeFont;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return default;
        }
    }
}