﻿using System;

namespace Edge360.Platform.VMS.Updater.Shared
{
    /// <summary>
    /// Class To Store Allowed Update Schedule Ranges
    /// </summary>
    [Serializable]
    public class UpdateSchedule
    {
        public static readonly TimeSpan MinTime = TimeSpan.Zero; // Midnight
        public static readonly TimeSpan MaxTime = new TimeSpan(23,59,59); // 11:59:59 PM

        private DaysOfWeek _daysOfWeek = DaysOfWeek.Any;

        /// <summary>
        /// Days Of The Week The Schedule Is Allowed (0 = Any Day)
        /// </summary>
        public DaysOfWeek DaysOfWeek
        {
            get => _daysOfWeek;
            set
            {
                // Only Use Correctly Set Flag Values
                _daysOfWeek =  value & (DaysOfWeek.Sunday | DaysOfWeek.Monday | DaysOfWeek.Tuesday | DaysOfWeek.Wednesday |
                                   DaysOfWeek.Thursday | DaysOfWeek.Friday | DaysOfWeek.Saturday);
            }
        }


        private TimeSpan _startTime = MinTime;
        /// <summary>
        /// Start Time Range (Midnight + StartTime)
        /// </summary>
        public TimeSpan StartTime
        {
            get
            {
                if (_startTime <= _endTime)
                {
                    return _startTime;
                }
                else
                {
                    // Do Not Allow End Time Before Start Time (Runtime Swap)
                    return _endTime;
                }
            }
            set
            {
                if (value < MinTime)
                {
                    _startTime = MinTime;
                }
                else if (value > MaxTime)
                {
                    _startTime = MaxTime;
                }
                else
                {
                    _startTime = value;    
                }
            }
        }

        private TimeSpan _endTime = MaxTime;
        /// <summary>
        /// End Time Range (Midnight + StartTime)
        /// </summary>
        public TimeSpan EndTime
        {
            get
            {
                if (_endTime >= _startTime)
                {
                    return _endTime;
                }
                else
                {
                    // Do Not Allow End Time Before Start Time (Runtime Swap)
                    return _startTime;
                }
            }
            set
            {
                if (value < MinTime)
                {
                    _endTime = MinTime;
                }
                else if (value > MaxTime)
                {
                    _endTime = MaxTime;
                }
                else
                {
                    _endTime = value;    
                }
            }
        }

        public bool ValidDateTime(DateTime checkDateTime)
        {
            if (ValidDay(checkDateTime.DayOfWeek))
            {
                return checkDateTime >= DateTime.Today.Add(_startTime) && checkDateTime <= DateTime.Today.Add(_endTime);
            }

            return false;
        }

        public DateTime NextValidDateTime(DateTime checkDateTime)
        {
            if (ValidDateTime(checkDateTime))
            {
                return checkDateTime;
            }

            if (ValidDay(checkDateTime.DayOfWeek) && checkDateTime < DateTime.Today.Add(_startTime))
            {
                return DateTime.Today.Add(_startTime);
            }

            for (int addDays = 1; addDays < 7; addDays++)
            {
                checkDateTime = DateTime.Today.AddDays(addDays);
                if (ValidDay(checkDateTime.DayOfWeek))
                {
                    return checkDateTime.Add(_startTime);
                }
            }

            return DateTime.MaxValue;
        }

        private bool ValidDay(DayOfWeek day)
        {
            if (_daysOfWeek == DaysOfWeek.Any)
            {
                // Any Day
                return true;
            }

            // Check For Correct Flag
            switch (day)
            {
                case DayOfWeek.Sunday:
                    return _daysOfWeek.HasFlag(DaysOfWeek.Sunday);
                case DayOfWeek.Monday:
                    return _daysOfWeek.HasFlag(DaysOfWeek.Monday);
                case DayOfWeek.Tuesday:
                    return _daysOfWeek.HasFlag(DaysOfWeek.Tuesday);
                case DayOfWeek.Wednesday:
                    return _daysOfWeek.HasFlag(DaysOfWeek.Wednesday);
                case DayOfWeek.Thursday:
                    return _daysOfWeek.HasFlag(DaysOfWeek.Thursday);
                case DayOfWeek.Friday:
                    return _daysOfWeek.HasFlag(DaysOfWeek.Friday);
                case DayOfWeek.Saturday:
                    return _daysOfWeek.HasFlag(DaysOfWeek.Saturday);
            }

            // Otherwise False
            return false;
        }
    }

    [Flags]
    public enum DaysOfWeek
    {
        Any = 0,
        Monday = 1 << 0,
        Tuesday = 1 << 1,
        Wednesday = 1 << 2,
        Thursday = 1 << 3,
        Friday = 1 << 4,
        Saturday = 1 << 5,
        Sunday = 1 << 6
    }
}
