﻿using System;
using System.Collections.Generic;

namespace Edge360.Platform.VMS.Updater.Shared
{
    //
    // Summary:
    //     EXPORTED FROM APP LIFE TO SHARE UPDATE DATA EXTERNALLY -->
    //     Contains information about an update available for download or installation.
    public class UpdateInformation
    {
        /// <summary>
        /// Conversion Helper From AppLife Version
        /// </summary>
        /// <param name="historicalSummary"></param>
        /// <param name="fileSize"></param>
        /// <param name="isDownloaded"></param>
        /// <param name="customData"></param>
        /// <param name="translatedHistoricalSummary"></param>
        /// <param name="translatedHistoricalSummaryHtml"></param>
        /// <param name="historicalSummaryHtml"></param>
        /// <param name="allowChaining"></param>
        /// <param name="overrideElevationType"></param>
        /// <param name="simpleSummaryHtml"></param>
        /// <param name="translatedSimpleSummary"></param>
        /// <param name="simpleSummary"></param>
        /// <param name="postedDateUtc"></param>
        /// <param name="postedDate"></param>
        /// <param name="version"></param>
        /// <param name="translatedSimpleSummaryHtml"></param>
        public UpdateInformation(string historicalSummary, long fileSize, bool isDownloaded, string customData,
            Dictionary<string, string> translatedHistoricalSummary,
            Dictionary<string, string> translatedHistoricalSummaryHtml, string historicalSummaryHtml,
            bool allowChaining, UpdateElevationType? overrideElevationType, string simpleSummaryHtml,
            Dictionary<string, string> translatedSimpleSummary, string simpleSummary, DateTime postedDateUtc,
            DateTime postedDate, Version version, Dictionary<string, string> translatedSimpleSummaryHtml)
        {
            HistoricalSummary = historicalSummary;
            FileSize = fileSize;
            IsDownloaded = isDownloaded;
            CustomData = customData;
            TranslatedHistoricalSummary = translatedHistoricalSummary;
            TranslatedHistoricalSummaryHtml = translatedHistoricalSummaryHtml;
            HistoricalSummaryHtml = historicalSummaryHtml;
            AllowChaining = allowChaining;
            OverrideElevationType = overrideElevationType;
            SimpleSummaryHtml = simpleSummaryHtml;
            TranslatedSimpleSummary = translatedSimpleSummary;
            SimpleSummary = simpleSummary;
            PostedDateUtc = postedDateUtc;
            PostedDate = postedDate;
            Version = version;
            TranslatedSimpleSummaryHtml = translatedSimpleSummaryHtml;
        }
        
        //
        // Summary:
        //     Gets the historical summary text for the update, including summaries for all
        //     updates between the current version and the update's version.
        //
        // Remarks:
        //     This property is created by appending all summaries from updates between the
        //     Kjs.AppLife.Update.Controller.UpdateController's Kjs.AppLife.Update.Controller.UpdateController.Version
        //     and this update's Kjs.AppLife.Update.Controller.UpdateInformation.Version together,
        //     separated by blank lines.
        //     Kjs.AppLife.Update.Controller.UpdateInformation.HistoricalSummary is only valid
        //     for updates in a controller's AvailableUpdates collection. Other updates in the
        //     AllUpdates collection will have a blank Kjs.AppLife.Update.Controller.UpdateInformation.HistoricalSummary.
        //     To cause an update to appear in AvailableUpdates, ensure that the Kjs.AppLife.Update.Controller.UpdateController's
        //     Version property is set to a Kjs.AppLife.Update.Controller.UpdateInformation.Version
        //     that the update applies to (Kjs.AppLife.Update.Controller.UpdateInformation.AppliesToVersion(System.Version)
        //     will return true).
        public string HistoricalSummary { get; }
        //
        // Summary:
        //     Gets the size, in bytes, of the update package file.
        public long FileSize { get; }
        //
        // Summary:
        //     Gets a value indicating whether the update has been downloaded.
        //
        // Remarks:
        //     This value will only be true for the Kjs.AppLife.Update.Controller.UpdateInformation
        //     returned by UpdateController.CurrentUpdate after calling the Kjs.AppLife.Update.Controller.UpdateController.DownloadUpdate,
        //     Kjs.AppLife.Update.Controller.UpdateController.DownloadUpdateAsync, or Overload:Kjs.AppLife.Update.Controller.UpdateController.ShowDownloadUpdateDialog
        //     methods.
        public bool IsDownloaded { get; }
        //
        // Summary:
        //     Gets custom data for the update.
        //
        // Remarks:
        //     Custom data is not displayed to the user.
        public string CustomData { get; }
        //
        // Summary:
        //     Gets the translated historical summary text for the update, including summaries
        //     for all updates between the current version and the update's version.
        //
        // Remarks:
        //     This property is created by appending all translated summaries from updates between
        //     the Kjs.AppLife.Update.Controller.UpdateController's Kjs.AppLife.Update.Controller.UpdateController.Version
        //     and this update's Kjs.AppLife.Update.Controller.UpdateInformation.Version together,
        //     separated by blank lines. The dictionary key is the locale of the tranlsation.
        //     Kjs.AppLife.Update.Controller.UpdateInformation.TranslatedHistoricalSummary is
        //     only valid for updates in a controller's AvailableUpdates collection. Other updates
        //     in the AllUpdates collection will have a blank Kjs.AppLife.Update.Controller.UpdateInformation.TranslatedHistoricalSummary.
        //     To cause an update to appear in AvailableUpdates, ensure that the Kjs.AppLife.Update.Controller.UpdateController's
        //     Version property is set to a Kjs.AppLife.Update.Controller.UpdateInformation.Version
        //     that the update applies to (Kjs.AppLife.Update.Controller.UpdateInformation.AppliesToVersion(System.Version)
        //     will return true).
        public Dictionary<string, string> TranslatedHistoricalSummary { get; }
        //
        // Summary:
        //     Gets the translated HTML historical summary text for the update, including summaries
        //     for all updates between the current version and the update's version.
        //
        // Remarks:
        //     This property is created by appending all translated rich summaries from updates
        //     between the Kjs.AppLife.Update.Controller.UpdateController's Kjs.AppLife.Update.Controller.UpdateController.Version
        //     and this update's Kjs.AppLife.Update.Controller.UpdateInformation.Version together,
        //     separated by blank lines. The dictionary key is the locale of the tranlsation.
        //     Kjs.AppLife.Update.Controller.UpdateInformation.TranslatedHistoricalSummaryHtml
        //     is only valid for updates in a controller's AvailableUpdates collection. Other
        //     updates in the AllUpdates collection will have a blank Kjs.AppLife.Update.Controller.UpdateInformation.TranslatedHistoricalSummaryHtml.
        //     To cause an update to appear in AvailableUpdates, ensure that the Kjs.AppLife.Update.Controller.UpdateController's
        //     Version property is set to a Kjs.AppLife.Update.Controller.UpdateInformation.Version
        //     that the update applies to (Kjs.AppLife.Update.Controller.UpdateInformation.AppliesToVersion(System.Version)
        //     will return true).
        public Dictionary<string, string> TranslatedHistoricalSummaryHtml { get; }
        //
        // Summary:
        //     Gets the HTML historical summary text for the update, including summaries for
        //     all updates between the current version and the update's version.
        //
        // Remarks:
        //     This property is created by appending all rich summaries from updates between
        //     the Kjs.AppLife.Update.Controller.UpdateController's Kjs.AppLife.Update.Controller.UpdateController.Version
        //     and this update's Kjs.AppLife.Update.Controller.UpdateInformation.Version together,
        //     separated by blank lines.
        //     Kjs.AppLife.Update.Controller.UpdateInformation.HistoricalSummary is only valid
        //     for updates in a controller's AvailableUpdates collection. Other updates in the
        //     AllUpdates collection will have a blank Kjs.AppLife.Update.Controller.UpdateInformation.HistoricalSummary.
        //     To cause an update to appear in AvailableUpdates, ensure that the Kjs.AppLife.Update.Controller.UpdateController's
        //     Version property is set to a Kjs.AppLife.Update.Controller.UpdateInformation.Version
        //     that the update applies to (Kjs.AppLife.Update.Controller.UpdateInformation.AppliesToVersion(System.Version)
        //     will return true).
        public string HistoricalSummaryHtml { get; }
        //
        // Summary:
        //     Gets a value that indicates whether further updates can be changed from the update.
        public bool AllowChaining { get; }
        //
        // Summary:
        //     Gets the elevation type that will be used when applying this update, or null
        //     if the setting specified on the controller will be used.
        public UpdateElevationType? OverrideElevationType { get; }
        //
        // Summary:
        //     Gets the HTML summary text for the update.
        //
        // Remarks:
        //     This property contains only the rich summary text entered for the update itself.
        public string SimpleSummaryHtml { get; }
        //
        // Summary:
        //     Gets any available localized translations of the summary text for this update.
        //     A dictionary of any available localized translations of the summary text for
        //     this update. If no alternate translations were provided, this dictionary will
        //     be empty. Default is en-us.
        public Dictionary<string, string> TranslatedSimpleSummary { get; }
        //
        // Summary:
        //     Gets the summary text for the update.
        //
        // Remarks:
        //     This property contains only the summary text entered for the update itself.
        public string SimpleSummary { get; }
        //
        // Summary:
        //     Gets the date and time the update was posted in coordinated universal time (UTC).
        public DateTime PostedDateUtc { get; }
        //
        // Summary:
        //     Gets the date and time the update was posted in local time.
        public DateTime PostedDate { get; }
        //
        // Summary:
        //     Gets the version of the available update.
        public Version Version { get; }
        //
        // Summary:
        //     Gets any available localized translations of the html summary text for this update.
        //     A dictionary of any available localized translations of the html summary text
        //     for this update. If no alternate translations were provided, this dictionary
        //     will be empty. Default is en-us.
        public Dictionary<string, string> TranslatedSimpleSummaryHtml { get; }

        //
        // *** Method Not Implemented ***
        //
        // Summary:
        //     Determines whether the update applies to the specified version.
        //
        // Parameters:
        //   version:
        //     A Kjs.AppLife.Update.Controller.UpdateInformation.Version to test.
        //
        // Returns:
        //     true if the update represented by the Kjs.AppLife.Update.Controller.UpdateInformation
        //     can be applied to version; otherwise, false.
        //
        // Remarks:
        //     Any update in the AllUpdates collection can be downloaded by setting it to CurrentUpdate
        //     and calling one of the DownloadUpdate methods. However, only updates that apply
        //     to a controller's Version can be applied by that controller. To see the list
        //     of updates that applies to the controller's current Version, check the AvailableUpdates
        //     collection.
        public bool AppliesToVersion(Version version)
        {
            throw new NotImplementedException("This Method Is Not Implemented In Exportable Version ");
        }
    }
}