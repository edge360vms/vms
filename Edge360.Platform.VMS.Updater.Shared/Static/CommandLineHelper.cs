﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Edge360.Platform.VMS.Updater.Shared.Static
{
    public static class CommandLineHelper
    {
        private static readonly ILog Log = LogManager.GetLogger
            (MethodBase.GetCurrentMethod().DeclaringType);

        public static string[] Args = null;


        public static string ArgString()
        {
            return ArgString(Args);
        }

        public static string ArgString(string[] args)
        {
            return args != null ? string.Join(" ", args) : "(null)";
        }
        
        public static int FindArgumentIndex(string[] args, string search)
        {
            if (args != null && args.Length > 0)
            {
                for (var i = 0; i < args.Length; i++)
                {
                    if (args[i].IndexOf(search, StringComparison.InvariantCultureIgnoreCase) >= 0)
                    {
                        return i;
                    }
                }
            }

            return -1;
        }
        
        public static bool HasArgument(string search)
        {
            return HasArgument(Args, search);
        }

        public static bool HasArgument(string[] args, string search)
        {
            return FindArgumentIndex(args, search) >= 0;
        }

        public static string FindArgumentValue(string search, string defaultValue = null)
        {
            return FindArgumentValue(Args, search, defaultValue);
        }

        public static string FindArgumentValue(string[] args, string search, string defaultValue = null)
        {
            if (args != null && args.Length > 0)
            {
                var argIndex = FindArgumentIndex(args, search);
                if (argIndex >= 0)
                {
                    if (args.Length > argIndex + 1)
                    {
                        return args[argIndex + 1];
                    }
                }
            }

            return defaultValue;
        }

        public static int FindArgumentValueInt(string search, int defaultValue, int? minValue = null,
            int? maxValue = null)
        {
            return FindArgumentValueInt(Args, search, defaultValue, minValue, maxValue);
        }

        public static int FindArgumentValueInt(string[] args, string search, int defaultValue, int? minValue=null, int? maxValue=null)
        {
            var result = FindArgumentValue(args, search);
            if (!string.IsNullOrWhiteSpace(result))
            {
                if (int.TryParse(result, out var resultInt))
                {
                    if (minValue != null && resultInt < minValue.Value)
                    {
                        return defaultValue;
                    }

                    if (maxValue != null && resultInt > maxValue.Value)
                    {
                        return defaultValue;
                    }

                    return resultInt;
                }

                Log.Error($"Error Parsing Int Value (Fallback To Default) - {search} - {result}");
            }

            return defaultValue;
        }

        public static TimeSpan FindArgumentValueTimeSpan(string search, TimeSpan defaultValue)
        {
            return FindArgumentValueTimeSpan(Args, search, defaultValue);
        }

        public static TimeSpan FindArgumentValueTimeSpan(string[] args, string search, TimeSpan defaultValue)
        {
            var result = FindArgumentValue(args, search);
            if (!string.IsNullOrWhiteSpace(result))
            {
                if (int.TryParse(result, out var resultInt))
                {
                    if (resultInt <= 0)
                    {
                        return TimeSpan.MaxValue;
                    }

                    return TimeSpan.FromMinutes(resultInt);
                }

                Log.Error($"Error Parsing Int Value (Fallback To Default) - {search} - {result}");
            }

            return defaultValue;
        }
    }
}