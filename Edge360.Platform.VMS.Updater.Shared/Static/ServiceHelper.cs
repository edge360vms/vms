﻿using System;
using System.Reflection;
using System.ServiceProcess;
using log4net;

namespace Edge360.Platform.VMS.Updater.Shared.Static
{
    public static class ServiceHelper
    {
        private static readonly ILog Log = LogManager.GetLogger
            (MethodBase.GetCurrentMethod().DeclaringType);

        public static string UpdateServiceName = "Edge360VMSClientUpdater";
        public static int DefaultTimeout = 10 * 1000; // 10 Secs
        
        public static bool StartService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                return true;
            }
            catch(Exception e)
            {
                Log.Error($"Error Starting Service - {serviceName}", e);
                return false;
            }
        }

        public static bool StopService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                return true;
            }
            catch(Exception e)
            {
                Log.Error($"Error Stopping Service - {serviceName}", e);
                return false;
            }
        }

        public static bool RestartService(string serviceName, int timeoutMilliseconds)
        {
            StopService(serviceName, timeoutMilliseconds);
            return StartService(serviceName, timeoutMilliseconds);
        }
    }
}
