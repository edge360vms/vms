﻿using System;
using System.IO;
using System.Reflection;
using log4net;

namespace Edge360.Platform.VMS.Updater.Shared
{
    public class UpdateInstallManager
    {
        private static readonly ILog Log = LogManager.GetLogger
            (MethodBase.GetCurrentMethod().DeclaringType);

        public static bool UninstallAll()
        {
            try
            {
                Log.Info("Uninstalling...");
                var installPath = Assembly.GetExecutingAssembly().Location;

                var installedList =
                    Directory.GetFiles(installPath, UpdateManager.AppFileNamePattern, SearchOption.AllDirectories);

                foreach (var installed in installedList)
                {
                    try
                    {
                        var installedClientPath = Path.GetDirectoryName(installed);
                        if (Directory.Exists(installedClientPath))
                        {
                            Log.Debug($"Removing {installedClientPath}");
                            Directory.Delete(installedClientPath, true);
                        }
                    }
                    catch (Exception e)
                    {
                        Log.Error($"Error Removing Client Install - {installed}",e);
                    }
                }

                Log.Debug("Uninstall Complete");

                return true;
            }
            catch (Exception e)
            {
                Log.Error("Error Uninstalling",e);
                return false;
            }

        }
    }
}
