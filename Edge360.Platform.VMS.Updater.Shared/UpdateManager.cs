﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Timers;
using Edge360.Platform.VMS.Updater.Shared.Extensions;
using Edge360.Platform.VMS.Updater.Shared.Settings;
using log4net;

namespace Edge360.Platform.VMS.Updater.Shared
{
    public static class UpdateManager
    {
        private static readonly ILog Log = LogManager.GetLogger
            (MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly UpdaterConfiguration Settings = UpdaterSettings.Instance.Settings;
        private static readonly UpdaterHistoryConfiguration History = UpdaterHistorySettings.Instance.Settings;

        private static readonly object VmsClientUpdateSystemLock = new object();
        private static VmsClientUpdateSystem _vmsClientUpdateSystem = null;

        public const string AppFileNamePattern = "*Edge360.Platform.VMS.Client.exe";

        public static void Start()
        {
            // Wait For Updater Settings To Init
            UpdaterSettings.Instance.InitializeAsync().Wait(TimeSpan.FromMinutes(1));
            Settings.PropertyChanged += SettingsOnPropertyChanged;

            // Wait For Updater History Settings To Init
            UpdaterHistorySettings.Instance.InitializeAsync().Wait(TimeSpan.FromMinutes(1));
        }

        public static void Stop()
        {
            AutomaticUpdateCheckStopTimer();
        }


        public static bool ConfigureUpdateSystem()
        {
            lock (VmsClientUpdateSystemLock)
            {
                if (_vmsClientUpdateSystem == null)
                {
                    _vmsClientUpdateSystem = new VmsClientUpdateSystem();
                }

                if (_vmsClientUpdateSystem.UpdateActive)
                {
                    // Do Not Provide Access When Install Already In Progress
                    return false;
                }

                var updateLocations = new ConcurrentBag<UpdateLocation>();

                // Static Update Locations
                Settings.UpdateLocations?.ForEach(e => updateLocations.Add(e));

                // Historical Update Location (Limit To Max Count Setting)
                History.UpdateLocations?.TakeLast(Settings.UpdateLocationHistoryCount)?.ToList()
                    ?.ForEach(updateLocation => updateLocations.Add(updateLocation));

                // Set Update Locations
                _vmsClientUpdateSystem.UpdateLocations = updateLocations;

                // Set Options
                _vmsClientUpdateSystem.EnableTestUpdates = Settings.TestUpdatesEnabled;
                _vmsClientUpdateSystem.EnableAutoChaining = Settings.UpdateChainingEnabled;

                // Set Configuration
                _vmsClientUpdateSystem.BasePath = Settings.ApplicationBasePath;
                _vmsClientUpdateSystem.ExePattern = Settings.ApplicationExePattern;

                return true;
            }
        }


        private static readonly object AutomaticUpdateCheckLock = new object();
        private static readonly TimeSpan AutomaticUpdateCheckInternalInterval = TimeSpan.FromSeconds(30);
        private static Timer _automaticUpdateCheckTimer;

        private static DateTime _automaticUpdateCheckNextCheckDateTime = DateTime.Now;
        private static DateTime AutomaticUpdateCheckNextCheckDateTime
        {
            get
            {
                lock (AutomaticUpdateCheckLock)
                {
                    return _automaticUpdateCheckNextCheckDateTime;
                }
            }
            set
            {
                lock (AutomaticUpdateCheckLock)
                {
                    try
                    {
                        // Override with calculated next check time if MinValue
                        if (DateTime.MinValue.Equals(value))
                        {
                            _automaticUpdateCheckNextCheckDateTime =
                                Settings.AutomaticUpdateScheduleFindNext(
                                    DateTime.Now.Add(Settings.AutomaticUpdateCheckInterval));

                            if (DateTime.MinValue.Equals(_automaticUpdateCheckNextCheckDateTime))
                            {
                                // No Valid Next Time Found In Schedule?? (Should Not Happen)
                                _automaticUpdateCheckNextCheckDateTime =
                                    DateTime.Now.Add(TimeSpan.FromMinutes(10)); // Default to 10 Minutes
                            }
                        }
                        else
                        {
                            _automaticUpdateCheckNextCheckDateTime = value;
                        }
                    }
                    catch (Exception e)
                    {
                        Trace.TraceError($"Error Calculating Next Auto Update Check {e.Message}");
                        _automaticUpdateCheckNextCheckDateTime =
                            DateTime.Now.Add(TimeSpan.FromMinutes(10)); // Default to 10 Minutes
                    }
                }
            }
        }



        private static void AutomaticUpdateCheckStartTimer()
        {
            lock (AutomaticUpdateCheckLock)
            {
                if (_automaticUpdateCheckTimer == null)
                {
                    _automaticUpdateCheckTimer = new Timer
                    {
                        AutoReset = false,
                        Enabled = false,
                        Interval = AutomaticUpdateCheckInternalInterval.TotalMilliseconds
                    };
                    _automaticUpdateCheckTimer.Elapsed += async (sender, e) =>
                    {
                        await AutomaticUpdateCheckTimerElapsedAsync();
                    };
                }

                AutomaticUpdateCheckNextCheckDateTime = DateTime.MinValue; // Calculate Next Check DateTime
                _automaticUpdateCheckTimer.Start();
            }
        }

        private static void AutomaticUpdateCheckStopTimer()
        {
            lock (AutomaticUpdateCheckLock)
            {
                _automaticUpdateCheckTimer?.Stop();
            }
        }


        private static async Task AutomaticUpdateCheckTimerElapsedAsync()
        {
            try
            {
                // Get Next Check Time and Return Lock
                var nextCheckTime = AutomaticUpdateCheckNextCheckDateTime;

                // Check if Valid Time
                if (nextCheckTime <= DateTime.Now)
                {
                    // Perform Check
                    await PerformAutomaticUpdateCheckAsync();
                }
            }
            catch (Exception e)
            {
                // Error Performing Update Check
                Trace.TraceError($"Error Calculating Next Auto Update Check {e.Message}");
            }
            finally
            {
                if (Settings.AutomaticUpdatesEnabled)
                {
                    // Reset Timer For Next Check
                    AutomaticUpdateCheckStartTimer();
                }
            }
        }

        public static async Task PerformAutomaticUpdateCheckAsync()
        {
            // Make Sure We Are Within The Authorized Schedule
            if (Settings.AutomaticUpdateScheduleCheck())
            {
                await PerformUpdateCheckAsync();
            }
        }

        public static async Task PerformUpdateCheckAsync()
        {
            // Init Update System
            if (!ConfigureUpdateSystem())
            {
                return;
            }

            await Task.Run(() =>
            {
                if (_vmsClientUpdateSystem.ApplyUpdate())
                {
                    // Update Installed!

                    // Send Update Installed Notification
                }
            });
        }


        
        private static readonly object AutomaticCleanupCheckLock = new object();
        private static readonly TimeSpan AutomaticCleanupCheckInternalInterval = TimeSpan.FromSeconds(30);
        private static Timer _automaticCleanupCheckTimer;

        private static DateTime _automaticCleanupCheckNextCheckDateTime = DateTime.Now;
        private static DateTime AutomaticCleanupCheckNextCheckDateTime
        {
            get
            {
                lock (AutomaticCleanupCheckLock)
                {
                    return _automaticCleanupCheckNextCheckDateTime;
                }
            }
            set
            {
                lock (AutomaticCleanupCheckLock)
                {
                    try
                    {
                        // Override with calculated next check time if MinValue
                        if (DateTime.MinValue.Equals(value))
                        {
                            _automaticCleanupCheckNextCheckDateTime =
                                Settings.AutomaticCleanupScheduleFindNext(
                                    DateTime.Now.Add(Settings.AutomaticCleanupInterval));

                            if (DateTime.MinValue.Equals(_automaticCleanupCheckNextCheckDateTime))
                            {
                                // No Valid Next Time Found In Schedule?? (Should Not Happen)
                                _automaticCleanupCheckNextCheckDateTime =
                                    DateTime.Now.Add(TimeSpan.FromMinutes(10)); // Default to 10 Minutes
                            }
                        }
                        else
                        {
                            _automaticCleanupCheckNextCheckDateTime = value;
                        }
                    }
                    catch (Exception e)
                    {
                        Trace.TraceError($"Error Calculating Next Auto Cleanup Check {e.Message}");
                        _automaticCleanupCheckNextCheckDateTime =
                            DateTime.Now.Add(TimeSpan.FromMinutes(10)); // Default to 10 Minutes
                    }
                }
            }
        }


        private static void AutomaticCleanupCheckStartTimer()
        {
            lock (AutomaticCleanupCheckLock)
            {
                if (_automaticCleanupCheckTimer == null)
                {
                    _automaticCleanupCheckTimer = new Timer
                    {
                        AutoReset = false,
                        Enabled = false,
                        Interval = AutomaticCleanupCheckInternalInterval.TotalMilliseconds
                    };
                    _automaticCleanupCheckTimer.Elapsed += async (sender, e) =>
                    {
                        await AutomaticCleanupCheckTimerElapsedAsync();
                    };
                }

                AutomaticCleanupCheckNextCheckDateTime = DateTime.MinValue; // Calculate Next Check DateTime
                _automaticCleanupCheckTimer.Start();
            }
        }

        private static void AutomaticCleanupCheckStopTimer()
        {
            lock (AutomaticCleanupCheckLock)
            {
                _automaticCleanupCheckTimer?.Stop();
            }
        }


        private static async Task AutomaticCleanupCheckTimerElapsedAsync()
        {
            try
            {
                // Get Next Check Time and Return Lock
                var nextCheckTime = AutomaticCleanupCheckNextCheckDateTime;

                // Check if Valid Time
                if (nextCheckTime <= DateTime.Now)
                {
                    // Perform Check
                    await PerformAutomaticCleanupCheckAsync();
                }
            }
            catch (Exception e)
            {
                // Error Performing Update Check
                Trace.TraceError($"Error Calculating Next Auto Cleanup Check {e.Message}");
            }
            finally
            {
                if (Settings.AutomaticCleanupEnabled)
                {
                    // Reset Timer For Next Check
                    AutomaticCleanupCheckStartTimer();
                }
            }
        }

        public static async Task PerformAutomaticCleanupCheckAsync()
        {
            // Make Sure We Are Within The Authorized Schedule
            if (Settings.AutomaticCleanupScheduleCheck())
            {
                await PerformCleanupCheckAsync();
            }
        }

        public static async Task PerformCleanupCheckAsync()
        {
            await Task.Run(() =>
            {
                // Perform Cleanup
            });
        }


        /// <summary>
        ///     Update Manager Based On Current Settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void SettingsOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(e?.PropertyName))
            {
                switch (e.PropertyName)
                {
                    case "AutomaticUpdatesEnabled":
                        if (Settings.AutomaticUpdatesEnabled)
                        {
                            AutomaticUpdateCheckNextCheckDateTime = DateTime.MinValue; // Calculate Next Check DateTime
                            AutomaticUpdateCheckStartTimer();
                        }
                        else
                        {
                            AutomaticUpdateCheckStopTimer();
                        }

                        break;
                    case "AutomaticUpdateCheckInterval":
                    case "AutomaticUpdateSchedules":
                        AutomaticUpdateCheckNextCheckDateTime = DateTime.MinValue; // Calculate Next Check DateTime
                        AutomaticUpdateCheckNextCheckDateTime = DateTime.MinValue; // Calculate Next Check DateTime
                        break;
                    case "AutomaticCleanupEnabled":
                        if (Settings.AutomaticCleanupEnabled)
                        {
                            AutomaticCleanupCheckNextCheckDateTime = DateTime.MinValue;// Calculate Next Check DateTime
                            AutomaticCleanupCheckStartTimer();
                        }
                        else
                        {
                            AutomaticCleanupCheckStopTimer();
                        }
                        break;
                    case "AutomaticCleanupInterval":
                    case "AutomaticCleanupSchedules":
                        AutomaticCleanupCheckNextCheckDateTime = DateTime.MinValue;// Calculate Next Check DateTime
                        break;
                }
            }
        }
    }
}