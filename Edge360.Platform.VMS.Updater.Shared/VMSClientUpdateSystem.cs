﻿using System;
using System.CodeDom;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Edge360.Platform.VMS.Updater.Shared.Extensions;
using Edge360.Platform.VMS.Updater.Shared.Settings;
using Kjs.AppLife.Update.Controller;
using Newtonsoft.Json;

namespace Edge360.Platform.VMS.Updater.Shared
{
    /// <summary>
    /// Edge360 VMS Client Update System Library
    /// </summary>
    public class VmsClientUpdateSystem
    {
        public static void Test()
        {
            

            var config = new UpdaterConfiguration();
            config.UpdateLocations.Add(new UpdateLocation
            {
                Location = @"http://vmsclientupdate.edge360.com"
            });
            config.AutomaticUpdateSchedules.Add(new UpdateSchedule
            {
                DaysOfWeek = DaysOfWeek.Friday| DaysOfWeek.Saturday| DaysOfWeek.Sunday,
                StartTime = new TimeSpan(20,0,0)
            });
            config.AutomaticUpdateSchedules.Add(new UpdateSchedule
            {
                DaysOfWeek = DaysOfWeek.Monday | DaysOfWeek.Saturday| DaysOfWeek.Sunday,
                EndTime = new TimeSpan(4,0,0)
            });
            config.AutomaticUpdateCheckInterval = new TimeSpan(0,30,0);
            config.AutomaticCleanupSchedules.Add(new UpdateSchedule
            {
                DaysOfWeek = DaysOfWeek.Any
            });
            config.AutomaticCleanupInterval = new TimeSpan(4,0,0);
            config.AutomaticCleanupKeepNewestCount = 3;

            config.PropertyChanged += (sender, args) =>
            {
                Trace.WriteLine($"{args.PropertyName} Changed");
            };

            var config2 = new UpdaterConfiguration();
            config2.TestUpdatesEnabled = true;
            config.MemberwiseCopy(config2);


            string outputPath = @"C:\Users\Bob\AppData\Local\Temp\Test.json";

            string serialOut = JsonConvert.SerializeObject(config, Formatting.Indented);
            File.WriteAllText(outputPath, serialOut, Encoding.ASCII);
            File.WriteAllText(outputPath + ".example.txt", Resources.updater_json_readme, Encoding.ASCII);
            
            string serialIn = File.ReadAllText(outputPath, Encoding.ASCII);
            var configIn = JsonConvert.DeserializeObject<UpdaterConfiguration>(serialIn);


        }

        /// <summary>
        /// Provides Access To The Applife Update Controller
        /// </summary>
        public UpdateController UpdateController { get; set; }

        /// <summary>
        /// Provides Access To Current Update (Exported)
        /// </summary>
        public UpdateInformation CurrentUpdate => UpdateController?.CurrentUpdate?.Export(); 
        
        /// <summary>
        /// Application Id For Updates
        /// </summary>
        public readonly Guid ApplicationId = new System.Guid("9c81fc8f-ac73-47a1-b466-9e4169b37e77");


        /// <summary>
        /// Public Key For Updates
        /// </summary>
        public readonly string PublicKeyToken =
            "<RSAKeyValue><Modulus>r5+10bZkxrynTTkS13BTh5EDQ85r0U1LR3EY09kv3Rwu8pGYC7PcUBwM+qS" +
            "Q3LVmQrK1GTQ0TNFwBXgcE6D8BILpx1qcwC3NHy5O76ipThDd4HB6NAaYKSQFmE5WjZfx9/3WXexA8Ax" +
            "QAIchbxXDxXOqbIRIAP8TgmQgS238Rjk=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        
     
        /// <summary>
        /// OPTION - UPDATE IN INTERACTIVE MODE (DEFAULT=FALSE)
        /// </summary>
        public bool Interactive { get; set; } = false;


        /// <summary>
        /// OPTION - ENABLE TEST UPDATES (DEFAULT=FALSE)
        /// </summary>
        public bool EnableTestUpdates { get; set; } = false;


        /// <summary>
        /// OPTION - ENABLE UPDATE CHAINING (DEFAULT=TRUE)
        /// </summary>
        public bool EnableAutoChaining { get; set; } = true;


        /// <summary>
        /// Location Where Updates Are Available For Download
        /// </summary>
        public ConcurrentBag<UpdateLocation> UpdateLocations { get; set; }


        /// <summary>
        /// Check For Update In Progress?
        /// </summary>
        public bool UpdateActive => UpdateEngineMonitor.UpdateEngineActive();


        /// <summary>
        /// DateTime When Update Was Started
        /// </summary>
        public DateTime? UpdateStarted { get; private set; }


        /// <summary>
        /// DateTime When Update Was Started
        /// </summary>
        public DateTime? UpdateCompleted { get; private set; }


        /// <summary>
        /// Application EXE Matching Pattern
        /// </summary>
        private string _exePattern;
        /// <summary>
        /// Application EXE Matching Pattern
        /// </summary>
        public string ExePattern
        {
            get => _exePattern;
            set
            {
                if (value != null && value.EndsWith(".exe", StringComparison.CurrentCultureIgnoreCase))
                {
                    _exePattern = value;
                }
            }
        }


        /// <summary>
        /// Application Install Base Path
        /// </summary>
        private string _basePath;
        /// <summary>
        /// Application Install Base Path
        /// </summary>
        public string BasePath
        {
            get => _basePath;
            set
            {
                if (value != null)
                {
                    _basePath = value;
                }
            }
        }

        /// <summary>
        /// VMS Client Update System Constructor
        /// </summary>
        /// <param name="updateLocations">Initial Update Locations Collection</param>
        /// <param name="exePattern">Application EXE Matching Pattern (DEFAULT=*VMS.Client.exe)</param>
        /// <param name="basePath">Application Install Base Path (DEFAULT=Current Working Directory)</param>
        /// <param name="useParent">Use Parent of Base Path (DEFAULT=FALSE)</param>
        public VmsClientUpdateSystem(ConcurrentBag<UpdateLocation> updateLocations, string exePattern = null, string basePath = null, bool useParent = false)
        {
            UpdateLocations = updateLocations ?? new ConcurrentBag<UpdateLocation>();

            if (string.IsNullOrWhiteSpace(exePattern))
            {
                exePattern = "*Edge360.Platform.VMS.Client.exe";
            }

            if (!exePattern.EndsWith(".exe", StringComparison.CurrentCultureIgnoreCase))
            {
                throw new ArgumentException("EXE Pattern Must End With .exe", exePattern);
            }

            _exePattern = exePattern;


            if (string.IsNullOrWhiteSpace(basePath))
            {
                basePath = AppDomain.CurrentDomain.BaseDirectory;
            }

            if (!Directory.Exists(basePath))
            {
                throw new ArgumentException("Base Path Directory Does Not Exist", basePath);
            }

            if (useParent)
            {
                basePath = Directory.GetParent(basePath).FullName;
            }

            _basePath = basePath;

            UpdateController = new UpdateController
            {
                ApplicationId = ApplicationId,
                PublicKeyToken = PublicKeyToken,
                EnableAutoChaining = true,
                UseHostAssemblyVersion = false,
                DisableAppLifeServerChecks = true
                
            };
            
            UpdateController.UpdateFound += UpdateControllerOnUpdateFound;
            UpdateController.UpdateStarting += UpdateControllerOnUpdateStarting;
        }


        /// <summary>
        /// VMS Client Update System Constructor
        /// </summary>
        /// <param name="updateLocation">Initial Update Location URL</param>
        /// <param name="exePattern">Application EXE Matching Pattern (DEFAULT=*VMS.Client.exe)</param>
        /// <param name="basePath">Application Install Base Path (DEFAULT=Current Working Directory)</param>
        /// <param name="useParent">Use Parent of Base Path (DEFAULT=FALSE)</param>
        public VmsClientUpdateSystem(string updateLocation, string exePattern = null,
            string basePath = null, bool useParent = false) : this(
            new ConcurrentBag<UpdateLocation> {new UpdateLocation {Location = updateLocation}}, exePattern, basePath, useParent)
        {

        }


        /// <summary>
        /// VMS Client Update System Constructor
        /// </summary>
        /// <param name="updateLocation">Initial Update Location</param>
        /// <param name="exePattern">Application EXE Matching Pattern (DEFAULT=*VMS.Client.exe)</param>
        /// <param name="basePath">Application Install Base Path (DEFAULT=Current Working Directory)</param>
        /// <param name="useParent">Use Parent of Base Path (DEFAULT=FALSE)</param>
        public VmsClientUpdateSystem(UpdateLocation updateLocation, string exePattern = null,
            string basePath = null, bool useParent = false) : this(
            new ConcurrentBag<UpdateLocation> {updateLocation}, exePattern, basePath, useParent)
        {

        }


        /// <summary>
        /// VMS Client Update System Constructor
        /// </summary>
        public VmsClientUpdateSystem() : this(new ConcurrentBag<UpdateLocation>())
        {

        }


        /// <summary>
        /// Triggered By Applife Update Controller When Update Is Starting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateControllerOnUpdateStarting(object sender, UpdateStartingEventArgs e)
        {
            UpdateStarted = DateTime.Now;
            UpdateCompleted = null;
        }


        // Allow Targeting Of A Specific Version
        private Version _targetUpdateVersion = null;
        /// <summary>
        /// Triggered By Applife Update Controller When Processing Update List (Used To Target Specific Updates)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void UpdateControllerOnUpdateFound(object sender, UpdateFoundEventArgs args)
        {
            if (_targetUpdateVersion == null || args == null || args.SelectedUpdate == null || args.AvailableUpdates == null ||
                args.AvailableUpdates.Count <= 0 || args.SelectedUpdate.Version.Equals(_targetUpdateVersion))
            {
                return;
            }

            foreach (var availableUpdate in args.AvailableUpdates)
            {
                if (availableUpdate.Version.Equals(_targetUpdateVersion))
                {
                    args.SelectedUpdate = availableUpdate;
                    return;
                }
            }

            args.SelectedUpdate = null;
            return;
        }
        

        /// <summary>
        /// Is There An Update Available From Any Source
        /// </summary>
        /// <returns>Update Available</returns>
        public bool HasUpdate(bool forceInstall = false)
        {
            return CheckUpdate(forceInstall:forceInstall) != null;
        }


        /// <summary>
        /// Check and Return Update Details
        /// </summary>
        /// <param name="targetLocation">Target Specific UpdateLocation</param>
        /// <param name="targetVersion">Target Specific Version</param>
        /// <returns></returns>
        public Tuple<UpdateLocation, UpdateInformation> CheckUpdate(UpdateLocation targetLocation = null, Version targetVersion = null, bool forceInstall = false)
        {
            try
            {
                _targetUpdateVersion = targetVersion;

                // If Force Install Use Not Installed Version
                Version currentVersion = NotInstalledVersion.Key;
                if (!forceInstall)
                {
                    // Determine Best Installed Version
                    currentVersion = LatestInstalledVersion().Key;
                    if (currentVersion == null)
                    {
                        // Default To Not Installed Version
                        currentVersion = NotInstalledVersion.Key;
                    }
                }

                UpdateController.Version = currentVersion;
                UpdateController.EnableTestUpdates = EnableTestUpdates;
                UpdateController.EnableAutoChaining = EnableAutoChaining;

                UpdateLocation bestUpdateLocation = null;
                Kjs.AppLife.Update.Controller.UpdateInformation bestUpdateVersion = null;
                foreach (var updateLocation in UpdateLocations)
                {
                    if (targetLocation == null || updateLocation.Location.Equals(updateLocation.Location,
                            StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (updateLocation != null && updateLocation.ConfigureUpdateController(UpdateController))
                        {
                            if (UpdateController.CheckForUpdate())
                            {
                                if (UpdateController.CurrentUpdate != null)
                                {
                                    if (bestUpdateVersion == null ||
                                        bestUpdateVersion.Version < UpdateController.CurrentUpdate.Version)
                                    {
                                        bestUpdateLocation = updateLocation;
                                        bestUpdateVersion = UpdateController.CurrentUpdate;
                                    }
                                }
                            }
                        }
                    }
                }

                if (bestUpdateVersion == null)
                {
                    return null;
                }

                return new Tuple<UpdateLocation, UpdateInformation>(bestUpdateLocation, bestUpdateVersion?.Export());
            }
            finally
            {
                _targetUpdateVersion = null;
            }
        }


        /// <summary>
        /// Get List Of Available Updates
        /// </summary>
        /// <param name="targetLocation">Target Specific UpdateLocation</param>
        /// <param name="targetVersion">Target Specific Version</param>
        /// <param name="allUpdateSteps">Show All Possible Update Steps (Instead of Best Update)</param>
        /// <param name="allUpdatesAvailable">Show All Updates Available From Update Location</param>
        /// <returns>List Of Available Updates</returns>
        public Dictionary<UpdateLocation, UpdateInformation[]> ListAvailableUpdates(UpdateLocation targetLocation = null, Version targetVersion = null, bool allUpdateSteps = false, bool allUpdatesAvailable = false)
        {
            try
            {
                _targetUpdateVersion = targetVersion;

                var result = new Dictionary<UpdateLocation, UpdateInformation[]>();

                Version currentVersion = LatestInstalledVersion().Key;
                if (currentVersion == null)
                {
                    currentVersion = NotInstalledVersion.Key;
                }

                if (allUpdatesAvailable)
                {
                    currentVersion = NotInstalledVersion.Key;
                    allUpdateSteps = true;
                }
                UpdateController.Version = currentVersion;
                UpdateController.EnableTestUpdates = EnableTestUpdates;

                foreach (var updateLocation in UpdateLocations)
                {
                    if (targetLocation == null || updateLocation.Location.Equals(updateLocation.Location,
                            StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (updateLocation != null && updateLocation.ConfigureUpdateController(UpdateController))
                        {
                            if (UpdateController.CheckForUpdate())
                            {
                                if (allUpdateSteps)
                                {
                                    result.Add(updateLocation, UpdateController.AllUpdates?.Select(t => t?.Export()).ToArray());
                                } 
                                else if (EnableAutoChaining)
                                {
                                    result.Add(updateLocation, UpdateController.GetUpdateChain()?.Select(t => t?.Export()).ToArray());
                                }
                                else
                                {
                                    if (UpdateController.AvailableUpdates != null &&
                                        UpdateController.AvailableUpdates.Count > 0)
                                    {
                                        result.Add(updateLocation, UpdateController.AvailableUpdates?.Select(t => t?.Export()).ToArray());
                                    }
                                }
                            }
                        }
                    }
                }

                return result;
            }
            finally
            {
                _targetUpdateVersion = null;
            }
        }

        /// <summary>
        /// Apply An Update To Installation
        /// </summary>
        /// <param name="targetLocation">(OPTIONAL) Target Specific UpdateLocation (DEFAULT=*ANY*)</param>
        /// <param name="targetVersion">(OPTIONAL) Target Specific Version (DEFAULT=*ANY*)</param>
        /// <param name="waitFinish">(OPTIONAL) Wait For Install To Finish Before Returning (DEFAULT=TRUE)</param>
        /// <param name="forceInstall">(OPTIONAL) Force Update Installation (DEFAULT=FALSE)</param>
        /// <returns>True if Update Applied</returns>
        public bool ApplyUpdate(UpdateLocation targetLocation = null, Version targetVersion = null, bool waitFinish = true, bool forceInstall = false)
        {
            try
            {
                if (UpdateEngineMonitor.UpdateEngineActive())
                {
                    Trace.TraceError("Update Already In Progress - Cannot Apply Update At This Time");
                    return false;
                }

                if (!PrepareUpdateController(targetLocation, targetVersion, forceInstall))
                {
                    Trace.TraceError("Error Preparing Update Controller");
                    return false;
                }

                // Check For Update And Verify One Ready
                if (!UpdateController.CheckForUpdate() || UpdateController.CurrentUpdate == null)
                {
                    Trace.TraceInformation("No Updates Found");
                    return false;
                }

                // Perform Update Process
                return PerformUpdate(waitFinish);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Error Applying Update {ex}");
                return false;
            }
            finally
            {
                // Clear Target Version
                _targetUpdateVersion = null;
            }
        }

        /// <summary>
        /// Prepare The Applife Update Controller With Selected Update (Or Best Update)
        /// </summary>
        /// <param name="targetLocation"></param>
        /// <param name="targetVersion"></param>
        /// <param name="forceInstall"></param>
        /// <returns></returns>
        private bool PrepareUpdateController(UpdateLocation targetLocation, Version targetVersion, bool forceInstall)
        {
            // Set Target Version
            _targetUpdateVersion = targetVersion;

            // If Force Install Use Not Installed Version
            Version currentVersion = NotInstalledVersion.Key;
            if (!forceInstall)
            {
                // Determine Best Installed Version
                currentVersion = LatestInstalledVersion().Key;
                if (currentVersion == null)
                {
                    // Default To Not Installed Version
                    currentVersion = NotInstalledVersion.Key;
                }
            }
            
            // Prepare Update Controller
            UpdateController.Version = currentVersion;
            UpdateController.EnableTestUpdates = EnableTestUpdates;
            UpdateController.EnableAutoChaining = EnableAutoChaining;

            // Find Best Update Location If Not Specified
            if (targetLocation == null)
            {
                var bestUpdate = CheckUpdate();
                if (bestUpdate == null)
                {
                    return false;
                }

                targetLocation = bestUpdate.Item1;
            }

            // Set Target Update Version (Possibly Cleared By Check Update Above)
            _targetUpdateVersion = targetVersion;


            // Configure The Update Controller With Target Values
            return targetLocation.ConfigureUpdateController(UpdateController);
        }


        /// <summary>
        /// Check If Update Chain Includes An Update Marked As Containing An UpdateService Update
        /// </summary>
        /// <returns>True If UpdateService Update Found</returns>
        private bool CheckForUpdateServiceUpdate()
        {
            Kjs.AppLife.Update.Controller.UpdateInformation update = UpdateController?.CurrentUpdate;
            if (update == null)
            {
                Trace.TraceInformation("No Updates Found");
                return false;
            }

            // Check All Updates In The Update Chain
            foreach (var updateInformation in UpdateController.GetUpdateChain())
            {
                if (updateInformation?.CustomData != null &&
                    updateInformation.CustomData.Contains("@UPDATESERVICE@"))
                {
                    // This update is marked as containing an update to the UpdateService (we need to be shutdown and restarted)
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        /// Calculate AppLife Update Options Value
        /// </summary>
        /// <param name="updateServiceUpdateIncluded"></param>
        /// <returns></returns>
        private ApplyUpdateOptions CalculateUpdateOptions()
        {
            ApplyUpdateOptions options = ApplyUpdateOptions.NoUpdateWindow;
            if (Interactive)
            {
                options = ApplyUpdateOptions.None;
            }

            if (!CheckForUpdateServiceUpdate())
            {
                // Do Not Shutdown Unless UpdateService Update Found
                options |= ApplyUpdateOptions.NoShutdown | ApplyUpdateOptions.NoRestart;
            }

            return options;
        }

        
        /// <summary>
        /// Perform The Requested Update
        /// </summary>
        /// <param name="waitFinish">Wait For Update To Complete</param>
        /// <returns></returns>
        private bool PerformUpdate(bool waitFinish)
        {
            if (Interactive)
            {
                return PerformInteractiveUpdate();
            }

            return PerformNonInteractiveUpdate(waitFinish);
        }


        /// <summary>
        /// Perform The Requested Update In Interactive Mode
        /// </summary>
        /// <returns></returns>
        private bool PerformInteractiveUpdate()
        {
            UpdateController.UpdateInteractive(options: CalculateUpdateOptions());
            return true;
        }


        /// <summary>
        /// Perform The Request Update In Non-Interactive Mode
        /// </summary>
        /// <param name="waitFinish">Wait For Update To Complete</param>
        /// <returns></returns>
        private bool PerformNonInteractiveUpdate(bool waitFinish)
        {
            // Download Update To Temp Folder
            UpdateController.DownloadUpdate();

            // Start Update Engine Monitor If Waiting
            UpdateEngineMonitor monitor = null;
            if (waitFinish)
            {
                monitor = new UpdateEngineMonitor();
                monitor.StartUpdateWatch();
            }

            // Verify Update Not In Progress
            if (UpdateEngineMonitor.UpdateEngineActive())
            {
                Trace.TraceError("Update Already In Progress - Cannot Apply Update At This Time");
                return false;
            }

            if (!UpdateController.ApplyUpdate(CalculateUpdateOptions()))
            {
                Trace.TraceError($"Error Applying Update - {UpdateController?.ReadLastUpdateInformation()?.ErrorText}");
                // Error Applying Update
                return false;
            }

            // If Waiting To Finish
            if (waitFinish)
            {
                // Wait For Monitor To Signal Complete
                return monitor.WaitForUpdateComplete();
            }

            // Return Success
            return true;
        }


        /// <summary>
        /// Version Used When Not Installed (0.0.0)
        /// </summary>
        public readonly KeyValuePair<Version, string> NotInstalledVersion = new KeyValuePair<Version, string>(new Version(0,0,0), null);


        /// <summary>
        /// Get Latest Install Version
        /// </summary>
        /// <returns></returns>
        public KeyValuePair<Version,string> LatestInstalledVersion()
        {
            return InstalledVersions()?.LastOrDefault() ?? NotInstalledVersion;
        }


        /// <summary>
        /// Get List Of All Installed Versions
        /// </summary>
        /// <returns></returns>
        public SortedList<Version, string> InstalledVersions()
        {
            var result = new SortedList<Version, string>();

            var baseDirectory = new DirectoryInfo(_basePath);
            var clients = baseDirectory.GetFiles(_exePattern, SearchOption.AllDirectories);
            foreach (var c in clients)
            {
                try
                {
                    var version = new Version(FileVersionInfo.GetVersionInfo(c.FullName).FileVersion);
                    result.Add(version, c.FullName);
                }
                catch (Exception e)
                {
                    Trace.TraceError($"Error Reading Client Version {e}");
                }
            }

            return result;
        }
    }
}
