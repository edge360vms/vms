﻿using log4net;
using System;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Edge360.Platform.VMS.Updater.Shared.Extensions;
using Edge360.Platform.VMS.Updater.Shared.Interfaces;
using Newtonsoft.Json;

namespace Edge360.Platform.VMS.Updater.Shared.Settings
{
    public abstract class BaseSettings<T> where T : class, ISettings, new()
    {
        private readonly string _directoryName;
        private readonly string _fileName;
        private readonly string _rootDirectory;
        protected readonly bool _monitorChanges;
        protected CancellationTokenSource MonitorFileCancellationTokenSource = null;
        protected Task MonitorFileTask = null;
        protected TimeSpan MonitorFileInterval = new TimeSpan(0,1,0);

        private bool _initialized;

        protected BaseSettings(string rootDirectory, string directoryName, string fileName, bool monitorChanges=true)
        {
            if (string.IsNullOrEmpty(rootDirectory)) throw new ArgumentNullException(nameof(rootDirectory));
            if (string.IsNullOrEmpty(directoryName)) throw new ArgumentNullException(nameof(directoryName));
            if (string.IsNullOrEmpty(fileName)) throw new ArgumentNullException(nameof(fileName));

            _rootDirectory = rootDirectory;
            _directoryName = directoryName;
            _fileName = fileName;
            _monitorChanges = monitorChanges;
        }

        private ILog Logger { get; } = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public string RootDirectory => _rootDirectory;

        public T Settings { get; set; } = new T();

        public virtual async Task InitializeAsync()
        {
            if (_initialized) return;

            _initialized = true;

            var file = GetFilePath();
            if (File.Exists(file))
            {
                try
                {
                    var result = File.ReadAllText(file);
                    Settings = Deserialize(result);
                    if (_monitorChanges)
                    {
                        // Cache Current Result
                        SettingFileContentsCache = result;

                        // Start Monitoring File For Changes
                        StartFileMonitor();
                    }
                }
                catch (Exception e)
                {
                    Logger.Error($"Failed to read settings from '{file}'.", e);
                }

                if (Settings == null) Settings = Activator.CreateInstance<T>();
            }
            else
            {
                Save();
            }
        }

        protected string SettingFileContentsCache = null;
        protected void StartFileMonitor()
        {
            // Stop Any Existing File Monitor
            StopFileMonitor();

            // Start New Task
            MonitorFileCancellationTokenSource = new CancellationTokenSource();
            MonitorFileTask = Task.Run(() => { MonitorFileDoWork(MonitorFileCancellationTokenSource.Token); }, MonitorFileCancellationTokenSource.Token);
        }

        protected void StopFileMonitor()
        {
            if (MonitorFileTask != null && MonitorFileTask.Status != TaskStatus.Running)
            {
                // Clean Out Old Task
                MonitorFileCancellationTokenSource?.Cancel();
                MonitorFileCancellationTokenSource = null;
                MonitorFileTask = null;
            }
        }
        

        protected void MonitorFileDoWork(CancellationToken token)
        {
            while (true)
            {
                try
                {
                    // Wait Until Next Interval
                    Task.Delay(MonitorFileInterval, token).Wait(token);

                    // Check for Cancellation
                    if (token.IsCancellationRequested)
                    {
                        // Exit
                        return;
                    }

                    var currentFileContents = File.ReadAllText(GetFilePath());
                    if (!string.IsNullOrWhiteSpace(currentFileContents) &&
                        !currentFileContents.Equals(SettingFileContentsCache))
                    {
                        // Potential Changes Detected
                        var newSettings = Deserialize(currentFileContents);
                        if (newSettings != null)
                        {
                            // Check for Support of Memberwise Copy
                            if (Settings is INotifyPropertyChanged settings &&
                                newSettings is INotifyPropertyChanged source)
                            {
                                // Copy New Settings into existing object and trigger OnPropertyChanged events.
                                settings.MemberwiseCopy(source);
                            }
                            else
                            {
                                // Otherwise replace Settings with new copy
                                Settings = newSettings;
                            }

                            // Cache Current Value
                            SettingFileContentsCache = currentFileContents;
                        }
                    }
                }
                catch (OperationCanceledException e)
                {
                    Logger.Debug("Monitor File Task Cancelled", e);
                    return;
                }
                catch (Exception e)
                {
                    Logger.Error("Error During Monitor File", e);
                    // Ignore
                }
            }
        }

        protected string GetFilePath()
        {
            if (!Directory.Exists(_rootDirectory)) Directory.CreateDirectory(_rootDirectory);

            var settingsDirectory = $"{_rootDirectory}/{_directoryName}";
            if (!Directory.Exists(settingsDirectory)) Directory.CreateDirectory(settingsDirectory);

            var file = settingsDirectory + "/" + _fileName;
            return file;
        }

        public virtual void Save()
        {
            var file = GetFilePath();

            try
            {
                var serialized = Serialize(Settings);
                File.WriteAllText(file, serialized);
            }
            catch (Exception e)
            {
                Logger.Error($"Failed to save settings to '{file}'.", e);
            }
        }

        protected abstract string Serialize(T t);
        protected abstract T Deserialize(string serialized);
    }
}