﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using log4net.Repository.Hierarchy;
using Newtonsoft.Json;

namespace Edge360.Platform.VMS.Updater.Shared.Settings
{
    public class UpdaterSettings : BaseSettings<UpdaterConfiguration>
    {
        public static UpdaterSettings Instance = new UpdaterSettings($"{Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)}\\Edge360\\VMS");

        private const string DirectoryName = "settings";
        private const string FileName = "updater.json";

        private readonly bool _watchFile = true;

        public UpdaterSettings(string rootDirectory, bool watchFile=true)
            : base(rootDirectory, DirectoryName, FileName)
        {
            _watchFile = watchFile;
        }

        public override void Save() 
        {
            // Call Save on Base
            base.Save();

            // Output Readme
            SaveReadMe();
        }

        public override async Task InitializeAsync()
        {
            await base.InitializeAsync();
        }

        protected void SaveReadMe()
        {
            try
            {
                string readmePath = GetFilePath() + ".readme.txt";
                if (!File.Exists(readmePath))
                {
                    File.WriteAllText(readmePath, Resources.updater_json_readme);
                }
            }
            catch (Exception e)
            {
                Trace.TraceError($"Error Writing Readme {e}");
                // Ignore
            }
        }

        protected override string Serialize(UpdaterConfiguration t)
        {
            return JsonConvert.SerializeObject(t, Formatting.Indented);
        }

        protected override UpdaterConfiguration Deserialize(string serialized)
        {
            return JsonConvert.DeserializeObject<UpdaterConfiguration>(serialized);
        }
    }
}
