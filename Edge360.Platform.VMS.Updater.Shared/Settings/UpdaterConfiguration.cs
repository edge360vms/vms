﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Edge360.Platform.VMS.Updater.Shared.Interfaces;
using Newtonsoft.Json;

namespace Edge360.Platform.VMS.Updater.Shared.Settings
{
    [Serializable]
    public class UpdaterConfiguration : ISettings, INotifyPropertyChanged
    {
        private List<UpdateLocation> _updateLocations = new List<UpdateLocation>(); // Default None
        public List<UpdateLocation> UpdateLocations
        {
            get => _updateLocations;
            set
            {
                if (value == null)
                {
                    value = new List<UpdateLocation>(); // Empty List
                }

                if (value == _updateLocations)
                {
                    return;
                }
                _updateLocations = value;
                OnPropertyChanged();
            }
        }


        private int _updateLocationHistoryCount = 5; // Default 5 - Min 0 (Disabled)
        public int UpdateLocationHistoryCount
        {
            get => _updateLocationHistoryCount;
            set
            {
                if (value < 0)
                {
                    value = 0; // No History
                }

                if (value == _updateLocationHistoryCount)
                {
                    return;
                }
                _updateLocationHistoryCount = value;
                OnPropertyChanged();
            }
        }


        private bool _automaticUpdatesEnabled = true; // Default Enabled
        public bool AutomaticUpdatesEnabled
        {
            get => _automaticUpdatesEnabled;
            set
            {
                if (value == _automaticUpdatesEnabled)
                {
                    return;
                }
                _automaticUpdatesEnabled = value;
                OnPropertyChanged();
            }
        }

        private static readonly TimeSpan MinUpdateInterval = TimeSpan.FromMinutes(1); // 1 Minute
        private TimeSpan _automaticUpdateCheckInterval = TimeSpan.FromHours(1); // Default 1 Hour
        public TimeSpan AutomaticUpdateCheckInterval
        {
            get => _automaticUpdateCheckInterval;
            set
            {
                if (value < MinUpdateInterval)
                {
                    value = MinUpdateInterval;
                }

                if (value == _automaticUpdateCheckInterval)
                {
                    return;
                }
                _automaticUpdateCheckInterval = value;
                OnPropertyChanged();
            }
        }

        private List<UpdateSchedule> _automaticUpdateSchedules = new List<UpdateSchedule>(); // Default Empty = Anytime
        public List<UpdateSchedule> AutomaticUpdateSchedules
        {
            get => _automaticUpdateSchedules;
            set
            {
                if (value == null)
                {
                    value = new List<UpdateSchedule>(); // Empty List
                }

                if (value == _automaticUpdateSchedules)
                {
                    return;
                }
                _automaticUpdateSchedules = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Check if DateTime is valid within configured schedules
        /// </summary>
        /// <param name="checkDate">DateTime to check (Defaults to DateTime.Now)</param>
        /// <returns>True if Valid DateTime else False</returns>
        public bool AutomaticUpdateScheduleCheck(DateTime? checkDate = null)
        {
            return _automaticUpdatesEnabled && ScheduleCheck(_automaticUpdateSchedules, checkDate.GetValueOrDefault(DateTime.Now));
        }
        /// <summary>
        /// Returns next valid DateTime within configured schedules
        /// </summary>
        /// <param name="checkDate">DateTime to check (Defaults to DateTime.Now)</param>
        /// <returns>Next Valid DateTime, DateTime.Max if none found</returns>
        public DateTime AutomaticUpdateScheduleFindNext(DateTime? checkDate = null)
        {
            return ScheduleFindNextAllowed(_automaticUpdateSchedules, checkDate.GetValueOrDefault(DateTime.Now));
        }


        private bool _automaticCleanupEnabled = true; // Default Enabled
        public bool AutomaticCleanupEnabled
        {
            get => _automaticCleanupEnabled;
            set
            {
                if (value == _automaticCleanupEnabled)
                {
                    return;
                }
                _automaticCleanupEnabled = value;
                OnPropertyChanged();
            }
        }

        private int _automaticCleanupKeepNewestCount = 3; // Default 3 Installs
        public int AutomaticCleanupKeepNewestCount
        {
            get => _automaticCleanupKeepNewestCount;
            set
            {
                if (value < 1)
                {
                    value = 1; // Minimum
                }
            
                if (value == _automaticCleanupKeepNewestCount)
                {
                    return;
                }

                _automaticCleanupKeepNewestCount = value;
                OnPropertyChanged();
            }
        }

        private TimeSpan _automaticCleanupInterval = TimeSpan.FromHours(1); // Default 1 Hour
        public TimeSpan AutomaticCleanupInterval
        {
            get => _automaticCleanupInterval;
            set
            {
                if (value < TimeSpan.Zero)
                {
                    value = TimeSpan.Zero;
                }

                if (value == _automaticCleanupInterval)
                {
                    return;
                }
                _automaticCleanupInterval = value;
                OnPropertyChanged();
            }
        }

        private List<UpdateSchedule> _automaticCleanupSchedules = new List<UpdateSchedule>(); // Default Empty = Anytime
        public List<UpdateSchedule> AutomaticCleanupSchedules
        {
            get => _automaticCleanupSchedules;
            set
            {
                if (value == null)
                {
                    value = new List<UpdateSchedule>(); // Empty List
                }

                if (value == _automaticCleanupSchedules)
                {
                    return;
                }
                _automaticCleanupSchedules = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Check if DateTime is valid within configured schedules
        /// </summary>
        /// <param name="checkDate">DateTime to check (Defaults to DateTime.Now)</param>
        /// <returns>True if Valid DateTime else False</returns>
        public bool AutomaticCleanupScheduleCheck(DateTime? checkDate = null)
        {
            return ScheduleCheck(_automaticCleanupSchedules, checkDate.GetValueOrDefault(DateTime.Now));
        }

        /// <summary>
        /// Returns next valid DateTime within configured schedules
        /// </summary>
        /// <param name="checkDate">DateTime to check (Defaults to DateTime.Now)</param>
        /// <returns>Next Valid DateTime, DateTime.Max if none found</returns>
        public DateTime AutomaticCleanupScheduleFindNext(DateTime? checkDate = null)
        {
            return ScheduleFindNextAllowed(_automaticCleanupSchedules, checkDate.GetValueOrDefault(DateTime.Now));
        }


        private bool _testUpdatesEnabled = false; // Default False
        public bool TestUpdatesEnabled
        {
            get => _testUpdatesEnabled;
            set
            {
                if (value == _testUpdatesEnabled)
                {
                    return;
                }
                _testUpdatesEnabled = value;
                OnPropertyChanged();
            }
        }

        private bool _updateChainingEnabled = true; // Default True
        public bool UpdateChainingEnabled
        {
            get => _updateChainingEnabled;
            set
            {
                if (value == _updateChainingEnabled)
                {
                    return;
                }
                _updateChainingEnabled = value;
                OnPropertyChanged();
            }
        }

        private string _applicationExePattern = null; // Default (null)
        public string ApplicationExePattern
        {
            get => _applicationExePattern;
            set
            {
                if (value == _applicationExePattern)
                {
                    return;
                }
                _applicationExePattern = value;
                OnPropertyChanged();
            }
        }

        private string _applicationBasePath = null; // Default (null)
        public string ApplicationBasePath
        {
            get => _applicationBasePath;
            set
            {
                if (value == _applicationBasePath)
                {
                    return;
                }
                _applicationBasePath = value;
                OnPropertyChanged();
            }
        }


        private bool ScheduleCheck(List<UpdateSchedule> schedules, DateTime checkDateTime)
        {
            if (schedules == null || schedules.Count <= 0)
            {
                return true;
            }

            foreach (var schedule in schedules)
            {
                if (schedule.ValidDateTime(checkDateTime))
                {
                    return true;
                }
            }

            return false;
        }

        private DateTime ScheduleFindNextAllowed(List<UpdateSchedule> schedules, DateTime startDateTime)
        {
            if (schedules == null || schedules.Count <= 0)
            {
                // No Schedule - Allow Anytime
                return startDateTime;
            }

            DateTime nextAllowedDateTime = DateTime.MaxValue;

            // Check each schedule and find earliest next allowed datetime
            foreach (var schedule in schedules)
            {
                var nextAllowedInSchedule = schedule.NextValidDateTime(startDateTime);
                if (nextAllowedInSchedule < nextAllowedDateTime)
                {
                    nextAllowedDateTime = nextAllowedInSchedule;
                }
            }

            return nextAllowedDateTime;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
