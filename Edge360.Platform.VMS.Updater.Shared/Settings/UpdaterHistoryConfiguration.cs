﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Edge360.Platform.VMS.Updater.Shared.Interfaces;

namespace Edge360.Platform.VMS.Updater.Shared.Settings
{
    public class UpdaterHistoryConfiguration: ISettings, INotifyPropertyChanged
    {
        private List<UpdateLocation> _updateLocations = new List<UpdateLocation>(); // Default None
        public List<UpdateLocation> UpdateLocations
        {
            get => _updateLocations;
            set
            {
                if (value == null)
                {
                    value = new List<UpdateLocation>(); // Empty List
                }

                if (value == _updateLocations)
                {
                    return;
                }
                _updateLocations = value;
                OnPropertyChanged();
            }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
