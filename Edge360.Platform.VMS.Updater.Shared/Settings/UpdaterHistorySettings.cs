﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using log4net.Repository.Hierarchy;
using Newtonsoft.Json;


namespace Edge360.Platform.VMS.Updater.Shared.Settings
{
    public class UpdaterHistorySettings : BaseSettings<UpdaterHistoryConfiguration>
    {
        public static UpdaterHistorySettings Instance = new UpdaterHistorySettings($"{Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)}\\Edge360\\VMS");

        private const string DirectoryName = "settings";
        private const string FileName = "updater.history.json";

        private readonly bool _watchFile = true;

        public UpdaterHistorySettings(string rootDirectory, bool watchFile=true)
            : base(rootDirectory, DirectoryName, FileName)
        {
            _watchFile = watchFile;
        }

        public override void Save() 
        {
            // Call Save on Base
            base.Save();
        }

        public override async Task InitializeAsync()
        {
            await base.InitializeAsync();
        }
        
        protected override string Serialize(UpdaterHistoryConfiguration t)
        {
            return JsonConvert.SerializeObject(t, Formatting.Indented);
        }

        protected override UpdaterHistoryConfiguration Deserialize(string serialized)
        {
            return JsonConvert.DeserializeObject<UpdaterHistoryConfiguration>(serialized);
        }
    }
}
