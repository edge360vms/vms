﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using log4net;

namespace Edge360.Platform.VMS.Updater.Shared.Settings
{
    class Class1
    {
        private static readonly ILog Log = LogManager.GetLogger
            (MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly UpdaterConfiguration Settings = UpdaterSettings.Instance.Settings;
        private static readonly UpdaterHistoryConfiguration History = UpdaterHistorySettings.Instance.Settings;

        private static readonly object VmsClientUpdateSystemLock = new object();
        private static VmsClientUpdateSystem _vmsClientUpdateSystem = null;


        private static readonly object AutomaticCleanupCheckLock = new object();
        private static readonly TimeSpan AutomaticCleanupCheckInternalInterval = TimeSpan.FromSeconds(30);
        private static Timer _automaticCleanupCheckTimer;

        private static DateTime _automaticCleanupCheckNextCheckDateTime = DateTime.Now;
        private static DateTime AutomaticCleanupCheckNextCheckDateTime
        {
            get
            {
                lock (AutomaticCleanupCheckLock)
                {
                    return _automaticCleanupCheckNextCheckDateTime;
                }
            }
            set
            {
                lock (AutomaticCleanupCheckLock)
                {
                    try
                    {
                        // Override with calculated next check time if MinValue
                        if (DateTime.MinValue.Equals(value))
                        {
                            _automaticCleanupCheckNextCheckDateTime =
                                Settings.AutomaticCleanupScheduleFindNext(
                                    DateTime.Now.Add(Settings.AutomaticCleanupInterval));

                            if (DateTime.MinValue.Equals(_automaticCleanupCheckNextCheckDateTime))
                            {
                                // No Valid Next Time Found In Schedule?? (Should Not Happen)
                                _automaticCleanupCheckNextCheckDateTime =
                                    DateTime.Now.Add(TimeSpan.FromMinutes(10)); // Default to 10 Minutes
                            }
                        }
                        else
                        {
                            _automaticCleanupCheckNextCheckDateTime = value;
                        }
                    }
                    catch (Exception e)
                    {
                        Trace.TraceError($"Error Calculating Next Auto Cleanup Check {e.Message}");
                        _automaticCleanupCheckNextCheckDateTime =
                            DateTime.Now.Add(TimeSpan.FromMinutes(10)); // Default to 10 Minutes
                    }
                }
            }
        }


        private static void AutomaticCleanupCheckStartTimer()
        {
            lock (AutomaticCleanupCheckLock)
            {
                if (_automaticCleanupCheckTimer == null)
                {
                    _automaticCleanupCheckTimer = new Timer
                    {
                        AutoReset = false,
                        Enabled = false,
                        Interval = AutomaticCleanupCheckInternalInterval.TotalMilliseconds
                    };
                    _automaticCleanupCheckTimer.Elapsed += async (sender, e) =>
                    {
                        await AutomaticCleanupCheckTimerElapsedAsync();
                    };
                }

                AutomaticCleanupCheckNextCheckDateTime = DateTime.MinValue; // Calculate Next Check DateTime
                _automaticCleanupCheckTimer.Start();
            }
        }

        private static void AutomaticCleanupCheckStopTimer()
        {
            lock (AutomaticCleanupCheckLock)
            {
                _automaticCleanupCheckTimer?.Stop();
            }
        }


        private static async Task AutomaticCleanupCheckTimerElapsedAsync()
        {
            try
            {
                // Get Next Check Time and Return Lock
                var nextCheckTime = AutomaticCleanupCheckNextCheckDateTime;

                // Check if Valid Time
                if (nextCheckTime <= DateTime.Now)
                {
                    // Perform Check
                    await PerformAutomaticCleanupCheckAsync();
                }
            }
            catch (Exception e)
            {
                // Error Performing Update Check
                Trace.TraceError($"Error Calculating Next Auto Cleanup Check {e.Message}");
            }
            finally
            {
                if (Settings.AutomaticCleanupEnabled)
                {
                    // Reset Timer For Next Check
                    AutomaticCleanupCheckStartTimer();
                }
            }
        }

        public static async Task PerformAutomaticCleanupCheckAsync()
        {
            // Make Sure We Are Within The Authorized Schedule
            if (Settings.AutomaticCleanupScheduleCheck())
            {
                await PerformCleanupCheckAsync();
            }
        }

        public static async Task PerformCleanupCheckAsync()
        {
            await Task.Run(() =>
            {
                // Perform Cleanup
            });
        }

    }
}
