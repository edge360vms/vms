﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Edge360.Platform.VMS.Updater.Shared
{

    /// <summary>
    /// Monitor's The AppLife Update Engine Progress (Start / Finish)
    /// </summary>
    public sealed class UpdateEngineMonitor
    {
        public static string ProcessName = "Kjs.AppLife.Update.Engine.UI";
        private readonly TimeSpan _maxWaitStarting = new TimeSpan(0,1,0);
        private readonly TimeSpan _maxWaitComplete = new TimeSpan(0,30,0);

        /// <summary>
        /// Creates A New Instance with Default Max Wait Times
        /// </summary>
        /// <param name="processName">(Optional) AppLife Update Engine Process Name</param>
        public UpdateEngineMonitor(string processName = null)
        {
            if (processName != null)
            {
                ProcessName = processName;
            }
        }

        /// <summary>
        /// Creates A New Instance with Override Max Wait Times
        /// </summary>
        /// <param name="maxWaitStarting">Max Time To Wait For Engine To Start</param>
        /// <param name="maxWaitComplete">Max Time To Wait For Engine To Finish</param>
        /// <param name="processName">(Optional) AppLife Update Engine Process Name</param>
        public UpdateEngineMonitor(TimeSpan maxWaitStarting, TimeSpan maxWaitComplete, string processName = null)
        {
            if (processName != null)
            {
                ProcessName = processName;
            }

            _maxWaitStarting = maxWaitStarting;
            _maxWaitComplete = maxWaitComplete;
        }


        private readonly object _lState = new object();
        private UpdateEngineMonitorState _monitorState = UpdateEngineMonitorState.Unknown;
        private readonly ManualResetEvent _updateEngineWatchStarted = new ManualResetEvent(false);
        private readonly ManualResetEvent _updateEngineStarted = new ManualResetEvent(false);

        /// <summary>
        /// Current State Of AppLife Update Engine Monitor
        /// </summary>
        public UpdateEngineMonitorState CurrentMonitorState
        {
            get
            {
                lock (_lState)
                {
                    return _monitorState;
                }
            }

            private set
            {
                lock (_lState)
                {
                    _monitorState = value;
                    switch (value)
                    {
                        case UpdateEngineMonitorState.WaitingToStart:
                            _updateEngineWatchStarted.Set();
                            _updateEngineStarted.Reset();
                            break;
                        case UpdateEngineMonitorState.Running:
                            _updateEngineWatchStarted.Set();
                            _updateEngineStarted.Set();
                            break;
                        case UpdateEngineMonitorState.Completed:
                        case UpdateEngineMonitorState.Unknown:
                        default:
                            _updateEngineWatchStarted.Reset();
                            _updateEngineStarted.Reset();
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Start Watching For Update Engine Process Start
        /// </summary>
        public void StartUpdateWatch()
        {
            Task.Factory.StartNew(WaitForUpdateStart);

            // Wait For Monitor Task To Signal Running
            _updateEngineWatchStarted.WaitOne(_maxWaitStarting);
        }

        /// <summary>
        /// Wait (ASYNC) For Update Engine Process Start
        /// </summary>
        public async Task<bool> WaitForUpdateStartAsync()
        {
            return await Task.Factory.StartNew(WaitForUpdateStart);
        }

        /// <summary>
        /// Wait For Update Engine Process Start
        /// </summary>
        public bool WaitForUpdateStart()
        {
            if (CurrentMonitorState != UpdateEngineMonitorState.Unknown && CurrentMonitorState != UpdateEngineMonitorState.Completed)
            {
                throw new InvalidOperationException("Incorrect State");
            }

            Stopwatch swWait = new Stopwatch();
            swWait.Start();
            CurrentMonitorState = UpdateEngineMonitorState.WaitingToStart;
            while (!UpdateEngineActive())
            {
                // Wait For Engine To Start
                if (swWait.Elapsed > _maxWaitStarting)
                {
                    Trace.TraceError("Error Waiting For Update Engine To Start");
                    CurrentMonitorState = UpdateEngineMonitorState.Unknown;
                    return false;
                }

                Thread.Sleep(250);
            }

            CurrentMonitorState = UpdateEngineMonitorState.Running;
            swWait.Stop();
            return true;
        }

        /// <summary>
        /// Wait (ASYNC) For Update Engine Process Complete
        /// </summary>
        public async Task<bool> WaitForUpdateCompleteAsync()
        {
            return await Task.Factory.StartNew(WaitForUpdateStart);
        }

        /// <summary>
        /// Wait For Update Engine Process Complete
        /// </summary>
        public bool WaitForUpdateComplete()
        {
            // If Not Already Monitoring Update Start - Begin That Now
            if (CurrentMonitorState != UpdateEngineMonitorState.WaitingToStart)
            {
                if (!WaitForUpdateStart())
                {
                    Trace.TraceError("Error Waiting For Update Engine To Start");
                    CurrentMonitorState = UpdateEngineMonitorState.Unknown;
                    return false;
                }
            }

            // Wait For Update Engine To Be Running
            if (!_updateEngineStarted.WaitOne(_maxWaitStarting))
            {
                Trace.TraceError("Error Waiting For Update Engine To Start");
                CurrentMonitorState = UpdateEngineMonitorState.Unknown;
                return false;
            }

            // State Verification 
            if (CurrentMonitorState != UpdateEngineMonitorState.Running)
            {
                throw new InvalidOperationException("Incorrect State");
            }

            Stopwatch swWait = new Stopwatch();
            swWait.Start();
            while (UpdateEngineActive())
            {
                // Wait For Engine To Stop
                if (swWait.Elapsed > _maxWaitComplete)
                {
                    Trace.TraceError("Error Waiting For Update Engine To Stop");
                    CurrentMonitorState = UpdateEngineMonitorState.Unknown;
                    return false;
                }

                Thread.Sleep(1000);
            }

            CurrentMonitorState = UpdateEngineMonitorState.Completed;
            swWait.Stop();
            return true;
        }

        /// <summary>
        /// Check If Update Engine Running
        /// </summary>
        /// <returns></returns>
        public static bool UpdateEngineActive()
        {
            var processes = Process.GetProcessesByName(ProcessName);
            return !(processes == null || processes.Length <= 0);
        }
    }

    /// <summary>
    /// Current Update Engine Monitor States
    /// </summary>
    public enum UpdateEngineMonitorState
    {
        Unknown = 0,
        WaitingToStart,
        Running,
        Completed
    }
}
