﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Edge360.Platform.VMS.Updater.Shared.Extensions
{
    public static class IEnumerableExtentions
    {
        public static IEnumerable<T> TakeLast<T>(this IEnumerable<T> source, int num)
        {
            return source.Skip(Math.Max(0, source.Count() - num));
        }
    }
}
