﻿using Edge360.Platform.VMS.Updater.Shared.Interfaces;
using System;
using System.ComponentModel;
using System.Diagnostics;

namespace Edge360.Platform.VMS.Updater.Shared.Extensions
{
    public static class INotifyPropertyChangedExtensions
    {
        public static void MemberwiseCopy(this INotifyPropertyChanged target, INotifyPropertyChanged source)
        {
            // Enforce Not Null and Same Type
            if (target != null && source != null && target.GetType() == source.GetType())
            {
                // Loop through every property and attempt shallow copy into target object
                // We do not want to replace target object only update it with any changed values and property changed events
                foreach (var property in target.GetType().GetProperties())
                {
                    if (property.CanRead && property.CanWrite)
                    {
                        try
                        {
                            // Each property is setup to ignore set if already set to that value
                            target.GetType()?.GetProperty(property.Name)?.SetValue(target, property.GetValue(source));
                        }
                        catch (Exception e)
                        {
                            Trace.TraceError($"Error Performing Memberwise Copy {e.Message}");
                            // IGNORE - Try Next Property
                        }
                    }
                }
            }

        }
    }
}
