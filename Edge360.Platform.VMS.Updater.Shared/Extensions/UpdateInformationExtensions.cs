﻿namespace Edge360.Platform.VMS.Updater.Shared.Extensions
{
    public static class UpdateInformationExtensions
    {
        /// <summary>
        /// Export An External Version of The AppLife UpdateInformation Object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static UpdateInformation Export(this Kjs.AppLife.Update.Controller.UpdateInformation source)
        {
            UpdateElevationType? overrideElevationType = null;
            if (source?.OverrideElevationType != null)
            {
                overrideElevationType = (UpdateElevationType) ((int) source.OverrideElevationType);
            }

            return source == null ? null : new UpdateInformation(source.HistoricalSummary, source.FileSize, source.IsDownloaded, source.CustomData, source.TranslatedHistoricalSummary, source.TranslatedHistoricalSummaryHtml, source.HistoricalSummaryHtml, source.AllowChaining, overrideElevationType, source.SimpleSummaryHtml, source.TranslatedSimpleSummary, source.SimpleSummary, source.PostedDateUtc, source.PostedDate, source.Version, source.TranslatedSimpleSummaryHtml);
        }
    }
}
