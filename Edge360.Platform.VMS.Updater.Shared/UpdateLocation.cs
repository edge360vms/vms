﻿using System;
using Kjs.AppLife.Update.Controller;

namespace Edge360.Platform.VMS.Updater.Shared
{
    /// <summary>
    /// Location Information Where AppLife Updates Are Available
    /// </summary>
    [Serializable]
    public class UpdateLocation
    {
        public string Location { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }

        public UpdateLocation()
        {
        }

        public UpdateLocation(string location)
        {
            Location = location;
        }

        public UpdateLocation(string location, string username, string password, string domain)
        {
            Location = location;
            Username = username;
            Password = password;
            Domain = domain;

        }

        public bool ConfigureUpdateController(UpdateController controller)
        {
            if (string.IsNullOrWhiteSpace(Location))
            {
                return false;
            }

            controller.UpdateLocation = Location;
            controller.UpdateLocationUserName = Username;
            controller.UpdateLocationPassword = Password;
            controller.UpdateLocationDomain = Domain;
            return true;
        }
    }}
