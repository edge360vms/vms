﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using Edge360.Platform.VMS.Updater.Shared.Static;
using log4net;
using log4net.Core;

namespace Edge360.Platform.VMS.Updater.Shared
{
    public static class UpdateManagerStandaloneUpdate
    {
        private const string DirectorXmlFileName = "Director.Xml";
        private const string DirectorZipFileName = "Director.zip";

        private static readonly ILog Log = LogManager.GetLogger
            (MethodBase.GetCurrentMethod().DeclaringType);
        
        public static void HandleStandaloneInstall()
        {
            // Example Valid Args
            // --INSTALL <PATH TO DIRECTOR.XML OR .ZIP> [--FORCE] [--TEST] [--NOCLEANUP]
            if (CommandLineHelper.Args == null)
            {
                Log.Error($"Invalid Installation Request -  {CommandLineHelper.ArgString()}");

                throw new ArgumentException(
                    $"Invalid Installation Request - {Process.GetCurrentProcess()?.MainModule?.FileName}.exe --install [Path To Update Folder]");
            }

            if (!CommandLineHelper.HasArgument("--quiet"))
            {
                Console.WriteLine("Installing...");
            }

            Log.Debug($"Performing Standalone Install");
            Log.Debug(CommandLineHelper.ArgString());

            var allowTestUpdates = CommandLineHelper.HasArgument("--test");
            var forceUpdate = CommandLineHelper.HasArgument("--force");
            var noCleanup = CommandLineHelper.HasArgument("--noclean");
            var waitToClose = CommandLineHelper.HasArgument("--wait");
            var startService = CommandLineHelper.HasArgument("--startservice");

            var installSourcePath = CommandLineHelper.FindArgumentValue("--install");
            if (string.IsNullOrWhiteSpace(installSourcePath) ||
                !Directory.Exists(installSourcePath) && !File.Exists(installSourcePath))
            {
                throw new InvalidOperationException(
                    "Invalid Installation Request - Unable To Locate Installation Source");
            }

            installSourcePath = Path.GetFullPath(installSourcePath);
            Log.Debug($"Install Source Path - {installSourcePath}");

            string installSourcePathTempExtracted = null;
            try
            {
                if (!ValidateDirector(installSourcePath))
                {
                    Log.Debug("Director.Xml Not Found - Trying Director.Zip");

                    // Director.Xml Not Found - Try Director.Zip
                    installSourcePathTempExtracted = ExtractDirector(installSourcePath) ??
                                                     throw new InvalidOperationException(
                                                         "Invalid Installation Request - Unable To Locate Installation Source");

                    // Point To Temp Install Path
                    installSourcePath = installSourcePathTempExtracted;
                }

                Log.Debug("Initializing Update System");

                // Init Update System
                var updateSystem = new VmsClientUpdateSystem(installSourcePath, UpdateManager.AppFileNamePattern);

                if (allowTestUpdates)
                {
                    updateSystem.EnableTestUpdates = true;
                }

                Log.Debug("Checking For Update");


                // Check For Update
                if (!updateSystem.HasUpdate(forceUpdate))
                {
                    throw new InvalidOperationException("No Valid Updates Are Available");
                }

                Log.Debug($"Applying Update {updateSystem?.CurrentUpdate?.Version}");

                // Apply Update
                if (!updateSystem.ApplyUpdate(forceInstall: forceUpdate))
                {
                    throw new InvalidOperationException("Error Installing Update");
                }

                Log.Info("Install Completed");
            }
            finally
            {
                // Delete Extracted File If Created During Install
                if (noCleanup == false && installSourcePathTempExtracted != null)
                {
                    Log.Debug($"Deleting Temp Working Folder {installSourcePathTempExtracted}");

                    try
                    {
                        Directory.Delete(installSourcePathTempExtracted, true);
                    }
                    catch (Exception e)
                    {
                        Log.Error("Error Deleting Extracted Files", e);
                    }
                }
                else
                {
                    Log.Debug($"No Cleanup Requested - Leaving Temp Working Folder - {installSourcePathTempExtracted}");
                }

                try
                {
                    if (startService)
                    {
                        Log.Debug($"Starting Service - {ServiceHelper.UpdateServiceName}");
                        ServiceHelper.StartService(ServiceHelper.UpdateServiceName, ServiceHelper.DefaultTimeout);
                    }
                }
                catch (Exception e)
                {
                    Log.Error($"Error Starting Service - {ServiceHelper.UpdateServiceName}", e);
                }

                if (waitToClose)
                {
                    Console.WriteLine("Press Enter To Exit");
                    Console.ReadLine();
                }
            }
        }


        /// <summary>
        ///     Validates A Folder Contains A Non-Empty Director.Xml
        /// </summary>
        /// <param name="pathToDirector"></param>
        /// <returns></returns>
        private static bool ValidateDirector(string pathToDirector)
        {
            try
            {
                // Check If Path Is Folder
                if (Directory.Exists(pathToDirector))
                {
                    if (File.Exists(Path.Combine(pathToDirector, DirectorXmlFileName)))
                    {
                        pathToDirector = Path.Combine(pathToDirector, DirectorXmlFileName);
                    }
                }

                // Validate Director Xml
                var directorXmlFileInfo = new FileInfo(pathToDirector);
                if (directorXmlFileInfo.Exists &&
                    directorXmlFileInfo.Name.Equals(DirectorXmlFileName, StringComparison.CurrentCultureIgnoreCase) &&
                    directorXmlFileInfo.Length > 0)
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                Log.Error("Error Validating Director", e);
            }

            return false;
        }

        /// <summary>
        ///     Extracts The Director.Zip Into A Temp Folder and Verifies The Contents
        /// </summary>
        /// <param name="pathToDirector">
        ///     Null if Invalid, Path To Temp Folder If Valid (You should delete the folder when you are
        ///     finished)
        /// </param>
        /// <returns></returns>
        private static string ExtractDirector(string pathToDirector)
        {
            if (string.IsNullOrWhiteSpace(pathToDirector))
            {
                throw new ArgumentException("Null Path", nameof(pathToDirector));
            }

            string pathToDirectorZip = null;

            // Check If Path Is Folder
            if (Directory.Exists(pathToDirector))
            {
                if (File.Exists(Path.Combine(pathToDirector, DirectorZipFileName)))
                {
                    pathToDirector = Path.Combine(pathToDirector, DirectorZipFileName);
                }
            }

            // Validate Path To Director.Zip
            var directorZipFileInfo = new FileInfo(pathToDirector);
            if (directorZipFileInfo.Exists &&
                directorZipFileInfo.Name.Equals(DirectorZipFileName, StringComparison.CurrentCultureIgnoreCase) &&
                directorZipFileInfo.Length > 0)
            {
                pathToDirectorZip = directorZipFileInfo.FullName;
            }

            if (pathToDirectorZip == null)
            {
                throw new InvalidOperationException($"Unable To Locate {DirectorZipFileName}");
            }


            // Create Temporary Working Folder
            var tempExtractPath = Path.Combine(Path.GetTempPath(), "Edge360_VMSCLIENT_INSTALL_" + Guid.NewGuid());

            Log.Debug($"Creating Temp Working Folder - {tempExtractPath}");

            Directory.CreateDirectory(tempExtractPath);
            ZipFile.ExtractToDirectory(pathToDirectorZip, tempExtractPath);

            // Verify Extracted Folder Contains Director
            if (!ValidateDirector(tempExtractPath))
            {
                Log.Debug($"Did Not Validate Director - Deleting Temp Working Folder - {tempExtractPath}");

                try
                {
                    // Remove Temporary Folder
                    Directory.Delete(tempExtractPath, true);
                }
                catch (Exception e)
                {
                    Log.Error("Error Removing Temp Working Folder After Not Validating Extracted Director", e);
                }

                throw new InvalidOperationException("Unable To Location Update Director (Extracted)");
            }

            // Return Path To Extracted Director
            return tempExtractPath;
        }

    }
}
