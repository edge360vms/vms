﻿namespace Edge360.Platform.VMS.Updater.Shared
{
    //
    // Summary:
    //     EXPORTED FROM APP LIFE TO SHARE UPDATE DATA EXTERNALLY -->
    //     Defines the possible levels of elevation for an update.
    public enum UpdateElevationType
    {
        //
        // Summary:
        //     The update is applied as the current user, with no elevation.
        None = 0,
        //
        // Summary:
        //     The update is applied using the AppLife Update Service, so that it runs as the
        //     Local System user. If this option is specified and the service is not running,
        //     an error will occur.
        AppLifeUpdateService = 1,
        //
        // Summary:
        //     The update is elevated by a User Account Control (UAC) prompt in Windows Vista
        //     and above. If the user cancels the prompt, the update is cancelled and the application
        //     will restart. If the update is applied on an earlier version of Windows, no UAC
        //     prompt will be presented and the update will be applied as the current user.
        Uac = 2
    }
}